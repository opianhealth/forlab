﻿using System;
using System.Collections.Generic;
using LQT.Core.Domain;

namespace LQT.Core.DataAccess.Interface
{
    public interface IMMGeneralAssumptionDao : IDao<MMGeneralAssumption>
    {
        IList<MMGeneralAssumption> GetAllGeneralAssumptionByProgram(int pid);
        IList<MMGeneralAssumption> GetAllGeneralDynamicAssumptionByProgram(int pid, string UseOn);
        //IList<MMGeneralAssumption> GetAllGeneralAssumptionByMethod(int mid);
        MMGeneralAssumption GetGeneralAssumptionByVariableName(string vname);
        MMGeneralAssumption GetGeneralAssumptionById(int id);
        IList<MMGeneralAssumption> GetAllGeneralAssumptionByType(int typeId,int pid );
        IList<MMGeneralAssumption> GetAllGeneralAssumptionByTypeAndProgram(int typeId, int pid, string UseOn);
    }
}
