﻿using System;
using System.Collections;
using System.Collections.Generic;
using LQT.Core.Domain;

namespace LQT.Core.DataAccess.Interface
{
    public interface INewMorbiditySiteDao : IDao<NewMorbiditySite>
    {
        IList<NewMorbiditySite> GetAllNewMorbiditySite(int forecastid);
        void OpenBatchTransaction();
        void CommitBatchTransaction();
        void RolebackBatchTransaction();
        void BatchSave(NewMorbiditySite artsite);
        void BatchDelete(NewMorbiditySite artsite);
    }
}
