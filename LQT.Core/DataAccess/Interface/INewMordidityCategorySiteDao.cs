﻿using System;
using System.Collections;
using System.Collections.Generic;
using LQT.Core.Domain;

namespace LQT.Core.DataAccess.Interface
{
    public interface INewMordidityCategorySiteDao : IDao<NewMordidityCategorySite>
    {
        IList<NewMordidityCategorySite> GetAllNewMordidityCategorySite(int forecastid);
        void OpenBatchTransaction();
        void CommitBatchTransaction();
        void RolebackBatchTransaction();
        void BatchSave(NewMordidityCategorySite artsite);
        void BatchDelete(NewMordidityCategorySite artsite);
    }
}
