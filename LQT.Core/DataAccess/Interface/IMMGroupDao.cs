﻿using System;
using System.Collections.Generic;
using LQT.Core.Domain;

namespace LQT.Core.DataAccess.Interface
{
    public interface IMMGroupDao : IDao<MMGroup>
    {
        IList<MMGroup> GetAllGroupByProgram(int pid);
        IList<MMGroup> GetAllGroupByMethod(int mid);
        MMGroup GetProgramGroupbyid(int id);
        //MMGroup GetMMForecastParameterByVariableName(string vname);
    }
}
