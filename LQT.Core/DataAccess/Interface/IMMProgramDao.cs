﻿using System;
using System.Collections.Generic;
using LQT.Core.Domain;

namespace LQT.Core.DataAccess.Interface
{
    public interface IMMProgramDao : IDao<MMProgram>
    {
        MMProgram GetMMProgramByName(string name);
    }
}
