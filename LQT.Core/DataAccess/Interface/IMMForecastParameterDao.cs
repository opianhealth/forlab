﻿using System;
using System.Collections.Generic;
using LQT.Core.Domain;
using System.Collections;

namespace LQT.Core.DataAccess.Interface
{
    public interface IMMForecastParameterDao : IDao<MMForecastParameter>
    {
        IList<MMForecastParameter> GetAllForecastParameterByProgram(int pid);
        IList<MMForecastParameter> GetAllForecastParameterByMethod(int mid);
        IList<MMForecastParameter> GetMMForecastParameterByProgramId(int pid, string UseOn);
        MMForecastParameter GetMMForecastParameterByVariableName(string vname);
        MMForecastParameter GetAllForecastParameterById(int id);
     
    
    }
}
