﻿using System;
using System.Collections;
using System.Collections.Generic;
using LQT.Core.Domain;
using LQT.Core.DataAccess.Interface;
using LQT.Core.Util;

using NHibernate;


namespace LQT.Core.DataAccess.NHibernate
{
    public class NHMMForecastParameterDao : NHibernateDao<MMForecastParameter>, IMMForecastParameterDao
    {
        public IList<MMForecastParameter> GetAllForecastParameterByProgram(int pid)
        {
            string hql = "from MMForecastParameter p where p.MMProgram.Id = :pid";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("pid", pid);

            return q.List<MMForecastParameter>();
        }
        public IList<MMForecastParameter> GetMMForecastParameterByProgramId(int pid, string UseOn)
        {
            string hql = "from MMForecastParameter p where p.MMProgram.Id = :pid and p.UseOn =:UseOn and IsActive=1";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("pid", pid);
            q.SetString("UseOn", UseOn);
            return q.List<MMForecastParameter>();
        }
        public MMForecastParameter GetMMForecastParameterByVariableName(string vname)
        {
            string hql = "from MMForecastParameter p where p.VariableName = :vname";

            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetString("vname", vname);

            object obj = q.UniqueResult();
            if (obj != null)
                return (MMForecastParameter)obj;
            return null;
        }
        public MMForecastParameter GetAllForecastParameterById(int id)
        {
            string hql = "from MMForecastParameter p where p.Id = :Id";

            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("Id", id);

            object obj = q.UniqueResult();
            if (obj != null)
                return (MMForecastParameter)obj;
            return null;
        }
        public IList<MMForecastParameter> GetAllForecastParameterByMethod(int mid)
        {
            string hql = "from MMForecastParameter p where p.ForecastMethod = :mid";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("mid", mid);

            return q.List<MMForecastParameter>();
        }
      
      

    }
}
