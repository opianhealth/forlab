﻿using System;
using System.Collections.Generic;
using LQT.Core.Domain;
using LQT.Core.DataAccess.Interface;
using LQT.Core.Util;

using NHibernate;


namespace LQT.Core.DataAccess.NHibernate
{
    public class NHMMProgramDao : NHibernateDao<MMProgram>, IMMProgramDao
    {
        public MMProgram GetMMProgramByName(string name)
        {
            string hql = "from MMProgram t where t.ProgramName = :tname";

            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetString("tname", name);

            IList<MMProgram> result = q.List<MMProgram>();

            if (result.Count > 0)
                return result[0];

            return null;
        }
    }
}
