﻿using System;
using System.Collections;
using System.Collections.Generic;
using LQT.Core.Domain;
using LQT.Core.DataAccess.Interface;
using LQT.Core.Util;

using NHibernate;


namespace LQT.Core.DataAccess.NHibernate
{
    public class NHMMGeneralAssumptionDao : NHibernateDao<MMGeneralAssumption>, IMMGeneralAssumptionDao
    {
        public IList<MMGeneralAssumption> GetAllGeneralAssumptionByProgram(int pid)
        {
            string hql = "from MMGeneralAssumption p where p.MMProgram.Id = :pid And IsActive=1";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("pid", pid);
             
            return q.List<MMGeneralAssumption>();
        }
        public IList<MMGeneralAssumption> GetAllGeneralAssumptionByType(int typeid, int pid)
        {
            string hql = "from MMGeneralAssumption p where p.MMProgram.Id = :pid And p.AssumptionType = :typeid And  IsActive=1";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("typeid", typeid);
            q.SetInt32("pid", pid);
            return q.List<MMGeneralAssumption>();
        }
        public IList<MMGeneralAssumption> GetAllGeneralAssumptionByTypeAndProgram(int typeid, int pid, string UseOn)
        {
            string hql = "from MMGeneralAssumption p where p.MMProgram.Id = :pid And p.AssumptionType = :typeid and p.UseOn =:UseOn And IsActive=1";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("typeid", typeid);
            q.SetInt32("pid", pid);
            q.SetString("UseOn", UseOn);
            return q.List<MMGeneralAssumption>();
        }
        public IList<MMGeneralAssumption> GetAllGeneralDynamicAssumptionByProgram(int pid, string UseOn)
        {
            string hql = "from MMGeneralAssumption p where p.MMProgram.Id = :pid and p.UseOn =:UseOn and IsActive=1 and isnull(VariableFormula,'')=''";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("pid", pid);
            q.SetString("UseOn", UseOn);
            return q.List<MMGeneralAssumption>();

        }
        public MMGeneralAssumption GetGeneralAssumptionByVariableName(string vname)
        {
            string hql = "from MMGeneralAssumption p where p.VariableName = :vname And IsActive=1";

            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetString("vname", vname);

            object obj = q.UniqueResult();
            if (obj != null)
                return (MMGeneralAssumption)obj;
            return null;
        }

        public MMGeneralAssumption GetGeneralAssumptionById(int id)
        {
            string hql = "from MMGeneralAssumption p where p.Id = :Id";

            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("Id", id);

            object obj = q.UniqueResult();
            if (obj != null)
                return (MMGeneralAssumption)obj;
            return null;
        }
        
        //public IList<MMForecastParameter> GetAllForecastParameterByMethod(int mid)
        //{
        //    string hql = "from MMForecastParameter p where p.ForecastMethod = :mid";
        //    ISession session = NHibernateHelper.OpenSession();
        //    IQuery q = session.CreateQuery(hql);
        //    q.SetInt32("mid", mid);

        //    return q.List<MMForecastParameter>();
        //}

    }
}
