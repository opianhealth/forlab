﻿using System;
using System.Collections;
using System.Collections.Generic;
using LQT.Core.Domain;
using LQT.Core.DataAccess.Interface;
using LQT.Core.Util;

using NHibernate;


namespace LQT.Core.DataAccess.NHibernate
{
    public class NHMMGroupDao : NHibernateDao<MMGroup>, IMMGroupDao
    {
        public IList<MMGroup> GetAllGroupByProgram(int pid)
        {
            string hql = "from MMGroup p where p.MMProgram.Id = :pid And p.IsActive=1";
            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("pid", pid);

            return q.List<MMGroup>();
        }
        public MMGroup GetProgramGroupbyid(int id)
        {
            string hql = "from MMGroup p where p.Id = :Id";                                                                                                                                           



            ISession session = NHibernateHelper.OpenSession();
            IQuery q = session.CreateQuery(hql);
            q.SetInt32("Id", id);

            object obj = q.UniqueResult();
            if (obj != null)
                return (MMGroup)obj;
            return null;
        }
        //public MMForecastParameter GetMMForecastParameterByVariableName(string vname)
        //{
        //    string hql = "from MMForecastParameter p where p.VariableName = :vname";

        //    ISession session = NHibernateHelper.OpenSession();
        //    IQuery q = session.CreateQuery(hql);
        //    q.SetString("vname", vname);

        //    object obj = q.UniqueResult();
        //    if (obj != null)
        //        return (MMForecastParameter)obj;
        //    return null;
        //}

        public IList<MMGroup> GetAllGroupByMethod(int mid)
        {
            //string hql = "from MMGroup p where p.ForecastMethod = :mid";
            //ISession session = NHibernateHelper.OpenSession();
            //IQuery q = session.CreateQuery(hql);
            //q.SetInt32("mid", mid);

            //return q.List<MMGroup>();
            return null;
        }

    }
}
