﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace LQT.Core.Util
{
   public class InsertRepository
    {


       public static void ExcuteScripts(SqlConnection cn, string _sql)
       {
           SqlTransaction trans;
           if (cn.State != ConnectionState.Open)
           cn.Open();
           trans = cn.BeginTransaction();

           try
           {
                   SqlCommand cmd = cn.CreateCommand();
                   cmd.CommandType = CommandType.Text;
                   cmd.CommandText = _sql;
                   cmd.Transaction = trans;
                   cmd.CommandTimeout = 300000;
                   cmd.ExecuteNonQuery();
                   trans.Commit();
           }
           catch (Exception ex)
           {
               trans.Rollback();
               cn.Close();
           }
           
       }
       public static bool getMonth(SqlConnection cn, string _sql)
       {
           SqlTransaction trans;
           int cnt = 0;
           if (cn.State != ConnectionState.Open)
               cn.Open();
           trans = cn.BeginTransaction();

           try
           {
               SqlCommand cmd = cn.CreateCommand();
               cmd.CommandType = CommandType.Text;
               cmd.CommandText = _sql;
               cmd.Transaction = trans;
               cmd.CommandTimeout = 300000;
               cnt=Convert.ToInt32(cmd.ExecuteScalar());
               trans.Commit();
               if (cnt != 0) return true;
           }
           catch (Exception ex)
           {
               trans.Rollback();
               cn.Close();
           }
           return false;
       }
       public static bool checkID(SqlConnection cn, string _sql)
       {
           SqlTransaction trans;
           int cnt = 0;
           if (cn.State != ConnectionState.Open)
               cn.Open();
           trans = cn.BeginTransaction();

           try
           {
               SqlCommand cmd = cn.CreateCommand();
               cmd.CommandType = CommandType.Text;
               cmd.CommandText = _sql;
               cmd.Transaction = trans;
               cmd.CommandTimeout = 300000;
               cnt = Convert.ToInt32(cmd.ExecuteScalar());
               trans.Commit();
               if (cnt != 0) return true;
           }
           catch (Exception ex)
           {
               trans.Rollback();
               cn.Close();
           }
           return false;
       }
       public static int returnMonth(SqlConnection cn, string _sql)
       {
           SqlTransaction trans;
           int cnt = 0;
           if (cn.State != ConnectionState.Open)
               cn.Open();
           trans = cn.BeginTransaction();

           try
           {
               SqlCommand cmd = cn.CreateCommand();
               cmd.CommandType = CommandType.Text;
               cmd.CommandText = _sql;
               cmd.Transaction = trans;
               cmd.CommandTimeout = 300000;
               cnt = Convert.ToInt32(cmd.ExecuteScalar());
               trans.Commit();
               if (cnt != 0) return cnt;
           }
           catch (Exception ex)
           {
               trans.Rollback();
               cn.Close();
           }
           return 0;
       }
       public static DataTable SelectData(SqlConnection cn, string _sql)
       {
           DataTable Dt_new = new DataTable();
           SqlCommand cmd = cn.CreateCommand();
           cmd.CommandType = CommandType.Text;
           cmd.CommandText = _sql;
           cmd.CommandTimeout = 300000;
          
           
           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(Dt_new);
           return Dt_new;
       }

    }
}
