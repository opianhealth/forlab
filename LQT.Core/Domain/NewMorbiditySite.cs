
using System;
using System.Collections.Generic;

namespace LQT.Core.Domain
{	
	/// <summary>
	/// MorbidityCategory object for NHibernate mapped table 'MorbidityCategory'.
	/// </summary>
    public class NewMorbiditySite
    {

        private int _id;
      
        private NewMorbidityForecast _newMorbidityForecast;
   
        private int _regionid;
        //private int _siteid;
        private ForlabSite _site;
        #region Constructors

        public NewMorbiditySite()
        {
            this._id = -1;
        }

        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }
        }



        public virtual NewMorbidityForecast NewMorbidityForecast
        {
            get { return _newMorbidityForecast; }
            set { _newMorbidityForecast = value; }
        }

        public virtual int RegionId
        {
            get { return _regionid; }
            set { _regionid = value; }
        }
        //public virtual int SiteId
        //{
        //    get { return _siteid; }
        //    set { _siteid = value; }
        //}
        public virtual ForlabSite Site
        {
            get { return _site; }
            set { _site = value; }
        }
        #endregion
    }

}
