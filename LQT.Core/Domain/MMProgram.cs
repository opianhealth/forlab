
using System;
using System.Collections;
using LQT.Core.Util;

namespace LQT.Core.Domain
{
	
	/// <summary>
    /// MMProgram object for NHibernate mapped table 'MMProgram'.
	/// </summary>
	public class MMProgram 
	{
		#region Member Variables
		
		private int _id;
		private string _programName;
		private string _description;

        private int _gTP;
        private int _rTP;
        private int _noofyear;
        private IList _mmForecastParameters;
        private IList _mmGroups;
        private IList _mmGeneralAssumptions;


		#endregion

		#region Constructors

        public MMProgram() 
		{
			this._id = -1;
		}

		
		#endregion

		#region Public Properties

        public virtual int Id
		{
			get {return _id;}
			set {_id = value;}
		}

        public virtual int GTP
        {
            get { return _gTP; }
            set { _gTP = value; }
        }

        public virtual int RTP
        {
            get { return _rTP; }
            set { _rTP = value; }
        }
        public virtual int NoofYear
        {
            get { return _noofyear; }
            set { _noofyear = value; }
        }
        public virtual string ProgramName
		{
            get { return _programName; }
			set
			{
				if ( value != null && value.Length > 64)
                    throw new ArgumentOutOfRangeException("Invalid value for Program Name", value, value.ToString());
                _programName = value;
			}
		}

        public virtual string Description
		{
			get { return _description; }
			set
			{
				if ( value != null && value.Length > 256)
					throw new ArgumentOutOfRangeException("Invalid value for Description", value, value.ToString());
				_description = value;
			}
		}

        public virtual IList MMForecastParameters
        {
            get
            {
                if (_mmForecastParameters == null)
                {
                    _mmForecastParameters = new ArrayList();
                }
                return _mmForecastParameters;
            }
            set { _mmForecastParameters = value; }
        }

        public virtual IList MMGroups
        {
            get
            {
                if (_mmGroups == null)
                {
                    _mmGroups = new ArrayList();
                }
                return _mmGroups;
            }
            set { _mmGroups = value; }
        }

        public virtual IList MMGeneralAssumptions
        {
            get
            {
                if (_mmGeneralAssumptions == null)
                {
                    _mmGeneralAssumptions = new ArrayList();
                }
                return _mmGeneralAssumptions;
            }
            set { _mmGeneralAssumptions = value; }
        }

        
		#endregion
		
       
	}

}
