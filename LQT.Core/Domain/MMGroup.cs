
using System;
using System.Collections;
using LQT.Core.Util;

namespace LQT.Core.Domain
{
	
	/// <summary>
    /// MMProgram object for NHibernate mapped table 'MMGroup'.
	/// </summary>
	public class MMGroup 
	{
		#region Member Variables
		
		private int _id;
		private string _groupName;
        private bool _IsActive;

        private MMProgram _mmProgram;

		#endregion

		#region Constructors

        public MMGroup() 
		{
			this._id = -1;
		}

		
		#endregion

		#region Public Properties

        public virtual int Id
		{
			get {return _id;}
			set {_id = value;}
		}

        public virtual string GroupName
		{
            get { return _groupName; }
			set
			{
				if ( value != null && value.Length > 64)
                    throw new ArgumentOutOfRangeException("Invalid value for Group Name", value, value.ToString());
                _groupName = value;
			}
		}
        public virtual bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        public virtual MMProgram MMProgram
        {
            get { return _mmProgram; }
            set { _mmProgram = value; }
        }
        
		#endregion
		
       
	}

}
