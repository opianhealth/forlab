
using System;
using System.Collections.Generic;
using LQT.Core.Util;

namespace LQT.Core.Domain
{
	
	public class MMGeneralAssumption 
	{


		#region Member Variables
		
		private int _id;
        private string _variableName;
        private int _variableDataType;
        private string _useOn;
        private string _variableFormula;
        private string _varCode;
        private int _assumptionType;
        private bool _variableEffect;
        private bool _IsActive;
        private MMProgram _mmProgram;

		#endregion

		#region Constructors

        public MMGeneralAssumption() 
		{
			this._id = -1;
		}

		
		#endregion

		#region Public Properties

		public virtual int Id
		{
			get {return _id;}
			set {_id = value;}
		}


        public virtual string VariableName
        {
            get { return _variableName; }
            set { _variableName = value; }
        }

        public virtual int VariableDataType
        {
            get { return _variableDataType; }
            set { _variableDataType = value; }
        }

        public virtual string UseOn
        {
            get { return _useOn; }
            set { _useOn = value; }
        }

        public virtual string VariableFormula
        {
            get { return _variableFormula; }
            set { _variableFormula = value; }
        }

		public virtual MMProgram MMProgram
		{
			get { return _mmProgram; }
			set { _mmProgram = value; }
		}

        public virtual string VarCode
        {
            get { return _varCode; }
            set { _varCode = value; }
        }

        public virtual int AssumptionType
        {
            get { return _assumptionType; }
            set { _assumptionType = value; }
        }

        public virtual bool VariableEffect
        {
            get { return _variableEffect; }
            set { _variableEffect = value; }
        }
        public virtual bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        public virtual string FvarCode
        {
            get { return _varCode + _id.ToString(); }
        }

		#endregion
	}

}
