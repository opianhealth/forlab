
using System;
using System.Collections.Generic;

namespace LQT.Core.Domain
{	
	/// <summary>
	/// MorbidityCategory object for NHibernate mapped table 'MorbidityCustomeAssumption'.
	/// </summary>
    public class MorbidityCustomeAssumption
    {

        private int _id;
        private string _variableName;
        private string _dataType;
        private bool _useForSite;
        private bool _useForAggregate;
        private string _variableValue;
        private string _pGroup;
        private string _dieseas;

        private NewMorbidityForecast _morbidityForecast;
       

        #region Constructors

        public MorbidityCustomeAssumption()
        {
            this._id = -1;
        }

        #endregion

        #region Public Properties

        public virtual string Group
        {
            get { return _pGroup; }
            set { _pGroup = value; }
        }
        public virtual string Dieseas
        {
            get { return _dieseas; }
            set { _dieseas = value; }
        }

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public virtual string VariableName
        {
            get { return _variableName; }
            set { _variableName = value; }
        }
        public virtual string DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }

        public virtual bool UseForSite
        {
            get { return _useForSite; }
            set { _useForSite = value; }
        }
        public virtual bool UseForAggregate
        {
            get { return _useForAggregate; }
            set { _useForAggregate = value; }
        }
        public virtual string VariableValue
        {
            get { return _variableValue; }
            set { _variableValue = value; }
        }
        public virtual NewMorbidityForecast MorbidityForecast
        {
            get { return _morbidityForecast; }
            set { _morbidityForecast = value; }
        }

     

        #endregion
    }

}
