
using System;
using System.Collections.Generic;
using LQT.Core.Util;

namespace LQT.Core.Domain
{	
	/// <summary>
	/// MordidityCategorySite object for NHibernate mapped table 'MordidityCategorySite'.
	/// </summary>
	public class NewMordidityCategorySite
    {
        #region properties

        private int _id;
		
		private NewMorbidityCategory _morbidityCategory;
		private ForlabSite _site;

        #endregion

        #region Constructors

        public NewMordidityCategorySite() 
		{
			this._id = -1;

		}


		#endregion
        		
		public virtual int Id
		{
			get {return _id;}
			set {_id = value;}
		}


        #region public property
       
		public virtual NewMorbidityCategory NewMorbidityCategory
		{
			get { return _morbidityCategory; }
			set { _morbidityCategory = value; }
		}

		public virtual ForlabSite Site
		{
			get { return _site; }
			set { _site = value; }
		}

		#endregion

    
        public virtual int LargestPlatformCount()
        {
            int temp = 0;
            if (Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.CD4).Count > temp)
                temp = Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.CD4).Count;

            if (Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Chemistry).Count > temp)
                temp = Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Chemistry).Count;

            if (Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Hematology).Count > temp)
                temp = Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Hematology).Count;

            if (Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.RapidTest).Count > temp)
                temp = Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.RapidTest).Count;

            if (Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.ViralLoad).Count > temp)
                temp = Site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.ViralLoad).Count;
            return temp;
        }

        public virtual bool HasPlatform(ClassOfMorbidityTestEnum ctest)
        {
            return NoOfPlatform(ctest) > 0;
        }

        public virtual int NoOfPlatform(ClassOfMorbidityTestEnum ctest)
        {
            return Site.GetInstrumentByPlatform(ctest).Count;
        }

        /*
        public virtual bool TestWasSelected(ClassOfMorbidityTestEnum ctest)
        {
            bool result = false;
            switch (ctest)
            {
                case ClassOfMorbidityTestEnum.CD4:
                    result = ForecastCD4;
                    break;
                case ClassOfMorbidityTestEnum.Chemistry:
                    result = ForecastChemistry;
                    break;
                case ClassOfMorbidityTestEnum.Consumable:
                    result = ForecastConsumable;
                    break;
                case ClassOfMorbidityTestEnum.Hematology:
                    result = ForecastHematology;
                    break;
                case ClassOfMorbidityTestEnum.OtherTest:
                    result = ForecastOtherTest;
                    break;
                case ClassOfMorbidityTestEnum.RapidTest:
                    result = ForecastVCT;
                    break;
                case ClassOfMorbidityTestEnum.ViralLoad:
                    result = ForecastViralLoad;
                    break;
            }

            return result;
        }
        */
        public virtual bool TestWasReffered(ClassOfMorbidityTestEnum ctest)
        {
            bool result = false;
            switch (ctest)
            {
                case ClassOfMorbidityTestEnum.CD4:
                    result = Site.CD4RefSite > 0;
                    break;
                case ClassOfMorbidityTestEnum.Chemistry:
                    result = Site.ChemistryRefSite > 0;
                    break;
                //case ClassOfMorbidityTestEnum.Consumable:
                //    result = Site.consRefSite > 0;
                //    break;
                case ClassOfMorbidityTestEnum.Hematology:
                    result = Site.HematologyRefSite > 0;
                    break;
                case ClassOfMorbidityTestEnum.OtherTest:
                    result = Site.OtherRefSite > 0;
                    break;
                //case ClassOfMorbidityTestEnum.RapidTest:
                //    result = Site.RefSite > 0;
                //    break;
                case ClassOfMorbidityTestEnum.ViralLoad:
                    result = Site.ViralLoadRefSite > 0;
                    break;
            }

            return result;
        }

        public virtual int TestRefferedSiteId(ClassOfMorbidityTestEnum ctest)
        {
            int result = 0;
            switch (ctest)
            {
                case ClassOfMorbidityTestEnum.CD4:
                    result = Site.CD4RefSite;
                    break;
                case ClassOfMorbidityTestEnum.Chemistry:
                    result = Site.ChemistryRefSite;
                    break;
                case ClassOfMorbidityTestEnum.Hematology:
                    result = Site.HematologyRefSite;
                    break;
                case ClassOfMorbidityTestEnum.OtherTest:
                    result = Site.OtherRefSite;
                    break;
                case ClassOfMorbidityTestEnum.ViralLoad:
                    result = Site.ViralLoadRefSite;
                    break;
            }

            return result;
        }
	}

}
