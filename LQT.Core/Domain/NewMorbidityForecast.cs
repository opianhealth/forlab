
using System;
using System.Collections.Generic;

using LQT.Core.Util;

namespace LQT.Core.Domain
{	
	/// <summary>
	/// MorbidityForecast object for NHibernate mapped table 'MorbidityForecast'.
	/// </summary>
    public class NewMorbidityForecast
    {

        private int _id;


        private string _forecastCode;
        private string _scopeOfTheForecast;
        private string _period;
        private int _extension;
        private DateTime _forecastDate;
        private int _monthInPeriod;
        private string _status;
        private DateTime _lastUpdated;
        private bool _conductSitebySite;
        private bool _includeControl;
        private string _reportingPeriod;
        private IList<NewMorbidityCategory> _morbidityCategories;
        private IList<NewMorbiditySite> _newMorbiditySites;

        private int _optForecastFormula;

        public virtual int OptForecastFormula
        {
            get { return _optForecastFormula; }
            set { _optForecastFormula = value; }
        }
        public virtual string ForecastCode
        {
            get { return _forecastCode; }
            set { _forecastCode = value; }
        }
        public virtual string ReportingPeriod
        {
            get { return _reportingPeriod; }
            set { _reportingPeriod = value; }
        }
        public virtual string ScopeOfTheForecast
        {
            get { return _scopeOfTheForecast; }
            set { _scopeOfTheForecast = value; }
        }
        public virtual string Period
        {
            get { return _period; }
            set { _period = value; }
        }
        public virtual int Extension
        {
            get { return _extension; }
            set { _extension = value; }
        }
        public virtual int MonthInPeriod
        {
            get { return _monthInPeriod; }
            set { _monthInPeriod = value; }
        }
        public virtual DateTime ForecastDate
        {
            get { return _forecastDate; }
            set { _forecastDate = value; }
        }
        public virtual DateTime LastUpdated
        {
            get { return _lastUpdated; }
            set { _lastUpdated = value; }
        }
        public virtual bool ConductSitebySite
        {
            get { return _conductSitebySite; }
            set { _conductSitebySite = value; }
        }
        public virtual bool IncludeControl
        {
            get { return _includeControl; }
            set { _includeControl = value; }
        }
        public virtual IList<NewMorbidityCategory> MorbidityCategories
        {
            get
            {
                if (_morbidityCategories == null)
                {
                    _morbidityCategories = new List<NewMorbidityCategory>();
                }
                return _morbidityCategories;
            }
            set { _morbidityCategories = value; }
        }
        public virtual NewMorbidityCategory GetCategoryById(int id)
        {
            foreach (NewMorbidityCategory cat in MorbidityCategories)
            {
                if (cat.Id == id)
                    return cat;
            }
            return null;
        }

        public virtual IList<NewMorbiditySite> NewMorbiditySites
        {
            get
            {
                if (_newMorbiditySites == null)
                {
                    _newMorbiditySites = new List<NewMorbiditySite>();
                }
                return _newMorbiditySites;
            }
            set { _newMorbiditySites = value; }
        }
        public virtual NewMorbiditySite GetNewMorbiditySiteById(int id)
        {
            foreach (NewMorbiditySite cat in NewMorbiditySites)
            {
                if (cat.Id == id)
                    return cat;
            }
            return null;
        }

        #region Constructors

        public NewMorbidityForecast()
        {
            this._id = -1;
            //this._useRegionAsCat = false;
            this._status = ForecastStatusEnum.OPEN.ToString();
        }


        #endregion

        #region Public Properties

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }
        }


        public virtual string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public virtual ForecastStatusEnum StatusEnum
        {
            get
            {
                if (Status == null)
                    return ForecastStatusEnum.OPEN;
                return (ForecastStatusEnum)Enum.Parse(typeof(ForecastStatusEnum), _status);
            }
        }
        

        #endregion


    }

}
