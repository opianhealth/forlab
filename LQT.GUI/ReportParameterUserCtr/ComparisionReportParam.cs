﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Util;
using LQT.Core.Domain;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using LQT.Core;
using LQT.GUI.Reports;
using Microsoft.Reporting.WinForms;

namespace LQT.GUI.ReportParameterUserCtr
{
    public partial class ComparisionReportParam : LQT.GUI.ReportParameterUserCtr.RptBaseUserControl
    {
        SqlConnection con = ConnectionManager.GetInstance().GetSqlConnection();
        private ForecastInfo _mMforecastinfo;
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mPMGeneralAssumption;
        DataSet ds = new DataSet();
        private int ID = 0;
        private string forecasttype = "";
        string onsite = "", _columnName = "", _MonthName = "", _variableName = "", varName = "", svarName = "", _type = "", sqlretrieve = "";
        int noofMonth = 0, typeId = 0;
        DataTable dtGetValue = new DataTable();
        DataTable dtParameterValue = new DataTable();
        DataTable dtPGetValue = new DataTable();
        DataTable DTmonth = new DataTable();
        List<string> listProductType = new List<string>();
        DataTable DT = new DataTable();
        decimal _parameterSum = 0, _tparameterSum = 0;
        public int k = 0;
        public ComparisionReportParam()
        {
            InitializeComponent();
            PopForecastInfo();
        }

        private void PopForecastInfo()
        {

            IList ServiceForecastInfo = DataRepository.GetForecastInfoByMethodology(MethodologyEnum.SERVICE_STATISTIC.ToString()).ToList();
            ReportRepository.AddItem(ServiceForecastInfo, typeof(ForecastInfo), "Id", "ForecastNo", "< Select Option >");
            cobservice.DataSource = ServiceForecastInfo;

            IList ConsumptionForecastInfo = DataRepository.GetForecastInfoByMethodology(MethodologyEnum.CONSUMPTION.ToString()).ToList();
            ReportRepository.AddItem(ConsumptionForecastInfo, typeof(ForecastInfo), "Id", "ForecastNo", "< Select Option >");
            cobconsumption.DataSource = ConsumptionForecastInfo;


            IList demographyForecastInfo = DataRepository.GetForecastInfoByMethodology("MORBIDITY").ToList();
            ReportRepository.AddItem(demographyForecastInfo, typeof(ForecastInfo), "Id", "ForecastNo", "< Select Option >");
            //IList demographyForecastInfo = DataRepository.GetAllMorbidityForecast().ToList();
            //ReportRepository.AddItem(demographyForecastInfo, typeof(MorbidityForecast), "Id", "Title", "< Select Option >");
            cobdemography.DataSource = demographyForecastInfo;

           
        }

        public override string GetControlTitle
        {
            get
            {
                return "Forecast Comparison Report";
            }
        }

        private void btnviewreport_Click(object sender, EventArgs e)
        {

            int SforecastId = -1;
            int CforecastId = -1;
            int DforecastId = -1;
            DataSet _rDataSet;
            if (cobservice.SelectedValue.ToString() != "-1")
                SforecastId = int.Parse(cobservice.SelectedValue.ToString());

            if (cobconsumption.SelectedValue.ToString() != "-1")
                CforecastId = int.Parse(cobconsumption.SelectedValue.ToString());

            if (cobdemography.SelectedValue.ToString() != "-1")
                DforecastId = int.Parse(cobdemography.SelectedValue.ToString());

            ReportParameter pSForecastId = new ReportParameter("SForecastId", SforecastId.ToString());
            ReportParameter pCForecastId = new ReportParameter("CForecastId", CforecastId.ToString());
            ReportParameter pDForecastId = new ReportParameter("MForecastId", DforecastId.ToString());

            param.Add(pSForecastId);
            param.Add(pCForecastId);
            param.Add(pDForecastId);
            

            SqlParameter rpMForecastId = new SqlParameter();
            rpMForecastId.ParameterName = "MForecastId";
            rpMForecastId.Value = DforecastId;

            SqlParameter rpSForecastId = new SqlParameter();
            rpSForecastId.ParameterName = "SForecastId";
            rpSForecastId.Value = SforecastId;

            SqlParameter rpCForecastId = new SqlParameter();
            rpCForecastId.ParameterName = "CForecastId";
            rpCForecastId.Value = CforecastId;

            sqlParams.Clear();
            sqlParams.Add(rpMForecastId);
            sqlParams.Add(rpSForecastId);
            sqlParams.Add(rpCForecastId);
            if (DT.Rows.Count > 0) DT.Rows.Clear();
            if (cobdemography.SelectedValue.ToString() != "-1")
            {
                ID = DforecastId; 
                DataTable dt = fillData();
                

                if (con.State == ConnectionState.Closed) con.Open();
                string sql = "DELETE FROM insertReportData WHERE ForecastId=" + ID + "";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.ExecuteNonQuery();
                //creating object of SqlBulkCopy  
                SqlBulkCopy objbulk = new SqlBulkCopy(con);
                //assigning Destination table name  
                objbulk.DestinationTableName = "insertReportData";
                //Mapping Table column  
                objbulk.ColumnMappings.Add("ForecastId", "ForecastId");
                objbulk.ColumnMappings.Add("Methodology", "Methodology");
                objbulk.ColumnMappings.Add("ProductType", "ProductType");
                objbulk.ColumnMappings.Add("FYear", "FYear");

                objbulk.ColumnMappings.Add("Quantity", "Quantity");
                objbulk.ColumnMappings.Add("TotalPrice", "TotalPrice");
                objbulk.ColumnMappings.Add("Duration", "Duration");
                objbulk.ColumnMappings.Add("DurationDateTime", "DurationDateTime");
                objbulk.ColumnMappings.Add("Title", "Title");
                //inserting bulk Records into DataBase   
                objbulk.WriteToServer(dt);  
                //fnStoredProc(dt, DforecastId);
            }
            _rDataSet = ReportRepository.GetDataSet(sqlConnection, sqlParams, "spGetForecastComparision");

            //DataSet fg = ReportRepository.GetDataSet(sqlConnection, sqlParams, "fnGetMorbidityMethoSummary1");
            filinfo = new FileInfo(Path.Combine(AppSettings.GetReportPath, String.Format("{0}.rdlc", OReports.forecastcomparision)));

            FrmReportViewer frmRV = new FrmReportViewer(filinfo, _rDataSet, param);
            frmRV.Dock = DockStyle.Fill;
            frmRV.ShowDialog();
        }
        private DataTable fillData()
        {
            // Chart12.Width = 2500;

            decimal existingtestnumber;
            decimal newpatienttestnumber;
            decimal Per;
            decimal testPermonth;
            decimal totaltest;
            decimal ttltest = 0;
            int period = 0;
            if (DTmonth.Rows.Count > 0) DTmonth.Rows.Clear();
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT *   FROM [TestByMonth] WHERE ForeCastID=" + ID + "  order by TestID", con);
            adapt.Fill(DTmonth);
            period = Convert.ToInt32(DTmonth.Rows[0]["Duration"]) * 12;
            con.Close();
            existingtestnumber = (Convert.ToDecimal(DTmonth.Rows[0]["ExistingPatient"]) * (Convert.ToDecimal(DTmonth.Rows[0]["Duration"]) * Convert.ToDecimal(DTmonth.Rows[0]["TotalTestPerYear"])));
            // existingtestnumber = existingtestnumber / (Convert.ToDecimal(DTmonth.Rows.Count));
            existingtestnumber = existingtestnumber / (Convert.ToInt32(period));
            //if (_mMforecastinfo.ForecastType == "S") _type = MorbidityVariableUsage.OnEachSite.ToString();
            //else if (_mMforecastinfo.ForecastType == "C") _type = MorbidityVariableUsage.OnAggSite.ToString();
            //_mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Test_Assumption"), _mMforecastinfo.ProgramId, _type);

            //foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            //{
            //    if (gAssumption.AssumptionType == 3 && gAssumption.UseOn.ToString() == _type)
            //    {
            //        DataRow dr = dtGetValue.NewRow();
            //        dr["VariableName"] = gAssumption.VariableName.ToString();
            //        dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
            //        dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
            //        dtGetValue.Rows.Add(dr);
            //    }
            //    dtGetValue.AcceptChanges();
            //}


            for (int i = 0; i < DTmonth.Rows.Count; i++)
            {
                //if (Convert.ToDouble(DTmonth.Rows[i]["tstNo"]) > 0)
                //{
                //foreach (DataRow dr in dtGetValue.Rows)
                //{
                //    _parameterSum = ((Convert.ToDecimal(DTmonth.Rows[i]["" + dr["VariableName"].ToString() + ""])));
                //    if (dr["variableEffect"].ToString() == "True") _tparameterSum = _tparameterSum + _parameterSum;
                //    if (dr["variableEffect"].ToString() == "False") _tparameterSum = _tparameterSum - _parameterSum;
                //}
                //if (Convert.ToString(DTmonth.Rows[i]["TestID"]) == "3")
                //{

                //}
                //Per = _tparameterSum;
                //Per = Convert.ToDecimal(DTmonth.Rows[i]["TestRepeatPerYear"]) + Convert.ToDecimal(DTmonth.Rows[i]["SymptomTestPerYear"]);
                testPermonth = Convert.ToDecimal(DTmonth.Rows[i]["tstNo"]) * Convert.ToDecimal(DTmonth.Rows[i]["NewPatient"]);
                totaltest = testPermonth + existingtestnumber;
                //newpatienttestnumber = totaltest + (totaltest * (Per / 100));
                newpatienttestnumber = totaltest;
                decimal newpatienttestnumber1 = Math.Round(newpatienttestnumber, 2);
                // calculation(Convert.ToDouble(DTmonth.Rows[i]["tstNo"]), Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k);
                calculation(Convert.ToDouble(newpatienttestnumber1), Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k);
                k++;

                //}

            }
            DataTable fg = DT;
            DataTable get = new DataTable();
            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("ForecastId", typeof(int));
            dtReport.Columns.Add("Methodology", typeof(string));

            dtReport.Columns.Add("ProductType", typeof(string));
            dtReport.Columns.Add("FYear", typeof(int));
            dtReport.Columns.Add("Quantity", typeof(decimal));
            dtReport.Columns.Add("TotalPrice", typeof(decimal));

            dtReport.Columns.Add("Duration", typeof(string));
            dtReport.Columns.Add("DurationDateTime", typeof(DateTime));
            dtReport.Columns.Add("Title", typeof(string));
            dtReport.Columns.Add("totalCost", typeof(decimal));

            var result = from row in fg.AsEnumerable()
                         group row by row.Field<string>("TypeName") into grp
                         select new
                         {
                             TeamID = grp.Key,
                             MemberCount = grp.Sum(row => row.Field<decimal>("totalCost")),
                             CountThisWeek = grp.Sum(r => r.Field<decimal>("totalquantityinpack") * r.Field<decimal>("Price"))
                         };
            foreach (var item in result)
            {
                string product = item.TeamID.ToString();
                string bn = item.MemberCount.ToString();
                string bn1 = item.CountThisWeek.ToString();
            }

            //var result = from row in fg.AsEnumerable()
            //             group row by row.Field<string>("TypeName") into grp
            //             select new
            //             {
            //                 Methodology = "Morbidity Statistics",
            //                // ProductType=grp.Max(row => row.Field<string>("TypeName")),
            //                 ProductType = grp.Key,
            //                 Fyear=1,
            //                 Quantity = grp.Sum(row => row.Field<decimal>("totalquantityinpack")),
            //                 TotalPrice = grp.Sum(row =>row.Field<decimal>("totalquantityinpack") * row.Field<decimal>("Price")),
            //                 Duration = grp.Max(row => row.Field<decimal>("Duration")),
            //                 DurationDateTime = grp.Max(row => row.Field<decimal>("DurationDateTime"))
            //             };
            //foreach (var item in result)
            //{
            //    string product = item.ProductType.ToString();
            //    string bn = item.Quantity.ToString();
            //    string bn1 = item.TotalPrice.ToString();
            //}
            DataTable t = //
            get = DT.AsEnumerable().GroupBy(r => r.Field<int>("TypeID")).Select(g =>
            {
                var row = DT.NewRow();
               
                row["Methodology"] = g.Max(r => r.Field<string>("Methodology"));
                row["ProductType"] = g.Max(r => r.Field<string>("TypeName"));
                row["Fyear"] = g.Max(r => r.Field<string>("Fyear")); ;
                row["Quantity"] = g.Sum(r => r.Field<decimal>("totalquantityinpack"));
               // row["TotalPrice"] = g.Sum(r => r.Field<decimal>("totalquantityinpack") * r.Field<decimal>("Price"));
                row["TotalPrice"] = g.Sum(r => r.Field<decimal>("totalCost"));
                row["Duration"] = g.Max(r => r.Field<string>("Duration"));
                row["DurationDateTime"] = g.Max(r => r.Field<string>("DurationDateTime"));
                row["Title"] = g.Max(r => r.Field<string>("Title"));
                row["totalCost"] = g.Sum(r => r.Field<decimal>("totalCost")); 
                return row;
            }).CopyToDataTable();

            foreach (DataRow dr in get.Rows)
            {
                DataRow dr1 = dtReport.NewRow();
                dr1["ForecastId"] = ID;
                dr1["Title"] = dr["Title"];
                dr1["Methodology"] = dr["Methodology"];
                dr1["ProductType"] = dr["ProductType"];
                dr1["Fyear"] = dr["Fyear"];
                dr1["Quantity"] = dr["Quantity"];
                dr1["TotalPrice"] = dr["TotalPrice"];
                dr1["Duration"] = dr["Duration"];
                dr1["DurationDateTime"] = dr["DurationDateTime"];
                dr1["totalCost"] = dr["totalCost"];
                dtReport.Rows.Add(dr1);
            }
            int m = DT.Rows.Count;
            int m1 = get.Rows.Count;
            int m2 = dtReport.Rows.Count;

            get.Rows.Clear();
            con.Close();
            return dtReport;

        }
        private void calculation(double testnumber, int testid, string Month, int j)
        {
            string str8 = "";
            string strsites = "", _sqlStr = "";
            string _sql = "";
            _sql = string.Format(@"Select Isnull([ForecastType],'') as forecasttype ,ForecastNo As Title  ,Right(Replace(convert(varchar(11), StartDate, 106),' ','-'),8) + '    '+ Right(Replace(convert(varchar(11), ForecastDate, 106),' ','-'),8) As forecastperiod,datename(m,ForecastDate)+'-'+cast(datepart(yyyy,ForecastDate) as varchar) as Duration,ForecastDate as DurationDateTime from ForecastInfo Where [ForecastID]='{0}'", ID);
            //using (connection = new SqlConnection(con))
            //{
            DataTable DTProductneed = new DataTable();

            //SqlCommand cmd8 = new SqlCommand();
            //cmd8.CommandType = CommandType.Text;

            //cmd8.Connection = con;
            //cmd8.CommandText = _sql;
            //if (con.State == ConnectionState.Closed)
            //{
            //    con.Open();
            //}
            //forecasttype = Convert.ToString(cmd8.ExecuteScalar());

            //con.Close();
            DataTable dtPParameterValue = new DataTable();
            DataTable dtGetValue = new DataTable();
            decimal _parameterSum = 0, _tparameterSum = 0;
            decimal _PparameterSum = 0, _PtparameterSum = 0;
            dtGetValue.Columns.Add("VariableName", typeof(string));
            dtGetValue.Columns.Add("VariableEffect", typeof(string));
            dtGetValue.Columns.Add("VariableDataType", typeof(string));

            //_mPMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Product_Assumption"), _mMforecastinfo.ProgramId, _type);

            //sqlretrieve = "";


            //foreach (MMGeneralAssumption gAssumption in _mPMGeneralAssumption)
            //{
            //    if (gAssumption.AssumptionType == 2 && gAssumption.UseOn.ToString() == _type)
            //    {
            //        DataRow dr = dtGetValue.NewRow();
            //        sqlretrieve += "[" + gAssumption.VariableName + "],";
            //        dr["VariableName"] = gAssumption.VariableName.ToString();
            //        dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
            //        dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
            //        dtGetValue.Rows.Add(dr);
            //    }
            //    dtGetValue.AcceptChanges();
            //}
            //sqlretrieve = sqlretrieve.TrimEnd(',');
            //string strSql = "select " + sqlretrieve + " from TestingAssumption  where ForecastinfoID=" + ID + "";
            //SqlDataAdapter adapt2 = new SqlDataAdapter(strSql, con);
            //adapt2.Fill(dtPParameterValue);

            //for (int ki = 0; ki < dtPParameterValue.Rows.Count; ki++)
            //{
            //    foreach (DataColumn dc in dtPParameterValue.Columns)
            //    {
            //        foreach (DataRow dr in dtPGetValue.Rows)
            //        {
            //            string v1 = dr["VariableName"].ToString();
            //            string v2 = dc.ColumnName.ToString();
            //            if (v1.Equals(v2))
            //            {
            //                _PparameterSum = ((Convert.ToDecimal(dtPParameterValue.Rows[ki]["" + v2 + ""])) / 100);
            //                if (dr["variableEffect"].ToString() == "True") _PtparameterSum = _PtparameterSum + _PparameterSum;
            //                if (dr["variableEffect"].ToString() == "False") _PtparameterSum = _PtparameterSum - _PparameterSum;

            //            }

            //        }
            //    }

            //}
            DataTable Dtforecastinfo = new DataTable();
           if(con.State==ConnectionState.Closed) con.Open();
            SqlDataAdapter adaptinfo = new SqlDataAdapter(_sql, con);
            adaptinfo.Fill(Dtforecastinfo);

            con.Close();
            if (Dtforecastinfo.Rows[0]["forecasttype"].ToString() == "S")
            {
                str8 = "select Avg(s.WorkingDays) as WorkingDays from ForecastSiteInfo FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastinfoID=" + ID + "";
                strsites = "select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + " ";
            }
            else
            {
                str8 = " SELECT Avg(s.WorkingDays) as WorkingDays  FROM [ForecastCategorySiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=" + ID + "";
                strsites = "select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + " ";
            }
            DataTable Dtsiteworkingdays = new DataTable();
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(str8, con);
            adapt.Fill(Dtsiteworkingdays);

            con.Close();

            con.Close();
            string str1 = "";
            //if (Dtforecastinfo.Rows[0]["forecasttype"].ToString() == "S")
            //{
            //    str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            //    str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
            //    str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
            //    str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate ";

            //    //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
            //    //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
            //    //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
            //    str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
            //    str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            //    //str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            //    str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
            //    str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
            //    str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS ";
            //    str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
            //    str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
            //    str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
            //    /////Control useage

            //    str1 += "	Union ";


            //    str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
            //    str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
            //    str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
            //    str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

            //    str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
            //    str1 += "	Union ";


            //    str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
            //    str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
            //    str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    from SiteInstrument where siteid  in (select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
            //    str1 += "   group by InstrumentID),0) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

            //    str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
            //    str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
            //    str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
            //    str1 += "   group by T.month1,T.ProductId ";
            //}
            //else
            //{
            //    str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            //    str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
            //    str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
            //    str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate ";

            //    //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
            //    //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
            //    //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
            //    str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
            //    str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            //    //str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            //    str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
            //    str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
            //    str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastCategorySiteInfo] FS ";
            //    str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
            //    str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
            //    str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
            //    /////Control useage

            //    str1 += "	Union ";


            //    str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
            //    str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
            //    str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
            //    str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

            //    str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
            //    str1 += "	Union ";


            //    str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
            //    str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
            //    str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    from SiteInstrument where siteid  in (select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
            //    str1 += "   group by InstrumentID),0) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

            //    str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
            //    str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
            //    str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
            //    str1 += "   group by T.month1,T.ProductId ";

            //}




            //string str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
            str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
            str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate,'" + Dtforecastinfo.Rows[0]["Duration"].ToString() + "' as Duration,'" + Dtforecastinfo.Rows[0]["DurationDateTime"].ToString() + "' as DurationDateTime,Max(PT.TypeName) as ProductType,'Morbidity Statistics' as Methodology,'1' as Fyear,0 as Quantity,0 as TotalPrice ";

            //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
            //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
            //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
            str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
            str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
            str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
            str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS ";
            str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
            str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
            str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
            /////Control useage

            str1 += "	Union ";


            str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
            str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
            str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
            str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

            str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
            str1 += "	Union ";


            str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
            str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
            str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    from SiteInstrument where siteid  in (select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
            str1 += "   group by InstrumentID),0) when CU.Period='Monthly' then (CU.UsageRate/12) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

            str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
            str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
            str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
            str1 += "   group by T.month1,T.ProductId ";
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataAdapter adapt1 = new SqlDataAdapter(str1, con);
            adapt1.Fill(DTProductneed);

            //DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
            //for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
            //{
            //    DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

            //}
            //DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

            //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            //{
            //    DTProductneed.Rows[i2]["totalquantityinpack"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(_PtparameterSum)))));
            //}

            //DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));
            //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            //{
            //    DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(0)));
            //}

            DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
            for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
            {
                DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

            }
            DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));

          
            for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            {
                DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i2]["ProgramGrowRate"].ToString()) / 100))));
            }
            DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

            for (int i3 = 0; i3 < DTProductneed.Rows.Count; i3++)
            {
                DTProductneed.Rows[i3]["totalquantityinpack"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i3]["WastageRate"].ToString()) / 100)));

            }


            DTProductneed.Columns.Add("totalCost", typeof(decimal));
            for (int i4 = 0; i4 < DTProductneed.Rows.Count; i4++)
            {
                DTProductneed.Rows[i4]["totalCost"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i4]["totalquantityinpack"].ToString()) * Convert.ToDecimal(DTProductneed.Rows[i4]["Price"].ToString()));

            }

          
            con.Close();
            /////consumble  useage
            if (j == 0)
            {
                DT = DTProductneed;
            }
            else
            {
               
                foreach (DataRow dr in DTProductneed.Rows)
                {
                    DataRow[] rows = DT.Select("ProductId='" + dr["ProductId"] + "' and month1='" + dr["month1"] + "' And TypeID='" + dr["TypeID"] + "'");
                    if (rows.Length > 0)
                    {
                        rows[0]["Productneed"] = Convert.ToDecimal(rows[0]["Productneed"]) + Convert.ToDecimal(dr["Productneed"]);
                        rows[0]["Adjustedpacksize"] = Convert.ToDecimal(rows[0]["Adjustedpacksize"]) + Convert.ToDecimal(dr["Adjustedpacksize"]);
                        rows[0]["adjProgramgrowthrate"] = Convert.ToDecimal(rows[0]["adjProgramgrowthrate"]) + Convert.ToDecimal(dr["adjProgramgrowthrate"]);
                        rows[0]["totalquantityinpack"] = Convert.ToDecimal(rows[0]["totalquantityinpack"]) + Convert.ToDecimal(dr["totalquantityinpack"]);
                        rows[0]["totalCost"] = Convert.ToDecimal(rows[0]["totalCost"]) + Convert.ToDecimal(dr["totalCost"]);
                    }
                    else
                    {
                        DT.ImportRow(dr);
                    }
                }
            }
            //   DTProductneed = null;

        }

        public string fnStoredProc(DataTable dtDetails, int ForecastId)
        {
            string strMsg = "";
            try
            {
                if (con.State == ConnectionState.Closed) con.Open();//Function for opening connection
                SqlCommand cmdProc = new SqlCommand("spGetReportData", con);
                cmdProc.CommandType = CommandType.StoredProcedure;
                cmdProc.Parameters.AddWithValue("@ForecastId", ForecastId);
                cmdProc.Parameters.AddWithValue("@Details", dtDetails);
                cmdProc.ExecuteNonQuery();
                strMsg = "Saved successfully.";
            }
            catch (SqlException e)
            {
                //strMsg = "Data not saved successfully.";
                strMsg = e.Message.ToString();
            }
            finally
            {
                con.Close();//Function for closing connection

            }
            return strMsg;
        }
    }
}
