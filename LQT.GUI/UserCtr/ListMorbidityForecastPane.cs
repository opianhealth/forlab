﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Quantification;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;
using System.Data;
using LQT.GUI.Tools;
using LQT.Core;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.IO;
using System.Text.RegularExpressions;
namespace LQT.GUI.UserCtr
{
    public partial class ListMorbidityForecastPane : BaseUserControl
    {
        private int _selectedForcastId = 0;
        public static string Mode = "";

        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        SqlCommand cmdForecast = null;
        private string metho1 = "";
        private int ProgramID;
     //   string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
       // string sqlConnection = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
      //  SqlConnection connection = new SqlConnection(String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName));
        public ListMorbidityForecastPane(string metho,string ProgramId)
        {
            InitializeComponent();
           
         //   BindForecasts();
            this.metho1=metho;
            this.ProgramID = Convert.ToInt32(ProgramId);
            BindForecasts1();
           // this.BindForecasts1();
        }
        public void  BindForecasts1()
        {
            string sDate = "", eDate = "";
             listView1.BeginUpdate();
            listView1.Items.Clear();

            foreach (ForecastInfo r in DataRepository.GetForecastInfoByMethodologyProgramId(metho1,ProgramID))
            {
                sDate = r.StartDate.ToString();
                eDate = r.ForecastDate.ToString();
                DateTime _startDate = DateTime.Parse(sDate);
                DateTime _endDate = DateTime.Parse(eDate);
                ListViewItem listitem = new ListViewItem(r.Id.ToString());
                listitem.SubItems.Add(r.ForecastNo);
                listitem.SubItems.Add(_startDate.ToString("dd-MMM-yyyy"));
                listitem.SubItems.Add(_endDate.ToString("dd-MMM-yyyy"));
                listitem.SubItems.Add(r.Period.ToString());
                listView1.Items.Add(listitem);
            }

            listView1.EndUpdate();
        
        }
        public override string GetControlTitle
        {
            get
            {
                return "Demographic Forecasting Methodology.";
            }
        }

        public override void ReloadUserCtrContents()
        {
            BindForecasts1();
        }
       
        public void BindForecasts()
        {
            string sDate = "", eDate = "";
            listView1.BeginUpdate();
            listView1.Items.Clear();
            IList<MorbidityForecast> result = DataRepository.GetAllMorbidityForecast();

            //SqlConnection pSqlConnection = ConnectionManager.GeneralSqlConnection;
            //using (connection = new SqlConnection(con))
            //{
                string _query = "SELECT [ForecastID],[ForecastNo],[Methodology],cast([StartDate] as date) as [StartDate],[Period],cast([ForecastDate] as date) as [ForecastDate],[ForecastType] FROM [ForecastInfo] Where Methodology='MORBIDITY'";
                using (daForecast = new SqlDataAdapter("SELECT [ForecastID],[ForecastNo],[Methodology],cast([StartDate] as date) as [StartDate],[Period],cast([ForecastDate] as date) as [ForecastDate],[ForecastType] FROM [ForecastInfo] Where Methodology='MORBIDITY'", connection))
                {
                    //SqlDataAdapter ada = new SqlDataAdapter("select * from ForecastInfo Where Methodology='MORBIDITY'", connection);
                    DataTable dt = new DataTable();
                    daForecast.Fill(dt);

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        sDate = dr["StartDate"].ToString();
                        eDate = dr["ForecastDate"].ToString();
                        DateTime _startDate = DateTime.Parse(sDate);
                        DateTime _endDate = DateTime.Parse(eDate);


                        // string nb = DateTime.ParseExact(de, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString();
                        ListViewItem listitem = new ListViewItem(dr["ForecastID"].ToString());
                        listitem.SubItems.Add(dr["ForecastNo"].ToString());
                        listitem.SubItems.Add(_startDate.ToString("dd-MMM-yyyy"));
                        listitem.SubItems.Add(_endDate.ToString("dd-MMM-yyyy"));
                        listitem.SubItems.Add(dr["Period"].ToString());

                        //if (Convert.ToInt32(dr["ForecastNo"].ToString()) == _selectedForcastId)
                        //{
                        //    listitem.Selected = true;
                        //}

                        listView1.Items.Add(listitem);
                        listView1.Refresh();
                        // System.Threading.Thread.Sleep(100);
                        //Thread.Sleep(1000);

                    }
                    //listView1.Invalidate();
                    //listView1.Update();
                    //listView1.Refresh();
                }
                connection.Close();

          ///  }
            listView1.EndUpdate();

        }
        public class Employee
        {
            public string ForecastID;
            public string ForecastNo;
            public string StartDate;
            public string ForecastDate;
            public string Period;
        }
        //public static Vanara.Interop.KnownShellItemPropertyKeys.Task Delay(int milliseconds)
        //{
        //    var tcs = new TaskCompletionSource<bool>();
        //    var timer = new System.Threading.Timer(o => tcs.SetResult(false));
        //    timer.Change(milliseconds, -1);
        //    return tcs;
        //}
        private MorbidityForecast GetSelectedForecast()
        {
            return DataRepository.GetMorbidityForecastById(_selectedForcastId);

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {


            //frmMorbidityForecastDefination frm = new frmMorbidityForecastDefination(MdiParentForm, _selectedForcastId, "E", this.Top, this.Width, this.Height, this.Left, this.Right);

            //frm.ShowDialog();
            FrmDyanmicMorbidity frm = new FrmDyanmicMorbidity(MdiParentForm, GetSelectedMorbidity());
            frm.ShowDialog();
        }
        private ForecastInfo GetSelectedMorbidity()
        {
            return DataRepository.GetForecastInfoById(_selectedForcastId);
        }


        private void lbtAddnew_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ForecastInfo finfo = new ForecastInfo();
            finfo.ProgramId = ProgramID;
            finfo.Methodology = "MORBIDITY";
         //   frmMorbidityForecastDefination frm = new frmMorbidityForecastDefination(MdiParentForm, _selectedForcastId, "A", this.Top, this.Width, this.Height, this.Left, this.Right);
            FrmDyanmicMorbidity frm = new FrmDyanmicMorbidity(MdiParentForm,finfo);
            ListMorbidityForecastPane gh = new ListMorbidityForecastPane("MORBIDITY",Convert.ToString(ProgramID));
            int top = gh.Top;
            int left = gh.Left;
            frm.ShowInTaskbar = false;
            frm.ShowDialog(this);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                int id =Convert.ToInt32(listView1.SelectedItems[0].Text);

                if (id != _selectedForcastId)
                {
                    _selectedForcastId = id;
                }
            }
            SelectedItemChanged(listView1);
        }


        public override void EditSelectedItem()
        {
            // frm.ShowDialog();
            FrmDyanmicMorbidity frm = new FrmDyanmicMorbidity(MdiParentForm, GetSelectedMorbidity());
            frm.ShowDialog();
        }

        public override bool DeleteSelectedItem()
        {
            string _sql = "";
            if (MessageBox.Show("Are you sure you want to delete this Forecast?", "Delete Forecasting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    //DataRepository.DeleteMorbidityForecast(GetSelectedForecast());


                    //string _sql = string.Format(@"Delete From ForecastInfo Where ForecastID={0}",_selectedForcastId);
                    //using (connection = new SqlConnection(con))
                    //{
                    //    connection.Open();
                    //    using (cmdForecast = new SqlCommand(_sql, connection))
                    //    {
                    //        cmdForecast.ExecuteNonQuery();
                    //        MdiParentForm.ShowStatusBarInfo("The Forecast was deleted successfully.");
                    //        _selectedForcastId = 0;
                    //        BindForecasts();
                    //    }
                    //}
                    List<string> sqlCommandList = new List<string>();
                    sqlCommandList.Add("Delete from PatientNumberDetail where HeaderID in (Select ID from PatientNumberHeader Where ForecastinfoID=" + _selectedForcastId + ")");
                    sqlCommandList.Add("Delete from PatientNumberHeader Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from PatientGroup Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from TestingProtocol Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from PatientAssumption Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from TestingAssumption Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from ForecastCategoryInfo Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from ForecastSiteInfo Where ForecastinfoID=" + _selectedForcastId + "");
                    sqlCommandList.Add("Delete from ForecastInfo Where ForecastID=" + _selectedForcastId + "");
                    //using (connection = new SqlConnection(con))
                    //{
                  connection.Open();
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            using (SqlCommand command = new SqlCommand("", connection, trans))
                            {
                                command.CommandType = System.Data.CommandType.Text;

                                foreach (var commandString in sqlCommandList)
                                {
                                    command.CommandText = commandString;
                                    command.ExecuteNonQuery();
                                }
                            }

                            trans.Commit();
                        }

                       MdiParentForm.ShowStatusBarInfo("The Forecast was deleted successfully.");
                        _selectedForcastId = 0;
                        BindForecasts1();
                    //}
                 connection.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    connection.Close();
                    MessageBox.Show("Unable to delete record", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
            }

            return false;
        }




        public Form MdiParent { get; set; }
    }
}
