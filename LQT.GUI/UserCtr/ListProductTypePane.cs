﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Asset;
using LQT.Core.UserExceptions;

namespace LQT.GUI.UserCtr
{
    public partial class ListProductTypePane : BaseUserControl
    {
        private int _selectedTypeId = 0;
       

        public ListProductTypePane()
        {
            InitializeComponent();
            PopProductType();
        }

        public override string GetControlTitle
        {
            get
            {
                return "Product Type";
            }
        }

        public override void ReloadUserCtrContents()
        {
            PopProductType();
        }

        private void PopProductType()
        {
            listView1.BeginUpdate();
            listView1.Items.Clear();

            foreach (ProductType r in DataRepository.GetAllProductType())
            {
                ListViewItem li = new ListViewItem(r.TypeName) { Tag = r.Id };
               // li.SubItems.Add(r.UseInDemography.ToString());
                li.SubItems.Add(r.Description);

                if (r.Id == _selectedTypeId)
                {
                    li.Selected = true;
                }
                listView1.Items.Add(li);
            }

            listView1.EndUpdate();

           
        }

       

        private ProductType GetSelectedProductType()
        {
            return DataRepository.GetProductTypeById(_selectedTypeId);
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            EditSelectedItem();
        }

        private void listView1_Resize(object sender, EventArgs e)
        {
            listView1.Columns[1].Width = listView1.Width - 307;
        }

        private void lbtAddnew_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ProductTypeForm frm = new ProductTypeForm(new ProductType(), MdiParentForm);
            frm.ShowDialog();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                int id = (int)listView1.SelectedItems[0].Tag;
                if (id != _selectedTypeId)
                {
                    _selectedTypeId = id;
                   
                }
            }
            SelectedItemChanged(listView1);
        }

        public override void EditSelectedItem()
        {
            ProductTypeForm frm = new ProductTypeForm(GetSelectedProductType(), MdiParentForm);
            frm.ShowDialog();
        }

        public override bool DeleteSelectedItem()
        {
            if (MessageBox.Show("Are you sure you want to delete this Product Type?", "Delete Product type", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    DataRepository.DeleteProductType(GetSelectedProductType());
                    MdiParentForm.ShowStatusBarInfo("Product Type was deleted successfully.");
                    _selectedTypeId = 0;
                    PopProductType();
                    MdiParentForm.BuildNavigationMenu();
                    return true;
                }
                catch (Exception ex)
                {
                    //DataRepository.CloseSession();
                    new FrmShowError(new ExceptionStatus() { ex=ex, message="Sorry, you could not delete Product Type."}).ShowDialog();
                }
                finally
                {
                    DataRepository.CloseSession();
                }
            }

            return false;
        }

        private void listView1_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            ListView lv = (ListView)sender;
            int widthDifference = Math.Abs(lv.Columns[e.ColumnIndex].Width - e.NewWidth);
            if (widthDifference > 2)
            {
                int maxWidth = 0;
                Graphics g = lv.CreateGraphics();
                for (int i = 0; i < lv.Items.Count; i++)
                {
                    string text = string.Empty;
                    if (e.ColumnIndex == 0)
                    {
                        text = lv.Items[i].Text;
                    }
                    else
                    {
                        text = lv.Items[i].SubItems[e.ColumnIndex - 1].Text;
                    }
                    SizeF sizeF = g.MeasureString(text, lv.Font);
                    if (maxWidth < (int)(sizeF.Width + 0.5))
                        maxWidth = (int)(sizeF.Width + 0.5);
                }
                g.Dispose();
                e.NewWidth = maxWidth;
            }
        }
    }
}
