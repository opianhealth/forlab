﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Quantification;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;

using LQT.GUI.Tools;
using LQT.Core;
using System.Windows.Forms.DataVisualization.Charting;
namespace LQT.GUI.UserCtr
{
    public partial class ShowCharts : BaseUserControl
    {

           SqlConnection con = new SqlConnection("Password=dataman@123;User ID=sa;Initial Catalog=ForecastInfo;Data Source=WEB_SERVER");
            DataSet ds = new DataSet();
            int ID = 89;

              public ShowCharts()
        {
            InitializeComponent();
          
        }

        public Form MdiParent { get; set; }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ShowCharts_Load(object sender, EventArgs e)
        {
            fillChartPatient();
          //  fillChartTectNoByTest();
          //  fillChartTest();
           fillChartByProductPrice();
        }

        private void fillChartPatient()
        {
           
            Chart1.Width=1200;
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter("Select  columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
            adapt.Fill(ds);
            Chart1.DataSource = ds;
            //set the member of the chart data source used to data bind to the X-values of the series  
            Chart1.Series["Patient Number"].XValueMember = "columnname";
            //set the member columns of the chart data source used to data bind to the X-values of the series  
            Chart1.Series["Patient Number"].YValueMembers = "serial";
         //   Chart1.Titles.Add("Patient Number over forecast");
            Chart1.Series["Patient Number"].IsValueShownAsLabel = true;
            Chart1.ChartAreas[0].AxisX.Interval = 1;
            Chart1.Series["Patient Number"]["PixelPointWidth"] = "25";
           // Chart1.ChartAreas[0].AxisX.LineColor = Color.Black;
            Chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            Chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            con.Close();

        }


//        private void fillChartTectNoByTest()
//        {
           
//            if(con.State== ConnectionState.Closed)
//            {
//            con.Open();
//            }

//            DataTable period = new System.Data.DataTable();
//            DataTable months = new System.Data.DataTable();
//            DataTable Duration = new System.Data.DataTable();
//            DataTable Percentage = new System.Data.DataTable();
//            DataTable TestingProtocal = new System.Data.DataTable();
//            DataTable Calculate = new System.Data.DataTable();
//            DataTable TestName = new System.Data.DataTable();
//            DataTable TestArea = new System.Data.DataTable();
//            DataTable ratiowise = new System.Data.DataTable();

//            SqlDataAdapter adapt0 = new SqlDataAdapter("Select period,forecastno from forecastinfo where forecastid='" + ID + "'", con);
//            adapt0.Fill(period);

//            LblForecastNo.Visible = true;
//            LblForecastNo.Text = period.Rows[0][1].ToString();

//            SqlDataAdapter adapt = new SqlDataAdapter("Select columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt.Fill(months);
//            int yrs = 0;
//            if (period.Rows[0][0].ToString() == "Monthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 1) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Bimonthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 2) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Quarterly")
//            {
//                yrs = ((months.Rows.Count - 1) * 4) / 12;
//            }
//            //if (period.Rows[0][0].ToString() == "Monthly")
//            //{ }

            
//            months.Columns.Add("NewPatient");

//            SqlDataAdapter adapt1 = new SqlDataAdapter("Select count(columnname) from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt1.Fill(Duration);

//           // int yrs = Convert.ToInt32(Duration.Rows[0][0].ToString())/12;

//            for (int i = 0; i < Convert.ToInt32(Duration.Rows[0][0])-1; i++)
//            {
//                months.Rows[i + 1][2] = (Convert.ToInt32(months.Rows[i + 1][1]) - Convert.ToInt32(months.Rows[i][1])).ToString();
               
//            }

//            SqlDataAdapter adapt2 = new SqlDataAdapter("Select id,patientgroupname,patientpercentage from patientgroup where forecastinfoid='" + ID + "' order by id", con);
//            adapt2.Fill(Percentage);

//            int newP = Convert.ToInt32(months.Rows[1][2]);
//            int OldP = Convert.ToInt32(months.Rows[0][1]);
//            Percentage.Columns.Add("PercentageCal");
//            Percentage.Columns.Add("PercentageCalOld");
//            for (int i1 = 0; i1 < Percentage.Rows.Count ; i1++)
//            {
//                Percentage.Rows[i1][3] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * newP) / 100).ToString();
//                Percentage.Rows[i1][4] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * OldP) / 100).ToString();
//            }

//            SqlDataAdapter adapt3 = new SqlDataAdapter(@"select TestID, PatientGroupid, PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],
//                [Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] from TestingProtocol 
//where ForecastinfoID="+ID+"  group by testid,PatientGroupID,PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],[Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] order by TestID", con);
//            adapt3.Fill(TestingProtocal);
//            TestingProtocal.Columns.Add("Percentage1"); TestingProtocal.Columns.Add("Percentageold");
//            for (int i2 = 0; i2 < Percentage.Rows.Count; i2++)
//            {
//                for (int i3 = 0; i3 < TestingProtocal.Rows.Count; i3++)
//                {
//                    if (Convert.ToInt32(Percentage.Rows[i2][0]) == Convert.ToInt32(TestingProtocal.Rows[i3][1]))
//                    {

//                        TestingProtocal.Rows[i3][19] = (Convert.ToDecimal(Convert.ToDecimal(Percentage.Rows[i2][3]) * Convert.ToDecimal(TestingProtocal.Rows[i3][2]))/100).ToString();
//                        TestingProtocal.Rows[i3][20] = Convert.ToDecimal(Percentage.Rows[i2][4]).ToString();
             
//                    }
                
//                }
//            }

//            TestingProtocal.Columns.Add("TestsNo");
//            for (int i4 = 0; i4 < TestingProtocal.Rows.Count; i4++)
//            {
//                TestingProtocal.Rows[i4][21] = Convert.ToInt32(TestingProtocal.Rows[i4][3]) + Convert.ToInt32(TestingProtocal.Rows[i4][4]) + Convert.ToInt32(TestingProtocal.Rows[i4][5])
//                    + Convert.ToInt32(TestingProtocal.Rows[i4][6]) + Convert.ToInt32(TestingProtocal.Rows[i4][7]) + Convert.ToInt32(TestingProtocal.Rows[i4][8]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][9]) + Convert.ToInt32(TestingProtocal.Rows[i4][10]) + Convert.ToInt32(TestingProtocal.Rows[i4][11]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][12]) + Convert.ToInt32(TestingProtocal.Rows[i4][13]) + Convert.ToInt32(TestingProtocal.Rows[i4][14]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][15]) + yrs * (Convert.ToInt32(TestingProtocal.Rows[i4][16]) + Convert.ToInt32(TestingProtocal.Rows[i4][17]) + Convert.ToInt32(TestingProtocal.Rows[i4][18]));  
//            }

//            TestingProtocal.Columns.Add("Total");
//            TestingProtocal.Columns.Add("TotalOld");
//            for (int i5 = 0; i5 < TestingProtocal.Rows.Count; i5++)
//            {
//                TestingProtocal.Rows[i5][22] = (Convert.ToDecimal(TestingProtocal.Rows[i5][19]) * Convert.ToDecimal(TestingProtocal.Rows[i5][21])).ToString();
//                TestingProtocal.Rows[i5][23] = (yrs * (Convert.ToDecimal(TestingProtocal.Rows[i5][20])) * (Convert.ToDecimal(TestingProtocal.Rows[i5][16]))).ToString();
//            }

//            SqlDataAdapter adapt4 = new SqlDataAdapter("Select distinct(testid) from testingprotocol where forecastinfoid='"+ID+"'", con);
//            adapt4.Fill(Calculate);

//            Calculate.Columns.Add("totaltestnew"); Calculate.Columns.Add("totaltestOld"); Calculate.Columns.Add("totaltests");
//            for (int i6 = 0; i6 < Calculate.Rows.Count; i6++)
//            {
//                decimal i8 = 0; decimal i9 = 0;
//                for (int i7 = 0; i7 < TestingProtocal.Rows.Count; i7++)
//                {
//                    if (Convert.ToInt32(Calculate.Rows[i6][0]) == Convert.ToInt32(TestingProtocal.Rows[i7][0]))
//                    {
                      
                        
//                        i8 = i8 + Convert.ToDecimal(TestingProtocal.Rows[i7][22]);
//                        i9 = i9 + Convert.ToDecimal(TestingProtocal.Rows[i7][23]);
//                    }
//                    Calculate.Rows[i6][1] = i8.ToString(); Calculate.Rows[i6][2] = i9.ToString();
//                }
//                decimal tot = Convert.ToDecimal(Calculate.Rows[i6][1]) + Convert.ToDecimal(Calculate.Rows[i6][2]);
//                Calculate.Rows[i6][3] = Math.Round(tot,0).ToString();
//            }
//            Calculate.Columns.Add("TestName"); Calculate.Columns.Add("TestAreas");
  
//            for (int k = 0; k < Calculate.Rows.Count; k++)
//            {
//                SqlDataAdapter adapt5 = new SqlDataAdapter("Select testid, testname,testingareaid from test where testid= '" + Calculate.Rows[k][0] + "'", con);
                
//                adapt5.Fill(TestName);
//               Calculate.Rows[k][4] = TestName.Rows[k][1]; Calculate.Rows[k][5] = TestName.Rows[k][2];
            
//            }

//         //   MessageBox.Show(Calculate.Rows[0][3].ToString()); MessageBox.Show(Calculate.Rows[1][3].ToString());
//            Chart3.DataSource = Calculate;
//            //set the member of the chart data source used to data bind to the X-values of the series  
//            Chart3.Series["Forecasted Test"].XValueMember = "TestName";
//            //set the member columns of the chart data source used to data bind to the X-values of the series  
//            Chart3.Series["Forecasted Test"].YValueMembers = "totaltests";
//           // chart2.Series["Forecasted Test"].ChartType = SeriesChartType.Pie;
//            Chart3.Series["Forecasted Test"].IsValueShownAsLabel = true;
//           // Chart3.Titles.Add("Ratio of forcasted test by testing area");
//            Chart3.Series["Forecasted Test"]["PixelPointWidth"] = "25";
//            Chart3.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
//            Chart3.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
//            con.Close();

//        }





//        private void fillChartTest()
//        {

//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }

//            DataTable period = new System.Data.DataTable();
//            DataTable months = new System.Data.DataTable();
//            DataTable Duration = new System.Data.DataTable();
//            DataTable Percentage = new System.Data.DataTable();
//            DataTable TestingProtocal = new System.Data.DataTable();
//            DataTable Calculate = new System.Data.DataTable();
//            DataTable TestName = new System.Data.DataTable();
//            DataTable TestArea = new System.Data.DataTable();
//            DataTable ratiowise = new System.Data.DataTable();

//            SqlDataAdapter adapt0 = new SqlDataAdapter("Select period from forecastinfo where forecastid='" + ID + "'", con);
//            adapt0.Fill(period);



//            SqlDataAdapter adapt = new SqlDataAdapter("Select columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt.Fill(months);
//            int yrs = 0;
//            if (period.Rows[0][0].ToString() == "Monthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 1) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Bimonthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 2) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Quarterly")
//            {
//                yrs = ((months.Rows.Count - 1) * 4) / 12;
//            }
//            //if (period.Rows[0][0].ToString() == "Monthly")
//            //{ }


//            months.Columns.Add("NewPatient");

//            SqlDataAdapter adapt1 = new SqlDataAdapter("Select count(columnname) from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt1.Fill(Duration);

//            // int yrs = Convert.ToInt32(Duration.Rows[0][0].ToString())/12;

//            for (int i = 0; i < Convert.ToInt32(Duration.Rows[0][0]) - 1; i++)
//            {
//                months.Rows[i + 1][2] = (Convert.ToInt32(months.Rows[i + 1][1]) - Convert.ToInt32(months.Rows[i][1])).ToString();

//            }

//            SqlDataAdapter adapt2 = new SqlDataAdapter("Select id,patientgroupname,patientpercentage from patientgroup where forecastinfoid='" + ID + "' order by id", con);
//            adapt2.Fill(Percentage);

//            int newP = Convert.ToInt32(months.Rows[1][2]);
//            int OldP = Convert.ToInt32(months.Rows[0][1]);
//            Percentage.Columns.Add("PercentageCal");
//            Percentage.Columns.Add("PercentageCalOld");
//            for (int i1 = 0; i1 < Percentage.Rows.Count; i1++)
//            {
//                Percentage.Rows[i1][3] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * newP) / 100).ToString();
//                Percentage.Rows[i1][4] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * OldP) / 100).ToString();
//            }

//            SqlDataAdapter adapt3 = new SqlDataAdapter(@"select TestID, PatientGroupid, PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],
//                [Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] from TestingProtocol 
//where ForecastinfoID=" + ID + "  group by testid,PatientGroupID,PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],[Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] order by TestID", con);
//            adapt3.Fill(TestingProtocal);
//            TestingProtocal.Columns.Add("Percentage1"); TestingProtocal.Columns.Add("Percentageold");
//            for (int i2 = 0; i2 < Percentage.Rows.Count; i2++)
//            {
//                for (int i3 = 0; i3 < TestingProtocal.Rows.Count; i3++)
//                {
//                    if (Convert.ToInt32(Percentage.Rows[i2][0]) == Convert.ToInt32(TestingProtocal.Rows[i3][1]))
//                    {

//                        TestingProtocal.Rows[i3][19] = (Convert.ToDecimal(Convert.ToDecimal(Percentage.Rows[i2][3]) * Convert.ToDecimal(TestingProtocal.Rows[i3][2])) / 100).ToString();
//                        TestingProtocal.Rows[i3][20] = Convert.ToDecimal(Percentage.Rows[i2][4]).ToString();

//                    }

//                }
//            }

//            TestingProtocal.Columns.Add("TestsNo");
//            for (int i4 = 0; i4 < TestingProtocal.Rows.Count; i4++)
//            {
//                TestingProtocal.Rows[i4][21] = Convert.ToInt32(TestingProtocal.Rows[i4][3]) + Convert.ToInt32(TestingProtocal.Rows[i4][4]) + Convert.ToInt32(TestingProtocal.Rows[i4][5])
//                    + Convert.ToInt32(TestingProtocal.Rows[i4][6]) + Convert.ToInt32(TestingProtocal.Rows[i4][7]) + Convert.ToInt32(TestingProtocal.Rows[i4][8]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][9]) + Convert.ToInt32(TestingProtocal.Rows[i4][10]) + Convert.ToInt32(TestingProtocal.Rows[i4][11]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][12]) + Convert.ToInt32(TestingProtocal.Rows[i4][13]) + Convert.ToInt32(TestingProtocal.Rows[i4][14]) +
//                    Convert.ToInt32(TestingProtocal.Rows[i4][15]) + yrs * (Convert.ToInt32(TestingProtocal.Rows[i4][16]) + Convert.ToInt32(TestingProtocal.Rows[i4][17]) + Convert.ToInt32(TestingProtocal.Rows[i4][18]));
//            }

//            TestingProtocal.Columns.Add("Total");
//            TestingProtocal.Columns.Add("TotalOld");
//            for (int i5 = 0; i5 < TestingProtocal.Rows.Count; i5++)
//            {
//                TestingProtocal.Rows[i5][22] = (Convert.ToDecimal(TestingProtocal.Rows[i5][19]) * Convert.ToDecimal(TestingProtocal.Rows[i5][21])).ToString();
//                TestingProtocal.Rows[i5][23] = (yrs * (Convert.ToDecimal(TestingProtocal.Rows[i5][20])) * (Convert.ToDecimal(TestingProtocal.Rows[i5][16]))).ToString();
//            }

//            SqlDataAdapter adapt4 = new SqlDataAdapter("Select distinct(testid) from testingprotocol where forecastinfoid='" + ID + "'", con);
//            adapt4.Fill(Calculate);

//            Calculate.Columns.Add("totaltestnew"); Calculate.Columns.Add("totaltestOld"); Calculate.Columns.Add("totaltests");
//            for (int i6 = 0; i6 < Calculate.Rows.Count; i6++)
//            {
//                decimal i8 = 0; decimal i9 = 0;
//                for (int i7 = 0; i7 < TestingProtocal.Rows.Count; i7++)
//                {
//                    if (Convert.ToInt32(Calculate.Rows[i6][0]) == Convert.ToInt32(TestingProtocal.Rows[i7][0]))
//                    {


//                        i8 = i8 + Convert.ToDecimal(TestingProtocal.Rows[i7][22]);
//                        i9 = i9 + Convert.ToDecimal(TestingProtocal.Rows[i7][23]);
//                    }
//                    Calculate.Rows[i6][1] = i8.ToString(); Calculate.Rows[i6][2] = i9.ToString();
//                }
//                decimal tot = Convert.ToDecimal(Calculate.Rows[i6][1]) + Convert.ToDecimal(Calculate.Rows[i6][2]);
//                Calculate.Rows[i6][3] = Math.Round(tot, 0).ToString();
//            }
//            Calculate.Columns.Add("TestName"); Calculate.Columns.Add("TestAreas");

//            for (int k = 0; k < Calculate.Rows.Count; k++)
//            {
//                SqlDataAdapter adapt5 = new SqlDataAdapter("Select testid, testname,testingareaid from test where testid= '" + Calculate.Rows[k][0] + "'", con);

//                adapt5.Fill(TestName);
//                Calculate.Rows[k][4] = TestName.Rows[k][1]; Calculate.Rows[k][5] = TestName.Rows[k][2];

//            }

//            SqlDataAdapter adapt6 = new SqlDataAdapter("Select testingareaid, areaname from testingarea", con);

//            adapt6.Fill(TestArea);
//            TestArea.Columns.Add("Total");

//            for (int j = 0; j < TestArea.Rows.Count; j++)
//            {
//                decimal i10 = 0; decimal i11 = 0;

//                for (int j1 = 0; j1 < Calculate.Rows.Count; j1++)
//                {
//                    if (Convert.ToInt32(TestArea.Rows[j][0]) == Convert.ToInt32(Calculate.Rows[j1][5]))
//                    {


//                        i10 = i10 + Convert.ToInt32(Calculate.Rows[j1][3]);
//                        // i9 = i9 + Convert.ToDecimal(TestingProtocal.Rows[i7][23]);
//                    }
//                    TestArea.Rows[j][2] = i10;
//                }
//            }
//            ratiowise.Columns.Add("TestArea"); ratiowise.Columns.Add("Test");

//            for (int j2 = 0; j2 < TestArea.Rows.Count; j2++)
//            {
//                if (Convert.ToInt32(TestArea.Rows[j2][2]) != 0)
//                {
//                    ratiowise.Rows.Add();
//                    ratiowise.Rows[j2][0] = TestArea.Rows[j2][1];
//                    ratiowise.Rows[j2][1] = TestArea.Rows[j2][2];
//                }

//            }
//            DataTable dtMarks1 = ratiowise.Clone();
//            dtMarks1.Columns["Test"].DataType = Type.GetType("System.Int32");

//            foreach (DataRow dr in ratiowise.Rows)
//            {
//                dtMarks1.ImportRow(dr);
//            }
//            dtMarks1.AcceptChanges();


//            DataView dv = dtMarks1.DefaultView;
//            dv.Sort = "Test Asc";

//            DataTable finalRatio = ratiowise.Clone();
//            // finalRatio.Columns.Add("ratio");
//            if (ratiowise.Rows.Count != 1)
//            {
//                for (int l2 = 0; l2 < ratiowise.Rows.Count; l2++)
//                {
//                    finalRatio.Rows.Add();
//                    finalRatio.Rows[l2][0] = ratiowise.Rows[l2][0];
//                    finalRatio.Rows[l2][1] = Convert.ToInt32(ratiowise.Rows[l2][1]) / Convert.ToInt32(dtMarks1.Rows[0][1]);

//                }
//                chart2.DataSource = finalRatio;
//            }
//            else
//            {
//                chart2.DataSource = ratiowise;

//            }


//            //set the member of the chart data source used to data bind to the X-values of the series  
//            chart2.Series["Forecasted Test Ratio"].XValueMember = "TestArea";
//            //set the member columns of the chart data source used to data bind to the X-values of the series  
//            chart2.Series["Forecasted Test Ratio"].YValueMembers = "Test";
//            chart2.Series["Forecasted Test Ratio"].ChartType = SeriesChartType.Pie;
//            chart2.Series["Forecasted Test Ratio"].IsValueShownAsLabel = true;
//            //chart2.Series["Forecasted Test Ratio"]["PixelPointWidth"] = "25";
//            //chart2.Titles.Add("Ratio of forcasted test by testing area");
//            con.Close();

//        }

        private void fillChartByProductPrice()
        {
            Chart1.Width = 2500;
            DataTable DTtestno = new DataTable();
            DataTable DTsiteinstrument = new DataTable();
            DataTable DTproductusagerate = new DataTable();
            DataTable DTproductControlusage = new DataTable();
            con.Open();


            /////testusage
            SqlDataAdapter adapt1 = new SqlDataAdapter(@"SELECT PU.InstrumentId As InstrumentId,ins.InstrumentName As InstrumentName,
            PU.ProductId as ProductId,mp.ProductName As ProductName,PU.Rate As Rate,PU.TestId As TestId,ins.TestingAreaID As AreaID,TA.AreaName as areaname
            ,Isnull(T.Quantity,0) As Quantity ,Isnull(T.Percentage,0) As Percentage,TN.testnumber,Cast(((PU.Rate*TN.testnumber)*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed
              FROM [ForecastInfo].[dbo].[ProductUsage] PU
              Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId
              Left join MasterProduct mp on mp.ProductID=PU.ProductId  
                Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID
            Left join  (select Max(T.testingArea) As testingArea,Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID from(
            SELECT SI.SiteID,TA.AreaName As testingArea ,Ins.InstrumentName As instrumentname,SI.Quantity As Quantity,SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID
              FROM [ForecastInfo].[dbo].[ForecastSiteInfo] FS 
              Left join SiteInstrument SI on SI.SiteID=FS.SiteID
                Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID
	             Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID 
	             where FS.ForecastinfoID=" + ID +") As T Group by TestingAreaID,InstrumentID) As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID 	 Left Join (SELECT Max(testname) as testname,TP.TestID,Max(TA.AreaName) As Areaname,Max(TA.TestingAreaID) As TestingAreaID,13000 as testnumber from Test TS Left join [TestingProtocol] TP on TP.TestID=TS.TestID Left Join TestingArea TA on TA.TestingAreaID=TS.TestingAreaID where TP.ForecastinfoID =" + ID +" group by TP.TestID) As TN on TN.TestID=PU.TestId   where PU.TestId in (Select TestID from [TestingProtocol] where ForecastinfoID =" + ID +")  and PU.IsForControl=0", con);


            adapt1.Fill(DTproductusagerate);
            
           /////Product control usasge
            



            chart4.DataSource = ds;
            //set the member of the chart data source used to data bind to the X-values of the series  
            chart4.Series["Product Cost"].XValueMember = "TypeName";
            //set the member columns of the chart data source used to data bind to the X-values of the series  
            chart4.Series["Product Cost"].YValueMembers = "Price";
            // chart2.Titles.Add("Ratio of forcasted test by testing area");
            // chart2.Series["Product Cost"].ChartType = SeriesChartType.Pie;
            chart4.Series["Product Cost"].IsValueShownAsLabel = true;
            //chart2.ChartAreas[0].AxisX.Interval = 1;
            con.Close();

        }

        private void Panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        } 
    }
}
