﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core;
using System.Data.SqlClient;
using System.Data;
using LQT.GUI.UserCtr;
using LQT.Core.Util;
using LQT.Core.UserExceptions;
using LQT.GUI.Quantification;
using System.Text.RegularExpressions;
using LQT.Core.Domain;
namespace LQT.GUI.UserCtr
{
    public partial class frmMorbidityForecastDefination : Form
    {
        private Form _mdiparent;
        private int _selectedForcastId = 0;
        public string OldForcastId = "0";
        public static string Mode = "";
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
        private ForecastInfo _mMforecastinfo;
        //string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        
        public frmMorbidityForecastDefination(Form mdiparent,int id,string mode,int _Top,int _Width,int _Height,int _Left,int _Right)
        {
            InitializeComponent();
            _selectedForcastId = id;
            _mdiparent = mdiparent;
            Mode = mode;
            this.fillPeriod();
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            txtForecastID.KeyPress+=new KeyPressEventHandler(txtForecastID_KeyPress);
            //dpStart.Format = DateTimePickerFormat.Custom;
            //dpStart.CustomFormat = "MMMM yyyy";
            //dpEnd.Format = DateTimePickerFormat.Custom;
            //dpEnd.CustomFormat = "MMMM yyyy";

            dpStart.Format = DateTimePickerFormat.Custom;
            dpStart.CustomFormat = "MMMM yyyy";
            dpStart.ShowUpDown = true;
            dpEnd.Format = DateTimePickerFormat.Custom;
            dpEnd.CustomFormat = "MMMM yyyy";
            dpEnd.ShowUpDown = true;
            this.Top =120;
           // this.Width = _Width;
           //this.Height = _Height;
           //panel1.Height = _Height;
           //panel1.Width = _Width;
            this.Left =250;
            //this.Right = _Right;
            if (Mode != "A")
            {
                this.editRecordDisplay1(_selectedForcastId);
            }
        }
        private void txtForecastID_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                if (e.KeyChar == (char)Keys.Back) e.Handled = false;
                else e.Handled = true;
            }
        }
        private void editRecordDisplay1(int ID)
        {


            _mMforecastinfo = DataRepository.GetForecastInfoById(ID);
            //string _sql = "";
            //_sql = string.Format(@"Select ForecastNo,Methodology,DataUsage,Status,StartDate,Period,ForecastDate,Method,SlowMovingPeriod,ForecastType,Extension from  ForecastInfo Where ForecastID={0}", ID);
            ////using (connection = new SqlConnection(con))
            ////{
            //using (cmdForecast = new SqlCommand(_sql, connection))
            //{
            //    connection.Open();
            //    SqlDataReader rdrForecast = cmdForecast.ExecuteReader();
            //    while (rdrForecast.Read())
            //    {
                    // get the results of each column
                   txtForecastID.Text = _mMforecastinfo.ForecastNo.ToString();
                      OldForcastId = _mMforecastinfo.ForecastNo.ToString();
                    dpStart.Text =_mMforecastinfo.StartDate.ToString();
                    dpEnd.Text = _mMforecastinfo.ForecastDate.ToString();
                    cbRPeriod.Text = _mMforecastinfo.Period.ToString();
                    if (_mMforecastinfo.ForecastType.Equals("S"))
                    {
                        rdSBSForecast.Checked = true;
                    }
                    else rbAForecast.Checked = true;
                    rdSBSForecast.Enabled = false;
                    rbAForecast.Enabled = false;

            //    }
            //    connection.Close();
            //}
        }
        private void editRecordDisplay(int ID)
        {
            string _sql = "";
                _sql = string.Format(@"Select ForecastNo,Methodology,DataUsage,Status,StartDate,Period,ForecastDate,Method,SlowMovingPeriod,ForecastType,Extension from  ForecastInfo Where ForecastID={0}", ID);
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                      connection.Open();
                        SqlDataReader rdrForecast = cmdForecast.ExecuteReader();
                        while (rdrForecast.Read())
                        {
                            // get the results of each column
                            txtForecastID.Text = (string)rdrForecast["ForecastNo"];
                            OldForcastId = (string)rdrForecast["ForecastNo"];
                            dpStart.Text = Convert.ToString(rdrForecast["StartDate"]);
                            dpEnd.Text = Convert.ToString(rdrForecast["ForecastDate"]);
                            cbRPeriod.Text = (string)rdrForecast["Period"];
                            if (rdrForecast["ForecastType"].Equals("S"))
                            {
                                rdSBSForecast.Checked = true;
                            }
                            else rbAForecast.Checked = true;
                            rdSBSForecast.Enabled = false;
                            rbAForecast.Enabled = false;

                    }
                        connection.Close();
               }
        }

        private void  fillPeriod()
        {
            string[] periods = Enum.GetNames(typeof(ForecastPeriodEnum));

            cbRPeriod.Items.Clear();
            for (int i = 0; i < periods.Length; i++)
            {
                cbRPeriod.Items.Add(periods[i]);
            }
        }
        private void addRecord()
        {
            string _sql = "Insert into ForecastInfo(ForecastNo,Methodology,DataUsage,Status,StartDate,Period,ForecastDate,Method,SlowMovingPeriod,ForecastType,Extension,LastUpdated) Values(@ForecastNo,@Methodology,@DataUsage,@Status,@StartDate,@Period,@ForecastDate,@Method,@SlowMovingPeriod,@ForecastType,@Extension,@LastUpdated);SELECT SCOPE_IDENTITY()";
           try
           {
               //using (connection = new SqlConnection(con))
               //{
                connection.Open();
                DateTime _startDate = DateTime.Parse(dpStart.Value.ToString());
                DateTime _endDate = DateTime.Parse(dpEnd.Value.ToString());
                int year = _endDate.Year;
                int day = _startDate.Day;
                int month = _endDate.Month;
                DateTime dtF = new DateTime(year, month, day); 
                   //SaveOrUpdateObject1();
                  
                   //string _sql1 = string.Format(@"Select Count(*) from ForecastInfo Where ForecastNo='{0}'", txtForecastID.Text);
                   //using (cmdForecast = new SqlCommand(_sql1, connection))
                   //{
                   //    int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                   //    if (cnt > 0) throw new LQTUserException("ForecastID must not be duplicate.");
                   //}
                   if (!validation(connection) == false)
                   {
                      
                       using (cmdForecast = new SqlCommand(_sql, connection))
                       {
                          
                           cmdForecast.Parameters.Add("@ForecastNo", SqlDbType.NVarChar).Value = txtForecastID.Text;
                           cmdForecast.Parameters.Add("@Methodology", SqlDbType.NVarChar).Value = "MORBIDITY";
                           cmdForecast.Parameters.Add("@DataUsage", SqlDbType.NVarChar).Value = "DATA_USAGE1";
                           cmdForecast.Parameters.Add("@Status", SqlDbType.NVarChar).Value = "OPEN";
                           cmdForecast.Parameters.Add("@StartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(dpStart.Text);
                           cmdForecast.Parameters.Add("@Period", SqlDbType.NVarChar).Value = cbRPeriod.Text;
                           cmdForecast.Parameters.Add("@ForecastDate", SqlDbType.DateTime).Value = Convert.ToDateTime(dtF.ToString("MMMM yyyy"));
                           cmdForecast.Parameters.Add("@Method", SqlDbType.NVarChar).Value = "Linear";
                           cmdForecast.Parameters.Add("@SlowMovingPeriod", SqlDbType.NVarChar).Value = cbRPeriod.Text;
                           if (rdSBSForecast.Checked == true) cmdForecast.Parameters.Add("@ForecastType", SqlDbType.Char).Value = "S";
                           else cmdForecast.Parameters.Add("@ForecastType", SqlDbType.Char).Value = "C";
                           cmdForecast.Parameters.Add("@Extension", SqlDbType.Char).Value = 4;
                           cmdForecast.Parameters.Add("@LastUpdated", SqlDbType.Char).Value =DateTime.Now;
                           int ForecastID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                           _selectedForcastId = ForecastID;
                           ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Forecast was Saved successfully.", true);
                           //SaveOrUpdateObject();
                          //MessageBox.Show("Forecast was Saved successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                           //MessageBox.Show("Forecast was saved successfully.");
                        connection.Close();
                           if (rdSBSForecast.Checked == true)
                           {
                               this.Hide();
                               Frmsitebysiteforecast frm = new Frmsitebysiteforecast(_mdiparent, _selectedForcastId, "E", dpStart.Value.ToString("dd-MMM-yyyy"), dpEnd.Value.ToString("dd-MMM-yyyy"), cbRPeriod.Text);
                               frm.ShowDialog();

                               this.Close();
                           }
                           else
                           {
                               //DateTime _startDate = DateTime.Parse( dpStart.Value.ToString());
                               //DateTime _endDate = DateTime.Parse(dpEnd.Value.ToString());
                               //_endDate.ToString("dd-MMM-yyyy")

                               this.Hide();
                               AggregateForecast frm = new AggregateForecast(_mdiparent,_selectedForcastId, "E", dpStart.Value.ToString("dd-MMM-yyyy"), dpEnd.Value.ToString("dd-MMM-yyyy"), cbRPeriod.Text);
                               frm.ShowDialog();

                               this.Close();
                           }

                       }
                   }
              // }
           }
           catch (Exception ex)
           {
               connection.Close();
           }
        }
        private void editRecord(int ID)
        {

            DateTime _startDate = DateTime.Parse(dpStart.Value.ToString());
            DateTime _endDate = DateTime.Parse(dpEnd.Value.ToString());
            int year = _endDate.Year;
            int day = _startDate.Day;
            int month = _endDate.Month;
            DateTime dtF = new DateTime(year, month, day);

            string _sql = string.Format(@"Update ForecastInfo Set ForecastNo='{0}',StartDate='{1}',Period='{2}',ForecastDate='{3}' Where ForecastID={4}", txtForecastID.Text, Convert.ToDateTime(dpStart.Text), cbRPeriod.Text, Convert.ToDateTime(dtF.ToString("MMMM yyyy")), ID);
         
            //using (connection = new SqlConnection())
            //{
           connection.Open();
                //SaveOrUpdateObject1();
                if (!validation(connection) == false)
                {
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        cmdForecast.ExecuteNonQuery();
                        SaveOrUpdateObject();
                        ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Forecast was updated successfully.", true);
                       
                       // MessageBox.Show("Forecast was updated successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                       // MessageBox.Show("Forecast was updated successfully.");
                    }
                }
            //}
            connection.Close();
            if (rdSBSForecast.Checked == true)
            {
                this.Hide();
                Frmsitebysiteforecast frm = new Frmsitebysiteforecast(_mdiparent, _selectedForcastId, "E",dpStart.Value.ToString("dd-MMM-yyyy"), dpEnd.Value.ToString("dd-MMM-yyyy"),cbRPeriod.Text);
                frm.ShowDialog();

                this.Close();
            }
            else
            {
                //DateTime _startDate = DateTime.Parse( dpStart.Value.ToString());
                //DateTime _endDate = DateTime.Parse(dpEnd.Value.ToString());
                //_endDate.ToString("dd-MMM-yyyy")

                this.Hide();
                AggregateForecast frm = new AggregateForecast(_mdiparent,_selectedForcastId, "E", dpStart.Value.ToString("dd-MMM-yyyy"), dpEnd.Value.ToString("dd-MMM-yyyy"), cbRPeriod.Text);
                frm.ShowDialog();

                this.Close();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            _mMforecastinfo = DataRepository.GetForecastInfoById(_selectedForcastId);
            if (Mode == "A")
            {
                addRecord();
                //SaveOrUpdateObject();
            }
            else { 
                editRecord(_selectedForcastId);
                //SaveOrUpdateObject();
            }
            ListMorbidityForecastPane Lm = new ListMorbidityForecastPane(_mMforecastinfo.Methodology,Convert.ToString( _mMforecastinfo.ProgramId));
            
            Lm.BindForecasts();
            //if (rdSBSForecast.Checked == true)
            //{
            //    Frmsitebysiteforecast frm = new Frmsitebysiteforecast(_selectedForcastId, Mode);
            //    frm.ShowDialog();
            //    this.Close();
            //}
            //else
            //{
            //}
           
            
        }
        public void SaveOrUpdateObject1()
        {
            string message = "";
            //string _sql = string.Format(@"Select Count(*) from ForecastInfo Where ForecastNo='{0}'",txtForecastID.Text);
            //using (connection = new SqlConnection(con))
            //{
            //    //connection.Open();
            //    using (cmdForecast = new SqlCommand(_sql, connection))
            //    {
            //        int cnt =Convert.ToInt32(cmdForecast.ExecuteScalar());
            //        if (cnt > 0) throw new LQTUserException("ForecastID must not be duplicate.");
            //    }
            //    //connection.Close();
            //}

            if (txtForecastID.Text.Trim() == string.Empty)
                message = "ForecastID must not be empty.";
               // throw new LQTUserException("ForecastID must not be empty.");
          

            if (cbRPeriod.Text.Trim() == string.Empty)
                throw new LQTUserException("Period must not be empty.");
            if (rbAForecast.Checked == false && rdSBSForecast.Checked == false)
                throw new LQTUserException("Atleast one Forecast Type must be selected.");
            //return new LQTUserMessage(message);
        }
        public LQTUserMessage SaveOrUpdateObject()
        {
            return new LQTUserMessage("Forecast was saved or updated successfully.");
        }
        public bool  validation(SqlConnection con)
        {
            try
            {
                if (txtForecastID.Text.Trim() == string.Empty)
                {
                    //MessageBox.Show("ForecastID must not be empty.");
                    MessageBox.Show("ForecastID must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtForecastID.Focus();
                    return false;
                }
                if (!OldForcastId.Equals(txtForecastID.Text))
                {
                    //using (connection = new SqlConnection(con))
                    //{
                    //SaveOrUpdateObject1();

                    string _sql1 = string.Format(@"Select Count(*) from ForecastInfo Where LOWER((ForecastNo))='{0}'", txtForecastID.Text.ToLower().Trim());
                    using (cmdForecast = new SqlCommand(_sql1, con))
                    {
                        int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                        if (cnt > 0)
                        {
                            //MessageBox.Show("ForecastID must not be duplicate.");
                            MessageBox.Show("ForecastID must not be duplicate.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    //connection.Close();
                    //}
                }
                if (dpEnd.Value < dpStart.Value)
                {
                    //MessageBox.Show("Forecast End Date must be greater then Forecast Start Date.");
                    MessageBox.Show("Forecast End Date must be greater then Forecast Start Date.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                if (cbRPeriod.Text.Trim() == string.Empty)
                {
                    //MessageBox.Show("Period must not be empty.");
                    MessageBox.Show("Period must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cbRPeriod.Focus();
                    return false;
                }
                if (rbAForecast.Checked == false && rdSBSForecast.Checked == false)
                {
                    //MessageBox.Show("Atleast one Forecast Type must be selected.");
                    MessageBox.Show("Atleast one Forecast Type must be selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
              
             
            }
            catch (Exception ex)
            {
                //connection.Close();
            }
            return true;
        }

    }
}
