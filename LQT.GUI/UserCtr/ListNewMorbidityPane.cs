﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Quantification;
using LQT.Core.UserExceptions;

namespace LQT.GUI.UserCtr
{
    public partial class ListNewMorbidityPane : BaseUserControl
    {
        private int _selectedForcastId = 0;
        
        public ListNewMorbidityPane()
        {
            InitializeComponent();
            BindForecasts();
        }

        public override string GetControlTitle
        {
            get
            {
                return "Morbidity Forecasting Methodology - New (Beta).";
            }
        }

        public override void ReloadUserCtrContents()
        {
            BindForecasts();
        }

        private void BindForecasts()
        {
            listView1.BeginUpdate();
            listView1.Items.Clear();
            IList<NewMorbidityForecast> result = DataRepository.GetAllNewMorbidityForecast();
            foreach (NewMorbidityForecast  r in result)
            {
                ListViewItem li = new ListViewItem(r.Status) { Tag = r.Id };
                
                li.SubItems.Add(r.ForecastCode);

                li.SubItems.Add(r.ScopeOfTheForecast);
                li.SubItems.Add(r.ForecastDate.ToShortDateString());
                li.SubItems.Add(r.ReportingPeriod);
                li.SubItems.Add(r.Extension.ToString());
                li.SubItems.Add(r.ConductSitebySite.ToString());
                li.SubItems.Add(r.IncludeControl.ToString());
                li.SubItems.Add(r.LastUpdated.ToShortDateString());
               

                if (r.Id == _selectedForcastId)
                {
                    li.Selected = true;
                }

                listView1.Items.Add(li);
            }

            listView1.EndUpdate();

        }
               
        private NewMorbidityForecast GetSelectedForecast()
        {
            return DataRepository.GetNewMorbidityForecastById(_selectedForcastId);
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            EditSelectedItem();
        }


        private void lbtAddnew_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //NewMorbidityForecast finfo = new NewMorbidityForecast();
            //NewMorbidityForm frm = new NewMorbidityForm(finfo, MdiParentForm);
            //frm.ShowDialog();
            //FrmNewMorbidity finfo = new FrmNewMorbidity(MdiParentForm);
            FrmNewMorbidity frm = new FrmNewMorbidity(MdiParentForm);
            frm.ShowDialog();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                int id = (int)listView1.SelectedItems[0].Tag;

                if (id != _selectedForcastId)
                {
                    _selectedForcastId = id;
                }
            }
            SelectedItemChanged(listView1);
        }


        public override void EditSelectedItem()
        {
                NewMorbidityForm frm = new NewMorbidityForm(GetSelectedForecast(), MdiParentForm);
                frm.ShowDialog();
        }

        public override bool DeleteSelectedItem()
        {
            if (MessageBox.Show("Are you sure you want to delete this Forecast?", "Delete Forecasting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    DataRepository.DeleteNewMorbidityForecast(GetSelectedForecast());
                    MdiParentForm.ShowStatusBarInfo("The Forecast was deleted successfully.");
                    _selectedForcastId = 0;
                    BindForecasts();
                    return true;
                }
                catch (Exception ex)
                {
                    new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();
                }
            }

            return false;
        }

    }
}
