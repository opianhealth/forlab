﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.UserExceptions;
using LQT.Core.Util;

namespace LQT.GUI
{
    public partial class FrmSelectProduct : Form
    {
        public IList<int> SelectedProductIds;
        private IList<int> _selectedPro;
        private IList<MasterProduct> _products;
        private IList<MasterProduct> _selectedproducts;
        public FrmSelectProduct(IList<int> selectedpro, IList<MasterProduct> products)
        {
            _selectedPro = selectedpro;
            _products = products;

            InitializeComponent();
            SelectedProductIds = new List<int>();
            PopProductTypes();
            BindProducts();
        }
        private void PopProductTypes()
        {
            comproductType.DataSource = DataRepository.GetAllProductType();
            comproductType.SelectedIndex = -1;

        }
        private void BindProducts()
        {
            lvProductAll.BeginUpdate();
            lvProductAll.Items.Clear();
            if (_selectedproducts != null)
            {
                _selectedproducts.Clear();
            }
            _selectedproducts = new List<MasterProduct>();
            foreach(MasterProduct p in _products)
            {
             
               if(p.ProductType.Id == Convert.ToInt32(comproductType.SelectedValue))
               {                   
                  _selectedproducts.Add(p);
               }
            }
            if (_selectedproducts != null)
            {
                foreach (MasterProduct p in _selectedproducts)
                {
                    if (!IsProductSelected(p.Id))
                    {
                        ListViewItem li = new ListViewItem(p.ProductName) { Tag = p.Id };

                        li.SubItems.Add(p.SerialNo);
                        li.SubItems.Add(p.BasicUnit);
                        //li.SubItems.Add(p.PackagingSize.ToString());
                        li.SubItems.Add(p.Specification);

                        lvProductAll.Items.Add(li);
                    }
                }

                lvProductAll.EndUpdate();
            }

        }

        private bool IsProductSelected(int proid)
        {
            foreach (int id in _selectedPro)
            {
                if (id == proid)
                    return true;
            }
            return false;
        }

        private void butSelect_Click(object sender, EventArgs e)
        {
            int len = lvProductAll.SelectedItems.Count;

            for (int i = 0; i < len; i++)
            {
                SelectedProductIds.Add((int)lvProductAll.SelectedItems[i].Tag);
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void butCancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
        public int NoRPeriod()
        {
            if (txtNoperiod.Text == "")
                return 3;
            int no = int.Parse(txtNoperiod.Text);

            return no > 3 ? no : 3;
        }

        private void txtNoperiod_KeyPress(object sender, KeyPressEventArgs e)
        {
            int x = e.KeyChar;

            if ((x >= 48 && x <= 57) || (x == 8))
            {
                e.Handled = false;
            }
            else
                e.Handled = true;
        }

        private void comproductType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            BindProducts();
        }
    }
}
