﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.UserExceptions;
using LQT.Core.Util;

namespace LQT.GUI
{
    public partial class FrmNewPickProducts : Form
    {
        public IList<MasterProduct> _prevSelectedProduct;
        public IList<MasterProduct> _newSelectedProduct;
        public IList<MasterProduct> _products;

        public FrmNewPickProducts()
        {
            //_prevSelectedProduct = prevSelectedProduct;
            _newSelectedProduct = new List<MasterProduct>();
            InitializeComponent();
            PopProductType();
            BindProducts();
        }

        private void PopProductType()
        {
            //comproducttype.Items.Insert(0, "--All--");
            //comproducttype.SelectedIndex = 0;
            comproducttype.DataSource = DataRepository.GetAllProductType();
            

            //if (comproducttype.SelectedIndex >= -1)
               // _products = DataRepository.GetAllProduct();
           // else
            if(comproducttype.SelectedValue!=null)
                _products = DataRepository.GetAllProductByType((int)comproducttype.SelectedValue);

        }

        private void BindProducts()
        {
            lvProductAll.BeginUpdate();
            lvProductAll.Items.Clear();
            if (_products != null)
            {
                foreach (MasterProduct p in _products)
                {
                    //if (!IsProductSelected(p.Id))
                    //{
                    ListViewItem li = new ListViewItem(p.ProductName) { Tag = p };

                    li.SubItems.Add(p.SerialNo);
                    li.SubItems.Add(p.BasicUnit);
                    // li.SubItems.Add(p.PackagingSize.ToString());
                    li.SubItems.Add(p.Specification);

                    lvProductAll.Items.Add(li);
                    //}
                }
            }

            lvProductAll.EndUpdate();
        }

        private bool IsProductSelected(int proid)
        {
            foreach (MasterProduct p in _prevSelectedProduct)
            {
                if (p.Id == proid)
                    return true;
            }
            return false;
        }

        private void butSelect_Click(object sender, EventArgs e)
        {

            int len = lvProductAll.SelectedItems.Count;

            for (int i = 0; i < len; i++)
            {
                _newSelectedProduct.Add((MasterProduct)lvProductAll.SelectedItems[i].Tag);
            }

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
   
        }

        private void butCancle_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void comproducttype_SelectedIndexChanged(object sender, EventArgs e)
        {
           /// if (comproducttype.SelectedIndex >=-1)
             ///   _products = DataRepository.GetAllProduct();
            ///else
            ///
            if (comproducttype.SelectedValue != null)
            {
                _products = DataRepository.GetAllProductByType((int)comproducttype.SelectedValue);

                BindProducts();
            }
        }
    }
}
