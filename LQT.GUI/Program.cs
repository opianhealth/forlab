﻿using System;
using System.Windows.Forms;
using LQT.Core.UserExceptions;
using System.Threading;
using System.IO;
using LQT.GUI.Tools;
using System.Net;
using LQT.GUI.Quantification;

namespace LQT.GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //try
            //{
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.ThreadException += new ThreadExceptionEventHandler(UIThreadExceptionHandler);
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                AppDomain currentDomain = AppDomain.CurrentDomain;
                currentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);
                getFile();        
               Application.DoEvents();
                new frmSplash().ShowDialog();                
                Application.Run(new LqtMainWindowForm());
            //}
            //catch (Exception ex)
            //{
            //    new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();
            //    //System.Diagnostics.Debugger.Break();
            //}
        }
        public static void getFile()
        {
            string oldversion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Version _oldversion = new Version(oldversion);

            WebClient request = new WebClient();
            string url = "ftp://34.68.140.74/ForLab_NewVersion/" + "version.TXT";
            request.Credentials = new NetworkCredential("forlab_admin", "dV8*4XjUN4J1");
            try
            {
                byte[] newFileData = request.DownloadData(url);
                string fileString = System.Text.Encoding.UTF8.GetString(newFileData);
                string[] parts = fileString.Split(':');
                string LatestVersion = parts[1];
                Version _latestVersion = new Version(LatestVersion);

                if (_latestVersion > _oldversion)
                {
                    if (MessageBox.Show("New version of " + Application.ProductName + " is available.Do you want Install it?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        DownloadInstaler di = new DownloadInstaler();
                        di.ShowDialog();
                        Application.Exit();
                        Environment.Exit(0);
                    }
                }
            }
            catch (WebException e)
            {

            }
        }
        static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception ex = (Exception)args.ExceptionObject;
            LogErr(ex);
            if (ex.Message.Contains("could not execute"))
            {
                MessageBox.Show("You are using older Database Version, Please Create the new version   ");
                Form frm = new frmAdvanceSetting(FrmDatabaseSettingsEnum.SqlServerSettings, false, true);
                frm.ShowDialog();
            }
            //ILog log = LogManager.GetLogger("RollingFileAppender");
            // log4net.Config.XmlConfigurator.Configure();
            // Exception ex = (Exception)args.ExceptionObject;
            // log.Error( ex.Message + ex.StackTrace);
            // foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            // {
            //     app.Name = "LogFileAppender";
            //     app.Close();
            // }
            // log.Logger.Repository.Shutdown();
            new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();

        }
        private static void UIThreadExceptionHandler(object sender, ThreadExceptionEventArgs t)
            {
            LogErr(t.Exception);
            //ILog log = LogManager.GetLogger("RollingFileAppender");
            //log4net.Config.XmlConfigurator.Configure();
            //log.Error(t.Exception.Message + t.Exception.StackTrace);
            //foreach (log4net.Appender.IAppender app in log.Logger.Repository.GetAppenders())
            //{
            //    app.Name = "LogFileAppender";
            //    app.Close();
            //}
            //log.Logger.Repository.Shutdown();
            new FrmShowError(CustomExceptionHandler.ShowExceptionText(t.Exception)).ShowDialog();
            //Application.Exit();
        }
        public static void LogErr(Exception ex)
        {

            string strDirectoryPath = "";
            try
            {
                string directoryPath = strDirectoryPath;
                //if (!string.IsNullOrEmpty(strDirectoryPath))
                //{ "\\bin\\Debug\\"
                string path = Path.Combine(System.IO.Path.GetTempPath(), "error-log.txt");
                //Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "error-log.txt");
                //StreamWriter swErrorLog = null;

                if (File.Exists(path))
                {
                    using (StreamWriter swErrorLog = new StreamWriter(path, true))
                    {
                        //write to the file

                        //swErrorLog = new StreamWriter(path, true); //append the error message
                        swErrorLog.WriteLine("Date and Time of Exception: " + DateTime.Now);
                        swErrorLog.WriteLine("Source of Exception: " + ex.StackTrace);
                        swErrorLog.WriteLine(" ");
                        swErrorLog.WriteLine("Error Message: " + ex.Message);
                        swErrorLog.WriteLine("------------------------------------------- ");
                        swErrorLog.WriteLine(" ");
                        //swErrorLog.WriteLine(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                        swErrorLog.Close();
                        //swErrorLog = null;
                    }
                }
                else
                {

                    using (StreamWriter swErrorLog = new StreamWriter(path, true))
                    {
                        //File.CreateText(path);
                        swErrorLog.WriteLine("Date and Time of Exception: " + DateTime.Now);
                        swErrorLog.WriteLine("Source of Exception: " + ex.StackTrace);
                        swErrorLog.WriteLine(" ");
                        swErrorLog.WriteLine("Error Message: " + ex.Message);
                        swErrorLog.WriteLine("------------------------------------------- ");
                        swErrorLog.WriteLine(" ");
                        swErrorLog.Close();
                    }
                }
            }
            //}
            catch (NullReferenceException)
            {
                throw;
            }
        }
    }
}
