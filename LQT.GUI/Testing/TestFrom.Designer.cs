﻿namespace LQT.GUI.Testing
{
    partial class TestFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lqtToolStrip1 = new LQT.GUI.UserCtr.LqtToolStrip();
            this.txtTestname = new System.Windows.Forms.TextBox();
            this.comTestarea = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblTestProductType = new System.Windows.Forms.Label();
            this.comTestproductType = new System.Windows.Forms.ComboBox();
            this.lsvProductUsage = new LQT.GUI.LQTListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comInstrument = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butAdd = new System.Windows.Forms.Button();
            this.butRemove = new System.Windows.Forms.Button();
            this.comProduct = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.comCProductType = new System.Windows.Forms.ComboBox();
            this.lblCInstrumentControlP = new System.Windows.Forms.Label();
            this.lsvCProductUsage = new LQT.GUI.LQTListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comCInstrument = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.butCAdd = new System.Windows.Forms.Button();
            this.butCRemove = new System.Windows.Forms.Button();
            this.comCProduct = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPerTest = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ComCTProducttype = new System.Windows.Forms.ComboBox();
            this.txtNoofTest = new System.Windows.Forms.TextBox();
            this.lsvProductUsageT = new LQT.GUI.LQTListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.butAddT = new System.Windows.Forms.Button();
            this.butRemoveT = new System.Windows.Forms.Button();
            this.comProductT = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lsvpanel = new System.Windows.Forms.ListView();
            this.tabPerPeriod = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ComCPProductType = new System.Windows.Forms.ComboBox();
            this.lsvProductUsageP = new LQT.GUI.LQTListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comPeriodP = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.butAddP = new System.Windows.Forms.Button();
            this.butRemoveP = new System.Windows.Forms.Button();
            this.comProductP = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPerInstrument = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ComCIProductType = new System.Windows.Forms.ComboBox();
            this.comPeriodI = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.lsvProductUsageI = new LQT.GUI.LQTListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.comInstrumentI = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.butAddI = new System.Windows.Forms.Button();
            this.butRemoveI = new System.Windows.Forms.Button();
            this.comProductI = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPerTest.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPerPeriod.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPerInstrument.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lqtToolStrip1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(921, 35);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lqtToolStrip1
            // 
            this.lqtToolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lqtToolStrip1.Location = new System.Drawing.Point(4, 5);
            this.lqtToolStrip1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lqtToolStrip1.Name = "lqtToolStrip1";
            this.lqtToolStrip1.Size = new System.Drawing.Size(913, 25);
            this.lqtToolStrip1.TabIndex = 8;
            this.lqtToolStrip1.Load += new System.EventHandler(this.lqtToolStrip1_Load);
            // 
            // txtTestname
            // 
            this.txtTestname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTestname.Location = new System.Drawing.Point(115, 19);
            this.txtTestname.MaximumSize = new System.Drawing.Size(311, 25);
            this.txtTestname.MinimumSize = new System.Drawing.Size(311, 25);
            this.txtTestname.Name = "txtTestname";
            this.txtTestname.Size = new System.Drawing.Size(311, 20);
            this.txtTestname.TabIndex = 0;
            // 
            // comTestarea
            // 
            this.comTestarea.DisplayMember = "AreaName";
            this.comTestarea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comTestarea.FormattingEnabled = true;
            this.comTestarea.Location = new System.Drawing.Point(115, 49);
            this.comTestarea.Name = "comTestarea";
            this.comTestarea.Size = new System.Drawing.Size(311, 21);
            this.comTestarea.TabIndex = 1;
            this.comTestarea.Tag = "";
            this.comTestarea.ValueMember = "Id";
            this.comTestarea.SelectedIndexChanged += new System.EventHandler(this.comTestarea_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Test Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 50;
            this.label4.Text = "Testing Area:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(10, 122);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(907, 370);
            this.tabControl1.TabIndex = 59;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(899, 344);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Test Product Usage Rate";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTestProductType);
            this.groupBox1.Controls.Add(this.comTestproductType);
            this.groupBox1.Controls.Add(this.lsvProductUsage);
            this.groupBox1.Controls.Add(this.comInstrument);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.butAdd);
            this.groupBox1.Controls.Add(this.butRemove);
            this.groupBox1.Controls.Add(this.comProduct);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(887, 335);
            this.groupBox1.TabIndex = 59;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = ".";
            // 
            // lblTestProductType
            // 
            this.lblTestProductType.AutoSize = true;
            this.lblTestProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblTestProductType.Location = new System.Drawing.Point(256, 22);
            this.lblTestProductType.Name = "lblTestProductType";
            this.lblTestProductType.Size = new System.Drawing.Size(83, 13);
            this.lblTestProductType.TabIndex = 313;
            this.lblTestProductType.Text = "Product Type";
            // 
            // comTestproductType
            // 
            this.comTestproductType.DisplayMember = "TypeName";
            this.comTestproductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comTestproductType.FormattingEnabled = true;
            this.comTestproductType.Location = new System.Drawing.Point(341, 18);
            this.comTestproductType.Name = "comTestproductType";
            this.comTestproductType.Size = new System.Drawing.Size(136, 21);
            this.comTestproductType.TabIndex = 312;
            this.comTestproductType.ValueMember = "Id";
            this.comTestproductType.SelectedIndexChanged += new System.EventHandler(this.comTestproductType_SelectedIndexChanged);
            // 
            // lsvProductUsage
            // 
            this.lsvProductUsage.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lsvProductUsage.FullRowSelect = true;
            this.lsvProductUsage.GridLines = true;
            this.lsvProductUsage.Location = new System.Drawing.Point(7, 46);
            this.lsvProductUsage.Name = "lsvProductUsage";
            this.lsvProductUsage.Size = new System.Drawing.Size(874, 283);
            this.lsvProductUsage.TabIndex = 311;
            this.lsvProductUsage.TabStop = false;
            this.lsvProductUsage.UseCompatibleStateImageBehavior = false;
            this.lsvProductUsage.View = System.Windows.Forms.View.Details;
            this.lsvProductUsage.SelectedIndexChanged += new System.EventHandler(this.lsvProductUsage_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Product";
            this.columnHeader1.Width = 800;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Usage Rate";
            this.columnHeader2.Width = 300;
            // 
            // comInstrument
            // 
            this.comInstrument.DisplayMember = "InstrumentName";
            this.comInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comInstrument.FormattingEnabled = true;
            this.comInstrument.Location = new System.Drawing.Point(107, 19);
            this.comInstrument.Name = "comInstrument";
            this.comInstrument.Size = new System.Drawing.Size(148, 21);
            this.comInstrument.TabIndex = 3;
            this.comInstrument.ValueMember = "Id";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Select Instrument";
            // 
            // butAdd
            // 
            this.butAdd.Enabled = false;
            this.butAdd.Location = new System.Drawing.Point(755, 16);
            this.butAdd.Name = "butAdd";
            this.butAdd.Size = new System.Drawing.Size(50, 22);
            this.butAdd.TabIndex = 5;
            this.butAdd.Text = "Add";
            this.butAdd.UseVisualStyleBackColor = true;
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // butRemove
            // 
            this.butRemove.Enabled = false;
            this.butRemove.Location = new System.Drawing.Point(821, 16);
            this.butRemove.Name = "butRemove";
            this.butRemove.Size = new System.Drawing.Size(60, 22);
            this.butRemove.TabIndex = 6;
            this.butRemove.Text = "Remove";
            this.butRemove.UseVisualStyleBackColor = true;
            this.butRemove.Click += new System.EventHandler(this.butRemove_Click);
            // 
            // comProduct
            // 
            this.comProduct.DisplayMember = "ProductName";
            this.comProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comProduct.FormattingEnabled = true;
            this.comProduct.Location = new System.Drawing.Point(572, 19);
            this.comProduct.Name = "comProduct";
            this.comProduct.Size = new System.Drawing.Size(153, 21);
            this.comProduct.TabIndex = 4;
            this.comProduct.ValueMember = "Id";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(478, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Select Product";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.comCProductType);
            this.tabPage2.Controls.Add(this.lblCInstrumentControlP);
            this.tabPage2.Controls.Add(this.lsvCProductUsage);
            this.tabPage2.Controls.Add(this.comCInstrument);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.butCAdd);
            this.tabPage2.Controls.Add(this.butCRemove);
            this.tabPage2.Controls.Add(this.comCProduct);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(899, 344);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Control Product Usage Rate";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(277, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 321;
            this.label14.Text = "Product Type";
            // 
            // comCProductType
            // 
            this.comCProductType.DisplayMember = "TypeName";
            this.comCProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCProductType.FormattingEnabled = true;
            this.comCProductType.Location = new System.Drawing.Point(361, 26);
            this.comCProductType.Name = "comCProductType";
            this.comCProductType.Size = new System.Drawing.Size(136, 21);
            this.comCProductType.TabIndex = 320;
            this.comCProductType.ValueMember = "Id";
            this.comCProductType.SelectedIndexChanged += new System.EventHandler(this.comCProductType_SelectedIndexChanged);
            // 
            // lblCInstrumentControlP
            // 
            this.lblCInstrumentControlP.AutoSize = true;
            this.lblCInstrumentControlP.Location = new System.Drawing.Point(41, 9);
            this.lblCInstrumentControlP.Name = "lblCInstrumentControlP";
            this.lblCInstrumentControlP.Size = new System.Drawing.Size(35, 13);
            this.lblCInstrumentControlP.TabIndex = 319;
            this.lblCInstrumentControlP.Text = "label2";
            // 
            // lsvCProductUsage
            // 
            this.lsvCProductUsage.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.lsvCProductUsage.FullRowSelect = true;
            this.lsvCProductUsage.GridLines = true;
            this.lsvCProductUsage.Location = new System.Drawing.Point(13, 52);
            this.lsvCProductUsage.Name = "lsvCProductUsage";
            this.lsvCProductUsage.Size = new System.Drawing.Size(880, 283);
            this.lsvCProductUsage.TabIndex = 318;
            this.lsvCProductUsage.TabStop = false;
            this.lsvCProductUsage.UseCompatibleStateImageBehavior = false;
            this.lsvCProductUsage.View = System.Windows.Forms.View.Details;
            this.lsvCProductUsage.SelectedIndexChanged += new System.EventHandler(this.lsvCProductUsage_SelectedIndexChanged);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Product";
            this.columnHeader3.Width = 520;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Usage Rate";
            this.columnHeader4.Width = 150;
            // 
            // comCInstrument
            // 
            this.comCInstrument.DisplayMember = "InstrumentName";
            this.comCInstrument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCInstrument.FormattingEnabled = true;
            this.comCInstrument.Location = new System.Drawing.Point(113, 27);
            this.comCInstrument.Name = "comCInstrument";
            this.comCInstrument.Size = new System.Drawing.Size(161, 21);
            this.comCInstrument.TabIndex = 312;
            this.comCInstrument.ValueMember = "Id";
            this.comCInstrument.SelectedIndexChanged += new System.EventHandler(this.comCInstrument_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(7, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 317;
            this.label3.Text = "Select Instrument";
            // 
            // butCAdd
            // 
            this.butCAdd.Enabled = false;
            this.butCAdd.Location = new System.Drawing.Point(778, 24);
            this.butCAdd.Name = "butCAdd";
            this.butCAdd.Size = new System.Drawing.Size(50, 22);
            this.butCAdd.TabIndex = 314;
            this.butCAdd.Text = "Add";
            this.butCAdd.UseVisualStyleBackColor = true;
            this.butCAdd.Click += new System.EventHandler(this.butCAdd_Click);
            // 
            // butCRemove
            // 
            this.butCRemove.Enabled = false;
            this.butCRemove.Location = new System.Drawing.Point(830, 23);
            this.butCRemove.Name = "butCRemove";
            this.butCRemove.Size = new System.Drawing.Size(60, 22);
            this.butCRemove.TabIndex = 315;
            this.butCRemove.Text = "Remove";
            this.butCRemove.UseVisualStyleBackColor = true;
            this.butCRemove.Click += new System.EventHandler(this.butCRemove_Click);
            // 
            // comCProduct
            // 
            this.comCProduct.DisplayMember = "ProductName";
            this.comCProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comCProduct.FormattingEnabled = true;
            this.comCProduct.Location = new System.Drawing.Point(591, 26);
            this.comCProduct.Name = "comCProduct";
            this.comCProduct.Size = new System.Drawing.Size(183, 21);
            this.comCProduct.TabIndex = 313;
            this.comCProduct.ValueMember = "Id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(497, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 316;
            this.label7.Text = "Select Product";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tabControl2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(899, 344);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Consumable Product Usage Rate";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPerTest);
            this.tabControl2.Controls.Add(this.tabPerPeriod);
            this.tabControl2.Controls.Add(this.tabPerInstrument);
            this.tabControl2.Location = new System.Drawing.Point(2, 4);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(891, 337);
            this.tabControl2.TabIndex = 121;
            // 
            // tabPerTest
            // 
            this.tabPerTest.Controls.Add(this.groupBox2);
            this.tabPerTest.Controls.Add(this.lsvpanel);
            this.tabPerTest.Location = new System.Drawing.Point(4, 22);
            this.tabPerTest.Name = "tabPerTest";
            this.tabPerTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPerTest.Size = new System.Drawing.Size(883, 311);
            this.tabPerTest.TabIndex = 0;
            this.tabPerTest.Text = "Per Test";
            this.tabPerTest.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.ComCTProducttype);
            this.groupBox2.Controls.Add(this.txtNoofTest);
            this.groupBox2.Controls.Add(this.lsvProductUsageT);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.butAddT);
            this.groupBox2.Controls.Add(this.butRemoveT);
            this.groupBox2.Controls.Add(this.comProductT);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(-3, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(880, 295);
            this.groupBox2.TabIndex = 59;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Product Usage Rate";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(193, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 323;
            this.label15.Text = "Product Type";
            // 
            // ComCTProducttype
            // 
            this.ComCTProducttype.DisplayMember = "TypeName";
            this.ComCTProducttype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComCTProducttype.FormattingEnabled = true;
            this.ComCTProducttype.Location = new System.Drawing.Point(282, 15);
            this.ComCTProducttype.Name = "ComCTProducttype";
            this.ComCTProducttype.Size = new System.Drawing.Size(165, 21);
            this.ComCTProducttype.TabIndex = 322;
            this.ComCTProducttype.ValueMember = "Id";
            this.ComCTProducttype.SelectedIndexChanged += new System.EventHandler(this.ComCTProducttype_SelectedIndexChanged);
            // 
            // txtNoofTest
            // 
            this.txtNoofTest.Location = new System.Drawing.Point(86, 19);
            this.txtNoofTest.Name = "txtNoofTest";
            this.txtNoofTest.Size = new System.Drawing.Size(100, 20);
            this.txtNoofTest.TabIndex = 312;
            this.txtNoofTest.Text = "0";
            this.txtNoofTest.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoofTest_KeyPress);
            // 
            // lsvProductUsageT
            // 
            this.lsvProductUsageT.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader6});
            this.lsvProductUsageT.FullRowSelect = true;
            this.lsvProductUsageT.GridLines = true;
            this.lsvProductUsageT.Location = new System.Drawing.Point(7, 50);
            this.lsvProductUsageT.Name = "lsvProductUsageT";
            this.lsvProductUsageT.Size = new System.Drawing.Size(867, 240);
            this.lsvProductUsageT.TabIndex = 311;
            this.lsvProductUsageT.TabStop = false;
            this.lsvProductUsageT.UseCompatibleStateImageBehavior = false;
            this.lsvProductUsageT.View = System.Windows.Forms.View.Details;
            this.lsvProductUsageT.SubitemTextChanged += new System.EventHandler<LQT.GUI.SubitemTextEventArgs>(this.lsvProductUsageT_OnSubitemTextChanged);
            this.lsvProductUsageT.SelectedIndexChanged += new System.EventHandler(this.lsvProductUsageT_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Product";
            this.columnHeader5.Width = 307;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "# of Test";
            this.columnHeader7.Width = 104;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Usage Rate";
            this.columnHeader6.Width = 197;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 41;
            this.label2.Text = "# of Test";
            // 
            // butAddT
            // 
            this.butAddT.Enabled = false;
            this.butAddT.Location = new System.Drawing.Point(758, 16);
            this.butAddT.Name = "butAddT";
            this.butAddT.Size = new System.Drawing.Size(50, 22);
            this.butAddT.TabIndex = 5;
            this.butAddT.Text = "Add";
            this.butAddT.UseVisualStyleBackColor = true;
            this.butAddT.Click += new System.EventHandler(this.butAddT_Click);
            // 
            // butRemoveT
            // 
            this.butRemoveT.Enabled = false;
            this.butRemoveT.Location = new System.Drawing.Point(814, 16);
            this.butRemoveT.Name = "butRemoveT";
            this.butRemoveT.Size = new System.Drawing.Size(60, 22);
            this.butRemoveT.TabIndex = 6;
            this.butRemoveT.Text = "Remove";
            this.butRemoveT.UseVisualStyleBackColor = true;
            this.butRemoveT.Click += new System.EventHandler(this.butRemoveT_Click);
            // 
            // comProductT
            // 
            this.comProductT.DisplayMember = "ProductName";
            this.comProductT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comProductT.FormattingEnabled = true;
            this.comProductT.Location = new System.Drawing.Point(550, 16);
            this.comProductT.Name = "comProductT";
            this.comProductT.Size = new System.Drawing.Size(202, 21);
            this.comProductT.TabIndex = 4;
            this.comProductT.ValueMember = "Id";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(453, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 40;
            this.label8.Text = "Select Product";
            // 
            // lsvpanel
            // 
            this.lsvpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lsvpanel.FullRowSelect = true;
            this.lsvpanel.GridLines = true;
            this.lsvpanel.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lsvpanel.Location = new System.Drawing.Point(6, 17);
            this.lsvpanel.MultiSelect = false;
            this.lsvpanel.Name = "lsvpanel";
            this.lsvpanel.Size = new System.Drawing.Size(614, 253);
            this.lsvpanel.TabIndex = 4;
            this.lsvpanel.UseCompatibleStateImageBehavior = false;
            this.lsvpanel.View = System.Windows.Forms.View.Details;
            // 
            // tabPerPeriod
            // 
            this.tabPerPeriod.Controls.Add(this.groupBox3);
            this.tabPerPeriod.Location = new System.Drawing.Point(4, 22);
            this.tabPerPeriod.Name = "tabPerPeriod";
            this.tabPerPeriod.Padding = new System.Windows.Forms.Padding(3);
            this.tabPerPeriod.Size = new System.Drawing.Size(883, 311);
            this.tabPerPeriod.TabIndex = 1;
            this.tabPerPeriod.Text = "Per Peroid";
            this.tabPerPeriod.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.ComCPProductType);
            this.groupBox3.Controls.Add(this.lsvProductUsageP);
            this.groupBox3.Controls.Add(this.comPeriodP);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.butAddP);
            this.groupBox3.Controls.Add(this.butRemoveP);
            this.groupBox3.Controls.Add(this.comProductP);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Location = new System.Drawing.Point(-3, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(880, 317);
            this.groupBox3.TabIndex = 60;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Product Usage Rate";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(256, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 13);
            this.label16.TabIndex = 325;
            this.label16.Text = "Product Type";
            // 
            // ComCPProductType
            // 
            this.ComCPProductType.DisplayMember = "TypeName";
            this.ComCPProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComCPProductType.FormattingEnabled = true;
            this.ComCPProductType.Location = new System.Drawing.Point(345, 18);
            this.ComCPProductType.Name = "ComCPProductType";
            this.ComCPProductType.Size = new System.Drawing.Size(165, 21);
            this.ComCPProductType.TabIndex = 324;
            this.ComCPProductType.ValueMember = "Id";
            this.ComCPProductType.SelectedIndexChanged += new System.EventHandler(this.ComCPProductType_SelectedIndexChanged);
            // 
            // lsvProductUsageP
            // 
            this.lsvProductUsageP.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.lsvProductUsageP.FullRowSelect = true;
            this.lsvProductUsageP.GridLines = true;
            this.lsvProductUsageP.Location = new System.Drawing.Point(7, 50);
            this.lsvProductUsageP.Name = "lsvProductUsageP";
            this.lsvProductUsageP.Size = new System.Drawing.Size(867, 216);
            this.lsvProductUsageP.TabIndex = 311;
            this.lsvProductUsageP.TabStop = false;
            this.lsvProductUsageP.UseCompatibleStateImageBehavior = false;
            this.lsvProductUsageP.View = System.Windows.Forms.View.Details;
            this.lsvProductUsageP.SubitemTextChanged += new System.EventHandler<LQT.GUI.SubitemTextEventArgs>(this.lsvProductUsageP_OnSubitemTextChanged);
            this.lsvProductUsageP.SelectedIndexChanged += new System.EventHandler(this.lsvProductUsageP_SelectedIndexChanged);
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Product";
            this.columnHeader8.Width = 305;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Peroid";
            this.columnHeader9.Width = 141;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Usage Rate";
            this.columnHeader10.Width = 157;
            // 
            // comPeriodP
            // 
            this.comPeriodP.DisplayMember = "Id";
            this.comPeriodP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPeriodP.FormattingEnabled = true;
            this.comPeriodP.Location = new System.Drawing.Point(53, 19);
            this.comPeriodP.Name = "comPeriodP";
            this.comPeriodP.Size = new System.Drawing.Size(202, 21);
            this.comPeriodP.TabIndex = 3;
            this.comPeriodP.ValueMember = "Id";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Period";
            // 
            // butAddP
            // 
            this.butAddP.Enabled = false;
            this.butAddP.Location = new System.Drawing.Point(758, 19);
            this.butAddP.Name = "butAddP";
            this.butAddP.Size = new System.Drawing.Size(50, 22);
            this.butAddP.TabIndex = 5;
            this.butAddP.Text = "Add";
            this.butAddP.UseVisualStyleBackColor = true;
            this.butAddP.Click += new System.EventHandler(this.butAddP_Click);
            // 
            // butRemoveP
            // 
            this.butRemoveP.Enabled = false;
            this.butRemoveP.Location = new System.Drawing.Point(814, 18);
            this.butRemoveP.Name = "butRemoveP";
            this.butRemoveP.Size = new System.Drawing.Size(60, 22);
            this.butRemoveP.TabIndex = 6;
            this.butRemoveP.Text = "Remove";
            this.butRemoveP.UseVisualStyleBackColor = true;
            this.butRemoveP.Click += new System.EventHandler(this.butRemoveP_Click);
            // 
            // comProductP
            // 
            this.comProductP.DisplayMember = "ProductName";
            this.comProductP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comProductP.FormattingEnabled = true;
            this.comProductP.Location = new System.Drawing.Point(550, 20);
            this.comProductP.Name = "comProductP";
            this.comProductP.Size = new System.Drawing.Size(202, 21);
            this.comProductP.TabIndex = 4;
            this.comProductP.ValueMember = "Id";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(514, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 40;
            this.label10.Text = "Pro.";
            // 
            // tabPerInstrument
            // 
            this.tabPerInstrument.Controls.Add(this.groupBox4);
            this.tabPerInstrument.Location = new System.Drawing.Point(4, 22);
            this.tabPerInstrument.Name = "tabPerInstrument";
            this.tabPerInstrument.Padding = new System.Windows.Forms.Padding(3);
            this.tabPerInstrument.Size = new System.Drawing.Size(883, 311);
            this.tabPerInstrument.TabIndex = 2;
            this.tabPerInstrument.Text = "Per Instrument Per Period";
            this.tabPerInstrument.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.ComCIProductType);
            this.groupBox4.Controls.Add(this.comPeriodI);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.lsvProductUsageI);
            this.groupBox4.Controls.Add(this.comInstrumentI);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.butAddI);
            this.groupBox4.Controls.Add(this.butRemoveI);
            this.groupBox4.Controls.Add(this.comProductI);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Location = new System.Drawing.Point(-3, 17);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(880, 292);
            this.groupBox4.TabIndex = 60;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Product Usage Rate";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(360, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 327;
            this.label17.Text = "Product Type";
            // 
            // ComCIProductType
            // 
            this.ComCIProductType.DisplayMember = "TypeName";
            this.ComCIProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComCIProductType.FormattingEnabled = true;
            this.ComCIProductType.Location = new System.Drawing.Point(449, 19);
            this.ComCIProductType.Name = "ComCIProductType";
            this.ComCIProductType.Size = new System.Drawing.Size(202, 21);
            this.ComCIProductType.TabIndex = 326;
            this.ComCIProductType.ValueMember = "Id";
            this.ComCIProductType.SelectedIndexChanged += new System.EventHandler(this.ComCIProductType_SelectedIndexChanged);
            // 
            // comPeriodI
            // 
            this.comPeriodI.DisplayMember = "Id";
            this.comPeriodI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPeriodI.FormattingEnabled = true;
            this.comPeriodI.Location = new System.Drawing.Point(111, 50);
            this.comPeriodI.Name = "comPeriodI";
            this.comPeriodI.Size = new System.Drawing.Size(202, 21);
            this.comPeriodI.TabIndex = 312;
            this.comPeriodI.ValueMember = "Id";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(63, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 313;
            this.label11.Text = "Period";
            // 
            // lsvProductUsageI
            // 
            this.lsvProductUsageI.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.lsvProductUsageI.FullRowSelect = true;
            this.lsvProductUsageI.GridLines = true;
            this.lsvProductUsageI.Location = new System.Drawing.Point(7, 86);
            this.lsvProductUsageI.Name = "lsvProductUsageI";
            this.lsvProductUsageI.Size = new System.Drawing.Size(867, 195);
            this.lsvProductUsageI.TabIndex = 311;
            this.lsvProductUsageI.TabStop = false;
            this.lsvProductUsageI.UseCompatibleStateImageBehavior = false;
            this.lsvProductUsageI.View = System.Windows.Forms.View.Details;
            this.lsvProductUsageI.SubitemTextChanged += new System.EventHandler<LQT.GUI.SubitemTextEventArgs>(this.lsvProductUsageI_OnSubitemTextChanged);
            this.lsvProductUsageI.SelectedIndexChanged += new System.EventHandler(this.lsvProductUsageI_SelectedIndexChanged);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Product";
            this.columnHeader11.Width = 221;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Period";
            this.columnHeader12.Width = 123;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Usage Rate";
            this.columnHeader13.Width = 116;
            // 
            // comInstrumentI
            // 
            this.comInstrumentI.DisplayMember = "InstrumentName";
            this.comInstrumentI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comInstrumentI.FormattingEnabled = true;
            this.comInstrumentI.Location = new System.Drawing.Point(111, 19);
            this.comInstrumentI.Name = "comInstrumentI";
            this.comInstrumentI.Size = new System.Drawing.Size(202, 21);
            this.comInstrumentI.TabIndex = 3;
            this.comInstrumentI.ValueMember = "Id";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "Select Instrument";
            // 
            // butAddI
            // 
            this.butAddI.Enabled = false;
            this.butAddI.Location = new System.Drawing.Point(657, 48);
            this.butAddI.Name = "butAddI";
            this.butAddI.Size = new System.Drawing.Size(50, 22);
            this.butAddI.TabIndex = 5;
            this.butAddI.Text = "Add";
            this.butAddI.UseVisualStyleBackColor = true;
            this.butAddI.Click += new System.EventHandler(this.butAddI_Click);
            // 
            // butRemoveI
            // 
            this.butRemoveI.Enabled = false;
            this.butRemoveI.Location = new System.Drawing.Point(713, 47);
            this.butRemoveI.Name = "butRemoveI";
            this.butRemoveI.Size = new System.Drawing.Size(60, 22);
            this.butRemoveI.TabIndex = 6;
            this.butRemoveI.Text = "Remove";
            this.butRemoveI.UseVisualStyleBackColor = true;
            this.butRemoveI.Click += new System.EventHandler(this.butRemoveI_Click);
            // 
            // comProductI
            // 
            this.comProductI.DisplayMember = "ProductName";
            this.comProductI.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comProductI.FormattingEnabled = true;
            this.comProductI.Location = new System.Drawing.Point(449, 49);
            this.comProductI.Name = "comProductI";
            this.comProductI.Size = new System.Drawing.Size(202, 21);
            this.comProductI.TabIndex = 4;
            this.comProductI.ValueMember = "Id";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(355, 53);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 13);
            this.label13.TabIndex = 40;
            this.label13.Text = "Select Product";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.comTestarea);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtTestname);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Location = new System.Drawing.Point(10, 37);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(903, 75);
            this.groupBox5.TabIndex = 60;
            this.groupBox5.TabStop = false;
            // 
            // TestFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 497);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TestFrom";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add/Edit Test Profile";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TestFrom_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TestFrom_FormClosed);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPerTest.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPerPeriod.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPerInstrument.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private UserCtr.LqtToolStrip lqtToolStrip1;
        private System.Windows.Forms.TextBox txtTestname;
        private System.Windows.Forms.ComboBox comTestarea;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private LQTListView lsvProductUsage;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ComboBox comInstrument;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butAdd;
        private System.Windows.Forms.Button butRemove;
        private System.Windows.Forms.ComboBox comProduct;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblCInstrumentControlP;
        private LQTListView lsvCProductUsage;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ComboBox comCInstrument;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button butCAdd;
        private System.Windows.Forms.Button butCRemove;
        private System.Windows.Forms.ComboBox comCProduct;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPerTest;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtNoofTest;
        private LQTListView lsvProductUsageT;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button butAddT;
        private System.Windows.Forms.Button butRemoveT;
        private System.Windows.Forms.ComboBox comProductT;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView lsvpanel;
        private System.Windows.Forms.TabPage tabPerPeriod;
        private System.Windows.Forms.GroupBox groupBox3;
        private LQTListView lsvProductUsageP;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ComboBox comPeriodP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button butAddP;
        private System.Windows.Forms.Button butRemoveP;
        private System.Windows.Forms.ComboBox comProductP;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPerInstrument;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comPeriodI;
        private System.Windows.Forms.Label label11;
        private LQTListView lsvProductUsageI;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ComboBox comInstrumentI;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button butAddI;
        private System.Windows.Forms.Button butRemoveI;
        private System.Windows.Forms.ComboBox comProductI;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblTestProductType;
        private System.Windows.Forms.ComboBox comTestproductType;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comCProductType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox ComCTProducttype;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox ComCPProductType;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox ComCIProductType;
        // private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}