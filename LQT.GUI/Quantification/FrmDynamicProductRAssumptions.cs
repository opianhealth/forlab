﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using LQT.Core.Domain;
using LQT.Core.Util;

namespace LQT.GUI.Quantification
{
    public partial class FrmDynamicProductRAssumptions : Form
    {
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private Form _mdiparent;
        public static Int64 _totalTarget = 0;
        DataTable mainDt = new DataTable();
        DataTable _Dtpatient1 = new DataTable();
        private ForecastInfo _mMforecastinfo;
        private string sqlretrieve = "";
        DataTable Dt = new DataTable();
        public SqlConnection sqlConnection = ConnectionManager.GetInstance().GetSqlConnection();
        private string UseOn = "";
        public FrmDynamicProductRAssumptions(Form mdiparent, ForecastInfo finfo, DataTable Dtpatient1, Int64 totalTarget, DataTable Dt1)
        {
            InitializeComponent();
            this._mMforecastinfo = finfo;
            this._mdiparent = mdiparent;
            mainDt = Dt1;
            _totalTarget = totalTarget;
            _Dtpatient1 = Dtpatient1;
            btnPrevious.TabStop = false;
            btnPrevious.FlatStyle = FlatStyle.Flat;
            btnPrevious.FlatAppearance.BorderSize = 0;
            btnPrevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            this.Top = 120;
    
            this.Left = 250;
            if (_mMforecastinfo.ForecastType == "S")
                UseOn = MorbidityVariableUsage.OnEachSite.ToString();
            else
                UseOn = MorbidityVariableUsage.OnAggSite.ToString();
           // _mMGeneralAssumption = DataRepository.GetAllGeneralDynamicAssumptionByProgram(_mMforecastinfo.ProgramId, UseOn);
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Product_Assumption"), _mMforecastinfo.ProgramId, UseOn);
        
            fillGrid(_mMforecastinfo.Id);
            fillList();
        }
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
      
         
            sqlretrieve = "";
            DataTable dt1 = new System.Data.DataTable();
         
            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                int bn = gAssumption.AssumptionType;
                if (gAssumption.AssumptionType ==2 && gAssumption.UseOn.ToString() == UseOn)
                {

                    sqlretrieve += "[" + gAssumption.VariableName + "],";             

                }
            }
            sqlretrieve = sqlretrieve.TrimEnd(',');
         
            try
            {
              
                    _sql = "SELECT [ID],[ForecastinfoID],[ProductTypeID],sc.TypeName as ProductType,";
                    _sql += "" + sqlretrieve + "  FROM [TestingAssumption] p ";
                    _sql += "inner join ProductType sc on p.ProductTypeID=sc.TypeID Where [ForecastinfoID]=" + ForecastinfoID + "  ";
              

                Dt = InsertRepository.SelectData(sqlConnection, _sql);
                gvPAssumptions.DataSource = Dt;
                gvPAssumptions.Columns[0].Visible = false;
                gvPAssumptions.Columns[1].Visible = false;
                gvPAssumptions.Columns[2].Visible = false;
                gvPAssumptions.Columns[3].ReadOnly = true;
            }
            catch (Exception ex)
            {

            }
        }
        private void fillList()
        {
            string _sql = "";
            DataTable dtCategory = new System.Data.DataTable();
            try
            {
                _sql = string.Format(@"Select TypeID,TypeName from ProductType");              
                    dtCategory = InsertRepository.SelectData(sqlConnection, _sql);
                    lstTestingArea.DataSource = dtCategory;
                    lstTestingArea.DisplayMember = "TypeName";
                    lstTestingArea.ValueMember = "TypeID";
                  
              
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
                bool status=false;
    
             
                int i = 3;
                try
                {
                    foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
                    {
                        if (gAssumption.AssumptionType != 1 && gAssumption.UseOn.ToString() == UseOn)
                        {

                           
                        }
                    }
              


                    Dt = gvPAssumptions.DataSource as DataTable;

                    DataRow dr = Dt.NewRow();

                    // dr["Id"] = Convert.ToString(ID);
                    dr["Id"] = 0;
                    dr["ForecastinfoID"] = _mMforecastinfo.Id;
                    dr["ProductTypeID"] = Convert.ToInt32(lstTestingArea.SelectedValue);
                    dr["ProductType"] = lstTestingArea.Text;
                  //  gvPAssumptions.DataSource = Dt;
                    for (int j = 4; j < Dt.Columns.Count; j++)
                    {
                        dr[j] = 0;
                    }
                        if (gvPAssumptions.Rows.Count > 0)
                        {
                            foreach (DataGridViewRow row in gvPAssumptions.Rows)
                            {
                                if (row.Cells["ProductType"].Value.ToString().ToLower().Equals(lstTestingArea.Text.ToString().ToLower()))
                                {
                                    MessageBox.Show("Product Type is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                                    status = true;
                                    break;
                                }
                            }
                            if (status == false)
                            {

                                Dt.Rows.Add(dr);

                            }
                        }
               else  Dt.Rows.Add(dr);
                    gvPAssumptions.DataSource = Dt;
                 
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                  
                }
      }

        private void btnNext_Click(object sender, EventArgs e)
        {
            string columns = "";
            string columnsvalues = "";
            string _sql = "";
            try
            {
                columns = "ForecastinfoID,ProductTypeID,";
                foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
                {
                    if (gAssumption.AssumptionType == 2 && gAssumption.UseOn.ToString() == UseOn)
                    {

                        columns += "[" + gAssumption.VariableName + "],";
                    }
                }
                columns = columns.TrimEnd(',');       
                for (int i = 0; i < gvPAssumptions.Rows.Count; i++)
                {
                    _sql = "Delete from [TestingAssumption] where ID=" + gvPAssumptions.Rows[i].Cells["ID"].Value + " ";
                    InsertRepository.ExcuteScripts(sqlConnection, _sql);
                 
                    columnsvalues = Convert.ToString(_mMforecastinfo.Id) + "," + gvPAssumptions.Rows[i].Cells["ProducttypeID"].Value.ToString() + ",";
                    for (int j = 4; j < gvPAssumptions.Rows[i].Cells.Count ; j++)
                    {

                        columnsvalues += Convert.ToString(gvPAssumptions.Rows[i].Cells[j].Value) + ",";

                    }
                    columnsvalues = columnsvalues.TrimEnd(',');
                    _sql = "INSERT INTO [TestingAssumption] (" + columns + ") VALUES(" + columnsvalues + ")";
                    InsertRepository.ExcuteScripts(sqlConnection, _sql);
                 }
                ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save successfully..", true);
                this.Hide();
                Frmlineargrowth FPA = new Frmlineargrowth(_mdiparent, _mMforecastinfo.Id, "E", _Dtpatient1, _mMforecastinfo.StartDate.ToString(), _mMforecastinfo.ForecastDate.ToString(), _mMforecastinfo.Period, _totalTarget, mainDt, _mMforecastinfo);
                FPA.ShowDialog();

                this.Close();
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            this.Hide();
            //FrmDynamicPatientAssumptions frm = new FrmDynamicPatientAssumptions(_mdiparent, _mMforecastinfo);
            FrmDynamicPatientAssumptions frm = new FrmDynamicPatientAssumptions(_mdiparent, _mMforecastinfo, _Dtpatient1, _totalTarget, mainDt);
            frm.ShowDialog();
            this.Close();
        }
    

    }
}
