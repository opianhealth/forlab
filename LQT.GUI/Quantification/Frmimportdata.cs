﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Runtime.InteropServices;
using System.Data.Common;
using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Asset;
using LQT.GUI.UserCtr;

namespace LQT.GUI.Quantification
{
    public partial class Frmimportdata : Form
    {

        private IList<Importlist.ImportTestData> _rdata;
        private IList<Importlist.ImportProductData> _rproductdata;
        private IList<Importlist.ImportInsData> _rinsata;
        private IList<Importlist.ImportProUsageData> _rtestprodata;
        private IList<Importlist.ImportConsumbleData> _rconsumbledata;
        private IList<Importlist.RegionReportedData> _rregiondata;
        private IList<Importlist.SiteImportData> _rsiteata;
        private IList<Importlist.SiteInstrumentImportData> _siteinstrumentdata;

        private IList<Importlist.RefSiteImportData> _rrefsitedata;

        private IList<Importlist.ImportQVariableData> _rvariabledata;
        IList<ForlabSite> result = new List<ForlabSite>();
     //   bool sitelist = false;
        private int sitecount = 0;
        private int siteIndex = 0;
        private int siteCatCount = 0;
        public string successfailuremsg = "";
        bool sitelist_new = false;
        bool ProcessStatus = false;
        bool btnclick = false;
        IList<ForlabSite> totalResult = new List<ForlabSite>();
        private string duplicatename = "";
        int instcount = 0;
        private Form _mdiparent;
        private int _tabid = -1;
        int errorsiteins = 0;
        public Frmimportdata(Form mdiparent)
        {
            InitializeComponent();
            this._mdiparent = mdiparent;
        }

        private void butBrowse_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtFilename.Text = openFileDialog1.FileName;
            }
        }
        private string validateandsavetestdata(string Sheetname)
        {
            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a aexcel file";
                return errorstr;
            }
            try
            {

                // ReadExcelFile(cmbtest.Text, 2);
                _rdata = GettestRow(ReadExcelFile(Sheetname, 2));

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. ";

              ///  label12.Text = " Validating " + Sheetname + "  data ................. ";
                bool haserror = false;
                bool Isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;
                for (var i = 0; i < _rdata.Count; i++)
                {
                    //foreach (Importlist.ImportTestData rd in _rdata)
                    //{
                    ListViewItem li = new ListViewItem(_rdata[i].RowNo.ToString());
                    li.SubItems.Add(_rdata[i].TestName);
                    li.SubItems.Add(_rdata[i].AreaName);

                    string str = _rdata[i].IsExist ? "Yes" : "No";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[0].Text.Trim() == _rdata[i].TestName.Trim())
                        {
                            _rdata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    errorString = "";
                    if (_rdata[i].HasError)
                    {
                        if (_rdata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rdata[i].AreaName == "")
                            errorString = errorString + " Area Name Required";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(Sheetname, _rdata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Test data, please troubleshoot and try to import again.";
                          //  successfailuremsg = successfailuremsg + errorstr;
                            return errorstr;
                        }
                        else
                        { 
                        }
                        //   label11.Text = "Please check excel to validate " + cmbtest.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rdata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        // changecolorexcelcolumnadd(cmbtest.SelectedItem.ToString(), _rdata[i].RowNo, "Test already exist", Color.Yellow);
                        // label11.Text = "Please check excel to validate " + cmbtest.SelectedItem.ToString() + " data";
                        Isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)//////////////|| Isexist
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Test data"; });
                    else
                        label12.Text = "Please check excel to validate Test data";
                   // label12.Text = "Please check excel to validate Test data";
                    successfailuremsg = successfailuremsg + label12.Text + ",";

                }
                //  label12.ForeColor = System.Drawing.Color.Red;
                if (!haserror) ////////////&& !Isexist
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + " data successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + " data successfully ................. ";
                   // label12.Text = " Validated " + Sheetname + " data successfully ................. ";
                    btnsavetest.Enabled = true;
                }
                savetest(Sheetname); errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
               // MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
        
        }
        private string validateandsaveproductdata(string Sheetname)
        {

            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a excel file";
                return errorstr;
            }
            try
            {

                DataSet ds = ReadExcelFile(Sheetname, 10);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. ";


              ///  label12.Text = " Validating " + Sheetname + "  data ................. ";
                ListView lvImport = new ListView();
                _rproductdata = GetProductRow(ds);
                bool haserror = false;
                bool isexist = false;
                string errorString;

                lvImport.BeginUpdate();
                lvImport.Items.Clear();

                //foreach (Importlist.ImportProductData rd in _rproductdata)
                //{
                for (var i = 0; i < _rproductdata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rproductdata[i].RowNo.ToString());
                    li.SubItems.Add(_rproductdata[i].ProductName);
                    li.SubItems.Add(_rproductdata[i].CategoryName);
                    li.SubItems.Add(_rproductdata[i].Serial);

                    if (_rproductdata[i].Specification != null)
                    {
                        li.SubItems.Add(_rproductdata[i].Specification.ToString());//b
                    }
                    else
                    {
                        li.SubItems.Add("");
                    }

                    li.SubItems.Add(_rproductdata[i].BasicUnit);
                    li.SubItems.Add(_rproductdata[i].minSize.ToString());//b

                    if (_rproductdata[i].RapidTest != null)
                    {
                        li.SubItems.Add(_rproductdata[i].RapidTest.ToString());//b
                    }
                    else
                    {
                        li.SubItems.Add("");
                    }

                    li.SubItems.Add(_rproductdata[i].Price.ToString());
                    li.SubItems.Add(_rproductdata[i].Packsize.ToString());
                    li.SubItems.Add(_rproductdata[i].PriceDate.ToShortDateString());

                    if (_rproductdata[i].CategoryName == string.Empty)
                        _rproductdata[i].HasError = true;

                    str = _rproductdata[i].IsExist ? "Yes" : "No";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim() == _rproductdata[i].ProductName.Trim() && Item.SubItems[2].Text.Trim() == _rproductdata[i].CategoryName.Trim())
                        {
                            _rproductdata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    //li.SubItems.Add(str);

                    if (_rproductdata[i].HasError)
                    {
                        if (_rproductdata[i].ProductName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rproductdata[i].CategoryName == "")
                            errorString = errorString + " Product Type Required";
                        if (_rproductdata[i].BasicUnit == "")
                            errorString = errorString + " Basic Unit Required";

                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(Sheetname, _rproductdata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with product data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                        //  label11.Text = "Please check excel to validate " + cmbproduct.SelectedItem.ToString() + " data";
                        haserror = true;
                    }
                    if (_rproductdata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        ////  changecolorexcelcolumnadd(cmbproduct.SelectedItem.ToString(), _rproductdata[i].RowNo, "Product already exist", Color.Yellow);
                        //  label11.Text = "Please check excel to validate " + cmbproduct.SelectedItem.ToString()  + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)      //////////// || isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Product data"; });
                    else
                        label12.Text = "Please check excel to validate Product data"; 
                   /// label12.Text = "Please check excel to validate Product data";
                    successfailuremsg = successfailuremsg + label12.Text + ",";
                }
                if (!haserror)      //////&& !isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + " data  successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + " data  successfully ................. ";

                    ///label12.Text = " Validated " + Sheetname + " data  successfully ................. ";
                    btnproduct.Enabled = true;
                }
                saveproduct(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
              ///  MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
        
        
        }
        private string validateandsaveInstrument(string Sheetname)
        {
            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a aexcel file";
                return errorstr;
            }
            try
            {


                DataSet ds = ReadExcelFile(Sheetname, 8);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. ";
          //      label12.Text = " Validating " + Sheetname + "  data ................. ";
                _rinsata = GetinsRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportInsData rd in _rinsata)
                //{
                for (var i = 0; i < _rinsata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rinsata[i].RowNo.ToString());
                    li.SubItems.Add(_rinsata[i].TestingArea != null ? _rinsata[i].TestingArea.AreaName : _rinsata[i].AreaName);
                    li.SubItems.Add(_rinsata[i].InsName);
                    li.SubItems.Add(_rinsata[i].Rate.ToString());
                    li.SubItems.Add(_rinsata[i].PerTestCtr.ToString());
                    li.SubItems.Add(_rinsata[i].DailyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].WeeklyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].MonthlyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].QuarterlyCtrTest.ToString());

                    str = _rinsata[i].IsExist ? "Yes" : "No";

                    if (!_rinsata[i].HasError)
                    {
                        foreach (ListViewItem Item in lvImport.Items)
                        {
                            if (Item.SubItems[1].Text.Trim() == _rinsata[i].TestingArea.AreaName.Trim() && Item.SubItems[2].Text.Trim() == _rinsata[i].InsName.Trim())
                            {
                                _rinsata[i].IsExist = true;
                                str = "Duplicated";
                                break;
                            }

                        }
                    }
                    li.SubItems.Add(str);

                    if (_rinsata[i].HasError)
                    {
                        if (_rinsata[i].TestingArea == null)
                            errorString = errorString + " Testing Area Required";
                        if (_rinsata[i].InsName == "")
                            errorString = errorString + " Instrument Name Required";
                        li.BackColor = Color.Red;
                        haserror = true;
                        changecolorexcelcolumnadd(Sheetname, _rinsata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Instrument data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                        //  label11.Text = "Please check excel to validate " + cmbinstrument.SelectedItem.ToString()  + " data";
                    }
                    else if (_rinsata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        // changecolorexcelcolumnadd(cmbinstrument.SelectedItem.ToString(), _rinsata[i].RowNo, "Instrument already exist", Color.Yellow);
                        //    label11.Text = "Please check excel to validate " + cmbinstrument.SelectedItem.ToString()  + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;

                if (haserror) ////// || isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Instrument data"; });
                    else
                        label12.Text = "Please check excel to validate Instrument data";
                   // label12.Text = "Please check excel to validate Instrument data";
                    successfailuremsg = successfailuremsg + label12.Text + ",";
                }
                if (!haserror) /// && !isexist
                {


                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + " data successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + " data successfully ................. ";

                 //   label12.Text = " Validated " + Sheetname + " data successfully ................. ";
                    btninstrument.Enabled = true;
                }
                saveinstrument(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr= ex.Message;
               /// MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
        
        }
        private string validateandsavetestproduct(string Sheetname)
    {

        int error = 0;
        string errorstr = "";
        if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
        {
            errorstr = "Please select a aexcel file";
            return errorstr;
        }
            try
            {
             
                DataSet ds = ReadExcelFile(Sheetname, 6);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. "; 
               /// label12.Text = " Validating " + Sheetname + "  data ................. ";
                _rtestprodata = GetProusageRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportProUsageData rd in _rtestprodata)
                //{
                for (var i = 0; i < _rtestprodata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rtestprodata[i].RowNo.ToString());

                    li.SubItems.Add(_rtestprodata[i].TestName);
                    li.SubItems.Add(_rtestprodata[i].InstrumentName);
                    li.SubItems.Add(_rtestprodata[i].ProName);
                    li.SubItems.Add(_rtestprodata[i].Rate.ToString());
                    li.SubItems.Add(_rtestprodata[i].IsForControl.ToString());
                    li.SubItems.Add(_rtestprodata[i].IsForTest.ToString());
                    str = _rtestprodata[i].IsExist ? "Yes" : "No";

                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rtestprodata[i].TestName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == _rtestprodata[i].InstrumentName.Trim().ToLower().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == _rtestprodata[i].ProName.Trim().ToLower())
                        {
                            _rtestprodata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }

                    li.SubItems.Add(str);

                    if (_rtestprodata[i].HasError)
                    {
                        if (_rtestprodata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rtestprodata[i].InstrumentName == "")
                            errorString = errorString + " Instrument Name Required";
                        if (_rtestprodata[i].ProName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rtestprodata[i].Test == null)
                            errorString = errorString + " Test Doesn't Exist";
                        if (_rtestprodata[i].Instrument == null)
                            errorString = errorString + " Instrument Doesn't Exist";
                        if (_rtestprodata[i].Product == null)
                            errorString = errorString + " Product Doesn't Exist";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(Sheetname, _rtestprodata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Product Usage data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                       // label11.Text = "Please check excel to validate " + cmbtestproduct.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rtestprodata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                      //  changecolorexcelcolumnadd(cmbtestproduct.SelectedItem.ToString(), _rtestprodata[i].RowNo, "Already exist", Color.Yellow);
                       // label11.Text = "Please check excel to validate " + cmbtestproduct.SelectedItem.ToString() + "  data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror) //////|| isexist
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Product Usage data"; });
                    else
                        label12.Text = "Please check excel to validate Product Usage data"; 

                 //   label12.Text = "Please check excel to validate Product Usage data";
                    successfailuremsg = successfailuremsg + label12.Text +  ",";
                }
                if (!haserror)       /////////////////&& !isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + "  data Successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + "  data Successfully ................. ";
                 

                    //label12.Text = " Validated " + Sheetname + "  data Successfully ................. ";
                    btntestproduct.Enabled = true;
                }
                savetestproduct(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
              //  MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
    
    }
        private string validateandsaveconsumbles(string Sheetname)
        {
            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a excel file";
                return errorstr;
            }
            try
            {

                DataSet ds = ReadExcelFile(Sheetname, 9);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. ";
                
              //  label12.Text = " Validating " + Sheetname + "  data ................. ";
                _rconsumbledata = GetconsumbleRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportConsumbleData rd in _rconsumbledata)
                //{
                for (var i = 0; i < _rconsumbledata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rconsumbledata[i].RowNo.ToString());

                    li.SubItems.Add(_rconsumbledata[i].TestName);
                    li.SubItems.Add(_rconsumbledata[i].InstrumentName);
                    li.SubItems.Add(_rconsumbledata[i].ProName);
                    li.SubItems.Add(_rconsumbledata[i].Period.ToString());
                    li.SubItems.Add(_rconsumbledata[i].NoOfTest.ToString());
                    li.SubItems.Add(_rconsumbledata[i].Rate.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForTest.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForPeriod.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForInstrument.ToString());
                    str = _rconsumbledata[i].IsExist ? "Yes" : "No";

                    //foreach (ListViewItem Item in lvImport.Items)
                    //{
                    //    if (Item.SubItems[1].Text.Trim().ToLower() == rd.TestName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == rd.InstrumentName.Trim().ToLower().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == rd.ProName.Trim().ToLower())
                    //    {
                    //        rd.IsExist = true;
                    //        str = "Duplicated";
                    //    }

                    //}

                    li.SubItems.Add(str);

                    if (_rconsumbledata[i].HasError)
                    {
                        if (_rconsumbledata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rconsumbledata[i].InstrumentName == "")
                            errorString = errorString + " Instrument Name Required";
                        if (_rconsumbledata[i].ProName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rconsumbledata[i].Cons == null)
                            errorString = errorString + " Test Doesn't Exist";
                        if (_rconsumbledata[i].Instrument == null)
                            errorString = errorString + " Instrument Doesn't Exist";
                        if (_rconsumbledata[i].Product == null)
                            errorString = errorString + " Product Doesn't Exist";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(Sheetname, _rconsumbledata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Consumables data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                        //    label11.Text = "Please check excel to validate " + cmbconsumables.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rconsumbledata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        /////   changecolorexcelcolumnadd(cmbconsumables.SelectedItem.ToString(), _rconsumbledata[i].RowNo, "Consumble already exist", Color.Yellow);
                        // label11.Text = "Please check excel to validate " + cmbconsumables.SelectedItem.ToString() + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;


                if (haserror) // || isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Consumables data";  });
                    else
                        label12.Text = "Please check excel to validate Consumables data";
                   // label12.Text = "Please check excel to validate Consumables data";
                    successfailuremsg = successfailuremsg + label12.Text + ",";
                }

                if (!haserror)        ///////&& !isexist
                {
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + "  Successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + "  Successfully ................. ";
                    //label12.Text = " Validated " + Sheetname + "  Successfully ................. ";
                    btnconsumables.Enabled = true;
                }
                saveconsumables(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
             //   MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }

        }
        private string validateandsaveregion(string Sheetname)
        {
            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a aexcel file";
                return errorstr;
            }
            try
            {

                DataSet ds = ReadExcelFile(Sheetname, 2);


                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. "; 



             //  label12.Text = " Validating " + Sheetname + "  data ................. ";
                _rregiondata = GetRegionRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.RegionReportedData rd in _rregiondata)
                //{
                for (var i = 0; i < _rregiondata.Count; i++)
                {
                    ListViewItem li = new ListViewItem(_rregiondata[i].RowNo.ToString());
                    li.SubItems.Add(_rregiondata[i].RegionName);
                    li.SubItems.Add(_rregiondata[i].ShortName);
                    string str = _rregiondata[i].IsExist ? "Yes" : "No";
                    errorString = "";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rregiondata[i].RegionName.Trim().ToLower())
                        {
                            _rregiondata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);

                    if (_rregiondata[i].HasError)
                    {
                        if (_rregiondata[i].RegionName == "")
                            errorString = errorString + " Region Name Required";
                        li.BackColor = Color.Red;
                        haserror = true;
                        changecolorexcelcolumnadd(Sheetname, _rregiondata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Region data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                        //  label11.Text = "Please check excel to validate " + cmbregion.SelectedItem.ToString()  + " data";
                    }
                    if (_rregiondata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        ////// changecolorexcelcolumnadd(cmbregion.SelectedItem.ToString(), _rregiondata[i].RowNo, "Region already exist", Color.Yellow);
                        //   label11.Text = "Please check excel to validate " + cmbregion.SelectedItem.ToString() +  " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)     //////////// || isexist
                {


                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Region data"; });
                    else
                        label12.Text = "Please check excel to validate Region data";
                  //  label12.Text = "Please check excel to validate Region data";
                    successfailuremsg = successfailuremsg + label12.Text + ",";
                }
                if (!haserror)  //// && !isexist
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + "  data successfully ................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + "  data successfully ................. "; 


                  //  label12.Text = " Validated " + Sheetname + "  data successfully ................. ";
                    btnregion.Enabled = true;
                }
                saveregion(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
               // MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
        }
        private string validateandsavesite(string Sheetname)
        {
            int error = 0;
            string errorstr = "";
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a aexcel file";
                return errorstr;
            }
            try
            {

                DataSet ds = ReadExcelFile(Sheetname, 13);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                else
                    label12.Text = " Validating " + Sheetname + "  data ................. ";
              //  label12.Text = " Validating " + Sheetname + "  data ................. ";
                _rsiteata = GetsiteRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;
                //foreach (Importlist.SiteImportData rd in _rsiteata)
                //{
                for (var i = 0; i < _rsiteata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rsiteata[i].RowNo.ToString());
                    li.SubItems.Add(_rsiteata[i].RegionName);
                    li.SubItems.Add(_rsiteata[i].CategoryName);
                    li.SubItems.Add(_rsiteata[i].SiteName);
                    //li.SubItems.Add(rd.SiteLevel);
                    li.SubItems.Add(_rsiteata[i].WorkingDays.ToString());
                    li.SubItems.Add(_rsiteata[i].Cd4Td.ToString());
                    li.SubItems.Add(_rsiteata[i].ChemTd.ToString());
                    li.SubItems.Add(_rsiteata[i].HemaTd.ToString());
                    li.SubItems.Add(_rsiteata[i].ViralTd.ToString());
                    li.SubItems.Add(_rsiteata[i].OtherTd.ToString());
                    li.SubItems.Add(_rsiteata[i].Latitude.ToString());
                    li.SubItems.Add(_rsiteata[i].Longtiude.ToString());
                    str = _rsiteata[i].OpeningDate != null ? _rsiteata[i].OpeningDate.Value.ToShortDateString() : "";
                    li.SubItems.Add(str);
                    str = _rsiteata[i].IsExist ? "Yes" : "No";


                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rsiteata[i].RegionName.Trim().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == _rsiteata[i].SiteName.Trim().ToLower())
                        {
                            _rsiteata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    if (_rsiteata[i].HasError)
                    {
                        if (_rsiteata[i].RegionName == "")
                            errorString = errorString + " Region Name Required";//14 may 14
                        if (_rsiteata[i].SiteName == "")
                            errorString = errorString + " Site Name Required";
                        if (_rsiteata[i].CategoryName == "")
                            errorString = errorString + " Category Name Required";
                        //if (rd.SiteLevel == "")
                        //    errorString = errorString + " Site Level Required";

                        if (_rsiteata[i].Region == null)
                            errorString = errorString + " Region Doesn't Exist";

                        _rsiteata[i].ErrorDescription = _rsiteata[i].ErrorDescription + errorString;
                        li.BackColor = Color.Red;
                        haserror = true;

                        changecolorexcelcolumnadd(Sheetname, _rsiteata[i].RowNo, errorString, Color.Red);
                        error++;
                        if (error > 20)
                        {
                            errorstr = "There too many problem with Site data, please troubleshoot and try to import again.";
                            return errorstr;
                        }
                        //   label11.Text = "Please check excel to validate " + cmbsite.SelectedItem.ToString() + " data";



                    }
                    if (_rsiteata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                        /// changecolorexcelcolumnadd(cmbsite.SelectedItem.ToString(), _rsiteata[i].RowNo, "Site already exist", Color.Yellow);
                        //  label11.Text = "Please check excel to validate " + cmbsite.SelectedItem.ToString() + " data";
                        isexist = true;

                    }
                    li.SubItems.Add(_rsiteata[i].ErrorDescription);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;

                if (haserror) ////// || isexist
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = "Please check excel to validate Site data"; });
                    else
                        label12.Text = "Please check excel to validate Site data";
                 //   label12.Text = "Please check excel to validate Site data";
                    successfailuremsg = successfailuremsg  + label12.Text + ",";
                }
                if (!haserror)//b jul 2 && !isexist
                {


                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validated " + Sheetname + "  data Successfully................. "; });
                    else
                        label12.Text = " Validated " + Sheetname + "  data Successfully................. ";

                 //   label12.Text = " Validated " + Sheetname + "  data Successfully................. ";
                    btnsite.Enabled = true;
                }
                savesite(Sheetname);
                errorstr = "";
                return errorstr;
            }
            catch (Exception ex)
            {
                errorstr = ex.Message;
               // MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
            }
        
        }
        private string validateandsavesiteinstrument(string Sheetname)
        {
            bool sitexist = false;
            string errorstr = "";
            int error = 0;
            bool isexist = false;
            int count = 0;
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
            {
                errorstr = "Please select a aexcel file";
                return errorstr;
            }
            if (!sitelist_new)
            {
                result = DataRepository.GetAllSite();
            }
            if (result.Count > 0)
            {
                try
                {

                    DataSet ds = ReadExcelFile(Sheetname, 6);
                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = " Validating " + Sheetname + "  data ................. "; });
                    else
                        label12.Text = " Validating " + Sheetname + "  data ................. "; 
                //    label12.Text = " Validating " + Sheetname + "  data ................. ";
                    _siteinstrumentdata = GetInDataRow(ds);
                    int[] percentage = new int[_siteinstrumentdata.Count];
                    bool haserror = false;
                    ListView lvInImport = new ListView();
                    lvInImport.BeginUpdate();
                    lvInImport.Items.Clear();
                    string errorString;

                    //foreach (Importlist.SiteInstrumentImportData rd in _siteinstrumentdata)
                    //{

                    for (var i = 0; i < _siteinstrumentdata.Count; i++)
                    {
                        string str;
                        errorString = "";
                        ListViewItem li = new ListViewItem(_siteinstrumentdata[i].RowNo.ToString());
                        li.SubItems.Add(_siteinstrumentdata[i].RegionName);
                        foreach (ForlabSite site in result)
                        {
                            if ((site.SiteName.ToLower() == _siteinstrumentdata[i].SiteName.ToLower()) && (site.Region.RegionName.ToLower() == _siteinstrumentdata[i].RegionName.ToLower()))
                            {
                                sitexist = true;
                                break;
                            }
                        }
                        if (!sitexist)
                        {
                            _siteinstrumentdata[i].HasError = true;
                            if (_siteinstrumentdata[i].RegionName == "")
                                errorString = errorString + " Region Doesn't Exist";//14 may 14
                            else
                                errorString = errorString + " Site Doesn't Exist In This Region";//14 may 14
                        }
                        sitexist = false;
                        li.SubItems.Add(_siteinstrumentdata[i].SiteName);
                        Instrument Inst = DataRepository.GetInstrumentByName(_siteinstrumentdata[i].InstrumentName);
                        ForlabSite s = DataRepository.GetSiteByName(_siteinstrumentdata[i].SiteName);

                        if (Inst != null)
                            li.SubItems.Add(Inst.TestingArea.AreaName);
                        else
                        {
                            haserror = true;
                            _siteinstrumentdata[i].HasError = true;
                            errorString = errorString + " Instrument Doesn't Exist";//14 may 14
                            li.SubItems.Add("");//?
                        }
                        count = 0;
                        foreach (ListViewItem Item in lvInImport.Items)
                        {
                            if (Item.SubItems[1].Text.Trim().ToLower() == _siteinstrumentdata[i].RegionName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == _siteinstrumentdata[i].SiteName.Trim().ToLower() && Item.SubItems[4].Text.Trim().ToLower() == _siteinstrumentdata[i].InstrumentName.Trim().ToLower())
                            {
                                _siteinstrumentdata[i].IsExist = true;
                                str = "Duplicated";
                            }
                            //if (rd.TestingArea == Item.SubItems[3].Text)
                            //{  
                            //    try
                            //    {
                            //        percentage[count] = int.Parse(Item.SubItems[6].Text);
                            //        count++;
                            //    }
                            //    catch
                            //    {
                            //    }
                            //}
                        }
                        //int sum = 0;
                        //for (int i = 0; i < count; i++)
                        //{
                        //    sum = sum + percentage[i];
                        //}
                        //if ((sum > 100 || sum < 100) && (sum!=0))
                        //    rd.HasError = true;
                        if (Inst != null && s != null)
                        {
                            foreach (SiteInstrument inst in s.SiteInstruments)
                            {
                                if (inst.Instrument == Inst)
                                {
                                    _siteinstrumentdata[i].IsExist = true;
                                }
                            }

                        }




                        li.SubItems.Add(_siteinstrumentdata[i].InstrumentName);
                        li.SubItems.Add(_siteinstrumentdata[i].Quantity.ToString());
                        li.SubItems.Add(_siteinstrumentdata[i].PecentRun.ToString());
                        str = _siteinstrumentdata[i].IsExist ? "Yes" : "No";
                        li.SubItems.Add(str);
                        if (_siteinstrumentdata[i].HasError)
                        {
                            li.BackColor = Color.Red;
                            haserror = true;
                            if (_siteinstrumentdata[i].RegionName == "")
                                errorString = errorString + " Region Name Required";//14 may 14
                            if (_siteinstrumentdata[i].SiteName == "")
                                errorString = errorString + " Site Name Required";
                            if (_siteinstrumentdata[i].InstrumentName == "")
                                errorString = errorString + " Instrument Name Required";//14 may 14
                            if (_siteinstrumentdata[i].SiteName == "")
                                errorString = errorString + " Site Name Required";

                            changecolorexcelcolumnadd(Sheetname, _siteinstrumentdata[i].RowNo, errorString, Color.Red);
                            error++;
                            if (error > 20)
                            {
                                errorstr = "There too many problem with Site Instrument data, please troubleshoot and try to import again.";
                                return errorstr;
                            }
                            // label11.Text = "Please check excel to validate " + cmbsiteinstrument.SelectedItem.ToString() + " data";
                        }
                        if (_siteinstrumentdata[i].IsExist)
                        {
                            li.BackColor = Color.Yellow;
                            ///////   changecolorexcelcolumnadd(cmbsiteinstrument.SelectedItem.ToString(), _siteinstrumentdata[i].RowNo, "Site Instrument akready exist", Color.Red);
                            //   label11.Text = "Please check excel to validate data";
                            isexist = true;
                        }
                        li.SubItems.Add(errorString);
                        lvInImport.Items.Add(li);
                    }



                    lvInImport.EndUpdate();



                    butClear.Enabled = true;
                    if (haserror)          ///////// || isexist
                    {
                      //  label12.Text = "Please check excel to validate Site Instrument data";
                        if (label12.InvokeRequired)
                            label12.Invoke((MethodInvoker)delegate { label12.Text =  "Please check excel to validate Site Instrument data"; });
                        else
                            label12.Text = "Please check excel to validate Site Instrument data";
                        successfailuremsg = successfailuremsg + label12.Text + ",";
                    }
                    if (!haserror)//b jul 9      && !isexist
                    {
                        if (label12.InvokeRequired)
                            label12.Invoke((MethodInvoker)delegate { label12.Text =  " Validated" + Sheetname + "  data Successfully ................. "; });
                        else
                            label12.Text = " Validated" + Sheetname + "  data Successfully ................. ";



                     //   label12.Text = " Validated" + Sheetname + "  data Successfully ................. ";
                        btnsiteinstrument.Enabled = true;
                    }
                    savesiteinstrument(Sheetname);
                    errorstr = "";
                    return errorstr;
                }
                catch (Exception ex)
                {
                    errorstr = ex.Message;
                   // MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return errorstr;
                }
            }
            else
            {
                errorstr = "Importing";
                // MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return errorstr;
               // MessageBox.Show("Error:Importing", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
               // return false;
            }
        }
        private void butImport_Click(object sender, EventArgs e)
        {
           // string status;
           // OleDbConnection objConn = null;
           // DataTable dt = null;
           // String connString = "provider=Microsoft.Jet.OLEDB.4.0;" +
           //     "Data Source=" + txtFilename.Text + ";Extended Properties=Excel 8.0;";
          
           // objConn = new OleDbConnection(connString);
           // if (objConn.State == ConnectionState.Closed)
           // {
           //     objConn.Open();
           // }

           // dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
           // if (objConn.State == ConnectionState.Open)
           // {
           //     objConn.Close();
           // }
           // // Set Minimum to 1 to represent the first file being copied.
           // progressBar1.Minimum = 1;
           // // Set Maximum to the total number of Users created.
           // progressBar1.Maximum = dt.Rows.Count;
           // // Set the initial value of the ProgressBar.
           // progressBar1.Value = 1;
           // // Set the Step property to a value of 1 to represent each user is being created.
           // progressBar1.Step = 1;
           // List<string> excelSheets = new List<string>();
           // foreach (DataRow row in dt.Rows)
           // {
           //     if (!row["TABLE_NAME"].ToString().Contains("_"))
           //     {
           //         row["TABLE_NAME"] = Convert.ToString(row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')).Trim('\''));
           //     }
           // }
           // DataView dv = dt.DefaultView;
           // dv.Sort = "TABLE_NAME asc";
           // dt= dv.ToTable();
           // foreach (DataRow row in dt.Rows)
           // {
           //     Application.DoEvents();
           //    // List lst = new ListViewItem();
           //     if (!row["TABLE_NAME"].ToString().Contains("_"))
           //     {
           //         //lst.ToolTipText = row["TABLE_NAME"].ToString();
           //         //lst.Text = row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')).Replace('\'', ' ');

           //         ///excelSheets.Add(row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')));
           //         if (row["TABLE_NAME"].ToString() == "Test")
           //         {
           //             status = validateandsavetestdata(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Test data, please troubleshoot and try to import again." || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //                 //label12.Text = "";
           //                 //return;
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }

           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString() == "Product")
           //         {

           //             status = validateandsaveproductdata(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with product data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString() == "Instrument")
           //         {

           //             status = validateandsaveInstrument(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Instrument data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString() == "Test Product Usage Rate")
           //         {

           //             status = validateandsavetestproduct(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Product Usage data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString() == "Consumables")
           //         {
           //             status = validateandsaveconsumbles(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Consumables data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString()== "Region")
           //         {

           //             status = validateandsaveregion(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Region data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //        else if (row["TABLE_NAME"].ToString()== "Site")
           //         {
           //             status = validateandsavesite(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Site data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
           //         else if (row["TABLE_NAME"].ToString() == "Site Instrument")
           //         {
           //             status = validateandsavesiteinstrument(row["TABLE_NAME"].ToString());
           //             if (status != "")
           //             {

           //                 MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                 if (status == "There too many problem with Site Instrument data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
           //                 {
           //                     MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
           //                     label12.Text = "";
           //                     return;
           //                 }
           //             }
           //             else
           //             {
           //                 btnExit.Enabled = true;
           //                 if (btnclick == true)
           //                 {
           //                     this.Close();
           //                 }
           //             }
           //             progressBar1.PerformStep();
           //         }
                  
           //     }
                
               
           // }
           // ////cmbtest.DataSource = excelSheets;
           // ////cmbproduct.BindingContext = new BindingContext();  
           // ////cmbproduct.DataSource = excelSheets;
           // ////cmbinstrument.BindingContext = new BindingContext(); 
           // ////cmbinstrument.DataSource = excelSheets;
           // ////cmbtestproduct.BindingContext = new BindingContext(); 
           // ////cmbtestproduct.DataSource = excelSheets;
           // ////cmbvariable.BindingContext = new BindingContext(); 
           // ////cmbvariable.DataSource = excelSheets;
           // ////cmbreferralsite.BindingContext = new BindingContext(); 
           // ////cmbreferralsite.DataSource = excelSheets;
           // ////cmbregion.BindingContext = new BindingContext(); 
           // ////cmbregion.DataSource = excelSheets;
           // ////cmbsite.BindingContext = new BindingContext(); 
           // ////cmbsite.DataSource = excelSheets;
           // ////cmbsiteinstrument.BindingContext = new BindingContext(); 
           // ////cmbsiteinstrument.DataSource = excelSheets;
           // ////cmbconsumables.BindingContext = new BindingContext(); 
           // ////cmbconsumables.DataSource = excelSheets;
           // progressBar1.Value = dt.Rows.Count;
           //// MessageBox.Show("imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
           // messagebox1 frmmsg = new messagebox1("Imported and saved successfully",successfailuremsg);
           // frmmsg.ShowDialog();
           //// this.DialogResult = System.Windows.Forms.DialogResult.OK;
           // this.Close();
           // objConn.Close();
          //  timer1.Start();
            progressBar1.Visible = true;
            progressBar1.Enabled = true;
            backgroundWorker1.RunWorkerAsync();
        }
        public DataSet ReadExcelFile(string sheetname, int noColumn)
        {
            string sheetname1 = sheetname;
            string connectionString = String.Format(@"provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=YES;""", txtFilename.Text);

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            DbDataAdapter adapter = factory.CreateDataAdapter();
            DbCommand selectCommand = factory.CreateCommand();
            sheetname = sheetname + "$";
            if (sheetname.Split(null).Length == 1)
            {

                selectCommand.CommandText = "Select * From [" + sheetname.TrimStart() + "]";
               
            }
            else
            {
                selectCommand.CommandText = "Select * From ['" + sheetname.TrimStart() + "']";
            }
            DbConnection connection = factory.CreateConnection();
            DataSet ds = new DataSet();
            bool empty;

            try
            {
                connection.ConnectionString = connectionString;
                selectCommand.Connection = connection;
                adapter.SelectCommand = selectCommand;
                adapter.Fill(ds, "Consumption");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    empty = true;

                    foreach (DataColumn col in ds.Tables[0].Columns)
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i][col].ToString().Trim()))
                            empty = false;

                    if (empty)
                        ds.Tables[0].Rows[i].Delete();
                }

                ds.Tables[0].AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }

            int colcount = ds.Tables[0].Columns.Count;

            if (noColumn > colcount)
                throw new Exception("Imported " + sheetname1 + " Sheet has less columns than needed.");
            if (noColumn < colcount)
                throw new Exception("Imported " + sheetname1 + " Sheet has too many columns.");
            if (ds.Tables[0].Rows.Count == 0)
                throw new Exception("Imported " + sheetname1 + " Sheet is empty.");
            return ds;
        }
      
        public void changecolorexcelcolumnadd(string sheetname ,int rowno,string errormsg,Color c)
        {

            try
            {
                Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();


                Microsoft.Office.Interop.Excel.Workbook workbook = application.Workbooks.Open(txtFilename.Text, Type.Missing, false);
                // Microsoft.Office.Interop.Excel.Worksheet worksheet1 = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Site Instrument"];

                Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[sheetname.TrimStart()]; //.Remove(sheetname.IndexOf('$')).Replace('\'', ' ')
                // worksheet.Columns.Insert(3);

                Microsoft.Office.Interop.Excel.Range usedRange = worksheet.UsedRange;


                Microsoft.Office.Interop.Excel.Range row = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["A" + Convert.ToString(rowno + 1) + ":C" + Convert.ToString(rowno + 1) + ""];
                Microsoft.Office.Interop.Excel.Range commentcolumn = worksheet.get_Range("C" + Convert.ToString(rowno + 1) + "");
                row.EntireRow.Font.Color = System.Drawing.ColorTranslator.ToOle(c);
                //row.EntireRow.Font.Color = System.Drawing.Color.Red;
               // row.EntireRow.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);


                commentcolumn.ClearComments();
                commentcolumn.AddComment(errormsg);


                application.DisplayAlerts = false;
                workbook.Save();

                application.Quit();
                Marshal.ReleaseComObject(worksheet);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(application);

               
            }
            catch (Exception ex)
            {
               // application.Quit();
            }
        
        }
        private IList<Importlist.ImportTestData> GettestRow(DataSet ds)
        {
            string testName;
            //string groupName;
            string areaName;
            string aName = "";
            // string gName = "";
            TestingArea testArea = null;

            int rowno = 0;
            bool haserror;

            IList<Importlist.ImportTestData> rdlist = new List<Importlist.ImportTestData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                haserror = false;
                testName = Convert.ToString(dr[0]).Trim();
                areaName = Convert.ToString(dr[1]).Trim();
                //groupName = Convert.ToString(dr[2]).Trim() ;



                Importlist.ImportTestData rd = new Importlist.ImportTestData(testName, areaName, rowno);

                if (aName != areaName)
                {
                    if (!string.IsNullOrEmpty(areaName))
                    {
                        testArea = DataRepository.GetTestingAreaByName(areaName);
                        if (testArea == null)
                        {
                            testArea = new TestingArea();
                            testArea.AreaName = areaName;
                            DataRepository.SaveOrUpdateTestingArea(testArea);
                        }
                    }
                    else
                        testArea = null;
                    aName = areaName;
                }
                rd.TestArea = testArea;

                if (testArea != null)
                {

                    if (!string.IsNullOrEmpty(testName))
                        rd.IsExist = DataRepository.GetTestByName(testName) != null;
                    else
                        haserror = true;
                }
                else
                    haserror = true;
                rd.HasError = haserror;

                rdlist.Add(rd);
            }

            return rdlist;
        }
        private IList<Importlist.ImportProductData> GetProductRow(DataSet ds)
        {
            string proName;
            string catName;
            string serial;
            string bunit;
            int psize;
            decimal price;
            string cName = "";
            ProductType category = null;
            int rowno = 0;
            bool haserror;
            ProductType defaultPt = DataRepository.GetProductTypeByName(MasterProduct.GetDefaultCategoryName);
            string rapidTest;
            string specification;
            int minSize;
            DateTime pricedate;
            if (defaultPt == null)
            {
                defaultPt = new ProductType();
                defaultPt.TypeName = MasterProduct.GetDefaultCategoryName;
                DataRepository.SaveOrUpdateProductType(defaultPt);
            }

            IList<Importlist.ImportProductData> rdlist = new List<Importlist.ImportProductData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                haserror = false;
                proName = Convert.ToString(dr[0]).Trim();
                catName = Convert.ToString(dr[1]).Trim();
                serial = Convert.ToString(dr[2]);
                bunit = Convert.ToString(dr[4]);
                category = DataRepository.GetProductTypeByName(catName);
                specification = Convert.ToString(dr[3]);
                bool testrapid = false;
                try
                {
                    if (category.UseInDemography)
                    {
                        string[] group = Enum.GetNames(typeof(TestingSpecificationGroup));

                        if (category.ClassOfTestToEnum == ClassOfMorbidityTestEnum.RapidTest)
                        {
                            for (int i = 0; i < group.Length; i++)
                            {
                                if (Convert.ToString(dr[6]) == group[i])
                                    testrapid = true;

                            }
                            if (testrapid)
                                rapidTest = Convert.ToString(dr[6]);//b
                            else
                                rapidTest = null;
                        }
                        else
                            rapidTest = null;
                    }
                    else
                        rapidTest = null;

                }
                catch
                {
                    rapidTest = null;
                }
                try
                {
                    psize = int.Parse(Convert.ToString(dr[8]));
                }
                catch
                {
                    psize = 1;
                }
                try//b
                {
                    minSize = int.Parse(Convert.ToString(dr[5]));//b
                }
                catch
                {
                    minSize = 1;
                }

                try
                {
                    price = decimal.Parse(Convert.ToString(dr[7]));
                }
                catch
                {
                    price = 1;
                }

                try
                {
                    pricedate = DateTime.Parse(Convert.ToString(dr[9]));
                }
                catch
                {
                    pricedate = DateTime.Now;
                }

                Importlist.ImportProductData rd = new Importlist.ImportProductData(proName, catName, serial, bunit, psize, price, rowno, specification, minSize, rapidTest, pricedate);

                if (cName != catName)
                {
                    if (!string.IsNullOrEmpty(catName))
                    {
                        category = DataRepository.GetProductTypeByName(catName);
                        if (category == null)
                        {
                            category = new ProductType();
                            category.TypeName = catName;
                            DataRepository.SaveOrUpdateProductType(category);
                        }
                    }
                    else
                    {
                        category = defaultPt;
                    }
                    cName = catName;
                }




                rd.Category = category;

                if (!String.IsNullOrEmpty(proName))
                {
                     duplicatename = proName;
                    rd.IsExist = DataRepository.GetProductByName(proName) != null;
                }
                else
                    haserror = true;


                rd.HasError = haserror;
                rdlist.Add(rd);
            }

            return rdlist;
        }

        IDictionary<string, ImportedTestingArea> _listOfImportedTA = new Dictionary<string, ImportedTestingArea>();
        private IList<Importlist.ImportInsData> GetinsRow(DataSet ds)
        {
            int rowno = 0;
            IList<Importlist.ImportInsData> rdlist = new List<Importlist.ImportInsData>();
            int maxThroughput, dailyTest, perTestCtrl, weeklyTest, monthlyTest, quarterlyTest;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                if (!DatarowValueToInt(dr[2], out maxThroughput))
                    maxThroughput = 1;
                if (!DatarowValueToInt(dr[3], out perTestCtrl))
                    perTestCtrl = 0;
                if (!DatarowValueToInt(dr[4], out dailyTest))
                    dailyTest = 0;
                if (!DatarowValueToInt(dr[5], out weeklyTest))
                    weeklyTest = 0;
                if (!DatarowValueToInt(dr[6], out monthlyTest))
                    monthlyTest = 0;
                if (!DatarowValueToInt(dr[7], out quarterlyTest))
                    quarterlyTest = 0;

                Importlist.ImportInsData rd = new Importlist.ImportInsData(Convert.ToString(dr[1]).Trim(), Convert.ToString(dr[0]).Trim(), maxThroughput, dailyTest, weeklyTest, monthlyTest, quarterlyTest, perTestCtrl, rowno);
                if (!string.IsNullOrEmpty(Convert.ToString(dr[0])))
                    AddImportedTestingArea(new ImportedTestingArea(Convert.ToString(dr[0]).Trim()));

                rdlist.Add(rd);
            }

           // FrmAssignTestingArea frm = new FrmAssignTestingArea(_listOfImportedTA);
           // frm.ShowDialog();

         
            
            TestingArea testArea = null;
            foreach (Importlist.ImportInsData rd in rdlist)
            {
                if (!string.IsNullOrEmpty(rd.AreaName))
                {
                  //  ImportedTestingArea ita = _listOfImportedTA[rd.AreaName.Trim()];
                    testArea = DataRepository.GetTestingAreaByName(rd.AreaName.Trim());
                    if (testArea == null)
                    {
                        testArea = new TestingArea();
                        testArea.AreaName = rd.AreaName.Trim();
                        DataRepository.SaveOrUpdateTestingArea(testArea);
                    }
                    rd.TestingArea = testArea;

                    if (!string.IsNullOrEmpty(rd.InsName))
                    {
                        rd.IsExist = DataRepository.GetInstrumentByNameAndTestingArea(rd.InsName.Trim(), rd.TestingArea.Id) != null;
                    }
                    else
                        rd.HasError = true;
                }
                else
                    rd.HasError = true;
            }

            return rdlist;
        }


        private IList<Importlist.ImportProUsageData> GetProusageRow(DataSet ds)
        {

            string testName;
            string insName;
            string proName;
            decimal rate;
            string tName = "";
            string iName = "";
            string pName = "";
            bool isForControl = false;
            bool isForTest = false;

            Test test = null;
            Instrument instrument = null;

            int rowno = 0;
            bool haserror;

            IList<Importlist.ImportProUsageData> rdlist = new List<Importlist.ImportProUsageData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                haserror = false;

                testName = Convert.ToString(dr[0]).Trim();   //region name
                insName = Convert.ToString(dr[1]).Trim();
                proName = Convert.ToString(dr[2]).Trim();
                try
                {
                    rate = decimal.Parse(Convert.ToString(dr[3]));
                }
                catch
                {
                    rate = 1;
                }
                //try { isForControl = Convert.ToBoolean(int.Parse(Convert.ToString(dr[4]))); }
                //catch { isForControl = false; }
                //try { isForTest = Convert.ToBoolean(int.Parse(Convert.ToString(dr[5]))); }
                //catch { isForTest = false; }
                int cScore = 0;
                int tScore = 0;
                string _isForControl = Convert.ToString(dr[4]);
                string _isForTest = Convert.ToString(dr[5]);
                if (_isForControl.ToLower().Trim() == "yes") cScore = 1;
                if (_isForTest.ToLower().Trim() == "yes") tScore = 1;
                //try { isForControl = Convert.ToBoolean((Convert.ToString(dr[4]))); }
                //catch { isForControl = false; }
                //try { isForTest = Convert.ToBoolean((Convert.ToString(dr[5]))); }
                //catch { isForTest = false; }


                try { isForControl = Convert.ToBoolean((cScore)); }
                catch { isForControl = false; }
                try { isForTest = Convert.ToBoolean((tScore)); }
                catch { isForTest = false; }

                Importlist.ImportProUsageData rd = new Importlist.ImportProUsageData(testName, insName, proName, rate, isForControl, isForTest, rowno);

                if (tName != testName)
                {
                    if (!string.IsNullOrEmpty(testName))
                        test = DataRepository.GetTestByName(testName);
                    else
                        test = null;
                    tName = testName;
                }

                if (test != null)
                {
                    rd.Test = test;

                    if (iName != insName)
                    {
                        if (!String.IsNullOrEmpty(insName))
                            instrument = DataRepository.GetInstrumentByName(insName);
                        else
                            instrument = null;
                        iName = insName;
                    }

                    if (instrument != null)
                    {
                        rd.Instrument = instrument;

                        if (!String.IsNullOrEmpty(proName))
                        {
                            duplicatename = proName;
                            rd.Product = DataRepository.GetProductByName(proName);
                            if (rd.Product == null)
                                haserror = true;
                            else if (test.IsExsistProductUsage(instrument.Id, rd.Product.Id))
                            {
                                rd.IsExist = true;
                            }
                        }
                        else
                            haserror = true;
                    }
                    else
                        haserror = true;
                }
                else
                    haserror = true;


                rd.HasError = haserror;
                rdlist.Add(rd);
            }

            return rdlist;
        }

        private IList<Importlist.ImportConsumbleData> GetconsumbleRow(DataSet ds)
        {

            string testName;
            string insName;
            string proName;
            string period;
            int noOfTest;
            decimal rate;
            string tName = "";
            string iName = "";
            bool isForTest = false;
            bool isForPeriod = false;
            bool isForInstrument = false;
            MasterConsumable cons = null;
            Test test = null;
            Instrument instrument = null;
            bool isnew = false;

            int rowno = 0;
            bool haserror;

            IList<Importlist.ImportConsumbleData> rdlist = new List<Importlist.ImportConsumbleData>();
            List<string> con = new List<string>();


            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                haserror = false;

                testName = Convert.ToString(dr[0]).Trim();   //region name
                insName = Convert.ToString(dr[1]).Trim();
                proName = Convert.ToString(dr[2]).Trim();
                period = Convert.ToString(dr[3]).Trim();
                try
                {
                    noOfTest = int.Parse(Convert.ToString(dr[4]));
                }
                catch
                {
                    noOfTest = 0;
                }
                try
                {
                    rate = decimal.Parse(Convert.ToString(dr[5]));
                }
                catch
                {
                    rate = 1;
                }
                //try { isForTest = Convert.ToBoolean(int.Parse(Convert.ToString(dr[6]))); }
                //catch { isForTest = false; }
                //try { isForPeriod = Convert.ToBoolean(int.Parse(Convert.ToString(dr[7]))); }
                //catch { isForPeriod = false; }
                //try { isForInstrument = Convert.ToBoolean(int.Parse(Convert.ToString(dr[8]))); }
                //catch { isForInstrument = false; }


                //try { isForTest = Convert.ToBoolean((Convert.ToString(dr[6]))); }
                //catch { isForTest = false; }
                //try { isForPeriod = Convert.ToBoolean((Convert.ToString(dr[7]))); }
                //catch { isForPeriod = false; }
                //try { isForInstrument = Convert.ToBoolean((Convert.ToString(dr[8]))); }
                //catch { isForInstrument = false; }

                int tScore = 0;
                int pScore = 0;
                int iScore = 0;
                if (Convert.ToString(dr[6]).ToLower().Trim() == "yes") tScore = 1;
                if (Convert.ToString(dr[7]).ToLower().Trim() == "yes") pScore = 1;
                if (Convert.ToString(dr[8]).ToLower().Trim() == "yes") iScore = 1;
                try { isForTest = Convert.ToBoolean((tScore)); }
                catch { isForTest = false; }
                try { isForPeriod = Convert.ToBoolean((pScore)); }
                catch { isForPeriod = false; }
                try { isForInstrument = Convert.ToBoolean((iScore)); }
                catch { isForInstrument = false; }

                Importlist.ImportConsumbleData rd = new Importlist.ImportConsumbleData(testName, insName, proName, period, noOfTest, rate, isForTest, isForPeriod, isForInstrument, rowno);

                if (!string.IsNullOrEmpty(Convert.ToString(dr[0])))
                    SaveConsumable1(Convert.ToString(dr[0]));


                if (tName != testName)
                {
                    if (!string.IsNullOrEmpty(testName))
                        test = DataRepository.GetTestByName(testName);
                    else
                        test = null;
                    tName = testName;
                }

                if (test != null)
                {
                    // rd.Test = test;
                    cons = DataRepository.GetConsumableByName(testName);

                    rd.Cons = cons;
                    if (iName != insName)
                    {
                        if (!String.IsNullOrEmpty(insName))
                            instrument = DataRepository.GetInstrumentByName(insName);
                        else
                            instrument = null;
                        iName = insName;
                    }

                    if (instrument != null)
                    {
                        rd.Instrument = instrument;

                        //if (!String.IsNullOrEmpty(proName) && !String.IsNullOrEmpty(rd.Period))
                        //{
                        //    rd.Product = DataRepository.GetProductByName(proName);
                        //    if (rd.Product == null)
                        //        haserror = true;
                        //    if (!isnew)
                        //        if (cons.IsExsistUsageRatePerInst(instrument.Id, rd.Product.Id) && rd.IsForInstrument == true)
                        //        {
                        //            rd.IsExist = true;
                        //        }
                        //}
                        if (!String.IsNullOrEmpty(proName))
                        {
                            rd.Product = DataRepository.GetProductByName(proName);
                            if (rd.Product == null)
                                haserror = true;
                            else
                            {
                                if (!isnew)
                                    if (cons.IsExsistUsageRatePerInst(instrument.Id, rd.Product.Id) && rd.IsForInstrument == true)
                                    {
                                        rd.IsExist = true;
                                    }
                            }
                        }
                        else
                            haserror = true;
                    }
                    // else
                    // haserror = true;
                    if (rd.IsForPeriod == true)
                    {
                        if (!String.IsNullOrEmpty(proName) && !String.IsNullOrEmpty(rd.Period))
                        {
                            rd.Product = DataRepository.GetProductByName(proName);
                            if (rd.Product == null)
                                haserror = true;
                            else
                            {
                                if (!isnew)
                                    if (cons.IsExsistUsageRatePerPeriod(rd.Product.Id))
                                    {
                                        rd.IsExist = true;
                                    }
                            }
                        }
                        else
                            haserror = true;
                    }
                    if (rd.IsForTest == true)
                    {
                        if (!String.IsNullOrEmpty(proName) && rd.NoOfTest > 0)
                        {
                            rd.Product = DataRepository.GetProductByName(proName);
                            if (rd.Product == null)
                                haserror = true;
                            if (!isnew)
                                if (cons.IsExsistUsageRatePerPeriod(rd.Product.Id))
                                {
                                    rd.IsExist = true;
                                }
                        }
                        else
                            haserror = true;
                    }

                }
                else
                    haserror = true;


                rd.HasError = haserror;
                rdlist.Add(rd);
                isnew = false;
            }

            return rdlist;
        }
        IDictionary<string, MasterConsumable> _listOfImportedConsumable = new Dictionary<string, MasterConsumable>();

        private IList<Importlist.RegionReportedData> GetRegionRow(DataSet ds)
        {
            string regionName;
            string shortName;
            int rowno = 0;

            IList<Importlist.RegionReportedData> rdlist = new List<Importlist.RegionReportedData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;

                regionName = Convert.ToString(dr[0]).Trim();   //region name
                shortName = Convert.ToString(dr[1]);   //short name
                Importlist.RegionReportedData rd = new Importlist.RegionReportedData(rowno, regionName, shortName);
                if (string.IsNullOrEmpty(regionName))
                    rd.HasError = true;
                else
                {
                    rd.IsExist = DataRepository.GetRegionByName(regionName) != null;
                }

                rdlist.Add(rd);
            }

            return rdlist;
        }
        private IList<Importlist.SiteImportData> GetsiteRow(DataSet ds)
        {
            string regionName;
            string categoryName;
            string siteName;
            string siteLevel;
            DateTime? openDate;
            string rName = "";
            ForlabRegion region = null;
            string cName = "";
            SiteCategory siteCategory = null;
            int workingDays;


            int Cd4Td;
            int ChemTd;
            int hemaTd;
            int ViralTd;
            int OtherTd;
            decimal lat = 0;
            decimal longitude = 0;
            int rowno = 0;
            bool haserror;
            IList<Importlist.SiteImportData> rdlist = new List<Importlist.SiteImportData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string errorDescription = "";
                rowno++;
                haserror = false;
                regionName = Convert.ToString(dr[0]).Trim();   //region name
                categoryName = Convert.ToString(dr[1]).Trim();   //site category name
                siteName = Convert.ToString(dr[2]).Trim();   //short name
                siteLevel = Convert.ToString(dr[3]);//Site Level 
                try
                {
                    workingDays = int.Parse(Convert.ToString(dr[4]));
                    if (workingDays > 31)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " Working Days Can't Be More Than 31 Days";
                    }
                }
                catch
                {
                    workingDays = 22;
                }
                try
                {
                    Cd4Td = int.Parse(Convert.ToString(dr[5]));
                    if (Cd4Td > 31 || Cd4Td > workingDays)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " CD4 Testing Days Can't Be More Than 31 Days or Working Days";//14 may 14
                    }
                }
                catch
                {
                    Cd4Td = 0;
                }
                try
                {
                    ChemTd = int.Parse(Convert.ToString(dr[6]));
                    if (ChemTd > 31 || ChemTd > workingDays)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " Chemistry Testing Days Can't Be More Than 31 Days or Working Days";
                    }
                }
                catch
                {
                    ChemTd = 0;
                }
                try
                {
                    hemaTd = int.Parse(Convert.ToString(dr[7]));
                    if (hemaTd > 31 || hemaTd > workingDays)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " Hematology Testing Days Can't Be More Than 31 Days or Working Days";
                    }
                }
                catch
                {
                    hemaTd = 0;
                }
                try
                {
                    ViralTd = int.Parse(Convert.ToString(dr[8]));
                    if (ViralTd > 31 || ViralTd > workingDays)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " ViralLoad Testing Days Can't Be More Than 31 Days or Working Days";
                    }
                }
                catch
                {
                    ViralTd = 0;
                }
                try
                {
                    OtherTd = int.Parse(Convert.ToString(dr[9]));
                    if (OtherTd > 31 || OtherTd > workingDays)
                    {
                        haserror = true;
                        errorDescription = errorDescription + " Testing Days Can't Be More Than 31 Days or Working Days";
                    }
                }
                catch
                {
                    OtherTd = 0;
                }


                try
                {
                    openDate = Convert.ToDateTime(dr[10]);
                }
                catch
                {
                    openDate = null;
                }

                lat = Convert.ToDecimal(dr[11]);//lat;
                longitude = Convert.ToDecimal(dr[12]);//long

                Importlist.SiteImportData rd = new Importlist.SiteImportData(rowno, regionName, categoryName, siteName, siteLevel, workingDays, Cd4Td, ChemTd, hemaTd, ViralTd, OtherTd, lat, longitude, openDate);

                if (rName != regionName)
                {
                    if (!string.IsNullOrEmpty(regionName))
                        region = DataRepository.GetRegionByName(regionName);
                    else
                        region = null;
                    rName = regionName;
                }

                if (region != null)
                {
                    rd.Region = region;
                    if (!String.IsNullOrEmpty(siteName))
                        rd.IsExist = DataRepository.GetSiteByName(siteName, region.Id) != null;
                    else
                        haserror = true;
                }
                else
                    haserror = true;


                if (!string.IsNullOrEmpty(categoryName))
                {
                    siteCategory = DataRepository.GetSiteCategoryByName(categoryName);
                    if (siteCategory == null)
                    {
                        siteCategory = new SiteCategory();
                        siteCategory.CategoryName = categoryName;
                        DataRepository.SaveOrUpdateSiteCategory(siteCategory);
                    }
                }
                else
                    haserror = true;
                cName = categoryName;



                //if (siteLevel != "")//14 may 14 null)//b
                //{
                //    string[] sitelevel = Enum.GetNames(typeof(SiteLevelEnum));
                //    string sl = "";
                //    bool level = false;
                //    for (int i = 0; i < sitelevel.Length; i++)
                //    {
                //        sl = sitelevel[i].Replace('_', ' ');
                //        if (siteLevel == sl)
                //        {
                //            rd.SiteLevel = siteLevel;
                //            level = true;
                //            break;
                //        }

                //    }
                //    if (!level)
                //    {
                //        haserror = true;
                //        errorDescription = errorDescription + " Is Not Valid Site Level";
                //    }

                //}
                //else
                //    haserror = true;
                rd.Category = siteCategory;
                rd.Cd4Td = Cd4Td;
                rd.ChemTd = ChemTd;
                rd.HemaTd = hemaTd;
                rd.ViralTd = ViralTd;
                rd.OtherTd = OtherTd;

                rd.HasError = haserror;
                rd.ErrorDescription = errorDescription;//14 may 14
                rdlist.Add(rd);

            }

            return rdlist;
        }


        private IList<Importlist.SiteInstrumentImportData> GetInDataRow(DataSet ds)
        {
            string regionName;
            string testingArea;
            string siteName;
            string instrumentName;
            int quantity;
            int percentRun;

            int rowno = 0;
            bool haserror;



            IList<ForlabSite> _referingSites = new List<ForlabSite>();
            IList<ForlabSite> _validsites = new List<ForlabSite>();

            IList<Importlist.SiteInstrumentImportData> rdlist = new List<Importlist.SiteInstrumentImportData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                haserror = false;
                regionName = Convert.ToString(dr[0]).Trim();   //region name

                siteName = Convert.ToString(dr[1]).Trim();
                testingArea = Convert.ToString(dr[2]).Trim();
                instrumentName = Convert.ToString(dr[3]).Trim();
                try
                {
                    quantity = int.Parse(Convert.ToString(dr[4]));
                }
                catch
                { quantity = 1; }
                try
                {
                    percentRun = int.Parse(Convert.ToString(dr[5]));
                }
                catch { percentRun = 100; }
                Importlist.SiteInstrumentImportData rd = new Importlist.SiteInstrumentImportData(rowno, regionName, siteName, testingArea, instrumentName, quantity, percentRun);

                rd.HasError = haserror;

                rdlist.Add(rd);
            }

            return rdlist;
        }

        private IList<Importlist.RefSiteImportData> GetrefsiteRow(DataSet ds)
        {
            string regionName;
            string siteName;
            string rName = "";
            ForlabRegion region = null;
            ForlabSite site = null;

            string CD4RefSite;
            string ChemRefSite;
            string HemRefSite;
            string ViralRefSite;
            string OtherRefSite;

            int rowno = 0;
            bool haserror;

            ForlabSite refSite = null;
            int CD4RefSiteId = 0;
            int ChemRefSiteId = 0;
            int HemRefSiteId = 0;
            int ViralRefSiteId = 0;
            int OtherRefSiteId = 0;

            IList<ForlabSite> _referingSites = new List<ForlabSite>();
            IList<ForlabSite> sites;
            IList<ForlabSite> _validsites = new List<ForlabSite>();

            IList<Importlist.RefSiteImportData> rdlist = new List<Importlist.RefSiteImportData>();

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string errorDiscription = "";
                rowno++;
                haserror = false;
                regionName = Convert.ToString(dr[0]).Trim();   //region namegory name
                siteName = Convert.ToString(dr[1]).Trim();   //short name
                try
                {
                    CD4RefSite = Convert.ToString(dr[2]);

                }
                catch
                {
                    CD4RefSite = "";
                }
                try
                {
                    ChemRefSite = Convert.ToString(dr[3]);

                }
                catch
                {
                    ChemRefSite = "";
                }
                try
                {
                    HemRefSite = Convert.ToString(dr[4]);

                }
                catch
                {
                    HemRefSite = "";
                }
                try
                {
                    ViralRefSite = Convert.ToString(dr[5]);

                }
                catch
                {
                    ViralRefSite = "";
                }
                try
                {
                    OtherRefSite = Convert.ToString(dr[6]);

                }
                catch
                {
                    OtherRefSite = "";
                }

                Importlist.RefSiteImportData rd = new Importlist.RefSiteImportData(rowno, regionName, siteName, CD4RefSite, ChemRefSite, HemRefSite, ViralRefSite, OtherRefSite);

                if (rName != regionName)
                {
                    if (!string.IsNullOrEmpty(regionName))
                        region = DataRepository.GetRegionByName(regionName);
                    else
                        region = null;
                    rName = regionName;
                }

                if (region != null)
                {
                    rd.Region = region;
                    if (!String.IsNullOrEmpty(siteName))
                    {
                        site = DataRepository.GetSiteByName(siteName, region.Id);
                        if (site != null)
                        {
                            //if((site.CD4RefSite.ToString().Trim()==(dr[2]).ToString().Trim().ToLower())||(site.ChemistryRefSite.ToString().Trim().ToLower()==(dr[3]).ToString().Trim().ToLower())||
                            //    (site.HematologyRefSite.ToString().Trim().ToLower()==(dr[4]).ToString().Trim().ToLower())||(site.ViralLoadRefSite.ToString().Trim().ToLower()==(dr[5]).ToString().Trim().ToLower())||
                            //    (site.OtherRefSite.ToString().Trim().ToLower()==(dr[6]).ToString().Trim().ToLower()))
                            //    rd.IsExist=true;
                        }
                        else
                        {
                            errorDiscription = errorDiscription + " Site doesn't exist";
                            haserror = true;
                        }

                    }
                    else
                    {
                        errorDiscription = errorDiscription + " Site name required";
                        haserror = true;
                    }
                    if (!haserror)
                    {
                        if (!String.IsNullOrEmpty(CD4RefSite))
                            if (ISValidReferralSite(CD4RefSite, siteName, "CD4", out refSite))
                                CD4RefSiteId = refSite.Id;
                            else
                            {
                                haserror = true;
                                CD4RefSiteId = -1;
                                errorDiscription = errorDiscription + " Is Not Valid CD4 Referral Site";//14 may 14
                            }

                        if (!String.IsNullOrEmpty(ChemRefSite))
                            if (ISValidReferralSite(ChemRefSite, siteName, "Chemistry", out refSite))
                                ChemRefSiteId = refSite.Id;
                            else
                            {
                                haserror = true;
                                ChemRefSiteId = -1;
                                errorDiscription = errorDiscription + " Is Not Valid Chemistry Referral Site";
                            }

                        if (!String.IsNullOrEmpty(HemRefSite))
                            if (ISValidReferralSite(HemRefSite, siteName, "Hematology", out refSite))
                                HemRefSiteId = refSite.Id;
                            else
                            {
                                haserror = true;
                                HemRefSiteId = -1;
                                errorDiscription = errorDiscription + " Is Not Valid Hematology Referral Site";
                            }

                        if (!String.IsNullOrEmpty(ViralRefSite))
                            if (ISValidReferralSite(ViralRefSite, siteName, "ViralLoad", out refSite))
                                ViralRefSiteId = refSite.Id;
                            else
                            {
                                haserror = true;
                                ViralRefSiteId = -1;
                                errorDiscription = errorDiscription + " Is Not Valid ViralLoad Referral Site";
                            }

                        if (!String.IsNullOrEmpty(OtherRefSite))
                            if (ISValidReferralSite(OtherRefSite, siteName, "Other", out refSite))
                                OtherRefSiteId = refSite.Id;
                            else
                            {
                                haserror = true;
                                OtherRefSiteId = -1;
                                errorDiscription = errorDiscription + " Is Not Valid Referral Site";
                            }
                    }
                    else
                        haserror = true;
                }
                else
                {
                    errorDiscription = errorDiscription + " Region doesn't exist";
                    haserror = true;
                }


                rd.CD4RefSiteId = CD4RefSiteId;
                rd.ChemistryRefSiteId = ChemRefSiteId;
                rd.HematologyRefSiteId = HemRefSiteId;
                rd.ViralLoadRefSiteId = ViralRefSiteId;
                rd.OtheRefSiteId = OtherRefSiteId;
                rd.HasError = haserror;
                if (rd.CD4RefSiteId == 0 && rd.ChemistryRefSiteId == 0 && rd.HematologyRefSiteId == 0 && rd.ViralLoadRefSiteId == 0 && rd.OtheRefSiteId == 0)
                {
                    rd.HasError = true;
                    errorDiscription = errorDiscription + " At least one referral site required";
                }

                rd.ErrorDescription = errorDiscription;//14 may 14
                rdlist.Add(rd);
                CD4RefSiteId = 0;
                ChemRefSiteId = 0;
                HemRefSiteId = 0;
                ViralRefSiteId = 0;
                OtherRefSiteId = 0;
            }

            return rdlist;
        }

        private bool DatarowValueToDouble(object drvalue, out double result)
        {
            return double.TryParse(Convert.ToString(drvalue), out result);
        }

        IDictionary<int, QuantifyMenu> _modifiedQuantifyMenus;
        private void AddModifiedQMenu(QuantifyMenu qmenu)
        {
            if (!_modifiedQuantifyMenus.ContainsKey(qmenu.Id))
                _modifiedQuantifyMenus.Add(qmenu.Id, qmenu);
        }

        private QuantifyMenu GetQuantifyMenuByTitle(string quaTitle)
        {
            if (_listofQmenus == null)
            {
                _listofQmenus = DataRepository.GetAllQuantifyMenus();
            }
            string title = "";
            foreach (QuantifyMenu qm in _listofQmenus)
            {
                title = qm.Title.Replace('_', ' ').Trim();
                if (title.Equals(quaTitle, StringComparison.OrdinalIgnoreCase))
                    return qm;
            }
            return null;
        }
        private IList<QuantifyMenu> _listofQmenus;

        private IList<Importlist.ImportQVariableData> GetvariableaRow(DataSet ds)
        {
            DataRepository.CloseSession();

            int rowno = 0;
            IList<Importlist.ImportQVariableData> rdlist = new List<Importlist.ImportQVariableData>();
            double usagerate;
            _modifiedQuantifyMenus = new Dictionary<int, QuantifyMenu>();
            _listofQmenus = null;

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                rowno++;
                if (!DatarowValueToDouble(dr[1], out usagerate))
                    usagerate = 1;

                Importlist.ImportQVariableData rd = new Importlist.ImportQVariableData(Convert.ToString(dr[0]), usagerate, Convert.ToString(dr[2]), Convert.ToString(dr[3]), rowno);
                rdlist.Add(rd);
            }

            foreach (Importlist.ImportQVariableData rd in rdlist)
            {
                if (!string.IsNullOrEmpty(rd.ProductName))
                {
                    rd.Product = DataRepository.GetProductByName(rd.ProductName);
                    if (rd.Product != null)
                    {
                        if (!string.IsNullOrEmpty(rd.QuantifyMenu))
                        {
                            QuantifyMenu qm = GetQuantifyMenuByTitle(rd.QuantifyMenu);
                            if (qm != null)
                            {
                                if (qm.IsProductSelected(rd.Product.Id))
                                {
                                    rd.IsExist = true;
                                    rd.ErrorDescription = "Already this quantification variable is existed";
                                }
                                else
                                {
                                    QuantificationMetric qmetric = new QuantificationMetric();
                                    qmetric.ClassOfTest = qm.ClassOfTest;
                                    qmetric.CollectionSupplieAppliedTo = rd.AppliedTo;
                                    qmetric.Product = rd.Product;
                                    qmetric.QuantifyMenu = qm;
                                    qmetric.UsageRate = rd.UsageRate;

                                    qm.QuantificationMetrics.Add(qmetric);
                                    AddModifiedQMenu(qm);
                                }
                            }
                            else
                            {
                                rd.HasError = true;
                                rd.ErrorDescription = "Error: unable to found Quantification variable";
                            }
                        }
                        else
                        {
                            rd.HasError = true;
                            rd.ErrorDescription = "Error: Quantify according to is empty";
                        }

                    }
                    else
                    {
                        rd.HasError = true; ;
                        rd.ErrorDescription = "Error: unable to found a Product";
                    }
                }
                else
                {
                    rd.HasError = true;
                    rd.ErrorDescription = "Error: Product name is empty";
                }
            }

            return rdlist;
        }

        private void SaveConsumable1(string impCons)
        {
            Test impTest = DataRepository.GetTestByName(impCons);
            if (!_listOfImportedConsumable.ContainsKey(impTest.TestName))
            {
                if (DataRepository.GetConsumableByName(impTest.TestName) == null)
                {
                    MasterConsumable con = new MasterConsumable();
                    con.Test = impTest;
                    con.TestingArea = impTest.TestingArea;
                    DataRepository.SaveOrUpdateConsumables(con);
                }
            }
        }

        private bool ISValidReferralSite(string Referralsite, string siten, string platform, out ForlabSite refSite)
        {

            IList<ForlabSite> _referingSites = new List<ForlabSite>();

            IList<ForlabSite> _validsites = new List<ForlabSite>();

            refSite = null;
            bool isvalid = false;
            if (!String.IsNullOrEmpty(Referralsite))
            {
                refSite = DataRepository.GetSiteByName(Referralsite);
                if (refSite != null)
                {

                    if (refSite.SiteName != siten)
                    {
                        _validsites = DataRepository.GetAllSiteByRegionandPlatform(-1, platform);
                        _referingSites = DataRepository.GetReferingSiteByPlatform(platform);
                        if (_validsites.Contains(refSite) && !_referingSites.Contains(refSite))
                            isvalid = true;
                        else
                            isvalid = false;
                    }
                    else
                        isvalid = false;
                }
                else
                {
                    isvalid = false;
                }
            }
            else
            {
                isvalid = false;
            }

            return isvalid;
        }


        private bool DatarowValueToInt(object drvalue, out int result)
        {
            return Int32.TryParse(Convert.ToString(drvalue), out result);
        }
        private void cmbtest_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
           
                // ReadExcelFile(cmbtest.Text, 2);
                _rdata = GettestRow(ReadExcelFile(cmbtest.SelectedItem.ToString(), 2));
                label12.Text = " Validating " + cmbtest.SelectedItem.ToString() + "  data ................. ";
                bool haserror = false;
                bool Isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;
                for (var i = 0; i < _rdata.Count; i++) {
                //foreach (Importlist.ImportTestData rd in _rdata)
                //{
                    ListViewItem li = new ListViewItem(_rdata[i].RowNo.ToString());
                    li.SubItems.Add(_rdata[i].TestName);
                    li.SubItems.Add(_rdata[i].AreaName);

                    string str = _rdata[i].IsExist ? "Yes" : "No";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[0].Text.Trim() == _rdata[i].TestName.Trim())
                        {
                            _rdata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    errorString = "";
                    if (_rdata[i].HasError)
                    {
                        if (_rdata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rdata[i].AreaName == "")
                            errorString = errorString + " Area Name Required";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbtest.SelectedItem.ToString(), _rdata[i].RowNo, errorString, Color.Red);
                     //   label11.Text = "Please check excel to validate " + cmbtest.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rdata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                       // changecolorexcelcolumnadd(cmbtest.SelectedItem.ToString(), _rdata[i].RowNo, "Test already exist", Color.Yellow);
                       // label11.Text = "Please check excel to validate " + cmbtest.SelectedItem.ToString() + " data";
                        Isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)//////////////|| Isexist
                    label12.Text = "Please check excel to validate " + cmbtest.SelectedItem.ToString() + " data";
                 //  label12.ForeColor = System.Drawing.Color.Red;
                if (!haserror) ////////////&& !Isexist
                {
                    label12.Text = " Validated " + cmbtest.SelectedItem.ToString() + " data successfully ................. ";
                    btnsavetest.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbproduct_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
              
                DataSet ds = ReadExcelFile(cmbproduct.SelectedItem.ToString(), 10);
                label12.Text = " Validating " + cmbproduct.SelectedItem.ToString() + "  data ................. ";
                ListView lvImport = new ListView();
                _rproductdata = GetProductRow(ds);
                bool haserror = false;
                bool isexist = false;
                string errorString;

                lvImport.BeginUpdate();
                lvImport.Items.Clear();

                //foreach (Importlist.ImportProductData rd in _rproductdata)
                //{
                      for (var i = 0; i < _rproductdata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rproductdata[i].RowNo.ToString());
                    li.SubItems.Add(_rproductdata[i].ProductName);
                    li.SubItems.Add(_rproductdata[i].CategoryName);
                    li.SubItems.Add(_rproductdata[i].Serial);

                    if (_rproductdata[i].Specification != null)
                    {
                        li.SubItems.Add(_rproductdata[i].Specification.ToString());//b
                    }
                    else
                    {
                        li.SubItems.Add("");
                    }

                    li.SubItems.Add(_rproductdata[i].BasicUnit);
                    li.SubItems.Add(_rproductdata[i].minSize.ToString());//b

                    if (_rproductdata[i].RapidTest != null)
                    {
                        li.SubItems.Add(_rproductdata[i].RapidTest.ToString());//b
                    }
                    else
                    {
                        li.SubItems.Add("");
                    }

                    li.SubItems.Add(_rproductdata[i].Price.ToString());
                    li.SubItems.Add(_rproductdata[i].Packsize.ToString());
                    li.SubItems.Add(_rproductdata[i].PriceDate.ToShortDateString());

                    if (_rproductdata[i].CategoryName == string.Empty)
                        _rproductdata[i].HasError = true;

                    str = _rproductdata[i].IsExist ? "Yes" : "No";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim() == _rproductdata[i].ProductName.Trim() && Item.SubItems[2].Text.Trim() == _rproductdata[i].CategoryName.Trim())
                        {
                            _rproductdata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    //li.SubItems.Add(str);

                    if (_rproductdata[i].HasError)
                    {
                        if (_rproductdata[i].ProductName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rproductdata[i].CategoryName == "")
                            errorString = errorString + " Product Type Required";
                        if (_rproductdata[i].BasicUnit == "")
                            errorString = errorString + " Basic Unit Required";

                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbproduct.SelectedItem.ToString(), _rproductdata[i].RowNo, errorString, Color.Red);
                      //  label11.Text = "Please check excel to validate " + cmbproduct.SelectedItem.ToString() + " data";
                        haserror = true;
                    }
                    if (_rproductdata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                      ////  changecolorexcelcolumnadd(cmbproduct.SelectedItem.ToString(), _rproductdata[i].RowNo, "Product already exist", Color.Yellow);
                      //  label11.Text = "Please check excel to validate " + cmbproduct.SelectedItem.ToString()  + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)      //////////// || isexist
                    label12.Text = "Please check excel to validate " + cmbproduct.SelectedItem.ToString() + " data";
                if (!haserror)      //////&& !isexist
                {
                    label12.Text = " Validated " + cmbproduct.SelectedItem.ToString() + " data  successfully ................. ";
                    btnproduct.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbinstrument_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {

              
                DataSet ds = ReadExcelFile(cmbinstrument.SelectedItem.ToString(), 8);
                label12.Text = " Validating " + cmbinstrument.SelectedItem.ToString() + "  data ................. ";
                _rinsata = GetinsRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportInsData rd in _rinsata)
                //{
                for (var i = 0; i < _rinsata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rinsata[i].RowNo.ToString());
                    li.SubItems.Add(_rinsata[i].TestingArea != null ? _rinsata[i].TestingArea.AreaName : _rinsata[i].AreaName);
                    li.SubItems.Add(_rinsata[i].InsName);
                    li.SubItems.Add(_rinsata[i].Rate.ToString());
                    li.SubItems.Add(_rinsata[i].PerTestCtr.ToString());
                    li.SubItems.Add(_rinsata[i].DailyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].WeeklyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].MonthlyCtrTest.ToString());
                    li.SubItems.Add(_rinsata[i].QuarterlyCtrTest.ToString());

                    str = _rinsata[i].IsExist ? "Yes" : "No";

                    if (!_rinsata[i].HasError)
                    {
                        foreach (ListViewItem Item in lvImport.Items)
                        {
                            if (Item.SubItems[1].Text.Trim() == _rinsata[i].TestingArea.AreaName.Trim() && Item.SubItems[2].Text.Trim() == _rinsata[i].InsName.Trim())
                            {
                                _rinsata[i].IsExist = true;
                                str = "Duplicated";
                                break;
                            }

                        }
                    }
                    li.SubItems.Add(str);

                    if (_rinsata[i].HasError)
                    {
                        if (_rinsata[i].TestingArea == null)
                            errorString = errorString + " Testing Area Required";
                        if (_rinsata[i].InsName == "")
                            errorString = errorString + " Instrument Name Required";
                        li.BackColor = Color.Red;
                        haserror = true;
                        changecolorexcelcolumnadd(cmbinstrument.SelectedItem.ToString(), _rinsata[i].RowNo, errorString, Color.Red);
                      //  label11.Text = "Please check excel to validate " + cmbinstrument.SelectedItem.ToString()  + " data";
                    }
                    else if (_rinsata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                       // changecolorexcelcolumnadd(cmbinstrument.SelectedItem.ToString(), _rinsata[i].RowNo, "Instrument already exist", Color.Yellow);
                    //    label11.Text = "Please check excel to validate " + cmbinstrument.SelectedItem.ToString()  + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;

                if (haserror) ////// || isexist
                    label12.Text = "Please check excel to validate " + cmbinstrument.SelectedItem.ToString() + " data";
                if (!haserror) /// && !isexist
                {
                    label12.Text = " Validated " + cmbinstrument.SelectedItem.ToString() + " data successfully ................. ";
                    btninstrument.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddImportedTestingArea(ImportedTestingArea impTa)
        {
            if (!_listOfImportedTA.ContainsKey(impTa.AreaName))
                _listOfImportedTA.Add(impTa.AreaName, impTa);
        }

        private void cmbtestproduct_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
             
                DataSet ds = ReadExcelFile(cmbtestproduct.SelectedItem.ToString(), 6);
                label12.Text = " Validating " + cmbtestproduct.SelectedItem.ToString() + "  data ................. ";
                _rtestprodata = GetProusageRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportProUsageData rd in _rtestprodata)
                //{
                for (var i = 0; i < _rtestprodata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rtestprodata[i].RowNo.ToString());

                    li.SubItems.Add(_rtestprodata[i].TestName);
                    li.SubItems.Add(_rtestprodata[i].InstrumentName);
                    li.SubItems.Add(_rtestprodata[i].ProName);
                    li.SubItems.Add(_rtestprodata[i].Rate.ToString());
                    li.SubItems.Add(_rtestprodata[i].IsForControl.ToString());
                    li.SubItems.Add(_rtestprodata[i].IsForTest.ToString());
                    str = _rtestprodata[i].IsExist ? "Yes" : "No";

                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rtestprodata[i].TestName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == _rtestprodata[i].InstrumentName.Trim().ToLower().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == _rtestprodata[i].ProName.Trim().ToLower())
                        {
                            _rtestprodata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }

                    li.SubItems.Add(str);

                    if (_rtestprodata[i].HasError)
                    {
                        if (_rtestprodata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rtestprodata[i].InstrumentName == "")
                            errorString = errorString + " Instrument Name Required";
                        if (_rtestprodata[i].ProName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rtestprodata[i].Test == null)
                            errorString = errorString + " Test Doesn't Exist";
                        if (_rtestprodata[i].Instrument == null)
                            errorString = errorString + " Instrument Doesn't Exist";
                        if (_rtestprodata[i].Product == null)
                            errorString = errorString + " Product Doesn't Exist";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbtestproduct.SelectedItem.ToString(), _rtestprodata[i].RowNo, errorString, Color.Red);
                       // label11.Text = "Please check excel to validate " + cmbtestproduct.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rtestprodata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                      //  changecolorexcelcolumnadd(cmbtestproduct.SelectedItem.ToString(), _rtestprodata[i].RowNo, "Already exist", Color.Yellow);
                       // label11.Text = "Please check excel to validate " + cmbtestproduct.SelectedItem.ToString() + "  data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror) //////|| isexist
                    label12.Text = "Please check excel to validate " + cmbtestproduct.SelectedItem.ToString() + " data";
                if (!haserror)       /////////////////&& !isexist
                {
                    label12.Text = " Validated " + cmbtestproduct.SelectedItem.ToString() + "  data Successfully ................. ";
                    btntestproduct.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }       

        private void cmbconsumables_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
             
                DataSet ds = ReadExcelFile(cmbconsumables.SelectedItem.ToString(), 9);
                label12.Text = " Validating " + cmbconsumables.SelectedItem.ToString() + "  data ................. ";
                _rconsumbledata = GetconsumbleRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.ImportConsumbleData rd in _rconsumbledata)
                //{
  for (var i = 0; i < _rconsumbledata.Count; i++) {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rconsumbledata[i].RowNo.ToString());

                    li.SubItems.Add(_rconsumbledata[i].TestName);
                    li.SubItems.Add(_rconsumbledata[i].InstrumentName);
                    li.SubItems.Add(_rconsumbledata[i].ProName);
                    li.SubItems.Add(_rconsumbledata[i].Period.ToString());
                    li.SubItems.Add(_rconsumbledata[i].NoOfTest.ToString());
                    li.SubItems.Add(_rconsumbledata[i].Rate.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForTest.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForPeriod.ToString());
                    li.SubItems.Add(_rconsumbledata[i].IsForInstrument.ToString());
                    str = _rconsumbledata[i].IsExist ? "Yes" : "No";

                    //foreach (ListViewItem Item in lvImport.Items)
                    //{
                    //    if (Item.SubItems[1].Text.Trim().ToLower() == rd.TestName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == rd.InstrumentName.Trim().ToLower().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == rd.ProName.Trim().ToLower())
                    //    {
                    //        rd.IsExist = true;
                    //        str = "Duplicated";
                    //    }

                    //}

                    li.SubItems.Add(str);

                    if (_rconsumbledata[i].HasError)
                    {
                        if (_rconsumbledata[i].TestName == "")
                            errorString = errorString + " Test Name Required";
                        if (_rconsumbledata[i].InstrumentName == "")
                            errorString = errorString + " Instrument Name Required";
                        if (_rconsumbledata[i].ProName == "")
                            errorString = errorString + " Product Name Required";
                        if (_rconsumbledata[i].Cons == null)
                            errorString = errorString + " Test Doesn't Exist";
                        if (_rconsumbledata[i].Instrument == null)
                            errorString = errorString + " Instrument Doesn't Exist";
                        if (_rconsumbledata[i].Product == null)
                            errorString = errorString + " Product Doesn't Exist";


                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbconsumables.SelectedItem.ToString(), _rconsumbledata[i].RowNo, errorString, Color.Red);
                    //    label11.Text = "Please check excel to validate " + cmbconsumables.SelectedItem.ToString()  + " data";
                        haserror = true;
                    }
                    if (_rconsumbledata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                     /////   changecolorexcelcolumnadd(cmbconsumables.SelectedItem.ToString(), _rconsumbledata[i].RowNo, "Consumble already exist", Color.Yellow);
                       // label11.Text = "Please check excel to validate " + cmbconsumables.SelectedItem.ToString() + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;


                if (haserror) // || isexist
                    label12.Text = "Please check excel to validate " + cmbconsumables.SelectedItem.ToString() + " data";
                if (!haserror)        ///////&& !isexist
                {
                    label12.Text = " Validated " + cmbconsumables.SelectedItem.ToString() + "  Successfully ................. ";
                    btnconsumables.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbregion_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
               
                DataSet ds = ReadExcelFile(cmbregion.SelectedItem.ToString(), 2);
                label12.Text = " Validating " + cmbregion.SelectedItem.ToString() + "  data ................. ";
                _rregiondata = GetRegionRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;

                //foreach (Importlist.RegionReportedData rd in _rregiondata)
                //{
                       for (var i = 0; i < _rregiondata.Count; i++)
                {
                    ListViewItem li = new ListViewItem(_rregiondata[i].RowNo.ToString());
                    li.SubItems.Add(_rregiondata[i].RegionName);
                    li.SubItems.Add(_rregiondata[i].ShortName);
                    string str = _rregiondata[i].IsExist ? "Yes" : "No";
                    errorString = "";
                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rregiondata[i].RegionName.Trim().ToLower())
                        {
                            _rregiondata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);

                    if (_rregiondata[i].HasError)
                    {
                        if (_rregiondata[i].RegionName == "")
                            errorString = errorString + " Region Name Required";
                        li.BackColor = Color.Red;
                        haserror = true;
                        changecolorexcelcolumnadd(cmbregion.SelectedItem.ToString(), _rregiondata[i].RowNo, errorString, Color.Red);
                      //  label11.Text = "Please check excel to validate " + cmbregion.SelectedItem.ToString()  + " data";
                    }
                    if (_rregiondata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                       ////// changecolorexcelcolumnadd(cmbregion.SelectedItem.ToString(), _rregiondata[i].RowNo, "Region already exist", Color.Yellow);
                     //   label11.Text = "Please check excel to validate " + cmbregion.SelectedItem.ToString() +  " data";
                        isexist = true;
                    }
                    li.SubItems.Add(errorString);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)     //////////// || isexist
                    label12.Text = "Please check excel to validate " + cmbregion.SelectedItem.ToString() + " data";
                if (!haserror)  //// && !isexist
                {
                    label12.Text = " Validated " + cmbregion.SelectedItem.ToString() + "  data successfully ................. ";
                    btnregion.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbsite_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
               
                DataSet ds = ReadExcelFile(cmbsite.SelectedItem.ToString(), 13);
                label12.Text = " Validating " + cmbsite.SelectedItem.ToString() + "  data ................. ";
                _rsiteata = GetsiteRow(ds);
                bool haserror = false;
                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;
                //foreach (Importlist.SiteImportData rd in _rsiteata)
                //{
                                      for (var i = 0; i < _rsiteata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rsiteata[i].RowNo.ToString());
                    li.SubItems.Add(_rsiteata[i].RegionName);
                    li.SubItems.Add(_rsiteata[i].CategoryName);
                    li.SubItems.Add(_rsiteata[i].SiteName);
                    //li.SubItems.Add(rd.SiteLevel);
                    li.SubItems.Add(_rsiteata[i].WorkingDays.ToString());
                    li.SubItems.Add(_rsiteata[i].Cd4Td.ToString());
                    li.SubItems.Add(_rsiteata[i].ChemTd.ToString());
                    li.SubItems.Add(_rsiteata[i].HemaTd.ToString());
                    li.SubItems.Add(_rsiteata[i].ViralTd.ToString());
                    li.SubItems.Add(_rsiteata[i].OtherTd.ToString());
                    li.SubItems.Add(_rsiteata[i].Latitude.ToString());
                    li.SubItems.Add(_rsiteata[i].Longtiude.ToString());
                    str = _rsiteata[i].OpeningDate != null ? _rsiteata[i].OpeningDate.Value.ToShortDateString() : "";
                    li.SubItems.Add(str);
                    str = _rsiteata[i].IsExist ? "Yes" : "No";


                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (Item.SubItems[1].Text.Trim().ToLower() == _rsiteata[i].RegionName.Trim().ToLower() && Item.SubItems[3].Text.Trim().ToLower() == _rsiteata[i].SiteName.Trim().ToLower())
                        {
                            _rsiteata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    if (_rsiteata[i].HasError)
                    {
                        if (_rsiteata[i].RegionName == "")
                            errorString = errorString + " Region Name Required";//14 may 14
                        if (_rsiteata[i].SiteName == "")
                            errorString = errorString + " Site Name Required";
                        if (_rsiteata[i].CategoryName == "")
                            errorString = errorString + " Category Name Required";
                        //if (rd.SiteLevel == "")
                        //    errorString = errorString + " Site Level Required";

                        if (_rsiteata[i].Region == null)
                            errorString = errorString + " Region Doesn't Exist";

                        _rsiteata[i].ErrorDescription = _rsiteata[i].ErrorDescription + errorString;
                        li.BackColor = Color.Red;
                        haserror = true;

                        changecolorexcelcolumnadd(cmbsite.SelectedItem.ToString(), _rsiteata[i].RowNo, errorString, Color.Red);
                       //   label11.Text = "Please check excel to validate " + cmbsite.SelectedItem.ToString() + " data";
                    


                    }
                    if (_rsiteata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                       /// changecolorexcelcolumnadd(cmbsite.SelectedItem.ToString(), _rsiteata[i].RowNo, "Site already exist", Color.Yellow);
                      //  label11.Text = "Please check excel to validate " + cmbsite.SelectedItem.ToString() + " data";
                        isexist = true;
                    
                    }
                    li.SubItems.Add(_rsiteata[i].ErrorDescription);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;

                if (haserror) ////// || isexist
                    label12.Text = "Please check excel to validate " + cmbsite.SelectedItem.ToString() + " data";
                if (!haserror)//b jul 2 && !isexist
                {
                    label12.Text = " Validated " + cmbsite.SelectedItem.ToString() + "  data Successfully................. ";
                    btnsite.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbsiteinstrument_SelectionChangeCommitted(object sender, EventArgs e)
        {
            bool sitexist = false;

          
            bool isexist = false;
            int count = 0;
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            if (!sitelist_new)
            {
                result = DataRepository.GetAllSite();
            }
            if (result.Count > 0)
            {
                try
                {
                 
                    DataSet ds = ReadExcelFile(cmbsiteinstrument.SelectedItem.ToString(), 6);
                    label12.Text = " Validating " + cmbsiteinstrument.SelectedItem.ToString() + "  data ................. ";
                    _siteinstrumentdata = GetInDataRow(ds);
                    int[] percentage = new int[_siteinstrumentdata.Count];
                    bool haserror = false;
                    ListView lvInImport = new ListView();
                    lvInImport.BeginUpdate();
                    lvInImport.Items.Clear();
                    string errorString;

                    //foreach (Importlist.SiteInstrumentImportData rd in _siteinstrumentdata)
                    //{
                      
                                            for (var i = 0; i < _siteinstrumentdata.Count; i++)
                {
                        string str;
                        errorString = "";
                        ListViewItem li = new ListViewItem(_siteinstrumentdata[i].RowNo.ToString());
                        li.SubItems.Add(_siteinstrumentdata[i].RegionName);
                        foreach (ForlabSite site in result)
                        {
                            if ((site.SiteName.ToLower() == _siteinstrumentdata[i].SiteName.ToLower()) && (site.Region.RegionName.ToLower() == _siteinstrumentdata[i].RegionName.ToLower()))
                            {
                                sitexist = true;
                                break;
                            }
                        }
                        if (!sitexist)
                        {
                            _siteinstrumentdata[i].HasError = true;
                            if (_siteinstrumentdata[i].RegionName == "")
                                errorString = errorString + " Region Doesn't Exist";//14 may 14
                            else
                                errorString = errorString + " Site Doesn't Exist In This Region";//14 may 14
                        }
                        sitexist = false;
                        li.SubItems.Add(_siteinstrumentdata[i].SiteName);
                        Instrument Inst = DataRepository.GetInstrumentByName(_siteinstrumentdata[i].InstrumentName);
                        ForlabSite s = DataRepository.GetSiteByName(_siteinstrumentdata[i].SiteName);

                        if (Inst != null)
                            li.SubItems.Add(Inst.TestingArea.AreaName);
                        else
                        {
                            haserror = true;
                            _siteinstrumentdata[i].HasError = true;
                            errorString = errorString + " Instrument Doesn't Exist";//14 may 14
                            li.SubItems.Add("");//?
                        }
                        count = 0;
                        foreach (ListViewItem Item in lvInImport.Items)
                        {
                            if (Item.SubItems[1].Text.Trim().ToLower() == _siteinstrumentdata[i].RegionName.Trim().ToLower() && Item.SubItems[2].Text.Trim().ToLower() == _siteinstrumentdata[i].SiteName.Trim().ToLower() && Item.SubItems[4].Text.Trim().ToLower() == _siteinstrumentdata[i].InstrumentName.Trim().ToLower())
                            {
                                _siteinstrumentdata[i].IsExist = true;
                                str = "Duplicated";
                            }
                            //if (rd.TestingArea == Item.SubItems[3].Text)
                            //{  
                            //    try
                            //    {
                            //        percentage[count] = int.Parse(Item.SubItems[6].Text);
                            //        count++;
                            //    }
                            //    catch
                            //    {
                            //    }
                            //}
                        }
                        //int sum = 0;
                        //for (int i = 0; i < count; i++)
                        //{
                        //    sum = sum + percentage[i];
                        //}
                        //if ((sum > 100 || sum < 100) && (sum!=0))
                        //    rd.HasError = true;
                        if (Inst != null && s != null)
                        {
                            foreach (SiteInstrument inst in s.SiteInstruments)
                            {
                                if (inst.Instrument == Inst)
                                {
                                    _siteinstrumentdata[i].IsExist = true;
                                }
                            }

                        }




                        li.SubItems.Add(_siteinstrumentdata[i].InstrumentName);
                        li.SubItems.Add(_siteinstrumentdata[i].Quantity.ToString());
                        li.SubItems.Add(_siteinstrumentdata[i].PecentRun.ToString());
                        str = _siteinstrumentdata[i].IsExist ? "Yes" : "No";
                        li.SubItems.Add(str);
                        if (_siteinstrumentdata[i].HasError)
                        {
                            li.BackColor = Color.Red;
                            haserror = true;
                            if (_siteinstrumentdata[i].RegionName == "")
                                errorString = errorString + " Region Name Required";//14 may 14
                            if (_siteinstrumentdata[i].SiteName == "")
                                errorString = errorString + " Site Name Required";
                            if (_siteinstrumentdata[i].InstrumentName == "")
                                errorString = errorString + " Instrument Name Required";//14 may 14
                            if (_siteinstrumentdata[i].SiteName == "")
                                errorString = errorString + " Site Name Required";

                            changecolorexcelcolumnadd(cmbsiteinstrument.SelectedItem.ToString(), _siteinstrumentdata[i].RowNo, errorString, Color.Red);
                           // label11.Text = "Please check excel to validate " + cmbsiteinstrument.SelectedItem.ToString() + " data";
                        }
                        if (_siteinstrumentdata[i].IsExist)
                        {
                            li.BackColor = Color.Yellow;
                         ///////   changecolorexcelcolumnadd(cmbsiteinstrument.SelectedItem.ToString(), _siteinstrumentdata[i].RowNo, "Site Instrument akready exist", Color.Red);
                         //   label11.Text = "Please check excel to validate data";
                            isexist = true;
                        }
                        li.SubItems.Add(errorString);
                        lvInImport.Items.Add(li);
                    }



                    lvInImport.EndUpdate();
                   
            

                    butClear.Enabled = true;
                    if (haserror)          ///////// || isexist
                        label12.Text = "Please check excel to validate " + cmbsiteinstrument.SelectedItem.ToString() + " data";
                    if (!haserror)//b jul 9      && !isexist
                    {
                        label12.Text = " Validated" + cmbsiteinstrument.SelectedItem.ToString() + "  data Successfully ................. ";
                        btnsiteinstrument.Enabled = true;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Error:Importing", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbreferralsite_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
               
                DataSet ds = ReadExcelFile(cmbreferralsite.SelectedItem.ToString(), 7);
                label12.Text = " Validating " + cmbreferralsite.SelectedItem.ToString() + "  data ................. ";
                _rrefsitedata = GetrefsiteRow(ds);
                bool haserror = false;

                bool isexist = false;
                ListView lvImport = new ListView();
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                string errorString;
                //foreach (Importlist.RefSiteImportData rd in _rrefsitedata)
                //{
                          for (var i = 0; i < _rrefsitedata.Count; i++)
                {
                    string str;
                    errorString = "";
                    ListViewItem li = new ListViewItem(_rrefsitedata[i].RowNo.ToString());
                    li.SubItems.Add(_rrefsitedata[i].RegionName);
                    li.SubItems.Add(_rrefsitedata[i].SiteName);
                    li.SubItems.Add(_rrefsitedata[i].CD4RefSite.ToString());
                    li.SubItems.Add(_rrefsitedata[i].ChemistryRefSite.ToString());
                    li.SubItems.Add(_rrefsitedata[i].HematologyRefSite.ToString());
                    li.SubItems.Add(_rrefsitedata[i].ViralLoadRefSite.ToString());
                    li.SubItems.Add(_rrefsitedata[i].OtheRefSite.ToString());


                    str = _rrefsitedata[i].IsExist ? "Yes" : "No";


                    foreach (ListViewItem Item in lvImport.Items)
                    {
                        if (((Item.SubItems[2].Text.Trim().ToLower() == _rrefsitedata[i].SiteName.Trim().ToLower()) && (Item.SubItems[3].Text.Trim().ToLower() == _rrefsitedata[i].CD4RefSite.Trim().ToLower())) ||
                            ((Item.SubItems[2].Text.Trim().ToLower() == _rrefsitedata[i].SiteName.Trim().ToLower()) && (Item.SubItems[4].Text.Trim().ToLower() == _rrefsitedata[i].ChemistryRefSite.Trim().ToLower())) ||
                            ((Item.SubItems[2].Text.Trim().ToLower() == _rrefsitedata[i].SiteName.Trim().ToLower()) && (Item.SubItems[5].Text.Trim().ToLower() == _rrefsitedata[i].HematologyRefSite.Trim().ToLower())) ||
                            ((Item.SubItems[2].Text.Trim().ToLower() == _rrefsitedata[i].SiteName.Trim().ToLower()) && (Item.SubItems[6].Text.Trim().ToLower() == _rrefsitedata[i].ViralLoadRefSite.Trim().ToLower())) ||
                            ((Item.SubItems[2].Text.Trim().ToLower() == _rrefsitedata[i].SiteName.Trim().ToLower()) && (Item.SubItems[7].Text.Trim().ToLower() == _rrefsitedata[i].OtheRefSite.Trim().ToLower())))
                        {
                            _rrefsitedata[i].IsExist = true;
                            str = "Duplicated";
                        }

                    }
                    li.SubItems.Add(str);
                    if (_rrefsitedata[i].HasError)
                    {
                        if (_rrefsitedata[i].RegionName == "")
                            errorString = errorString + " Region Name Required";//14 may 14
                        if (_rrefsitedata[i].SiteName == "")
                            errorString = errorString + " Site Name Required";
                        if (_rrefsitedata[i].Region == null)
                            errorString = errorString + " Region Doesn't Exist";

                        _rrefsitedata[i].ErrorDescription = _rrefsitedata[i].ErrorDescription + errorString;
                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbreferralsite.SelectedItem.ToString(), _rrefsitedata[i].RowNo, errorString, Color.Red);
                        haserror = true;
                       // label11.Text = "Please check excel to validate " + cmbreferralsite.SelectedItem.ToString()  + " data";
                    }
                    if (_rrefsitedata[i].IsExist)
                    {
                        li.BackColor = Color.Yellow;
                       // changecolorexcelcolumnadd(cmbreferralsite.SelectedItem.ToString(), _rrefsitedata[i].RowNo, "", Color.Yellow);
                        //label11.Text = "Please check excel to validate " + cmbreferralsite.SelectedItem.ToString() + " data";
                        isexist = true;
                    }
                    li.SubItems.Add(_rrefsitedata[i].ErrorDescription);
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;


                if (haserror) ////// || isexist
                    label12.Text = "Please check excel to validate " + cmbreferralsite.SelectedItem.ToString() + " data";
                if (!haserror)//b jul 2  && !isexist
                {

                    label12.Text = " Validated " + cmbreferralsite.SelectedItem.ToString() + "  data Successfully ................. ";
                    btnreferralsite.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbvariable_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
         
                DataSet ds = ReadExcelFile(cmbvariable.SelectedItem.ToString(), 4);
                label12.Text = " Validating " + cmbvariable.SelectedItem.ToString() + "  data ................. ";
                ListView lvImport = new ListView();
                _rvariabledata = GetvariableaRow(ds);
                bool haserror = false;
                bool isexist = false;
                lvImport.BeginUpdate();
                lvImport.Items.Clear();

                //foreach (Importlist.ImportQVariableData rd in _rvariabledata)
                //{
                for (var i = 0; i < _rvariabledata.Count; i++)
                {
                    ListViewItem li = new ListViewItem(_rvariabledata[i].RowNo.ToString());
                    li.SubItems.Add(_rvariabledata[i].ProductName);
                    li.SubItems.Add(_rvariabledata[i].UsageRate.ToString());
                    li.SubItems.Add(_rvariabledata[i].QuantifyMenu);
                    li.SubItems.Add(_rvariabledata[i].AppliedTo);
                    li.SubItems.Add(_rvariabledata[i].IsExist ? "Yes" : "No");
                    li.SubItems.Add(_rvariabledata[i].ErrorDescription);

                    if (_rvariabledata[i].HasError)
                    {
                        li.BackColor = Color.Red;
                        changecolorexcelcolumnadd(cmbreferralsite.SelectedItem.ToString(), _rvariabledata[i].RowNo, "", Color.Red);
                       // label11.Text = "Please check excel to validate " + cmbvariable.SelectedItem.ToString() + " data";
                        haserror = true;
                    }
                    else if (_rvariabledata[i].IsExist)
                    {
                        li.BackColor = Color.Green;
                       /////// changecolorexcelcolumnadd(cmbreferralsite.SelectedItem.ToString(), _rvariabledata[i].RowNo, "Already exist", Color.Green);
                       // label11.Text = "Please check excel to validate " + cmbvariable.SelectedItem.ToString() + "  data";
                    }
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                if (haserror)     //////////// || isexist
                    label11.Text = "Please check excel to validate " + cmbvariable.SelectedItem.ToString() + " data";
                if (!haserror)      ////////// && !isexist
                {
                    label12.Text = " Validated " + cmbvariable.SelectedItem.ToString() + "  data Successfully ................. ";
                    btnvariable.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            //savetest();
            //saveproduct();
            //saveinstrument();
            //savetestproduct();
            //saveconsumables();
            //saveregion();
            //savesite();
            //savesiteinstrument();
            //savereferralsite();
            //////////////insert variable

            //savevariable();
        }

        private void savetest(string Sheetname)
        {
            int count = 0;
            int error = 0;
            bool isexist;
            try
            {

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Test Data...................... "; });
                else
                    label12.Text = "Saving Test Data...................... "; 
              //  label12.Text = "Saving Test Data...................... ";
                foreach (Importlist.ImportTestData rd in _rdata)
                {
                    if (!rd.IsExist)
                    {
                        if (!rd.HasError)
                        {
                            isexist = DataRepository.GetTestByName(rd.TestName) != null;
                            if (isexist)
                            {
                               // MessageBox.Show(rd.TestName + "Already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                changecolorexcelcolumnadd(Sheetname, rd.RowNo, rd.TestName + " already exist in sheet", Color.Red);
                                error++;
                                continue;
                            }
                            Test test = new Test();
                            test.TestName = rd.TestName;
                            test.TestingArea = rd.TestArea;

                            count++;
                            DataRepository.SaveOrUpdateTest(test);
                        }
                        else { error++; }
                    }
                    else { 
                        
                        //error++; 
                    
                    
                    }
                }
                string errorstr = count + " Tests are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Tests  Failed.Please check excel and correct Tests Entry";
                }
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr;
                //label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";
               // MessageBox.Show(count + " Tests are imported and saved successfully." + Environment.NewLine + error + " Tests Failed.Please check excel to correct test Entry ", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
               // label12.Text = "";
            }
            catch
            {
                MessageBox.Show("Error: Unable to imported and saved Test data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        }
        private void saveproduct(string Sheetname)
        {
            int count = 0;
            int error = 0;
            bool isexist;
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Product Data...................... "; });
                else
                    label12.Text = "Saving Product Data...................... "; 
               // label12.Text = "Saving Product Data...................... ";
                foreach (Importlist.ImportProductData rd in _rproductdata)
                {
                    if (!rd.IsExist)
                    {
                        if (!rd.HasError)
                        {
                            isexist = DataRepository.GetProductByName(rd.ProductName) != null;
                            if(isexist)
                            {
                                //MessageBox.Show(rd.ProductName + " Already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                changecolorexcelcolumnadd(Sheetname, rd.RowNo, rd.ProductName + " already exist in sheet", Color.Red);
                                error++;
                                continue;
                            }
                            MasterProduct pro = new MasterProduct();
                            pro.ProductName = rd.ProductName;
                            pro.SerialNo = rd.Serial;
                            pro.BasicUnit = rd.BasicUnit;
                            pro.ProductType = rd.Category;
                            pro.Specification = rd.Specification;
                            pro.MinimumPackSize = rd.minSize;
                            pro.RapidTestGroup = rd.RapidTest;

                            ProductPrice pp = new ProductPrice();
                            pp.FromDate = rd.PriceDate;
                            pp.PackSize = rd.Packsize;
                            pp.Price = rd.Price;

                            pro.ProductPrices.Add(pp);
                            count++;
                            DataRepository.SaveOrUpdateProduct(pro);
                        }
                        else { error++; }
                    }
                    else { error++; }

                }
                string errorstr = count + " Products are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Products Failed.Please check excel and correct product Entry";
                }

             //   MessageBox.Show(count + " Products are imported and saved successfully." + Environment.NewLine + error + " Products Failed.Please check excel and correct product entry", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
             //   if (error > 0)

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr; 
               // label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";


            }
            catch
            {
                MessageBox.Show("Error: Unable to import and save product data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        }
        private void saveinstrument(string Sheetname)
        {
            int count = 0;
            int error = 0;
            bool isexist;
            try
            {

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Instrument Data...................... ";  });
                else
                    label12.Text = "Saving Instrument Data...................... "; 

             //   label12.Text = "Saving Instrument Data...................... ";
                foreach (Importlist.ImportInsData rd in _rinsata)
                {
                    if (!rd.IsExist)
                    {
                        if (!rd.HasError)
                        {
                            isexist = DataRepository.GetInstrumentByName(rd.InsName) != null;
                            if (isexist)
                            {
                               // MessageBox.Show(rd.InsName + " Already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                changecolorexcelcolumnadd(Sheetname, rd.RowNo, rd.InsName + " already exist in sheet", Color.Red);
                                error++;
                                continue;
                            }
                            Instrument ins = new Instrument();

                            ins.InstrumentName = rd.InsName;
                            ins.MaxThroughPut = rd.Rate;
                            // ins.MonthMaxTPut = (rd.Rate * 5) * 22;
                            ins.TestingArea = rd.TestingArea;
                            ins.DailyCtrlTest = rd.DailyCtrTest;
                            ins.WeeklyCtrlTest = rd.WeeklyCtrTest;
                            ins.MonthlyCtrlTest = rd.MonthlyCtrTest;
                            ins.QuarterlyCtrlTest = rd.QuarterlyCtrTest;
                            ins.MaxTestBeforeCtrlTest = rd.PerTestCtr;
                            count++;
                            DataRepository.SaveOrUpdateInstrument(ins);

                            if (rd.TestingArea.UseInDemography)
                            {
                                if (rd.TestingArea.ClassOfTestToEnum == ClassOfMorbidityTestEnum.CD4 ||
                                    rd.TestingArea.ClassOfTestToEnum == ClassOfMorbidityTestEnum.Chemistry ||
                                    rd.TestingArea.ClassOfTestToEnum == ClassOfMorbidityTestEnum.Hematology ||
                                    rd.TestingArea.ClassOfTestToEnum == ClassOfMorbidityTestEnum.ViralLoad)
                                {
                                    MorbidityTest mtest = new MorbidityTest();
                                    mtest.Instrument = ins;
                                    mtest.ClassOfTest = rd.TestingArea.Category;
                                    mtest.TestName = ManageQuantificationMenus.BuildTestName(ins.InstrumentName, mtest.ClassOfTestEnum);
                                    ManageQuantificationMenus.CreateQuantifyMenus(mtest);

                                    DataRepository.SaveOrUpdateMorbidityTest(mtest);
                                }
                            }
                        }
                        else { error++; }
                    }
                    else {
                        
                       // error++; 
                    }

                }
                string errorstr = count + " Instruments are imported and saved successfully.";

                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Instruments Failed.Please check excel and correct Instruments Entry";
                }
              //  MessageBox.Show(count + " Instruments are imported and saved successfully." + Environment.NewLine + error + " Instruments Failed.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
               // this.DialogResult = System.Windows.Forms.DialogResult.OK;
               // this.Close();
              ///  if (error > 0)
              ///  

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr;
               // label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";

            }
            catch
            {
                MessageBox.Show("Error: Unable to import and save Instrument data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        }
        private void savetestproduct(string Sheetname)
        {
            int count = 0;
            int error = 0;
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Test product Usages Data...................... ";  });
                else
                    label12.Text = "Saving Test product Usages Data...................... ";
                // label12.Text = errorstr;
               // label12.Text = "Saving Test product Usages Data...................... ";
                foreach (Importlist.ImportProUsageData rd in _rtestprodata)
                {
                    if (!rd.IsExist)
                    {
                          if (!rd.HasError)
                         {
                        ProductUsage pu = new ProductUsage();
                        pu.Instrument = rd.Instrument;
                        pu.Product = rd.Product;
                        pu.Rate = rd.Rate;
                        pu.IsForControl = rd.IsForControl;
                        pu.IsForTest = rd.IsForTest;
                        rd.Test.ProductUsages.Add(pu);
                        count++;
                        #region preLogic
                        //if (rd.IsForControl || rd.IsForTest)
                        //{
                        //    ProductUsage puC = new ProductUsage();
                        //    puC.Instrument = rd.Instrument;
                        //    puC.Product = rd.Product;
                        //    puC.Rate = rd.Rate;
                        //    puC.IsForControl = rd.IsForControl;
                        //    puC.IsForTest = rd.IsForTest;
                        //    rd.Test.ProductUsages.Add(puC);
                        //    count++;

                        //    if (rd.UseForBoth)
                        //    {
                        //        pu.IsForControl = false;
                        //        rd.Test.ProductUsages.Add(pu);
                        //        count++;
                        //    }
                        //}
                        //else
                        //{
                        //    pu.IsForControl = false;
                        //    rd.Test.ProductUsages.Add(pu);
                        //    count++;
                        //}
                        #endregion
                        DataRepository.SaveOrUpdateTest(rd.Test);
                          }
                         else { error++; 
                          }
                    }
                    else { //error++;
                    }

                }
                string errorstr = count + " Product Usages are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Product Usages  Failed.Please check excel and correct Product Usages Entry";
                }
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr;
               // label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";
                //MessageBox.Show(count + "Product Usages are imported and saved successfully." + Environment.NewLine + error + " Products Failed.Please Check excel and correct Test product Usage", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
                //label12.Text = "";
            }
            catch
            {
                MessageBox.Show("Error: Unable to imported and saved Product Usage data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        
        }
        private void saveconsumables(string Sheetname)
        {
            int count = 0;
            int error = 0;
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Consumbles Data...................... "; });
                else
                    label12.Text = "Saving Consumbles Data...................... ";
               // label12.Text = "Saving Consumbles Data...................... ";
                foreach (Importlist.ImportConsumbleData rd in _rconsumbledata)
                {
                    if (!rd.IsExist)
                    {

                        if (!rd.HasError)
                        {
                            ConsumableUsage pu = new ConsumableUsage();
                            //  pu.Instrument = rd.Instrument;
                            pu.Product = rd.Product;
                            pu.ProductUsageRate = rd.Rate;
                            if (rd.IsForTest)
                            {
                                pu.NoOfTest = rd.NoOfTest;
                                pu.PerTest = rd.IsForTest;
                                rd.Cons.ConsumableUsages.Add(pu);
                                count++;
                            }
                            else if (rd.IsForPeriod)
                            {
                                pu.Period = rd.Period;
                                pu.PerPeriod = rd.IsForPeriod;
                                rd.Cons.ConsumableUsages.Add(pu);
                                count++;
                            }
                            else if (rd.IsForInstrument)
                            {
                                pu.Period = rd.Period;
                                pu.PerInstrument = rd.IsForInstrument;
                                pu.Instrument = rd.Instrument;
                                rd.Cons.ConsumableUsages.Add(pu);
                                count++;

                            }
                            DataRepository.SaveOrUpdateConsumables(rd.Cons);
                        }
                        else { error++; }
                    }
                    else { //error++;
                    
                    }

                }
                string errorstr = count + " Consumables are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Consumables Failed.Please check excel and correct Consumables Entry";
                }
             
               // MessageBox.Show(count + "Consumables are imported and saved successfully." + Environment.NewLine + error + " Consumables Failed.Please check excel and correct consumble records", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr;
              //  label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
            }
            catch
            {
                MessageBox.Show("Error: Unable to imported and saved Consumables data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        }
        private void saveregion(string Sheetname)
        {
            int count = 0;
            int error = 0;
            bool isexist;
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Region Data...................... "; });
                else
                    label12.Text = "Saving Region Data...................... ";
               // label12.Text = "Saving Region Data...................... ";
                foreach (Importlist.RegionReportedData rd in _rregiondata)
                {
                    if (!rd.IsExist)
                    {
                        if (!rd.HasError)
                        {
                            isexist = DataRepository.GetRegionByName(rd.RegionName) != null;
                            if (isexist)
                            {
                               // MessageBox.Show(rd.RegionName + "Already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                changecolorexcelcolumnadd(Sheetname, rd.RowNo, rd.RegionName + " already exist in sheet", Color.Red);
                                error++;
                                continue;
                            }
                            ForlabRegion region = new ForlabRegion();
                            region.RegionName = rd.RegionName;
                            region.ShortName = rd.ShortName;
                            count++;
                            DataRepository.SaveOrUpdateRegion(region);
                        }
                        else
                        {
                            error++;
                        }


                    }
                    else
                    {
                        error++;
                    }

                }
                string errorstr = count + " Regions/Districts/Provinces  are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Regions/Districts/Provinces  Failed.Please check excel and correct Regions/Districts/Provinces Entry";
                }

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr; 
             //   label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";
               // MessageBox.Show(count + " Regions/Districts/Provinces are imported and saved successfully." + Environment.NewLine + error + " Regions/Districts/Provinces Failed.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
              //  label12.Text = "";


            }
            catch
            {
                MessageBox.Show("Error: Unable to import and save Region/District/Province data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        }
        private void savesite(string Sheetname)
        {
            int count = 0;
            int error = 0;
            bool isexist;
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = "Saving Site Data...................... ";  });
                else
                    label12.Text = "Saving Site Data...................... ";
               // label12.Text = "Saving Site Data...................... ";
                foreach (Importlist.SiteImportData rd in _rsiteata)
                {
                    if (!rd.IsExist && !rd.HasError)
                    {

                        isexist = DataRepository.GetSiteByName(rd.SiteName) != null;
                        if (isexist)
                        {
                            //MessageBox.Show(rd.SiteName + " Already Exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            changecolorexcelcolumnadd(Sheetname, rd.RowNo, rd.SiteName + " already exist in sheet", Color.Red);
                            error++;
                            continue;
                        }
                        ForlabSite site = new ForlabSite();

                        site.Region = rd.Region;
                        site.SiteName = rd.SiteName;
                        site.SiteLevel = rd.SiteLevel;
                        site.SiteCategory = rd.Category;
                        site.WorkingDays = rd.WorkingDays;
                        site.CD4TestingDaysPerMonth = rd.Cd4Td;
                        site.ChemistryTestingDaysPerMonth = rd.ChemTd;
                        site.HematologyTestingDaysPerMonth = rd.HemaTd;
                        site.ViralLoadTestingDaysPerMonth = rd.ViralTd;
                        site.OtherTestingDaysPerMonth = rd.OtherTd;
                        site.Latitude = rd.Latitude;
                        site.Longitude = rd.Longtiude;
                        SiteStatus ss = new SiteStatus();
                        ss.OpenedFrom = rd.OpeningDate != null ? rd.OpeningDate.Value : DateTime.Now;
                        site.SiteStatuses.Add(ss);
                        count++;
                        DataRepository.SaveOrUpdateSite(site);


                    }
                    else { error++; }

                }

                string errorstr = count + " Sites  are imported and saved successfully.";
                if (error > 0)
                {
                    errorstr = errorstr + Environment.NewLine + error + " Sites  Failed.Please check excel and correct Sites Entry";
                }

                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = errorstr; });
                else
                    label12.Text = errorstr; 
              //  label12.Text = errorstr;
                successfailuremsg = successfailuremsg + errorstr + ",";
             //   MessageBox.Show(count + " Sites are imported and saved successfully." + Environment.NewLine + error + " Sites Failed.Please check excel and correct site Record", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
               // label12.Text = "";

            }
            catch
            {
                MessageBox.Show("Error: Unable to import Site data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();

            }
        }
        private void SaveInstrument1()
        {
            instcount = 0;
            IList<SiteInstrument> resultin = new List<SiteInstrument>();
            result = DataRepository.GetAllSite();
            try
            {
                if (label12.InvokeRequired)
                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                else
                    label12.Text = ""; 
                foreach (Importlist.SiteInstrumentImportData rd in _siteinstrumentdata)
                {
                    if (!rd.IsExist)
                    {
                        if (!rd.HasError)
                        {
                            SiteInstrument sitein = new SiteInstrument();
                            ForlabSite site = DataRepository.GetSiteByName(rd.SiteName);
                            if (site == null)
                                rd.HasError = true;
                            Instrument Inst = DataRepository.GetInstrumentByName(rd.InstrumentName);
                            sitein.Site = site;
                            sitein.Instrument = Inst;
                            sitein.Quantity = rd.Quantity;
                            sitein.TestRunPercentage = rd.PecentRun;
                            //   DataRepository.SaveOrUpdateSite(site);

                            foreach (ForlabSite sitei in result)
                            {
                                if (sitei.SiteName == rd.SiteName && !sitei.SiteInstruments.Contains(sitein))
                                {
                                    sitein.Site = sitei;
                                    sitei.SiteInstruments.Add(sitein);
                                    instcount++;
                                }


                            }
                        }
                        else { errorsiteins++; }
                    }
                 

                }
                //  SaveAll();
                // MessageBox.Show(instcount + " Site Instrument are imported successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();

            }
            catch
            {
                MessageBox.Show("Error: Unable to import Site Instrument data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // DataRepository.CloseSession();
            }

        }

        private int InstrumentUsage(ForlabSite s, ClassOfMorbidityTestEnum platform)
        {
            decimal sum = 0;
            // bool error = false;
            int removedInst = 0;
            IList<SiteInstrument> sinst = new List<SiteInstrument>();
            if (s.GetInstrumentByPlatform(platform).Count > 0)//instrument percentage
            {
                sinst = s.GetInstrumentByPlatform(platform);
                foreach (SiteInstrument si in sinst)
                {
                    sum = sum + si.TestRunPercentage;
                }
                if (sum != 100)
                {
                    // error = true;
                    foreach (SiteInstrument si in sinst)
                    {
                        foreach (Importlist.SiteInstrumentImportData rd in _siteinstrumentdata)
                        {
                            if (rd.InstrumentName == si.Instrument.InstrumentName && rd.SiteName == s.SiteName && !rd.IsExist && !rd.HasError)
                            {
                                s.SiteInstruments.Remove(si);
                                sum = sum + si.TestRunPercentage;
                                removedInst++;
                            }
                        }
                    }
                }
            }
            return removedInst;
        }
        private void savesiteinstrument(string Sheetname)
        {
            SaveInstrument1();
            ListViewItem li = new ListViewItem();
            string siteName;
            int count = 0, instPercent = 0;
            if (label12.InvokeRequired)
                label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
            else
                label12.Text = "";

            foreach (ForlabSite site in result)
            {

                siteName = site.SiteName;
             //   li = lvImport.FindItemWithText(site.SiteName);
                if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.CD4).Count > 0)//|| site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.FlowCytometry).Count > 0)
                {
                    if (site.CD4TestingDaysPerMonth <= 0)
                    {
                        site.CD4TestingDaysPerMonth = 1;
                    }
                    // if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.CD4).Count > 0)
                    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.CD4);//instrument percentage
                    //else
                    //    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.FlowCytometry);//instrument percentage
                    if (instPercent > 0)
                    {
                        count = count + instPercent;
                        instPercent = 0;
                    }

                }

                if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Chemistry).Count > 0)
                {
                    if (site.ChemistryTestingDaysPerMonth <= 0)
                    {
                        site.ChemistryTestingDaysPerMonth = 1;
                    }


                    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.Chemistry);//instrument percentage
                    if (instPercent > 0)
                    {
                        count = count + instPercent;
                        instPercent = 0;
                    }


                }

                if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.Hematology).Count > 0)
                {
                    if (site.HematologyTestingDaysPerMonth <= 0)
                    {
                        site.HematologyTestingDaysPerMonth = 1;
                    }
                    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.Hematology);//instrument percentage
                    if (instPercent > 0)
                    {
                        count = count + instPercent;
                        instPercent = 0;
                    }

                }

                if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.ViralLoad).Count > 0)
                {
                    if (site.ViralLoadTestingDaysPerMonth <= 0)
                    {
                        site.ViralLoadTestingDaysPerMonth = 1;
                    }
                    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.ViralLoad);//instrument percentage
                    if (instPercent > 0)
                    {
                        count = count + instPercent;
                        instPercent = 0;
                    }

                }

                if (site.GetInstrumentByPlatform(ClassOfMorbidityTestEnum.OtherTest).Count > 0)
                {
                    if (site.OtherTestingDaysPerMonth <= 0)
                    {
                        site.OtherTestingDaysPerMonth = 1;
                    }
                    instPercent = InstrumentUsage(site, ClassOfMorbidityTestEnum.OtherTest);//instrument percentage
                    if (instPercent > 0)
                    {
                        count = count + instPercent;
                        instPercent = 0;
                    }

                }

                if (site.Id > siteIndex && sitelist_new)
                {
                    site.Id = -1;

                }
                DataRepository.SaveOrUpdateSite(site);
            }
            totalResult = DataRepository.GetAllSite();
            ForlabSite lastsite = DataRepository.GetSiteById(siteIndex);
            int index = totalResult.IndexOf(lastsite);

            if (!sitelist_new)
            {
                if (instcount - count > 0)
                {

                    if (label12.InvokeRequired)
                        label12.Invoke((MethodInvoker)delegate { label12.Text = instcount - count + " Site Instrument are imported and saved successfully."; });
                    else
                        label12.Text = instcount - count + " Site Instrument are imported and saved successfully.";

                  
                    // MessageBox.Show(instcount - count + " Site Instrument are imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                   // label12.Text = instcount - count + " Site Instrument are imported and saved successfully.";
                    successfailuremsg = successfailuremsg + Convert.ToString(instcount - count) + " Site Instrument are imported and saved successfully.";
                    if (errorsiteins > 0)
                    {
                        successfailuremsg = successfailuremsg + Environment.NewLine + errorsiteins + " Site Instrument  Failed.Please check excel and correct Tests Entry" + ",";
                    }
                    else
                    {
                        successfailuremsg = successfailuremsg + ",";
                    }
                }
                else
                    MessageBox.Show("No Data imported .", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if ((count - sitecount) <= 0)
            {
                MessageBox.Show("No Data imported .", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {

                MessageBox.Show(count - sitecount + " Site Instrument are imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            sitelist_new = false;
            instcount = 0;
        }
        private void savereferralsite()
        {

            int count = 0;
            int error = 0;
            try
            {
                label12.Text = " ";
                foreach (Importlist.RefSiteImportData rd in _rrefsitedata)
                {
                    if (!rd.IsExist && !rd.HasError)
                    {
                        ForlabSite site = DataRepository.GetSiteByName(rd.SiteName);
                        site.CD4RefSite = rd.CD4RefSiteId;
                        site.ChemistryRefSite = rd.ChemistryRefSiteId;
                        site.HematologyRefSite = rd.HematologyRefSiteId;
                        site.ViralLoadRefSite = rd.ViralLoadRefSiteId;
                        site.OtherRefSite = rd.OtheRefSiteId;
                        count++;
                        DataRepository.SaveOrUpdateSite(site);


                    }
                    else { error++; }

                }
            }
            catch
            {
                MessageBox.Show("Error: Unable to import Referral Site data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();

            }
        }
        private void savevariable()
        {


            int count = 0;
            try
            {
                label12.Text = "Saving variable Data...................... ";
                foreach (QuantifyMenu qm in _modifiedQuantifyMenus.Values)
                {
                    count++;
                    DataRepository.SaveOrUpdateQuantifyMenu(qm);
                }
                MessageBox.Show(count + " Quantification variable are imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
                label12.Text = "";

            }
            catch
            {
                MessageBox.Show("Error: Unable to import and save Quantification variable.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                DataRepository.CloseSession();
            }
        
        }

        private void btnsavetest_Click(object sender, EventArgs e)
        {
           // savetest();
        }

        private void btnproduct_Click(object sender, EventArgs e)
        {
           // saveproduct();
        }

        private void btninstrument_Click(object sender, EventArgs e)
        {
            //saveinstrument();
        }

        private void btntestproduct_Click(object sender, EventArgs e)
        {
           // savetestproduct();
        }

        private void btnconsumables_Click(object sender, EventArgs e)
        {
            //saveconsumables();
        }

        private void btnregion_Click(object sender, EventArgs e)
        {
           // saveregion();
        }

        private void btnsite_Click(object sender, EventArgs e)
        {
           //savesite();
        }

        private void btnsiteinstrument_Click(object sender, EventArgs e)
        {
           // savesiteinstrument();
        }

        private void btnreferralsite_Click(object sender, EventArgs e)
        {
           // savereferralsite();
        }

        private void btnvariable_Click(object sender, EventArgs e)
        {
           // savevariable();
           
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("Are you sure you want to delete this Forecast?", "Delete Forecasting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    try
            //    {
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("Unable to delete record", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
            btnclick = true;
        }
        //public static DataTable GetDataTableFromArray(object[] array)
        //{

        //    DataTable dataTable = new DataTable();

        //    dataTable.LoadDataRow(array, true);//Pass array object to LoadDataRow method

        //    return dataTable;

        //}
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            string status;
            int progress =1;
            BackgroundWorker worker = sender as BackgroundWorker;

            OleDbConnection objConn = null;
            DataTable dt = null;
            String connString = "provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + txtFilename.Text + ";Extended Properties=Excel 8.0;";
           // Application.DoEvents();
            objConn = new OleDbConnection(connString);
            if (objConn.State == ConnectionState.Closed)
            {
                objConn.Open();
            }

            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (objConn.State == ConnectionState.Open)
            {
                objConn.Close();
            }

            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook excelBook = xlApp.Workbooks.Open(""+ txtFilename.Text +"");

            String[] excelSheets1 = new String[excelBook.Worksheets.Count];
            int i = 0, jk;
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("TABLE_NAME", typeof(System.String));
            DataTable dt2 = new DataTable();
            dt2.Columns.Add("TABLE_NAME", typeof(System.String));
            dt2.Rows.Add("Region");
            dt2.Rows.Add("Site");
            dt2.Rows.Add("Product");
            dt2.Rows.Add("Test");
            dt2.Rows.Add("Instrument");
            dt2.Rows.Add("Test Product Usage Rate");
            dt2.Rows.Add("Site Instrument");
            dt2.Rows.Add("Consumables");
           
            foreach (Microsoft.Office.Interop.Excel.Worksheet wSheet in excelBook.Worksheets)
            {
                excelSheets1[i] = wSheet.Name;
                i++;
            }
            foreach (var item in excelSheets1)
            {
                DataRow row = dt1.NewRow();
                row["TABLE_NAME"] = item;
                dt1.Rows.Add(row);
            }
            //if (progressBar1.InvokeRequired)
            //{
            //    progressBar1.Invoke((MethodInvoker)delegate
            //    {
            //        // Set Minimum to 1 to represent the first file being copied.
            //        progressBar1.Minimum = 1;
            //        // Set Maximum to the total number of Users created.
            //        progressBar1.Maximum = dt.Rows.Count;
            //        // Set the initial value of the ProgressBar.
            //        progressBar1.Value = 1;
            //        // Set the Step property to a value of 1 to represent each user is being created.
            //        progressBar1.Step = 1;
            //    });
            //}
            //else
            //{
            //    // Set Minimum to 1 to represent the first file being copied.
            //    progressBar1.Minimum = 1;
            //    // Set Maximum to the total number of Users created.
            //    progressBar1.Maximum = dt.Rows.Count;
            //    // Set the initial value of the ProgressBar.
            //    progressBar1.Value = 1;
            //    // Set the Step property to a value of 1 to represent each user is being created.
            //    progressBar1.Step = 1;
            //}
        
            List<string> excelSheets = new List<string>();
            foreach (DataRow row in dt.Rows)
            {
                if (!row["TABLE_NAME"].ToString().Contains("_"))
                {
                    row["TABLE_NAME"] = Convert.ToString(row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')).Trim('\''));
                }
            }

            excelBook.Save();

            xlApp.Quit();
            DataView dv = dt.DefaultView;
            //dv.Sort = "TABLE_NAME asc";
            dv.Sort = "TABLE_NAME desc";
            dt = dv.ToTable();
            foreach (DataRow row in dt2.Rows)
            {

                // List lst = new ListViewItem();
                if (!row["TABLE_NAME"].ToString().Contains("_"))
                {
                    //lst.ToolTipText = row["TABLE_NAME"].ToString();
                    //lst.Text = row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')).Replace('\'', ' ');

                    ///excelSheets.Add(row["TABLE_NAME"].ToString().Remove(row["TABLE_NAME"].ToString().IndexOf('$')));
                    if (row["TABLE_NAME"].ToString() == "Test")
                    {
                        System.Threading.Thread.Sleep(2000);
                        status = validateandsavetestdata(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                           // MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Test data, please troubleshoot and try to import again." || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = ""; 
                                //label12.Text = "";
                                return;
                            }
                            else if (status == "query did not return a unique result: 2")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            //label12.Text = "";
                            //return;
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                               btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                //if (this.InvokeRequired)
                                //    this.Invoke((MethodInvoker)delegate { this.Close(); });
                                //else
                                //    this.Close(); 
                                 e.Cancel = true;
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Product")
                    {
                        System.Threading.Thread.Sleep(2000);
                        status = validateandsaveproductdata(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with product data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                              //  label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                               // timer1.Stop();
                                //if (this.InvokeRequired)
                                //    this.Invoke((MethodInvoker)delegate { this.Close(); });
                                //else
                                //    this.Close();

                                e.Cancel = true;
                                break;
                                
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Instrument")
                    {

                        System.Threading.Thread.Sleep(2000);
                        status = validateandsaveInstrument(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Instrument data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                //if (this.InvokeRequired)
                                //    this.Invoke((MethodInvoker)delegate { this.Close(); });
                                //else
                                //    this.Close(); 
                                 e.Cancel = true;
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Test Product Usage Rate")
                    {

                        System.Threading.Thread.Sleep(2000);
                        status = validateandsavetestproduct(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Product Usage data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {

                                 e.Cancel = true;
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Consumables")
                    {
                        System.Threading.Thread.Sleep(2000);
                        status = validateandsaveconsumbles(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Consumables data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                e.Cancel = true;
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Region")
                    {

                        System.Threading.Thread.Sleep(2000);
                        status = validateandsaveregion(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Region data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                  e.Cancel = true;
                                break;
                         
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Site")
                    {
                        System.Threading.Thread.Sleep(2000);
                        status = validateandsavesite(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Site data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = "";
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                e.Cancel = true;
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }
                    else if (row["TABLE_NAME"].ToString() == "Site Instrument")
                    {
                        System.Threading.Thread.Sleep(2000);
                        status = validateandsavesiteinstrument(row["TABLE_NAME"].ToString());
                        System.Threading.Thread.Sleep(2000);
                        if (status != "")
                        {

                            MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            if (status == "There too many problem with Site Instrument data, please troubleshoot and try to import again" || status == "Please select a aexcel file")
                            {
                                MessageBox.Show(status, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                if (label12.InvokeRequired)
                                    label12.Invoke((MethodInvoker)delegate { label12.Text = ""; });
                                else
                                    label12.Text = ""; 
                                return;
                            }
                        }
                        else
                        {
                            if (btnExit.InvokeRequired)
                                btnExit.Invoke((MethodInvoker)delegate { btnExit.Enabled = true; });
                            else
                                btnExit.Enabled = true; 
                            if (btnclick == true)
                            {
                                e.Cancel = true;
                                break;
                           
                            }
                        }
                        System.Threading.Thread.Sleep(2000);
                        //progressBar1.PerformStep();
                    }

                }
                int fProgress=0;
                if (progress == 8) fProgress = 100;
                else fProgress=progress*10;
                System.Threading.Thread.Sleep(100);
                worker.ReportProgress(fProgress);
                progress++;
            }
            ////cmbtest.DataSource = excelSheets;
            ////cmbproduct.BindingContext = new BindingContext();  
            ////cmbproduct.DataSource = excelSheets;
            ////cmbinstrument.BindingContext = new BindingContext(); 
            ////cmbinstrument.DataSource = excelSheets;
            ////cmbtestproduct.BindingContext = new BindingContext(); 
            ////cmbtestproduct.DataSource = excelSheets;
            ////cmbvariable.BindingContext = new BindingContext(); 
            ////cmbvariable.DataSource = excelSheets;
            ////cmbreferralsite.BindingContext = new BindingContext(); 
            ////cmbreferralsite.DataSource = excelSheets;
            ////cmbregion.BindingContext = new BindingContext(); 
            ////cmbregion.DataSource = excelSheets;
            ////cmbsite.BindingContext = new BindingContext(); 
            ////cmbsite.DataSource = excelSheets;
            ////cmbsiteinstrument.BindingContext = new BindingContext(); 
            ////cmbsiteinstrument.DataSource = excelSheets;
            ////cmbconsumables.BindingContext = new BindingContext(); 
            ////cmbconsumables.DataSource = excelSheets;


            //if (progressBar1.InvokeRequired)
            //{
            //    progressBar1.Invoke((MethodInvoker)delegate
            //    {
            //        progressBar1.Value = dt.Rows.Count;
            //    });
            //}
            //else
            //{
            //    progressBar1.Value = dt.Rows.Count;
            //}
           
            // MessageBox.Show("imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
            messagebox1 frmmsg = new messagebox1("Imported and saved successfully", successfailuremsg);
            frmmsg.ShowDialog();
         
            // this.DialogResult = System.Windows.Forms.DialogResult.OK;
        ///    this.Close();
            objConn.Close();
        }

     

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
            ((LqtMainWindowForm)_mdiparent).BuildNavigationMenu();
            ((LqtMainWindowForm)_mdiparent)._currentUserCtr = new DashBoard();
            ((LqtMainWindowForm)_mdiparent).LoadCurrentUserCtr();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

    }
}
