﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.Quantification
{
    public partial class FrmproductAssumption : Form
    {
        private int _forecastid = 0;
        private string _mode = "";
        public FrmproductAssumption(int forecastid,string mode)
        {
            this._forecastid = forecastid;
            this._mode = mode;
            InitializeComponent();
            PopTestingAreas();
        }
        private void PopTestingAreas()
        {
            lsttestarea.BeginUpdate();
            lsttestarea.Items.Clear();

            foreach (TestingArea ta in DataRepository.GetAllTestingArea())
            {
                ListViewItem li = new ListViewItem(ta.AreaName) { Tag = ta.Id };

                lsttestarea.Items.Add(li);
            }

            lsttestarea.EndUpdate();


        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmproductAssumption_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add(_forecastid, txtwastagerate.Text, txtprogramrate.Text, txtpatientE.Text, txtpatientN.Text);
        }
    }
}
