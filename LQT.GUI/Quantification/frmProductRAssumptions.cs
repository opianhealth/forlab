﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using LQT.Core;
using System.Text.RegularExpressions;
namespace LQT.GUI.Quantification
{
    public partial class frmProductRAssumptions : Form
    {
        private int _selectedForcastId = 0;
        public static Int64 _totalTarget = 0;
        public static string Mode = "";
        public static string _status = "";
        private Form _mdiparent;
        DataTable Dtpatient1 = new DataTable();
        DataTable dt1 = new DataTable();
        private string _startdate = "";
        private string _enddate = "";
        private string _period = "";
       // public static Int64 _totalTarget = 0;
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
        DataTable dtGrid = null;
        private string pattern = "^[0-9]{0,2}$";
     //   string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        public frmProductRAssumptions(Form mdiparent,int ID, string mode, DataTable Dtpatient, string startdate, string enddate, string period, Int64 totaltarget, DataTable Dt)
        {
            InitializeComponent();
            _selectedForcastId = ID;
          //  _selectedForcastId = 53;
             Mode = mode;
            _mdiparent=mdiparent;
             _totalTarget = totaltarget;
             _selectedForcastId = ID;
             _startdate = startdate;
             _enddate = enddate;
             _period = period;
             Dtpatient1 = Dtpatient;
             dt1 = Dt;


           string _sql = string.Format(@"Select Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]='{0}'", _selectedForcastId);
            //using (connection = new SqlConnection(con))
            //{
                using (cmdForecast = new SqlCommand(_sql, connection))
                {
                    if (connection.State == ConnectionState.Closed)
                   connection.Open();
                    _status = Convert.ToString(cmdForecast.ExecuteScalar());
                    if (connection.State != ConnectionState.Closed)
                   connection.Close();
                }
            //}
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;

           // _status = "S";
            this.fillGrid(_selectedForcastId);
            this.fillList();
            //this.fillSiteCategory(_selectedForcastId);
            btnPrevious.TabStop = false;
            btnPrevious.FlatStyle = FlatStyle.Flat;
            btnPrevious.FlatAppearance.BorderSize = 0;
            btnPrevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            gvPAssumptions.AutoGenerateColumns = false;
            this.gvPAssumptions.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(gvPGroup_EditingControlShowing);
            this.gvPAssumptions.CellValidating += new DataGridViewCellValidatingEventHandler(gvPGroup_CellValidating);
            this.gvPAssumptions.DataError += this.gvPGroup_DataError;
        }
        public void gvPGroup_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }
        private void fillList()
        {
            string _sql = "";
            DataTable dtCategory = new System.Data.DataTable();
            try
            {
                _sql = string.Format(@"Select TypeID,TypeName from ProductType");
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Closed)
                        connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dtCategory);
                        lstTestingArea.DataSource = dtCategory;
                        lstTestingArea.DisplayMember = "TypeName";
                        lstTestingArea.ValueMember = "TypeID";
                        //gvPGroup.DataSource = dt;
                    }
                    if (connection.State != ConnectionState.Closed)
  connection.Close();
                //}
            }
            catch (Exception ex)
            {
               connection.Close();
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string checkValue = "",searchValue="";
            bool stuts = false;
            if (!validation() == false)
            {
                searchValue = txtWRate.Text;
                try
                {
                    //if (gvPGroup.Rows.Count > 0)
                    //{
                    //    foreach (DataGridViewRow row in gvPGroup.Rows)
                    //    {
                    //        if (row.Cells[2].Value.ToString().ToLower().Equals(searchValue.ToLower()))
                    //        {
                    //            //MessageBox.Show("Site Category Name is already exist");
                    //            MessageBox.Show("Patient Group Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                    //            txtPGN.Focus();
                    //            stuts = true;
                    //            break;
                    //        }

                    //    }
                    //}
                    //if (stuts == false)
                    //{
                    
                        dt = gvPAssumptions.DataSource as DataTable;
                        gvPAssumptions.DataSource = dt;
                       // DataTable dtPA = this.fillSiteCategory(_selectedForcastId);
                        //foreach (DataRow dr in dtPA.Rows)
                        //{
                        //if (dt != null)
                        //{
                            if (gvPAssumptions.Rows.Count > 0)
                            {
                                foreach (DataGridViewRow row in gvPAssumptions.Rows)
                                {
                                    if (row.Cells["ProductType"].Value.ToString().ToLower().Equals(lstTestingArea.Text.ToString().ToLower()))
                                    {
                                        MessageBox.Show("Product Type is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                                        txtWRate.Focus();
                                        stuts = true;
                                        break;
                                    }
                                }
                                if (stuts == false)
                                {
                                    dt.Rows.Add(0, _selectedForcastId, Convert.ToInt32(lstTestingArea.SelectedValue), lstTestingArea.Text, txtWRate.Text, txtPGRate.Text, txtEPYear.Text, txtNPYear.Text);
                                    //gvPAssumptions.DataSource = dt;
                                }
                            }
                            else dt.Rows.Add(0, _selectedForcastId, Convert.ToInt32(lstTestingArea.SelectedValue), lstTestingArea.Text, txtWRate.Text, txtPGRate.Text, txtEPYear.Text, txtNPYear.Text);
                      gvPAssumptions.DataSource = dt;
                        //}
                    ///    gvPAssumptions.Rows.Add(0, _selectedForcastId, Convert.ToInt32(lstTestingArea.SelectedValue), lstTestingArea.Text, txtWRate.Text, txtPGRate.Text, txtEPYear.Text, txtNPYear.Text);
                     
                         txtWRate.Text = "";
                        txtPGRate.Text = "";
                        txtEPYear.Text = "";
                        txtNPYear.Text = "";
                        txtWRate.Focus();
                   
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
        }
        public DataTable fillSiteCategory(int _forecastId)
        {
            string _sql = "";
            DataTable dtCategory = new System.Data.DataTable();
            try
            {
                if (_status == "C") _sql = string.Format(@"Select [SiteCategoryName] as SiteCategoryName,SiteCategoryId as CategoryId from ForecastCategoryInfo where [ForecastinfoID]={0}", _selectedForcastId);
                else if (_status == "S") _sql = string.Format(@"Select s.[SiteName] as SiteCategoryName,a.SiteId as CategoryId from ForecastSiteInfo a inner join Site s on a.SiteID=s.SiteID Where [ForecastinfoID]={0}", _selectedForcastId);    
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Closed)
                        connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dtCategory);
                        //gvPGroup.DataSource = dt;
                    }
                    if (connection.State != ConnectionState.Closed)
             connection.Close();
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return dtCategory;
        }
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
            DataTable dt1 = new System.Data.DataTable();
            try
            {
                _sql = string.Format(@"SELECT [ID],[ForecastinfoID],[ProductTypeID],sc.TypeName as ProductType,WastageRate,[ProgramGrowRate],[AvgBloodDrawPerYearE],[AvgBloodDrawPerYearN] FROM [TestingAssumption] p inner join ProductType sc on p.ProductTypeID=sc.TypeID Where [ForecastinfoID]={0}", ForecastinfoID);
                
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Closed)
                        connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dt1);
                        gvPAssumptions.AutoGenerateColumns = false;
                        gvPAssumptions.DataSource = dt1;
                    }
                    if (connection.State != ConnectionState.Closed)
 connection.Close();
                //}
            }
            catch (Exception ex)
            {
        connection.Close();
            }
        }
        private bool validation()
        {
            try
            {
                if (txtWRate.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Group Name.");
                    MessageBox.Show("Please Enter Wastage Rate.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtWRate.Focus();
                    return false;
                }
                if (txtWRate.Text == "0")
                {
                    MessageBox.Show("Wastage Rate must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
                if (txtPGRate.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Ratio.");
                    MessageBox.Show("Please Enter Program Growth Rate.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPGRate.Focus();
                    return false;
                }
                if (txtPGRate.Text == "0")
                {
                    MessageBox.Show("Program Growth Rate must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
                if (txtEPYear.Text == "")
                {
                    MessageBox.Show("Please Enter Existing Patient per year.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
                if (txtEPYear.Text =="0")
                {
                    MessageBox.Show("Existing Patient per year must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
                if (txtNPYear.Text == "")
                {
                    MessageBox.Show("Please Enter New Patient per year.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
                if (txtNPYear.Text == "0")
                {
                    MessageBox.Show("New Patient per year must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEPYear.Focus();
                    return false;
                }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return true;
        }

        private void txtPGN_KeyPress(object sender, KeyPressEventArgs e)
        {
            //var regex = new Regex(@"[^a-zA-Z0-9\s]");
            //if (regex.IsMatch(e.KeyChar.ToString()))
            //{
            //    e.Handled = true;
            //}
            //onlynumwithsinglepoint(sender, e);
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void txtRation_KeyPress(object sender, KeyPressEventArgs e)
        {
           // e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            //onlynumwithsinglepoint(sender, e);       
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            string _sql = "Insert into TestingAssumption(ForecastinfoID,WastageRate,ProgramGrowRate,AvgBloodDrawPerYearE,AvgBloodDrawPerYearN,ProductTypeID) Values(@ForecastinfoID,@WastageRate,@ProgramGrowRate,@AvgBloodDrawPerYearE,@AvgBloodDrawPerYearN,@ProductTypeID);SELECT SCOPE_IDENTITY()";
            string _sqlUpdate = "UPDATE [dbo].[TestingAssumption]  SET [ForecastinfoID] =@ForecastinfoID,[WastageRate] =@WastageRate ,[ProgramGrowRate] =@ProgramGrowRate,[AvgBloodDrawPerYearE] =@AvgBloodDrawPerYearE,[AvgBloodDrawPerYearN] =@AvgBloodDrawPerYearN,[ProductTypeID] =@ProductTypeID WHERE ID=@ID";

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("ForecastinfoID", typeof(int));
            table.Columns.Add("SiteCategoryName", typeof(string));
            table.Columns.Add("CurrentPatient", typeof(int));
            table.Columns.Add("TargetPatient", typeof(int));
            //table.Columns.Add("StartDate", typeof(string));
            //table.Columns.Add("EndDate", typeof(string));
            //table.Columns.Add("Period", typeof(string));
            try
            {
                ////using (connection = new SqlConnection(con))
                ////{
                if (connection.State == ConnectionState.Closed)
                connection.Open();
                    if (!checkGridRecord() == false)
                    {
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            if (gvPAssumptions.Rows.Count > 0)
                            {
                                using (cmdForecast = new SqlCommand("", connection, trans))
                                {
                                    for (int i = 0; i < gvPAssumptions.Rows.Count; i++)
                                    {
                                        if (Convert.ToInt32(gvPAssumptions.Rows[i].Cells["ID"].Value) == 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                           // cmdForecast.Parameters.Add("@TestAreaID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProducttypeID"].Value;

                                            cmdForecast.Parameters.Add("@WastageRate", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["WastageRate"].Value;
                                            cmdForecast.Parameters.Add("@ProgramGrowRate", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProgramGrowRate"].Value;
                                            cmdForecast.Parameters.Add("@AvgBloodDrawPerYearE", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["AvgBloodDrawPerYearE"].Value;
                                            cmdForecast.Parameters.Add("@AvgBloodDrawPerYearN", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["AvgBloodDrawPerYearN"].Value;

                                           cmdForecast.Parameters.Add("@ProductTypeID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProducttypeID"].Value;
                                            cmdForecast.CommandText = _sql;
                                            int ForecastCategoryInfo = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                            //table.Rows.Add(ForecastCategoryInfo, _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));
                                        }
                                        if (Convert.ToInt32(gvPAssumptions.Rows[i].Cells["ID"].Value) > 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                           // cmdForecast.Parameters.Add("@TestAreaID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProducttypeID"].Value;
                                            cmdForecast.Parameters.Add("@WastageRate", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["WastageRate"].Value;
                                            cmdForecast.Parameters.Add("@ProgramGrowRate", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProgramGrowRate"].Value;
                                            cmdForecast.Parameters.Add("@AvgBloodDrawPerYearE", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["AvgBloodDrawPerYearE"].Value;
                                            cmdForecast.Parameters.Add("@AvgBloodDrawPerYearN", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["AvgBloodDrawPerYearN"].Value;
                                            cmdForecast.Parameters.Add("@ProductTypeID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ProducttypeID"].Value;
                                            cmdForecast.Parameters.Add("@ID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ID"].Value;
                                            cmdForecast.CommandText = _sqlUpdate;
                                            cmdForecast.ExecuteNonQuery();
                                            //table.Rows.Add(Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value), _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));
                                        }

                                        cmdForecast.Parameters.Clear();
                                    }
                                }
                            }
                            trans.Commit();
                            ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save successfully..", true);
                            this.Hide();
                            //Frmlineargrowth FPA = new Frmlineargrowth(_mdiparent, _selectedForcastId, "E", Dtpatient1, _startdate, _enddate, _period, _totalTarget, dt1, _mMforecastinfo);
                            //FPA.ShowDialog();

                            this.Close();
                        }
                    }
                    if (connection.State != ConnectionState.Closed)
                 connection.Close();

                //}
            }
            catch (Exception ex)
            {
               connection.Close();
            }
        }
        private bool checkGridRecord()
        {
            //foreach (DataGridViewRow row in gvPAssumptions.Rows)
            //{
            //    if (Convert.ToInt32(row.Cells[3].Value) == 0 || Convert.ToInt32(row.Cells[4].Value) == 0)
            //    {
            //        MessageBox.Show("The value of Ratio And Calculated value must be a greater then Zero");
            //        return false;
            //    }
            //}
            return true;
        }

        private void gvPGroup_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }
        public void gvPGroup_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            int newInteger = 0;
            string PatientPercentage = "0";
            string CurrentPatient = "0";
            string newValue = e.FormattedValue.ToString();
            string oldValue = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
            if (e.ColumnIndex != ((System.Windows.Forms.DataGridView)(sender)).Columns["ProductType"].Index)
            {
                if (string.IsNullOrEmpty(newValue) || Convert.ToDecimal(newValue) == 0)
                {
                    e.Cancel = true;
                    MessageBox.Show("Value is not empty or Zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void txtCD4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //onlynumwithsinglepoint(sender, e);
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            this.Hide();

            PatientAssumptions frm = new PatientAssumptions(_mdiparent,_selectedForcastId, "E", Dtpatient1, _startdate, _enddate, _period, _totalTarget, dt1);
            // Form1 frm = new Form1();
            frm.ShowDialog();
            this.Close();
        }

        private void txtNPYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }
    }
}
