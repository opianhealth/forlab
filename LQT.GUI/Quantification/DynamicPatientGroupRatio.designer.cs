﻿namespace LQT.GUI.Quantification
{
    partial class DynamicPatientGroupRatio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DynamicPatientGroupRatio));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gvPGroup = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.PatientGroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ForecastinfoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientPercentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PatientRatio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(3, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 480);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gvPGroup);
            this.panel3.Location = new System.Drawing.Point(8, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1049, 436);
            this.panel3.TabIndex = 1;
            // 
            // gvPGroup
            // 
            this.gvPGroup.AllowUserToAddRows = false;
            this.gvPGroup.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvPGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPGroup.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.PatientGroupName,
            this.ID,
            this.ForecastinfoID,
            this.PatientPercentage,
            this.PatientRatio});
            this.gvPGroup.Location = new System.Drawing.Point(3, 3);
            this.gvPGroup.Name = "gvPGroup";
            this.gvPGroup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPGroup.Size = new System.Drawing.Size(1042, 428);
            this.gvPGroup.TabIndex = 0;
            this.gvPGroup.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvPGroup_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.FillWeight = 20.30457F;
            this.Column1.HeaderText = "";
            this.Column1.Image = global::LQT.GUI.Properties.Resources.Delete;
            this.Column1.Name = "Column1";
            // 
            // PatientGroupName
            // 
            this.PatientGroupName.DataPropertyName = "PatientGroupName";
            this.PatientGroupName.HeaderText = "Patient Group Name";
            this.PatientGroupName.Name = "PatientGroupName";
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // ForecastinfoID
            // 
            this.ForecastinfoID.DataPropertyName = "ForecastinfoID";
            this.ForecastinfoID.HeaderText = "ForecastinfoID";
            this.ForecastinfoID.Name = "ForecastinfoID";
            this.ForecastinfoID.Visible = false;
            // 
            // PatientPercentage
            // 
            this.PatientPercentage.DataPropertyName = "PatientPercentage";
            this.PatientPercentage.FillWeight = 126.5651F;
            this.PatientPercentage.HeaderText = "Ratio of the group from the total patient # ";
            this.PatientPercentage.MaxInputLength = 5;
            this.PatientPercentage.Name = "PatientPercentage";
            // 
            // PatientRatio
            // 
            this.PatientRatio.DataPropertyName = "PatientRatio";
            this.PatientRatio.FillWeight = 126.5651F;
            this.PatientRatio.HeaderText = "Calculated Value From Target Patient";
            this.PatientRatio.Name = "PatientRatio";
            this.PatientRatio.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(359, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Create patient group based on treatment characterstics";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Image")));
            this.btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrevious.Location = new System.Drawing.Point(12, 499);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(112, 23);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(1000, 499);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // DynamicPatientGroupRatio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 534);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DynamicPatientGroupRatio";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Patient Group Ratio";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPGroup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.DataGridView gvPGroup;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientGroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ForecastinfoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientPercentage;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientRatio;
    }
}