﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using LQT.Core.Util;
using LQT.Core.Domain;
using LQT.GUI.MorbidityUserCtr;
using LQT.Core.UserExceptions;

namespace LQT.GUI.Quantification
{
    public partial class NewMorbidityForm : Form
    {
        private NewMorbidityForecast _mforecast;
        private MorbidityCtrEnum _priviousCtr;
        private MorbidityCtrEnum _nextCtr;
        private NewBaseMorbidityControl _currentCtr;
        private InventoryAssumption _invAssumption;
        private string TempFile = Path.GetTempFileName();
        private Form _mdiparent;
        private bool _edited = false;
        private IList<NewMordidityCategorySite> _artSites;
        private IList<NewMorbiditySite> _aSites;
        private IList<ARTSite> _asite;

        public NewMorbidityForm(NewMorbidityForecast mforecast, Form mdiparent)
        {
            this._mforecast = mforecast;
            this._mdiparent = mdiparent;
            this._artSites = DataRepository.GetAllNewMordidityCategorySite(mforecast.Id);
            this._aSites = DataRepository.GetAllNewMorbiditySite(mforecast.Id);
            //this._asite = DataRepository.GetAllARTSite(mforecast.Id);

            if (mforecast.Id > 0)
            {
                this._invAssumption = DataRepository.GetInventoryAssumptionByForecastId(mforecast.Id);
            }

            if (_invAssumption == null)
            {
                this._invAssumption = new InventoryAssumption();
               // this._invAssumption.MorbidityForecast = _mforecast;
            }

            InitializeComponent();
            PopPeriod();
            NewPopPeriod();
            BindForecast();
            
        }

        public void ReInitMorbidityFrm()
        {
            this._mforecast = DataRepository.GetNewMorbidityForecastById(_mforecast.Id);
            this._artSites = DataRepository.GetAllNewMordidityCategorySite(_mforecast.Id);
            this._aSites = DataRepository.GetAllNewMorbiditySite(_mforecast.Id);
            this._invAssumption = DataRepository.GetInventoryAssumptionByForecastId(_mforecast.Id);

            if (_invAssumption == null)
            {
                this._invAssumption = new InventoryAssumption();
               // this._invAssumption.MorbidityForecast = _mforecast;
            }
        }

        public void ReInitMorbidityFrm(ref MorbidityForecast forecast, ref InventoryAssumption assumption, ref IList<NewMordidityCategorySite> artSites, ref IList<NewMorbiditySite> aSites)
        {
            ReInitMorbidityFrm();
           // forecast = _mforecast;
            assumption = _invAssumption;
            artSites = _artSites;
            aSites = _aSites;
        }
        
        private void PopPeriod()
        {
            //comBudgetend.Items.AddRange(Enum.GetNames(typeof(MonthNameEnum)));
            //comBudgetsart.Items.AddRange(Enum.GetNames(typeof(MonthNameEnum)));
        }
        
        private void BindForecast()
        {
            if (_mforecast.Id > 0)
            {
                //txtforecastid.Text = _mforecast.Title;
               // txtDescription.Text = _mforecast.Descritpion;
               // dtpDateofquan.Value = _mforecast.DateOfQuantification;

                //dtpForecastsdate.Value = _mforecast.SatartDate;
                //comBudgetsart.Text = _mforecast.StartBudgetPeriodEnum.ToString();
                //comBudgetend.Text = _mforecast.EndBudgetPeriodEnum.ToString();
                
                //if (_mforecast.TypeofAlgorithmEnum == AlgorithmType.Serial)
                //    rdbincludeCtrl.Checked = true;
                //else
                //    rdbNoCtrl.Checked = true;
                InitNavigation();
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            DateTime startdate = new DateTime();
            bool initnav = _mforecast.Id <= 0;
            try//b
            {
            if (txtforecastid.Text.Trim() == string.Empty)
            {
                throw new LQTUserException("Forecast ID can not be empty.");
            }
            if (cboscope.Text.Trim() == string.Empty)
            {
                throw new LQTUserException("Forecast Scope can not be empty.");
            }
            if (this.cboscope.Text == "CUSTOM")
            {
                if (txtScope.Text.Trim() == string.Empty)
                {
                    throw new LQTUserException("Custom Forecast Scope name can not be empty.");
                }
            }
            if (comPeriod.Text.Trim() == string.Empty)
            {
                throw new LQTUserException("Forecast Reporting period can not be empty.");
            }
            
            if (txtExtension.Text.Trim() == string.Empty)
            {
                throw new LQTUserException("Forecast Reporting period value can not be empty.");
            }
                //////////////// Forecast Start Date //////////////

            if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Bimonthly.ToString())
            {
                if (dtpstart.GetMonth == 0)
                {
                    MessageBox.Show("Month name can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (dtpstart.GetYear == -1)//b
                    {
                        MessageBox.Show("Year can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        startdate = new DateTime(dtpstart.GetYear, dtpstart.GetMonth, 1);
                        _mforecast.MonthInPeriod = 2;
                    }
                }
            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Monthly.ToString())
            {
                if (dtpstart.GetMonth == 0)
                {
                    MessageBox.Show("Month name can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (dtpstart.GetYear == -1)//b
                    {
                        MessageBox.Show("Year can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    startdate = new DateTime(dtpstart.GetYear, dtpstart.GetMonth, 1);
                    _mforecast.MonthInPeriod = 1;
                }
            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Quarterly.ToString())
            {
                if (dtpstart.GetQuarter == 0)
                {
                    MessageBox.Show("Quarter name can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (dtpstart.GetYear == -1)//b
                    {
                        MessageBox.Show("Year can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    startdate = new DateTime(dtpstart.GetYear, dtpstart.GetQuarter, 1);
                    _mforecast.MonthInPeriod = 3;
                }
            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Yearly.ToString())
            {
                if (dtpstart.GetYear == 0)
                {
                    MessageBox.Show("Year can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (dtpstart.GetYear == -1)//b
                    {
                        MessageBox.Show("Year can not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    startdate = new DateTime(dtpstart.GetYear, 1, 1);
                    _mforecast.MonthInPeriod = 12;
                }
            }
                _mforecast.ReportingPeriod = comPeriod.SelectedItem.ToString();
                _mforecast.ForecastCode=txtforecastid.Text;
                _mforecast.ScopeOfTheForecast=txtScope.Text;
                _mforecast.Period=comPeriod.SelectedItem.ToString();
                _mforecast.Extension = txtExtension.Text != "" ? int.Parse(this.txtExtension.Text) : 3;
                _mforecast.Status = ForecastStatusEnum.OPEN.ToString();
                _mforecast.ForecastDate=startdate;
                _mforecast.LastUpdated=DateTime.Now;

                if (rdbsitebysite.Checked)
                    _mforecast.ConductSitebySite=true;
                else if(rdbaggregated.Checked)
                    _mforecast.ConductSitebySite=false;

                if (rdbincludeCtrl.Checked)
                    _mforecast.IncludeControl=true;
                else if (rdbNoCtrl.Checked)
                    _mforecast.IncludeControl=false;
  
            
           /*

            _mforecast.SatartDate = dtpForecastsdate.Value;

                if (comBudgetsart.Text.Trim() != string.Empty)
            {
                _mforecast.StartBudgetPeriod = (int)Enum.Parse(typeof(MonthNameEnum), comBudgetsart.Text);
            }
                if (comBudgetend.Text.Trim() != string.Empty)
            {
                _mforecast.EndBudgetPeriod = (int)Enum.Parse(typeof(MonthNameEnum), comBudgetend.Text);
            }
             
                _mforecast.DateModified = DateTime.Now;
                _mforecast.TypeofAlgorithm = rdbSerial.Checked ? AlgorithmType.Serial.ToString() : AlgorithmType.Parallel.ToString();
            */
                DataRepository.SaveOrUpdateNewMorbidityForecast(_mforecast); 
                SaveForecast("Forecast Information was saved successfully.");
            if (initnav)
                InitNavigation();

            }
            catch (Exception ex)//b
            {
                new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();
            }

            
        }


        private bool SaveForecast(string msg)
        {
            try
            {
               ///DataRepository.SaveOrUpdateMorbidityForecast(_mforecast);
                ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo(msg, true);
                _edited = false;                
            }
            catch
            {
                MessageBox.Show("Error: Unable to save the forecast.", "Morbidity Quantification", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void butResult_Click(object sender, EventArgs e)
        {

        }

        private void MorbidityForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            NHibernateHelper.CloseSession();
        }

        private void MorbidityForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_edited)
            {
                System.Windows.Forms.DialogResult dr = MessageBox.Show("Do you want to save changes?", "Quantification Process", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    if (!SaveForecast("Forecast Information was saved successfully."))
                        e.Cancel = true;
                }
                else if (dr == System.Windows.Forms.DialogResult.Cancel)
                    e.Cancel = true;
            }
        }

        #region navigation.........

        private void InitNavigation()
        {
            if (_mforecast.StatusEnum == ForecastStatusEnum.CLOSED)
            {
                LoadDashBoard();
            }
            else
            {
                if (_mforecast.ConductSitebySite)
                    this._currentCtr = new NewSiteSelection(_mforecast, _aSites);
                else
                    this._currentCtr = new NewCatSelection(_mforecast, _artSites);
            }

            LoadCurrentCtr();
        }

        private void LoadCurrentCtr()
        {            
            panContainer.Controls.Clear();
            
            _priviousCtr = _currentCtr.PriviousCtr;
            _nextCtr = _currentCtr.NextCtr;
            lblCurrentCtr.Text = _currentCtr.Title;
            if (_currentCtr.EnableNextButton())
                butNext.Enabled = true;
            else
                butNext.Enabled = false;
            _currentCtr.NextButtonStatusChanged += new EventHandler<NextButtonStatusEventArgss>(_currentCtr_NextButtonStatusChanged);
            
            butBack.Enabled = _priviousCtr != MorbidityCtrEnum.Nothing;
            _currentCtr.MorbidityForm = this;
            _currentCtr.Dock = DockStyle.Fill;
            panContainer.Controls.Add(_currentCtr);
            ShowHelp(_currentCtr.Title, _currentCtr.Description);
        }

        void _currentCtr_NextButtonStatusChanged(object sender, NextButtonStatusEventArgss e)
        {
            butNext.Enabled = e.BoolValue;
            _nextCtr = _currentCtr.NextCtr;
        }

        private void butBack_Click(object sender, EventArgs e)
        {
            if (_currentCtr.DoSomthingBeforeUnload())
            {
                _currentCtr = GetMorbidityControl(_priviousCtr);
                LoadCurrentCtr();
            }
        }

        private void butNext_Click(object sender, EventArgs e)
        {
            if (_currentCtr.DoSomthingBeforeUnload())
            {
                _currentCtr = GetMorbidityControl(_nextCtr);
                LoadCurrentCtr();
            }

        }

        private NewBaseMorbidityControl GetMorbidityControl(MorbidityCtrEnum ctr)
        {
            NewBaseMorbidityControl mcontrol = null;
            switch (ctr)
            {
                case MorbidityCtrEnum.SiteSelection:
                    mcontrol = new NewSiteSelection(_mforecast, _aSites);
                    break;
                case MorbidityCtrEnum.CategorySelection:
                    mcontrol = new NewCatSelection(_mforecast, _artSites);
                    break;
               case MorbidityCtrEnum.OptRecentData:
                    mcontrol = new NewOptRecentData(_mforecast);
                    break;
               case MorbidityCtrEnum.OptPopulationBased:
                    mcontrol = new OptPopulationBased(_mforecast);
                    break;
               case MorbidityCtrEnum.OptTargetBased:
                    mcontrol = new OptTargetBased(_mforecast);
                    break;
               case MorbidityCtrEnum.OptDynamicBased:
                    mcontrol = new OptDynamicBased(_mforecast);
                    break;
               case MorbidityCtrEnum.OptPatientGroupChar:
                    mcontrol = new OptPatientGroupChar(_mforecast);
                    break;
               case MorbidityCtrEnum.OptOtherGenralAssumption:
                    mcontrol = new OptOtherGenralAssumption(_mforecast, _currentCtr.PMorbidity);
                    break;
               case MorbidityCtrEnum.NewGeneralProtocol:
                    mcontrol = new NewGeneralProtocol(_mforecast);
                    break;
               case MorbidityCtrEnum.NewRapidTestProtocol:
                    mcontrol = new NewRapidTestProtocol();
                    break;
               case MorbidityCtrEnum.NewReviewadjustment:
                    mcontrol = new NewReviewadjustment();
                    break;
               case MorbidityCtrEnum.NewCheckupForm:
                    mcontrol = new NewCheckupForm();
                    break;
                /*case MorbidityCtrEnum.OptArtPatientTarget:
                    mcontrol = new OptArtPatientTarget(_mforecast);
                    break;
                case MorbidityCtrEnum.OpEverStartedPatientTarget:
                    mcontrol = new OpEverStartedPatientTarget(_mforecast);
                    break;
                case MorbidityCtrEnum.SiteTargetCalculator:
                    mcontrol = new SiteTargetCalculator(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.PatientNumbersSites:
                    mcontrol = new PatientNumbersSites(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.OptPreTreatmentPatientTargets:
                    mcontrol = new OptPreTreatmentPatientTargets(_mforecast);
                    break;
                case MorbidityCtrEnum.SiteTargetCalculatorPreART:
                    mcontrol = new SiteTargetCalculatorPreART(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.PreTxNumbersSites:
                    mcontrol = new PreTxNumbersSites(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.TestingInformation:
                    mcontrol = new TestingInformation(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.TestingEfficiency:
                    mcontrol = new TestingEfficiency(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.AdultPatientBehavior:
                    mcontrol = new AdultPatientBehavior(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.PediatricPatientBehavior:
                    mcontrol = new PediatricPatientBehavior(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.EverStartedRecentData:
                    mcontrol = new EverStartedRecentData(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.EverStartedOldData:
                    mcontrol = new EverStartedOldData(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.EverStartedNoData:
                    mcontrol = new EverStartedNoData(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.RapidTestSerial:
                    mcontrol = new RapidTestSerial(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.RapidTestParallel:
                    mcontrol = new RapidTestParallel(_mforecast, _artSites);
                    break;
                case MorbidityCtrEnum.InvAssumption:
                    mcontrol = new InvAssumption(_mforecast, _invAssumption);
                    break;
                case MorbidityCtrEnum.CheckupForm:
                    mcontrol = new CheckupForm(_mforecast,_invAssumption, _artSites, _mdiparent);
                    break;                     
                case MorbidityCtrEnum.CalculateForm:
                    mcontrol = new CalculateForm(_mforecast);
                    break;
                case MorbidityCtrEnum.Dashboard:
                    mcontrol = new DashboardForm(_mforecast);
                    break;
                case MorbidityCtrEnum.RapidTestProtocol:
                    mcontrol = new RapidTestProtocol(_mforecast);
                    break;
                case MorbidityCtrEnum.TestProtocolsCd4:
                    mcontrol = new TestProtocols(_mforecast, ClassOfMorbidityTestEnum.CD4, _mdiparent);
                    break;
                case MorbidityCtrEnum.TestProtocolsChem:
                    mcontrol = new TestProtocols(_mforecast, ClassOfMorbidityTestEnum.Chemistry, _mdiparent);
                    break;
                case MorbidityCtrEnum.TestProtocolsHem:
                    mcontrol = new TestProtocols(_mforecast, ClassOfMorbidityTestEnum.Hematology, _mdiparent);
                    break;
                case MorbidityCtrEnum.TestProtocolsVir:
                    mcontrol = new TestProtocols(_mforecast, ClassOfMorbidityTestEnum.ViralLoad, _mdiparent);
                    break;
                case MorbidityCtrEnum.TestProtocolsOther:
                    mcontrol = new TestProtocols(_mforecast, ClassOfMorbidityTestEnum.OtherTest, _mdiparent);
                    break;*/
            }

            if (ctr == MorbidityCtrEnum.SiteSelection)
                butGohome.Enabled = false;
            else
                butGohome.Enabled = true;

            return mcontrol;
        }

        #endregion


        private void ShowHelp(string header, string detail)
        {
            // Read the about HTML from the assembly
            string html = (new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("LQT.GUI.Morbidity.htm"))).ReadToEnd();

            // Replace sections with appropriate data
            html = html.Replace("{header}", header);

            html = html.Replace("{detail}", detail);
            // Save the temp file so the web browser has a target to navigate to
            File.WriteAllText(TempFile, html);

            webBrowser1.Navigate(TempFile);
            webBrowser1.Refresh();
        }

     
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            File.Delete(TempFile);
        }

        private void lblColapse_Click(object sender, EventArgs e)
        {
            lblColapse.Visible = false;
            lblExpand.Visible = true;
            splitContainer2.Panel2Collapsed = true;
        }

        private void lblExpand_Click(object sender, EventArgs e)
        {
            lblColapse.Visible = true;
            lblExpand.Visible = false;
            splitContainer2.Panel2Collapsed = false;
        }

        public void LoadDashBoard()
        {
            if (_mforecast.StatusEnum == ForecastStatusEnum.CLOSED)
            {
               // this._currentCtr = new NewDashboardForm(_mforecast);


                lblColapse.Visible = false;
                lblExpand.Visible = true;
                splitContainer2.Panel2Collapsed = true;
                LoadCurrentCtr();
            }
        }

        public void LoadForecastResult()
        {
            //this._currentCtr = new NewCalculateForm(_mforecast);
            LoadCurrentCtr();
        }

        private void dtpForecastsdate_ValueChanged(object sender, EventArgs e)
        {
           /* MonthNameEnum month = (MonthNameEnum)Enum.ToObject(typeof(MonthNameEnum), dtpForecastsdate.Value.Month);
            comBudgetsart.Text = month.ToString();

            if (dtpForecastsdate.Value.Month - 1 == 0)
                month = (MonthNameEnum)Enum.ToObject(typeof(MonthNameEnum), 12);
            else
                month = (MonthNameEnum)Enum.ToObject(typeof(MonthNameEnum), dtpForecastsdate.Value.Month - 1);
            comBudgetend.Text = month.ToString();
            * */
        }

        private void butGohome_Click(object sender, EventArgs e)
        {
            if (_currentCtr.DoSomthingBeforeUnload())
            {
                //this._currentCtr = new NewSiteSelection(_mforecast, _artSites);
                LoadCurrentCtr();
            }
        }

        private void cboscope_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (this.cboscope.Text == "NATIONAL" || this.cboscope.Text == "PROGRAM" || this.cboscope.Text == "SITE" || this.cboscope.Text == "GLOBAL")
            {
                this.txtScope.Visible = false;
                this.txtScope.Text = this.cboscope.Text;
            }
            else
            {
                //if (_forecastInfo != null)
                //    this.txtScope.Text = _forecastInfo.ScopeOfTheForecast;
                //else
                //    this.txtScope.Text = this.cboscope.Text;


                this.txtScope.Visible = true;
            }
        }

        private void NewPopPeriod()
        {
            string[] periods = Enum.GetNames(typeof(ForecastPeriodEnum));
            comPeriod.Items.Clear();
            //comslowmovingperiod.Items.Clear();
            for (int i = 0; i < periods.Length; i++)
            {
                comPeriod.Items.Add(periods[i]);
                //comslowmovingperiod.Items.Add(periods[i]);
            }
        }

        private void comPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Bimonthly.ToString())
            {
                lblfperiod.Text = "Forecast in Bimonthly:";
                dtpstart.DisableMonthCom = false;
                dtpstart.PopQuarter = false;
                dtpstart.PopMonthOrQuarterValue();
               

            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Monthly.ToString())
            {
                lblfperiod.Text = "Forecast in Months:";
                dtpstart.DisableMonthCom = false;
                dtpstart.PopQuarter = false;
                dtpstart.PopMonthOrQuarterValue();
               
            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Quarterly.ToString())
            {
                lblfperiod.Text = "Forecast in Quarters:";
                dtpstart.DisableMonthCom = false;
                dtpstart.PopQuarter = true;
                dtpstart.PopMonthOrQuarterValue();
               
            }
            else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Yearly.ToString())
            {
                lblfperiod.Text = "Forecast in Years:";
                dtpstart.DisableMonthCom = true;
                dtpstart.PopMonthOrQuarterValue();
                
            }

            dtpstart.SetDefaultDate();
        }

        private void txtExtension_KeyPress(object sender, KeyPressEventArgs e)
        {
            int x = e.KeyChar;

            if ((x >= 48 && x <= 57) || (x == 8))
            {
                e.Handled = false;
            }
            else
                e.Handled = true;
        }
    }

}
