﻿namespace LQT.GUI.Quantification
{
    partial class NewMorbidityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewMorbidityForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.butSave = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.imageButton6 = new LQT.GUI.ImageButton();
            this.rdbNoCtrl = new System.Windows.Forms.RadioButton();
            this.rdbincludeCtrl = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbaggregated = new System.Windows.Forms.RadioButton();
            this.rdbsitebysite = new System.Windows.Forms.RadioButton();
            this.imageButton4 = new LQT.GUI.ImageButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.imageButton10 = new LQT.GUI.ImageButton();
            this.imageButton9 = new LQT.GUI.ImageButton();
            this.imageButton1 = new LQT.GUI.ImageButton();
            this.txtExtension = new System.Windows.Forms.TextBox();
            this.lblfperiod = new System.Windows.Forms.Label();
            this.dtpstart = new LQT.GUI.DurationPicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comPeriod = new System.Windows.Forms.ComboBox();
            this.imageButton8 = new LQT.GUI.ImageButton();
            this.cboscope = new System.Windows.Forms.ComboBox();
            this.txtScope = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imageButton7 = new LQT.GUI.ImageButton();
            this.txtforecastid = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panContainer = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblExpand = new System.Windows.Forms.Label();
            this.lblColapse = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.butGohome = new System.Windows.Forms.Button();
            this.lblCurrentCtr = new System.Windows.Forms.Label();
            this.butNext = new System.Windows.Forms.Button();
            this.butBack = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton6)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton7)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer1.Panel1.Margin = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel2.Controls.Add(this.splitter1);
            this.splitContainer1.Panel2.Controls.Add(this.panel4);
            this.splitContainer1.Size = new System.Drawing.Size(784, 613);
            this.splitContainer1.SplitterDistance = 265;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(265, 613);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.butSave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(262, 34);
            this.panel2.TabIndex = 1;
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(8, 1);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(110, 30);
            this.butSave.TabIndex = 2;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.groupBox3);
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 43);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(262, 567);
            this.panel3.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.imageButton6);
            this.groupBox3.Controls.Add(this.rdbNoCtrl);
            this.groupBox3.Controls.Add(this.rdbincludeCtrl);
            this.groupBox3.Location = new System.Drawing.Point(5, 348);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(250, 56);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Control Included";
            // 
            // imageButton6
            // 
            this.imageButton6.Description = "Select option if controls are included in the forecast";
            this.imageButton6.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton6.Location = new System.Drawing.Point(114, 0);
            this.imageButton6.Name = "imageButton6";
            this.imageButton6.Size = new System.Drawing.Size(16, 16);
            this.imageButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton6.TabIndex = 2;
            this.imageButton6.TabStop = false;
            this.imageButton6.Title = "Helpful Tips";
            // 
            // rdbNoCtrl
            // 
            this.rdbNoCtrl.AutoSize = true;
            this.rdbNoCtrl.Location = new System.Drawing.Point(75, 20);
            this.rdbNoCtrl.Name = "rdbNoCtrl";
            this.rdbNoCtrl.Size = new System.Drawing.Size(39, 17);
            this.rdbNoCtrl.TabIndex = 11;
            this.rdbNoCtrl.Text = "No";
            this.rdbNoCtrl.UseVisualStyleBackColor = true;
            // 
            // rdbincludeCtrl
            // 
            this.rdbincludeCtrl.AutoSize = true;
            this.rdbincludeCtrl.Checked = true;
            this.rdbincludeCtrl.Location = new System.Drawing.Point(13, 21);
            this.rdbincludeCtrl.Name = "rdbincludeCtrl";
            this.rdbincludeCtrl.Size = new System.Drawing.Size(43, 17);
            this.rdbincludeCtrl.TabIndex = 10;
            this.rdbincludeCtrl.TabStop = true;
            this.rdbincludeCtrl.Text = "Yes";
            this.rdbincludeCtrl.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbaggregated);
            this.groupBox2.Controls.Add(this.rdbsitebysite);
            this.groupBox2.Controls.Add(this.imageButton4);
            this.groupBox2.Location = new System.Drawing.Point(5, 272);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(250, 70);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Conduct Forecast In:";
            // 
            // rdbaggregated
            // 
            this.rdbaggregated.AutoSize = true;
            this.rdbaggregated.Location = new System.Drawing.Point(9, 42);
            this.rdbaggregated.Name = "rdbaggregated";
            this.rdbaggregated.Size = new System.Drawing.Size(146, 17);
            this.rdbaggregated.TabIndex = 17;
            this.rdbaggregated.Text = "Aggregated (groupd sites)";
            this.rdbaggregated.UseVisualStyleBackColor = true;
            // 
            // rdbsitebysite
            // 
            this.rdbsitebysite.AutoSize = true;
            this.rdbsitebysite.Checked = true;
            this.rdbsitebysite.Location = new System.Drawing.Point(9, 19);
            this.rdbsitebysite.Name = "rdbsitebysite";
            this.rdbsitebysite.Size = new System.Drawing.Size(78, 17);
            this.rdbsitebysite.TabIndex = 16;
            this.rdbsitebysite.TabStop = true;
            this.rdbsitebysite.Text = "Site-by-Site";
            this.rdbsitebysite.UseVisualStyleBackColor = true;
            // 
            // imageButton4
            // 
            this.imageButton4.Description = "Select category type for the Forecast. This should correspond to site selection o" +
                "ption and required reporting output";
            this.imageButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton4.Location = new System.Drawing.Point(114, 0);
            this.imageButton4.Name = "imageButton4";
            this.imageButton4.Size = new System.Drawing.Size(16, 16);
            this.imageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton4.TabIndex = 15;
            this.imageButton4.TabStop = false;
            this.imageButton4.Title = "Helpful Tips";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.imageButton10);
            this.groupBox1.Controls.Add(this.imageButton9);
            this.groupBox1.Controls.Add(this.imageButton1);
            this.groupBox1.Controls.Add(this.txtExtension);
            this.groupBox1.Controls.Add(this.lblfperiod);
            this.groupBox1.Controls.Add(this.dtpstart);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.comPeriod);
            this.groupBox1.Controls.Add(this.imageButton8);
            this.groupBox1.Controls.Add(this.cboscope);
            this.groupBox1.Controls.Add(this.txtScope);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.imageButton7);
            this.groupBox1.Controls.Add(this.txtforecastid);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 263);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Forecast Description";
            // 
            // imageButton10
            // 
            this.imageButton10.Description = "";
            this.imageButton10.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton10.Location = new System.Drawing.Point(75, 207);
            this.imageButton10.Name = "imageButton10";
            this.imageButton10.Size = new System.Drawing.Size(16, 16);
            this.imageButton10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton10.TabIndex = 29;
            this.imageButton10.TabStop = false;
            this.imageButton10.Title = "Forecast ID";
            // 
            // imageButton9
            // 
            this.imageButton9.Description = "";
            this.imageButton9.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton9.Location = new System.Drawing.Point(121, 163);
            this.imageButton9.Name = "imageButton9";
            this.imageButton9.Size = new System.Drawing.Size(16, 16);
            this.imageButton9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton9.TabIndex = 28;
            this.imageButton9.TabStop = false;
            this.imageButton9.Title = "Forecast ID";
            // 
            // imageButton1
            // 
            this.imageButton1.Description = "";
            this.imageButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton1.Location = new System.Drawing.Point(111, 115);
            this.imageButton1.Name = "imageButton1";
            this.imageButton1.Size = new System.Drawing.Size(16, 16);
            this.imageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton1.TabIndex = 27;
            this.imageButton1.TabStop = false;
            this.imageButton1.Title = "Forecast ID";
            // 
            // txtExtension
            // 
            this.txtExtension.Location = new System.Drawing.Point(9, 182);
            this.txtExtension.Name = "txtExtension";
            this.txtExtension.Size = new System.Drawing.Size(235, 20);
            this.txtExtension.TabIndex = 26;
            this.txtExtension.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtExtension_KeyPress);
            // 
            // lblfperiod
            // 
            this.lblfperiod.AutoSize = true;
            this.lblfperiod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfperiod.Location = new System.Drawing.Point(6, 166);
            this.lblfperiod.Name = "lblfperiod";
            this.lblfperiod.Size = new System.Drawing.Size(117, 13);
            this.lblfperiod.TabIndex = 25;
            this.lblfperiod.Text = "Forecasting Period:";
            // 
            // dtpstart
            // 
            this.dtpstart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtpstart.Location = new System.Drawing.Point(9, 226);
            this.dtpstart.Margin = new System.Windows.Forms.Padding(0);
            this.dtpstart.Name = "dtpstart";
            this.dtpstart.Size = new System.Drawing.Size(229, 24);
            this.dtpstart.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Start Date:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 117);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Reporting Period:";
            // 
            // comPeriod
            // 
            this.comPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPeriod.FormattingEnabled = true;
            this.comPeriod.Location = new System.Drawing.Point(9, 133);
            this.comPeriod.Name = "comPeriod";
            this.comPeriod.Size = new System.Drawing.Size(235, 21);
            this.comPeriod.TabIndex = 21;
            this.comPeriod.SelectedIndexChanged += new System.EventHandler(this.comPeriod_SelectedIndexChanged);
            // 
            // imageButton8
            // 
            this.imageButton8.Description = "Select a scope to categorized a forecast and easy reporting filtering";
            this.imageButton8.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton8.Location = new System.Drawing.Point(144, 67);
            this.imageButton8.Name = "imageButton8";
            this.imageButton8.Size = new System.Drawing.Size(16, 16);
            this.imageButton8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton8.TabIndex = 20;
            this.imageButton8.TabStop = false;
            this.imageButton8.Title = "Forecast ID";
            // 
            // cboscope
            // 
            this.cboscope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboscope.FormattingEnabled = true;
            this.cboscope.Items.AddRange(new object[] {
            "NATIONAL",
            "PROGRAM",
            "SITE",
            "GLOBAL",
            "CUSTOM"});
            this.cboscope.Location = new System.Drawing.Point(9, 85);
            this.cboscope.Name = "cboscope";
            this.cboscope.Size = new System.Drawing.Size(121, 21);
            this.cboscope.TabIndex = 19;
            this.cboscope.SelectedIndexChanged += new System.EventHandler(this.cboscope_SelectedIndexChanged);
            // 
            // txtScope
            // 
            this.txtScope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScope.Location = new System.Drawing.Point(136, 86);
            this.txtScope.Name = "txtScope";
            this.txtScope.Size = new System.Drawing.Size(108, 20);
            this.txtScope.TabIndex = 18;
            this.txtScope.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Scope of the Forecast:";
            // 
            // imageButton7
            // 
            this.imageButton7.Description = "Enter a unique identifier or code";
            this.imageButton7.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton7.Location = new System.Drawing.Point(81, 17);
            this.imageButton7.Name = "imageButton7";
            this.imageButton7.Size = new System.Drawing.Size(16, 16);
            this.imageButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton7.TabIndex = 7;
            this.imageButton7.TabStop = false;
            this.imageButton7.Title = "Forecast ID";
            // 
            // txtforecastid
            // 
            this.txtforecastid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtforecastid.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtforecastid.Location = new System.Drawing.Point(9, 36);
            this.txtforecastid.Name = "txtforecastid";
            this.txtforecastid.Size = new System.Drawing.Size(235, 23);
            this.txtforecastid.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Forecast ID:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.splitContainer2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 42);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 571F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(517, 571);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.panContainer);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splitContainer2.Panel2.Controls.Add(this.webBrowser1);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(2);
            this.splitContainer2.Size = new System.Drawing.Size(494, 565);
            this.splitContainer2.SplitterDistance = 306;
            this.splitContainer2.SplitterWidth = 3;
            this.splitContainer2.TabIndex = 0;
            // 
            // panContainer
            // 
            this.panContainer.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panContainer.Location = new System.Drawing.Point(0, 0);
            this.panContainer.Name = "panContainer";
            this.panContainer.Size = new System.Drawing.Size(306, 565);
            this.panContainer.TabIndex = 0;
            // 
            // webBrowser1
            // 
            this.webBrowser1.AllowNavigation = false;
            this.webBrowser1.AllowWebBrowserDrop = false;
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(2, 2);
            this.webBrowser1.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(181, 561);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblExpand);
            this.panel5.Controls.Add(this.lblColapse);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(497, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(20, 571);
            this.panel5.TabIndex = 1;
            // 
            // lblExpand
            // 
            this.lblExpand.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblExpand.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblExpand.Image = ((System.Drawing.Image)(resources.GetObject("lblExpand.Image")));
            this.lblExpand.Location = new System.Drawing.Point(1, 89);
            this.lblExpand.Margin = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.lblExpand.Name = "lblExpand";
            this.lblExpand.Size = new System.Drawing.Size(18, 18);
            this.lblExpand.TabIndex = 4;
            this.lblExpand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.lblExpand, "Show");
            this.lblExpand.Visible = false;
            this.lblExpand.Click += new System.EventHandler(this.lblExpand_Click);
            // 
            // lblColapse
            // 
            this.lblColapse.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblColapse.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.lblColapse.Image = ((System.Drawing.Image)(resources.GetObject("lblColapse.Image")));
            this.lblColapse.Location = new System.Drawing.Point(1, 89);
            this.lblColapse.Margin = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.lblColapse.Name = "lblColapse";
            this.lblColapse.Size = new System.Drawing.Size(18, 18);
            this.lblColapse.TabIndex = 2;
            this.lblColapse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip1.SetToolTip(this.lblColapse, "Hide");
            this.lblColapse.Click += new System.EventHandler(this.lblColapse_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(0, 11);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 98);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "Description panel");
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 40);
            this.splitter1.Margin = new System.Windows.Forms.Padding(0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(517, 2);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(3);
            this.panel4.Size = new System.Drawing.Size(517, 40);
            this.panel4.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.butGohome);
            this.panel1.Controls.Add(this.lblCurrentCtr);
            this.panel1.Controls.Add(this.butNext);
            this.panel1.Controls.Add(this.butBack);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(511, 34);
            this.panel1.TabIndex = 1;
            // 
            // butGohome
            // 
            this.butGohome.Location = new System.Drawing.Point(51, 2);
            this.butGohome.Name = "butGohome";
            this.butGohome.Size = new System.Drawing.Size(75, 28);
            this.butGohome.TabIndex = 1;
            this.butGohome.Text = "First";
            this.butGohome.UseVisualStyleBackColor = true;
            this.butGohome.Click += new System.EventHandler(this.butGohome_Click);
            // 
            // lblCurrentCtr
            // 
            this.lblCurrentCtr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCurrentCtr.Location = new System.Drawing.Point(213, 6);
            this.lblCurrentCtr.Name = "lblCurrentCtr";
            this.lblCurrentCtr.Size = new System.Drawing.Size(312, 23);
            this.lblCurrentCtr.TabIndex = 4;
            this.lblCurrentCtr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butNext
            // 
            this.butNext.Enabled = false;
            this.butNext.Location = new System.Drawing.Point(531, 3);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(75, 28);
            this.butNext.TabIndex = 3;
            this.butNext.Text = "Next";
            this.butNext.UseVisualStyleBackColor = true;
            this.butNext.Click += new System.EventHandler(this.butNext_Click);
            // 
            // butBack
            // 
            this.butBack.Enabled = false;
            this.butBack.Location = new System.Drawing.Point(132, 3);
            this.butBack.Name = "butBack";
            this.butBack.Size = new System.Drawing.Size(75, 28);
            this.butBack.TabIndex = 2;
            this.butBack.Text = "Previous";
            this.butBack.UseVisualStyleBackColor = true;
            this.butBack.Click += new System.EventHandler(this.butBack_Click);
            // 
            // NewMorbidityForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(784, 613);
            this.Controls.Add(this.splitContainer1);
            this.Name = "NewMorbidityForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Morbidity Quantification";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MorbidityForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MorbidityForm_FormClosed);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton6)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton7)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panContainer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.TextBox txtforecastid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCurrentCtr;
        private System.Windows.Forms.Button butNext;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblColapse;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblExpand;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbNoCtrl;
        private System.Windows.Forms.RadioButton rdbincludeCtrl;
        private ImageButton imageButton4;
        private ImageButton imageButton6;
        private System.Windows.Forms.Button butGohome;
        private ImageButton imageButton7;
        private System.Windows.Forms.ComboBox cboscope;
        private System.Windows.Forms.TextBox txtScope;
        private System.Windows.Forms.Label label1;
        private ImageButton imageButton8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comPeriod;
        private DurationPicker dtpstart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtExtension;
        private System.Windows.Forms.Label lblfperiod;
        private ImageButton imageButton10;
        private ImageButton imageButton9;
        private ImageButton imageButton1;
        private System.Windows.Forms.RadioButton rdbaggregated;
        private System.Windows.Forms.RadioButton rdbsitebysite;

    }
}