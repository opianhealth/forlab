﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Domain;
using LQT.Core.Util;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace LQT.GUI.Quantification
{
    public partial class FrmDynamicPatientAssumptions : Form
    {
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private Form _mdiparent;
        public static Int64 _totalTarget = 0;
        private ForecastInfo _mMforecastinfo;
        private string sqlretrieve = "";
        DataTable Dt = new DataTable();
        DataTable mainDt = new DataTable();
        DataTable _Dtpatient1 = new DataTable();
        public SqlConnection sqlConnection = ConnectionManager.GetInstance().GetSqlConnection();
        private string UseOn = "";

        public FrmDynamicPatientAssumptions(Form mdiparent, ForecastInfo finfo, DataTable Dtpatient1, Int64 totalTarget, DataTable Dt1)
        {
            InitializeComponent();
            this._mMforecastinfo = finfo;
            this._mdiparent = mdiparent;
            mainDt = Dt1;
            _totalTarget = totalTarget;
            _Dtpatient1 = Dtpatient1;
            btnPrevious.TabStop = false;
            btnPrevious.FlatStyle = FlatStyle.Flat;
            btnPrevious.FlatAppearance.BorderSize = 0;
            btnPrevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            if (_mMforecastinfo.ForecastType == "S")
                UseOn = MorbidityVariableUsage.OnEachSite.ToString();
            else
                UseOn = MorbidityVariableUsage.OnAggSite.ToString();
            // _mMGeneralAssumption = DataRepository.GetAllGeneralDynamicAssumptionByProgram(_mMforecastinfo.ProgramId, UseOn);
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Patient_Number_Assumption"), _mMforecastinfo.ProgramId, UseOn);
            this.Top = 120;

            this.Left = 250;
          
            fillGrid(_mMforecastinfo.Id);
            
        }

     
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
               
            string newvariable = "";
            sqlretrieve = "";
            DataTable dt1 = new System.Data.DataTable();
           
            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 1 && gAssumption.UseOn.ToString() == UseOn)
                {

                    sqlretrieve += "[" + gAssumption.VariableName + "],";
                    newvariable += "0,";

                }
            }
            sqlretrieve = sqlretrieve.TrimEnd(',');
            newvariable = newvariable.TrimEnd(',');
            try
            {
                if (_mMforecastinfo.ForecastType == "S")
                {
                    _sql = "SELECT p.ID,p.ForecastinfoID,p.SiteID as CategoryID,si.SiteName as SiteCategoryName," + sqlretrieve + " FROM PatientAssumption p";
                    _sql += " inner  join [ForecastSiteInfo] sc on p.SiteID=sc.SiteId  inner join [Site] si on sc.SiteId=si.SiteID where p.ForecastinfoID=" + ForecastinfoID + "";
                    _sql += " union ";
                    _sql += "select 0 as ID,ForecastinfoID,sc.SiteID,si.SiteName as SiteCategoryName," + newvariable + " from [ForecastSiteInfo] sc ";
                    _sql += " inner join [Site] si on sc.SiteId=si.SiteID where [ForecastinfoID]=" + ForecastinfoID + "";
                    _sql += " and sc.SiteId not in (Select SiteID from PatientAssumption where [ForecastinfoID]=" + ForecastinfoID + ")";
                }

                else if (_mMforecastinfo.ForecastType == "C")
                {
                       _sql = "SELECT p.ID,p.ForecastinfoID,p.CategoryID as CategoryID,sc.SiteCategoryName as SiteCategoryName," + sqlretrieve + " FROM PatientAssumption p";
                    _sql += " inner  join ForecastCategoryInfo sc on p.CategoryID=sc.SiteCategoryId where p.ForecastinfoID=" + ForecastinfoID + "";
                    _sql += "union ";
                    _sql += "select 0 as ID,ForecastinfoID,SiteCategoryID,SiteCategoryName," + newvariable + " from ForecastCategoryInfo where [ForecastinfoID]="+ForecastinfoID+" and SiteCategoryID not in (Select CategoryId from PatientAssumption where [ForecastinfoID]=" + ForecastinfoID + ")";
                }

                Dt = InsertRepository.SelectData(sqlConnection, _sql);
                gvPAssumptions.DataSource = Dt;
                gvPAssumptions.Columns[0].Visible = false;
                gvPAssumptions.Columns[1].Visible = false;
                gvPAssumptions.Columns[2].Visible = false;
                gvPAssumptions.Columns[3].ReadOnly = true;
            }
            catch (Exception ex)
            {
          
            }
        }

        private void gvPAssumptions_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int column = gvPAssumptions.CurrentCellAddress.X;
            string senderText = (sender as TextBox).Text;
            string senderName = (sender as TextBox).Name;
            string[] splitByDecimal = senderText.Split('.');
            int cursorPosition = (sender as TextBox).SelectionStart;

            if (column != 2)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }

            }
            else
            {
                var regex = new Regex(@"[^a-zA-Z0-9\s]");
                if (regex.IsMatch(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsControl(e.KeyChar) && senderText.IndexOf('.') < cursorPosition && splitByDecimal.Length > 1 && splitByDecimal[1].Length == 2)
            {
                e.Handled = true;
            }
       
        }

        private void gvPAssumptions_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex > 3)
            {
                if (Convert.ToDouble(e.FormattedValue) == 0)
                {

                    e.Cancel = true;
                    gvPAssumptions.Rows[e.RowIndex].ErrorText = "The value  must be greater than zero";

                    MessageBox.Show("The value  must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            string columns = "";
            string columnsvalues = "";
            string _sql = "";
            try
            {
                columns = "ForecastinfoID,CategoryID,SiteID,";
                foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
                {
                    if (gAssumption.AssumptionType == 1 && gAssumption.UseOn.ToString() == UseOn)
                    {
                        columns += "[" + gAssumption.VariableName + "],";
                    }
                }
                columns = columns.TrimEnd(',');
                if (!checkGridRecord() == false)
                {
                    for (int i = 0; i < gvPAssumptions.Rows.Count; i++)
                    {
                        _sql = "Delete from [PatientAssumption] where ID=" + gvPAssumptions.Rows[i].Cells["ID"].Value + " ";
                        InsertRepository.ExcuteScripts(sqlConnection, _sql);
                        if (_mMforecastinfo.ForecastType=="S")
                            columnsvalues = Convert.ToString(_mMforecastinfo.Id) + ",1," + gvPAssumptions.Rows[i].Cells["CategoryID"].Value.ToString() + ",";
                        else
                            columnsvalues = Convert.ToString(_mMforecastinfo.Id) + "," + gvPAssumptions.Rows[i].Cells["CategoryID"].Value.ToString() +","+ "1,";
                        for (int j = 4; j < gvPAssumptions.Rows[i].Cells.Count ; j++)
                        {

                            columnsvalues += Convert.ToString(gvPAssumptions.Rows[i].Cells[j].Value) + ",";

                        }
                        columnsvalues = columnsvalues.TrimEnd(',');
                        _sql = "INSERT INTO [PatientAssumption] (" + columns + ") VALUES(" + columnsvalues + ")";
                        InsertRepository.ExcuteScripts(sqlConnection, _sql);
                    } 
                    this.Hide();
                    FrmDynamicProductRAssumptions frm = new FrmDynamicProductRAssumptions(_mdiparent, _mMforecastinfo, _Dtpatient1, _totalTarget, mainDt);
                    frm.ShowDialog();
                    this.Close();
                }
            }
            catch (Exception ex)
            { 
            
            }
        }

        private bool checkGridRecord()
        {
            foreach (DataGridViewRow row in gvPAssumptions.Rows)
            {
                for (int i = 4; i < row.Cells.Count - 1; i++)
                {

                    if (Convert.ToDouble(row.Cells[i].Value) == 0.00 )
                    {
                        MessageBox.Show("The value of Variables must be a greater then Zero");
                        return false;
                    }
                }
            }
            return true;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmDynamicTestTestingprotocol frm = new FrmDynamicTestTestingprotocol(_mdiparent, _mMforecastinfo.Id, "E", _Dtpatient1, _mMforecastinfo.StartDate.ToString(), _mMforecastinfo.LastUpdated.ToString(), _mMforecastinfo.Period, _totalTarget, mainDt, _mMforecastinfo);
            // Form1 frm = new Form1();
            frm.ShowDialog();
            this.Close();
        }
    }
}
