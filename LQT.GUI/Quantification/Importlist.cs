﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LQT.Core.Domain;

namespace LQT.GUI.Quantification
{
    class Importlist
    {
        public class ImportTestData
        {
            private string _testname;
            //private string _groupname;
            private string _areaname;
            private int _rowno;
            private TestingArea _testarea;

            private bool _haserror = false;
            private bool _isexist = false;

            public ImportTestData(string tname, string aname, int rowno)
            {
                _testname = tname;
                //_groupname = gname;
                _areaname = aname;
                _rowno = rowno;
            }

            public string TestName
            {
                get { return _testname; }
            }
            //public string GroupName
            //{
            //    get { return _groupname; }
            //}
            public string AreaName
            {
                get { return _areaname; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
            public TestingArea TestArea
            {
                get { return _testarea; }
                set { _testarea = value; }
            }


        }
        public class ImportProductData
        {
            private string _proname;
            private string _catname;
            private string _serial;
            private string _unit;
            private int _size;
            private decimal _price;
            private int _rowno;
            private ProductType _protype;
            private bool _haserror = false;
            private bool _isexist = false;
            private string _rapidTest;//b
            private string _specification;//b
            private int _minSize;//b
            private DateTime _pricedate = DateTime.Now;

            public ImportProductData(string pname, string cname, string serial, string unit, int size, decimal price, int rowno, string specification, int minsize, string rapidTest, DateTime pricedate)
            {
                _proname = pname;
                _catname = cname;
                _serial = serial;
                _unit = unit;
                _size = size;
                _price = price;
                _rowno = rowno;
                _rapidTest = rapidTest;
                _specification = specification;
                _minSize = minsize;
                _pricedate = pricedate;
            }

            public string ProductName
            {
                get { return _proname; }
            }
            public string CategoryName
            {
                get { return _catname; }
            }
            public string Serial
            {
                get { return _serial; }
            }
            public string BasicUnit
            {
                get { return _unit; }
            }
            public int Packsize
            {
                get { return _size; }
            }
            public decimal Price
            {
                get { return _price; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public ProductType Category
            {
                get { return _protype; }
                set { _protype = value; }
            }


            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
            public string RapidTest
            {
                get { return _rapidTest; }
            }
            public string Specification
            {
                get { return _specification; }
            }
            public int minSize
            {
                get { return _minSize; }
            }

            public DateTime PriceDate
            {
                get { return _pricedate; }
            }
        }
        public class SiteInstrumentImportData
        {
            private string _tArea;
            private string _regionName;
            private string _siteName;
            private string _instrumentName;
            private int _quantity;
            private int _percentRun;

            private int _rowno;
            private bool _hasError = false;
            private bool _isexist = false;


            public SiteInstrumentImportData(int rowno, string rname, string sname, string tarea, string instname, int quantity, int percentrun)
            {
                this._rowno = rowno;
                this._regionName = rname;
                this._siteName = sname;
                this._tArea = tarea;
                this._instrumentName = instname;
                this._quantity = quantity;
                this._percentRun = percentrun;

            }

            public string InstrumentName
            {
                get { return _instrumentName; }
                set { _instrumentName = value; }

            }

            public string RegionName
            {
                get { return _regionName; }
            }


            public string SiteName
            {
                get { return _siteName; }
            }
            public string TestingArea
            {
                get { return _tArea; }
                set { _tArea = value; }
            }
            public int Quantity
            {
                get { return _quantity; }
                set { _quantity = value; }
            }
            public int PecentRun
            {
                get { return _percentRun; }
                set { _percentRun = value; }
            }


            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _hasError; }
                set { _hasError = value; }
            }


            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }

        }
        public class ImportedTestingArea
        {
            private string _areaName;
            private TestingArea _testingArea;

            public ImportedTestingArea(string areaName)
            {
                this._areaName = areaName;
            }

            public string AreaName
            {
                get { return _areaName; }
            }
            public TestingArea TestingArea
            {
                get { return _testingArea; }
                set { _testingArea = value; }
            }
        }
        public class ImportInsData
        {
            private string _insname;
            private string _areaname;
            private int _rate;
            private int _rowno;
            private bool _haserror = false;
            private bool _isexist = false;
            private TestingArea _testingArea;
            private int _dailyCtrTest;
            private int _weeklyCtrTest;
            private int _monthlyCtrTest;
            private int _quarterlyCtrTest;
            private int _perTestCtr;

            public ImportInsData(string iname, string areaname, int mput, int dailyCtrTest, int weeklyCtrTest, int monthlyCtrTest, int quareterlyCtrTest, int perTestCtr, int rowno)
            {
                _insname = iname;
                _areaname = areaname;
                _rate = mput;
                _rowno = rowno;
                _dailyCtrTest = dailyCtrTest;
                _weeklyCtrTest = weeklyCtrTest;
                _monthlyCtrTest = monthlyCtrTest;
                _quarterlyCtrTest = quareterlyCtrTest;
                _perTestCtr = perTestCtr;
            }

            public string InsName
            {
                get { return _insname; }
            }

            public string AreaName
            {
                get { return _areaname; }
            }

            public TestingArea TestingArea
            {
                get { return _testingArea; }
                set { _testingArea = value; }
            }

            public int Rate
            {
                get { return _rate; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }

            public int DailyCtrTest
            {
                get { return _dailyCtrTest; }
            }
            public int WeeklyCtrTest
            {
                get { return _weeklyCtrTest; }
            }
            public int MonthlyCtrTest
            {
                get { return _monthlyCtrTest; }
            }

            public int QuarterlyCtrTest
            {
                get { return _quarterlyCtrTest; }
            }
            public int PerTestCtr
            {
                get { return _perTestCtr; }
            }
        }
        public class ImportProUsageData
        {

            private string _testname;
            private string _insName;
            private string _proname;
            private decimal _rate;
            private int _rowno;
            private Test _test;
            private Instrument _instrument;
            private MasterProduct _product;

            private bool _haserror = false;
            private bool _isexist = false;
            bool _isForControl = false;
            bool _isForTest = false;

            public ImportProUsageData(string tname, string iname, string pname, decimal rate, bool isForControl, bool isForTest, int rowno)
            {

                _testname = tname;
                _insName = iname;
                _proname = pname;
                _rate = rate;
                _isForControl = isForControl;
                _isForTest = isForTest;
                _rowno = rowno;
            }


            public string TestName
            {
                get { return _testname; }
            }
            public string InstrumentName
            {
                get { return _insName; }
            }
            public string ProName
            {
                get { return _proname; }
            }
            public decimal Rate
            {
                get { return _rate; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public Test Test
            {
                get { return _test; }
                set { _test = value; }
            }
            public Instrument Instrument
            {
                get { return _instrument; }
                set { _instrument = value; }
            }
            public MasterProduct Product
            {
                get { return _product; }
                set { _product = value; }
            }

            public bool IsForTest
            {
                get { return _isForTest; }
            }

            public bool IsForControl
            {
                get { return _isForControl; }
            }


            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
        }

        public class RegionReportedData
        {
            private string _regionName;
            private ForlabRegion _region;
            private string _shortName;
            private int _rowno;
            private bool _hasError;
            private bool _isexist;

            private RegionReportedData()
            {
            }

            public RegionReportedData(int rowno, string rname, string sname)
            {
                this._rowno = rowno;
                this._regionName = rname;
                this._shortName = sname;
                this._hasError = false;
                this._isexist = false;
            }

            public string RegionName
            {
                get { return _regionName; }
            }

            public string ShortName
            {
                get { return _shortName; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
            public bool HasError
            {
                get { return _hasError; }
                set { _hasError = value; }
            }

        }

        public class ImportConsumbleData
        {

            private string _testname;
            private string _insName;
            private string _proname;
            private string _period;
            private decimal _rate;
            private int _rowno;
            private MasterConsumable _cons;
            private Instrument _instrument;
            private MasterProduct _product;

            private bool _haserror = false;
            private bool _isexist = false;
            bool _isForTest = false;
            bool _isForPeriod = false;
            bool _isForInstrument = false;
            int _noOfTest;

            public ImportConsumbleData(string tname, string iname, string pname, string period, int noOfTest, decimal rate, bool isForTest, bool isForPeriod, bool isForInstrument, int rowno)
            {

                _testname = tname;
                _insName = iname;
                _proname = pname;
                _period = period;
                _noOfTest = noOfTest;
                _rate = rate;
                _isForTest = isForTest;
                _isForPeriod = isForPeriod;
                _isForInstrument = isForInstrument;
                _rowno = rowno;
            }


            public string TestName
            {
                get { return _testname; }
            }
            public string InstrumentName
            {
                get { return _insName; }
            }
            public string ProName
            {
                get { return _proname; }
            }
            public string Period
            {
                get { return _period; }
            }

            public int NoOfTest
            {
                get { return _noOfTest; }
            }

            public decimal Rate
            {
                get { return _rate; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public MasterConsumable Cons
            {
                get { return _cons; }
                set { _cons = value; }
            }
            public Instrument Instrument
            {
                get { return _instrument; }
                set { _instrument = value; }
            }
            public MasterProduct Product
            {
                get { return _product; }
                set { _product = value; }
            }

            public bool IsForTest
            {
                get { return _isForTest; }
            }

            public bool IsForPeriod
            {
                get { return _isForPeriod; }
            }

            public bool IsForInstrument
            {
                get { return _isForInstrument; }
            }


            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
        }

        public class SiteImportData
        {
            private string _categoryName;
            private string _regionName;
            private ForlabRegion _region;
            private SiteCategory _siteCategory;
            private string _siteName;
            private string _siteLevel;//b
            private int _workingDays;

            private int _Cd4Td;
            private int _ChemTd;
            private int _hemaTd;
            private int _ViralTd;
            private int _OtherTd;
            private decimal _latitude;
            private decimal _longitude;
            private int _rowno;
            private bool _hasError = false;
            private bool _isexist = false;
            private DateTime? _openingDate;
            private string _errorDescription;

            public SiteImportData(int rowno, string rname, string cname, string sname, string slevel, int workingDays, int Cd4Td, int ChemTd, int hemaTd, int ViralTd, int OtherTd, decimal latitude, decimal longitude, DateTime? opendate)
            {
                this._rowno = rowno;
                this._regionName = rname;
                this._siteName = sname;
                this._categoryName = cname;
                this._siteLevel = slevel;//b
                this._workingDays = workingDays;//b
                this._openingDate = opendate;

                this._Cd4Td = Cd4Td;
                this._ChemTd = ChemTd;
                this._hemaTd = hemaTd;
                this._ViralTd = ViralTd;
                this._OtherTd = OtherTd;
                this._latitude = latitude;
                this._longitude = longitude;


            }

            public string CategoryName
            {
                get { return _categoryName; }
            }

            public string RegionName
            {
                get { return _regionName; }
            }

            public ForlabRegion Region
            {
                get { return _region; }
                set { _region = value; }
            }

            public SiteCategory Category
            {
                get { return _siteCategory; }
                set { _siteCategory = value; }
            }
            public string SiteName
            {
                get { return _siteName; }
            }
            public string SiteLevel//b
            {
                get { return _siteLevel; }
                set { _siteLevel = value; }
            }
            public int WorkingDays//b
            {
                get { return _workingDays; }
                set { _workingDays = value; }
            }
            public int Cd4Td
            {
                get { return _Cd4Td; }
                set { _Cd4Td = value; }
            }
            public int ChemTd
            {
                get { return _ChemTd; }
                set { _ChemTd = value; }
            }
            public int HemaTd
            {
                get { return _hemaTd; }
                set { _hemaTd = value; }
            }
            public int ViralTd
            {
                get { return _ViralTd; }
                set { _ViralTd = value; }
            }
            public int OtherTd
            {
                get { return _OtherTd; }
                set { _OtherTd = value; }
            }

            public decimal Latitude
            {
                get { return _latitude; }
                set { _latitude = value; }
            }
            public decimal Longtiude
            {
                get { return _longitude; }
                set { _longitude = value; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _hasError; }
                set { _hasError = value; }
            }

            public DateTime? OpeningDate
            {
                get { return _openingDate; }
            }
            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }

            public string ErrorDescription //14 may 14
            {
                get { return _errorDescription; }
                set { _errorDescription = value; }
            }

        }



        public class RefSiteImportData
        {

            private string _regionName;
            private ForlabRegion _region;
            private string _siteName;
            private ForlabSite _site;

            private string _CD4RefSite;
            private string _ChemistryRefSite;
            private string _HematologyRefSite;
            private string _OtheRefSite;
            private string _ViralLoadRefSite;

            private int _CD4RefSiteId;
            private int _ChemistryRefSiteId;
            private int _HematologyRefSiteId;
            private int _OtheRefSiteId;
            private int _ViralLoadRefSiteId;




            private int _rowno;
            private bool _hasError = false;
            private bool _isexist = false;
            private string _errorDescription;

            public RefSiteImportData(int rowno, string rname, string sname, string CD4RefSite, string ChemistryRefSite, string HematologyRefSite, string ViralLoadRefSite, string OtheRefSite)
            {
                this._rowno = rowno;
                this._regionName = rname;
                this._siteName = sname;

                this._CD4RefSite = CD4RefSite;
                this._ChemistryRefSite = ChemistryRefSite;
                this._HematologyRefSite = HematologyRefSite;
                this._OtheRefSite = OtheRefSite;
                this._ViralLoadRefSite = ViralLoadRefSite;

            }


            public string RegionName
            {
                get { return _regionName; }
            }

            public ForlabRegion Region
            {
                get { return _region; }
                set { _region = value; }
            }

            public string SiteName
            {
                get { return _siteName; }
            }
            public string CD4RefSite//b
            {
                get { return _CD4RefSite; }
                set { _CD4RefSite = value; }
            }

            public string ChemistryRefSite//b
            {
                get { return _ChemistryRefSite; }
                set { _ChemistryRefSite = value; }
            }
            public string HematologyRefSite//b
            {
                get { return _HematologyRefSite; }
                set { _HematologyRefSite = value; }
            }
            public string ViralLoadRefSite//b
            {
                get { return _ViralLoadRefSite; }
                set { _ViralLoadRefSite = value; }
            }
            public string OtheRefSite//b
            {
                get { return _OtheRefSite; }
                set { _OtheRefSite = value; }
            }

            public int CD4RefSiteId//b
            {
                get { return _CD4RefSiteId; }
                set { _CD4RefSiteId = value; }
            }

            public int ChemistryRefSiteId//b
            {
                get { return _ChemistryRefSiteId; }
                set { _ChemistryRefSiteId = value; }
            }
            public int HematologyRefSiteId//b
            {
                get { return _HematologyRefSiteId; }
                set { _HematologyRefSiteId = value; }
            }
            public int ViralLoadRefSiteId//b
            {
                get { return _ViralLoadRefSiteId; }
                set { _ViralLoadRefSiteId = value; }
            }
            public int OtheRefSiteId//b
            {
                get { return _OtheRefSiteId; }
                set { _OtheRefSiteId = value; }
            }

            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _hasError; }
                set { _hasError = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }

            public string ErrorDescription //14 may 14
            {
                get { return _errorDescription; }
                set { _errorDescription = value; }
            }

        }


        public class ImportQVariableData
        {
            private int _rowno;
            private string _productName;
            private double _usageRate;
            private string _quantifyMenu;
            private string _appliedTo;
            private MasterProduct _product;
            private bool _haserror = false;
            private bool _isexist = false;
            private string _errorDescription;

            public ImportQVariableData(string productname, double usagerate, string quantifymenu, string appliedto, int rowno)
            {
                _rowno = rowno;
                _productName = productname;
                _usageRate = usagerate;
                _quantifyMenu = quantifymenu;
                _appliedTo = appliedto;
            }

            public string ProductName
            {
                get { return _productName; }
            }

            public MasterProduct Product
            {
                get { return _product; }
                set { _product = value; }
            }

            public double UsageRate
            {
                get { return _usageRate; }
            }
            public int RowNo
            {
                get { return _rowno; }
            }

            public bool HasError
            {
                get { return _haserror; }
                set { _haserror = value; }
            }

            public bool IsExist
            {
                get { return _isexist; }
                set { _isexist = value; }
            }
            public string ErrorDescription
            {
                get { return _errorDescription; }
                set { _errorDescription = value; }
            }
            public string QuantifyMenu
            {
                get { return _quantifyMenu; }
            }
            public string AppliedTo
            {
                get { return _appliedTo; }
            }
        }
    }
}
