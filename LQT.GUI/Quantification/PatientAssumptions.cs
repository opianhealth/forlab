﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using LQT.Core;
using System.Text.RegularExpressions;
namespace LQT.GUI.Quantification
{
    public partial class PatientAssumptions : Form
    {
        private int _selectedForcastId = 0;
        public static Int64 _totalTarget = 0;
        public static string Mode = "";
        public static string _status = "";
        private Form _mdiparent;
        DataTable Dtpatient1 = new DataTable();
        DataTable dt1 = new DataTable();
        private string _startdate = "";
        private string _enddate = "";
        private string _period = "";
       // public static Int64 _totalTarget = 0;
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
        DataTable dtGrid = null;
        private string pattern = "^[0-9]{0,2}$";
       // string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        public PatientAssumptions(Form mdiparent, int ID, string mode, DataTable Dtpatient, string startdate, string enddate, string period, Int64 totaltarget, DataTable Dt)
        {
            InitializeComponent();
            _mdiparent = mdiparent;
            _totalTarget = totaltarget;
            _selectedForcastId = ID;
            _startdate = startdate;
            _enddate = enddate;
           _period = period;
           Dtpatient1 = Dtpatient;
           dt1 = Dt;
           this.Top = 120;
           //this.Width = _Width;
           //this.Height = _Height;
           this.Left = 250;
            //_selectedForcastId = 55;
            Mode = mode;
           string _sql = string.Format(@"Select Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]='{0}'", _selectedForcastId);
            //using (connection = new SqlConnection(con))
            //{
                using (cmdForecast = new SqlCommand(_sql, connection))
                {
                    if (connection.State == ConnectionState.Closed)
                  connection.Open();
                    _status = Convert.ToString(cmdForecast.ExecuteScalar());
                    if (connection.State != ConnectionState.Closed)
                   connection.Close();
                }
            //}
           // _status = "S";
            this.fillGrid(_selectedForcastId);
            //this.fillSiteCategory(_selectedForcastId);
            btnPrevious.TabStop = false;
            btnPrevious.FlatStyle = FlatStyle.Flat;
            btnPrevious.FlatAppearance.BorderSize = 0;
            btnPrevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            gvPAssumptions.AutoGenerateColumns = false;
            this.gvPAssumptions.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(gvPGroup_EditingControlShowing);
            this.gvPAssumptions.CellValidating += new DataGridViewCellValidatingEventHandler(gvPGroup_CellValidating);
            this.gvPAssumptions.DataError += this.gvPGroup_DataError;
        }
        public void gvPGroup_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string checkValue = "",searchValue="";
            bool stuts = false;
            if (!validation() == false)
            {
                searchValue = txtHIVP.Text;
                try
                {
                    //if (gvPGroup.Rows.Count > 0)
                    //{
                    //    foreach (DataGridViewRow row in gvPGroup.Rows)
                    //    {
                    //        if (row.Cells[2].Value.ToString().ToLower().Equals(searchValue.ToLower()))
                    //        {
                    //            //MessageBox.Show("Site Category Name is already exist");
                    //            MessageBox.Show("Patient Group Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                    //            txtPGN.Focus();
                    //            stuts = true;
                    //            break;
                    //        }

                    //    }
                    //}
                    //if (stuts == false)
                    //{
                    
                        dt = gvPAssumptions.DataSource as DataTable;
                        //dt.Rows.Clear();
                        gvPAssumptions.DataSource = dt;
                        DataTable dtPA = this.fillSiteCategory(_selectedForcastId);
                        //foreach (DataRow dr in dtPA.Rows)
                        //{
                        //    if (gvPAssumptions.Rows.Count > 0)
                        //    {
                        //        foreach (DataGridViewRow row in gvPAssumptions.Rows)
                        //        {
                        //            if (row.Cells[4].Value.ToString().ToLower().Equals(dr["SiteCategoryName"].ToString().ToLower()))
                        //            {
                        //                MessageBox.Show("Site or Category Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                        //                txtHIVP.Focus();
                        //                stuts = true;
                        //                break;
                        //            }
                                   
                        //        }
                        //        if (stuts == false)
                        //        {
                        //            dt.Rows.Add(0, _selectedForcastId, Convert.ToInt32(dr["CategoryId"]), dr["SiteCategoryName"], txtHIVP.Text, txtHIVPD.Text, txtCD4.Text);
                        //            //gvPAssumptions.DataSource = dt;
                        //        }
                        //    }
                        //    else dt.Rows.Add(0, _selectedForcastId, Convert.ToInt32(dr["CategoryId"]), dr["SiteCategoryName"], txtHIVP.Text, txtHIVPD.Text, txtCD4.Text);
                        //}
                        if (gvPAssumptions.Rows.Count > 0)
                        {
                            foreach (DataGridViewRow row in gvPAssumptions.Rows)
                            {
                                row.Cells["ForecastinfoID"].Value =_selectedForcastId;
                                row.Cells["HIVPositivePer"].Value = txtHIVP.Text;
                                row.Cells["HIVPerWithoutFollow"].Value = txtHIVPD.Text;
                                row.Cells["ReceiveCD4Per"].Value = txtCD4.Text;
                                
                            }
                        }
                        //gvPAssumptions.DataSource = dt;
                        txtHIVP.Text = "";
                        txtHIVPD.Text = "";
                        txtCD4.Text = "";
                        txtHIVP.Focus();
                   
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
        }
        public DataTable fillSiteCategory(int _forecastId)
        {
            string _sql = "";
            DataTable dtCategory = new System.Data.DataTable();
            try
            {
                if (_status == "C") _sql = string.Format(@"Select [SiteCategoryName] as SiteCategoryName,SiteCategoryId as CategoryId from ForecastCategoryInfo where [ForecastinfoID]={0}", _selectedForcastId);
                else if (_status == "S") _sql = string.Format(@"Select s.[SiteName] as SiteCategoryName,a.SiteId as CategoryId from ForecastSiteInfo a inner join Site s on a.SiteID=s.SiteID Where [ForecastinfoID]={0}", _selectedForcastId);    
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                     connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dtCategory);
                        //gvPGroup.DataSource = dt;
                    }

                 connection.Close();
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return dtCategory;
        }
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
            DataTable dt1 = new System.Data.DataTable();
            try
            {
                if (_status=="S")
                    _sql = string.Format(@"SELECT p.ID,p.ForecastinfoID,p.SiteID as CategoryID,si.SiteName as SiteCategoryName,HIVPositivePer,HIVPerWithoutFollow,ReceiveCD4Per FROM PatientAssumption p
                                            inner  join [ForecastSiteInfo] sc on p.SiteID=sc.SiteId
                                            inner join [Site] si on sc.SiteId=si.SiteID 
                                            where p.ForecastinfoID={0}
                                            union 
                                            select 0 as ID,ForecastinfoID,sc.SiteID,si.SiteName as SiteCategoryName,0,0,0 from [ForecastSiteInfo] sc
                                            inner join [Site] si on sc.SiteId=si.SiteID 
                                            where [ForecastinfoID]={1} and sc.SiteId not in (Select SiteID from PatientAssumption where [ForecastinfoID]={2})", ForecastinfoID, ForecastinfoID, ForecastinfoID);
                else if (_status == "C") {
                 _sql = string.Format(@"SELECT p.ID,p.ForecastinfoID,p.CategoryID as CategoryID,sc.SiteCategoryName as SiteCategoryName,HIVPositivePer,HIVPerWithoutFollow,ReceiveCD4Per FROM PatientAssumption p
                                            inner  join ForecastCategoryInfo sc on p.CategoryID=sc.SiteCategoryId where p.ForecastinfoID={0} 
                                            union 
                                            select 0 as ID,ForecastinfoID,SiteCategoryID,SiteCategoryName,0,0,0 from ForecastCategoryInfo where [ForecastinfoID]={1} and SiteCategoryID not in (Select CategoryId from PatientAssumption where [ForecastinfoID]={2})", ForecastinfoID, ForecastinfoID,ForecastinfoID); 
                
                
                }
                //else _sql=string.Format(@"SELECT [ID] ,[ForecastinfoID],[PatientGroupName] ,[PatientPercentage],[PatientRatio] FROM [PatientGroup] WHERE 1 = 0");
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Closed)
                      connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dt1);
                        gvPAssumptions.AutoGenerateColumns = false;
                        gvPAssumptions.DataSource = dt1;
                     }
                    if (connection.State != ConnectionState.Closed)
 connection.Close();
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }
        private bool validation()
        {
            try
            {
                if (txtHIVP.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Group Name.");
                    MessageBox.Show("Please Enter Patient population HIV positive.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtHIVP.Focus();
                    return false;
                }
                if (txtHIVP.Text == "0")
                {
                    MessageBox.Show("Patient population HIV positive must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCD4.Focus();
                    return false;
                }
                if (txtHIVPD.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Ratio.");
                    MessageBox.Show("Please Enter HIV positive diognosis to depart without follow-up.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtHIVPD.Focus();
                    return false;
                }
                if (txtHIVPD.Text == "0")
                {
                    MessageBox.Show("Positive diognosis to depart without follow-up must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCD4.Focus();
                    return false;
                }
                if (txtCD4.Text == "")
                {
                    MessageBox.Show("Please Enter HIV positive diognosis that follow-up which receive CD4.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCD4.Focus();
                    return false;
                }
                if (txtCD4.Text =="0")
                {
                    MessageBox.Show("HIV positive diognosis that follow-up which receive CD4 must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCD4.Focus();
                    return false;
                }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return true;
        }

        private void txtPGN_KeyPress(object sender, KeyPressEventArgs e)
        {
            //var regex = new Regex(@"[^a-zA-Z0-9\s]");
            //if (regex.IsMatch(e.KeyChar.ToString()))
            //{
            //    e.Handled = true;
            //}
            //onlynumwithsinglepoint(sender, e);
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void txtRation_KeyPress(object sender, KeyPressEventArgs e)
        {
           // e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            //onlynumwithsinglepoint(sender, e);       
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }
        public void onlynumwithsinglepoint(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == '.'))
            { e.Handled = true; }
            TextBox txtDecimal = sender as TextBox;
            if (e.KeyChar == '.' && txtDecimal.Text.Contains("."))
            {
                e.Handled = true;
            }
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            string _sql = "Insert into PatientAssumption(ForecastinfoID,CategoryID,SiteID,HIVPositivePer,HIVPerWithoutFollow,ReceiveCD4Per) Values(@ForecastinfoID,@CategoryID,@SiteID,@HIVPositivePer,@HIVPerWithoutFollow,@ReceiveCD4Per);SELECT SCOPE_IDENTITY()";
            string _sqlUpdate = "UPDATE [dbo].[PatientAssumption]  SET [ForecastinfoID] =@ForecastinfoID,[CategoryID] =@CategoryID,[SiteID] =@SiteID ,[HIVPositivePer] =@HIVPositivePer,[HIVPerWithoutFollow] =@HIVPerWithoutFollow,[ReceiveCD4Per] =@ReceiveCD4Per WHERE ID=@ID";

            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("ForecastinfoID", typeof(int));
            table.Columns.Add("SiteCategoryName", typeof(string));
            table.Columns.Add("CurrentPatient", typeof(int));
            table.Columns.Add("TargetPatient", typeof(int));
            //table.Columns.Add("StartDate", typeof(string));
            //table.Columns.Add("EndDate", typeof(string));
            //table.Columns.Add("Period", typeof(string));
            try
            {
                //using (connection = new SqlConnection(con))
                //{
                 connection.Open();
                    if (!checkGridRecord() == false)
                    {
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            if (gvPAssumptions.Rows.Count > 0)
                            {
                                using (cmdForecast = new SqlCommand("", connection, trans))
                                {
                                    for (int i = 0; i < gvPAssumptions.Rows.Count; i++)
                                    {
                                        if (Convert.ToInt32(gvPAssumptions.Rows[i].Cells["ID"].Value) == 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                            if (_status == "C")
                                            {
                                                cmdForecast.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["CategoryID"].Value;
                                                cmdForecast.Parameters.Add("@SiteID", SqlDbType.NVarChar).Value =1;
                                            }
                                            else {
                                                cmdForecast.Parameters.Add("@SiteID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["CategoryID"].Value;
                                                cmdForecast.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value =1;
                                            } 
                                           // cmdForecast.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["CategoryID"].Value;
                                            
                                            cmdForecast.Parameters.Add("@HIVPositivePer", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["HIVPositivePer"].Value;
                                            cmdForecast.Parameters.Add("@HIVPerWithoutFollow", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["HIVPerWithoutFollow"].Value;
                                            cmdForecast.Parameters.Add("@ReceiveCD4Per", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ReceiveCD4Per"].Value;
                                            cmdForecast.CommandText = _sql;
                                            int ForecastCategoryInfo = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                            //table.Rows.Add(ForecastCategoryInfo, _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));
                                        }
                                        if (Convert.ToInt32(gvPAssumptions.Rows[i].Cells["ID"].Value) > 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                            if (_status == "C")
                                            {
                                                cmdForecast.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["CategoryID"].Value;
                                                cmdForecast.Parameters.Add("@SiteID", SqlDbType.NVarChar).Value = 1;
                                            }
                                            else
                                            {
                                                cmdForecast.Parameters.Add("@SiteID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["CategoryID"].Value;
                                                cmdForecast.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = 1;
                                            } 
                                            cmdForecast.Parameters.Add("@HIVPositivePer", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["HIVPositivePer"].Value;
                                            cmdForecast.Parameters.Add("@HIVPerWithoutFollow", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["HIVPerWithoutFollow"].Value;
                                            cmdForecast.Parameters.Add("@ReceiveCD4Per", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ReceiveCD4Per"].Value; 
                                            cmdForecast.Parameters.Add("@ID", SqlDbType.NVarChar).Value = gvPAssumptions.Rows[i].Cells["ID"].Value;
                                            cmdForecast.CommandText = _sqlUpdate;
                                            cmdForecast.ExecuteNonQuery();
                                            //table.Rows.Add(Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value), _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));
                                        }

                                        cmdForecast.Parameters.Clear();
                                    }
                                }
                            }
                            trans.Commit();
                            ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save successfully..", true);
                            this.Hide();
                            frmProductRAssumptions FPA = new frmProductRAssumptions(_mdiparent,_selectedForcastId, "E", Dtpatient1, _startdate, _enddate, _period, _totalTarget, dt1);
                            FPA.ShowDialog();

                             this.Close();
                        }
                    }
                connection.Close();

                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }
        private bool checkGridRecord()
        {
            foreach (DataGridViewRow row in gvPAssumptions.Rows)
            {
                if (Convert.ToDouble(row.Cells["HIVPositivePer"].Value) == 0.00 || Convert.ToDouble(row.Cells["HIVPerWithoutFollow"].Value) == 0.00 || Convert.ToDouble(row.Cells["ReceiveCD4Per"].Value) == 0.00)
                {
                    MessageBox.Show("The value of Ratio And Calculated value must be a greater then Zero");
                    return false;
                }
            }
            return true;
        }

        private void gvPGroup_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int column = gvPAssumptions.CurrentCellAddress.X;
            string senderText = (sender as TextBox).Text;
            string senderName = (sender as TextBox).Name;
            string[] splitByDecimal = senderText.Split('.');
            int cursorPosition = (sender as TextBox).SelectionStart;

            if (column != 2)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.' && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }

            }
            else
            {
                var regex = new Regex(@"[^a-zA-Z0-9\s]");
                if (regex.IsMatch(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
            if (!char.IsControl(e.KeyChar) && senderText.IndexOf('.') < cursorPosition && splitByDecimal.Length > 1 && splitByDecimal[1].Length == 2)
            {
                e.Handled = true;
            }
            //if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            //{
            //    if (Regex.IsMatch(
            //     (sender as TextBox).Text,
            //     "^\\d*\\.\\d{2}$")) e.Handled = true;
            //}
            //else e.Handled = e.KeyChar != (char)Keys.Back;
        }
        public void gvPGroup_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            int newInteger = 0;
            string PatientPercentage = "0";
            string CurrentPatient = "0";
            string newValue = e.FormattedValue.ToString();
            string oldValue = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
            if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["HIVPositivePer"].Index || e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["HIVPositivePer"].Index || e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["HIVPositivePer"].Index)
            {
                if (string.IsNullOrEmpty(newValue))
                {
                    e.Cancel = true;
                    MessageBox.Show("Value is not empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

            //if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["PatientPercentage"].Index)
            //{
            //    PatientPercentage = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientRatio"].FormattedValue.ToString();
            //    ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "";
            //    if (Convert.ToInt32(e.FormattedValue) > 0)
            //    {
            //        int _calValue = (_totalTarget *Convert.ToInt32(e.FormattedValue)) / 100;
            //        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientRatio"].Value = _calValue;
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "The value of Ratio must be grater then Zero";
            //        MessageBox.Show("The value of Ratio must be a grater then Zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}
            
            //if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["PatientGroupName"].Index)
            //{
            //    int cIndex = e.RowIndex;
            //    string value = e.FormattedValue.ToString();
            //    value = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientGroupName"].FormattedValue.ToString();
            //    foreach (DataGridViewRow row in gvPAssumptions.Rows)
            //    {
            //        int oIndex = row.Cells["PatientGroupName"].RowIndex;
            //        if (cIndex != oIndex)
            //        {
            //            if (row.Cells["PatientGroupName"].Value.ToString().ToLower().Equals(e.FormattedValue.ToString().ToLower()))
            //            {
            //                //MessageBox.Show("Site Category Name is already exist");
            //                MessageBox.Show("Patient Group Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
            //                row.Cells[oIndex].Value = value;
            //                row.Cells[oIndex].Selected = true;
            //                break;
            //                //e.Cancel = true;
            //            }
            //        }
            //    }
            //}
        }

        private void txtCD4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //onlynumwithsinglepoint(sender, e);
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 (sender as TextBox).Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmTestTestingprotocol frm = new FrmTestTestingprotocol(_mdiparent,_selectedForcastId, "E", Dtpatient1, _startdate, _enddate, _period, _totalTarget, dt1);
            // Form1 frm = new Form1();
            frm.ShowDialog();
            this.Close();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                try
                {
                    //using (connection = new SqlConnection(con))
                    //{
                     connection.Open();
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            using (cmdForecast = new SqlCommand("", connection, trans))
                            {
                                if (Convert.ToInt32(gvPAssumptions.CurrentRow.Cells["ID"].Value) != 0)
                                {
                                    string sSql = "DELETE FROM PatientAssumption WHERE ID=" + gvPAssumptions.CurrentRow.Cells["ID"].Value + "";
                                    cmdForecast.CommandText = sSql;
                                    cmdForecast.ExecuteNonQuery();
                                    MessageBox.Show("Record Delete From The Database.", "Record Deleted.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    //FillGrid();
                                }
                            }
                            trans.Commit();
                            
                        }
                     connection.Close();
                        this.fillGrid(_selectedForcastId);
                    //}
                }
                catch (Exception ex)
                {
                    connection.Close();
                }
            }
        }

        //private void deleteRecordToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    if (!this.gvPAssumptions.Rows[rowIndex].IsNewRow)
        //    {
        //        this.gvPAssumptions.Rows.RemoveAt(this.rowIndex);
        //    }
        //}

        //private void gvPAssumptions_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Right)
        //    {
        //        this.gvPAssumptions.Rows[e.RowIndex].Selected = true;
        //        this.rowIndex = e.RowIndex;
        //        this.gvPAssumptions.CurrentCell = this.gvPAssumptions.Rows[e.RowIndex].Cells["ID"];
        //        this.contextMenuStrip1.Show(this.gvPAssumptions, e.Location);
        //        contextMenuStrip1.Show(Cursor.Position);
        //    }
        //}

        //public int rowIndex { get; set; }
    }
}
