﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.GUI.UserCtr;
using LQT.Core.Util;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;
using System.Data;
using LQT.Core;
using System.Text.RegularExpressions;
using LQT.Core.Domain;
namespace LQT.GUI.Quantification
{
    public partial class AggregateForecast : Form
    {
        private int _selectedForcastId =0;

        private Form _mdiparent;
        public static string Period = "";
        public static string StartDate = "";
        public static string EndDate = "";
        public static string Mode = "";
         private int _selectedRegionId = 0;
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
        DataTable dtGrid = null;
        private string pattern = "^[0-9]{0,2}$";
       // string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        public AggregateForecast(Form mdiparent, int id, string mode, string _startDate, string _endDate, string _period)
        {
            InitializeComponent();
            _selectedForcastId = id;
            Mode = mode;
            _mdiparent=mdiparent;
            Period = _period;
            StartDate = _startDate;
            EndDate = _endDate;
            this.Load += new EventHandler(Form1_Load);
            //PopRegion();
           // fillSiteList();
        }
        private void Form1_Load(System.Object sender, System.EventArgs e)
        {
            // Attach DataGridView events to the corresponding event handlers.
            this.fillCategory();
            this.fillGrid(_selectedForcastId);
            gvAFE.AutoGenerateColumns = false;
            gvAFE.AllowUserToAddRows = false;
            btnPre.TabStop = false;
            btnPre.FlatStyle = FlatStyle.Flat;
            btnPre.FlatAppearance.BorderSize = 0;
            btnPre.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;
            //this.gvAFE.CellValidating += new DataGridViewCellValidatingEventHandler(dataGridView1_CellValidating);
           // this.gvAFE.CellEndEdit += new DataGridViewCellEventHandler(dataGridView1_CellEndEdit);
            this.gvAFE.CellValidating+=new DataGridViewCellValidatingEventHandler(gvAFE_CellValidating); 
            this.gvAFE.EditingControlShowing+=new DataGridViewEditingControlShowingEventHandler(gvAFE_EditingControlShowing);
            gvAFE.SelectionChanged += new EventHandler(gvAFE_SelectionChanged);
            fillDataGrid();
            this.setErrorText();
        }
        private void PopRegion(string _categoryId)
        {
            lstRegion.BeginUpdate();
            lstRegion.Items.Clear();

            //foreach (var items in DataRepository.GetAllRegion())
            //{
            //    ListViewItem li = new ListViewItem(items.RegionName) { Tag = items.Id };
            //    if (items.Id == _selectedRegionId)
            //    {
            //        li.Selected = true;
            //    }
            //    lstRegion.Items.Add(li);
            //}

            using (cmdForecast = new SqlCommand("", connection))
            {
                // connection.Open();
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                string _sql = string.Format("select * from [Region] where RegionID in (Select Distinct RegionID from [Site] Where CategoryID ='{0}')", _categoryId);
                cmdForecast.CommandText = _sql;
                DataTable dtSite = new System.Data.DataTable();
                daForecast = new SqlDataAdapter(cmdForecast);
                daForecast.Fill(dtSite);
                List<DataRow> list = new List<DataRow>(dtSite.Select());
                 var item = dtSite.TableName.ToList();
                 foreach (DataRow dr in dtSite.Rows )
                 {
                     ListViewItem li = new ListViewItem(dr["RegionName"].ToString()) { Tag = dr["RegionID"] };
                     //if (dr["RegionID"] == _selectedRegionId)
                     //{
                     //    li.Selected = true;
                     //}
                     lstRegion.Items.Add(li);
                 }
            }
            
            lstRegion.EndUpdate();


        }

        private void lstRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string _categoryId = "";
            //if (lstRegion.SelectedItems.Count > 0)
            //{
            //    int id = (int)lstRegion.SelectedItems[0].Tag;
            //    foreach (ListViewItem items in lstRegion.Items)
            //    {
            //        if (items.Checked == true)
            //        {
            //            _categoryId +="'"+items.Tag+ "'"+",";
            //        }
            //    }
            //    using (cmdForecast = new SqlCommand("", connection))
            //    {
            //        // connection.Open();
            //        _categoryId = _categoryId.TrimEnd(',');
            //        if (connection.State == ConnectionState.Open)
            //        { }
            //        else
            //        { connection.Open(); }
            //        string _sql = string.Format("select SiteId,SiteName from [Site] where RegionID in ({0})", _categoryId);
            //        cmdForecast.CommandText = _sql;
            //        DataTable dtSite = new System.Data.DataTable();
            //        daForecast = new SqlDataAdapter(cmdForecast);
            //        daForecast.Fill(dtSite);
            //        //List<DataRow> list = new List<DataRow>(dtSite.Select());
            //        //var item = dtSite.TableName.ToList();
            //        foreach (DataRow dr in dtSite.Rows)
            //        {
            //            ListViewItem li = new ListViewItem(dr["SiteName"].ToString()) { Tag = dr["SiteID"] };
            //            //if (dr["RegionID"] == _selectedRegionId)
            //            //{
            //            //    li.Selected = true;
            //            //}
            //            lstSite.Items.Add(li);
            //        }
            //    }
            //    //if (id != _selectedRegionId)
            //    //{
            //    //    //_selectedRegionId = id;
            //    //    //lstSite.BeginUpdate();
            //    //    //lstSite.Items.Clear();
            //    //    //foreach (var items in DataRepository.GetAllSiteByRegionId(_selectedRegionId))
            //    //    //{
            //    //    //    ListViewItem li = new ListViewItem(items.SiteName) { Tag = items.Id };
            //    //    //    //if (items.Id == _selectedRegionId)
            //    //    //    //{
            //    //    //    //    li.Selected = true;
            //    //    //    //}
            //    //    //    lstSite.Items.Add(li);
            //    //    //}

                    

            //    //    lstSite.EndUpdate();

            //    //}
            //    lstSite.EndUpdate();
            //}
        }
        public void fillSiteList1()
        {
            //if (lstRegion.SelectedItems.Count > 0)
            //{
               // int id = (int)lstRegion.SelectedItems[0].Tag;
                int id = _selectedRegionId;
                if (_selectedRegionId>0)
                {
                    _selectedRegionId = id;
                    lstSite.BeginUpdate();
                    lstSite.Items.Clear();
                    foreach (var items in DataRepository.GetAllSiteByRegionId(_selectedRegionId))
                    {
                        ListViewItem li = new ListViewItem(items.SiteName) { Tag = items.Id };
                        //if (items.Id == _selectedRegionId)
                        //{
                        //    li.Selected = true;
                        //}
                        lstSite.Items.Add(li);
                    }

                    lstSite.EndUpdate();

                }
            //}
        }

        public void setErrorText()
        {
            if (gvAFE.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in gvAFE.Rows)
                {
                    if (Convert.ToInt32(row.Cells["CurrentPatient"].Value) > Convert.ToInt32(row.Cells["TargetPatient"].Value))
                    {
                        row.Cells["CurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                    }
                }
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string checkValue = "";
            bool stuts = false;
            int _siteCategoryID = 0;
            if (btnAdd.Text != "Edit")
            {

                if (validation() != false)
                {
                    string searchValue = cboCategory.Text;

                    gvAFE.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    try
                    {
                        //using (connection = new SqlConnection(con))
                        //{
                        //connection.Open();
                        if (connection.State == ConnectionState.Open)
                        { }
                        else
                        { connection.Open(); }
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            string _sql = string.Format(@"Select Count(*) FROM [SiteCategory] Where LOWER(CategoryName)='" + searchValue.ToLower() + "'");

                            using (cmdForecast = new SqlCommand("", connection, trans))
                            {
                                cmdForecast.CommandText = _sql;
                                int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                if (cnt == 0)
                                {
                                    _sql = "Insert Into [SiteCategory] Values('" + searchValue + "');SELECT SCOPE_IDENTITY()";
                                    cmdForecast.CommandText = _sql;
                                    _siteCategoryID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                }
                                else
                                {
                                    _siteCategoryID = Convert.ToInt32(cboCategory.SelectedValue);

                                }
                                foreach (ListViewItem items in lstSite.Items)
                                {
                                    if (items.Checked)
                                    {
                                        if (btnAdd.Text == "Add") _sql = "Insert Into [ForecastCategorySiteInfo] Values(" + _selectedForcastId + "," + _siteCategoryID + "," + items.Tag.ToString() + ");SELECT SCOPE_IDENTITY()";
                                        cmdForecast.CommandText = _sql;
                                        Convert.ToInt32(cmdForecast.ExecuteScalar());
                                    }
                                }
                            }
                            trans.Commit();
                            connection.Close();

                            //this.fillCategory();
                        }

                        if (gvAFE.Rows.Count > 0)
                        {
                            foreach (DataGridViewRow row in gvAFE.Rows)
                            {
                                if (row.Cells["SiteCategoryName"].Value.ToString().ToLower().Equals(searchValue.ToLower()))
                                {
                                    //MessageBox.Show("Site Category Name is already exist");
                                    MessageBox.Show("Site Category  Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 

                                    cboCategory.Focus();
                                    stuts = true;
                                    break;
                                }
                                if (Convert.ToInt32(row.Cells["CurrentPatient"].Value) > Convert.ToInt32(row.Cells["TargetPatient"].Value))
                                {
                                    row.Cells["CurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                                    checkValue += "false" + ",";
                                }
                            }
                        }
                        if (stuts == false)
                        {
                            //if (!checkValue.Contains("false"))
                            //{

                                dt = gvAFE.DataSource as DataTable;
                                dt.Rows.Add(0, 0, _siteCategoryID, cboCategory.Text, 0, 0);
                                gvAFE.DataSource = dt;
                                cboCategory.Text = "";
                                cboCategory.Focus();

                                string _sql = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + _siteCategoryID + "";
                                cmdForecast.CommandText = _sql;
                                DataTable dtSite = new System.Data.DataTable();
                                daForecast = new SqlDataAdapter(cmdForecast);
                                daForecast.Fill(dtSite);
                                gvSite.DataSource = dtSite;
                            //}
                            if (checkValue.Contains("false"))
                            {
                               // MessageBox.Show("The value of Current Patient must be a less then Target Patient value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                // MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                            }
                        }
                    }
                    // }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                        connection.Close();
                    }

                    //int index = this.gvAFE.Rows.Count;
                    //index++;
                    //gvAFE.Rows.Add("", "", txtCategoryName.Text, 0, 0);

                    ////DataTable dt = gvAFE.DataSource as DataTable;
                    ////dt.Rows.Add("", "", txtCategoryName.Text, 0, 0);
                    ////gvAFE.DataSource = dt;
                }

            }
            else
            {
                //this.editSiteCategory();
                //fillGrid(_selectedForcastId);
            }
           
        }
        public void fillListGrid()
        {
            DataGridViewRow row = this.gvAFE.SelectedRows[0];
            int CategoryID = Convert.ToInt32(row.Cells["SiteCategoryID"].Value);
            string _sql = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + CategoryID + "";
            cmdForecast.CommandText = _sql;
            DataTable dtSite = new System.Data.DataTable();
            daForecast = new SqlDataAdapter(cmdForecast);
            daForecast.Fill(dtSite);
            gvSite.DataSource = dtSite;
        }
        public bool checkGrid(int cValue,int tValue)
        {
            if (cValue > tValue)
            {
                //MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                return false;
            }

            return true;
        }
        private bool validation()
        {
            try
            {
                if (cboCategory.Text == "")
                {
                    //MessageBox.Show("Please Enter Site Category Name.");
                    MessageBox.Show("Please Enter Site Category Name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cboCategory.Focus(); 
                    return false;
                }
                //if (cboCategory.Text != "")
                //{
                //    using (connection = new SqlConnection(con))
                //    {
                //        //SaveOrUpdateObject1();
                //        connection.Open();
                //        string _sql1 = string.Format(@"Select Count(*) from ForecastCategoryInfo Where LOWER(SiteCategoryName)='{0}'", cboCategory.Text.ToLower().Trim());
                //        using (cmdForecast = new SqlCommand(_sql1, connection))
                //        {
                //            int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                //            if (cnt > 0)
                //            {
                //                //MessageBox.Show("Site Category Name must not be duplicate.");
                //                MessageBox.Show("Site Category Name must not be duplicate.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //                return false;
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return true;
        }
        private void fillCategory()
        {
            string _sql = "";
            try
            {
                _sql = string.Format(@"Select [CategoryID],[CategoryName] FROM [SiteCategory]");
                DataTable dt1 = new System.Data.DataTable();
              
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Open)
                        { }
                        else
                        { connection.Open(); }
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dt1);
                        dt1.Rows.Add("0", "--Select Category--");
                        DataView dv = dt1.DefaultView;
                        dv.Sort = "CategoryID asc";
                        dt1 = dv.ToTable();
                        cboCategory.DataSource = dt1;
                      
                        cboCategory.DisplayMember = "CategoryName";
                        cboCategory.ValueMember = "CategoryID";
                       
                    }
                    

                    //if (cboCategory.Items.Count > 0)
                    //{
                    //    cboCategory.Items.Insert(0, "Select Item");
                    //    cboCategory.SelectedIndex = 0;
                    //}
                    connection.Close();
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }
        private void gvAFE_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                //using (connection = new SqlConnection(con))
                //{
                    //connection.Open();
                //btnEdit.Enabled = true;
                DataTable dt1 = new System.Data.DataTable();
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }

                    lstSite.Items.Clear();
                    fillSiteList1();
                   // fillSiteList();
                   // btnAdd.Text = "Edit";
                    if (gvAFE.SelectedRows.Count != 0)
                    {
                    DataGridViewRow row = this.gvAFE.SelectedRows[0];
                    gvSite.AutoGenerateColumns = false;
                    if (gvSite.Rows.Count > 0)
                    {
                       dt1 = gvSite.DataSource as DataTable;
                       dt1.Clear();
                    }
                    int CategoryID = Convert.ToInt32(row.Cells["SiteCategoryID"].Value);
                    string _sql = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + CategoryID + "";
                    using (cmdForecast = new SqlCommand("", connection, null))
                    {
                        cmdForecast.CommandText = _sql;
                        SqlDataAdapter da = new SqlDataAdapter(cmdForecast);
                        da.Fill(dt1);
                        int bn = lstSite.Items.Count;
                        gvSite.DataSource = dt1;
                        foreach (DataRow dr in dt1.Rows)
                        {
                            //if (lstSite.Items.ContainsKey(dr["SiteID"].ToString()))
                            //{
                            //}
                            
                            foreach (ListViewItem items in lstSite.Items)
                            {
                                if (dr["SiteID"].ToString() == items.Tag.ToString())
                                {
                                    items.Checked = true;
                                }
                            }
                          
                        }
                    }
                    }
                    
               // }
            }
            catch (Exception ex)
            { 
            
            }
        }

        private void editSiteCategory()
        {
            //if (btnAdd.Text == "Edit")
            //{
                
                DataGridViewRow row = this.gvAFE.SelectedRows[0];
                int CategoryID = Convert.ToInt32(row.Cells["SiteCategoryID"].Value);
                //using (connection = new SqlConnection(con))
                //{
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                    //string _sql = "Delete From [ForecastCategorySiteInfo] Where ForecastInfoID=" + _selectedForcastId + " And CategoryID=" + CategoryID + " ";
                    using (cmdForecast = new SqlCommand("", connection, null))
                    {
                        //cmdForecast.CommandText = _sql;
                        //cmdForecast.ExecuteNonQuery();

                        foreach (ListViewItem items in lstSite.Items)
                        {
                            if (items.Checked)
                            {
                                string _sql = "Select Count(*) From [ForecastCategorySiteInfo] Where ForecastInfoID=" + _selectedForcastId + " And CategoryID=" + CategoryID + " And SiteID=" + items.Tag.ToString() + " ";
                                cmdForecast.CommandText = _sql;
                                int cnt =Convert.ToInt32(cmdForecast.ExecuteScalar());
                                if (cnt == 0)
                                {
                                    _sql = "Insert Into [ForecastCategorySiteInfo] Values(" + _selectedForcastId + "," + CategoryID + "," + items.Tag.ToString() + ")";
                                    cmdForecast.CommandText = _sql;
                                    cmdForecast.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                //}
                lstSite.Items.Clear();
                fillListGrid();
                //this.fillSiteList();
            //}
        }
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int RowIndex = e.RowIndex;
            int columnIndex = e.ColumnIndex;
            string vb = gvAFE.Rows[RowIndex].Cells[columnIndex].Value.ToString();
            string TargetPatient = gvAFE.Rows[e.RowIndex].Cells["TargetPatient"].Value.ToString();
            string CurrentPatient = gvAFE.Rows[e.RowIndex].Cells["CurrentPatient"].Value.ToString();
           
            int newInteger = 0;
            if (e.ColumnIndex == gvAFE.Columns["CurrentPatient"].Index)
            {
                gvAFE.Rows[e.RowIndex].ErrorText = "";
                if (Convert.ToInt32(TargetPatient) > 0)
                {
                    if (Convert.ToInt32(CurrentPatient) > Convert.ToInt32(TargetPatient))
                    {
                        //gvAFE.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        //MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                        
                        btnAdd.Enabled = false;
                    }
                    else btnAdd.Enabled = true;
                }
             
                //if (gvAFE.Rows[e.RowIndex].IsNewRow) { return; }
                //if (!int.TryParse(CurrentPatient.ToString(), out newInteger) || newInteger < 0)
                //{
                    
                //    gvAFE.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a Positive integer";
                //    MessageBox.Show("The value of Current Patient must be a Positive integer");
                //    gvAFE.Rows[e.RowIndex].ErrorText = string.Empty;
                //}
            }
            if (e.ColumnIndex == gvAFE.Columns["TargetPatient"].Index)
            {
                int inde = gvAFE.Columns["TargetPatient"].Index;
               
                if (Convert.ToInt32(TargetPatient) > 0)
                {
                    if (Convert.ToInt32(CurrentPatient) > Convert.ToInt32(TargetPatient))
                    {
                        //gvAFE.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        //MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                        btnAdd.Enabled = false;
                    }
                    else btnAdd.Enabled = true;
                }
            }
        }
        private void gvAFE_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                //gvAFE.EditingControl.KeyPress -= EditingControl_KeyPress;
                //gvAFE.EditingControl.KeyPress += EditingControl_KeyPress;
            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) ///&& e.KeyChar != '.'
            {
                e.Handled = true;
            }

            // only allow one decimal point
            ////if (e.KeyChar == '.'
            ////    && (sender as TextBox).Text.IndexOf('.') > -1)
            ////{
            ////    e.Handled = true;
            ////}
        }
        private void EditingControl_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar))
            {
                Control editingControl = (Control)sender;
                if (!Regex.IsMatch(editingControl.Text + e.KeyChar, pattern))
                    e.Handled = true;
            }
        }
        public void gvAFE_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            int newInteger = 0;
            string TargetPatient ="0";
            string CurrentPatient = "0";
           // string CurrentPatient = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].FormattedValue.ToString();
            string newValue = e.FormattedValue.ToString();
            string oldValue = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
            if (string.IsNullOrEmpty(newValue))
            {
                e.Cancel = true;
                MessageBox.Show("The value is not empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show("The value is not empty");
                cboCategory.Text = "";
                btnAdd.Enabled = false;
            }
            else btnAdd.Enabled = true;
            if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["CurrentPatient"].Index)
            {
                TargetPatient = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["TargetPatient"].FormattedValue.ToString();
                ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "";
                if (Convert.ToInt32(TargetPatient) > 0)
                {
                    if (Convert.ToInt32(e.FormattedValue.ToString()) > Convert.ToInt32(TargetPatient))
                    {
                        // e.Cancel = true;
                        // cboCategory.Text = "";
                        // btnAdd.Enabled = false;
                        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        //((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        //MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                        // MessageBox.Show("The value of Current Patient must be a less then Target Patient value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        btnAdd.Enabled = true;
                        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].ErrorText = "";
                    }
                }

            }
            if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["TargetPatient"].Index)
            {
               CurrentPatient = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].FormattedValue.ToString();
               ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "";
               if (Convert.ToInt32(e.FormattedValue) > 0)
                {
                    if (Convert.ToInt32(CurrentPatient) > Convert.ToInt32(e.FormattedValue.ToString()))
                    {
                        //e.Cancel = true;
                        //  cboCategory.Text = "";
                        // btnAdd.Enabled = false;
                        // ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        // MessageBox.Show("The value of Current Patient must be a less then Target Patient value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //MessageBox.Show("The value of Current Patient must be a less then Target Patient value");
                        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                    }
                    else
                    {
                        btnAdd.Enabled = true;
                        ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["CurrentPatient"].ErrorText = "";
                    }
                   
                }
            }
        }
        private void dataGridView1_CellValidating(object sender,DataGridViewCellValidatingEventArgs e)
        {
            int newInteger=0;
            //if (e.ColumnIndex == gvAFE.Columns["CurrentPatient"].Index || e.ColumnIndex == gvAFE.Columns["TargetPatient"].Index)
            //{
            //    gvAFE.Rows[e.RowIndex].ErrorText = "";
            //    if (gvAFE.Rows[e.RowIndex].IsNewRow) { return; }
            //    if (!int.TryParse(e.FormattedValue.ToString(),out newInteger) || newInteger < 0)
            //    {
            //        e.Cancel = true;
            //        gvAFE.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a Positive integer";
            //        MessageBox.Show("The value of Current Patient must be a Positive integer");
            //    }
               
            //}
        }
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
            if (dt.Rows.Count > 0) dt.Rows.Clear();
            try
            {
                if (ForecastinfoID > 0)
                    _sql = string.Format(@"SELECT ID,ForecastinfoID,ISNULL((SiteCategoryId),0) as SiteCategoryId,SiteCategoryName,CAST(CurrentPatient AS INT) AS CurrentPatient,CAST(TargetPatient AS INT) AS TargetPatient FROM [ForecastCategoryInfo] Where [ForecastinfoID]={0}", ForecastinfoID);
                else _sql = string.Format(@"SELECT ID,ForecastinfoID,ISNULL((SiteCategoryId),0) as SiteCategoryId,SiteCategoryName,CAST(CurrentPatient AS INT) AS CurrentPatient,CAST(TargetPatient AS INT) AS TargetPatient FROM [ForecastCategoryInfo] WHERE 1 = 0");
                //using (connection = new SqlConnection(con))
                //{
                using (cmdForecast = new SqlCommand("", connection))
                {
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    cmdForecast.CommandText = _sql;
                    daForecast = new SqlDataAdapter(cmdForecast);
                    daForecast.Fill(dt);
                    gvAFE.DataSource = dt;
                    if (gvAFE.Rows.Count > 0)
                    {
                        DataGridViewRow row = this.gvAFE.SelectedRows[0];
                        int CategoryID = Convert.ToInt32(row.Cells["SiteCategoryID"].Value);
                        _sql = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + CategoryID + "";
                        cmdForecast.CommandText = _sql;
                        DataTable dtSite = new System.Data.DataTable();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dtSite);
                        gvSite.DataSource = dtSite;
                        if (dtSite.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtSite.Rows)
                            {
                                foreach (ListViewItem items in lstSite.Items)
                                {
                                    if (dr["SiteID"].ToString() == items.Tag.ToString())
                                    {
                                        items.Checked = true;
                                       // btnAdd.Text = "Edit";
                                    }
                                }

                            }
                        }
                    }
                }

                connection.Close();
                //  }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }

        public void fillDataGrid()
        {
            //DataTable dtstudent = new DataTable();
            //// add column to datatable  
            //dtstudent.Columns.Add("StudentID", typeof(int));
            //dtstudent.Columns.Add("Name", typeof(string));
            //dtstudent.Columns.Add("RollNo", typeof(string));


            ////Child table
            //DataTable dtstudentMarks = new DataTable();
            //dtstudentMarks.Columns.Add("ID", typeof(int));
            //dtstudentMarks.Columns.Add("Subject", typeof(int));
            //dtstudentMarks.Columns.Add("SubjectName", typeof(string));
            //dtstudentMarks.Columns.Add("Marks", typeof(int));



            ////Adding Rows

            //dtstudent.Rows.Add(111, "Vikram", "03021013014");
            //dtstudent.Rows.Add(222, "Vicky", "0302101444");
            //dtstudent.Rows.Add(333, "Aryan", "030212222");
            //dtstudent.Rows.Add(444, "Aatharav", "KANPUR");

            //// data for devesh ID=111
            //dtstudentMarks.Rows.Add(111, "01", "Physics", 99);
            //dtstudentMarks.Rows.Add(111, "02", "Maths", 77);
            //dtstudentMarks.Rows.Add(111, "03", "C#", 100);
            //dtstudentMarks.Rows.Add(111, "01", "Physics", 99);


            ////data for ROLI ID=222
            //dtstudentMarks.Rows.Add(222, "01", "Physics", 80);
            //dtstudentMarks.Rows.Add(222, "02", "English", 95); 
            //dtstudentMarks.Rows.Add(222, "03", "Commerce", 95);
            //dtstudentMarks.Rows.Add(222, "01", "BankPO", 99);



            //DataSet dsDataset = new DataSet();
            ////Add two DataTables  in Dataset

            //dsDataset.Tables.Add(dtstudent);
            //dsDataset.Tables.Add(dtstudentMarks);


            //DataRelation Datatablerelation = new DataRelation("DetailsMarks", dsDataset.Tables[0].Columns[0], dsDataset.Tables[1].Columns[0], true);
            //dsDataset.Relations.Add(Datatablerelation);
            //dataGrid1.DataSource = dsDataset.Tables[0];
        }
        private void btnNext_Click(object sender, EventArgs e)
        {

            string _sql = "Insert into ForecastCategoryInfo(ForecastinfoID,SiteCategoryName,CurrentPatient,TargetPatient,SiteCategoryId) Values(@ForecastinfoID,@SiteCategoryName,@CurrentPatient,@TargetPatient,@SiteCategoryId);SELECT SCOPE_IDENTITY()";
            string _sqlUpdate = "UPDATE [dbo].[ForecastCategoryInfo]  SET [ForecastinfoID] =@ForecastinfoID,[SiteCategoryName] =@SiteCategoryName,[CurrentPatient] =@CurrentPatient ,[TargetPatient] =@TargetPatient,SiteCategoryId=@SiteCategoryId  WHERE ID=@ID";
            int Totaltarget = 0;
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("ForecastinfoID", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("CurrentPatient", typeof(int));
            table.Columns.Add("TargetPatient", typeof(int));
            //table.Columns.Add("StartDate", typeof(string));
            //table.Columns.Add("EndDate", typeof(string));
            //table.Columns.Add("Period", typeof(string));
            try
            {
                //using (connection = new SqlConnection(con))
                //{
                   // connection.Open();
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                    if (!checkGridRecord() == false)
                    {
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            if (gvAFE.Rows.Count > 0)
                            {
                                using (cmdForecast = new SqlCommand("", connection, trans))
                                {
                                    for (int i = 0; i < gvAFE.Rows.Count; i++)
                                    {
                                        if (Convert.ToInt32(gvAFE.Rows[i].Cells["ID"].Value) == 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                            cmdForecast.Parameters.Add("@SiteCategoryName", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["SiteCategoryName"].Value;
                                            cmdForecast.Parameters.Add("@CurrentPatient", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["CurrentPatient"].Value;
                                            cmdForecast.Parameters.Add("@TargetPatient", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["TargetPatient"].Value;
                                            cmdForecast.Parameters.Add("@SiteCategoryId", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["SiteCategoryId"].Value;
                                            cmdForecast.CommandText = _sql;
                                            int ForecastCategoryInfo = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                            Totaltarget = Totaltarget + Convert.ToInt32(gvAFE.Rows[i].Cells["TargetPatient"].Value);
                                            table.Rows.Add(Convert.ToInt32(gvAFE.Rows[i].Cells["SiteCategoryId"].Value), _selectedForcastId, gvAFE.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvAFE.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvAFE.Rows[i].Cells["TargetPatient"].Value));
                                        }
                                        if (Convert.ToInt32(gvAFE.Rows[i].Cells["ID"].Value) > 0)
                                        {
                                            cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                            cmdForecast.Parameters.Add("@SiteCategoryName", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["SiteCategoryName"].Value;
                                            cmdForecast.Parameters.Add("@CurrentPatient", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["CurrentPatient"].Value;
                                            cmdForecast.Parameters.Add("@TargetPatient", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["TargetPatient"].Value;
                                            cmdForecast.Parameters.Add("@SiteCategoryId", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["SiteCategoryId"].Value;
                                            cmdForecast.Parameters.Add("@ID", SqlDbType.NVarChar).Value = gvAFE.Rows[i].Cells["ID"].Value;
                                            cmdForecast.CommandText = _sqlUpdate;
                                            cmdForecast.ExecuteNonQuery();
                                            Totaltarget = Totaltarget + Convert.ToInt32(gvAFE.Rows[i].Cells["TargetPatient"].Value);
                                            table.Rows.Add(Convert.ToInt32(gvAFE.Rows[i].Cells["SiteCategoryId"].Value), _selectedForcastId, gvAFE.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvAFE.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvAFE.Rows[i].Cells["TargetPatient"].Value));
                                        }

                                        cmdForecast.Parameters.Clear();
                                    }
                                }
                            }
                            trans.Commit();

                            connection.Close();

                            if (gvAFE.Rows.Count > 0)
                            {
                                //MessageBox.Show("Record Save/Update successfully.. ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                                ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save/Update successfully..", true);
                                this.Hide();
                                PatientGroupRatio frm = new PatientGroupRatio(_mdiparent,_selectedForcastId, "E", StartDate, EndDate, Period, Totaltarget, table);
                                // Form1 frm = new Form1();
                                frm.ShowDialog();
                                this.Close();
                            }
                            else MessageBox.Show("Please add atleast one record.. ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        }

                        //this.Hide();

                        //Frmlineargrowth frm = new Frmlineargrowth(_selectedForcastId, StartDate, EndDate, Period, table);
                        //// Form1 frm = new Form1();
                        //frm.ShowDialog();
                        //this.Close();
                    }


              //  }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }

        private bool checkGridRecord()
        {
            foreach (DataGridViewRow row in gvAFE.Rows)
            {
                if (Convert.ToInt32(row.Cells["CurrentPatient"].Value) == 0 || Convert.ToInt32(row.Cells["TargetPatient"].Value) == 0)
                {
                    MessageBox.Show("The value of Current Patient And Target Patient must be a greater then Zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //MessageBox.Show("The value of Current Patient And Target Patient must be a greater then Zero");
                    return false;
                }
            }
            return true;
        }
        private void btnPre_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMorbidityForecastDefination frm = new frmMorbidityForecastDefination(_mdiparent, _selectedForcastId, Mode, 0, 0, 0, 0, 0);
            //frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog();
            this.Close();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (MessageBox.Show("Are you sure you want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        //using (connection = new SqlConnection(con))
                        //{
                        if (connection.State == ConnectionState.Open)
                        { }
                        else
                        { connection.Open(); }
                            using (SqlTransaction trans = connection.BeginTransaction())
                            {
                                using (cmdForecast = new SqlCommand("", connection, trans))
                                {
                                    if (Convert.ToInt32(gvAFE.CurrentRow.Cells["ID"].Value) != 0)
                                    {
                                        string sSql2 = "DELETE FROM ForecastCategorySiteInfo WHERE ForecastinfoID=" + _selectedForcastId + " And CategoryId=" + gvAFE.CurrentRow.Cells["SiteCategoryId"].Value + "";
                                        cmdForecast.CommandText = sSql2;
                                        cmdForecast.ExecuteNonQuery();

                                        string sSql = "DELETE FROM PatientAssumption WHERE ForecastinfoID=" + _selectedForcastId + " And CategoryId=" + gvAFE.CurrentRow.Cells["SiteCategoryId"].Value + "";
                                        cmdForecast.CommandText = sSql;
                                        cmdForecast.ExecuteNonQuery();
                                        string sSql1 = "DELETE FROM ForecastCategoryInfo WHERE ForecastinfoID=" + _selectedForcastId + " And SiteCategoryId=" + gvAFE.CurrentRow.Cells["SiteCategoryId"].Value + "";
                                        cmdForecast.CommandText = sSql1;
                                        cmdForecast.ExecuteNonQuery();
                                        MessageBox.Show("Record Delete From The Database.", "Record Deleted.", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        //FillGrid();
                                    }
                                    if (this.gvAFE.SelectedRows.Count > 0)
                                    {
                                        gvAFE.Rows.RemoveAt(this.gvAFE.SelectedRows[0].Index);
                                    }
                                }
                                trans.Commit();
                                if (Convert.ToInt32(gvAFE.CurrentRow.Cells["ID"].Value) != 0)
                                {
                                    this.fillGrid(_selectedForcastId);
                                }
                            }
                            connection.Close();

                        }
                   // }

                    catch (Exception ex)
                    {
                        connection.Close();
                    }
                }
            }
        }

        private void fillSiteList()
        {
            lstSite.BeginUpdate();
            lstSite.Items.Clear();
            foreach (var items in DataRepository.GetAllSite())
            {
                ListViewItem li = new ListViewItem(items.SiteName) { Tag = items.Id };
                lstSite.Items.Add(li);
            }
            lstSite.EndUpdate();
        }

        private void cboCategory_TextChanged(object sender, EventArgs e)
         {
            btnAdd.Text = "Add";
            //btnEdit.Enabled = false;
            lstSite.Items.Clear();
            //this.fillSiteList();
        }

        private void cboCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cb = cboCategory.Text;
            int index = cboCategory.SelectedIndex;
            var bn = cboCategory.SelectedValue;
            //btnEdit.Enabled = false;
            if (index != 0)
            {
                //using (connection = new SqlConnection(con))
                //{
                using (cmdForecast = new SqlCommand("", connection))
                {
                    // connection.Open();
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    string _sql = "Select SiteID from ForecastCategorySiteInfo Where ForecastInfoID=" + _selectedForcastId + " And CategoryID=" + cboCategory.SelectedValue + "";
                    cmdForecast.CommandText = _sql;
                    DataTable dtSite = new System.Data.DataTable();
                    daForecast = new SqlDataAdapter(cmdForecast);
                    daForecast.Fill(dtSite);
                    if (dtSite.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtSite.Rows)
                        {
                            foreach (ListViewItem items in lstSite.Items)
                            {
                                if (dr["SiteID"].ToString() == items.Tag.ToString())
                                {
                                    items.Checked = true;
                                    // btnAdd.Text = "Edit";
                                }
                            }

                        }
                    }
                    PopRegion(cboCategory.SelectedValue.ToString());
                }

                //}
            }
            else
            {
                lstRegion.Items.Clear();
            }
        }
        private void gvSite_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (MessageBox.Show("Are you sure you want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        //using (connection = new SqlConnection(con))
                        //{
                        if (connection.State == ConnectionState.Open)
                        { }
                        else
                        { connection.Open(); }
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            using (cmdForecast = new SqlCommand("", connection, trans))
                            {
                                if (Convert.ToInt32(gvSite.CurrentRow.Cells["SID"].Value) != 0)
                                {
                                    string sSql1 = "DELETE FROM ForecastCategorySiteInfo WHERE ID=" + gvSite.CurrentRow.Cells["SID"].Value + "";
                                    cmdForecast.CommandText = sSql1;
                                    cmdForecast.ExecuteNonQuery();
                                    MessageBox.Show("Record Delete From The Database.", "Record Deleted.", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //FillGrid();
                                }
                                if (this.gvSite.SelectedRows.Count > 0)
                                {
                                    gvSite.Rows.RemoveAt(this.gvSite.SelectedRows[0].Index);
                                }
                            }
                            trans.Commit();
                            
                        }
                        connection.Close();

                    }
                    // }

                    catch (Exception ex)
                    {
                        connection.Close();
                    }
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            editSiteCategory();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Frmimportpatientno frm = new Frmimportpatientno();
            DialogResult dr = frm.ShowDialog();
            DataTable Dt;
            if (dr == DialogResult.OK)
            {
                Dt = frm.patientData;
                FillGridData(Dt, frm);
            }
        }
        private void FillGridData(DataTable Dtsite, Frmimportpatientno importpatient)
        {

            string regionName;

            string siteName;
            bool error = false;
            int errorno = 0;
            string rName = "";
            ForlabRegion region = null;
            IList<ForlabSite> result = new List<ForlabSite>();
            ForlabSite site = null;

            result = DataRepository.GetAllSite();
            if (result.Count > 0)
            {

                try
                {
                        if (connection.State == ConnectionState.Open)
                        { }
                        else
                        { connection.Open(); }

      for (var i = 0; i < Dtsite.Rows.Count; i++)
      {
          label12.Text = "Importing Data..........................";
          //////if (Convert.ToInt32(Dtsite.Rows[i][2].ToString()) > Convert.ToInt32(Dtsite.Rows[i][3].ToString()))
          //////{
          //////    errorno++;
          //////    importpatient.changecolorexcel("Target Patient should be greater than Current Patient", Color.Red, i);
          //////    error = true;
          //////    continue;
          //////}
          //  Dtsite.Rows[i]["rowno"] = i;
          regionName = Convert.ToString(Dtsite.Rows[i][0]).Trim();
          siteName = Convert.ToString(Dtsite.Rows[i][1]).Trim();
          if (rName != regionName)
          {
              if (!string.IsNullOrEmpty(regionName))
                  region = DataRepository.GetRegionByName(regionName);
              else
                  region = null;
              rName = regionName;
          }

          if (region != null)
          {

              if (!String.IsNullOrEmpty(siteName))
                  site = DataRepository.GetSiteByName(siteName, region.Id);
              if (site != null)
              {
                  bool isduplicate = true;

                  foreach (DataGridViewRow row in gvAFE.Rows)
                  {
                      if (row.Cells["SiteCategoryId"].Value != null)
                      {
                          if (row.Cells["SiteCategoryId"].Value.ToString() == Convert.ToString(site.SiteCategory.Id))
                          {
                              if (connection.State == ConnectionState.Open)
                              { }
                              else
                              { connection.Open(); }
                              using (SqlTransaction trans = connection.BeginTransaction())
                              {
                                  using (cmdForecast = new SqlCommand("", connection, trans))
                                  {
                                      string _sql = "Insert Into [ForecastCategorySiteInfo] Values(" + _selectedForcastId + "," + site.SiteCategory.Id + "," + site.Id + ");SELECT SCOPE_IDENTITY()";
                                      cmdForecast.CommandText = _sql;
                                      Convert.ToInt32(cmdForecast.ExecuteScalar());
                                  }
                                  trans.Commit();
                                  connection.Close();
                              }
                              row.Cells["CurrentPatient"].Value = Convert.ToInt32(row.Cells["CurrentPatient"].Value) + Convert.ToInt32(Dtsite.Rows[i][2].ToString());
                              row.Cells["TargetPatient"].Value = Convert.ToInt32(row.Cells["TargetPatient"].Value) + Convert.ToInt32(Dtsite.Rows[i][3].ToString());
                              string _sql1 = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + site.SiteCategory.Id + "";
                              cmdForecast.CommandText = _sql1;
                              DataTable dtSite = new System.Data.DataTable();
                              daForecast = new SqlDataAdapter(cmdForecast);
                              daForecast.Fill(dtSite);
                              gvSite.DataSource = dtSite;
                              isduplicate = false;
                          }
                      }
                  }

                  if (isduplicate == false)
                  {
                     // MessageBox.Show("Site Category  Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //  continue;

                  }
                  else
                  {

                      if (connection.State == ConnectionState.Open)
                      { }
                      else
                      { connection.Open(); }
                        using (SqlTransaction trans = connection.BeginTransaction())
                       {
                              using (cmdForecast = new SqlCommand("", connection, trans))
                              {
                                  string _sql = "Insert Into [ForecastCategorySiteInfo] Values(" + _selectedForcastId + "," + site.SiteCategory.Id + "," + site.Id + ");SELECT SCOPE_IDENTITY()";
                                  cmdForecast.CommandText = _sql;
                                  Convert.ToInt32(cmdForecast.ExecuteScalar());
                              }
                            trans.Commit();
                            connection.Close();
                        }


                          dt = gvAFE.DataSource as DataTable;
                          dt.Rows.Add(0, 0, site.SiteCategory.Id, site.SiteCategory.CategoryName, Dtsite.Rows[i][2], Dtsite.Rows[i][3]);
                          gvAFE.DataSource = dt;
                        string _sql1 = "Select fs.ID, fs.SiteID,[Site].SiteName,fs.ForecastInfoID as ForecastInfoID,fs.CategoryID as SiteCatID from ForecastCategorySiteInfo fs inner join Site on [Site].SiteID=fs.SiteID Where fs.ForecastInfoID=" + _selectedForcastId + " And fs.CategoryID=" + site.SiteCategory.Id + "";
                          cmdForecast.CommandText = _sql1;
                          DataTable dtSite = new System.Data.DataTable();
                          daForecast = new SqlDataAdapter(cmdForecast);
                          daForecast.Fill(dtSite);
                          gvSite.DataSource = dtSite;
                     

                  }


              }
              else
              {
                  //  Dtsite.Rows[i]["ErrorMsg"] = "Site Doesn't Exist";
                  errorno++;
                  importpatient.changecolorexcel("Site Doesn't Exist", Color.Red, i);
                  error = true;
              }
          }
          else
          {
              // Dtsite.Rows[i]["ErrorMsg"] = "Region Doesn't Exist";
              errorno++;
              importpatient.changecolorexcel("Region Doesn't Exist", Color.Red, i);
              error = true;
          }
          if (errorno > 20)
          {
              label12.Text = "";
              throw new Exception("There too many problem with Site Instrument data, please troubleshoot and try to import again.");
          }

      }
      //trans.Commit();
      //connection.Close();

 
                    if (error == true)
                        label12.Text = "Please Check excel to correct some records";
                    //this.DialogResult = DialogResult.OK;
                    //this.Close();

                }
                catch (Exception ex)
                {
                    label12.Text = "";
                    MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem items in lstSite.Items)
            {
                if (items.Checked == false)
                {
                   // items.Selected = true;
                    items.Checked = true;
                   // items.BackColor = ColorTranslator.FromHtml("#3399FF");
                   // items.ForeColor = Color.White;
                }
                else
                {
                   // items.Selected = false;
                    items.Checked = false;
                    items.BackColor = Color.White;
                    items.ForeColor = Color.Black;

                }

            }
        }

        private void lstRegion_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            string RegionID = "";
            lstSite.BeginUpdate();
            lstSite.Items.Clear();
            foreach (ListViewItem items in lstRegion.Items)
            {
                if (items.Checked == true)
                {
                    RegionID += "'" + items.Tag + "'" + ",";
                }
            }
            if (RegionID != "")
            {
                using (cmdForecast = new SqlCommand("", connection))
                {
                    // connection.Open();
                    RegionID = RegionID.TrimEnd(',');
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    string _sql = string.Format("select SiteName,SiteID from [Site] where RegionID in ({0})", RegionID);
                    cmdForecast.CommandText = _sql;
                    DataTable dtSite = new System.Data.DataTable();
                    daForecast = new SqlDataAdapter(cmdForecast);
                    daForecast.Fill(dtSite);
                    foreach (DataRow dr in dtSite.Rows)
                    {
                        ListViewItem li = new ListViewItem(dr["SiteName"].ToString()) { Tag = dr["SiteID"] };
                        lstSite.Items.Add(li);
                    }
                }
            }
            lstSite.EndUpdate();
        }
    }
}
