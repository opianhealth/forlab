﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using LQT.Core;
using System.Text.RegularExpressions;
namespace LQT.GUI.Quantification
{
    public partial class PatientGroupRatio : Form
    {
        private int _selectedForcastId = 0;
        private Form _mdiparent;
        public static Int64 _totalTarget = 0;
        public static string Mode = "";
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dtGrid = null;
        private string _startdate = "";
        private string _enddate = "";
        private string _period = "";
        private string pattern = "^[0-9]{0,2}$";
        private string ForecastType = "";
       // string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        public PatientGroupRatio(Form mdiparent,int ID, string mode, string startdate, string enddate, string period, Int64 totaltarget, DataTable Dt)
        {
            InitializeComponent();
            _totalTarget = totaltarget;
            _selectedForcastId = ID;
            _mdiparent = mdiparent;
            this._startdate = startdate;
            this._enddate = enddate;
            this._period = period;
            string _sql = "";
            _sql = string.Format(@"Select Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]='{0}'", _selectedForcastId);
            //using (connection = new SqlConnection(con))
            //{
                using (cmdForecast = new SqlCommand(_sql, connection))
                {
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    ForecastType = Convert.ToString(cmdForecast.ExecuteScalar());
                   connection.Close();
                }
          //  }
          //  _selectedForcastId = 55;
            this.dt1 = Dt;
            Mode = mode;
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;
            this.fillGrid(_selectedForcastId);
            btnPrevious.TabStop = false;
            btnPrevious.FlatStyle = FlatStyle.Flat;
            btnPrevious.FlatAppearance.BorderSize = 0;
            btnPrevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnNext.TabStop = false;
            btnNext.FlatStyle = FlatStyle.Flat;
            btnNext.FlatAppearance.BorderSize = 0;
            btnNext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            this.gvPGroup.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(gvPGroup_EditingControlShowing);
            this.gvPGroup.CellValidating += new DataGridViewCellValidatingEventHandler(gvPGroup_CellValidating);
            this.gvPGroup.DataError += this.gvPGroup_DataError;
        }
        public void gvPGroup_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            string checkValue = "", searchValue = "";
            bool stuts = false;
            if (!validation() == false)
            {
                //if (!checkPercent() == false)
                //{

                    searchValue = txtPGN.Text;
                    try
                    {
                        if (gvPGroup.Rows.Count > 0)
                        {
                            foreach (DataGridViewRow row in gvPGroup.Rows)
                            {
                                if (row.Cells[2].Value.ToString().ToLower().Equals(searchValue.ToLower()))
                                {
                                    //MessageBox.Show("Site Category Name is already exist");
                                    MessageBox.Show("Patient Group Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                                    txtPGN.Focus();
                                    stuts = true;
                                    break;
                                }

                            }
                        }
                        if (stuts == false)
                        {
                            string _ration = txtRation.Text;
                            decimal cValue = (_totalTarget * Convert.ToDecimal(txtRation.Text)) / 100;
                            dt = gvPGroup.DataSource as DataTable;
                            dt.Rows.Add(0, 0, txtPGN.Text, _ration, cValue.ToString());
                            gvPGroup.DataSource = dt;
                            txtPGN.Text = "";
                            txtRation.Text = "";
                            txtPGN.Focus();

                        }
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        connection.Close();
                    }
                //}
            }
        }
        private void fillGrid(int ForecastinfoID)
        {
            string _sql = "";
            try
            {
                if (ForecastinfoID > 0)
                    _sql = string.Format(@"SELECT [ID] ,[ForecastinfoID],[PatientGroupName] ,[PatientPercentage],[PatientRatio] FROM [PatientGroup] Where [ForecastinfoID]={0}", ForecastinfoID);
                else _sql = string.Format(@"SELECT [ID] ,[ForecastinfoID],[PatientGroupName] ,[PatientPercentage],[PatientRatio] FROM [PatientGroup] WHERE 1 = 0");
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        ///connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dt);
                        gvPGroup.DataSource = dt;
                    }

                    if (gvPGroup.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow row in gvPGroup.Rows)
                        {
                            row.Cells["PatientRatio"].Value = 0;
                            long re = (_totalTarget * Convert.ToInt32(row.Cells["PatientPercentage"].Value)) / 100;
                            row.Cells["PatientRatio"].Value = (_totalTarget * Convert.ToInt32(row.Cells["PatientPercentage"].Value)) / 100;
                        }
                    }
                //    connection.Close();
                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }
        private bool validation()
        {
            try
            {
                if (txtPGN.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Group Name.");
                    MessageBox.Show("Please Enter Patient Group Name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPGN.Focus();
                    return false;
                }
                if (txtRation.Text == "")
                {
                    //MessageBox.Show("Please Enter Patient Ratio.");
                    MessageBox.Show("Please Enter Patient Ratio.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRation.Focus();
                    return false;
                }
                if (txtRation.Text =="0")
                {
                    MessageBox.Show("Patient Ratio must be grater then Zero.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtRation.Focus();
                    return false;
                }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
            return true;
        }

        private void txtPGN_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                if (e.KeyChar == (char)Keys.Back) e.Handled = false;
                else e.Handled = true;
            }
        }

        private void txtRation_KeyPress(object sender, KeyPressEventArgs e)
        {
         //   e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);


            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }
        private bool checkPercent()
        {
            decimal percent = 0;
            foreach (DataGridViewRow row in gvPGroup.Rows)
            {
                percent += Convert.ToDecimal(row.Cells["PatientPercentage"].Value);
            }
            if (percent != 100)
            {
                MessageBox.Show("Percent Ratio should be Equal to 100.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            //if (txtRation.Text != "")
            //{
            //    if (percent + Convert.ToDecimal(txtRation.Text) != 100)
            //    {
            //        MessageBox.Show("Percent Ratio should be Equal to 100.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return false;
            //    }
            //}
            //else
            //{
            //    if (percent != 100)
            //    {
            //        MessageBox.Show("Percent Ratio should be Equal to 100.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        return false;
            //    }
            //}

            return true;
        }
        private void btnNext_Click(object sender, EventArgs e)
        {
            string _sql = "Insert into PatientGroup(ForecastinfoID,PatientGroupName ,PatientPercentage,PatientRatio) Values(@ForecastinfoID,@PatientGroupName ,@PatientPercentage,@PatientRatio);SELECT SCOPE_IDENTITY()";
            string _sqlUpdate = "UPDATE [dbo].[PatientGroup]  SET [ForecastinfoID] =@ForecastinfoID,[PatientGroupName] =@PatientGroupName,[PatientPercentage] =@PatientPercentage ,[PatientRatio] =@PatientRatio WHERE ID=@ID";

            DataTable table = new DataTable();
            table.Columns.Add("Patientgroupid", typeof(int));
            table.Columns.Add("Patientgroupname", typeof(string));
            // table.Columns.Add("Patientpercentage", typeof(double));
            //  table.Columns.Add("TargetPatient", typeof(int));
            //table.Columns.Add("StartDate", typeof(string));
            //table.Columns.Add("EndDate", typeof(string));
            //table.Columns.Add("Period", typeof(string));
            try
            {
                //using (connection = new SqlConnection(con))
                //{
               // DataTable Dt1 = new DataTable();
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                    if (!checkGridRecord() == false)
                    {
                        if (!checkPercent() == false)
                        {
                            using (SqlTransaction trans = connection.BeginTransaction())
                            {
                                if (gvPGroup.Rows.Count > 0)
                                {
                                    using (cmdForecast = new SqlCommand("", connection, trans))
                                    {
                                        for (int i = 0; i < gvPGroup.Rows.Count; i++)
                                        {
                                            if (Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value) == 0)
                                            {
                                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                                cmdForecast.Parameters.Add("@PatientGroupName", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientGroupName"].Value;
                                                cmdForecast.Parameters.Add("@PatientPercentage", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientPercentage"].Value;
                                                cmdForecast.Parameters.Add("@PatientRatio", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientRatio"].Value;
                                                cmdForecast.CommandText = _sql;
                                                int patientgroupId = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                                table.Rows.Add(patientgroupId, gvPGroup.Rows[i].Cells["PatientGroupName"].Value);
                                                //table.Rows.Add(ForecastCategoryInfo, _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));
                                            }
                                            if (Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value) > 0)
                                            {
                                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.NVarChar).Value = _selectedForcastId;
                                                cmdForecast.Parameters.Add("@PatientGroupName", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientGroupName"].Value;
                                                cmdForecast.Parameters.Add("@PatientPercentage", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientPercentage"].Value;
                                                cmdForecast.Parameters.Add("@PatientRatio", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["PatientRatio"].Value;
                                                cmdForecast.Parameters.Add("@ID", SqlDbType.NVarChar).Value = gvPGroup.Rows[i].Cells["ID"].Value;
                                                cmdForecast.CommandText = _sqlUpdate;
                                                cmdForecast.ExecuteNonQuery();
                                                //table.Rows.Add(Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value), _selectedForcastId, gvPGroup.Rows[i].Cells["SiteCategoryName"].Value, Convert.ToInt32(gvPGroup.Rows[i].Cells["CurrentPatient"].Value), Convert.ToInt32(gvPGroup.Rows[i].Cells["TargetPatient"].Value));

                                                table.Rows.Add(Convert.ToInt32(gvPGroup.Rows[i].Cells["ID"].Value), gvPGroup.Rows[i].Cells["PatientGroupName"].Value);
                                            }

                                            cmdForecast.Parameters.Clear();
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Please select atleast one patient group", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                                trans.Commit();
                            }
                        connection.Close();

                            ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save/Update successfully..", true);
                            this.Hide();

                            FrmTestTestingprotocol frm = new FrmTestTestingprotocol(_mdiparent,_selectedForcastId, "E", table, _startdate, _enddate, _period, _totalTarget, dt1);
                            // Form1 frm = new Form1();
                            frm.ShowDialog();
                            this.Close();
                        }

                    }

                //}
            }
            catch (Exception ex)
            {
               connection.Close();
            }
        }
        private bool checkGridRecord()
        {
            foreach (DataGridViewRow row in gvPGroup.Rows)
            {
                if (Convert.ToInt32(row.Cells[4].Value) == 0 || Convert.ToInt32(row.Cells[5].Value) == 0)
                {
                    MessageBox.Show("The value of Ratio And Calculated value must be a greater then Zero");
                    return false;
                }
            }
            return true;
        }

        private void gvPGroup_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int column = gvPGroup.CurrentCellAddress.X;
            if (column != 2)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            }
            else
            {
                var regex = new Regex(@"[^a-zA-Z0-9\s]");
                if (regex.IsMatch(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
            if (e.KeyChar == (char)Keys.Back) e.Handled = false;
        }
        public void gvPGroup_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            int newInteger = 0;
            string PatientPercentage = "0";
            string CurrentPatient = "0";
            string newValue = e.FormattedValue.ToString();
            string oldValue = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells[e.ColumnIndex].FormattedValue.ToString();
            if (string.IsNullOrEmpty(newValue))
            {
                e.Cancel = true;
                MessageBox.Show("Value is not empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               
            }

            if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["PatientPercentage"].Index)
            {
                PatientPercentage = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientRatio"].FormattedValue.ToString();
                ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "";
                if (Convert.ToDouble(e.FormattedValue) > 0)
                {
                    Double _calValue = (_totalTarget * Convert.ToDouble(e.FormattedValue)) / 100;
                    ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientRatio"].Value = _calValue;
                }
                else
                {
                    e.Cancel = true;
                    ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].ErrorText = "The value of Ratio must be grater then Zero";
                    MessageBox.Show("The value of Ratio must be a grater then Zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            
            if (e.ColumnIndex == ((System.Windows.Forms.DataGridView)(sender)).Columns["PatientGroupName"].Index)
            {
                int cIndex = e.RowIndex;
                string value = e.FormattedValue.ToString();
                value = ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["PatientGroupName"].FormattedValue.ToString();
                foreach (DataGridViewRow row in gvPGroup.Rows)
                {
                    int oIndex = row.Cells["PatientGroupName"].RowIndex;
                    if (cIndex != oIndex)
                    {
                        if (row.Cells["PatientGroupName"].Value.ToString().ToLower().Equals(e.FormattedValue.ToString().ToLower()))
                        {
                            //MessageBox.Show("Site Category Name is already exist");
                            MessageBox.Show("Patient Group Name is already exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error); //For triangle Warning 
                            row.Cells[oIndex].Value = value;
                            row.Cells[oIndex].Selected = true;
                            break;
                            //e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            this.Hide();

            if (ForecastType == "S")
            {
                Frmsitebysiteforecast frm = new Frmsitebysiteforecast(_mdiparent,_selectedForcastId, "E", _startdate, _enddate, _period);
                frm.ShowDialog();
            }
            else
            {

                AggregateForecast Ag = new AggregateForecast(_mdiparent,_selectedForcastId, "E", _startdate, _enddate, _period);
                Ag.ShowDialog();
            }
            this.Close();
        }

        private void gvPGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string sSql = "";
            if (e.ColumnIndex == 0)
            {
                try
                {
                    //using (connection = new SqlConnection(con))
                    //{
                     connection.Open();
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            using (cmdForecast = new SqlCommand("", connection, trans))
                            {


                                if (Convert.ToInt32(gvPGroup.CurrentRow.Cells["ID"].Value) != 0)
                                {
                                    sSql = "select count(*) from testingprotocol where PatientGroupID = " + Convert.ToInt32(gvPGroup.CurrentRow.Cells["ID"].Value) + " ";
                                    cmdForecast.CommandText = sSql;
                                    int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                    if (cnt > 0)
                                    {
                                        MessageBox.Show("You can't delete " + gvPGroup.CurrentRow.Cells["PatientGroupName"].Value.ToString() + " it is already use in testing protocol", "Information.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                    else
                                    {

                                        sSql = "DELETE FROM [PatientGroup] WHERE ID=" + Convert.ToInt32(gvPGroup.CurrentRow.Cells["ID"].Value) + " ";
                                        cmdForecast.CommandText = sSql;
                                        cmdForecast.ExecuteNonQuery();
                                        gvPGroup.Rows.RemoveAt(e.RowIndex);
                                        MessageBox.Show("Record Delete From The Database.", "Record Deleted.", MessageBoxButtons.OK, MessageBoxIcon.Information);



                                    }


                                    //FillGrid();
                                }
                                else
                                {
                                    gvPGroup.Rows.RemoveAt(e.RowIndex);
                                }
                              
                            }
                            trans.Commit();
                           
                        }
                        connection.Close();
                       // this.fillGrid(_selectedForcastId);
                    //}
                  
                }
                catch (Exception ex)
                {
                    connection.Close();
                }
            }
        }
    }
}
