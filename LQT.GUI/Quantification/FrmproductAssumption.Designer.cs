﻿namespace LQT.GUI.Quantification
{
    partial class FrmproductAssumption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtpatientN = new System.Windows.Forms.TextBox();
            this.txtpatientE = new System.Windows.Forms.TextBox();
            this.txtprogramrate = new System.Windows.Forms.TextBox();
            this.txtwastagerate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lsttestarea = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.coltestingarea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colwastagerate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colprogramgrowth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colpatientedit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colpatientnew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(734, 450);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.coltestingarea,
            this.colwastagerate,
            this.colprogramgrowth,
            this.colpatientedit,
            this.colpatientnew});
            this.dataGridView1.Location = new System.Drawing.Point(16, 231);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(694, 164);
            this.dataGridView1.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.button1);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtpatientN);
            this.panel3.Controls.Add(this.txtpatientE);
            this.panel3.Controls.Add(this.txtprogramrate);
            this.panel3.Controls.Add(this.txtwastagerate);
            this.panel3.Location = new System.Drawing.Point(210, 26);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(500, 189);
            this.panel3.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(310, 145);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(270, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Avg # of blood draws per new patient per year";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(290, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Avg # of blood draws per existing patient per year";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(288, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Program growth rate during the forecasting period";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Wastage Rate";
            // 
            // txtpatientN
            // 
            this.txtpatientN.Location = new System.Drawing.Point(310, 106);
            this.txtpatientN.MaxLength = 10;
            this.txtpatientN.Name = "txtpatientN";
            this.txtpatientN.Size = new System.Drawing.Size(56, 20);
            this.txtpatientN.TabIndex = 3;
            // 
            // txtpatientE
            // 
            this.txtpatientE.Location = new System.Drawing.Point(310, 77);
            this.txtpatientE.MaxLength = 10;
            this.txtpatientE.Name = "txtpatientE";
            this.txtpatientE.Size = new System.Drawing.Size(56, 20);
            this.txtpatientE.TabIndex = 2;
            // 
            // txtprogramrate
            // 
            this.txtprogramrate.Location = new System.Drawing.Point(310, 48);
            this.txtprogramrate.MaxLength = 4;
            this.txtprogramrate.Name = "txtprogramrate";
            this.txtprogramrate.Size = new System.Drawing.Size(100, 20);
            this.txtprogramrate.TabIndex = 1;
            // 
            // txtwastagerate
            // 
            this.txtwastagerate.Location = new System.Drawing.Point(310, 19);
            this.txtwastagerate.MaxLength = 4;
            this.txtwastagerate.Name = "txtwastagerate";
            this.txtwastagerate.Size = new System.Drawing.Size(100, 20);
            this.txtwastagerate.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Testing Area";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lsttestarea);
            this.panel2.Location = new System.Drawing.Point(16, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(177, 189);
            this.panel2.TabIndex = 1;
            // 
            // lsttestarea
            // 
            this.lsttestarea.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lsttestarea.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lsttestarea.Cursor = System.Windows.Forms.Cursors.Default;
            this.lsttestarea.FullRowSelect = true;
            this.lsttestarea.Location = new System.Drawing.Point(16, 14);
            this.lsttestarea.MultiSelect = false;
            this.lsttestarea.Name = "lsttestarea";
            this.lsttestarea.ShowItemToolTips = true;
            this.lsttestarea.Size = new System.Drawing.Size(146, 163);
            this.lsttestarea.TabIndex = 1;
            this.lsttestarea.UseCompatibleStateImageBehavior = false;
            this.lsttestarea.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Testing Area";
            this.columnHeader1.Width = 141;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tests and Tesing protocol to be included in this forecast";
            // 
            // coltestingarea
            // 
            this.coltestingarea.HeaderText = "Testing Area";
            this.coltestingarea.Name = "coltestingarea";
            this.coltestingarea.ReadOnly = true;
            // 
            // colwastagerate
            // 
            this.colwastagerate.HeaderText = "Wastage Rate";
            this.colwastagerate.MaxInputLength = 4;
            this.colwastagerate.Name = "colwastagerate";
            // 
            // colprogramgrowth
            // 
            this.colprogramgrowth.HeaderText = "Program growth rate during the forcasting period";
            this.colprogramgrowth.MaxInputLength = 4;
            this.colprogramgrowth.Name = "colprogramgrowth";
            this.colprogramgrowth.Width = 150;
            // 
            // colpatientedit
            // 
            this.colpatientedit.HeaderText = "Avg # of blood draws per existing patient per year";
            this.colpatientedit.MaxInputLength = 10;
            this.colpatientedit.Name = "colpatientedit";
            this.colpatientedit.Width = 150;
            // 
            // colpatientnew
            // 
            this.colpatientnew.HeaderText = "Avg # of blood draws per new patient per year";
            this.colpatientnew.MaxInputLength = 10;
            this.colpatientnew.Name = "colpatientnew";
            this.colpatientnew.Width = 150;
            // 
            // FrmproductAssumption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 489);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "FrmproductAssumption";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmproductAssumption_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView lsttestarea;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtpatientN;
        private System.Windows.Forms.TextBox txtpatientE;
        private System.Windows.Forms.TextBox txtprogramrate;
        private System.Windows.Forms.TextBox txtwastagerate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn coltestingarea;
        private System.Windows.Forms.DataGridViewTextBoxColumn colwastagerate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colprogramgrowth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colpatientedit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colpatientnew;
    }
}