﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Quantification;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;

using LQT.GUI.Tools;
using LQT.Core;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text.RegularExpressions;

namespace LQT.GUI.Quantification
{
    public partial class ForecastChart : Form
    {
        SqlConnection con = ConnectionManager.GetInstance().GetSqlConnection();
        private ForecastInfo _mMforecastinfo;
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mPMGeneralAssumption;
        DataSet ds = new DataSet();
        private int ID = 0;
        private string forecasttype = "";
        string onsite = "", _columnName = "", _MonthName = "", _variableName = "", varName = "", svarName = "", _type = "", sqlretrieve="";
        int noofMonth = 0, typeId = 0;
        DataTable dtGetValue = new DataTable();
        DataTable dtParameterValue = new DataTable();
        DataTable dtPGetValue = new DataTable();
        DataTable DTmonth = new DataTable();
        List<string> listProductType = new List<string>();
        DataTable DT = new DataTable();
        decimal _parameterSum = 0, _tparameterSum=0;
        //   DataTable DT = new DataTable();
      
     
      
        public int k = 0;
        public ForecastChart(Form mdiparent, int _forecastId, string mode, DataTable Dtpatient, string StartDate, string EndDate, string Period, Int64 totaltarget, DataTable Dt, ForecastInfo finfo)
        {
            InitializeComponent();
            this.ID = _forecastId;
            this.Top = 120;
            _mMforecastinfo = finfo;
            
            dtGetValue.Columns.Add("VariableName", typeof(string));
            dtGetValue.Columns.Add("VariableEffect", typeof(string));
            dtGetValue.Columns.Add("VariableDataType", typeof(string));
            dtPGetValue.Columns.Add("VariableName", typeof(string));
            dtPGetValue.Columns.Add("VariableEffect", typeof(string));
            dtPGetValue.Columns.Add("VariableDataType", typeof(string));
            if (_mMforecastinfo.ForecastType == "S") _type = MorbidityVariableUsage.OnEachSite.ToString();
            else if (_mMforecastinfo.ForecastType == "C") _type = MorbidityVariableUsage.OnAggSite.ToString();
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Test_Assumption"), _mMforecastinfo.ProgramId, _type);

            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 3 && gAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtGetValue.NewRow();
                    dr["VariableName"] = gAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
                    dtGetValue.Rows.Add(dr);
                }
                dtGetValue.AcceptChanges();
            }


            _mPMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Product_Assumption"), _mMforecastinfo.ProgramId, _type);

            sqlretrieve = "";


            foreach (MMGeneralAssumption gPAssumption in _mPMGeneralAssumption)
            {
                if (gPAssumption.AssumptionType == 2 && gPAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtPGetValue.NewRow();
                    sqlretrieve += "[" + gPAssumption.VariableName + "],";
                    dr["VariableName"] = gPAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gPAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gPAssumption.VariableDataType.ToString();
                    dtPGetValue.Rows.Add(dr);
                }
                dtPGetValue.AcceptChanges();
            }
            sqlretrieve = sqlretrieve.TrimEnd(',');

            string strSql = "select " + sqlretrieve + " from TestingAssumption  where ForecastinfoID=" + ID + "";
            SqlDataAdapter adapt = new SqlDataAdapter(strSql, con);
            adapt.Fill(dtParameterValue);

            for (int ki = 0; ki < dtParameterValue.Rows.Count; ki++)
            {
                foreach (DataColumn dc in dtParameterValue.Columns)
                {
                    foreach (DataRow dr in dtPGetValue.Rows)
                    {
                        string v1 = dr["VariableName"].ToString();
                        string v2 = dc.ColumnName.ToString();
                        if (v1.Equals(v2))
                        {
                            _parameterSum = ((Convert.ToDecimal(dtParameterValue.Rows[ki]["" + v2 + ""])) / 100);
                            if (dr["variableEffect"].ToString() == "True") _tparameterSum = _tparameterSum + _parameterSum;
                            if (dr["variableEffect"].ToString() == "False") _tparameterSum = _tparameterSum - _parameterSum;

                        }

                    }
                }

            }

            //foreach (DataRow dr in dtParameterValue.Rows)
            //{
            //    foreach (DataColumn dc in dtParameterValue.Columns)
            //    {
            //        _parameterSum += (int)dr[dc];
            //    }
            //} 
        }

        private void ForecastChart_Load(object sender, EventArgs e)
        {
            fillChartPatient();
            fillChartTectNoByTest();
            fillchartByArea();
            fillChartByProductPrice();
        }

        private void fillChartPatient()
        {

            Chart1.Width = 1200;
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter("Select  columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
            adapt.Fill(ds);
      

             foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                       

                            Chart1.Series["Patient Number"].Points.AddXY(dr["columnname"].ToString(), dr["serial"].ToString());
                          //  chart2.Series["Coverage"].LegendText = "Instrument Distribution";
                    }
             Chart1.Series["Patient Number"].IsValueShownAsLabel = true;
             Chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
             Chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            //lqtChart1.Series["Patient Number"].XValueMember = "columnname";
            ////set the member columns of the chart data source used to data bind to the X-values of the series  
            //lqtChart1.Series["Patient Number"].YValueMembers = "serial";
            ////   Chart1.Titles.Add("Patient Number over forecast");
            //lqtChart1.Series["Patient Number"].IsValueShownAsLabel = true;
            //lqtChart1.ChartAreas[0].AxisX.Interval = 1;
            //lqtChart1.Series["Patient Number"]["PixelPointWidth"] = "25";
            //lqtChart1.Series["Patient Number"].Points.Add(
            //// Chart1.ChartAreas[0].AxisX.LineColor = Color.Black;
            //lqtChart1.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            //lqtChart1.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            con.Close();

        }

//        private void fillChartTectNoByTest()
//        {

//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }

//            DataTable period = new System.Data.DataTable();
//            DataTable months = new System.Data.DataTable();
//            DataTable Duration = new System.Data.DataTable();
//            DataTable Percentage = new System.Data.DataTable();
//            DataTable TestingProtocal = new System.Data.DataTable();
//            DataTable Calculate = new System.Data.DataTable();
//            DataTable TestName = new System.Data.DataTable();
//            DataTable TestArea = new System.Data.DataTable();
//            DataTable ratiowise = new System.Data.DataTable();
//            DataTable TestChart = new System.Data.DataTable();
//            DataTable BaseLine = new System.Data.DataTable();
//            DataTable yr = new System.Data.DataTable(); DataTable margins = new System.Data.DataTable(); DataTable Percentag = new System.Data.DataTable();

//            DataTable TestNoChart = new System.Data.DataTable();

//            DataTable TestByMonth = new System.Data.DataTable(); TestByMonth.Columns.Add("FOreCastID"); TestByMonth.Columns.Add("TestID");
//            TestByMonth.Columns.Add("Month"); TestByMonth.Columns.Add("Tests"); TestByMonth.Columns.Add("PGrp"); TestByMonth.Columns.Add("SNo");


//            SqlCommand cmd02 = new SqlCommand();
//            cmd02.CommandType = CommandType.Text;

//            cmd02.Connection = con;
//            cmd02.CommandText = "delete from forecastedtestbytest  where forecastid='" + ID + "'";
//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }
//            cmd02.ExecuteNonQuery();

//            con.Close();


//            SqlDataAdapter adapt0 = new SqlDataAdapter("Select period from forecastinfo where forecastid='" + ID + "'", con);
//            adapt0.Fill(period);



//            SqlDataAdapter adapt = new SqlDataAdapter("Select columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt.Fill(months);
//            int yrs = 0;
//            if (period.Rows[0][0].ToString() == "Monthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 1) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Bimonthly")
//            {
//                yrs = ((months.Rows.Count - 1) * 2) / 12;
//            }
//            if (period.Rows[0][0].ToString() == "Quarterly")
//            {
//                yrs = ((months.Rows.Count - 1) * 4) / 12;
//            }
//            //if (period.Rows[0][0].ToString() == "Monthly")
//            //{ }


//            months.Columns.Add("NewPatient");

//            SqlDataAdapter adapt1 = new SqlDataAdapter("Select count(columnname) from patientnumberdetail where forecastid='" + ID + "'", con);
//            adapt1.Fill(Duration);

//            // int yrs = Convert.ToInt32(Duration.Rows[0][0].ToString())/12;

//            for (int i = 0; i < Convert.ToInt32(Duration.Rows[0][0]) - 1; i++)
//            {
//                months.Rows[i + 1][2] = (Convert.ToDecimal(months.Rows[i + 1][1]) - Convert.ToDecimal(months.Rows[i][1])).ToString();

//            }

//            SqlDataAdapter adapt2 = new SqlDataAdapter("Select id,patientgroupname,patientpercentage from patientgroup where forecastinfoid='" + ID + "' order by id", con);
//            adapt2.Fill(Percentage);

//            decimal newP = Convert.ToDecimal(months.Rows[1][2]);
//            decimal OldP = Convert.ToDecimal(months.Rows[0][1]);
//            Percentage.Columns.Add("PercentageCal");
//            Percentage.Columns.Add("PercentageCalOld");
//            for (int i1 = 0; i1 < Percentage.Rows.Count; i1++)
//            {
//                Percentage.Rows[i1][3] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * newP) / 100).ToString();
//                Percentage.Rows[i1][4] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * OldP) / 100).ToString();
//            }

//            SqlDataAdapter adapt3 = new SqlDataAdapter(@"select TestID, PatientGroupid, PercentagePanel,[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],
//                [Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] from TestingProtocol 
//where ForecastinfoID=" + ID + "  group by testid,PatientGroupID,PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],[Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], [TestRepeatPerYear],[SymptomTestPerYear] order by TestID", con);
//            adapt3.Fill(TestingProtocal);

//            TestingProtocal.Columns.Add("Percentage1"); TestingProtocal.Columns.Add("Percentageold");
//            for (int i2 = 0; i2 < Percentage.Rows.Count; i2++)
//            {
//                for (int i3 = 0; i3 < TestingProtocal.Rows.Count; i3++)
//                {
//                    if (Convert.ToInt32(Percentage.Rows[i2][0]) == Convert.ToInt32(TestingProtocal.Rows[i3][1]))
//                    {

//                        TestingProtocal.Rows[i3][22] = (Convert.ToDecimal(Convert.ToDecimal(Percentage.Rows[i2][3]) * Convert.ToDecimal(TestingProtocal.Rows[i3][2])) / 100).ToString();
//                        TestingProtocal.Rows[i3][23] = Convert.ToDecimal(Percentage.Rows[i2][4]).ToString();

//                    }

//                }
//            }


//            TestChart.Columns.Add("Total");
//            TestChart.Columns.Add("ForeCastId");
//            TestChart.Columns.Add("BaseLine");
//            TestChart.Columns.Add("TestID");
//            TestChart.Columns.Add("PatientGroupID");
//            TestingProtocal.Columns.Add("Total0"); TestingProtocal.Columns.Add("TotalNo"); TestingProtocal.Columns.Add("TotalGap");

//            for (int k4 = 1; k4 < Convert.ToInt32(Duration.Rows[0][0]); k4++)
//            {
//                TestChart.Columns.Add(k4.ToString());
//            }

//            SqlCommand cmd01 = new SqlCommand();
//            cmd01.CommandType = CommandType.Text;

//            cmd01.Connection = con;
//            cmd01.CommandText = "delete from percentageval";
//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }
//            cmd01.ExecuteNonQuery();

//            con.Close();


//            for (int k3 = 0; k3 < TestingProtocal.Rows.Count; k3++)
//            {
//                int k6 = 0;
//                for (int k5 = 0; k5 < 12; k5++)
//                {
//                    if (TestingProtocal.Rows[k3][7 + k5].ToString() == "0")
//                    {
//                        k6 = k6 + 1;
//                    }
//                }
//                TestingProtocal.Rows[k3][24] = k6;
//                TestingProtocal.Rows[k3][25] = 12 - k6;
//                 int no=0;
//                if (Convert.ToInt32(TestingProtocal.Rows[k3][25]) !=0 )
//                 no = 12 / Convert.ToInt32(TestingProtocal.Rows[k3][25]);
//                TestingProtocal.Rows[k3][26] = no.ToString();

//                string sql1 = "";

//                SqlCommand cmd1 = new SqlCommand();
//                cmd1.CommandType = CommandType.Text;

//                cmd1.Connection = con;
//                cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld,TotalTestPerYear,TestRepeatPerYear,SymptomTestPerYear) values('"
//                          + TestingProtocal.Rows[k3][0].ToString().Trim() + "','"
//                          + TestingProtocal.Rows[k3][1].ToString().Trim() + "','" + TestingProtocal.Rows[k3][22].ToString().Trim() + "','"
//                          + TestingProtocal.Rows[k3][23].ToString().Trim() + "', '" + TestingProtocal.Rows[k3][19].ToString().Trim() + "','"
//                          + TestingProtocal.Rows[k3][20].ToString().Trim() + "',  '" + TestingProtocal.Rows[k3][21].ToString().Trim() + "')";
//                if (con.State == ConnectionState.Closed)
//                {
//                    con.Open();
//                }
//                int ij = cmd1.ExecuteNonQuery();

//                con.Close();

//            }



//            BaseLine.Columns.Add("Test"); BaseLine.Columns.Add("PGrp"); BaseLine.Columns.Add("No"); BaseLine.Columns.Add("Value");

//            for (int k9 = 0; k9 < TestingProtocal.Rows.Count; k9++)
//            {
//                for (int k8 = 0; k8 < 12; k8++)
//                {

//                    if (Convert.ToInt32(TestingProtocal.Rows[k9][k8 + 7]) != 0)
//                    {
//                        BaseLine.Rows.Add();
//                        int a1 = BaseLine.Rows.Count;
//                        BaseLine.Rows[a1 - 1][0] = TestingProtocal.Rows[k9][0];
//                        BaseLine.Rows[a1 - 1][1] = TestingProtocal.Rows[k9][1];

//                        BaseLine.Rows[a1 - 1][2] = k8 + 1;
//                        BaseLine.Rows[a1 - 1][3] = TestingProtocal.Rows[k9][k8 + 7].ToString();

//                    }

//                }
//            }

//            SqlCommand cmd0 = new SqlCommand();
//            cmd0.CommandType = CommandType.Text;

//            cmd0.Connection = con;
//            cmd0.CommandText = "delete from Temptbl1";
//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }
//            cmd0.ExecuteNonQuery();

//            con.Close();


//            for (int i = 0; i < BaseLine.Rows.Count; i++)
//            {
//                string sql = "";

//                SqlCommand cmd = new SqlCommand();
//                cmd.CommandType = CommandType.Text;

//                cmd.Connection = con;
//                cmd.CommandText = "insert into Temptbl1 (tst, PGrp, Num,Valu) values('"
//                          + BaseLine.Rows[i][0].ToString().Trim() + "','"
//                          + BaseLine.Rows[i][1].ToString().Trim() + "','" + BaseLine.Rows[i][2].ToString().Trim() + "','"
//                          + BaseLine.Rows[i][3].ToString().Trim() + "')";
//                if (con.State == ConnectionState.Closed)
//                {
//                    con.Open();
//                }
//                int ij = cmd.ExecuteNonQuery();

//                con.Close();
//            }

//            SqlCommand cm0 = new SqlCommand();
//            cm0.CommandType = CommandType.Text;

//            cm0.Connection = con;
//            cm0.CommandText = "delete from testbymonth where forecastid='" + ID + "'";
//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }
//            cm0.ExecuteNonQuery();

//            con.Close();

//            for (int k3 = 0; k3 < TestingProtocal.Rows.Count; k3++)
//            {



//                TestChart.Clear(); TestByMonth.Clear();
//                SqlDataAdapter adapt11 = new SqlDataAdapter("Select max(num) from temptbl1 where tst='" + TestingProtocal.Rows[k3][0] + "' and pgrp='" + TestingProtocal.Rows[k3][1] + "'", con);
//                adapt11.Fill(yr);

//                SqlDataAdapter adapt12 = new SqlDataAdapter("Select num,valu from temptbl1 where tst='" + TestingProtocal.Rows[k3][0] + "' and pgrp='" + TestingProtocal.Rows[k3][1] + "'", con);
//                adapt12.Fill(margins);

//                int y = yrs * 12 / Convert.ToInt32(yr.Rows[0][0]);

//                for (int k7 = 0; k7 < 12; k7++)
//                {
//                    TestChart.Rows.Add();
//                    TestChart.Rows[k7][1] = ID.ToString();
//                    TestChart.Rows[k7][2] = TestingProtocal.Rows[k3][6].ToString();
//                    TestChart.Rows[k7][3] = TestingProtocal.Rows[k3][0].ToString();
//                    TestChart.Rows[k7][4] = TestingProtocal.Rows[k3][1].ToString();

//                    for (int j1 = 0; j1 < yrs; j1++)
//                    {
//                        // TestChart.Rows[k7][5 + k7] = TestChart.Rows[k7][2];
//                        for (int j2 = 0; j2 < margins.Rows.Count; j2++)
//                        {
//                            string mr = margins.Rows[j2][0].ToString();
//                            if (k7 + 4 + Convert.ToInt32(mr) < TestChart.Columns.Count)
//                            {
//                                TestChart.Rows[k7][k7 + 4 + Convert.ToInt32(mr)] = margins.Rows[j2][1].ToString();
//                                if (k7 + 16 + Convert.ToInt32(mr) < TestChart.Columns.Count)
//                                {
//                                    TestChart.Rows[k7][k7 + 16 + Convert.ToInt32(mr)] = margins.Rows[j2][1].ToString();
//                                }
//                            }
//                        }

//                    }

//                    //for (int j1 = 0; j1 < margins.Rows.Count; j1++)
//                    //{
//                    //    TestChart.Rows[k7][k7 + 5] = margins.Rows[j1][1];
//                    //    if (TestChart.Columns.Count > (k7 + 17))
//                    //    {
//                    //        TestChart.Rows[k7][k7 + 5 + 12] = margins.Rows[j1][1];
//                    //    }
//                    //    if (k7 + Convert.ToInt32(margins.Rows[j1][0]) < 12)
//                    //    {

//                    //        TestChart.Rows[k7][k7 + 5 + Convert.ToInt32(margins.Rows[j1][0])] = margins.Rows[j1][1];
//                    //        if (TestChart.Columns.Count > (k7 + 17))
//                    //        {
//                    //            TestChart.Rows[k7][k7 + 5 + 12 + Convert.ToInt32(margins.Rows[j1][0])] = margins.Rows[j1][1];
//                    //        }
//                    //    }


//                    //}


//                    int tot = 0;
//                    for (int jj = 0; jj < TestChart.Columns.Count - 5; jj++)
//                    {
//                        if (!string.IsNullOrEmpty(TestChart.Rows[k7][jj + 5].ToString()))
//                        {
//                            tot = tot + Convert.ToInt32(TestChart.Rows[k7][jj + 5]);
//                        }

//                    }
//                    TestChart.Rows[k7][0] = tot;


//                }
//                int totalTest = 0;
//                for (int jj = 0; jj < TestChart.Rows.Count; jj++)
//                {

//                    totalTest = totalTest + Convert.ToInt32(TestChart.Rows[jj][0]);

//                }

//                SqlDataAdapter adapt13 = new SqlDataAdapter(@"Select testn,pgrpid,pernew,perold,TotalTestPerYear
//      ,TestRepeatPerYear,SymptomTestPerYear from percentageval where testn='" + TestChart.Rows[0][3] + "' and pgrpid='" + TestChart.Rows[0][4] + "'", con);
//                adapt13.Fill(Percentag);


//                decimal totalcal = (totalTest * Convert.ToDecimal(Percentag.Rows[0][2])) + (Convert.ToDecimal(Percentag.Rows[0][3]) * (yrs * Convert.ToDecimal(Percentag.Rows[0][4])));
//                Decimal per = Convert.ToDecimal(Percentag.Rows[0][5]) + Convert.ToDecimal(Percentag.Rows[0][6]);

//                decimal finalCal = totalcal + (totalcal * per / 100);

//                string sql = "";

//                SqlCommand cmd = new SqlCommand();
//                cmd.CommandType = CommandType.Text;

//                cmd.Connection = con;
//                cmd.CommandText = "insert into forecastedtestbytest (ForeCastID, Tst, PGrp,TotalTst) values('"
//                          + TestChart.Rows[0][1].ToString().Trim() + "','"
//                          + TestChart.Rows[0][3].ToString().Trim() + "','" + TestChart.Rows[0][4].ToString().Trim() + "', '" + finalCal + "')";
//                if (con.State == ConnectionState.Closed)
//                {
//                    con.Open();
//                }
//                int ij = cmd.ExecuteNonQuery();

//                con.Close();


//                for (int i = 0; i < TestChart.Columns.Count - 5; i++)
//                {
//                    int tot = 0; int j = 0;
//                    for (j = 0; j < 12; j++)
//                    {
//                        if (!string.IsNullOrEmpty(TestChart.Rows[j][i + 5].ToString()))
//                        {
//                            tot = tot + Convert.ToInt32(TestChart.Rows[j][i + 5]);
//                        }
//                    }


//                    string mnth = "";
//                    if ((i + 1) % 12 == 1)
//                    {
//                        mnth = "Jan";
//                    }
//                    if ((i + 1) % 12 == 2)
//                    {
//                        mnth = "Feb";
//                    }
//                    if ((i + 1) % 12 == 3)
//                    {
//                        mnth = "Mar";
//                    }
//                    if ((i + 1) % 12 == 4)
//                    {
//                        mnth = "Apr";
//                    }
//                    if ((i + 1) % 12 == 5)
//                    {
//                        mnth = "May";
//                    }
//                    if ((i + 1) % 12 == 6)
//                    {
//                        mnth = "Jun";
//                    }
//                    if ((i + 1) % 12 == 7)
//                    {
//                        mnth = "Jul";
//                    }
//                    if ((i + 1) % 12 == 8)
//                    {
//                        mnth = "Aug";
//                    }
//                    if ((i + 1) % 12 == 9)
//                    {
//                        mnth = "Sep";
//                    }
//                    if ((i + 1) % 12 == 10)
//                    {
//                        mnth = "Oct";
//                    }
//                    if ((i + 1) % 12 == 11)
//                    {
//                        mnth = "Nov";
//                    }
//                    if ((i + 1) % 12 == 0)
//                    {
//                        mnth = "Dec";
//                    }
//                    string m = months.Rows[0][0].ToString();

//                    string ys = (m.Length > 3) ? m.Substring(m.Length - 4, 4) : m;
//                    string years = "";
//                    if (i + 1 <= 12)
//                    {
//                        years = ys;
//                    }

//                    if (i + 1 > 12 && i + 1 <= 24)
//                    {
//                        years = (Convert.ToInt32(ys) + 1).ToString();
//                    }
//                    if (i + 1 > 24 && i + 1 <= 36)
//                    {
//                        years = (Convert.ToInt32(ys) + 2).ToString();
//                    }
//                    if (i + 1 > 36 && i + 1 <= 48)
//                    {
//                        years = (Convert.ToInt32(ys) + 3).ToString();
//                    }
//                    if (i + 1 > 48 && i + 1 <= 60)
//                    {
//                        years = (Convert.ToInt32(ys) + 4).ToString();
//                    }

//                    TestByMonth.Rows.Add();
//                    TestByMonth.Rows[i][0] = TestChart.Rows[0][1];
//                    TestByMonth.Rows[i][1] = TestChart.Rows[0][3];
//                    TestByMonth.Rows[i][2] = string.Concat(mnth, " ", years);
//                    TestByMonth.Rows[i][3] = tot.ToString();
//                    TestByMonth.Rows[i][4] = TestChart.Rows[0][4];
//                    TestByMonth.Rows[i][5] = (Convert.ToInt32(i) + 1).ToString();

//                }

//                for (int ij1 = 0; ij1 < TestByMonth.Rows.Count; ij1++)
//                {
//                    string s = "";


//                    SqlCommand cm = new SqlCommand();
//                    cm.CommandType = CommandType.Text;

//                    cm.Connection = con;
//                    cm.CommandText = "insert into Testbymonth (ForeCastID, TestID, Month,TstNo,PGrp,SNo) values('"
//                              + TestByMonth.Rows[ij1][0].ToString().Trim() + "','"
//                              + TestByMonth.Rows[ij1][1].ToString().Trim() + "','" + TestByMonth.Rows[ij1][2].ToString().Trim() + "', '" + TestByMonth.Rows[ij1][3].ToString().Trim() + "', '" + TestByMonth.Rows[ij1][4].ToString().Trim() + "','" + TestByMonth.Rows[ij1][5].ToString() + "')";
//                    if (con.State == ConnectionState.Closed)
//                    {
//                        con.Open();
//                    }
//                    int jk = cm.ExecuteNonQuery();

//                    con.Close();
//                }


//            }


//            SqlDataAdapter adapt14 = new SqlDataAdapter(@"SELECT test.TestName,round(sum(ForecastedTestByTest.TotalTst),0) as tst
//FROM test,ForecastedTestByTest WHERE test.TestID=ForecastedTestByTest.Tst group by test.TestName", con);
//            adapt14.Fill(TestNoChart);

//            Chart3.DataSource = TestNoChart;

//            //set the member of the chart data source used to data bind to the X-values of the series  
//            Chart3.Series["Forecasted Test"].XValueMember = "TestName";
//            //set the member columns of the chart data source used to data bind to the X-values of the series  
//            Chart3.Series["Forecasted Test"].YValueMembers = "tst";
//            // chart2.Series["Forecasted Test Ratio"].ChartType = SeriesChartType.Pie;
//            Chart3.Series["Forecasted Test"].IsValueShownAsLabel = true;
//            Chart3.Series["Forecasted Test"]["PixelPointWidth"] = "25";
//            Chart3.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
//            Chart3.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
//            //chart2.Titles.Add("Ratio of forcasted test by testing area");
//            con.Close();

//        }

        private void fillChartTectNoByTest()
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            if (_mMforecastinfo.ForecastType == "C") onsite = MorbidityVariableUsage.OnAggSite.ToString();
            else onsite = MorbidityVariableUsage.OnEachSite.ToString();
            noofMonth = InsertRepository.returnMonth(con, "SELECT [NoofYear]  FROM [MMProgram] Where Id=" + _mMforecastinfo.ProgramId + "");
            typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), MorbidityGeneralAssumptionTypes.Test_Assumption.ToString().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByTypeAndProgram(typeId, _mMforecastinfo.ProgramId, onsite);
            this.Left = 250;
            for (int i = 1; i <= 12 * noofMonth; i++)
            {
                _columnName += "[Month" + Convert.ToString(i) + "] ,";
                _MonthName += "[Month" + Convert.ToString(i) + "] ,";
            }
            foreach (var item in mMGAbyType)
            {
                _columnName += "[" + item.VariableName + "]" + ",";
                _variableName += "[" + item.VariableName + "]" + ",";
            }

            DataTable period = new System.Data.DataTable();
            DataTable months = new System.Data.DataTable();
            DataTable Duration = new System.Data.DataTable();
            DataTable Percentage = new System.Data.DataTable();
            DataTable TestingProtocal = new System.Data.DataTable();
            DataTable Calculate = new System.Data.DataTable();
            DataTable TestName = new System.Data.DataTable();
            DataTable TestArea = new System.Data.DataTable();
            DataTable ratiowise = new System.Data.DataTable();
            DataTable TestChart = new System.Data.DataTable();
            DataTable BaseLine = new System.Data.DataTable();
            DataTable yr = new System.Data.DataTable(); DataTable margins = new System.Data.DataTable(); DataTable Percentag = new System.Data.DataTable();

            DataTable TestNoChart = new System.Data.DataTable();

            DataTable TestByMonth = new System.Data.DataTable(); TestByMonth.Columns.Add("FOreCastID"); TestByMonth.Columns.Add("TestID");
            TestByMonth.Columns.Add("Month"); TestByMonth.Columns.Add("Tests"); TestByMonth.Columns.Add("PGrp"); TestByMonth.Columns.Add("SNo");TestByMonth.Columns.Add("NewPatient"); TestByMonth.Columns.Add("TestPerYear");
            foreach (var item in mMGAbyType)
            {
                TestByMonth.Columns.Add(item.VariableName); 
            }
            //TestByMonth.Columns.Add("RepeatTest"); TestByMonth.Columns.Add("Symptomtest");


            SqlCommand cmd02 = new SqlCommand();
            cmd02.CommandType = CommandType.Text;

            cmd02.Connection = con;
            cmd02.CommandText = "delete from forecastedtestbytest  where forecastid='" + ID + "'";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cmd02.ExecuteNonQuery();

            con.Close();


            SqlDataAdapter adapt0 = new SqlDataAdapter("Select period from forecastinfo where forecastid='" + ID + "'", con);
            adapt0.Fill(period);



            SqlDataAdapter adapt = new SqlDataAdapter("Select columnname,serial from patientnumberdetail where forecastid='" + ID + "'", con);
            adapt.Fill(months);
            int yrs = 0;
            if (period.Rows[0][0].ToString() == "Monthly")
            {
                yrs = ((months.Rows.Count - 1) * 1) / 12;
            }
            if (period.Rows[0][0].ToString() == "Bimonthly")
            {
                yrs = ((months.Rows.Count - 1) * 2) / 12;
            }
            if (period.Rows[0][0].ToString() == "Quarterly")
            {
                yrs = ((months.Rows.Count - 1) * 4) / 12;
            }
            //if (period.Rows[0][0].ToString() == "Monthly")
            //{ }


            months.Columns.Add("NewPatient");

            SqlDataAdapter adapt1 = new SqlDataAdapter("Select count(columnname) from patientnumberdetail where forecastid='" + ID + "'", con);
            adapt1.Fill(Duration);

            // int yrs = Convert.ToInt32(Duration.Rows[0][0].ToString())/12;

            for (int i = 0; i < Convert.ToInt32(Duration.Rows[0][0]) - 1; i++)
            {
                months.Rows[i + 1][2] = (Convert.ToDecimal(months.Rows[i + 1][1]) - Convert.ToDecimal(months.Rows[i][1])).ToString();

            }

            SqlDataAdapter adapt2 = new SqlDataAdapter("Select id,patientgroupname,patientpercentage from patientgroup where forecastinfoid='" + ID + "' order by id", con);
            adapt2.Fill(Percentage);

            decimal newP=0;
            if(months.Rows.Count>1)
             newP = Convert.ToDecimal(months.Rows[1][2]);
            else newP = 0;
            decimal OldP = Convert.ToDecimal(months.Rows[0][1]);
            Percentage.Columns.Add("PercentageCal");
            Percentage.Columns.Add("PercentageCalOld");
            for (int i1 = 0; i1 < Percentage.Rows.Count; i1++)
            {
                Percentage.Rows[i1][3] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * newP) / 100).ToString();
                Percentage.Rows[i1][4] = ((Convert.ToDecimal(Percentage.Rows[i1][2]) * OldP) / 100).ToString();
            }
            
//            SqlDataAdapter adapt3 = new SqlDataAdapter(@"select TestID, PatientGroupid, PercentagePanel,[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],
//                [Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear] from TestingProtocol 
//where ForecastinfoID=" + ID + "  group by testid,PatientGroupID,PercentagePanel, [Baseline] ,[Month1],[Month2] ,[Month3],[Month4] ,[Month5] ,[Month6],[Month7],[Month8] ,[Month9],[Month10],[Month11] ,[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], [TestRepeatPerYear],[SymptomTestPerYear] order by TestID", con);

            SqlDataAdapter adapt3 = new SqlDataAdapter(@"select TestID, PatientGroupid, PercentagePanel,[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], [Baseline] ," + _columnName.TrimEnd(',') + ",[TotalTestPerYear] from TestingProtocol \n" +
                                      "where ForecastinfoID=" + ID + "  group by testid,PatientGroupID,PercentagePanel, [Baseline] ,[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear], " + _columnName.TrimEnd(',') + " order by TestID", con);

            adapt3.Fill(TestingProtocal);  //changes in according to MMProgram 

            TestingProtocal.Columns.Add("Percentage1"); TestingProtocal.Columns.Add("Percentageold");
            for (int i2 = 0; i2 < Percentage.Rows.Count; i2++)
            {
                for (int i3 = 0; i3 < TestingProtocal.Rows.Count; i3++)
                {
                    if (Convert.ToInt32(Percentage.Rows[i2][0]) == Convert.ToInt32(TestingProtocal.Rows[i3][1]))
                    {

                        TestingProtocal.Rows[i3]["Percentage1"] = (Convert.ToDecimal(Convert.ToDecimal(Percentage.Rows[i2][3]) * Convert.ToDecimal(TestingProtocal.Rows[i3]["PercentagePanel"])) / 100).ToString();
                        TestingProtocal.Rows[i3]["Percentageold"] = Convert.ToDecimal(Percentage.Rows[i2][4]).ToString();

                    }

                }
            }


            TestChart.Columns.Add("Total");
            TestChart.Columns.Add("ForeCastId");
            TestChart.Columns.Add("BaseLine");
            TestChart.Columns.Add("TestID");
            TestChart.Columns.Add("PatientGroupID");
            TestChart.Columns.Add("TestPerYear");
            foreach (var item in mMGAbyType)
            {
                TestChart.Columns.Add(item.VariableName);
            }

            //TestChart.Columns.Add("TestPerYear"); TestChart.Columns.Add("RepeatTest"); TestChart.Columns.Add("Symptomtest"); 
            
            //create column here according to MMProgram
            TestingProtocal.Columns.Add("Total0"); TestingProtocal.Columns.Add("TotalNo"); TestingProtocal.Columns.Add("TotalGap");

            //for (int k4 = 1; k4 < Convert.ToInt32(Duration.Rows[0][0]); k4++)
            for (int k4 = 1; k4 <= yrs * 12; k4++)
            {
                TestChart.Columns.Add(k4.ToString());
            }
            
            SqlCommand cmd01 = new SqlCommand();
            cmd01.CommandType = CommandType.Text;

            cmd01.Connection = con;
            cmd01.CommandText = "delete from percentageval";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cmd01.ExecuteNonQuery();

            con.Close();


            for (int k3 = 0; k3 < TestingProtocal.Rows.Count; k3++)
            {
                int k6 = 0;
                for (int k5 = 0; k5 <noofMonth*12; k5++) //here 12 replce with number oy years
                {
                    if (TestingProtocal.Rows[k3][7 + k5].ToString() == "0")
                    {
                        k6 = k6 + 1;
                    }
                }
                int no = 0;
                TestingProtocal.Rows[k3]["Total0"] = k6;//replace 24 and 25 with name
                TestingProtocal.Rows[k3]["TotalNo"] = noofMonth * 12 - k6;//here 12 replce with number oy years
                if (Convert.ToInt32(TestingProtocal.Rows[k3]["TotalNo"]) > 0)//replace 24 and 25 with name
                {
                    no = 12 / Convert.ToInt32(TestingProtocal.Rows[k3]["TotalNo"]);//here 12 replce with number oy years
                }
                TestingProtocal.Rows[k3]["TotalGap"] = no.ToString();//replace0 26 with name

                string sql1 = "",_inserValuesPV="" ;

                SqlCommand cmd1 = new SqlCommand();
                cmd1.CommandType = CommandType.Text;

                cmd1.Connection = con;

                //foreach (DataRow dr in TestingProtocal.Rows)
                //{
                    //if (TestingProtocal.Rows[k3][19].ToString() == "") TestingProtocal.Rows[k3][19] = 0;
                    //if (TestingProtocal.Rows[k3][20].ToString() == "") TestingProtocal.Rows[k3][20] = 0;
                    //if (TestingProtocal.Rows[k3][21].ToString() == "") TestingProtocal.Rows[k3][21] = 0;
                //}
                    _inserValuesPV += "'" + TestingProtocal.Rows[k3][0].ToString().Trim() + "','" + TestingProtocal.Rows[k3][1].ToString().Trim() + "','" + TestingProtocal.Rows[k3]["Percentage1"].ToString().Trim() + "','" + TestingProtocal.Rows[k3]["Percentageold"].ToString().Trim() + "','" + TestingProtocal.Rows[k3]["TotalTestPerYear"].ToString().Trim() + "' ,";
                    foreach (var item in mMGAbyType)
                    {
                        string vname = Regex.Replace(item.VariableName, @"(\s+|@|&|'|\(|\)|<|>|#)", "");
                        if (TestingProtocal.Rows[k3][item.VariableName].ToString() == "") TestingProtocal.Rows[k3][item.VariableName] = 0;
                        //if (TestingProtocal.Rows[k3][vname].ToString() == "") TestingProtocal.Rows[k3][vname] = 0;

                        _inserValuesPV += "" + TestingProtocal.Rows[k3][item.VariableName].ToString().Trim() + " ,";
                    }
                //cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld,TotalTestPerYear,"+_columnName.TrimEnd(',')+") values('"
                //          + TestingProtocal.Rows[k3][0].ToString().Trim() + "','"
                //          + TestingProtocal.Rows[k3][1].ToString().Trim() + "','" + TestingProtocal.Rows[k3][22].ToString().Trim() + "','"
                //          + TestingProtocal.Rows[k3][23].ToString().Trim() + "', '" + TestingProtocal.Rows[k3][19].ToString().Trim() + "','"
                //          + TestingProtocal.Rows[k3][20].ToString().Trim() + "',  '" + TestingProtocal.Rows[k3][21].ToString().Trim() + "')";//rename number with name 19 to 23 and Create column here according to MMProgram

                    if (_variableName!="") cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld,TotalTestPerYear," + _variableName.TrimEnd(',') + ") values(" + _inserValuesPV.TrimEnd(',') + ")";
                    else if (_variableName == "") cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld,TotalTestPerYear) values(" + _inserValuesPV.TrimEnd(',') + ")";
                // cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld," + _variableName.TrimEnd(',') + ") values(" + _inserValuesPV.TrimEnd(',') + ")";
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int ij = cmd1.ExecuteNonQuery();

                con.Close();

            }



            BaseLine.Columns.Add("Test"); BaseLine.Columns.Add("PGrp"); BaseLine.Columns.Add("No"); BaseLine.Columns.Add("Value");

            for (int k9 = 0; k9 < TestingProtocal.Rows.Count; k9++)
            {
                for (int k8 = 0; k8 <noofMonth * 12;  k8++)  //rename 12 with no of year
                {

                    if (Convert.ToInt32(TestingProtocal.Rows[k9][k8 + 7]) != 0)
                    {
                        BaseLine.Rows.Add();
                        int a1 = BaseLine.Rows.Count;
                        BaseLine.Rows[a1 - 1][0] = TestingProtocal.Rows[k9][0];
                        BaseLine.Rows[a1 - 1][1] = TestingProtocal.Rows[k9][1];

                        BaseLine.Rows[a1 - 1][2] = k8 + 1;
                        BaseLine.Rows[a1 - 1][3] = TestingProtocal.Rows[k9][k8 + 7].ToString();

                    }

                }
            }

            SqlCommand cmd0 = new SqlCommand();
            cmd0.CommandType = CommandType.Text;

            cmd0.Connection = con;
            cmd0.CommandText = "delete from Temptbl1";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cmd0.ExecuteNonQuery();

            con.Close();


            for (int i = 0; i < BaseLine.Rows.Count; i++)
            {
                string sql = "";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;

                cmd.Connection = con;
                cmd.CommandText = "insert into Temptbl1 (tst, PGrp, Num,Valu) values('"
                          + BaseLine.Rows[i][0].ToString().Trim() + "','"
                          + BaseLine.Rows[i][1].ToString().Trim() + "','" + BaseLine.Rows[i][2].ToString().Trim() + "','"
                          + BaseLine.Rows[i][3].ToString().Trim() + "')";
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int ij = cmd.ExecuteNonQuery();

                con.Close();
            }

            SqlCommand cm0 = new SqlCommand();
            cm0.CommandType = CommandType.Text;

            cm0.Connection = con;
            cm0.CommandText = "delete from testbymonth where forecastid='" + ID + "'";
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            cm0.ExecuteNonQuery();

            con.Close();

            //main loop insert data according to Adult and child
            for (int k3 = 0; k3 < TestingProtocal.Rows.Count; k3++)
            {
                TestChart.Clear(); TestByMonth.Clear(); margins.Clear(); Percentag.Clear();
                
                //SqlDataAdapter adapt11 = new SqlDataAdapter("Select baseline,Month1,Month2,Month3,Month4,Month5,Month6,Month7,Month8,Month9,Month10,Month11,Month12 from testingprotocol where testid='" + TestingProtocal.Rows[k3][0] + "' and patientgroupid='" + TestingProtocal.Rows[k3][1] + "' and forecastinfoid='" + ID + "'", con);
                SqlDataAdapter adapt11 = new SqlDataAdapter("Select baseline,"+_MonthName.TrimEnd(',')+" from testingprotocol where testid='" + TestingProtocal.Rows[k3][0] + "' and patientgroupid='" + TestingProtocal.Rows[k3][1] + "' and forecastinfoid='" + ID + "'", con);
                adapt11.Fill(yr);
                //Changes in dynamic month with Parameter

                SqlDataAdapter adapt12 = new SqlDataAdapter("Select num,valu from temptbl1 where tst='" + TestingProtocal.Rows[k3][0] + "' and pgrp='" + TestingProtocal.Rows[k3][1] + "'", con);
                adapt12.Fill(margins);

                //  int y = yrs * 12 / Convert.ToInt32(yr.Rows[0][0]);

                for (int k7 = 0; k7 <noofMonth*12; k7++) //no of year in 12  
                {
                    TestChart.Rows.Add();
                    TestChart.Rows[k7][1] = ID.ToString();
                    TestChart.Rows[k7][2] = TestingProtocal.Rows[k3][6].ToString();
                    TestChart.Rows[k7][3] = TestingProtocal.Rows[k3][0].ToString();
                    TestChart.Rows[k7][4] = TestingProtocal.Rows[k3][1].ToString();
                    TestChart.Rows[k7][5] = TestingProtocal.Rows[k3][3].ToString();
                    TestChart.Rows[k7][6] = TestingProtocal.Rows[k3][4].ToString();
                    foreach (var item in mMGAbyType)
                    {
                        TestChart.Rows[k7][item.VariableName] = TestingProtocal.Rows[k3][item.VariableName].ToString();
                    }
                    //TestChart.Rows[k7][6] = TestingProtocal.Rows[k3][4].ToString();
                    //TestChart.Rows[k7][7] = TestingProtocal.Rows[k3][5].ToString();

                    int columnIndex = TestChart.Columns.IndexOf("1");
                    for (int k07 = 0; k07 < yrs * 12; k07++)
                    {
                        if (k07 == 0 && k7 == 0)
                        {
                            TestChart.Rows[k7][columnIndex + k07] = Convert.ToInt32(yr.Rows[k3][0]) + Convert.ToInt32(yr.Rows[k3][1]);
                        }
                        else if (k07 != 0 && k7 == 0)
                        {
                            if (k07 < 12)
                            {
                                TestChart.Rows[k7][columnIndex + k07] = Convert.ToInt32(yr.Rows[k3][k07 + 1]);
                            }
                            else
                            {
                                int kj = k07 % 12;
                                TestChart.Rows[k7][columnIndex + k07] = Convert.ToInt32(yr.Rows[0][kj + 1]);
                            }
                        }
                        else
                        {
                            if (columnIndex + k07 + k7 < TestChart.Columns.Count)
                            {
                                TestChart.Rows[k7][columnIndex + k07 + k7] = TestChart.Rows[k7 - 1][columnIndex + k07 + k7 - 1];
                            }

                        }
                    }


                    int tot = 0;
                    for (int jj = 0; jj < TestChart.Columns.Count - columnIndex; jj++)
                    {
                        if (!string.IsNullOrEmpty(TestChart.Rows[k7][jj + columnIndex].ToString()))
                        {
                            tot = tot + Convert.ToInt32(TestChart.Rows[k7][jj + columnIndex]);
                        }

                    }
                    TestChart.Rows[k7][0] = tot;


                }
                int totalTest = 0; decimal aAmount = 0;//, per=0;
                for (int jj = 0; jj < TestChart.Rows.Count; jj++)
                {

                    totalTest = totalTest + Convert.ToInt32(TestChart.Rows[jj][0]);

                }
                string _query="";
//                SqlDataAdapter adapt13 = new SqlDataAdapter(@"Select testn,pgrpid,pernew,perold,TotalTestPerYear
//      ,TestRepeatPerYear,SymptomTestPerYear from percentageval where testn='" + TestChart.Rows[0][3] + "' and pgrpid='" + TestChart.Rows[0][4] + "'", con);
                if (_variableName != "") _query = "Select testn,pgrpid,pernew,perold,TotalTestPerYear ," + _variableName.TrimEnd(',') + "from percentageval where testn='" + TestChart.Rows[0][3] + "' and pgrpid='" + TestChart.Rows[0][4] + "'";
                else if (_variableName == "") _query = "Select testn,pgrpid,pernew,perold,TotalTestPerYear from percentageval where testn='" + TestChart.Rows[0][3] + "' and pgrpid='" + TestChart.Rows[0][4] + "'";
                SqlDataAdapter adapt13 = new SqlDataAdapter(_query, con);
                adapt13.Fill(Percentag);
                //Changes parameter according to MMProgram

                decimal totalcal = (totalTest * Convert.ToDecimal(Percentag.Rows[0][2])) + (Convert.ToDecimal(Percentag.Rows[0][3]) * (yrs * Convert.ToDecimal(Percentag.Rows[0][4])));
                //Decimal per = Convert.ToDecimal(Percentag.Rows[0][5]) + Convert.ToDecimal(Percentag.Rows[0][6]);
                Decimal per = 0;
                for (int ki = 0; ki < Percentag.Rows.Count; ki++)
                {
                    foreach (DataColumn dc in Percentag.Columns)
                    {
                        foreach (DataRow dr in dtGetValue.Rows)
                        {
                            string v1 = dr["VariableName"].ToString();
                            string v2 = dc.ColumnName.ToString();
                            if (v1.Equals(v2))
                            {
                                //bool variableEffect = gAssumption.Where(x => x.VariableName == item.ToString()).SingleOrDefault().VariableEffect;
                                //int variableDatatype = _mMGeneralAssumption.Where(x => x.VariableName == item.ToString()).SingleOrDefault().VariableDataType;
                                //if(variableDatatype==2)aAmount=aAmount+(newpatient*(Convert.ToDecimal(dtValue.Rows[ki][""+item.ToString()+""]))/100);
                                //else if (variableDatatype == 2) aAmount = aAmount + (newpatient * (Convert.ToDecimal(dtValue.Rows[ki]["" + item.ToString() + ""])) / 100);
                                decimal vvalue = Convert.ToDecimal(Percentag.Rows[ki]["" + v2 + ""]);
                                aAmount = ((vvalue) / 100);
                                if (dr["variableEffect"].ToString() == "True") per = per + aAmount;
                                if (dr["variableEffect"].ToString() == "False") per = per - aAmount;

                            }

                        }
                    }

                }

               
                //check parameter should add or subtract

                decimal finalCal = totalcal + (totalcal * per);  //correct

                string sql = "";

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;

                cmd.Connection = con;
                cmd.CommandText = "insert into forecastedtestbytest (ForeCastID, Tst, PGrp,TotalTst) values('"
                          + TestChart.Rows[0][1].ToString().Trim() + "','"
                          + TestChart.Rows[0][3].ToString().Trim() + "','" + TestChart.Rows[0][4].ToString().Trim() + "', '" + finalCal + "')";
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int ij = cmd.ExecuteNonQuery();

                con.Close();
                int columnIndex1 = TestChart.Columns.IndexOf("1");

                // for (int i = 0; i < TestChart.Columns.Count - 8; i++)
                //{
                //    int tot = 0; int j = 0;
                //    TestByMonth.Rows.Add();
                //    TestByMonth.Rows[i][0] = TestChart.Rows[0][1];
                //    TestByMonth.Rows[i][1] = TestChart.Rows[0][3];
                //    for (j = 0; j < 12; j++)
                //    {
                //        if (!string.IsNullOrEmpty(TestChart.Rows[j][i + 8].ToString()))
                //        {
                //            tot = tot + Convert.ToInt32(TestChart.Rows[j][i + 8]);
                //        }
                //        TestByMonth.Rows[i][7] = TestChart.Rows[j][5];
                //        foreach (var item in mMGAbyType)
                //        {
                //            TestByMonth.Rows[i][item.VariableName] = TestChart.Rows[j][item.VariableName];
                //        }
                //        //TestByMonth.Rows[i][8] = TestChart.Rows[j][6];
                //        //TestByMonth.Rows[i][9] = TestChart.Rows[j][7];
                //    }

                for (int i = 0; i < TestChart.Columns.Count - columnIndex1; i++)
                {
                    int tot = 0; int j = 0;
                    TestByMonth.Rows.Add();
                    TestByMonth.Rows[i][0] = TestChart.Rows[0][1];
                    TestByMonth.Rows[i][1] = TestChart.Rows[0][3];
                    for (j = 0; j < 12; j++)
                    {
                        if (!string.IsNullOrEmpty(TestChart.Rows[j][i + columnIndex1].ToString()))
                        {
                            tot = tot + Convert.ToInt32(TestChart.Rows[j][i + columnIndex1]);
                        }
                        TestByMonth.Rows[i][7] = TestChart.Rows[j][5];
                        foreach (var item in mMGAbyType)
                        {
                            TestByMonth.Rows[i][item.VariableName] = TestChart.Rows[j][item.VariableName];
                        }
                        //TestByMonth.Rows[i][8] = TestChart.Rows[j][6];
                        //TestByMonth.Rows[i][9] = TestChart.Rows[j][7];
                    }


                    string mnth = "";
                    if ((i + 1) % 12 == 1)
                    {
                        mnth = "Jan";
                    }
                    if ((i + 1) % 12 == 2)
                    {
                        mnth = "Feb";
                    }
                    if ((i + 1) % 12 == 3)
                    {
                        mnth = "Mar";
                    }
                    if ((i + 1) % 12 == 4)
                    {
                        mnth = "Apr";
                    }
                    if ((i + 1) % 12 == 5)
                    {
                        mnth = "May";
                    }
                    if ((i + 1) % 12 == 6)
                    {
                        mnth = "Jun";
                    }
                    if ((i + 1) % 12 == 7)
                    {
                        mnth = "Jul";
                    }
                    if ((i + 1) % 12 == 8)
                    {
                        mnth = "Aug";
                    }
                    if ((i + 1) % 12 == 9)
                    {
                        mnth = "Sep";
                    }
                    if ((i + 1) % 12 == 10)
                    {
                        mnth = "Oct";
                    }
                    if ((i + 1) % 12 == 11)
                    {
                        mnth = "Nov";
                    }
                    if ((i + 1) % 12 == 0)
                    {
                        mnth = "Dec";
                    }
                    string m = months.Rows[0][0].ToString();

                    string ys = (m.Length > 3) ? m.Substring(m.Length - 4, 4) : m;
                    string years = "";
                    if (i + 1 <= 12)
                    {
                        years = ys;
                    }

                    if (i + 1 > 12 && i + 1 <= 24)
                    {
                        years = (Convert.ToInt32(ys) + 1).ToString();
                    }
                    if (i + 1 > 24 && i + 1 <= 36)
                    {
                        years = (Convert.ToInt32(ys) + 2).ToString();
                    }
                    if (i + 1 > 36 && i + 1 <= 48)
                    {
                        years = (Convert.ToInt32(ys) + 3).ToString();
                    }
                    if (i + 1 > 48 && i + 1 <= 60)
                    {
                        years = (Convert.ToInt32(ys) + 4).ToString();
                    }

                  
                    TestByMonth.Rows[i][2] = string.Concat(mnth, " ", years);
                    TestByMonth.Rows[i][3] = tot.ToString();
                    TestByMonth.Rows[i][4] = TestChart.Rows[0][4];
                    TestByMonth.Rows[i][5] = (Convert.ToInt32(i) + 1).ToString();

                }

                for (int ij1 = 0; ij1 < TestByMonth.Rows.Count; ij1++)
                {
                    string s = "";


                    SqlCommand cm = new SqlCommand();
                    cm.CommandType = CommandType.Text;

                    cm.Connection = con;

                  
                    //cm.CommandText = "insert into Testbymonth (ForeCastID, TestID, Month,TstNo,PGrp,SNo,NewPatient,TotalTestPerYear,TestRepeatPerYear,SymptomTestPerYear,ExistingPatient,Duration) values('"
                    //          + TestByMonth.Rows[ij1][0].ToString().Trim() + "','"
                    //          + TestByMonth.Rows[ij1][1].ToString().Trim() + "','" + TestByMonth.Rows[ij1][2].ToString().Trim() + "', '" + TestByMonth.Rows[ij1][3].ToString().Trim() + "', '" + TestByMonth.Rows[ij1][4].ToString().Trim() + "','" + TestByMonth.Rows[ij1][5].ToString() + "'," + TestingProtocal.Rows[k3]["Percentage1"].ToString() + "," + TestByMonth.Rows[ij1][7].ToString() + "," + TestByMonth.Rows[ij1][8].ToString() + "," + TestByMonth.Rows[ij1][9].ToString() + "," + TestingProtocal.Rows[k3]["Percentageold"].ToString() + "," + yrs + ")";


                    string _inserValuesTBM = "";

                    _inserValuesTBM += "'" + TestByMonth.Rows[ij1][0].ToString().Trim() + "','" + TestByMonth.Rows[ij1][1].ToString().Trim() + "','" + TestByMonth.Rows[ij1][2].ToString().Trim() + "','" + TestByMonth.Rows[ij1][3].ToString().Trim() + "','" + TestByMonth.Rows[ij1][4].ToString().Trim() + "','" + TestByMonth.Rows[ij1][5].ToString() + "'," + TestingProtocal.Rows[k3]["Percentage1"].ToString() + "," + TestByMonth.Rows[ij1][7].ToString() + " ,";
                    foreach (var item in mMGAbyType)
                    {
                        _inserValuesTBM += "" + TestingProtocal.Rows[k3][item.VariableName].ToString().Trim() + " ,";
                    }
                    _inserValuesTBM += "" + TestingProtocal.Rows[k3]["Percentageold"].ToString().Trim() + "," + yrs + "";

                    string _query1 = "";
                    if (_variableName != "") _query1 = "insert into Testbymonth (ForeCastID, TestID, Month,TstNo,PGrp,SNo,NewPatient,TotalTestPerYear," + _variableName.TrimEnd(',') + ",ExistingPatient,Duration) values(" + _inserValuesTBM.TrimEnd(',') + ")";
                    else if (_variableName == "") _query1 = "insert into Testbymonth (ForeCastID, TestID, Month,TstNo,PGrp,SNo,NewPatient,TotalTestPerYear,ExistingPatient,Duration) values(" + _inserValuesTBM.TrimEnd(',') + ")";
                    //cm.CommandText = "insert into Testbymonth (ForeCastID, TestID, Month,TstNo,PGrp,SNo,NewPatient,TotalTestPerYear," + _variableName.TrimEnd(',') + ",ExistingPatient,Duration) values(" + _inserValuesTBM.TrimEnd(',') + ")";
                    cm.CommandText = _query1;
                    
                    //here we should add name TestPerYear in place of TestByMonth.Rows[ij1][7],RepeatTest in place of TestByMonth.Rows[ij1][8] and Symptomtest in place of TestByMonth.Rows[ij1][9]
                    //also create column according to Program in Testbymonth table
                    
                   
                   

                    //cmd1.CommandText = "insert into PercentageVal (TestN, PGrpID, PerNew,PerOld,TotalTestPerYear," + _columnName.TrimEnd(',') + ") values(" + _inserValuesPV + ")";

                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    int jk = cm.ExecuteNonQuery();

                    con.Close();
                }


            }

            string query="SELECT test.TestName,round(sum(ForecastedTestByTest.TotalTst),0) as tst FROM test,ForecastedTestByTest WHERE test.TestID=ForecastedTestByTest.Tst and  ForeCastID='" + ID + "' group by test.TestName";
            SqlDataAdapter adapt14 = new SqlDataAdapter(@"SELECT test.TestName,round(sum(ForecastedTestByTest.TotalTst),0) as tst
FROM test,ForecastedTestByTest WHERE test.TestID=ForecastedTestByTest.Tst and  ForeCastID='" + ID + "' group by test.TestName", con);
            adapt14.Fill(TestNoChart);

            

            foreach (DataRow dr in TestNoChart.Rows)
            {


                Chart3.Series["Forecasted Test"].Points.AddXY(dr["TestName"].ToString(), dr["tst"].ToString());
                //  chart2.Series["Coverage"].LegendText = "Instrument Distribution";
            }
            Chart3.Series["Forecasted Test"].IsValueShownAsLabel = true;
            Chart3.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            Chart3.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;

            //Chart3.DataSource = TestNoChart;

            ////set the member of the chart data source used to data bind to the X-values of the series  
            //Chart3.Series["Forecasted Test"].XValueMember = "TestName";
            ////set the member columns of the chart data source used to data bind to the X-values of the series  
            //Chart3.Series["Forecasted Test"].YValueMembers = "tst";
            //// chart2.Series["Forecasted Test Ratio"].ChartType = SeriesChartType.Pie;
            //Chart3.Series["Forecasted Test"].IsValueShownAsLabel = true;
            //Chart3.Series["Forecasted Test"]["PixelPointWidth"] = "25";
            //Chart3.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            //Chart3.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            //chart2.Titles.Add("Ratio of forcasted test by testing area");
            con.Close();

        }


//        private void fillchartByArea()
//        {
//            if (con.State == ConnectionState.Closed)
//            {
//                con.Open();
//            }
//            DataTable TestArea = new System.Data.DataTable();
//            SqlDataAdapter adapt14 = new SqlDataAdapter(@"select t1.AreaName as AreaName,round(sum(t3.TotalTst),0) as TotalTst from TestingArea t1 inner join Test t2 on t1.TestingAreaID=t2.TestingAreaID
//inner join ForecastedTestByTest t3 on t3.Tst=t2.TestID group by t1.AreaName", con);
//            adapt14.Fill(TestArea);

//            DataTable dtMarks1 = TestArea.Clone();
//            dtMarks1.Columns["TotalTst"].DataType = Type.GetType("System.Int32");

//            foreach (DataRow dr in TestArea.Rows)
//            {
//                dtMarks1.ImportRow(dr);
//            }
//            dtMarks1.AcceptChanges();


//            DataView dv = dtMarks1.DefaultView;
//            dv.Sort = "TotalTst Asc";

//            DataTable finalRatio = TestArea.Clone();

//            decimal no = Convert.ToDecimal(dv[0]["TotalTst"]);
//            // finalRatio.Columns.Add("ratio");
//            if (TestArea.Rows.Count != 1)
//            {
//                for (int l2 = 0; l2 < TestArea.Rows.Count; l2++)
//                {
//                    finalRatio.Rows.Add();
//                    finalRatio.Rows[l2][0] = TestArea.Rows[l2][0];
//                    finalRatio.Rows[l2][1] = Math.Round((Convert.ToDecimal(TestArea.Rows[l2][1]) / no), 1);

//                }
//                chart2.DataSource = finalRatio;
//            }
//            else
//            {
//                chart2.DataSource = TestArea;

//            }






//            //set the member of the chart data source used to data bind to the X-values of the series  
//            chart2.Series["Forecasted Test Ratio"].XValueMember = "AreaName";
//            //set the member columns of the chart data source used to data bind to the X-values of the series  
//            chart2.Series["Forecasted Test Ratio"].YValueMembers = "TotalTst";
//            chart2.Series["Forecasted Test Ratio"].ChartType = SeriesChartType.Pie;
//            chart2.Series["Forecasted Test Ratio"].IsValueShownAsLabel = true;
//            chart2.Series["Forecasted Test Ratio"]["PixelPointWidth"] = "25";
//            //chart2.Titles.Add("Ratio of forcasted test by testing area");
//            con.Close();

//        }
        private void fillchartByArea()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            DataTable TestArea = new System.Data.DataTable();
            SqlDataAdapter adapt14 = new SqlDataAdapter(@"select t1.AreaName as AreaName,round(sum(t3.TotalTst),0) as TotalTst from TestingArea t1 inner join Test t2 on t1.TestingAreaID=t2.TestingAreaID
inner join ForecastedTestByTest t3 on t3.Tst=t2.TestID and forecastid='" + ID + "' group by t1.AreaName", con);
            adapt14.Fill(TestArea);

            TestArea.Columns.Add("Percentage");

            //DataTable dtMarks1 = TestArea.Clone();
            //dtMarks1.Columns["TotalTst"].DataType = Type.GetType("System.Int32");

            //foreach (DataRow dr in TestArea.Rows)
            //{
            //    dtMarks1.ImportRow(dr);
            //}
            //dtMarks1.AcceptChanges();


            //DataView dv = dtMarks1.DefaultView;
            //dv.Sort = "TotalTst Asc";

            //DataTable finalRatio = TestArea.Clone();

            //decimal no = Convert.ToDecimal(dv[0]["TotalTst"]);
            // finalRatio.Columns.Add("ratio");
            if (TestArea.Rows.Count >= 1)
            {
                int tot = 0;
                for (int l2 = 0; l2 < TestArea.Rows.Count; l2++)
                {
                    if (!string.IsNullOrEmpty(TestArea.Rows[l2][1].ToString()))
                    {
                        tot = tot + Convert.ToInt32(TestArea.Rows[l2][1]);
                    }

                }
                for (int l2 = 0; l2 < TestArea.Rows.Count; l2++)
                {
                    if (tot != 0)
                    {
                        TestArea.Rows[l2][2] = Math.Round((Convert.ToDecimal(TestArea.Rows[l2][1]) * 100 / tot), 1);
                    }
                }
               // chart26.DataSource = TestArea;
            }

            foreach (DataRow dr in TestArea.Rows)
            {


                chart2.Series["Forecasted Test Ratio"].Points.AddXY(dr["AreaName"].ToString(), dr["Percentage"].ToString());
                //  chart2.Series["Coverage"].LegendText = "Instrument Distribution";
            }
            chart2.Series["Forecasted Test Ratio"].IsValueShownAsLabel = true;
            chart2.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            chart2.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            //set the member of the chart data source used to data bind to the X-values of the series  
            ////chart26.Series["Forecasted Test Ratio"].XValueMember = "AreaName";
            //////set the member columns of the chart data source used to data bind to the X-values of the series  
            ////chart26.Series["Forecasted Test Ratio"].YValueMembers = "Percentage";
            ////chart26.Series["Forecasted Test Ratio"].ChartType = SeriesChartType.Pie;
            ////chart26.Series["Forecasted Test Ratio"].IsValueShownAsLabel = true;
            ////chart26.Series["Forecasted Test Ratio"]["PixelPointWidth"] = "25";
            //////chart2.Titles.Add("Ratio of forcasted test by testing area");

            con.Close();

        }

        //private void fillChartByProductPrice()
        //{
        //    Chart1.Width = 2500;
        //    DataTable DTtestno = new DataTable();
        //    DataTable DTsiteinstrument = new DataTable();
        //    DataTable DTproductusagerate = new DataTable();
        //    DataTable DTproductControlusage = new DataTable();
        //    DataTable DTproductConsumbleusage = new DataTable();
            
        //       DataTable DTcostproduct = new DataTable();
        //    SqlCommand cmd0 = new SqlCommand();
        //    cmd0.CommandType = CommandType.Text;

        //    cmd0.Connection = con;
        //    cmd0.CommandText = "delete from Tempproductneed where [ForcaseID] =" + ID + " ";
        //    if (con.State == ConnectionState.Closed)
        //    {
        //        con.Open();
        //    }
        //    cmd0.ExecuteNonQuery();

        //    con.Close();

        //    string _sql = "";
        //    _sql = string.Format(@"Select Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]='{0}'", ID);
        //    //using (connection = new SqlConnection(con))
        //    //{

        //    SqlCommand cmd8 = new SqlCommand();
        //    cmd8.CommandType = CommandType.Text;

        //    cmd8.Connection = con;
        //    cmd8.CommandText = _sql;
        //    if (con.State == ConnectionState.Closed)
        //    {
        //        con.Open();
        //    }
        //   forecasttype = Convert.ToString(cmd8.ExecuteScalar());

        //    con.Close();
           
        //    //  }
        //    string str8 = "";
        //    string strsites = "";
        //    if (forecasttype == "S")
        //    {
        //        str8 = "select Avg(s.WorkingDays) as WorkingDays from ForecastSiteInfo FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastinfoID=" + ID + "";
        //        strsites = "select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + " ";
        //    }
        //    else
        //    {
        //        str8 = " SELECT Avg(s.WorkingDays) as WorkingDays  FROM [ForecastCategorySiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=" + ID + "";
        //        strsites = "select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + " ";
        //    }
        //    DataTable Dtsiteworkingdays = new DataTable();
        //    con.Open();
        //    SqlDataAdapter adapt = new SqlDataAdapter(str8, con);
        //    adapt.Fill(Dtsiteworkingdays);

        //    con.Close();



        //    con.Open();
        //    string str2="";

        //    str2 ="SELECT PU.InstrumentId As InstrumentId,ins.InstrumentName As InstrumentName,  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.Rate As Rate,PU.TestId As TestId,ins.TestingAreaID As AreaID,TA.AreaName as areaname ";
        //    str2 += " ,Isnull(T.Quantity,0) As Quantity ,Isnull(T.Percentage,0) As Percentage,TN.testnumber,Cast(((PU.Rate*TN.testnumber)*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ";
        //    str2 += "  FROM [ProductUsage] PU   Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId  Left join MasterProduct mp on mp.ProductID=PU.ProductId  Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID ";
        //    str2 += " Left join  (select Max(T.testingArea) As testingArea,Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID from(  SELECT SI.SiteID,TA.AreaName As testingArea ,Ins.InstrumentName As instrumentname,SI.Quantity As Quantity,SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS ";
        //    str2 += " Left join SiteInstrument SI on SI.SiteID=FS.SiteID   Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID  where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID ";
        //    str2 += " Left Join (SELECT Max([ForeCastID]) as forecastID   ,[Tst] as TestID, Max(TA.AreaName) As Areaname,Max(TA.TestingAreaID) As TestingAreaID,  cast(round (Sum([TotalTst]),0) as int) as testnumber   FROM [ForecastedTestByTest]   Left join Test TS on [ForecastedTestByTest].tst=TS.TestID     Left Join TestingArea TA on TA.TestingAreaID=TS.TestingAreaID    where forecastID=" + ID + "   group by Tst ";
            
            
        //    str2 += " ) As TN on TN.TestID=PU.TestId   where PU.TestId in (Select Distinct TestID from [TestingProtocol] where ForecastinfoID =" + ID + ")  and PU.IsForControl=0";
      
        //     SqlDataAdapter adapt1 = new SqlDataAdapter(str2,con);
        //    adapt1.Fill(DTproductusagerate);
        //    for (int i = 0; i < DTproductusagerate.Rows.Count; i++)
        //    {

        //        SqlCommand cmd1 = new SqlCommand();
        //        cmd1.CommandType = CommandType.Text;

        //        cmd1.Connection = con;
        //        if (!string.IsNullOrEmpty(DTproductusagerate.Rows[i]["productneed"].ToString()))
        //        {
        //            cmd1.CommandText = " INSERT INTO [dbo].[Tempproductneed]([ProductID] ,[ProductName],[Productneed],[ForcaseID])  VALUES ('"
        //                      + DTproductusagerate.Rows[i]["ProductId"].ToString().Trim() + "','"
        //                      + DTproductusagerate.Rows[i]["ProductName"].ToString().Trim() + "','" + DTproductusagerate.Rows[i]["Productneed"].ToString().Trim() + "', '" + ID + "')";


        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            int ij = cmd1.ExecuteNonQuery();

        //            con.Close();
        //        }

        //    }
        //    /////Product control usasge
        //    string str = "";


        //            str ="select Ins.InstrumentName,mp.ProductName,mp.productID,PU.Rate,PT.TypeID ,PT.TypeName,(Case when Ins.DailyCtrlTest>0 then DailyCtrlTest when INS.MaxTestBeforeCtrlTest>0  then MaxTestBeforeCtrlTest";
        //            str+=" when INS.WeeklyCtrlTest>0 then WeeklyCtrlTest when INS.MonthlyCtrlTest>0 then MonthlyCtrlTest when Ins.QuarterlyCtrlTest>0 then QuarterlyCtrlTest else 0 end) as controlrequired,(Case when Ins.DailyCtrlTest>0 then 'Daily' when INS.MaxTestBeforeCtrlTest>0  then 'Maxtest'";
        //            str += " when INS.WeeklyCtrlTest>0 then 'Weekly' when INS.MonthlyCtrlTest>0 then 'Monthly' when Ins.QuarterlyCtrlTest>0 then 'Quaterly' else '' end) as Frequency,FI.TestID," + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + ",(Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*Datediff(mm,Fc.StartDate,Fc.ForecastDate)*pu.Rate*INS.DailyCtrlTest";
        //            str+=" when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  Datediff(mm,Fc.StartDate,Fc.ForecastDate)*4*pu.Rate*INS.WeeklyCtrlTest  when INS.MonthlyCtrlTest>0 then Datediff(mm,Fc.StartDate,Fc.ForecastDate)*pu.Rate*INS.MonthlyCtrlTest";
        //            str+=" when Ins.QuarterlyCtrlTest>0 then (Datediff(mm,Fc.StartDate,Fc.ForecastDate)/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as productneed,Datediff(mm,Fc.StartDate,Fc.ForecastDate) As month1";
        //            str+=" from ProductUsage PU Left join (Select  Distinct testID,ForecastinfoID from TestingProtocol where ForecastinfoID=" + ID + ") FI on PU.TestId=FI.TestID lEFT JOIN ForecastInfo fc ON FC.ForecastID=FI.ForecastinfoID";
        //            str+=" lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";
        //             str+=" Left join ProductType PT on PT.TypeID=mp.ProductTypeId";
        //            str+=" where FI.ForecastinfoID=" + ID + " and PU.IsForControl=1";
        //    SqlDataAdapter adapt2 = new SqlDataAdapter(str,con);
        //    adapt2.Fill(DTproductControlusage);
            
        //    for (int i = 0; i < DTproductControlusage.Rows.Count; i++)
        //    {

        //        SqlCommand cmd1 = new SqlCommand();
        //        cmd1.CommandType = CommandType.Text;

        //        cmd1.Connection = con;
        //        if (!string.IsNullOrEmpty(DTproductControlusage.Rows[i]["productneed"].ToString()))
        //        {
        //            cmd1.CommandText = " INSERT INTO [dbo].[Tempproductneed]([ProductID] ,[ProductName],[Productneed],[ForcaseID])  VALUES ('"
        //                      + DTproductControlusage.Rows[i]["productID"].ToString().Trim() + "','"
        //                      + DTproductControlusage.Rows[i]["ProductName"].ToString().Trim() + "','" + DTproductControlusage.Rows[i]["productneed"].ToString().Trim() + "', '" + ID + "')";

        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            int ij = cmd1.ExecuteNonQuery();

        //            con.Close();
        //        }
               
        //    }

        //    string str1 = "";



        //    str1 = " select MP.ProductName,Mp.ProductID,(case when PerTest>0 then NoOfTest else 0 end) as Pertest,isnull(CU.Period,'') as Perperiod ,isnull(Ins.InstrumentName,'') as PerInstrument ,";
        //    str1 += " CU.UsageRate as Rate,Ins.InstrumentID,T.TestId,T.totaltest,Isnull((select  Isnull(avg(Quantity),0)  from SiteInstrument where siteid in (select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + ")";
        //    str1 += " and InstrumentID =Ins.InstrumentID group by InstrumentID),0) as totalquantity,(case when Pertest>0 and T.totaltest >0 then (T.totaltest/PerTest)*CU.UsageRate when CU.Period='Daily' then DATEDIFF(mm,StartDate,ForecastDate)*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate";
        //    str1 += " when CU.Period='Weekly' then DATEDIFF(mm,StartDate,ForecastDate)*4*Isnull((select  Isnull(avg(Quantity),0)  from SiteInstrument where siteid  in (" + strsites + ")";
        //    str1 += " and InstrumentID =Ins.InstrumentID group by InstrumentID),0) else 0 end ) as productneed from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId ";
        //    str1 += "Left join (SELECT Max([ForeCastID]) as forecastID   ,[Tst] as TestID       ,cast(round (Sum([TotalTst]),0) as int) as totaltest   FROM [ForecastedTestByTest] where forecastID=" + ID + " ";
        //    str1 += " group by Tst) as T on T.TestID=mc.TestId Left join MasterProduct MP on mp.ProductID=CU.ProductId Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId Left join (select Avg(s.WorkingDays) as WorkingDays ,FSI.ForecastinfoID as ForecastinfoID from ForecastSiteInfo FSI ";
        //    str1 += " Left join site s on s.SiteID =FSI.SiteID ";
        //    str1 += "	group by ForecastinfoID ";
        //    str1 += "  ) as  TN on TN.ForecastinfoID= T.forecastID ";
        //    str1 += " Left join ForecastInfo FR on fr.ForecastID=TN.ForecastinfoID where  Isnull(T.TestId,'')<>'' ";

        //    SqlDataAdapter adapt3 = new SqlDataAdapter(str1, con);
        //    adapt3.Fill(DTproductConsumbleusage);


        //    for (int i = 0; i < DTproductConsumbleusage.Rows.Count; i++)
        //    {

        //        SqlCommand cmd1 = new SqlCommand();
        //        cmd1.CommandType = CommandType.Text;

        //        cmd1.Connection = con;
        //        if (!string.IsNullOrEmpty(DTproductConsumbleusage.Rows[i]["productneed"].ToString()))
        //        {
        //            cmd1.CommandText = " INSERT INTO [dbo].[Tempproductneed]([ProductID] ,[ProductName],[Productneed],[ForcaseID])  VALUES ('"
        //                      + DTproductConsumbleusage.Rows[i]["ProductID"].ToString().Trim() + "','"
        //                      + DTproductConsumbleusage.Rows[i]["ProductName"].ToString().Trim() + "','" + DTproductConsumbleusage.Rows[i]["productneed"].ToString() + "', '" + ID + "')";


        //            if (con.State == ConnectionState.Closed)
        //            {
        //                con.Open();
        //            }
        //            int ij = cmd1.ExecuteNonQuery();

        //            con.Close();
        //        }
               
        //    }



        //    string str4 = "";


        //                str4 ="Select Max(TypeName) As ProductTypename,Cast (Round(Sum(cost),2) as numeric(18,2)) As cost From(select Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName,Sum(Productneed) As productneed,Max(Mp.ProductName) As ProductName,";
        //     str4 +=" (Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end) as adjustpacksize, ((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)+((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)*(Max(TA.ProgramGrowRate)/100))) as Programgrowthrate,";
        //     str4 +=" (((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)+( (Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)*(Max(TA.ProgramGrowRate)/100)))+(((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)+((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)*(Max(TA.ProgramGrowRate)/100)))*(Max(TA.WastageRate)/100))) as Adjustedforwastagerate ,";
        //     str4 +=" (((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)+((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)*(Max(TA.ProgramGrowRate)/100)))+(((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)+((Case when Max(PackSize)>0 then Sum(Productneed)/Max(PackSize) else 0 end)*(Max(TA.ProgramGrowRate)/100)))*(Max(TA.WastageRate)/100))) *Max(Price) as cost from Tempproductneed TP";
        //     str4 += " Left join ProductPrice PP on PP.ProductId=TP.ProductID Left join TestingAssumption TA on TA.ForecastinfoID=TP.ForcaseID Left join MasterProduct mp on mp.ProductID=TP.ProductID Left join ProductType PT on PT.TypeID=mp.ProductTypeId where TP.ForcaseID=" + ID + "   Group by TP.ProductID) as T where T.cost>0 group  by T.TypeID";


        //     SqlDataAdapter adapt4 = new SqlDataAdapter(str4, con);
        //     adapt4.Fill(DTcostproduct);
        //    chart4.DataSource = DTcostproduct;
        //    //set the member of the chart data source used to data bind to the X-values of the series  
        //    chart4.Series["Product Cost"].XValueMember = "ProductTypename";
        //    //set the member columns of the chart data source used to data bind to the X-values of the series  
        //    chart4.Series["Product Cost"].YValueMembers = "cost";
        //    // chart2.Titles.Add("Ratio of forcasted test by testing area");
        //    // chart2.Series["Product Cost"].ChartType = SeriesChartType.Pie;
        //    chart4.Series["Product Cost"].IsValueShownAsLabel = true;
        //    //chart2.ChartAreas[0].AxisX.Interval = 1;
        //    chart4.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
        //    chart4.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
        //    con.Close();

        //}

        private void calculation(double testnumber, int testid, string Month, int j, int period)
        {
            string str8 = "";
            string strsites = "",_sqlStr="";
            string _sql = "";
            _sql = string.Format(@"Select Isnull([ForecastType],'') as forecasttype ,ForecastNo As Title  ,Right(Replace(convert(varchar(11), StartDate, 106),' ','-'),8) + '    '+ Right(Replace(convert(varchar(11), ForecastDate, 106),' ','-'),8) As forecastperiod from ForecastInfo Where [ForecastID]='{0}'", ID);
            //using (connection = new SqlConnection(con))
            //{
            DataTable DTProductneed = new DataTable();

            //SqlCommand cmd8 = new SqlCommand();
            //cmd8.CommandType = CommandType.Text;

            //cmd8.Connection = con;
            //cmd8.CommandText = _sql;
            //if (con.State == ConnectionState.Closed)
            //{
            //    con.Open();
            //}
            //forecasttype = Convert.ToString(cmd8.ExecuteScalar());

            //con.Close();
            DataTable dtPParameterValue = new DataTable();
            DataTable dtGetValue = new DataTable();
            decimal _parameterSum = 0, _tparameterSum=0;
            decimal _PparameterSum = 0, _PtparameterSum=0;
            dtGetValue.Columns.Add("VariableName", typeof(string));
            dtGetValue.Columns.Add("VariableEffect", typeof(string));
            dtGetValue.Columns.Add("VariableDataType", typeof(string));

            _mPMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Product_Assumption"), _mMforecastinfo.ProgramId, _type);

            sqlretrieve = "";
            

            foreach (MMGeneralAssumption gAssumption in _mPMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 2 && gAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtGetValue.NewRow(); 
                    sqlretrieve += "[" + gAssumption.VariableName + "],";
                    dr["VariableName"] = gAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
                    dtGetValue.Rows.Add(dr);
                }
                dtGetValue.AcceptChanges();
            }
            sqlretrieve = sqlretrieve.TrimEnd(',');
            string strSql = "select " + sqlretrieve + " from TestingAssumption  where ForecastinfoID=" + ID + "";
            SqlDataAdapter adapt2 = new SqlDataAdapter(strSql, con);
            adapt2.Fill(dtPParameterValue);

            for (int ki = 0; ki < dtPParameterValue.Rows.Count; ki++)
            {
                foreach (DataColumn dc in dtPParameterValue.Columns)
                {
                    foreach (DataRow dr in dtPGetValue.Rows)
                    {
                        string v1 = dr["VariableName"].ToString();
                        string v2 = dc.ColumnName.ToString();
                        if (v1.Equals(v2))
                        {
                            _PparameterSum = ((Convert.ToDecimal(dtPParameterValue.Rows[ki]["" + v2 + ""])) / 100);
                            if (dr["variableEffect"].ToString() == "True") _PtparameterSum = _PtparameterSum + _PparameterSum;
                            if (dr["variableEffect"].ToString() == "False") _PtparameterSum = _PtparameterSum - _PparameterSum;

                        }

                    }
                }

            }
            DataTable Dtforecastinfo = new DataTable();
            con.Open();
            SqlDataAdapter adaptinfo = new SqlDataAdapter(_sql, con);
            adaptinfo.Fill(Dtforecastinfo);

            con.Close();
            if (Dtforecastinfo.Rows[0]["forecasttype"].ToString() == "S")
            {
                str8 = "select Avg(s.WorkingDays) as WorkingDays from ForecastSiteInfo FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastinfoID=" + ID + "";
                strsites = "select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + " ";
            }
            else
            {
                str8 = " SELECT Avg(s.WorkingDays) as WorkingDays  FROM [ForecastCategorySiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=" + ID + "";
                strsites = "select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + " ";
            }
            DataTable Dtsiteworkingdays = new DataTable();
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(str8, con);
            adapt.Fill(Dtsiteworkingdays);

            con.Close();

            con.Close();
            string str1 = "";
            int _period = (period * 12) / 3;

            //}




            //string str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
            str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
            str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
            str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate ";

            //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
            //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
            //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
            str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
            str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
            str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
            str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
            str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS ";
            str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
            str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
            str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
            /////Control useage

            str1 += "	Union ";


            str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
            str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
            str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
            str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

            str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
            str1 += "	Union ";


            str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
            str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
            str1 += " when CU.Period='Yearly' then " + period + "*CU.UsageRate";
            str1 += " when CU.Period='Quarterly' then " + _period + "*CU.UsageRate";
            str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)  ";
            str1 += " from SiteInstrument where siteid  in (select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
            str1 += "   group by InstrumentID),0) when CU.Period='Monthly' then (CU.UsageRate/12) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

            str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
            str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
            str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
            str1 += "   group by T.month1,T.ProductId ";
            con.Open();
            SqlDataAdapter adapt1 = new SqlDataAdapter(str1, con);
            adapt1.Fill(DTProductneed);

            //DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
            //for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
            //{
            //    DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

            //}
            //DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

            //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            //{
            //    DTProductneed.Rows[i2]["totalquantityinpack"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(_PtparameterSum)))));
            //}

            //DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));
            //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            //{
            //    DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(0)));
            //}

            DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
            for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
            {
                DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

            }
            DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));

            //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            //{
            //    DTProductneed.Rows[i2]["totalquantityinpack"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(_tparameterSum)))));
            //}

            for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
            {
                DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i2]["ProgramGrowRate"].ToString()) / 100))));
            }
            DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

            for (int i3 = 0; i3 < DTProductneed.Rows.Count; i3++)
            {
                DTProductneed.Rows[i3]["totalquantityinpack"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i3]["WastageRate"].ToString()) / 100)));

            }


            DTProductneed.Columns.Add("totalCost", typeof(decimal));
            for (int i4 = 0; i4 < DTProductneed.Rows.Count; i4++)
            {
                DTProductneed.Rows[i4]["totalCost"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i4]["totalquantityinpack"].ToString()) * Convert.ToDecimal(DTProductneed.Rows[i4]["Price"].ToString()));

            }

            //DTProductneed.Columns.Add("WastageRate", typeof(decimal));
            //DTProductneed.Columns.Add("ProgramGrowRate", typeof(decimal));
            con.Close();
            /////consumble  useage
            if (j == 0)
            {
                DT = DTProductneed;
            }
            else
            {
                // for (int i = 0; i < DTProductneed.Rows.Count; i++)
                // {
                //     DataRow[] rows = DT.Select("ProductId='" + DTProductneed.Rows[i]["ProductId"] + "'");

                //     if (rows.Length > 0)
                //     {
                //         rows[0]["Productneed"] = "";

                //     }
                //}
                foreach (DataRow dr in DTProductneed.Rows)
                {
                    DataRow[] rows = DT.Select("ProductId='" + dr["ProductId"] + "' and month1='" + dr["month1"] + "' And TypeID='" + dr["TypeID"] + "'");
                    if (rows.Length > 0)
                    {
                        rows[0]["Productneed"] = Convert.ToDecimal(rows[0]["Productneed"]) + Convert.ToDecimal(dr["Productneed"]);
                        rows[0]["Adjustedpacksize"] = Convert.ToDecimal(rows[0]["Adjustedpacksize"]) + Convert.ToDecimal(dr["Adjustedpacksize"]);
                        rows[0]["adjProgramgrowthrate"] = Convert.ToDecimal(rows[0]["adjProgramgrowthrate"]) + Convert.ToDecimal(dr["adjProgramgrowthrate"]);
                        rows[0]["totalquantityinpack"] = Convert.ToDecimal(rows[0]["totalquantityinpack"]) + Convert.ToDecimal(dr["totalquantityinpack"]);
                        rows[0]["totalCost"] = Convert.ToDecimal(rows[0]["totalCost"]) + Convert.ToDecimal(dr["totalCost"]);
                    }
                    else
                    {
                        DT.ImportRow(dr);
                    }
                }
            }
            //   DTProductneed = null;

        }
        private void fillChartByProductPrice()
        {
           // Chart12.Width = 2500;

            decimal existingtestnumber;
            decimal newpatienttestnumber;
            decimal Per;
            decimal testPermonth;
            decimal totaltest;
            decimal ttltest = 0;
            int period = 0;
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter("SELECT *   FROM [TestByMonth] WHERE ForeCastID=" + ID + "  order by TestID", con);
            adapt.Fill(DTmonth);
            period = Convert.ToInt32(DTmonth.Rows[0]["Duration"]) * 12;
            con.Close();
            existingtestnumber = (Convert.ToDecimal(DTmonth.Rows[0]["ExistingPatient"]) * (Convert.ToDecimal(DTmonth.Rows[0]["Duration"]) * Convert.ToDecimal(DTmonth.Rows[0]["TotalTestPerYear"])));
           // existingtestnumber = existingtestnumber / (Convert.ToDecimal(DTmonth.Rows.Count));
            existingtestnumber = existingtestnumber / (Convert.ToInt32(period));
            if (_mMforecastinfo.ForecastType == "S") _type = MorbidityVariableUsage.OnEachSite.ToString();
            else if (_mMforecastinfo.ForecastType == "C") _type = MorbidityVariableUsage.OnAggSite.ToString();
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Test_Assumption"), _mMforecastinfo.ProgramId, _type);

            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 3 && gAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtGetValue.NewRow();
                    dr["VariableName"] = gAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
                    dtGetValue.Rows.Add(dr);
                }
                dtGetValue.AcceptChanges();
            }


            for (int i = 0; i < DTmonth.Rows.Count; i++)
            {
                //if (Convert.ToDouble(DTmonth.Rows[i]["tstNo"]) > 0)
                //{
                    foreach (DataRow dr in dtGetValue.Rows)
                    {
                        _parameterSum = ((Convert.ToDecimal(DTmonth.Rows[i]["" + dr["VariableName"].ToString() + ""])));
                        if (dr["variableEffect"].ToString() == "True") _tparameterSum = _tparameterSum + _parameterSum;
                        if (dr["variableEffect"].ToString() == "False") _tparameterSum = _tparameterSum - _parameterSum;
                    }
                    if (Convert.ToString(DTmonth.Rows[i]["TestID"]) =="3")
                    {

                    }
                    Per = _tparameterSum;
                    //Per = Convert.ToDecimal(DTmonth.Rows[i]["TestRepeatPerYear"]) + Convert.ToDecimal(DTmonth.Rows[i]["SymptomTestPerYear"]);
                    testPermonth = Convert.ToDecimal(DTmonth.Rows[i]["tstNo"]) * Convert.ToDecimal(DTmonth.Rows[i]["NewPatient"]);
                    totaltest = testPermonth + existingtestnumber;
                    //newpatienttestnumber = totaltest + (totaltest * (Per / 100));
                    newpatienttestnumber = totaltest;
                    decimal newpatienttestnumber1 = Math.Round(newpatienttestnumber, 2);
                   // calculation(Convert.ToDouble(DTmonth.Rows[i]["tstNo"]), Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k);
                    calculation(Convert.ToDouble(newpatienttestnumber1), Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k, Convert.ToInt32(DTmonth.Rows[0]["Duration"]));
                k++;

                //}

            }
            DataTable fg = DT;
            var result = from row in fg.AsEnumerable()
                         group row by row.Field<string>("TypeName") into grp
                         select new
                         {
                             TeamID = grp.Key,
                             MemberCount = grp.Sum(row => row.Field<decimal>("totalCost")),
                             CountThisWeek = grp.Sum(r => r.Field<decimal>("totalquantityinpack") * r.Field<decimal>("Price"))
                         };
            foreach (var item in result)
            {
                string product = item.TeamID.ToString();
                string bn = item.MemberCount.ToString();
                string bn1 = item.CountThisWeek.ToString();
            }
            DataTable t = //
            DT = DT.AsEnumerable().GroupBy(r => r.Field<int>("TypeID")).Select(g =>
              {
                  var row = DT.NewRow();

                  row["Typename"] = g.Max(r => r.Field<string>("TypeName"));
                  row["totalCost"] = g.Sum(r => r.Field<decimal>("totalCost"));
                

                  return row;
              }).CopyToDataTable();

            //var result = from row in DT.AsEnumerable()
            //             group row by row.Field<string>("TypeName") into grp
            //             select new
            //             {
            //                 TeamID = grp.Key,
            //                 MemberCount = grp.Sum(row => row.Field<decimal>("totalCost"))
            //             };
            //foreach (var item in result)
            //{
            //    string product = item.TeamID.ToString();
            //    string bn = item.MemberCount.ToString();
            //}
            foreach (DataRow dr in DT.Rows)
            {


                chart4.Series["Product Cost"].Points.AddXY(dr["Typename"].ToString(), dr["totalCost"].ToString());
                //  chart2.Series["Coverage"].LegendText = "Instrument Distribution";
            }
            chart4.Series["Product Cost"].IsValueShownAsLabel = true;
            chart4.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            chart4.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;


            //chart4.DataSource = DT;
            ////set the member of the chart data source used to data bind to the X-values of the series  
            //chart4.Series["Product Cost"].XValueMember = "Typename";
            ////set the member columns of the chart data source used to data bind to the X-values of the series  
            //chart4.Series["Product Cost"].YValueMembers = "totalCost";
            //// chart2.Titles.Add("Ratio of forcasted test by testing area");
            //// chart2.Series["Product Cost"].ChartType = SeriesChartType.Pie;
            //chart4.Series["Product Cost"].IsValueShownAsLabel = true;
            ////chart2.ChartAreas[0].AxisX.Interval = 1;
            //chart4.ChartAreas[0].AxisX.MajorGrid.LineColor = Color.White;
            //chart4.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            con.Close();

        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            //this.Hide();
            Productsummary frm = new Productsummary(ID, _mMforecastinfo);

           // PatientGroupRatio frm = new PatientGroupRatio(_mdiparent1, _forecastId, "E", StartDate, EndDate, reportingPeriod, Totaltarget, table);
            // Form1 frm = new Form1();
            frm.ShowDialog();
           // this.Close();
        }

        

    }
}
