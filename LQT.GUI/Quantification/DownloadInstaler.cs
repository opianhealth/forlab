﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Security.Principal;
using System.Diagnostics;
using Microsoft.Win32;

namespace LQT.GUI.Quantification
{
    public partial class DownloadInstaler : Form
    {
     public DownloadInstaler()
        {
            InitializeComponent();
            //GetInstalledApps();
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.WorkerReportsProgress = true;
            btnInstall.Enabled = false;
            
           
        }
        public void GetInstalledApps()
        {
            string uninstallKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(uninstallKey))
            {
                foreach (string skName in rk.GetSubKeyNames())
                {
                    using (RegistryKey sk = rk.OpenSubKey(skName))
                    {
                        try
                        {
                           // listBox1.Items.Add(sk.GetValue("DisplayName"));
                        }
                        catch (Exception ex)
                        { }
                    }
                }
               // label1.Text = listBox1.Items.Count.ToString();
            }
        }   
        private void btnDownload_Click(object sender, EventArgs e)
        {
            //this.downloadFile();
            backgroundWorker1.RunWorkerAsync();
            
        }
        public void endP()
        {
            foreach (Process Proc in Process.GetProcesses())
                if (Proc.ProcessName.Equals("For Lab.msi"))
                    Proc.Kill();

            string UninstallCommandString = "/x {0} /qr";

            System.Diagnostics.Process process = new System.Diagnostics.Process();
      
            ProcessStartInfo startInfo = new ProcessStartInfo();
            process.StartInfo = startInfo;

            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardError = true;

            startInfo.FileName = "msiexec.exe";
            startInfo.Arguments = string.Format(UninstallCommandString, "5EC501B6-6902-4F47-8620-5C931F122F56");

            process.Start();
            process.WaitForExit();
        }

        static bool CheckApplicationUpdate()
        {
            // location of setup file
            string InstallFile = @"C:\SetUp\For Lab.msi";
            // name of the .bat file that does the uninstall/install
            string BatName = @"C:\SetUp\Update.bat";
            // product code of the application
            string ProductCode = "{5EC501B6-6902-4F47-8620-5C931F122F56}";

            // if install file is not available skip the whole process
            if (!File.Exists(InstallFile))
                return false;

            // calculates the time difference betwenn install package and executable
            DateTime EXE_Stamp = Directory.GetLastWriteTime(Application.ExecutablePath);
            DateTime MSI_Stamp = Directory.GetLastWriteTime(InstallFile);
            //TimeSpan diff = MSI_Stamp - EXE_Stamp;
            //diff = TimeSpan.FromMinutes(2);
            ////if installable is newer than 2 minutes, does the new install
            //if (diff.Minutes > 0)
            //{
                string msg = "A new version of " + Application.ProductName +
                             " is available.\r\n\r\nClick OK to install it.";
                //MessageBox.Show(msg, "Updates available",
                //                MessageBoxButtons.OK,
                //                MessageBoxIcon.Information);

                // prepares the batch file
                string BatFile = "";
                string old_dir = Directory.GetCurrentDirectory();
                BatFile += "@echo off\r\n";
                BatFile += "ping localhost -n 2\r\n";
                BatFile += "C:\\WINDOWS\\system32\\msiexec.exe /x " +
                           ProductCode + " /qr \r\n";
                BatFile += "C:\\WINDOWS\\system32\\msiexec.exe /i \"" +
                           InstallFile + "\" /qr\r\n";
                BatFile += "cd \"" + old_dir + "\"\r\n";
                BatFile += "start \"\" " +
                           Environment.CommandLine + "\r\n";
                StreamWriter sw = new StreamWriter(BatName);
                sw.Write(BatFile);
                sw.Close();

                // executes the batch file
                System.Diagnostics.ProcessStartInfo psi =
                  new System.Diagnostics.ProcessStartInfo();

                psi.FileName = BatName;
                psi.Arguments = "";
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo = psi;
                p.Start();

                return true;
                
            //}
            //return false;
        }
        public static bool WebRequestTest()
        {
            string url = "http://www.google.com";
            try
            {
                System.Net.WebRequest myRequest = System.Net.WebRequest.Create(url);
                System.Net.WebResponse myResponse = myRequest.GetResponse();
            }
            catch (System.Net.WebException)
            {
                return false;
            }
            return true;
        }
        private void btnInstall_Click(object sender, EventArgs e)
        {
           
            //string installerFilePath;
            //if (IsAdministrator() == false)
            //{
            //    UninstallProduct("LQT Installer.msi");
                

            //    downloadFile();
            //    installerFilePath = @"C:\SetUp\LQT Installer.msi";
            //    //System.Diagnostics.Process installerProcess;
            //    Process installerProcess = new Process();
            //    installerProcess.StartInfo.Arguments = "/i " + installerFilePath + "";
            //    installerProcess = System.Diagnostics.Process.Start(installerFilePath);
            //    while (installerProcess.HasExited == false)
            //    {
            //        //indicate progress to user
            //        Application.DoEvents();
            //        System.Threading.Thread.Sleep(250);
            //    }
            //    MessageBox.Show("Installing Complete");
            //}
           bool status= WebRequestTest();
           if (status == true)
           {
               //downloadFile();
               //backgroundWorker1.RunWorkerAsync();
             //  if (CheckApplicationUpdate()) return;

               CheckApplicationUpdate();
               this.Close();
               Environment.Exit(0);
              // Application.Exit();
           }
           else MessageBox.Show("Please connect with internet");
        }
        private static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
       
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                const string url = "ftp://34.68.140.74/ForLab_NewVersion/For Lab.msi";
                NetworkCredential credentials = new NetworkCredential("forlab_admin", "dV8*4XjUN4J1");

                // Query size of the file to be downloaded
                WebRequest sizeRequest = WebRequest.Create(url);
                sizeRequest.Credentials = credentials;
                sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                int size = (int)sizeRequest.GetResponse().ContentLength;

                if (!this.IsHandleCreated)
                {
                    this.CreateHandle();
                }
                string root = @"C:\SetUp";

                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                if (!File.Exists(@"C:\SetUp\Update.bat"))
                {
                    string file = Path.Combine(root, "Update.bat");
                    File.WriteAllText(file, "");
                }
                //progressBar1.Invoke(
                //    (MethodInvoker)(() => progressBar1.Maximum = size));

                // Download the file
                if (File.Exists(@"C:\SetUp\For Lab.msi"))
                {
                    File.Delete(@"C:\SetUp\For Lab.msi");
                }
                WebRequest request = WebRequest.Create(url);
                request.Credentials = credentials;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                //using (var inputStream = File.OpenRead())

                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(@"C:\SetUp\For Lab.msi"))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    int totalReadBytesCount = 0;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                        //
                        totalReadBytesCount += read;
                        var progress = (totalReadBytesCount * 100.0) / size;
                        backgroundWorker1.ReportProgress((int)progress);
                        //

                        //int position = (int)fileStream.Position;
                        //progressBar1.Invoke(
                        //    (MethodInvoker)(() => progressBar1.Value = position));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //backgroundWorker1.CancelAsync();
         }

        public void downloadFile()
        {
            try
            {
                const string url = "ftp://brown.mysitehosted.com/test/For Lab.msi";
                NetworkCredential credentials = new NetworkCredential("asp", "asp@123");

                // Query size of the file to be downloaded
                WebRequest sizeRequest = WebRequest.Create(url);
                sizeRequest.Credentials = credentials;
                sizeRequest.Method = WebRequestMethods.Ftp.GetFileSize;
                int size = (int)sizeRequest.GetResponse().ContentLength;

                if (!this.IsHandleCreated)
                {
                    this.CreateHandle();
                }
                string root = @"C:\SetUp";
              
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                if (!File.Exists(@"C:\SetUp\Update.bat"))
                {
                    string file = Path.Combine(root, "Update.bat");
                    File.WriteAllText(file, "");
                }
                //progressBar1.Invoke(
                //    (MethodInvoker)(() => progressBar1.Maximum = size));

                // Download the file
                if (File.Exists(@"C:\SetUp\For Lab.msi"))
                {
                    File.Delete(@"C:\SetUp\For Lab.msi");
                }
                WebRequest request = WebRequest.Create(url);
                request.Credentials = credentials;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                //using (var inputStream = File.OpenRead())

                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(@"C:\SetUp\For Lab.msi"))
                {
                    byte[] buffer = new byte[10240];
                    int read;
                    int totalReadBytesCount = 0;
                    while ((read = ftpStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fileStream.Write(buffer, 0, read);
                        //
                        totalReadBytesCount += read;
                        var progress = (totalReadBytesCount * 100.0) / size;
                        backgroundWorker1.ReportProgress((int)progress);
                        //

                        //int position = (int)fileStream.Position;
                        //progressBar1.Invoke(
                        //    (MethodInvoker)(() => progressBar1.Value = position));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }
        void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Download complete.Please click on the Install button to begin installing updated version.");
            btnInstall.Enabled = true;
        }

        public static void UninstallProduct(string m_product)
        {
            // Strip file path (if any)
            string product = new System.IO.FileInfo(m_product).Name;

            // MessageBox.Show("Searching for product: " + product + "");
            Microsoft.Win32.RegistryKey root = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"Installer\Products");
            bool is_installed = false;

            // For each subkey, try to open its SourceList key
            foreach (string subkey_name in root.GetSubKeyNames())
            {
                Microsoft.Win32.RegistryKey subkey = root.OpenSubKey(subkey_name);
                Microsoft.Win32.RegistryKey sub_sub_key = subkey.OpenSubKey("SourceList");

                if (sub_sub_key.GetValue("PackageName") != null)
                {
                    if (sub_sub_key.GetValue("PackageName").ToString().ToLower() == m_product.ToLower())
                    {
                        // Retrieve the ProductName from the parent
                        if (subkey.GetValue("ProductName") != null)
                        {
                            string product_name = subkey.GetValue("ProductName").ToString();
                            //MessageBox.Show("Found Package: " + product_name);

                            // Now search on ProductName in another hive
                            Microsoft.Win32.RegistryKey prod_root = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall");

                            foreach (string inst_prod in prod_root.GetSubKeyNames())
                            {
                                Microsoft.Win32.RegistryKey inst_key = prod_root.OpenSubKey(inst_prod);
                                if (inst_key.GetValue("DisplayName") != null)
                                {
                                    if (inst_key.GetValue("DisplayName").ToString().ToLower() == product_name.ToLower())
                                    {

                                        is_installed = true; 
                                      
                                        string prod_code = inst_key.Name.Substring(1 + inst_key.Name.LastIndexOf(@"\"));
                                        //MessageBox.Show("Found Product Code: " + prod_code);

                                        //MessageBox.Show("Uninstalling product");
                                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                                        process.StartInfo.FileName = "msiexec";
                                        process.StartInfo.Arguments = "/quiet /x " + prod_code;
                                        process.Start();
                                        process.WaitForExit();

                                        string UninstallCommandString = "/x {0} /qr";


                                        //MessageBox.Show("First UnInstall Older version");
                                        ////Application.Exit();
                                        //System.Environment.Exit(0);
                                        //inst_key.Close();
                                        ProcessStartInfo startInfo = new ProcessStartInfo();
                                        process.StartInfo = startInfo;

                                        startInfo.UseShellExecute = false;
                                        startInfo.RedirectStandardError = true;

                                        startInfo.FileName = "msiexec.exe";
                                        startInfo.Arguments = string.Format(UninstallCommandString, prod_code);

                                        process.Start();
                                        process.WaitForExit();
                                    }
                                }

                                inst_key.Close();
                            }

                            prod_root.Close();
                        }
                    }
                }

                sub_sub_key.Close();
                subkey.Close();
            }

            root.Close();

            if (!is_installed)
            {
                //MessageBox.Show("Product not installed");
            }
        }
    }
}
