﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using LQT.Core.Domain;
using LQT.Core.Util;
//using LQT.GUI.Testing;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;
using LQT.GUI.UserCtr;
using LQT.Core;
using LQT.Core.Domain;
using System.Runtime.InteropServices;


namespace LQT.GUI.Quantification
{
    public partial class Frmsitebysiteforecast : Form
    {
        private Form _mdiparent1;
        private int _forecastId = 0;
        private string _Mode = "";
        private string StartDate = "";
        private string EndDate = "";
        private string reportingPeriod = "";
        private int _selectedRegionId = 0;
        private Form _mdiparent;
        SqlCommand cmdForecast = null;
        IList<ForlabSite> result = new List<ForlabSite>();
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        DataTable dt = new DataTable();
     //   string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
 
        public Frmsitebysiteforecast(Form mdiparent,int ForecastID,string mode,string StartDate,string EndDate,string reportingPeriod)
        {
       
          
            InitializeComponent();

            this._forecastId = ForecastID;
            this._Mode = mode;
            _mdiparent1 = mdiparent;
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.reportingPeriod = reportingPeriod;
            this.Load += new EventHandler(Form1_Load);
          
        }
        private void editRecordDisplay(int ID)
        {
            string _sql = "";
           
            _sql = string.Format(@"SELECT sf.[SiteID],st.SiteName as SiteName ,[CurrentPatient] ,[TargetPatient],'E' as Flag FROM [ForecastSiteInfo] sf  Left Join site st on st.SiteID = sf.SiteID   where ForecastinfoID={0}", ID);
            //using (connection = new SqlConnection(con))
            //{           
                using (cmdForecast = new SqlCommand(_sql, connection))
                {

                    cmdForecast.CommandType = CommandType.Text;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmdForecast))
                    {
                    
                            sda.Fill(dt);
                            dataGridView1.Columns[0].DataPropertyName = "SiteID";
                            dataGridView1.Columns[1].DataPropertyName = "SiteName";
                            dataGridView1.Columns[2].DataPropertyName = "CurrentPatient";
                            dataGridView1.Columns[3].DataPropertyName = "TargetPatient";
                            dataGridView1.Columns[4].DataPropertyName = "Flag";
                            dataGridView1.DataSource = dt;
                        
                    }
                 
                   connection.Close();
                }
                
            //}
        }

        //private void btnstyle(Button btn)
        //{
        //    Button b = btn;
        //    btn.TabStop = false;
        //    btn.FlatStyle = FlatStyle.Flat;
        //    btn.FlatAppearance.BorderSize = 0;
        //    btn.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
        //}
        private void PopRegion()
        {
            lstRegion.BeginUpdate();
            lstRegion.Items.Clear();
        
            foreach ( var items in DataRepository.GetAllRegion())
            {
                ListViewItem li = new ListViewItem(items.RegionName) { Tag = items.Id };            
                if (items.Id == _selectedRegionId)
                {
                    li.Selected = true;
                }
                lstRegion.Items.Add(li);
            }

            lstRegion.EndUpdate();


        }

        private void lstRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstRegion.SelectedItems.Count > 0)
            {
                int id = (int)lstRegion.SelectedItems[0].Tag;
                if (id != _selectedRegionId)
                {
                    _selectedRegionId = id;
                    lstSite.BeginUpdate();
                    lstSite.Items.Clear();
                    foreach (var items in DataRepository.GetAllSiteByRegionId(_selectedRegionId))
                    {
                        ListViewItem li = new ListViewItem(items.SiteName) { Tag = items.Id };
                        //if (items.Id == _selectedRegionId)
                        //{
                        //    li.Selected = true;
                        //}
                        lstSite.Items.Add(li);
                    }

                    lstSite.EndUpdate();
                 
                }
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            bool isduplicate = true;
            foreach (ListViewItem items in lstSite.SelectedItems)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["SiteID"].Value != null)
                    {
                        if (row.Cells["SiteID"].Value.ToString() == items.Tag.ToString())
                        {
                            isduplicate = false;
                        }
                    }
                }

                if (isduplicate == false)
                {
                    MessageBox.Show("Site already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.NewRow();
                        dr["SiteID"] = items.Tag;
                        dr["SiteName"] = items.Text;
                        dr["CurrentPatient"] = 0;
                        dr["TargetPatient"] = 0;
                        dr["Flag"] = "N";
                        dt.Rows.Add(dr);
                        dt.AcceptChanges();
                    }
                    else
                    {
                        //dt.Columns.Add("SiteID", typeof(System.Int32));
                        //dt.Columns.Add("SiteName", typeof(System.String));
                        //dt.Columns.Add("CurrentPatient", typeof(System.Decimal));
                        //dt.Columns.Add("TargetPatient", typeof(System.Decimal));
                        //dt.Columns.Add("Flag", typeof(System.String));
                        DataRow dr = dt.NewRow();
                        dr["SiteID"] = items.Tag;
                        dr["SiteName"] = items.Text;
                        dr["CurrentPatient"] = 0;
                        dr["TargetPatient"] = 0;
                        dr["Flag"] = "N";
                        dt.Rows.Add(dr);
                        dt.AcceptChanges();
                    }
                    dataGridView1.Columns[0].DataPropertyName = "SiteID";
                    dataGridView1.Columns[1].DataPropertyName = "SiteName";
                    dataGridView1.Columns[2].DataPropertyName = "CurrentPatient";
                    dataGridView1.Columns[3].DataPropertyName = "TargetPatient";
                    dataGridView1.Columns[4].DataPropertyName = "Flag";
                    dataGridView1.DataSource = dt;
                 
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

            try
            {


                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    //if (row.Index != 0)
                    //{

                    ////using (connection = new SqlConnection(con))
                    ////{
                    ////    //SaveOrUpdateObject1();
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            // string _sql1 = string.Format(@"Select Count(*) from ForecastSiteInfo Where SiteID={0} and  ForecastinfoID={1}", row.Cells["SiteID"].Value, _forecastId);
                            using (cmdForecast = new SqlCommand("", connection,trans))
                            {
                                //   int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                //  if (cnt > 0)
                                //  {
                                cmdForecast.CommandText = "Delete from ForecastSiteInfo Where SiteID= " + row.Cells["SiteID"].Value + " and  ForecastinfoID=" + _forecastId + " ";
                              ///  cmdForecast = new SqlCommand("Delete from ForecastSiteInfo Where SiteID= " + row.Cells["SiteID"].Value + " and  ForecastinfoID=" + _forecastId + " ", connection);
                                cmdForecast.ExecuteNonQuery();
                                cmdForecast.CommandText = "Delete from [PatientAssumption] Where SiteID= " + row.Cells["SiteID"].Value + " and  ForecastinfoID=" + _forecastId + " ";
                               // cmdForecast = new SqlCommand("Delete from [PatientAssumption] Where SiteID= " + row.Cells["SiteID"].Value + " and  ForecastinfoID=" + _forecastId + " ", connection);
                                cmdForecast.ExecuteNonQuery();
                                //  }
                            }
                            trans.Commit();
                        }
                      connection.Close();
                    //}
                    dataGridView1.Rows.RemoveAt(row.Index);
                    // }
                }
            }
            catch (Exception ex)
            {
             connection.Close();
            }
        }

        private void btnprevious_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMorbidityForecastDefination frm = new frmMorbidityForecastDefination(_mdiparent1, _forecastId, _Mode, 0, 0, 0, 0, 0);
            frm.ShowDialog();
            this.Close();
        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            string _sql = "";
            string _msg = "Site by site forecast inserted successfully";
            bool Isvalid = true;
            bool IsSave = false;
            int rowindex = 0;
            int colindex = 0;
           try
           {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToInt32(row.Cells["ColCurrentPatient"].Value) == 0 )
                {

                    Isvalid = false;
                    rowindex = row.Index;
                    colindex = row.Cells["ColCurrentPatient"].ColumnIndex;
                    break;
                }
            }
            if (Isvalid == false)
            {
                dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                MessageBox.Show("Current Patient must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
              //  throw new LQTUserException("Current Patient must be greater than zero");
            
            }



            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToInt32(row.Cells["ColTargetPatient"].Value) == 0)
                {

                    Isvalid = false;
                    rowindex = row.Index;
                    colindex = row.Cells["ColTargetPatient"].ColumnIndex;
                }
            }
            if (Isvalid == false)
            {
                dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                MessageBox.Show("Target Patient must be greater than zero", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
             //   throw new LQTUserException("Target Patient must be greater than zero");

            }

            int Totaltarget = 0;
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(int));
            table.Columns.Add("ForecastinfoID", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("CurrentPatient", typeof(int));
            table.Columns.Add("TargetPatient", typeof(int));
            //using (connection = new SqlConnection(con))
            //{
            if (connection.State == ConnectionState.Open)
            { }
            else
            { connection.Open(); }
                using (cmdForecast = new SqlCommand("", connection))
                {
                    if (dataGridView1.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow row in dataGridView1.Rows)
                        {
                            //if (row.Cells["SiteID"].Value != null)
                            //{
                            if (Convert.ToString(row.Cells["Flag"].Value) == "N")
                            {
                                _sql = "INSERT INTO [ForecastSiteInfo] ([SiteID],[ForecastinfoID],[CurrentPatient],[TargetPatient]) VALUES(@SiteID,@ForecastinfoID,@CurrentPatient,@TargetPatient)";
                            }
                            else
                            {
                                _sql = "UPDATE [ForecastSiteInfo] SET [CurrentPatient] = @CurrentPatient ,[TargetPatient]= @TargetPatient WHERE [SiteID] = @SiteID AND [ForecastinfoID]=@ForecastinfoID ";
                            }
                            if (_sql != "")
                            {


                                cmdForecast.Parameters.Add("@SiteID", SqlDbType.Int).Value = row.Cells["SiteID"].Value;
                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.Int).Value = _forecastId;
                                cmdForecast.Parameters.Add("@CurrentPatient", SqlDbType.BigInt).Value = row.Cells["ColCurrentPatient"].Value;
                                cmdForecast.Parameters.Add("@TargetPatient", SqlDbType.BigInt).Value = row.Cells["ColTargetPatient"].Value;
                                Totaltarget = Totaltarget + Convert.ToInt32(row.Cells["ColTargetPatient"].Value);
                                cmdForecast.CommandText = _sql;
                                cmdForecast.ExecuteNonQuery();
                                table.Rows.Add(row.Cells["SiteID"].Value, _forecastId, row.Cells["ColSiteName"].Value, row.Cells["ColCurrentPatient"].Value, row.Cells["ColTargetPatient"].Value);
                                IsSave = true;
                                cmdForecast.Parameters.Clear();
                                // MessageBox.Show(_msg);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select atleast one site", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    
                    }
                        }
                //}   
             connection.Close();
            //}


            if (IsSave == true)
            {
               // MessageBox.Show("Site by site forecast saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                
                ((LqtMainWindowForm)_mdiparent1).ShowStatusBarInfo("Site by site forecast saved successfully", true);
                this.Hide();
           //    Productsummary frm = new Productsummary();
          
                PatientGroupRatio frm = new PatientGroupRatio(_mdiparent1, _forecastId, "E", StartDate, EndDate, reportingPeriod, Totaltarget, table);
               // Form1 frm = new Form1();
                frm.ShowDialog();
                this.Close();
            }
            //FrmTestTestingprotocol frm = new FrmTestTestingprotocol();
        }
            //frm.ShowDialog();
                catch (Exception ex)
            {
            connection.Close();
            }
           
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
         
        }
        private void setErrorText1()
        {
            if (dataGridView1.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (Convert.ToInt32(row.Cells["ColCurrentPatient"].Value) > Convert.ToInt32(row.Cells["ColTargetPatient"].Value))
                    {
                       row.Cells["ColCurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        //row.ErrorText = "The value of Current Patient must be a less then Target Patient value";
                        
                    }
                }
            }
        }
        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string TargetPatient = dataGridView1.Rows[e.RowIndex].Cells["ColTargetPatient"].Value.ToString();
            string CurrentPatient = dataGridView1.Rows[e.RowIndex].Cells["ColCurrentPatient"].Value.ToString();
            int newInteger = 0;
            //if (e.ColumnIndex == dataGridView1.Columns["ColCurrentPatient"].Index )
            //{
            //    dataGridView1.Rows[e.RowIndex].ErrorText = "";
            //    if (dataGridView1.Rows[e.RowIndex].IsNewRow) { return; }
            //    if (!int.TryParse(e.FormattedValue.ToString(), out newInteger) || newInteger < 0)
            //    {
            //        e.Cancel = true;
            //        dataGridView1.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a Positive integer";
            //       // throw new LQTUserException("The value of Current Patient must be a Positive integer");
            //        MessageBox.Show("The value of Current Patient must be a Positive integer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}
            //if ( e.ColumnIndex == dataGridView1.Columns["ColTargetPatient"].Index)
            //{
            //    dataGridView1.Rows[e.RowIndex].ErrorText = "";
            //    if (dataGridView1.Rows[e.RowIndex].IsNewRow) { return; }
            //    if (!int.TryParse(e.FormattedValue.ToString(), out newInteger) || newInteger < 0)
            //    {
            //        e.Cancel = true;
            //        dataGridView1.Rows[e.RowIndex].ErrorText = "The value of  Target Patient must be a Positive integer";

            //       // throw new LQTUserException("The value of Target Patient must be a Positive integer");
            //        MessageBox.Show("The value of Target Patient must be a Positive integer", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }

            //}

            if (e.ColumnIndex == dataGridView1.Columns["ColCurrentPatient"].Index)
            {
                dataGridView1.Rows[e.RowIndex].ErrorText = "";
                if (Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells["ColTargetPatient"].Value.ToString()) > 0)
                {
                    if (int.TryParse(e.FormattedValue.ToString(), out newInteger) || newInteger < 0)
                    {
                        if (Convert.ToInt64(e.FormattedValue) > Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells["ColTargetPatient"].Value.ToString()))
                        {
                          //  e.Cancel = true;
                            ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["ColCurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                           // dataGridView1.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                         //   throw new LQTUserException("The value of Current Patient must be a less then Target Patient value");
                           // MessageBox.Show("The value of Current Patient must be a less then Target Patient value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            // btnAdd.Enabled = false;
                        }
                    }
                    //   else //btnAdd.Enabled = true;
                }
                else if (Convert.ToInt64(e.FormattedValue) == 0)
                {

                    e.Cancel = true;
                    dataGridView1.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be greater than zero";

                  //  throw new LQTUserException("The value of Current Patient must be greater than zero");
                    MessageBox.Show("The value of Current Patient must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
            if (e.ColumnIndex == dataGridView1.Columns["ColTargetPatient"].Index)
            {
                int inde = dataGridView1.Columns["ColTargetPatient"].Index;
                if (int.TryParse(e.FormattedValue.ToString(), out newInteger) || newInteger < 0)
                {
                    if (Convert.ToInt64(e.FormattedValue) > 0 )
                    {
                        if (Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells["ColCurrentPatient"].Value.ToString()) > Convert.ToInt64(e.FormattedValue))
                        {
                          //  e.Cancel = true;
                            ((System.Windows.Forms.DataGridView)(sender)).Rows[e.RowIndex].Cells["ColCurrentPatient"].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                          //  dataGridView1.Rows[e.RowIndex].ErrorText = "The value of Current Patient must be a less then Target Patient value";
                            //throw new LQTUserException("The value of Current Patient must be a less then Target Patient value");
                           // MessageBox.Show("The value of Current Patient must be a less then Target Patient value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            //  btnAdd.Enabled = false;
                        }
                        //  else btnAdd.Enabled = true;
                    }
                    else if (Convert.ToInt64(e.FormattedValue) == 0)
                    {
                        e.Cancel = true;
                        dataGridView1.Rows[e.RowIndex].ErrorText = "The value of Target Patient must be greater than zero";

                       // throw new LQTUserException("The value of Target Patient must be greater than zero");
                        MessageBox.Show("The value of Target Patient must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
             
            }
        }



        public bool validation()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (Convert.ToInt32(row.Cells["ColTargetPatient"].Value) == 0)
                {
                    return false;
                }
            }
           
            return true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
          
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem items in lstSite.Items)
            {
                if (items.Selected == false)
                {
                    items.Selected = true;                
                    items.BackColor = ColorTranslator.FromHtml("#3399FF");
                    items.ForeColor = Color.White;
                }
                else
                {
                    items.Selected = false;
                    items.BackColor = Color.White;
                    items.ForeColor = Color.Black;
                
                }

            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
           
            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) ) //&& e.KeyChar != '.'
            {
                e.Handled = true;
            }

            // only allow one decimal point
            //if (e.KeyChar == '.'
            //    && (sender as TextBox).Text.IndexOf('.') > -1)
            //{
            //    e.Handled = true;
            //}
        }
        private void Form1_Load(System.Object sender, System.EventArgs e)
        {
            PopRegion();

            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;

            btnadd.TabStop = false;
            btnadd.FlatStyle = FlatStyle.Flat;
            btnadd.FlatAppearance.BorderSize = 0;
            btnadd.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnRemove.TabStop = false;
            btnRemove.FlatStyle = FlatStyle.Flat;
            btnRemove.FlatAppearance.BorderSize = 0;
            btnRemove.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            btnprevious.TabStop = false;
            btnprevious.FlatStyle = FlatStyle.Flat;
            btnprevious.FlatAppearance.BorderSize = 0;
            btnprevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            btnnext.TabStop = false;
            btnnext.FlatStyle = FlatStyle.Flat;
            btnnext.FlatAppearance.BorderSize = 0;
            btnnext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            if (_Mode != "A")
            {
                this.editRecordDisplay(_forecastId);
            }

            this.dataGridView1.CellValidating += new DataGridViewCellValidatingEventHandler(dataGridView1_CellValidating);
            this.dataGridView1.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dataGridView1_EditingControlShowing);
            this.dataGridView1.CellEndEdit += new DataGridViewCellEventHandler(dataGridView1_CellEndEdit);
            this.setErrorText1();

        }
        private void Frmsitebysiteforecast_Load(object sender, EventArgs e)
        {
         
        }

        private void lstSite_Click(object sender, EventArgs e)
        {
          
        }

        private void lstSite_SelectedIndexChanged(object sender, EventArgs e)
        {



             
              
                 
            //if (lstSite.SelectedItems.Count > 0)
            //{
            //    if (lstSite.SelectedItems[0].Selected == true)
            //    {
            //         lstSite.SelectedItems[0].Selected = false;
            //        lstSite.SelectedItems[0].BackColor = Color.White;
            //        lstSite.SelectedItems[0].ForeColor = Color.Black;
            //    }
            //    else
            //    {
            //        lstSite.SelectedItems[0].Selected = true;
            //        lstSite.SelectedItems[0].BackColor = ColorTranslator.FromHtml("#3399FF");
            //        lstSite.SelectedItems[0].ForeColor = Color.White;
                
            //    }

            //}
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Frmimportpatientno frm = new Frmimportpatientno();
            DialogResult dr = frm.ShowDialog();
            DataTable Dt;
            if (dr == DialogResult.OK)
            {
                Dt = frm.patientData;
                FillGridData(Dt, frm);
            }

        }
        private void FillGridData(DataTable Dtsite, Frmimportpatientno importpatient)
        {

            string regionName;

            string siteName;
            bool error = false;
            int errorno = 0;
            string rName = "";
            ForlabRegion region = null;

            ForlabSite site = null;
          
            result = DataRepository.GetAllSite();
            if (result.Count > 0)
            {

                try
                {



                
                    for (var i = 0; i < Dtsite.Rows.Count; i++)
                    {
                        label12.Text = "Importing Data.....................";

                      //  Dtsite.Rows[i]["rowno"] = i;
                        regionName = Convert.ToString(Dtsite.Rows[i][0]).Trim();
                        siteName = Convert.ToString(Dtsite.Rows[i][1]).Trim();
                        if (rName != regionName)
                        {
                            if (!string.IsNullOrEmpty(regionName))
                                region = DataRepository.GetRegionByName(regionName);
                            else
                                region = null;
                            rName = regionName;
                        }

                        if (region != null)
                        {

                            if (!String.IsNullOrEmpty(siteName))
                                site=DataRepository.GetSiteByName(siteName, region.Id);
                            if (site != null)
                                {
                                    bool isduplicate = true;
                                    //foreach (ListViewItem items in lstSite.SelectedItems)
                                    //{
                                        foreach (DataGridViewRow row in dataGridView1.Rows)
                                        {
                                            if (row.Cells["SiteID"].Value != null)
                                            {
                                                if (row.Cells["SiteID"].Value.ToString() ==  Convert.ToString(site.Id))
                                                {
                                                    isduplicate = false;
                                                }
                                            }
                                        }

                                        if (isduplicate == false)
                                        {
                                            MessageBox.Show("Site already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return;

                                        }
                                        else
                                        {
                                            //if (Convert.ToInt32(Dtsite.Rows[i][2].ToString()) < Convert.ToInt32(Dtsite.Rows[i][3].ToString()))
                                            //{
                                                DataRow dr = dt.NewRow();
                                                dr["SiteID"] = Convert.ToString(site.Id);
                                                dr["SiteName"] = siteName;
                                                dr["CurrentPatient"] = Dtsite.Rows[i][2];
                                                dr["TargetPatient"] = Dtsite.Rows[i][3];
                                                dr["Flag"] = "N";
                                                dt.Rows.Add(dr);
                                                dt.AcceptChanges();
                                                dataGridView1.Columns[0].DataPropertyName = "SiteID";
                                                dataGridView1.Columns[1].DataPropertyName = "SiteName";
                                                dataGridView1.Columns[2].DataPropertyName = "CurrentPatient";
                                                dataGridView1.Columns[3].DataPropertyName = "TargetPatient";
                                                dataGridView1.Columns[4].DataPropertyName = "Flag";
                                                dataGridView1.DataSource = dt;
                                            //}
                                            //else
                                            //{
                                            //    errorno++;
                                            //    importpatient.changecolorexcel("Target Patient should be greater than Current Patient", Color.Red, i);
                                            //    error = true;
                                            //}
                                        }
                                    //}

                                }
                                else
                                {
                                  //  Dtsite.Rows[i]["ErrorMsg"] = "Site Doesn't Exist";
                                    errorno++;
                                    importpatient.changecolorexcel("Site Doesn't Exist",Color.Red,i);
                                    error = true;
                                }
                        }
                        else
                        {
                           // Dtsite.Rows[i]["ErrorMsg"] = "Region Doesn't Exist";
                            errorno++;
                            importpatient.changecolorexcel("Region Doesn't Exist", Color.Red, i);
                            error = true;
                        }
                        if (errorno > 20)
                        {
                            label12.Text = "";
                            throw new Exception("There too many problem with Site Instrument data, please troubleshoot and try to import again.");
                        }

                    }
                    if (error == true)
                        label12.Text = "Please Check excel to correct some records";
                    //this.DialogResult = DialogResult.OK;
                    //this.Close();

                }
                catch (Exception ex)
                {
                    label12.Text = "";
                    MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        
        
        
        }


       
  
    }
}
