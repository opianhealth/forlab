﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using LQT.Core.Util;
using LQT.Core.Domain;
using LQT.GUI.MorbidityUserCtr;
using LQT.Core.UserExceptions;

using System.Linq;

namespace LQT.GUI.Quantification
{
    public partial class FrmNewMorbidity : Form
    {

        private Form _mdiparent;
        public IList<MMProgram> _mMPrograms = new List<MMProgram>();
        public MMProgram _selectedProgram;
        public List<string> _distinctFMethods = new List<string>();
        public bool _siteBySite = false;

        public string _selectedProgramM;
  
        public FrmNewMorbidity( Form mdiparent)
        {
     
            this._mdiparent = mdiparent;
         _mMPrograms = DataRepository.GetMMPrograms();
            InitializeComponent();
             LoadMorbidityParameters();
      
        }

         public void LoadMorbidityParameters()
        {
            foreach (MMProgram mProgram in _mMPrograms)
            {

                RadioButton button = new RadioButton();
                button.Tag = mProgram;
                button.Text = mProgram.ProgramName;
                button.Width = 350;
                //button.Height = 40;
                button.AutoSize = false;
                button.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
                //button.CheckedChanged += button_CheckedChanged;
                button.CheckedChanged += new EventHandler(button_CheckedChanged);
                flowLayoutPanelWiz1.Controls.Add(button);
            }

        }

         public void button_CheckedChanged(object sender, EventArgs e)
         {
             RadioButton radioButton = sender as RadioButton;
             if(radioButton.Checked)
             {
                 _selectedProgram = (MMProgram)radioButton.Tag;
               
                 List<string> pMethods = new List<string>(); 
                 int i=0;
                 foreach (MMForecastParameter mParams in _selectedProgram.MMForecastParameters)
                 {
                     pMethods.Add(((MorbidityForecastingMethod)mParams.ForecastMethod).ToString());
                     //pMethods.SetValue(, i);
                     i++;
                 }
                 _distinctFMethods = pMethods.Distinct().ToList();
                 BindForecastMethod();
                 BindPatientGroup();
                 BindGeneralAssumption();
             }

             
         }

         public void BindForecastMethod()
         {
             lvForecastMethod.BeginUpdate();
             lvForecastMethod.Items.Clear();
             foreach (string s in _distinctFMethods)
             {
                 var listViewItem = new ListViewItem(s.Replace('_',' '));
                 lvForecastMethod.Items.Add(listViewItem);
             }
             lvForecastMethod.EndUpdate();
         }

         public void BindPatientGroup()
         {
             lvPatientGroup.BeginUpdate();
             lvPatientGroup.Items.Clear();
             foreach (MMGroup group in _selectedProgram.MMGroups)
             {
                 var listViewItem = new ListViewItem(group.GroupName);
                 lvPatientGroup.Items.Add(listViewItem);
             }
             lvPatientGroup.EndUpdate();
         }

         public void BindGeneralAssumption()
         {
             lvGeneralAssumptions.BeginUpdate();
             lvGeneralAssumptions.Items.Clear();
             foreach (MMGeneralAssumption gAssumption in _selectedProgram.MMGeneralAssumptions)
             {
                 ListViewItem listViewItem = new ListViewItem(((MorbidityVariablesDataType)gAssumption.VariableDataType).ToString())
                 {
                     Tag = gAssumption
                 };
                 listViewItem.SubItems.Add(gAssumption.VariableName);
                 listViewItem.SubItems.Add(gAssumption.VariableFormula);
                 listViewItem.SubItems.Add(gAssumption.UseOn);

                 lvGeneralAssumptions.Items.Add(listViewItem);

             }
             lvGeneralAssumptions.EndUpdate();
         }

         private void cboscope_SelectedIndexChanged(object sender, EventArgs e)
         {

         }

         private void comPeriod_SelectedIndexChanged(object sender, EventArgs e)
         {

         }

         private void stepWizardControl1_SelectedPageChanged(object sender, EventArgs e)
         {
             if (stepWizardControl1.SelectedPage == wizardPage2)
             {
                 BindSelectedModelForecastMethod();
                 NewPopPeriod();
             }
             if (stepWizardControl1.SelectedPage == wizardPage3)
             {
                 _siteBySite=rdbsitebysite.Checked ? true : false;
                 if (_siteBySite)
                 {
                     NewSiteSelection sitebysite = new NewSiteSelection();
                     sitebysite.Dock=DockStyle.Fill;
                     wizardPage3.Controls.Add(sitebysite);
                 }
                 else
                 {
                     NewCatSelection catSelection = new NewCatSelection();
                     catSelection.Dock = DockStyle.Fill;
                     wizardPage3.Controls.Add(catSelection);
                 }

             }
             if (stepWizardControl1.SelectedPage == wizardPage5)
             {
                 BindProgramModelVariables();
             }
             if (stepWizardControl1.SelectedPage == wizardPage4)
             {
                 BindPatientPopulaitonGroupRatio();
             }
             if (stepWizardControl1.SelectedPage == wizardPage6)
             {
                 BindPatientPopulaitonAssumption();
             }
             if (stepWizardControl1.SelectedPage == wizardPage7)
             {
                 BindProductAssumption();
             }
             if (stepWizardControl1.SelectedPage == wizardPage8)
             {
                 BindTestAssumption();
             }
             if (stepWizardControl1.SelectedPage == wizardPage9)
             {
                 NewTestSelection ctrlTestSelection = new NewTestSelection();
                 ctrlTestSelection.Dock = DockStyle.Fill;
                 wizardPage9.Controls.Add(ctrlTestSelection);

                 if (_selectedProgram.RTP == 1)
                     wizardPage9.NextPage = wizardPage10;
                 else
                     wizardPage9.NextPage = wizardPage11;
             }
             if (stepWizardControl1.SelectedPage == wizardPage10)
             {
                 NewRapidTestProtocol ctrlRapidTestProtocol = new NewRapidTestProtocol();
                 ctrlRapidTestProtocol.Dock = DockStyle.Fill;
                 wizardPage10.Controls.Add(ctrlRapidTestProtocol);
                 if (_selectedProgram.GTP == 1)
                     wizardPage9.NextPage = wizardPage11;
                 else
                     wizardPage9.NextPage = wizardPage12;
             }
             if (stepWizardControl1.SelectedPage == wizardPage11)
             {
                 NewGeneralTestsProtocol ctrlGeneralTestsProtocol = new NewGeneralTestsProtocol();
                 ctrlGeneralTestsProtocol.Dock = DockStyle.Fill;
                 wizardPage11.Controls.Add(ctrlGeneralTestsProtocol);
             }
         }

         private void BindSelectedModelForecastMethod()
         {
             comForecastMethodWiz2.BeginUpdate();
             comForecastMethodWiz2.Items.Clear();
             foreach (string s in _distinctFMethods)
             {
                 comForecastMethodWiz2.Items.Add(s.ToString());
             }
             comForecastMethodWiz2.EndUpdate();
         }

         private void NewPopPeriod()
         {
             string[] periods = Enum.GetNames(typeof(ForecastPeriodEnum));
             comPeriod.Items.Clear();
             for (int i = 0; i < periods.Length; i++)
             {
                 comPeriod.Items.Add(periods[i]);
             }
         }

         private void comPeriod_SelectedIndexChanged_1(object sender, EventArgs e)
         {
             if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Bimonthly.ToString())
             {
                 label5.Text = "Forecast in Bimonthly:";
                 dtpstart.DisableMonthCom = false;
                 dtpstart.PopQuarter = false;
                 dtpstart.PopMonthOrQuarterValue();

             }
             else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Monthly.ToString())
             {
                 label5.Text = "Forecast in Months:";
                 dtpstart.DisableMonthCom = false;
                 dtpstart.PopQuarter = false;
                 dtpstart.PopMonthOrQuarterValue();
             }
             else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Quarterly.ToString())
             {
                 label5.Text = "Forecast in Quarters:";
                 dtpstart.DisableMonthCom = false;
                 dtpstart.PopQuarter = true;
                 dtpstart.PopMonthOrQuarterValue();
             }
             else if (comPeriod.SelectedItem.ToString() == ForecastPeriodEnum.Yearly.ToString())
             {
                 label5.Text = "Forecast in Years:";
                 dtpstart.DisableMonthCom = true;
                 dtpstart.PopMonthOrQuarterValue();
             }

             dtpstart.SetDefaultDate();
         }

         private void BindProgramModelVariables()
         {
             foreach (MMForecastParameter mParam in _selectedProgram.MMForecastParameters)
             {


                 if (_selectedProgramM == ((MorbidityForecastingMethod)mParam.ForecastMethod).ToString())
                 {
                     Panel p = new Panel();
                     p.Height = 3;
                     flowLayoutPanelWiz4.Controls.Add(p);
                     flowLayoutPanelWiz4.SetFlowBreak(p, true);

                     Label l = new Label();
                     l.Width = 450;
                     l.Text = mParam.VariableName;
                     flowLayoutPanelWiz4.Controls.Add(l);
                     flowLayoutPanelWiz4.SetFlowBreak(l, true);

                     
                     TextBox t = new TextBox();
                     flowLayoutPanelWiz4.Controls.Add(t);
                     t.Width = 450;
                     t.Enabled = false;

                     if (mParam.VariableFormula == string.Empty || mParam.VariableFormula == null)
                         t.Enabled = true;

                     flowLayoutPanelWiz4.SetFlowBreak(t, true);

                    

                 }
             }

         }

         private void comForecastMethodWiz2_SelectedIndexChanged(object sender, EventArgs e)
         {
             _selectedProgramM = comForecastMethodWiz2.Text;
         }

         private void BindPatientPopulaitonGroupRatio()
         {
             foreach (MMGroup mGroup in _selectedProgram.MMGroups)
             {

                     Panel p = new Panel();
                     p.Height = 3;
                     flowLayoutPanelWizPGroup.Controls.Add(p);
                     flowLayoutPanelWizPGroup.SetFlowBreak(p, true);

                     Label l = new Label();
                     l.Width = 450;
                     l.Text = mGroup.GroupName;
                     flowLayoutPanelWizPGroup.Controls.Add(l);
                     flowLayoutPanelWizPGroup.SetFlowBreak(l, true);


                     TextBox t = new TextBox();
                     flowLayoutPanelWizPGroup.Controls.Add(t);
                     t.Width = 450;


                     flowLayoutPanelWizPGroup.SetFlowBreak(t, true);
             }

         }

         private void BindPatientPopulaitonAssumption()
         {
             foreach (MMGeneralAssumption gAssumption in _selectedProgram.MMGeneralAssumptions)
             {
                 if (gAssumption.AssumptionType == 1)
                 {
                     Panel p = new Panel();
                     p.Height = 2;
                     flowLayoutPanelPatientPopAss.Controls.Add(p);
                     flowLayoutPanelPatientPopAss.SetFlowBreak(p, true);

                     Label l = new Label();
                     l.Width = 450;
                     l.Text = gAssumption.VariableName;
                     flowLayoutPanelPatientPopAss.Controls.Add(l);
                     flowLayoutPanelPatientPopAss.SetFlowBreak(l, true);


                     TextBox t = new TextBox();
                     flowLayoutPanelPatientPopAss.Controls.Add(t);
                     t.Width = 450;
                     t.Enabled = false;

                     if (gAssumption.VariableFormula == string.Empty || gAssumption.VariableFormula == null)
                         t.Enabled = true;

                     flowLayoutPanelPatientPopAss.SetFlowBreak(t, true);
                 }

             }
             BindPatientPopulaitonDDl();

         }

         private void BindProductAssumption()
         {
             foreach (MMGeneralAssumption gAssumption in _selectedProgram.MMGeneralAssumptions)
             {
                 if (gAssumption.AssumptionType == 2)
                 {
                     Panel p = new Panel();
                     p.Height = 2;
                     flowLayoutPanelProdAssump.Controls.Add(p);
                     flowLayoutPanelProdAssump.SetFlowBreak(p, true);

                     Label l = new Label();
                     l.Width = 450;
                     l.Text = gAssumption.VariableName;
                     flowLayoutPanelProdAssump.Controls.Add(l);
                     flowLayoutPanelProdAssump.SetFlowBreak(l, true);


                     TextBox t = new TextBox();
                     flowLayoutPanelProdAssump.Controls.Add(t);
                     t.Width = 450;
                     t.Enabled = false;

                     if (gAssumption.VariableFormula == string.Empty || gAssumption.VariableFormula == null)
                         t.Enabled = true;

                     flowLayoutPanelProdAssump.SetFlowBreak(t, true);
                 }

             }
             BindProductDDl();

         }

         private void BindTestAssumption()
         {
             foreach (MMGeneralAssumption gAssumption in _selectedProgram.MMGeneralAssumptions)
             {
                 if (gAssumption.AssumptionType == 3)
                 {
                     Panel p = new Panel();
                     p.Height = 2;
                     flowLayoutPanelTestAssump.Controls.Add(p);
                     flowLayoutPanelTestAssump.SetFlowBreak(p, true);

                     Label l = new Label();
                     l.Width = 450;
                     l.Text = gAssumption.VariableName;
                     flowLayoutPanelTestAssump.Controls.Add(l);
                     flowLayoutPanelTestAssump.SetFlowBreak(l, true);


                     TextBox t = new TextBox();
                     flowLayoutPanelTestAssump.Controls.Add(t);
                     t.Width = 450;
                     t.Enabled = false;

                     if (gAssumption.VariableFormula == string.Empty || gAssumption.VariableFormula == null)
                         t.Enabled = true;

                     flowLayoutPanelTestAssump.SetFlowBreak(t, true);
                 }
             }
             BindTestDDl();
         }


         public void BindPatientPopulaitonDDl()
         {
             ddlpatientgroupPatient.DataSource = _selectedProgram.MMGroups;
          
         }

         public void BindProductDDl()
         {
             ddlpatientgroupProduct.DataSource = _selectedProgram.MMGroups;
         }

         public void BindTestDDl()
         {
             ddlpatientgroupTest.DataSource = _selectedProgram.MMGroups;
         }
    }

}
