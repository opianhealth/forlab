﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.CSharp;
using System.Reflection;
using System.IO;
using Microsoft.Reporting.WinForms;
using LQT.Core;
using LQT.Core.Util;
using LQT.GUI.Reports;
using LQT.Core.Domain;
namespace LQT.GUI.Quantification
{
    public partial class Productsummary : Form
    {
        SqlConnection con = ConnectionManager.GetInstance().GetSqlConnection();
        DataTable DTmonth = new DataTable();
        private ForecastInfo _mMforecastinfo;
        string onsite = "", _columnName = "", _MonthName = "", _variableName = "", varName = "", svarName = "", _type = "", sqlretrieve = "";
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mPMGeneralAssumption;
        List<string> listProductType = new List<string>(); 
        DataTable DT = new DataTable();
     //   DataTable DT = new DataTable();
        private FileInfo _fileToLoad;
        private string forecasttype = "";
        private int ID = 0;
        public int k = 0;
        private DataSet _rDataSet;
        DataTable dtParameterValue = new DataTable();
        DataTable dtGetValue = new DataTable();
        decimal _parameterSum = 0, _tparameterSum;
        decimal _PparameterSum = 0, _PtparameterSum;
        public Productsummary(int _forecastID, ForecastInfo finfo)
        {
            ID = _forecastID;
            InitializeComponent();

            dtGetValue.Columns.Add("VariableName", typeof(string));
            dtGetValue.Columns.Add("VariableEffect", typeof(string));
            dtGetValue.Columns.Add("VariableDataType", typeof(string));
            _mMforecastinfo = finfo;
            displaydata();
        }
        private void displaydata()
        {
            decimal existingtestnumber;
            decimal newpatienttestnumber;
            decimal Per;
            decimal testPermonth;
            decimal totaltest;
            decimal ttltest=0;
            int period = 0;
            if (con.State == ConnectionState.Open)
            { }
            else
            { con.Open(); }
            //con.Open();

            if (_mMforecastinfo.ForecastType == "S") _type = MorbidityVariableUsage.OnEachSite.ToString();
            else if (_mMforecastinfo.ForecastType == "C") _type = MorbidityVariableUsage.OnAggSite.ToString();
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Test_Assumption"), _mMforecastinfo.ProgramId, _type);

            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 3 && gAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtGetValue.NewRow();
                    dr["VariableName"] = gAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
                    dtGetValue.Rows.Add(dr);
                }
                dtGetValue.AcceptChanges();
            }



            SqlDataAdapter adapt = new SqlDataAdapter("SELECT *   FROM [TestByMonth] WHERE ForeCastID=" + ID + "   order by SNo", con);
            adapt.Fill(DTmonth);
           
            con.Close();
            period = Convert.ToInt32(DTmonth.Rows[0]["Duration"]) * 12;
            existingtestnumber = (Convert.ToDecimal(DTmonth.Rows[0]["ExistingPatient"]) * ( Convert.ToDecimal(DTmonth.Rows[0]["Duration"]) * Convert.ToDecimal(DTmonth.Rows[0]["TotalTestPerYear"])));
            //existingtestnumber = existingtestnumber / (Convert.ToDecimal(DTmonth.Rows.Count));
            existingtestnumber = existingtestnumber / (Convert.ToDecimal("12"));
            for (int i = 0; i < DTmonth.Rows.Count; i++)
            {
                //if (Convert.ToDouble(DTmonth.Rows[i]["tstNo"]) > 0)
                //{
                foreach (DataRow dr in dtGetValue.Rows)
                {
                    _parameterSum = ((Convert.ToDecimal(DTmonth.Rows[i]["" + dr["VariableName"].ToString() + ""])));
                    if (dr["variableEffect"].ToString() == "True") _tparameterSum = _tparameterSum + _parameterSum;
                    if (dr["variableEffect"].ToString() == "False") _tparameterSum = _tparameterSum - _parameterSum;
                }
                Per = _tparameterSum;
                //Per = Convert.ToDecimal(DTmonth.Rows[i]["TestRepeatPerYear"]) + Convert.ToDecimal(DTmonth.Rows[i]["SymptomTestPerYear"]);
                testPermonth = Convert.ToDecimal(DTmonth.Rows[i]["tstNo"]) * Convert.ToDecimal(DTmonth.Rows[i]["NewPatient"]);
                totaltest = testPermonth + existingtestnumber;
                //newpatienttestnumber = totaltest + (totaltest * (Per / 100));
                newpatienttestnumber = totaltest;
                decimal newpatienttestnumber1 = Math.Round(newpatienttestnumber, 2);
                ttltest = ttltest + newpatienttestnumber;
               // calculation(newpatienttestnumber, Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k);
                //calculation(Convert.ToDecimal(DTmonth.Rows[i]["tstNo"]), Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k);
                calculation(newpatienttestnumber1, Convert.ToInt32(DTmonth.Rows[i]["TestID"]), DTmonth.Rows[i]["Month"].ToString(), k, Convert.ToInt32(DTmonth.Rows[0]["Duration"]));
                k++;
                //}

            }
            FileInfo filinfo = null;
            List<ReportParameter> param = new List<ReportParameter>();
            //ReportParameter finfo = new ReportParameter("ForecastId", "1");
            //param.Add(finfo);
            //cmd.CommandType = CommandType.StoredProcedure;
            //cmd.CommandText = "spMorbiditySupplyForecast";
            //cmd.Parameters.AddWithValue("@ForecastId", _mforecast.Id);
            //_rDataSet = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(_rDataSet, "spMorbiditySupplyForecast");

            //con.Open();
            //SqlDataAdapter adapt = new SqlDataAdapter(str8, con);
            //adapt.Fill(Dtsiteworkingdays);
            DataSet _rDataSet = new DataSet();
            _rDataSet.Tables.Add(DT);
            //var  fg = from r in DT.AsEnumerable()
            //         group r by r.Field<int>("TypeName") into g
            //         select new
            //         {
            //             ProductId = g.Key,
            //             //CountThisWeek = g.Sum(r => r.Field<int>("totalquantityinpack")*("Price"))
            //             CountThisWeek = g.Sum(r => r.Field<int>("totalquantityinpack") * r.Field<int>("Price"))
            //         };

            var result = from row in DT.AsEnumerable()
                         group row by row.Field<string>("TypeName") into grp
                         select new
                         {
                             TeamID = grp.Key,
                             //MemberCount = grp.Sum(r => r.Field<decimal>("totalquantityinpack") * r.Field<decimal>("Price"))
                             MemberCount = grp.Sum(row =>row.Field<decimal>("totalCost"))
                         };



            foreach (var item in result)
            {
                string product = item.TeamID.ToString();
                string bn = item.MemberCount.ToString();
            }
           // _rDataSet = DT;
            con.Close();
            filinfo = new FileInfo(Path.Combine(AppSettings.GetReportPath, String.Format("{0}.rdlc", OReports.ProductsummaryReport)));
            _fileToLoad = filinfo;
            reportViewer1.LocalReport.ReportEmbeddedResource = "ProductsummaryReport.rdlc";
            
            string exeFolder = Path.GetDirectoryName(Application.ExecutablePath);
            string reportPath = Path.Combine(exeFolder, @"Reports\ProductsummaryReport.rdlc");

            reportViewer1.LocalReport.ReportPath = reportPath;

            reportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource RDS1 = new ReportDataSource("DataSet1", DT);
            reportViewer1.ProcessingMode = ProcessingMode.Local;
            reportViewer1.LocalReport.EnableExternalImages = true;
            reportViewer1.LocalReport.ReportEmbeddedResource = reportPath;
            reportViewer1.LocalReport.DataSources.Add(RDS1);









            //int yloc = this.Location.Y + 15;
            //int xloc = this.Location.X + 13;
            //int height = 500;
            //int width = 1000;
            //for (var i = 0; i < listProductType.Count; i++) {

            //    DataGrid Datagrid1 = new DataGrid();
            //    Datagrid1.Name = "Datagrid " + listProductType[i].ToString();
            //    Datagrid1.Text = "Datagrid " + listProductType[i].ToString();
            //    Datagrid1.Height = height;
            //    Datagrid1.Width = width;
            //    Datagrid1.Location = new Point(xloc, yloc);

            //    this.Controls.Add(Datagrid1);
            //    yloc = yloc + 300;
            // }

            //DataTable dt2 = GetInversedDataTable(DT, "month1", "ProductName", "Productneed", "-", true);
        }

        public DataTable Pivot(DataTable src,string VerticalColumnName,string HorizontalColumnName,string ValueColumnName)
        {

            DataTable dst = new DataTable();
            if (src == null || src.Rows.Count == 0)
                return dst;

            // find all distinct names for column and row
            ArrayList ColumnValues = new ArrayList();
            ArrayList RowValues = new ArrayList();
            foreach (DataRow dr in src.Rows)
            {
                // find all column values
                object column = dr[VerticalColumnName];
                if (!ColumnValues.Contains(column))
                    ColumnValues.Add(column);

                //find all row values
                object row = dr[HorizontalColumnName];
                if (!RowValues.Contains(row))
                    RowValues.Add(row);
            }

            ColumnValues.Sort();
            RowValues.Sort();

            //create columns
            dst = new DataTable();
            dst.Columns.Add(VerticalColumnName, src.Columns[VerticalColumnName].DataType);
            Type t = src.Columns[ValueColumnName].DataType;
            foreach (object ColumnNameInRow in RowValues)
            {
                dst.Columns.Add(ColumnNameInRow.ToString(), t);
            }

            //create destination rows
            foreach (object RowName in ColumnValues)
            {
                DataRow NewRow = dst.NewRow();
                NewRow[VerticalColumnName] = RowName.ToString();
                dst.Rows.Add(NewRow);
            }

            //fill out pivot table
            foreach (DataRow drSource in src.Rows)
            {
                object key = drSource[VerticalColumnName];
                string ColumnNameInRow = Convert.ToString(drSource[HorizontalColumnName]);
                int index = ColumnValues.IndexOf(key);
                var bv = dst.Rows[index][ColumnNameInRow];
                var bv1 = drSource[ValueColumnName];
                //dst.Rows[index][ColumnNameInRow] = sum(Convert.ToDecimal(dst.Rows[index][ColumnNameInRow]),Convert.ToDecimal(drSource[ValueColumnName]));

            }

            return dst;
        }
        public static DataTable GetInversedDataTable(DataTable table, string columnX,string columnY, string columnZ, string nullValue, bool sumValues)
        {
            //Create a DataTable to Return
            DataTable returnTable = new DataTable();
            decimal sum1= 0;
            decimal Totalcost =0;
            if (columnX == "")
                columnX = table.Columns[0].ColumnName;

            //Add a Column at the beginning of the table
            returnTable.Columns.Add(table.Columns[2].ColumnName);
            returnTable.Columns.Add(columnY);

          //  

            //Read all DISTINCT values from columnX Column in the provided DataTale
            List<string> columnXValues = new List<string>();

            foreach (DataRow dr in table.Rows)
            {

                string columnXTemp = dr[columnX].ToString();
                if (!columnXValues.Contains(columnXTemp))
                {
                    //Read each row value, if it's different from others provided, add to 
                    //the list of values and creates a new Column with its value.
                    columnXValues.Add(columnXTemp);
                    returnTable.Columns.Add(columnXTemp);
                }
            }
             returnTable.Columns.Add("Total Quantity need");
             returnTable.Columns.Add("Total Quantity in pack");
             returnTable.Columns.Add("Unit Cost");
             returnTable.Columns.Add("Total Cost");
             //Verify if Y and Z Axis columns re provided
            //Verify if Y and Z Axis columns re provided
            if (columnY != "" && columnZ != "")
            {
                //Read DISTINCT Values for Y Axis Column
                List<string> columnYValues = new List<string>();

                foreach (DataRow dr in table.Rows)
                {
                    if (!columnYValues.Contains(dr[columnY].ToString()))
                        columnYValues.Add(dr[columnY].ToString());
                }


                List<string> columnproducttypeValues = new List<string>();
                foreach (DataRow dr in table.Rows)
                {
                 ///   if (!columnproducttypeValues.Contains(dr[2].ToString()))
                        columnproducttypeValues.Add(dr[2].ToString());
                }
                //Loop all Column Y Distinct Value
          ///      foreach (string columnYValue in columnYValues)
                 for (var i = 0; i < columnYValues.Count; i++) 
                {
                    //Creates a new Row
                    DataRow drReturn = returnTable.NewRow();
                    drReturn[1] = columnYValues[i].ToString();
                    drReturn[0] = columnproducttypeValues[i].ToString();
                    //foreach column Y value, The rows are selected distincted
                    DataRow[] rows = table.Select(columnY + "='" + columnYValues[i].ToString() + "'");

                    //Read each row to fill the DataTable
                    foreach (DataRow dr in rows)
                    {
                        string rowColumnTitle = dr[columnX].ToString();

                        //Read each column to fill the DataTable
                        foreach (DataColumn dc in returnTable.Columns)
                        {
                            if (dc.ColumnName == rowColumnTitle)
                            {
                                //If Sum of Values is True it try to perform a Sum
                                //If sum is not possible due to value types, the value 
                                // displayed is the last one read
                                if (sumValues)
                                {
                                    try
                                    {
                                        drReturn[rowColumnTitle] =
                                             Convert.ToDecimal(drReturn[rowColumnTitle]) +
                                             Convert.ToDecimal(dr[columnZ]);
                                        sum1 = sum1 + Convert.ToDecimal(dr[columnZ]);
                                    }
                                    catch
                                    {
                                        drReturn[rowColumnTitle] = dr[columnZ];
                                        sum1 = sum1 + Convert.ToDecimal(dr[columnZ]);
                                    }
                                }
                                else
                                {
                                    drReturn[rowColumnTitle] = dr[columnZ];
                                    sum1 = sum1 + Convert.ToDecimal(dr[columnZ]);
                                }
                            }
                        }
                        drReturn["Total Quantity need"] = sum1;
                        drReturn["Total Quantity in pack"] = dr["Packsize"];
                        drReturn["Unit Cost"] = dr["Price"];
                        Totalcost = (sum1 / Convert.ToDecimal(dr["Packsize"])) * Convert.ToDecimal(dr["Price"]);
                        drReturn["Total Cost"] = Math.Round(Totalcost,2);
                     // sum1=0;
                    }
                   
                    returnTable.Rows.Add(drReturn);
                }
            }
            else
            {
                throw new Exception("The columns to perform inversion are not provided");
            }

            //if a nullValue is provided, fill the datable with it
            if (nullValue != "")
            {
                foreach (DataRow dr in returnTable.Rows)
                {
                    foreach (DataColumn dc in returnTable.Columns)
                    {
                        if (dr[dc.ColumnName].ToString() == "")
                            dr[dc.ColumnName] = nullValue;
                    }
                }
            }

            return returnTable;
        }
        //public decimal sum(decimal a, decimal b)
        //{
        //    //if (a is DBNull && b is DBNull)
        //    //    return 0;
        //    //else if (a is DBNull && !(b is DBNull))
        //    //    return b;
        //    //else if (!(a is DBNull) && b is DBNull)
        //    //    return a;
        //    //else
        //        return a + b;
        //}
        //dynamic sum(dynamic a, dynamic b)
        //{
        //    if (a is DBNull && b is DBNull)
        //        return DBNull.Value;
        //    else if (a is DBNull && !(b is DBNull))
        //        return b;
        //    else if (!(a is DBNull) && b is DBNull)
        //        return a;
        //    else
        //        return a + b;
        //}
        private void calculation(decimal testnumber,int testid,string Month,int j,int period)
        {
            string str8 = "";
            string strsites = "";
            string _sql = ""; string str1 = "";
            _sql = string.Format(@"Select Isnull([ForecastType],'') as forecasttype ,ForecastNo As Title  ,Right(Replace(convert(varchar(11), StartDate, 106),' ','-'),8) + '    '+ Right(Replace(convert(varchar(11), ForecastDate, 106),' ','-'),8) As forecastperiod from ForecastInfo Where [ForecastID]='{0}'", ID);
            //using (connection = new SqlConnection(con))
            //{
            DataTable DTProductneed = new DataTable();

            //SqlCommand cmd8 = new SqlCommand();
            //cmd8.CommandType = CommandType.Text;

            //cmd8.Connection = con;
            //cmd8.CommandText = _sql;
            //if (con.State == ConnectionState.Closed)
            //{
            //    con.Open();
            //}
            //forecasttype = Convert.ToString(cmd8.ExecuteScalar());

            //con.Close();
            DataTable dtPParameterValue = new DataTable();
            DataTable dtPGetValue = new DataTable();
            dtPGetValue.Columns.Add("VariableName", typeof(string));
            dtPGetValue.Columns.Add("VariableEffect", typeof(string));
            dtPGetValue.Columns.Add("VariableDataType", typeof(string));

            _mPMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Product_Assumption"), _mMforecastinfo.ProgramId, _type);

            sqlretrieve = "";


            foreach (MMGeneralAssumption gPAssumption in _mPMGeneralAssumption)
            {
                if (gPAssumption.AssumptionType == 2 && gPAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtPGetValue.NewRow();
                    sqlretrieve += "[" + gPAssumption.VariableName + "],";
                    dr["VariableName"] = gPAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gPAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gPAssumption.VariableDataType.ToString();
                    dtPGetValue.Rows.Add(dr);
                }
                dtPGetValue.AcceptChanges();
            }
            sqlretrieve = sqlretrieve.TrimEnd(',');
            string strSql = "select " + sqlretrieve + " from TestingAssumption  where ForecastinfoID=" + ID + "";
            SqlDataAdapter adapt2 = new SqlDataAdapter(strSql, con);
            adapt2.Fill(dtPParameterValue);

            for (int ki = 0; ki < dtPParameterValue.Rows.Count; ki++)
            {
                foreach (DataColumn dc in dtPParameterValue.Columns)
                {
                    foreach (DataRow dr in dtPGetValue.Rows)
                    {
                        string v1 = dr["VariableName"].ToString();
                        string v2 = dc.ColumnName.ToString();
                        if (v1.Equals(v2))
                        {
                            _PparameterSum = ((Convert.ToDecimal(dtPParameterValue.Rows[ki]["" + v2 + ""])) / 100);
                            if (dr["variableEffect"].ToString() == "True") _PtparameterSum = _PtparameterSum + _PparameterSum;
                            if (dr["variableEffect"].ToString() == "False") _PtparameterSum = _PtparameterSum - _PparameterSum;

                        }

                    }
                }

            }

            DataTable Dtforecastinfo = new DataTable();
            con.Open();
            SqlDataAdapter adaptinfo = new SqlDataAdapter(_sql, con);
            adaptinfo.Fill(Dtforecastinfo);

            con.Close();



            if ( Dtforecastinfo.Rows[0]["forecasttype"].ToString() == "S")
            {
                str8 = "select Avg(s.WorkingDays) as WorkingDays from ForecastSiteInfo FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastinfoID=" + ID + "";
                strsites = "select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + " ";
            }
            else
            {
                str8 = " SELECT Avg(s.WorkingDays) as WorkingDays  FROM [ForecastCategorySiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=" + ID + "";
                strsites = "select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + " ";
            }
            DataTable Dtsiteworkingdays = new DataTable();
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(str8, con);
            adapt.Fill(Dtsiteworkingdays);

            con.Close();

            con.Close();



            int _period = (period * 12) / 3;
            if (Dtforecastinfo.Rows[0]["forecasttype"].ToString() == "S")
            {
                str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
                str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
                str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
                str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate ";

                //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
                //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
                //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
                str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
                str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
                //str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
                str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
                str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
                str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS ";
                str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
                str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
                str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
                /////Control useage

                str1 += "	Union ";


                str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
                str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
                str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
                str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

                str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
                str1 += "	Union ";


                str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
                str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
                str1 += " when CU.Period='Yearly' then "+period+"*CU.UsageRate";
                str1 += " when CU.Period='Quarterly' then " + _period + "*CU.UsageRate";
                str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    from SiteInstrument where siteid  in (select SiteID from ForecastSiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
                str1 += "   group by InstrumentID),0) when CU.Period='Monthly' then (CU.UsageRate/12) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

                str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
                str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
                str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
                str1 += "   group by T.month1,T.ProductId ";
            }
            else
            {
                str1 = "select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1,";
                str1 += " Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,";
                str1 += " '" + Dtforecastinfo.Rows[0]["Title"].ToString() + "' as title,'" + Dtforecastinfo.Rows[0]["forecastperiod"].ToString() + "' as forecastperiod,";
                str1 += " Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate ";

                //str1 += " (sum(Productneed)/Max(PackSize)) as adjustedPacksize ,";
                //str1 += " (ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) as adjProgramgrowthrate,";
                //str1 += " ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) + ((ceiling(sum(Productneed)/Max(PackSize)) + ceiling(sum(Productneed)/Max(PackSize))*(Max(Isnull(ProgramGrowRate,0))/100)) *(Max(Isnull(WastageRate,0))/100))) as totalquantityinpack ";
                str1 += " from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,";
                str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
                //str1 += " " + testnumber + "  As testnumber, Cast(((PU.Rate*" + testnumber + ")) as numeric(18,2)) As Productneed ,'" + Month + "' as month1   FROM [ProductUsage] PU      Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId ";
                str1 += "  Left join MasterProduct mp on mp.ProductID=PU.ProductId Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  Left join  (select Max(T.testingArea) As testingArea,";
                str1 += "   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   (  SELECT SI.SiteID,TA.AreaName As testingArea ,";
                str1 += "  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastCategorySiteInfo] FS ";
                str1 += "    Left join SiteInstrument SI on SI.SiteID=FS.SiteID  Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID ";
                str1 += "	  Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=" + ID + ") As T Group by TestingAreaID,InstrumentID) ";
                str1 += "	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  where PU.TestId =" + testid + "	and PU.IsForControl=0 ";
                /////Control useage

                str1 += "	Union ";


                str1 += "select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId," + testnumber + " As testnumber, (Case when Ins.DailyCtrlTest>0 then " + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + " *1*pu.Rate*INS.DailyCtrlTest ";
                str1 += " when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest ";
                str1 += " when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest ";
                str1 += " when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,'" + Month + "' as month1     from ProductUsage PU  lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId ";

                str1 += " Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=" + testid + " ";
                str1 += "	Union ";


                str1 += "  select Mp.ProductID,MP.ProductName,Mc.TestId As TestId," + testnumber + " As testnumber,";
                str1 += "  (case when Pertest>0 and " + testnumber + " >0 then (" + testnumber + "/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*" + Convert.ToInt32(Dtsiteworkingdays.Rows[0]["WorkingDays"]) + "*CU.UsageRate ";
                str1 += " when CU.Period='Yearly' then " + period + "*CU.UsageRate";
                str1 += " when CU.Period='Quarterly' then " + _period + "*CU.UsageRate";
                str1 += "  when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    from SiteInstrument where siteid  in (select SiteID from ForecastCategorySiteInfo where ForecastinfoID=" + ID + ") and InstrumentID =Ins.InstrumentID ";
                str1 += "   group by InstrumentID),0) when CU.Period='Monthly' then (CU.UsageRate/12) else 0 end ) as productneed,'" + Month + "' as month1  from ConsumableUsage CU  Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  Left join MasterProduct MP on mp.ProductID=CU.ProductId  Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId ";

                str1 += "            where    Isnull(mc.TestId,'')=" + testid + ") As T Left join MasterProduct mp on mp.ProductID = T.ProductId  ";
                str1 += "  Left join ProductType PT on PT.TypeID = mp.ProductTypeID Left join Productprice pp on pp.ProductId=T.ProductId  ";
                str1 += " Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=" + ID + ")  as TA on TA.ProductTypeID=PT.TypeID ";
                str1 += "   group by T.month1,T.ProductId ";
            
            }
           con.Open();
           SqlDataAdapter adapt1 = new SqlDataAdapter(str1, con);
           adapt1.Fill(DTProductneed);
           //DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
           //for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
           //{
           //    DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

           //}
           //DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

           //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
           //{
           //    DTProductneed.Rows[i2]["totalquantityinpack"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(_PtparameterSum)))));
           //}

           //DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));
           //for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
           //{
           //    DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(0)));
           //}
           DTProductneed.Columns.Add("Adjustedpacksize", typeof(decimal));
           for (int i1 = 0; i1 < DTProductneed.Rows.Count; i1++)
           {
               DTProductneed.Rows[i1]["Adjustedpacksize"] = (Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i1]["Productneed"].ToString()) / Convert.ToDecimal(DTProductneed.Rows[i1]["Packsize"].ToString())));

           }
           DTProductneed.Columns.Add("adjProgramgrowthrate", typeof(decimal));

           for (int i2 = 0; i2 < DTProductneed.Rows.Count; i2++)
           {
               DTProductneed.Rows[i2]["adjProgramgrowthrate"] = Math.Ceiling((Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i2]["Adjustedpacksize"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i2]["ProgramGrowRate"].ToString()) / 100))));
           }
           DTProductneed.Columns.Add("totalquantityinpack", typeof(decimal));

           for (int i3 = 0; i3 < DTProductneed.Rows.Count; i3++)
           {
               DTProductneed.Rows[i3]["totalquantityinpack"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) + (Convert.ToDecimal(DTProductneed.Rows[i3]["adjProgramgrowthrate"].ToString()) * (Convert.ToDecimal(DTProductneed.Rows[i3]["WastageRate"].ToString()) / 100)));

           }
           DTProductneed.Columns.Add("totalCost", typeof(decimal));
           for (int i4 = 0; i4 < DTProductneed.Rows.Count; i4++)
           {
               DTProductneed.Rows[i4]["totalCost"] = Math.Ceiling(Convert.ToDecimal(DTProductneed.Rows[i4]["totalquantityinpack"].ToString()) * Convert.ToDecimal(DTProductneed.Rows[i4]["Price"].ToString()));

           }

           con.Close();
            /////consumble  useage
           if (j == 0)
           {
               DT = DTProductneed;
           }
           else
           {
              // for (int i = 0; i < DTProductneed.Rows.Count; i++)
              // {
              //     DataRow[] rows = DT.Select("ProductId='" + DTProductneed.Rows[i]["ProductId"] + "'");
                       
              //     if (rows.Length > 0)
              //     {
              //         rows[0]["Productneed"] = "";
                  
              //     }
              //}
               foreach (DataRow dr in DTProductneed.Rows)
               {
                   DataRow[] rows = DT.Select("ProductId='" + dr["ProductId"] + "' and month1='" + dr["month1"] + "'");
                   if (rows.Length > 0)
                   {
                       rows[0]["Productneed"] = Convert.ToDecimal(rows[0]["Productneed"]) + Convert.ToDecimal(dr["Productneed"]);
                       rows[0]["Adjustedpacksize"] = Convert.ToDecimal(rows[0]["Adjustedpacksize"]) + Convert.ToDecimal(dr["Adjustedpacksize"]);
                       rows[0]["adjProgramgrowthrate"] = Convert.ToDecimal(rows[0]["adjProgramgrowthrate"]) + Convert.ToDecimal(dr["adjProgramgrowthrate"]);
                       rows[0]["totalquantityinpack"] = Convert.ToDecimal(rows[0]["totalquantityinpack"]) + Convert.ToDecimal(dr["totalquantityinpack"]);
                       rows[0]["totalCost"] = Convert.ToDecimal(rows[0]["totalCost"]) + Convert.ToDecimal(dr["totalCost"]);
                   }
                   else
                   {
                       DT.ImportRow(dr);
                   }
               }
           }
        //   DTProductneed = null;

        }

        private void Productsummary_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
