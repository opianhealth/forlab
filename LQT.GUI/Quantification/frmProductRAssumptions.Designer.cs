﻿namespace LQT.GUI.Quantification
{
    partial class frmProductRAssumptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductRAssumptions));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lstTestingArea = new System.Windows.Forms.ListBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gvPAssumptions = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ForecastinfoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProducttypeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WastageRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProgramGrowRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AvgBloodDrawPerYearE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AvgBloodDrawPerYearN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtNPYear = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEPYear = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtPGRate = new System.Windows.Forms.TextBox();
            this.txtWRate = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPAssumptions)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(3, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 480);
            this.panel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Product Type";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.lstTestingArea);
            this.panel4.Location = new System.Drawing.Point(8, 24);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(407, 141);
            this.panel4.TabIndex = 2;
            // 
            // lstTestingArea
            // 
            this.lstTestingArea.FormattingEnabled = true;
            this.lstTestingArea.Location = new System.Drawing.Point(4, 12);
            this.lstTestingArea.Name = "lstTestingArea";
            this.lstTestingArea.Size = new System.Drawing.Size(398, 121);
            this.lstTestingArea.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gvPAssumptions);
            this.panel3.Location = new System.Drawing.Point(3, 171);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1054, 286);
            this.panel3.TabIndex = 1;
            // 
            // gvPAssumptions
            // 
            this.gvPAssumptions.AllowUserToAddRows = false;
            this.gvPAssumptions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvPAssumptions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPAssumptions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ForecastinfoID,
            this.ProducttypeID,
            this.ProductType,
            this.WastageRate,
            this.ProgramGrowRate,
            this.AvgBloodDrawPerYearE,
            this.AvgBloodDrawPerYearN});
            this.gvPAssumptions.Location = new System.Drawing.Point(0, 4);
            this.gvPAssumptions.Name = "gvPAssumptions";
            this.gvPAssumptions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPAssumptions.Size = new System.Drawing.Size(1049, 277);
            this.gvPAssumptions.TabIndex = 0;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // ForecastinfoID
            // 
            this.ForecastinfoID.DataPropertyName = "ForecastinfoID";
            this.ForecastinfoID.HeaderText = "ForecastinfoID";
            this.ForecastinfoID.Name = "ForecastinfoID";
            this.ForecastinfoID.Visible = false;
            // 
            // ProducttypeID
            // 
            this.ProducttypeID.DataPropertyName = "ProducttypeID";
            this.ProducttypeID.HeaderText = "ProducttypeID";
            this.ProducttypeID.Name = "ProducttypeID";
            this.ProducttypeID.Visible = false;
            // 
            // ProductType
            // 
            this.ProductType.DataPropertyName = "ProductType";
            this.ProductType.HeaderText = "Product Type";
            this.ProductType.Name = "ProductType";
            this.ProductType.ReadOnly = true;
            // 
            // WastageRate
            // 
            this.WastageRate.DataPropertyName = "WastageRate";
            this.WastageRate.HeaderText = "Wastage Rate";
            this.WastageRate.Name = "WastageRate";
            // 
            // ProgramGrowRate
            // 
            this.ProgramGrowRate.DataPropertyName = "ProgramGrowRate";
            this.ProgramGrowRate.HeaderText = "Program growth rate during the forcasting period";
            this.ProgramGrowRate.Name = "ProgramGrowRate";
            // 
            // AvgBloodDrawPerYearE
            // 
            this.AvgBloodDrawPerYearE.DataPropertyName = "AvgBloodDrawPerYearE";
            this.AvgBloodDrawPerYearE.HeaderText = "Avg # of blood draws per existing patient per year";
            this.AvgBloodDrawPerYearE.Name = "AvgBloodDrawPerYearE";
            // 
            // AvgBloodDrawPerYearN
            // 
            this.AvgBloodDrawPerYearN.DataPropertyName = "AvgBloodDrawPerYearN";
            this.AvgBloodDrawPerYearN.HeaderText = "Avg # of blood draws per new patient per year";
            this.AvgBloodDrawPerYearN.Name = "AvgBloodDrawPerYearN";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtNPYear);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtEPYear);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.txtPGRate);
            this.panel2.Controls.Add(this.txtWRate);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(440, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(613, 141);
            this.panel2.TabIndex = 0;
            // 
            // txtNPYear
            // 
            this.txtNPYear.Location = new System.Drawing.Point(394, 80);
            this.txtNPYear.Name = "txtNPYear";
            this.txtNPYear.Size = new System.Drawing.Size(72, 20);
            this.txtNPYear.TabIndex = 8;
            this.txtNPYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNPYear_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(83, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(306, 15);
            this.label5.TabIndex = 7;
            this.label5.Text = "Avg # of blood draws per new patient per year :";
            // 
            // txtEPYear
            // 
            this.txtEPYear.Location = new System.Drawing.Point(394, 55);
            this.txtEPYear.Name = "txtEPYear";
            this.txtEPYear.Size = new System.Drawing.Size(74, 20);
            this.txtEPYear.TabIndex = 6;
            this.txtEPYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCD4_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(87, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(302, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Avg # of blood draws per existing patient per year  :";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(393, 106);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtPGRate
            // 
            this.txtPGRate.Location = new System.Drawing.Point(394, 31);
            this.txtPGRate.Name = "txtPGRate";
            this.txtPGRate.Size = new System.Drawing.Size(74, 20);
            this.txtPGRate.TabIndex = 3;
            this.txtPGRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRation_KeyPress);
            // 
            // txtWRate
            // 
            this.txtWRate.Location = new System.Drawing.Point(394, 7);
            this.txtWRate.Name = "txtWRate";
            this.txtWRate.Size = new System.Drawing.Size(74, 20);
            this.txtWRate.TabIndex = 2;
            this.txtWRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPGN_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(93, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(296, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Program growth rate during the forecasting period :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(293, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Wastage Rate :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Patient related assumptions per site or category";
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Image")));
            this.btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrevious.Location = new System.Drawing.Point(11, 499);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(112, 23);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(1000, 499);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "Forecast";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // frmProductRAssumptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 534);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProductRAssumptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Patient Assumptions";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPAssumptions)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtPGRate;
        private System.Windows.Forms.TextBox txtWRate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtEPYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ListBox lstTestingArea;
        private System.Windows.Forms.TextBox txtNPYear;
        private System.Windows.Forms.DataGridView gvPAssumptions;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ForecastinfoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProducttypeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductType;
        private System.Windows.Forms.DataGridViewTextBoxColumn WastageRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProgramGrowRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn AvgBloodDrawPerYearE;
        private System.Windows.Forms.DataGridViewTextBoxColumn AvgBloodDrawPerYearN;
    }
}