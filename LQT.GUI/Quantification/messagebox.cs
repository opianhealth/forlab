﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Location;

namespace LQT.GUI.Quantification
{
    public partial class messagebox : Form
    {
        private string message1 = "";
        private string siteids1 = "";
        private Form _mdiparent;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        string _sql = "";
        SqlCommand cmdForecast = null;
        DataTable dtSite = new DataTable();
        private int _selectedSiteId = 0;
        public messagebox(string message,string siteids,Form mdiform)
        {
            message1 = message;
            siteids1 = siteids;
            _mdiparent = mdiform;
            InitializeComponent();
            bindlistview();
            label1.Text = message1;
    
        }
        private void bindlistview()
        {
            listView1.BeginUpdate();
            listView1.Items.Clear();
            _sql = "select * from site where SiteID In (" + siteids1 + ")";
            using (cmdForecast = new SqlCommand("", connection))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                cmdForecast.CommandText = _sql;
                SqlDataAdapter da = new SqlDataAdapter(cmdForecast);
                da.Fill(dtSite);
                if (dtSite.Rows.Count > 0)
                {


                    foreach (DataRow dr1 in dtSite.Rows)
                    {
                        ListViewItem li = new ListViewItem(dr1["SiteName"].ToString()) { Tag = Convert.ToInt32(dr1["SiteID"].ToString()) };

                        listView1.Items.Add(li);
                    }

                    listView1.EndUpdate();


                    dtSite.Rows.Clear();
                }
                if (connection.State == ConnectionState.Open) connection.Close();

                label2.Text = "List of sites";
            }
        
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Height = 411;
            panel1.Visible = true;
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            SiteForm frm = new SiteForm(GetSelectedSite(), _mdiparent);
            frm.ShowDialog();
        }
        private ForlabSite GetSelectedSite()
        {
            return DataRepository.GetSiteById(_selectedSiteId);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                int id = (int)listView1.SelectedItems[0].Tag;
                if (id != _selectedSiteId)
                {
                    _selectedSiteId = id;

                }
            }
        }
    }
}
