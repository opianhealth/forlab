﻿namespace LQT.GUI.Quantification
{
    partial class PatientAssumptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatientAssumptions));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gvPAssumptions = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ForecastinfoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SiteCategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HIVPositivePer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HIVPerWithoutFollow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ReceiveCD4Per = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCD4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.txtHIVPD = new System.Windows.Forms.TextBox();
            this.txtHIVP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvPAssumptions)).BeginInit();
            this.panel2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(3, 13);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1072, 475);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.gvPAssumptions);
            this.panel3.Location = new System.Drawing.Point(15, 157);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1039, 306);
            this.panel3.TabIndex = 1;
            // 
            // gvPAssumptions
            // 
            this.gvPAssumptions.AllowUserToAddRows = false;
            this.gvPAssumptions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvPAssumptions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvPAssumptions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.ForecastinfoID,
            this.CategoryID,
            this.SiteCategoryName,
            this.HIVPositivePer,
            this.HIVPerWithoutFollow,
            this.ReceiveCD4Per});
            this.gvPAssumptions.Location = new System.Drawing.Point(4, 4);
            this.gvPAssumptions.Name = "gvPAssumptions";
            this.gvPAssumptions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvPAssumptions.Size = new System.Drawing.Size(1030, 297);
            this.gvPAssumptions.TabIndex = 0;
            this.gvPAssumptions.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // ForecastinfoID
            // 
            this.ForecastinfoID.DataPropertyName = "ForecastinfoID";
            this.ForecastinfoID.HeaderText = "ForecastinfoID";
            this.ForecastinfoID.Name = "ForecastinfoID";
            this.ForecastinfoID.Visible = false;
            // 
            // CategoryID
            // 
            this.CategoryID.DataPropertyName = "CategoryID";
            this.CategoryID.HeaderText = "CategoryID";
            this.CategoryID.Name = "CategoryID";
            this.CategoryID.Visible = false;
            // 
            // SiteCategoryName
            // 
            this.SiteCategoryName.DataPropertyName = "SiteCategoryName";
            this.SiteCategoryName.FillWeight = 118.6548F;
            this.SiteCategoryName.HeaderText = "Site/Category Name";
            this.SiteCategoryName.Name = "SiteCategoryName";
            this.SiteCategoryName.ReadOnly = true;
            // 
            // HIVPositivePer
            // 
            this.HIVPositivePer.DataPropertyName = "HIVPositivePer";
            this.HIVPositivePer.FillWeight = 118.6548F;
            this.HIVPositivePer.HeaderText = "% Of patient population HIV positive";
            this.HIVPositivePer.MaxInputLength = 5;
            this.HIVPositivePer.Name = "HIVPositivePer";
            // 
            // HIVPerWithoutFollow
            // 
            this.HIVPerWithoutFollow.DataPropertyName = "HIVPerWithoutFollow";
            this.HIVPerWithoutFollow.FillWeight = 118.6548F;
            this.HIVPerWithoutFollow.HeaderText = "% Of HIV positive diognosis to depart without follow-up";
            this.HIVPerWithoutFollow.MaxInputLength = 5;
            this.HIVPerWithoutFollow.Name = "HIVPerWithoutFollow";
            // 
            // ReceiveCD4Per
            // 
            this.ReceiveCD4Per.DataPropertyName = "ReceiveCD4Per";
            this.ReceiveCD4Per.FillWeight = 118.6548F;
            this.ReceiveCD4Per.HeaderText = "% Of HIV positive diognosis that follow-up which receive CD4";
            this.ReceiveCD4Per.MaxInputLength = 5;
            this.ReceiveCD4Per.Name = "ReceiveCD4Per";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtCD4);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btnAdd);
            this.panel2.Controls.Add(this.txtHIVPD);
            this.panel2.Controls.Add(this.txtHIVP);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(15, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1039, 124);
            this.panel2.TabIndex = 0;
            // 
            // txtCD4
            // 
            this.txtCD4.Location = new System.Drawing.Point(571, 67);
            this.txtCD4.Name = "txtCD4";
            this.txtCD4.Size = new System.Drawing.Size(74, 20);
            this.txtCD4.TabIndex = 6;
            this.txtCD4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCD4_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(198, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(367, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "% Of HIV positive diognosis that follow-up which receive CD4  :";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(570, 93);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtHIVPD
            // 
            this.txtHIVPD.Location = new System.Drawing.Point(571, 38);
            this.txtHIVPD.Name = "txtHIVPD";
            this.txtHIVPD.Size = new System.Drawing.Size(74, 20);
            this.txtHIVPD.TabIndex = 3;
            this.txtHIVPD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRation_KeyPress);
            // 
            // txtHIVP
            // 
            this.txtHIVP.Location = new System.Drawing.Point(571, 8);
            this.txtHIVP.Name = "txtHIVP";
            this.txtHIVP.Size = new System.Drawing.Size(74, 20);
            this.txtHIVP.TabIndex = 2;
            this.txtHIVP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPGN_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(239, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(326, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "% Of HIV positive diognosis to depart without follow-up :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(346, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "% Of patient population HIV positive :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Patient related assumptions per site or category";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRecordToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(148, 26);
            // 
            // deleteRecordToolStripMenuItem
            // 
            this.deleteRecordToolStripMenuItem.Name = "deleteRecordToolStripMenuItem";
            this.deleteRecordToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.deleteRecordToolStripMenuItem.Text = "Delete Record";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.FillWeight = 25.38071F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 38;
            // 
            // btnNext
            // 
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Image = ((System.Drawing.Image)(resources.GetObject("btnNext.Image")));
            this.btnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNext.Location = new System.Drawing.Point(1000, 499);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 3;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevious.Image")));
            this.btnPrevious.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrevious.Location = new System.Drawing.Point(12, 499);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(112, 23);
            this.btnPrevious.TabIndex = 2;
            this.btnPrevious.Text = "Previous";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // PatientAssumptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 534);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatientAssumptions";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Patient Assumptions";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvPAssumptions)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox txtHIVPD;
        private System.Windows.Forms.TextBox txtHIVP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TextBox txtCD4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView gvPAssumptions;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteRecordToolStripMenuItem;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ForecastinfoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SiteCategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn HIVPositivePer;
        private System.Windows.Forms.DataGridViewTextBoxColumn HIVPerWithoutFollow;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReceiveCD4Per;
    }
}