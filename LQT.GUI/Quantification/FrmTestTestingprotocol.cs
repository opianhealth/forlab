﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Util;
using LQT.Core.Domain;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;
using LQT.Core;
using System.Text.RegularExpressions;

namespace LQT.GUI.Quantification
{
    public partial class FrmTestTestingprotocol : Form
    {
       private int _selectedtestareaId=0;
        private int _forecastId = 0;
        private Form _mdiparent;
        private string _mode = "";
        DataTable Dtpatient1 = new DataTable();
        DataTable dt1 = new DataTable();
        private string _startdate = "";
        private string _enddate = "";
        private string _period = "";
    //    private bool Isvalid = true;
        public static Int64 _totalTarget = 0;
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;

        List<string> Lstmonthname = new List<string>();
       // string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);

        public FrmTestTestingprotocol(Form mdiparent,int forecastid, string mode, DataTable Dtpatient, string startdate, string enddate, string period, Int64 totaltarget, DataTable Dt)
        {
            InitializeComponent();
            this._forecastId = forecastid;
            this._mode = mode;
            this.Dtpatient1 = Dtpatient;

            _mdiparent=mdiparent;
            _totalTarget = totaltarget;
          
            this._startdate = startdate;
            this._enddate = enddate;
            this._period = period;

            //  _selectedForcastId = 55;
            this.dt1 = Dt;
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;
            if (Dt.Rows.Count > 0)
            {
                //table.Columns.Add("Patientgroupid", typeof(int));
                //table.Columns.Add("Patientgroupname", typeof(string));
                //table.Columns.Add("Patientpercentage", typeof(double));
                if (Dtpatient.Columns.Count == 2)
                {
                Dtpatient.Columns.Add("ID");
                Dtpatient.Columns.Add("TestID");
                

                Dtpatient.Columns.Add("ColPerPatient");
                Dtpatient.Columns.Add("ColBaseline");
                Dtpatient.Columns.Add("ColM1");
                Dtpatient.Columns.Add("ColM2");
                Dtpatient.Columns.Add("ColM3");
                Dtpatient.Columns.Add("ColM4");
                Dtpatient.Columns.Add("ColM5");
                Dtpatient.Columns.Add("ColM6");
                Dtpatient.Columns.Add("ColM7");
                Dtpatient.Columns.Add("ColM8");
                Dtpatient.Columns.Add("ColM9");
                Dtpatient.Columns.Add("ColM10");
                Dtpatient.Columns.Add("ColM11");
                Dtpatient.Columns.Add("ColM12");

                Dtpatient.Columns.Add("ColTestperyear");
                Dtpatient.Columns.Add("Colpertestrepeat");
                Dtpatient.Columns.Add("colpersymptomtest");
            }

                dataGridView1.Columns["ID"].DataPropertyName = "ID";
                dataGridView1.Columns["TestID"].DataPropertyName = "TestID";
                dataGridView1.Columns["ColpatientgroupID"].DataPropertyName = "Patientgroupid";
                dataGridView1.Columns["ColPatientgroup"].DataPropertyName = "Patientgroupname";
                dataGridView1.Columns["ColPerPatient"].DataPropertyName = "ColPerPatient";
                dataGridView1.Columns["ColBaseline"].DataPropertyName = "ColBaseline";
                dataGridView1.Columns["ColM1"].DataPropertyName = "ColM1";
                dataGridView1.Columns["ColM2"].DataPropertyName = "ColM2";



                dataGridView1.Columns["ColM3"].DataPropertyName = "ColM3";
                dataGridView1.Columns["ColM4"].DataPropertyName = "ColM4";


                dataGridView1.Columns["ColM5"].DataPropertyName = "ColM5";
                dataGridView1.Columns["ColM6"].DataPropertyName = "ColM6";

                dataGridView1.Columns["ColM7"].DataPropertyName = "ColM7";
                dataGridView1.Columns["ColM8"].DataPropertyName = "ColM8";

                dataGridView1.Columns["ColM9"].DataPropertyName = "ColM9";
                dataGridView1.Columns["ColM10"].DataPropertyName = "ColM10";
                dataGridView1.Columns["ColM11"].DataPropertyName = "ColM11";
                dataGridView1.Columns["ColM12"].DataPropertyName = "ColM12";
                dataGridView1.Columns["ColTestperyear"].DataPropertyName = "ColTestperyear";
                dataGridView1.Columns["Colpertestrepeat"].DataPropertyName = "Colpertestrepeat";
                dataGridView1.Columns["colpersymptomtest"].DataPropertyName = "colpersymptomtest";
        //  dataGridView1.Columns[3].DataPropertyName = "Patientpercentage";
               
                dataGridView1.DataSource = Dtpatient;
                dataGridView1.Columns["ID"].Visible = false;
            
            }

            btnadd.TabStop = false;
            btnadd.FlatStyle = FlatStyle.Flat;
            btnadd.FlatAppearance.BorderSize = 0;
            btnadd.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnRemove.TabStop = false;
            btnRemove.FlatStyle = FlatStyle.Flat;
            btnRemove.FlatAppearance.BorderSize = 0;
            btnRemove.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            button2.TabStop = false;
            button2.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            button2.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            GetSelectedtest(_forecastId);
            PopTestingAreas();
        }
        private void GetSelectedtest(int ID)
        {
            string _sql = "";
            DataTable dt = new DataTable();
            try
            {
                if (ID > 0)
                    _sql = string.Format(@"select distinct  tp.TestID As testID,ts.TestName,tp.ForecastinfoID from testingprotocol tp
Left Join test ts on ts.TestID =tp.TestID
where ForecastinfoID = {0}", ID);
              ///  else _sql = string.Format(@"SELECT [ID] ,[ForecastinfoID],[PatientGroupName] ,[PatientPercentage],[PatientRatio] FROM [PatientGroup] WHERE 1 = 0");
                //using (connection = new SqlConnection(con))
                //{
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        connection.Open();
                        daForecast = new SqlDataAdapter(cmdForecast);
                        daForecast.Fill(dt);
                      
                    }

                    if (dt.Rows.Count > 0)
                   {
                        int testid1 = 0;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            ListViewItem li = new ListViewItem(dt.Rows[i]["TestName"].ToString()) { Tag = Convert.ToInt32(dt.Rows[i]["testID"].ToString())};
                            lstSelectTest.Items.Add(li);
                        

                        }
                            testid1 = Convert.ToInt32(dt.Rows[0]["testID"].ToString());
                            DataTable Dt1 = new DataTable();
                            if (connection.State == ConnectionState.Open)
                            { }
                            else
                            { connection.Open(); }
                            using (cmdForecast = new SqlCommand("", connection))
                            {

                                cmdForecast.CommandType = CommandType.Text;
                                using (SqlDataAdapter sda = new SqlDataAdapter(cmdForecast))
                                {


                                    foreach (DataGridViewRow row in dataGridView1.Rows)
                                    {
                                        _sql = string.Format(@"SELECT * from [TestingProtocol]   where [ForecastinfoID]={0} and [TestID]={1} and PatientGroupID={2}", _forecastId, testid1, Convert.ToInt32(row.Cells["ColpatientgroupID"].Value));
                                        cmdForecast.CommandText = _sql;
                                        sda.Fill(Dt1);
                                        if (Dt1.Rows.Count > 0)
                                        {
                                            row.Cells["ID"].Value = Dt1.Rows[0]["ID"].ToString();
                                            row.Cells["TestID"].Value = Dt1.Rows[0]["TestID"].ToString();
                                            row.Cells["ColPerPatient"].Value = Convert.ToInt32(Dt1.Rows[0]["PercentagePanel"]);
                                            row.Cells["ColBaseline"].Value = Convert.ToInt32(Dt1.Rows[0]["Baseline"]);

                                            row.Cells["ColM1"].Value = Convert.ToInt32(Dt1.Rows[0]["Month1"]);
                                            row.Cells["ColM2"].Value = Convert.ToInt32(Dt1.Rows[0]["Month2"]);
                                            row.Cells["ColM3"].Value = Convert.ToInt32(Dt1.Rows[0]["Month3"]);
                                            row.Cells["ColM4"].Value = Convert.ToInt32(Dt1.Rows[0]["Month4"]);

                                            row.Cells["ColM5"].Value = Convert.ToInt32(Dt1.Rows[0]["Month5"]);
                                            row.Cells["ColM6"].Value = Convert.ToInt32(Dt1.Rows[0]["Month6"]);
                                            row.Cells["ColM7"].Value = Convert.ToInt32(Dt1.Rows[0]["Month7"]);
                                            row.Cells["ColM8"].Value = Convert.ToInt32(Dt1.Rows[0]["Month8"]);

                                            row.Cells["ColM9"].Value = Convert.ToInt32(Dt1.Rows[0]["Month9"]);
                                            row.Cells["ColM10"].Value = Convert.ToInt32(Dt1.Rows[0]["Month10"]);
                                            row.Cells["ColM11"].Value = Convert.ToInt32(Dt1.Rows[0]["Month11"]);
                                            row.Cells["ColM12"].Value = Convert.ToInt32(Dt1.Rows[0]["Month12"]);

                                            row.Cells["ColTestperyear"].Value = Convert.ToInt32(Dt1.Rows[0]["TotalTestPerYear"]);
                                            row.Cells["Colpertestrepeat"].Value = Convert.ToInt32(Dt1.Rows[0]["TestRepeatPerYear"]);
                                            row.Cells["colpersymptomtest"].Value = Convert.ToInt32(Dt1.Rows[0]["SymptomTestPerYear"]);


                                        }
                                        //else
                                        //{
                                        //    row.Cells["TestID"].Value = id;
                                        //    row.Cells["ID"].Value = "0";

                                        //    row.Cells["ColPerPatient"].Value = "0";
                                        //    row.Cells["ColBaseline"].Value = "0";

                                        //    row.Cells["ColM1"].Value = "0";
                                        //    row.Cells["ColM2"].Value = "0";
                                        //    row.Cells["ColM3"].Value = "0";
                                        //    row.Cells["ColM4"].Value = "0";

                                        //    row.Cells["ColM5"].Value = "0";
                                        //    row.Cells["ColM6"].Value = "0";
                                        //    row.Cells["ColM7"].Value = "0";
                                        //    row.Cells["ColM8"].Value = "0";

                                        //    row.Cells["ColM9"].Value = "0";
                                        //    row.Cells["ColM10"].Value = "0";
                                        //    row.Cells["ColM11"].Value = "0";
                                        //    row.Cells["ColM12"].Value = "0";

                                        //    row.Cells["ColTestperyear"].Value = "0";
                                        //    row.Cells["Colpertestrepeat"].Value = "0";
                                        //    row.Cells["colpersymptomtest"].Value = "0";
                                        //}
                                        Dt1.Clear();
                                    }
                                    dataGridView1.Columns[0].Visible = false;
                                }


                            }

                            connection.Close();
                            // }
                            lbltest.Text = "Testing Protocol : " + dt.Rows[0]["TestName"].ToString();

                          //  lstSelectTest.Items[0].Selected = true;
                          //  lstSelectTest_SelectedIndexChanged();
                        }
                        connection.Close();
              
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }
        private void PopTestingAreas()
        {
            lsttestarea.BeginUpdate();
            lsttestarea.Items.Clear();

            foreach (TestingArea ta in DataRepository.GetAllTestingArea())
            {
                ListViewItem li = new ListViewItem(ta.AreaName) { Tag = ta.Id };

                lsttestarea.Items.Add(li);
            }

            lsttestarea.EndUpdate();


        }
          
        private void lsttestarea_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lsttestarea.SelectedItems.Count > 0)
            {
                 int id = (int)lsttestarea.SelectedItems[0].Tag;
                if (id != _selectedtestareaId)
                {
                    _selectedtestareaId = id;
                    lsttest.BeginUpdate();
                    lsttest.Items.Clear();

                        foreach (Test tg in DataRepository.GetAllTestsByAreaId(_selectedtestareaId))
                        {
                            ListViewItem li = new ListViewItem(tg.TestName) { Tag = tg.Id };
                            li.SubItems.Add(tg.TestingArea.AreaName);
                            //li.SubItems.Add(tg.TestingGroup.GroupName);
                            //if (tg.Id == _selectedTestId)
                            //{
                            //    li.Selected = true;
                            //}
                            lsttest.Items.Add(li);
                        }

                  lsttest.EndUpdate();
                  //this.button3_Click(null, null);
                }

             }
            }

        private void btnadd_Click(object sender, EventArgs e)
        {
            bool isduplicate = true;
            string testname = "";
            bool isValid = false;
            string testIds = "";
            string _sql = "", _siteids = "", _ids = "";
            DataTable dtSite = new DataTable();
            foreach (ListViewItem items in lsttest.CheckedItems)
            {
                testIds += items.Tag + ",";
            }
            using (cmdForecast = new SqlCommand("", connection))
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                foreach (DataRow dr in dt1.Rows)
                {
                    string name = dr["Name"].ToString().ToLower().Replace(" ", "");

                    _sql = "Select ForecastType from ForecastInfo where ForecastID=" + _forecastId + "";
                    cmdForecast.CommandText = _sql;
                    string type = Convert.ToString(cmdForecast.ExecuteScalar());
                    if (type == "S")
                    {
                       // dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                        _sql = "Select SiteID from Site Where SiteName like('" + dr["Name"].ToString().Replace("'", @"''") + "') ";
                        cmdForecast.CommandText = _sql;
                        _ids = Convert.ToString(cmdForecast.ExecuteScalar());
                        _siteids += _ids + ",";
                    }
                    else if (type == "C")
                    {
                        _sql = "select SiteID from ForecastCategorySiteInfo where CategoryID IN(Select CategoryID from SiteCategory Where CategoryName like('" + dr["Name"].ToString() + "'))And ForecastInfoID=" + _forecastId + " ";
                        cmdForecast.CommandText = _sql;
                        SqlDataAdapter da = new SqlDataAdapter(cmdForecast);
                        da.Fill(dtSite);
                        if (dtSite.Rows.Count > 0)
                        {
                            foreach (DataRow dr1 in dtSite.Rows)
                            {
                                _siteids += Convert.ToString(dr1["SiteID"]) + ",";
                            }
                            dtSite.Rows.Clear();
                        }
                    }

                }

                _siteids = _siteids.TrimEnd(',');
                string existsites = "";
                foreach (ListViewItem items in lsttest.CheckedItems)
                {
                    if (_siteids != "")
                    {
                        string[] result = _siteids.Split(',');
                        for (int i = 0; i < result.Length; i++)
                        {
                            if (!result[i].ToString().Equals(""))
                            _sql = string.Format("select * from SiteInstrument SI Left join Instrument ins on ins.InstrumentID=SI.InstrumentID where ins.TestingAreaID ={0} and SI.SiteID = {1}  ",lsttestarea.SelectedItems[0].Tag, result[i].ToString());
                            cmdForecast.CommandText = _sql;
                            int count = Convert.ToInt32(cmdForecast.ExecuteScalar());
                            testname = items.Text;
                            if (count > 0)
                            {
                                //////foreach (ListViewItem items1 in lstSelectTest.Items)
                                //////{

                                //////    if (Convert.ToString(items1.Tag) == Convert.ToString(items.Tag))
                                //////    {
                                //////        isduplicate = false;
                                //////        // testname = items.Text;
                                //////        break;
                                //////    }
                                //////    else
                                //////    {
                                //////        isduplicate = true;
                                //////    }
                                //////}
                                //////if (isduplicate == false)
                                //////{
                                //////    MessageBox.Show(testname + " already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //////    //return;
                                //////}
                                //////else
                                //////{
                                //////    ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };

                                //////    lstSelectTest.Items.Add(li);
                                //////}

                            }
                            else
                            {
                                existsites = existsites + "," + result[i].ToString();
                               // MessageBox.Show("Site doesn't have the necessary instrument for " + testname + "", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                // MessageBox.Show(testname + " Have not necessary Instrument.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                ///sites doesn't have CD4 instrument 
                            }
                        }

                        if (existsites != "")
                        {
                            existsites = existsites.TrimStart(',');
                            ////DialogResult dialog = MessageBox.Show("Some Site doesn't have the necessary instrument for " + testname + "", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ////if (dialog == DialogResult.OK)
                            ////{
                            ////}
                            messagebox frmmsg = new messagebox("Some Site doesn't have the necessary instrument for " + testname + "", existsites, _mdiparent);
                            frmmsg.ShowDialog();
                        }




                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                            _sql = string.Format("select ID from (select count(InstrumentID) AS ID from [ConsumableUsage] where [ConsumableId] in ( \n" +
                                                  "select MasterCID from [MasterConsumable] where TestId IN ({0})) And [InstrumentID] in ( \n" +
                                                  "select [InstrumentID] from [SiteInstrument] where SiteID IN({1})) \n" +
                                                  "union \n" +
                                                  "select count(InstrumentID) AS ID from [ProductUsage] where TestId IN ({2}) And [InstrumentID] in (select [InstrumentID] from [SiteInstrument] where SiteID IN({3}))) a where a.ID>0", items.Tag, _siteids, items.Tag, _siteids);
                            cmdForecast.CommandText = _sql;
                            int count1 = Convert.ToInt32(cmdForecast.ExecuteScalar());
                            testname = items.Text;
                            if (count1 > 0)
                            {
                                foreach (ListViewItem items1 in lstSelectTest.Items)
                                {

                                    if (Convert.ToString(items1.Tag) == Convert.ToString(items.Tag))
                                    {
                                        isduplicate = false;
                                        // testname = items.Text;
                                        break;
                                    }
                                    else
                                    {
                                        isduplicate = true;
                                    }
                                }
                                if (isduplicate == false)
                                {
                                    MessageBox.Show(testname + " already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    //return;
                                }
                                else
                                {
                                    ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };

                                    lstSelectTest.Items.Add(li);
                                }

                            }
                            else
                            {
                              //  existsites = existsites + "," + result[i].ToString();
                          //   MessageBox.Show("Site doesn't have the necessary instrument for " + testname + "", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                MessageBox.Show("Product usage rate for " + testname +  " hasnt been defined" , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                ///sites doesn't have CD4 instrument 
                            }
                        }

                        //if (existsites != "")
                        //{
                        //    existsites = existsites.TrimStart(',');
                        //    ////DialogResult dialog = MessageBox.Show("Some Site doesn't have the necessary instrument for " + testname + "", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    ////if (dialog == DialogResult.OK)
                        //    ////{
                        //    ////}
                        //    messagebox frmmsg = new messagebox("Some Site doesn't have the necessary instrument for " + testname + "", existsites, _mdiparent);
                        //    frmmsg.ShowDialog();
                        //}

                    }

                    // ListViewItem li = new ListViewItem(items.Text);
                    //ListViewItem item = lstSelectTest.FindItemWithText(items.Text);
                    //if (item != null)
                    //{
                    //    if (Convert.ToString(item.Tag) != Convert.ToString(items.Tag))
                    //    {
                    //        ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };
                    //        lstSelectTest.Items.Add(li);
                    //        //lstSelectTest.Items.Add(items.Text);
                    //    }
                    //}
                    //else
                    //{
                    //    ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };
                    //    lstSelectTest.Items.Add(li);

                    //}

                    //if (!lstSelectTest.Items.Contains(li))
                    //{
                    //    lstSelectTest.Items.Add(items.Text);
                    //}

                }
                if (connection.State == ConnectionState.Open) connection.Close();
            
        }

        //private void btnadd_Click(object sender, EventArgs e)
        //{
        //    bool isduplicate = true;
        //    string testname = "";
        //    foreach (ListViewItem items in lsttest.CheckedItems)
        //    {

        //        foreach (ListViewItem items1 in lstSelectTest.Items)
        //        {

        //            if (Convert.ToString(items1.Tag) == Convert.ToString(items.Tag))
        //            {
        //                isduplicate = false;
        //                testname = items.Text;
        //                break;
        //            }
        //            else
        //            {
        //                isduplicate = true;
        //            }
        //        }
        //        if (isduplicate == false)
        //        {
        //            //MessageBox.Show(testname + " already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            //return;
        //        }
        //        else
        //        {
        //            ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };

        //            lstSelectTest.Items.Add(li);
        //        }

        //        // ListViewItem li = new ListViewItem(items.Text);
        //        //ListViewItem item = lstSelectTest.FindItemWithText(items.Text);
        //        //if (item != null)
        //        //{
        //        //    if (Convert.ToString(item.Tag) != Convert.ToString(items.Tag))
        //        //    {
        //        //        ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };
        //        //        lstSelectTest.Items.Add(li);
        //        //        //lstSelectTest.Items.Add(items.Text);
        //        //    }
        //        //}
        //        //else
        //        //{
        //        //    ListViewItem li = new ListViewItem(items.Text) { Tag = items.Tag };
        //        //    lstSelectTest.Items.Add(li);

        //        //}

        //        //if (!lstSelectTest.Items.Contains(li))
        //        //{
        //        //    lstSelectTest.Items.Add(items.Text);
        //        //}

        //    }
        //}

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem items in lsttest.Items)
            {
                items.Checked = true;
             
                //items.BackColor = ColorTranslator.FromHtml("#3399FF");
                //items.ForeColor = Color.White;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {

                foreach (ListViewItem items in lstSelectTest.SelectedItems)
                {

                    //using (connection = new SqlConnection(con))
                    //{
                        //SaveOrUpdateObject1();
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                        using (SqlTransaction trans = connection.BeginTransaction())
                        {
                            // string _sql1 = string.Format(@"Select Count(*) from ForecastSiteInfo Where SiteID={0} and  ForecastinfoID={1}", row.Cells["SiteID"].Value, _forecastId);
                            using (cmdForecast = new SqlCommand("", connection,trans))
                            {
                                //   int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                //  if (cnt > 0)
                                //  {
                                cmdForecast.CommandText = "Delete from testingprotocol Where testID= " + items.Tag + " and  ForecastinfoID=" + _forecastId + " ";
                                //cmdForecast = new SqlCommand("Delete from testingprotocol Where testID= " + items.Tag + " and  ForecastinfoID=" + _forecastId + " ", connection);
                                cmdForecast.ExecuteNonQuery();

                                //  }
                            }
                            trans.Commit();
                        }
                    //    connection.Close();
                    //}
                    items.Remove();
                }
            }
            catch (Exception ex)
            {
                connection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (lstSelectTest.Items.Count > 0)
            {
                string _sql = "";
                // string ForecastType = "";

                bool IsSave = false;
                int Totaltarget = 0;
                DataTable table = new DataTable();
                bool Isvalid = true;
                //table.Columns.Add("ID", typeof(int));
                //table.Columns.Add("ForecastinfoID", typeof(int));
                //table.Columns.Add("Name", typeof(string));
                //table.Columns.Add("CurrentPatient", typeof(int));
                //table.Columns.Add("TargetPatient", typeof(int));
                //CheckrecordExist();
                try
                {

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        if (Convert.ToString(row.Cells["TestID"].Value) == "")
                        {

                            Isvalid = false;
                            break;
                        }
                        else
                        {
                            Isvalid = true;
                        }


                    }
                    if (Isvalid == false)
                    {
                        /// dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                        MessageBox.Show("Please select atleast one Test", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                        //  throw new LQTUserException("Current Patient must be greater than zero");

                    }
                    _sql = @"INSERT INTO [dbo].[TestingProtocol] ([TestID] ,[PatientGroupID] ,[ForecastinfoID],[PercentagePanel] ,[Baseline],[Month1],[Month2] ,[Month3] ,[Month4],[Month5]
           ,[Month6] ,[Month7],[Month8],[Month9] ,[Month10] ,[Month11],[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear]) VALUES (@TestID,@PatientGroupID,@ForecastinfoID,@PercentagePanel,@Baseline,@Month1,@Month2
           ,@Month3,@Month4,@Month5,@Month6,@Month7,@Month8,@Month9,@Month10,@Month11,@Month12, @TotalTestPerYear,@TestRepeatPerYear,@SymptomTestPerYear);SELECT SCOPE_IDENTITY()";
                    //using (connection = new SqlConnection(con))
                    //{
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    using (SqlTransaction trans = connection.BeginTransaction())
                    {
                        using (cmdForecast = new SqlCommand("", connection, trans))
                        {

                            foreach (DataGridViewRow row in dataGridView1.Rows)
                            {

                                if (Convert.ToInt32(row.Cells["ID"].Value) > 0)
                                {
                                    string _sql1 = string.Format(@"Delete from [TestingProtocol] Where [ID]='{0}'", Convert.ToInt32(row.Cells["ID"].Value));
                                    cmdForecast.CommandText = _sql1;
                                    cmdForecast.ExecuteNonQuery();
                                    cmdForecast.Parameters.Clear();
                                }
                                cmdForecast.Parameters.Add("@TestID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["TestID"].Value);
                                cmdForecast.Parameters.Add("@PatientGroupID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["ColpatientgroupID"].Value);
                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.Int).Value = _forecastId;
                                cmdForecast.Parameters.Add("@PercentagePanel", SqlDbType.Int).Value = row.Cells["ColPerPatient"].Value;
                                cmdForecast.Parameters.Add("@Baseline", SqlDbType.Int).Value = row.Cells["ColBaseline"].Value;


                                cmdForecast.Parameters.Add("@Month1", SqlDbType.Int).Value = row.Cells["ColM1"].Value;
                                cmdForecast.Parameters.Add("@Month2", SqlDbType.Int).Value = row.Cells["ColM2"].Value;
                                cmdForecast.Parameters.Add("@Month3", SqlDbType.Int).Value = row.Cells["ColM3"].Value;
                                cmdForecast.Parameters.Add("@Month4", SqlDbType.Int).Value = row.Cells["ColM4"].Value;
                                cmdForecast.Parameters.Add("@Month5", SqlDbType.Int).Value = row.Cells["ColM5"].Value;
                                cmdForecast.Parameters.Add("@Month6", SqlDbType.Int).Value = row.Cells["ColM6"].Value;
                                cmdForecast.Parameters.Add("@Month7", SqlDbType.Int).Value = row.Cells["ColM7"].Value;
                                cmdForecast.Parameters.Add("@Month8", SqlDbType.Int).Value = row.Cells["ColM8"].Value;
                                cmdForecast.Parameters.Add("@Month9", SqlDbType.Int).Value = row.Cells["ColM9"].Value;
                                cmdForecast.Parameters.Add("@Month10", SqlDbType.Int).Value = row.Cells["ColM10"].Value;
                                cmdForecast.Parameters.Add("@Month11", SqlDbType.Int).Value = row.Cells["ColM11"].Value;
                                cmdForecast.Parameters.Add("@Month12", SqlDbType.Int).Value = row.Cells["ColM12"].Value;
                                cmdForecast.Parameters.Add("@TotalTestPerYear", SqlDbType.Int).Value = row.Cells["ColTestperyear"].Value;
                                cmdForecast.Parameters.Add("@TestRepeatPerYear", SqlDbType.Int).Value = row.Cells["Colpertestrepeat"].Value;
                                cmdForecast.Parameters.Add("@SymptomTestPerYear", SqlDbType.Int).Value = row.Cells["colpersymptomtest"].Value;
                                cmdForecast.CommandText = _sql;
                                int ID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                row.Cells["ID"].Value = ID;
                                cmdForecast.Parameters.Clear();
                            }

                        }
                        trans.Commit();
                        ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), true);
                        // MessageBox.Show("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    connection.Close();

                    //}
                }
                catch (Exception ex)
                {
                    connection.Close();
                }
                if (Isvalid == true)
                {

                    this.Hide();
                    PatientAssumptions frm = new PatientAssumptions(_mdiparent,_forecastId, "E", Dtpatient1, _startdate, _enddate, _period, _totalTarget, dt1);
                    frm.ShowDialog();
                    this.Close();
                }
               
            }
            else
            {
                MessageBox.Show("Please select atleast one test", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            PatientGroupRatio frm = new PatientGroupRatio(_mdiparent, _forecastId, "E",_startdate,_enddate,_period,_totalTarget,dt1);          
            frm.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string _sql = "";
            // string ForecastType = "";

            bool IsSave = false;
            int Totaltarget = 0;
            DataTable table = new DataTable();
            bool Isvalid = true;
            //table.Columns.Add("ID", typeof(int));
            //table.Columns.Add("ForecastinfoID", typeof(int));
            //table.Columns.Add("Name", typeof(string));
            //table.Columns.Add("CurrentPatient", typeof(int));
            //table.Columns.Add("TargetPatient", typeof(int));
            //CheckrecordExist();
            try
            {

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["TestID"].Value == null)
                    {

                        Isvalid = false;
                        break;
                    }
                    else
                    {
                        Isvalid = true;
                    }


                }
                if (Isvalid == false)
                {
                   /// dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                  MessageBox.Show("Please select atleast one Test", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                    //  throw new LQTUserException("Current Patient must be greater than zero");

                }
                _sql = @"INSERT INTO [dbo].[TestingProtocol] ([TestID] ,[PatientGroupID] ,[ForecastinfoID],[PercentagePanel] ,[Baseline],[Month1],[Month2] ,[Month3] ,[Month4],[Month5]
           ,[Month6] ,[Month7],[Month8],[Month9] ,[Month10] ,[Month11],[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear]) VALUES (@TestID,@PatientGroupID,@ForecastinfoID,@PercentagePanel,@Baseline,@Month1,@Month2
           ,@Month3,@Month4,@Month5,@Month6,@Month7,@Month8,@Month9,@Month10,@Month11,@Month12, @TotalTestPerYear,@TestRepeatPerYear,@SymptomTestPerYear);SELECT SCOPE_IDENTITY()";
                //using (connection = new SqlConnection(con))
                //{
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                    using (SqlTransaction trans = connection.BeginTransaction())
                    {
                        using (cmdForecast = new SqlCommand("", connection, trans))
                        {

                            foreach (DataGridViewRow row in dataGridView1.Rows)
                            {

                                if (Convert.ToInt32(row.Cells["ID"].Value) > 0)
                                {
                                    string _sql1 = string.Format(@"Delete from [TestingProtocol] Where [ID]='{0}'", Convert.ToInt32(row.Cells["ID"].Value));
                                    cmdForecast.CommandText = _sql1;
                                    cmdForecast.ExecuteNonQuery();
                                    cmdForecast.Parameters.Clear();
                                }
                                cmdForecast.Parameters.Add("@TestID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["TestID"].Value);
                                cmdForecast.Parameters.Add("@PatientGroupID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["ColpatientgroupID"].Value);
                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.Int).Value = _forecastId;
                                cmdForecast.Parameters.Add("@PercentagePanel", SqlDbType.Int).Value = row.Cells["ColPerPatient"].Value;
                                cmdForecast.Parameters.Add("@Baseline", SqlDbType.Int).Value = row.Cells["ColBaseline"].Value;


                                cmdForecast.Parameters.Add("@Month1", SqlDbType.Int).Value = row.Cells["ColM1"].Value;
                                cmdForecast.Parameters.Add("@Month2", SqlDbType.Int).Value = row.Cells["ColM2"].Value;
                                cmdForecast.Parameters.Add("@Month3", SqlDbType.Int).Value = row.Cells["ColM3"].Value;
                                cmdForecast.Parameters.Add("@Month4", SqlDbType.Int).Value = row.Cells["ColM4"].Value;
                                cmdForecast.Parameters.Add("@Month5", SqlDbType.Int).Value = row.Cells["ColM5"].Value;
                                cmdForecast.Parameters.Add("@Month6", SqlDbType.Int).Value = row.Cells["ColM6"].Value;
                                cmdForecast.Parameters.Add("@Month7", SqlDbType.Int).Value = row.Cells["ColM7"].Value;
                                cmdForecast.Parameters.Add("@Month8", SqlDbType.Int).Value = row.Cells["ColM8"].Value;
                                cmdForecast.Parameters.Add("@Month9", SqlDbType.Int).Value = row.Cells["ColM9"].Value;
                                cmdForecast.Parameters.Add("@Month10", SqlDbType.Int).Value = row.Cells["ColM10"].Value;
                                cmdForecast.Parameters.Add("@Month11", SqlDbType.Int).Value = row.Cells["ColM11"].Value;
                                cmdForecast.Parameters.Add("@Month12", SqlDbType.Int).Value = row.Cells["ColM12"].Value;
                                cmdForecast.Parameters.Add("@TotalTestPerYear", SqlDbType.Int).Value = row.Cells["ColTestperyear"].Value;
                                cmdForecast.Parameters.Add("@TestRepeatPerYear", SqlDbType.Int).Value = row.Cells["Colpertestrepeat"].Value;
                                cmdForecast.Parameters.Add("@SymptomTestPerYear", SqlDbType.Int).Value = row.Cells["colpersymptomtest"].Value;
                                cmdForecast.CommandText = _sql;
                                int ID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                row.Cells["ID"].Value = ID;
                                cmdForecast.Parameters.Clear();
                            }

                        }
                        trans.Commit();
                        ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), true);
                       // MessageBox.Show("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
               connection.Close();

                //}
            }
            catch (Exception ex)
            {
                connection.Close();
            }
           
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            int column = dataGridView1.CurrentCellAddress.X;
            if (column != 1 && column != 2)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                //if (e.KeyChar == '.'
                //    && (sender as TextBox).Text.IndexOf('.') > -1)
                //{
                //    e.Handled = true;
                //}
            }
            else if (column == 2)
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if (e.KeyChar == '.'
                    && (sender as TextBox).Text.IndexOf('.') > -1)
                {
                    e.Handled = true;
                }
            
            }

            else
            {
                var regex = new Regex(@"[^a-zA-Z0-9\s]");
                if (regex.IsMatch(e.KeyChar.ToString()))
                {
                    e.Handled = true;
                }
            }
        }
        private void lstSelectTest_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            if (lstSelectTest.SelectedItems.Count > 0)
            {

                string _sql = "";
                // string ForecastType = "";

                bool IsSave = false;
                int Totaltarget = 0;
                DataTable table = new DataTable();
                 bool Isvalid = true;
                //table.Columns.Add("ID", typeof(int));
                //table.Columns.Add("ForecastinfoID", typeof(int));
                //table.Columns.Add("Name", typeof(string));
                //table.Columns.Add("CurrentPatient", typeof(int));
                //table.Columns.Add("TargetPatient", typeof(int));
                //CheckrecordExist();
                try
                {

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        if (row.Cells["TestID"].Value == null)
                        {

                            Isvalid = false;
                            break;
                        }
                        else
                        {
                            Isvalid = true;
                        }


                    }
                    if (Isvalid == false)
                    {
                        /// dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                        MessageBox.Show("Please select atleast one Test", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                        //  throw new LQTUserException("Current Patient must be greater than zero");

                    }
                    _sql = @"INSERT INTO [dbo].[TestingProtocol] ([TestID] ,[PatientGroupID] ,[ForecastinfoID],[PercentagePanel] ,[Baseline],[Month1],[Month2] ,[Month3] ,[Month4],[Month5]
           ,[Month6] ,[Month7],[Month8],[Month9] ,[Month10] ,[Month11],[Month12],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear]) VALUES (@TestID,@PatientGroupID,@ForecastinfoID,@PercentagePanel,@Baseline,@Month1,@Month2
           ,@Month3,@Month4,@Month5,@Month6,@Month7,@Month8,@Month9,@Month10,@Month11,@Month12, @TotalTestPerYear,@TestRepeatPerYear,@SymptomTestPerYear);SELECT SCOPE_IDENTITY()";
                    //using (connection = new SqlConnection(con))
                    //{
                    if (connection.State == ConnectionState.Open)
                    { }
                    else
                    { connection.Open(); }
                    using (SqlTransaction trans = connection.BeginTransaction())
                    {
                        using (cmdForecast = new SqlCommand("", connection, trans))
                        {

                            foreach (DataGridViewRow row in dataGridView1.Rows)
                            {

                                if (Convert.ToInt32(row.Cells["ID"].Value) > 0)
                                {
                                    string _sql1 = string.Format(@"Delete from [TestingProtocol] Where [ID]='{0}'", Convert.ToInt32(row.Cells["ID"].Value));
                                    cmdForecast.CommandText = _sql1;
                                    cmdForecast.ExecuteNonQuery();
                                    cmdForecast.Parameters.Clear();
                                }
                                cmdForecast.Parameters.Add("@TestID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["TestID"].Value);
                                cmdForecast.Parameters.Add("@PatientGroupID", SqlDbType.Int).Value = Convert.ToInt32(row.Cells["ColpatientgroupID"].Value);
                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.Int).Value = _forecastId;
                                cmdForecast.Parameters.Add("@PercentagePanel", SqlDbType.Int).Value = row.Cells["ColPerPatient"].Value;
                                cmdForecast.Parameters.Add("@Baseline", SqlDbType.Int).Value = row.Cells["ColBaseline"].Value;


                                cmdForecast.Parameters.Add("@Month1", SqlDbType.Int).Value = row.Cells["ColM1"].Value;
                                cmdForecast.Parameters.Add("@Month2", SqlDbType.Int).Value = row.Cells["ColM2"].Value;
                                cmdForecast.Parameters.Add("@Month3", SqlDbType.Int).Value = row.Cells["ColM3"].Value;
                                cmdForecast.Parameters.Add("@Month4", SqlDbType.Int).Value = row.Cells["ColM4"].Value;
                                cmdForecast.Parameters.Add("@Month5", SqlDbType.Int).Value = row.Cells["ColM5"].Value;
                                cmdForecast.Parameters.Add("@Month6", SqlDbType.Int).Value = row.Cells["ColM6"].Value;
                                cmdForecast.Parameters.Add("@Month7", SqlDbType.Int).Value = row.Cells["ColM7"].Value;
                                cmdForecast.Parameters.Add("@Month8", SqlDbType.Int).Value = row.Cells["ColM8"].Value;
                                cmdForecast.Parameters.Add("@Month9", SqlDbType.Int).Value = row.Cells["ColM9"].Value;
                                cmdForecast.Parameters.Add("@Month10", SqlDbType.Int).Value = row.Cells["ColM10"].Value;
                                cmdForecast.Parameters.Add("@Month11", SqlDbType.Int).Value = row.Cells["ColM11"].Value;
                                cmdForecast.Parameters.Add("@Month12", SqlDbType.Int).Value = row.Cells["ColM12"].Value;
                                cmdForecast.Parameters.Add("@TotalTestPerYear", SqlDbType.Int).Value = row.Cells["ColTestperyear"].Value;
                                cmdForecast.Parameters.Add("@TestRepeatPerYear", SqlDbType.Int).Value = row.Cells["Colpertestrepeat"].Value;
                                cmdForecast.Parameters.Add("@SymptomTestPerYear", SqlDbType.Int).Value = row.Cells["colpersymptomtest"].Value;
                                cmdForecast.CommandText = _sql;
                                int ID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                row.Cells["ID"].Value = ID;
                                cmdForecast.Parameters.Clear();
                            }

                        }
                        trans.Commit();
                        ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), true);
                        // MessageBox.Show("Record saved successfully for " + lbltest.Text.Split(':')[1].ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                    connection.Close();

                    //}
                }
                catch (Exception ex)
                {
                    connection.Close();
                }

                int id = (int)lstSelectTest.SelectedItems[0].Tag;
                _sql = "";

              //  _sql = string.Format(@"SELECT * from [TestingProtocol]   where [ForecastinfoID]={0} and [TestID]={1}", _forecastId, id);
                //using (connection = new SqlConnection(con))
                //{
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }
                    using (cmdForecast = new SqlCommand("", connection))
                    {

                        cmdForecast.CommandType = CommandType.Text;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmdForecast))
                        {

                            
                             foreach (DataGridViewRow row in dataGridView1.Rows)
                                {
                                       _sql = string.Format(@"SELECT * from [TestingProtocol]   where [ForecastinfoID]={0} and [TestID]={1} and PatientGroupID={2}", _forecastId, id,Convert.ToInt32(row.Cells["ColpatientgroupID"].Value));
                                       cmdForecast.CommandText = _sql;      
                                       sda.Fill(dt);
                                  if (dt.Rows.Count > 0)
                                     {
                                        row.Cells["ID"].Value = dt.Rows[0]["ID"].ToString();
                                        row.Cells["TestID"].Value = dt.Rows[0]["TestID"].ToString();
                                        row.Cells["ColPerPatient"].Value = Convert.ToInt32(dt.Rows[0]["PercentagePanel"]);
                                        row.Cells["ColBaseline"].Value = Convert.ToInt32(dt.Rows[0]["Baseline"]);

                                        row.Cells["ColM1"].Value = Convert.ToInt32(dt.Rows[0]["Month1"]);
                                        row.Cells["ColM2"].Value = Convert.ToInt32(dt.Rows[0]["Month2"]);
                                        row.Cells["ColM3"].Value = Convert.ToInt32(dt.Rows[0]["Month3"]);
                                        row.Cells["ColM4"].Value = Convert.ToInt32(dt.Rows[0]["Month4"]);

                                        row.Cells["ColM5"].Value = Convert.ToInt32(dt.Rows[0]["Month5"]);
                                        row.Cells["ColM6"].Value = Convert.ToInt32(dt.Rows[0]["Month6"]);
                                        row.Cells["ColM7"].Value = Convert.ToInt32(dt.Rows[0]["Month7"]);
                                        row.Cells["ColM8"].Value = Convert.ToInt32(dt.Rows[0]["Month8"]);

                                        row.Cells["ColM9"].Value = Convert.ToInt32(dt.Rows[0]["Month9"]);
                                        row.Cells["ColM10"].Value = Convert.ToInt32(dt.Rows[0]["Month10"]);
                                        row.Cells["ColM11"].Value = Convert.ToInt32(dt.Rows[0]["Month11"]);
                                        row.Cells["ColM12"].Value = Convert.ToInt32(dt.Rows[0]["Month12"]);

                                        row.Cells["ColTestperyear"].Value = Convert.ToInt32(dt.Rows[0]["TotalTestPerYear"]);
                                        row.Cells["Colpertestrepeat"].Value = Convert.ToInt32(dt.Rows[0]["TestRepeatPerYear"]);
                                        row.Cells["colpersymptomtest"].Value = Convert.ToInt32(dt.Rows[0]["SymptomTestPerYear"]);
                                      
                            
                                    }
                                 else
                                 {
                                    row.Cells["TestID"].Value = id;
                                    row.Cells["ID"].Value = "0";
                                   
                                    row.Cells["ColPerPatient"].Value = "0";
                                    row.Cells["ColBaseline"].Value = "0";

                                    row.Cells["ColM1"].Value = "0";
                                    row.Cells["ColM2"].Value = "0";
                                    row.Cells["ColM3"].Value = "0";
                                    row.Cells["ColM4"].Value = "0";

                                    row.Cells["ColM5"].Value = "0";
                                    row.Cells["ColM6"].Value = "0";
                                    row.Cells["ColM7"].Value = "0";
                                    row.Cells["ColM8"].Value = "0";

                                    row.Cells["ColM9"].Value = "0";
                                    row.Cells["ColM10"].Value = "0";
                                    row.Cells["ColM11"].Value = "0";
                                    row.Cells["ColM12"].Value = "0";

                                    row.Cells["ColTestperyear"].Value = "0";
                                    row.Cells["Colpertestrepeat"].Value = "0";
                                    row.Cells["colpersymptomtest"].Value = "0";
                                 }
                                  dt.Clear();
                             }
                             
                            }
                           

                        }

                        connection.Close();
                   // }
                lbltest.Text = "Testing Protocol : " + lstSelectTest.SelectedItems[0].Text;
             //   this.button3_Click(null, null);
                }
            }
       

        private void dataGridView1_EditingControlShowing_1(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
            }
            catch (Exception ex)
            { }
        }

    }
}
