﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using LQT.Core.Domain;
using LQT.Core.Util;
//using LQT.GUI.Testing;
using LQT.Core.UserExceptions;
using System.Data.SqlClient;
using LQT.GUI.UserCtr;
using LQT.Core;
using System.Globalization;
using LQT.Core.Domain;


namespace LQT.GUI.Quantification
{
    public partial class Frmlineargrowth : Form
    {
        private ForecastInfo _mMforecastinfo;
        private IList<MMGeneralAssumption> _mMGeneralAssumption;
        private Form _mdiparent;
        private int forecastId = 0;
        private string StartDate = "";
        private string EndDate = "";
        private string Period = "";
        private int reportingPeriod = 0;
        private int totalmonth = 0 ;
        private Int64 _totalTarget = 0;
        private string ForecastType = "";
        private DataTable Dt1;
        private DataTable dt = new DataTable();
        SqlCommand cmdForecast = null;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        public static string Mode = "";
        DataTable Dtpatient1 = new DataTable();
        DataTable dt1 = new DataTable();
        List<string> Lstmonthname = new List<string>();
        string varName = "";
        string svarName = "";
        DataTable dtGetValue = new DataTable();
        //string con = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10", AppSettings.DatabaseLoginName, AppSettings.DatabasePassword, AppSettings.DatabaseServerName, AppSettings.DatabaseName);
        public Frmlineargrowth(Form mdiparent, int _forecastId, string mode, DataTable Dtpatient, string StartDate, string EndDate, string Period, Int64 totaltarget, DataTable Dt, ForecastInfo finfo) //
        {
          //  DateTime d 
            this.StartDate = StartDate;
            this.EndDate = EndDate;
            this.forecastId = _forecastId;
            _mdiparent = mdiparent;
            this._mMforecastinfo = finfo;
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;
            string _type = "";
            Mode = mode;

            _totalTarget = totaltarget;
           
            Dtpatient1 = Dtpatient;
            dt1 = Dt;

            dtGetValue.Columns.Add("VariableName", typeof(string));
            dtGetValue.Columns.Add("VariableEffect", typeof(string));
            dtGetValue.Columns.Add("VariableDataType", typeof(string));

            if (_mMforecastinfo.ForecastType == "S") _type = MorbidityVariableUsage.OnEachSite.ToString();
            else if (_mMforecastinfo.ForecastType == "C") _type = MorbidityVariableUsage.OnAggSite.ToString();
            _mMGeneralAssumption = DataRepository.GetAllGeneralAssumptionByTypeAndProgram((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), "Patient_Number_Assumption"), _mMforecastinfo.ProgramId, _type);

            foreach (MMGeneralAssumption gAssumption in _mMGeneralAssumption)
            {
                if (gAssumption.AssumptionType == 1 && gAssumption.UseOn.ToString() == _type)
                {
                    DataRow dr = dtGetValue.NewRow(); 
                    varName += "Isnull([" + gAssumption.VariableName + "],0) As [" + gAssumption.VariableName + "],";
                    svarName += ""+ gAssumption.VariableName + " "+",";
                    dr["VariableName"] = gAssumption.VariableName.ToString();
                    dr["VariableEffect"] = gAssumption.VariableEffect.ToString();
                    dr["VariableDataType"] = gAssumption.VariableDataType.ToString();
                    dtGetValue.Rows.Add(dr);
                }
                dtGetValue.AcceptChanges();
            }

            this.Period = Period;
            if (Period == "Bimonthly")
            {
                this.reportingPeriod = 2;
            }
            else if (Period == "Monthly")
            {
                this.reportingPeriod = 1;
            }
            else if (Period == "Quarterly")
            {
                this.reportingPeriod = 4;
            }
            else if (Period == "Yearly")
            {
                this.reportingPeriod = 12;
            }
            this.Dt1 = Dt;
       
            InitializeComponent();
            string _sql = "";
            _sql = string.Format(@"Select Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]='{0}'", forecastId);
            //using (connection = new SqlConnection(con))
            //{
                using (cmdForecast = new SqlCommand(_sql, connection))
                {
                    if (connection.State == ConnectionState.Closed)
                    connection.Open();
                    ForecastType = Convert.ToString(cmdForecast.ExecuteScalar());
                    if (connection.State != ConnectionState.Closed)
                    connection.Close();
                }
          //  }

            btnprevious.TabStop = false;
            btnprevious.FlatStyle = FlatStyle.Flat;
            btnprevious.FlatAppearance.BorderSize = 0;
            btnprevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);


            btnnext.TabStop = false;
            btnnext.FlatStyle = FlatStyle.Flat;
            btnnext.FlatAppearance.BorderSize = 0;
            btnnext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            DisplayData();
        }


        private void DisplayData()
        {
            DateTime StartDate1 = DateTime.Parse(StartDate);
            DateTime EndDate1 = DateTime.Parse(EndDate);
            DateTime month = new DateTime(StartDate1.Year, StartDate1.Month , 1); // StartDate1.Month;
         //   StartDate1.Month = month.AddMonths(-1);
          //  StartDate1.Month =StartDate1.AddMonths(StartDate1.Month
            StartDate1 = StartDate1.AddMonths(-1);
            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;

            int newpatient = 0;
            int totalnewpatient = 0;
            int targetpatient = 0;
            int totaltargetpatient = 0;
            int numberofslot = 0;
            int HIVpositivewithoutfollowing = 0;
            int totalcurrentpatient = 0;
            int totalmonth = GetMonthDifference(StartDate1, EndDate1);
            Lstmonthname = MonthsBetween(StartDate1, EndDate1);
            int aAmount = 0;
            int fPatient = 0;
            int tPatient = 0;
            DataTable dtValue = new DataTable();
            if (reportingPeriod !=0)
            {
                 numberofslot = totalmonth / reportingPeriod;
            }
            for (int i = 0; i < Dt1.Rows.Count; i++)
            {
                newpatient = Convert.ToInt32(Dt1.Rows[i]["TargetPatient"].ToString()) - Convert.ToInt32(Dt1.Rows[i]["CurrentPatient"].ToString());
                //string _sql = string.Format(@"Select Isnull([HIVPerWithoutFollow],0) from [PatientAssumption] Where [ForecastinfoID]='{0}' and [SiteID]='{1}'", forecastId, Convert.ToInt32(Dt1.Rows[i]["ID"].ToString()));
                //    using (cmdForecast = new SqlCommand(_sql, connection))
                //    {
                //        if (connection.State == ConnectionState.Closed)
                //        connection.Open();
                //        HIVpositivewithoutfollowing = Convert.ToInt32(cmdForecast.ExecuteScalar());
                //        if (connection.State != ConnectionState.Closed)
                //        connection.Close();
                //    }
                string _sql = string.Format(@"Select " + varName.TrimEnd(',')  + " from [PatientAssumption] Where [ForecastinfoID]='{0}' and [SiteID]='{1}'", forecastId, Convert.ToInt32(Dt1.Rows[i]["ID"].ToString()));
                    using (cmdForecast = new SqlCommand(_sql, connection))
                    {
                        if (connection.State == ConnectionState.Closed)
                             connection.Open();
                        //HIVpositivewithoutfollowing = Convert.ToInt32(cmdForecast.ExecuteScalar());
                        SqlDataAdapter da = new SqlDataAdapter(cmdForecast);
                        
                        da.Fill(dtValue);
                        if (connection.State != ConnectionState.Closed)
                            connection.Close();
                    }
                    svarName = svarName.TrimEnd(',');
                    string[] words = svarName.Split(',');
                    for (int ki = 0; ki < dtValue.Rows.Count; ki++)
                    {
                        foreach (DataColumn dc in dtValue.Columns )
                        {
                        foreach (DataRow dr in dtGetValue.Rows)
                        {
                            string v1 = dr["VariableName"].ToString();
                            string v2 = dc.ColumnName.ToString();
                            if (v1.Equals(v2))
                            {
                                //bool variableEffect = gAssumption.Where(x => x.VariableName == item.ToString()).SingleOrDefault().VariableEffect;
                                //int variableDatatype = _mMGeneralAssumption.Where(x => x.VariableName == item.ToString()).SingleOrDefault().VariableDataType;
                                //if(variableDatatype==2)aAmount=aAmount+(newpatient*(Convert.ToDecimal(dtValue.Rows[ki][""+item.ToString()+""]))/100);
                                //else if (variableDatatype == 2) aAmount = aAmount + (newpatient * (Convert.ToDecimal(dtValue.Rows[ki]["" + item.ToString() + ""])) / 100);
                                aAmount =(newpatient * (Convert.ToInt32(dtValue.Rows[ki]["" + v2 + ""])) / 100);
                                if (dr["variableEffect"].ToString() == "True") fPatient = fPatient + aAmount;
                                if (dr["variableEffect"].ToString() == "False") fPatient = fPatient - aAmount;
                               
                            }
                            
                            }
                        }
                       
                    }


                 //newpatient =newpatient + (fPatient);
                
                //newpatient = newpatient - ((newpatient * HIVpositivewithoutfollowing) / 100);
                targetpatient = newpatient + Convert.ToInt32(Dt1.Rows[i]["CurrentPatient"].ToString());

                totaltargetpatient += targetpatient;
                totalcurrentpatient += Convert.ToInt32(Dt1.Rows[i]["CurrentPatient"].ToString());
               // calculatelineargrowth(Convert.ToInt32(Dt1.Rows[i]["ID"].ToString()), Dt1.Rows[i]["Name"].ToString(), Convert.ToInt32(Dt1.Rows[i]["CurrentPatient"].ToString()), Convert.ToInt32(Dt1.Rows[i]["TargetPatient"].ToString()), numberofslot,i);
            }
            calculatelineargrowth(totalcurrentpatient, totaltargetpatient, numberofslot, 0);
          
        }
        private void calculatelineargrowth(double currentpatient, double targetpatient, int numberslot, int k)//int ID,string sitename,
        {
            int count = numberslot;
            double slotvalue = 0;
            int colindex=4;
            double diff = Convert.ToDouble(targetpatient - currentpatient);
            List<string> list1 = new List<string>(); 
            string List="";
            if (k == 0)
            {
                //dt.Columns.Add("Id");
                dt.Columns.Add("ForecastId");
                dt.Columns.Add("Currentpatient");
                dt.Columns.Add("Targetpatient");
               // dt.Columns.Add("Name");
            
            }
            DataRow dr = dt.NewRow();

           // dr["Id"] = Convert.ToString(ID);
            dr["ForecastId"] = Convert.ToString(forecastId);
            dr["Currentpatient"] = Convert.ToString(currentpatient);
            dr["Targetpatient"] = Convert.ToString(targetpatient);
            //dr["Name"] = sitename;
         
   
            List +="," + Convert.ToString(currentpatient);
            while (count > 1)
            {
                slotvalue = currentpatient + (diff / numberslot);
                 List += "," + Convert.ToString(Math.Round(slotvalue,0));
              //  list1.Add(Convert.ToString(slotvalue));               
                count--;
                colindex++;
                currentpatient = slotvalue;
            }
            List += "," + Convert.ToString(targetpatient);
            List = List.TrimStart(',');
            string[] gridvalue = List.Split(',');
            int j = 0; int i = 0;

            while (i < Lstmonthname.Count)
            {
                if (k==0)
                 dt.Columns.Add(Lstmonthname[i].ToString());
                dr[Lstmonthname[i].ToString()] = gridvalue[j].ToString();
           
                i = i + reportingPeriod;
                j++;
            }
            dt.AcceptChanges();
            dt.Rows.Add(dr);
     
            dataGridView1.DataSource = dt;
            dataGridView1.Columns[0].Visible =false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[2].Visible = false;
            
           // dataGridView1.Columns[3].Visible = false;
      
        }
        private  int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }
        private List<string> MonthsBetween(DateTime startDate, DateTime endDate)
        {
            DateTime iterator;
            DateTime limit;
            List<string> List =  new List<string>(); 
            if (endDate > startDate)
            {
                iterator = new DateTime(startDate.Year, startDate.Month, 1);
                limit = endDate;
            }
            else
            {
                iterator = new DateTime(endDate.Year, endDate.Month, 1);
                limit = startDate;
            }

            var dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            while (iterator <= limit)
            {
               List.Add( dateTimeFormat.GetMonthName(iterator.Month).Substring(0,3) + " " + iterator.Year.ToString() );
                iterator = iterator.AddMonths(1);
            }
            return List;
        }

        private void btnprevious_Click(object sender, EventArgs e)
        {
            this.Hide();
            //frmProductRAssumptions F = new frmProductRAssumptions(_mdiparent,forecastId, "E", Dtpatient1, StartDate, EndDate, Period, _totalTarget, dt1);
            FrmDynamicProductRAssumptions frm = new FrmDynamicProductRAssumptions(_mdiparent, _mMforecastinfo, Dtpatient1, _totalTarget, dt1);
            frm.ShowDialog(); 
            //if (ForecastType == "S")
            //{
            //    Frmsitebysiteforecast frm = new Frmsitebysiteforecast(forecastId, "E", StartDate, EndDate, Period);
            //    frm.ShowDialog();
            //}
            //else

            //{

            //    AggregateForecast Ag = new AggregateForecast(forecastId, "E", StartDate, EndDate, Period);
            //    Ag.ShowDialog();
            //}
            this.Close();
        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            string  _sql ="";
           // string ForecastType = "";

            bool IsSave = false;
            int Totaltarget = 0 ;
            DataTable table = new DataTable();
            //table.Columns.Add("ID", typeof(int));
            //table.Columns.Add("ForecastinfoID", typeof(int));
            //table.Columns.Add("Name", typeof(string));
            //table.Columns.Add("CurrentPatient", typeof(int));
            //table.Columns.Add("TargetPatient", typeof(int));
            try
            {
                CheckrecordExist();

                _sql = "INSERT INTO [PatientNumberHeader] ([ForecastinfoID],[CurrentPatient],[TargetPatient]) OUTPUT Inserted.ID VALUES(@ForecastinfoID,@CurrentPatient,@TargetPatient)";
                ////using (connection = new SqlConnection(con))
                ////{
                if (connection.State == ConnectionState.Closed)
                connection.Open();
                    using (SqlTransaction trans = connection.BeginTransaction())
                    {
                        using (cmdForecast = new SqlCommand("", connection, trans))
                        {
                            foreach (DataGridViewRow row in dataGridView1.Rows)
                            {
                                cmdForecast.Parameters.Add("@ForecastinfoID", SqlDbType.Int).Value = forecastId;
                                //if (ForecastType == "S")
                                //{
                                //    cmdForecast.Parameters.Add("@SiteID", SqlDbType.Int).Value = row.Cells["ID"].Value;
                                //    cmdForecast.Parameters.Add("@CategoryID", SqlDbType.Int).Value = 0;
                                //}
                                //else
                                //{
                                //    cmdForecast.Parameters.Add("@SiteID", SqlDbType.Int).Value = 0;
                                //    cmdForecast.Parameters.Add("@CategoryID", SqlDbType.Int).Value = row.Cells["ID"].Value;
                                //}

                                cmdForecast.Parameters.Add("@CurrentPatient", SqlDbType.BigInt).Value = row.Cells["Currentpatient"].Value;
                                cmdForecast.Parameters.Add("@TargetPatient", SqlDbType.BigInt).Value = row.Cells["Targetpatient"].Value;
                                cmdForecast.CommandText = _sql;
                                // table.Rows.Add(row.Cells["ID"].Value, forecastId, row.Cells["ColSiteName"].Value, row.Cells["ColCurrentPatient"].Value, row.Cells["ColTargetPatient"].Value);

                                Totaltarget = Totaltarget + Convert.ToInt32(row.Cells["Targetpatient"].Value);
                                int ID = Convert.ToInt32(cmdForecast.ExecuteScalar());
                                cmdForecast.Parameters.Clear();
                                for (int i = 3; i < dataGridView1.Columns.Count; i++)
                                {
                                    string _sql1 = "INSERT INTO [PatientNumberDetail] ([HeaderID],[Serial],[Columnname],[ForeCastId])  VALUES(@headerID,@Serial,@Columnname,@ForecastinfoID1)";
                                    // cmdForecast = new SqlCommand(_sql, connection,trans);
                                    cmdForecast.Parameters.Add("@headerID", SqlDbType.BigInt).Value = ID;
                                    cmdForecast.Parameters.Add("@Serial", SqlDbType.Decimal).Value = row.Cells[i].Value;
                                    cmdForecast.Parameters.Add("@Columnname", SqlDbType.VarChar, 20).Value = dataGridView1.Columns[i].HeaderText;
                                    cmdForecast.Parameters.Add("@ForecastinfoID1", SqlDbType.Int).Value = forecastId;
                                    cmdForecast.CommandText = _sql1;
                                    cmdForecast.ExecuteNonQuery();
                                    cmdForecast.Parameters.Clear();
                                    IsSave = true;

                                }

                            }


                        }
                        trans.Commit();
                    }
                    if (connection.State != ConnectionState.Closed)
             connection.Close();


                //}
                if (IsSave == true)
                {
                    this.Hide();

                    ForecastChart frm = new ForecastChart(_mdiparent, forecastId, "E", Dtpatient1, StartDate, EndDate, Period, _totalTarget, Dt1, _mMforecastinfo);
                    // Form1 frm = new Form1();
                    frm.ShowDialog();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
            connection.Close();
            }
        }

        private void CheckrecordExist()
        {
            //using (connection = new SqlConnection(con))
            //{
            if (connection.State == ConnectionState.Closed)
        connection.Open();
                string _sql1 = string.Format(@"Select count(*) from [PatientNumberHeader] Where [ForecastinfoID]='{0}'", forecastId);
                using (SqlTransaction trans = connection.BeginTransaction())
                {
                    using (cmdForecast = new SqlCommand(_sql1, connection,trans))
                    {
                      
                        int cnt = Convert.ToInt32(cmdForecast.ExecuteScalar());
                        if (cnt > 0)
                        {
                            _sql1 = string.Format(@"Delete from [PatientNumberDetail] Where [ForeCastId]='{0}'", forecastId);
                            cmdForecast = new SqlCommand(_sql1, connection, trans);
                            cmdForecast.ExecuteNonQuery();
                            _sql1 = string.Format(@"Delete from [PatientNumberHeader] Where [ForecastinfoID]='{0}'", forecastId);
                            cmdForecast = new SqlCommand(_sql1, connection, trans);
                            cmdForecast.ExecuteNonQuery();

                        }
                      
                    }
                    trans.Commit();
                }
                if (connection.State != ConnectionState.Closed)
         connection.Close();
            //}
        }
    }
}
