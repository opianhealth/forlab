﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LQT.GUI.Quantification
{
    public partial class messagebox1 : Form
    {
        private string message1 = "";
        private string success1 = "";
        public messagebox1(string message, string success)
        {
            InitializeComponent();
            message1 = message;
            success1 = success;
            label1.Text = message1;
            fillListview();
        }
        private void fillListview()
        {
            listView1.BeginUpdate();
            listView1.Items.Clear();
            string[] arrsuccess = success1.Split(',');
            for (int i = 0; i < arrsuccess.Length; i++)
            {
                ListViewItem li = new ListViewItem(arrsuccess[i]);

                listView1.Items.Add(li);
            }

            listView1.EndUpdate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Height = 493;
            panel1.Visible = true;
        }
    }
}
