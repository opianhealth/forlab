﻿namespace LQT.GUI.Quantification
{
    partial class Frmimportdata
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.butClear = new System.Windows.Forms.Button();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.butSave = new System.Windows.Forms.Button();
            this.butImport = new System.Windows.Forms.Button();
            this.butBrowse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnvariable = new System.Windows.Forms.Button();
            this.btnreferralsite = new System.Windows.Forms.Button();
            this.btnsiteinstrument = new System.Windows.Forms.Button();
            this.btnsite = new System.Windows.Forms.Button();
            this.btnregion = new System.Windows.Forms.Button();
            this.btnconsumables = new System.Windows.Forms.Button();
            this.btntestproduct = new System.Windows.Forms.Button();
            this.btninstrument = new System.Windows.Forms.Button();
            this.btnproduct = new System.Windows.Forms.Button();
            this.btnsavetest = new System.Windows.Forms.Button();
            this.cmbvariable = new System.Windows.Forms.ComboBox();
            this.cmbreferralsite = new System.Windows.Forms.ComboBox();
            this.cmbsiteinstrument = new System.Windows.Forms.ComboBox();
            this.cmbsite = new System.Windows.Forms.ComboBox();
            this.cmbregion = new System.Windows.Forms.ComboBox();
            this.cmbconsumables = new System.Windows.Forms.ComboBox();
            this.cmbinstrument = new System.Windows.Forms.ComboBox();
            this.cmbtestproduct = new System.Windows.Forms.ComboBox();
            this.cmbproduct = new System.Windows.Forms.ComboBox();
            this.cmbtest = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnExit = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // butClear
            // 
            this.butClear.Enabled = false;
            this.butClear.Location = new System.Drawing.Point(452, 12);
            this.butClear.Name = "butClear";
            this.butClear.Size = new System.Drawing.Size(50, 30);
            this.butClear.TabIndex = 10;
            this.butClear.Text = "Clear";
            this.butClear.UseVisualStyleBackColor = true;
            this.butClear.Visible = false;
            // 
            // txtFilename
            // 
            this.txtFilename.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtFilename.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFilename.Location = new System.Drawing.Point(13, 12);
            this.txtFilename.MaximumSize = new System.Drawing.Size(2, 25);
            this.txtFilename.MinimumSize = new System.Drawing.Size(250, 25);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.ReadOnly = true;
            this.txtFilename.Size = new System.Drawing.Size(250, 25);
            this.txtFilename.TabIndex = 6;
            this.txtFilename.WordWrap = false;
            // 
            // butSave
            // 
            this.butSave.Enabled = false;
            this.butSave.Location = new System.Drawing.Point(508, 12);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(50, 30);
            this.butSave.TabIndex = 9;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Visible = false;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butImport
            // 
            this.butImport.Location = new System.Drawing.Point(396, 12);
            this.butImport.Name = "butImport";
            this.butImport.Size = new System.Drawing.Size(50, 30);
            this.butImport.TabIndex = 8;
            this.butImport.Text = "Import";
            this.butImport.UseVisualStyleBackColor = true;
            this.butImport.Click += new System.EventHandler(this.butImport_Click);
            // 
            // butBrowse
            // 
            this.butBrowse.Location = new System.Drawing.Point(317, 12);
            this.butBrowse.Name = "butBrowse";
            this.butBrowse.Size = new System.Drawing.Size(73, 30);
            this.butBrowse.TabIndex = 7;
            this.butBrowse.Text = "Browse...";
            this.butBrowse.UseVisualStyleBackColor = true;
            this.butBrowse.Click += new System.EventHandler(this.butBrowse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xls";
            this.openFileDialog1.Filter = "Excel files|*.xls";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnvariable);
            this.panel1.Controls.Add(this.btnreferralsite);
            this.panel1.Controls.Add(this.btnsiteinstrument);
            this.panel1.Controls.Add(this.btnsite);
            this.panel1.Controls.Add(this.btnregion);
            this.panel1.Controls.Add(this.btnconsumables);
            this.panel1.Controls.Add(this.btntestproduct);
            this.panel1.Controls.Add(this.btninstrument);
            this.panel1.Controls.Add(this.btnproduct);
            this.panel1.Controls.Add(this.btnsavetest);
            this.panel1.Controls.Add(this.cmbvariable);
            this.panel1.Controls.Add(this.cmbreferralsite);
            this.panel1.Controls.Add(this.cmbsiteinstrument);
            this.panel1.Controls.Add(this.cmbsite);
            this.panel1.Controls.Add(this.cmbregion);
            this.panel1.Controls.Add(this.cmbconsumables);
            this.panel1.Controls.Add(this.cmbinstrument);
            this.panel1.Controls.Add(this.cmbtestproduct);
            this.panel1.Controls.Add(this.cmbproduct);
            this.panel1.Controls.Add(this.cmbtest);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(10, 236);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(591, 153);
            this.panel1.TabIndex = 11;
            this.panel1.Visible = false;
            // 
            // btnvariable
            // 
            this.btnvariable.Enabled = false;
            this.btnvariable.Location = new System.Drawing.Point(438, 269);
            this.btnvariable.Name = "btnvariable";
            this.btnvariable.Size = new System.Drawing.Size(74, 21);
            this.btnvariable.TabIndex = 29;
            this.btnvariable.Text = "Save";
            this.btnvariable.UseVisualStyleBackColor = true;
            this.btnvariable.Click += new System.EventHandler(this.btnvariable_Click);
            // 
            // btnreferralsite
            // 
            this.btnreferralsite.Enabled = false;
            this.btnreferralsite.Location = new System.Drawing.Point(438, 242);
            this.btnreferralsite.Name = "btnreferralsite";
            this.btnreferralsite.Size = new System.Drawing.Size(74, 21);
            this.btnreferralsite.TabIndex = 28;
            this.btnreferralsite.Text = "Save";
            this.btnreferralsite.UseVisualStyleBackColor = true;
            this.btnreferralsite.Click += new System.EventHandler(this.btnreferralsite_Click);
            // 
            // btnsiteinstrument
            // 
            this.btnsiteinstrument.Enabled = false;
            this.btnsiteinstrument.Location = new System.Drawing.Point(438, 215);
            this.btnsiteinstrument.Name = "btnsiteinstrument";
            this.btnsiteinstrument.Size = new System.Drawing.Size(74, 21);
            this.btnsiteinstrument.TabIndex = 27;
            this.btnsiteinstrument.Text = "Save";
            this.btnsiteinstrument.UseVisualStyleBackColor = true;
            this.btnsiteinstrument.Click += new System.EventHandler(this.btnsiteinstrument_Click);
            // 
            // btnsite
            // 
            this.btnsite.Enabled = false;
            this.btnsite.Location = new System.Drawing.Point(438, 188);
            this.btnsite.Name = "btnsite";
            this.btnsite.Size = new System.Drawing.Size(74, 21);
            this.btnsite.TabIndex = 26;
            this.btnsite.Text = "Save";
            this.btnsite.UseVisualStyleBackColor = true;
            this.btnsite.Click += new System.EventHandler(this.btnsite_Click);
            // 
            // btnregion
            // 
            this.btnregion.Enabled = false;
            this.btnregion.Location = new System.Drawing.Point(438, 161);
            this.btnregion.Name = "btnregion";
            this.btnregion.Size = new System.Drawing.Size(74, 21);
            this.btnregion.TabIndex = 25;
            this.btnregion.Text = "Save";
            this.btnregion.UseVisualStyleBackColor = true;
            this.btnregion.Click += new System.EventHandler(this.btnregion_Click);
            // 
            // btnconsumables
            // 
            this.btnconsumables.Enabled = false;
            this.btnconsumables.Location = new System.Drawing.Point(438, 134);
            this.btnconsumables.Name = "btnconsumables";
            this.btnconsumables.Size = new System.Drawing.Size(74, 21);
            this.btnconsumables.TabIndex = 24;
            this.btnconsumables.Text = "Save";
            this.btnconsumables.UseVisualStyleBackColor = true;
            this.btnconsumables.Click += new System.EventHandler(this.btnconsumables_Click);
            // 
            // btntestproduct
            // 
            this.btntestproduct.Enabled = false;
            this.btntestproduct.Location = new System.Drawing.Point(438, 108);
            this.btntestproduct.Name = "btntestproduct";
            this.btntestproduct.Size = new System.Drawing.Size(74, 21);
            this.btntestproduct.TabIndex = 23;
            this.btntestproduct.Text = "Save";
            this.btntestproduct.UseVisualStyleBackColor = true;
            this.btntestproduct.Click += new System.EventHandler(this.btntestproduct_Click);
            // 
            // btninstrument
            // 
            this.btninstrument.Enabled = false;
            this.btninstrument.Location = new System.Drawing.Point(438, 81);
            this.btninstrument.Name = "btninstrument";
            this.btninstrument.Size = new System.Drawing.Size(74, 21);
            this.btninstrument.TabIndex = 22;
            this.btninstrument.Text = "Save";
            this.btninstrument.UseVisualStyleBackColor = true;
            this.btninstrument.Click += new System.EventHandler(this.btninstrument_Click);
            // 
            // btnproduct
            // 
            this.btnproduct.Enabled = false;
            this.btnproduct.Location = new System.Drawing.Point(438, 54);
            this.btnproduct.Name = "btnproduct";
            this.btnproduct.Size = new System.Drawing.Size(74, 21);
            this.btnproduct.TabIndex = 21;
            this.btnproduct.Text = "Save";
            this.btnproduct.UseVisualStyleBackColor = true;
            this.btnproduct.Click += new System.EventHandler(this.btnproduct_Click);
            // 
            // btnsavetest
            // 
            this.btnsavetest.Enabled = false;
            this.btnsavetest.Location = new System.Drawing.Point(438, 25);
            this.btnsavetest.Name = "btnsavetest";
            this.btnsavetest.Size = new System.Drawing.Size(74, 21);
            this.btnsavetest.TabIndex = 20;
            this.btnsavetest.Text = "Save";
            this.btnsavetest.UseVisualStyleBackColor = true;
            this.btnsavetest.Click += new System.EventHandler(this.btnsavetest_Click);
            // 
            // cmbvariable
            // 
            this.cmbvariable.FormattingEnabled = true;
            this.cmbvariable.Location = new System.Drawing.Point(245, 272);
            this.cmbvariable.Name = "cmbvariable";
            this.cmbvariable.Size = new System.Drawing.Size(177, 21);
            this.cmbvariable.TabIndex = 19;
            this.cmbvariable.SelectionChangeCommitted += new System.EventHandler(this.cmbvariable_SelectionChangeCommitted);
            // 
            // cmbreferralsite
            // 
            this.cmbreferralsite.FormattingEnabled = true;
            this.cmbreferralsite.Location = new System.Drawing.Point(245, 245);
            this.cmbreferralsite.Name = "cmbreferralsite";
            this.cmbreferralsite.Size = new System.Drawing.Size(177, 21);
            this.cmbreferralsite.TabIndex = 18;
            this.cmbreferralsite.SelectionChangeCommitted += new System.EventHandler(this.cmbreferralsite_SelectionChangeCommitted);
            // 
            // cmbsiteinstrument
            // 
            this.cmbsiteinstrument.FormattingEnabled = true;
            this.cmbsiteinstrument.Location = new System.Drawing.Point(245, 218);
            this.cmbsiteinstrument.Name = "cmbsiteinstrument";
            this.cmbsiteinstrument.Size = new System.Drawing.Size(177, 21);
            this.cmbsiteinstrument.TabIndex = 17;
            this.cmbsiteinstrument.SelectionChangeCommitted += new System.EventHandler(this.cmbsiteinstrument_SelectionChangeCommitted);
            // 
            // cmbsite
            // 
            this.cmbsite.FormattingEnabled = true;
            this.cmbsite.Location = new System.Drawing.Point(245, 191);
            this.cmbsite.Name = "cmbsite";
            this.cmbsite.Size = new System.Drawing.Size(177, 21);
            this.cmbsite.TabIndex = 16;
            this.cmbsite.SelectionChangeCommitted += new System.EventHandler(this.cmbsite_SelectionChangeCommitted);
            // 
            // cmbregion
            // 
            this.cmbregion.FormattingEnabled = true;
            this.cmbregion.Location = new System.Drawing.Point(245, 162);
            this.cmbregion.Name = "cmbregion";
            this.cmbregion.Size = new System.Drawing.Size(177, 21);
            this.cmbregion.TabIndex = 15;
            this.cmbregion.SelectionChangeCommitted += new System.EventHandler(this.cmbregion_SelectionChangeCommitted);
            // 
            // cmbconsumables
            // 
            this.cmbconsumables.FormattingEnabled = true;
            this.cmbconsumables.Location = new System.Drawing.Point(245, 135);
            this.cmbconsumables.Name = "cmbconsumables";
            this.cmbconsumables.Size = new System.Drawing.Size(177, 21);
            this.cmbconsumables.TabIndex = 14;
            this.cmbconsumables.SelectionChangeCommitted += new System.EventHandler(this.cmbconsumables_SelectionChangeCommitted);
            // 
            // cmbinstrument
            // 
            this.cmbinstrument.FormattingEnabled = true;
            this.cmbinstrument.Location = new System.Drawing.Point(245, 80);
            this.cmbinstrument.Name = "cmbinstrument";
            this.cmbinstrument.Size = new System.Drawing.Size(177, 21);
            this.cmbinstrument.TabIndex = 12;
            this.cmbinstrument.SelectionChangeCommitted += new System.EventHandler(this.cmbinstrument_SelectionChangeCommitted);
            // 
            // cmbtestproduct
            // 
            this.cmbtestproduct.FormattingEnabled = true;
            this.cmbtestproduct.Location = new System.Drawing.Point(245, 107);
            this.cmbtestproduct.Name = "cmbtestproduct";
            this.cmbtestproduct.Size = new System.Drawing.Size(177, 21);
            this.cmbtestproduct.TabIndex = 13;
            this.cmbtestproduct.SelectionChangeCommitted += new System.EventHandler(this.cmbtestproduct_SelectionChangeCommitted);
            // 
            // cmbproduct
            // 
            this.cmbproduct.FormattingEnabled = true;
            this.cmbproduct.Location = new System.Drawing.Point(245, 53);
            this.cmbproduct.Name = "cmbproduct";
            this.cmbproduct.Size = new System.Drawing.Size(177, 21);
            this.cmbproduct.TabIndex = 11;
            this.cmbproduct.SelectionChangeCommitted += new System.EventHandler(this.cmbproduct_SelectionChangeCommitted);
            // 
            // cmbtest
            // 
            this.cmbtest.FormattingEnabled = true;
            this.cmbtest.Location = new System.Drawing.Point(245, 25);
            this.cmbtest.Name = "cmbtest";
            this.cmbtest.Size = new System.Drawing.Size(177, 21);
            this.cmbtest.TabIndex = 10;
            this.cmbtest.SelectionChangeCommitted += new System.EventHandler(this.cmbtest_SelectionChangeCommitted);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 273);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(198, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Import Quantification Variable";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 246);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Referral Site";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Site Instrument";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 192);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Site";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(161, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Region/District/Province";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Consumables";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(166, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Test Product Usage Rate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Instrument";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Product";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(12, 392);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(589, 25);
            this.label11.TabIndex = 12;
            this.label11.Text = "*";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label12.Location = new System.Drawing.Point(12, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(589, 63);
            this.label12.TabIndex = 13;
            this.label12.Text = "*";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(13, 111);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(588, 23);
            this.progressBar1.TabIndex = 14;
            // 
            // btnExit
            // 
            this.btnExit.Enabled = false;
            this.btnExit.Location = new System.Drawing.Point(524, 138);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(77, 30);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "Done";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // Frmimportdata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 185);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.butClear);
            this.Controls.Add(this.txtFilename);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.butImport);
            this.Controls.Add(this.butBrowse);
            this.Name = "Frmimportdata";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Import Complete Data";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butClear;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butImport;
        private System.Windows.Forms.Button butBrowse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbvariable;
        private System.Windows.Forms.ComboBox cmbreferralsite;
        private System.Windows.Forms.ComboBox cmbsiteinstrument;
        private System.Windows.Forms.ComboBox cmbsite;
        private System.Windows.Forms.ComboBox cmbregion;
        private System.Windows.Forms.ComboBox cmbconsumables;
        private System.Windows.Forms.ComboBox cmbtestproduct;
        private System.Windows.Forms.ComboBox cmbinstrument;
        private System.Windows.Forms.ComboBox cmbproduct;
        private System.Windows.Forms.ComboBox cmbtest;
        private System.Windows.Forms.Button btnvariable;
        private System.Windows.Forms.Button btnreferralsite;
        private System.Windows.Forms.Button btnsiteinstrument;
        private System.Windows.Forms.Button btnsite;
        private System.Windows.Forms.Button btnregion;
        private System.Windows.Forms.Button btnconsumables;
        private System.Windows.Forms.Button btntestproduct;
        private System.Windows.Forms.Button btninstrument;
        private System.Windows.Forms.Button btnproduct;
        private System.Windows.Forms.Button btnsavetest;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button btnExit;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}