﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Domain;
using LQT.Core.Util;
using System.Data.Common;
using System.Data.OleDb;
using System.Runtime.InteropServices;

namespace LQT.GUI.Quantification
{
    public partial class Frmimportpatientno : Form
    {
        private DataSet Ds;
        IList<ForlabSite> result = new List<ForlabSite>();
        public DataTable patientData { get { return Dt; } }
        private DataTable Dt;
        private string sheetname;
        public Frmimportpatientno()
        {
            InitializeComponent();
        }

        private void butBrowse_Click(object sender, EventArgs e)
        {

            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtFilename.Text = openFileDialog1.FileName;
            }
        }


        public void changecolorexcel(string errormsg, Color c, int rowno)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();


                Microsoft.Office.Interop.Excel.Workbook workbook = application.Workbooks.Open(txtFilename.Text, Type.Missing, false);
                // Microsoft.Office.Interop.Excel.Worksheet worksheet1 = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Site Instrument"];

                Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[sheetname.Remove(sheetname.IndexOf('$')).Replace('\'', ' ').TrimStart()]; //.Remove(sheetname.IndexOf('$')).Replace('\'', ' ')
                // worksheet.Columns.Insert(3);

                Microsoft.Office.Interop.Excel.Range usedRange = worksheet.UsedRange;


                Microsoft.Office.Interop.Excel.Range row = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["A" + Convert.ToString(rowno + 2) + ":D" + Convert.ToString(rowno + 2) + ""];
                Microsoft.Office.Interop.Excel.Range commentcolumn = worksheet.get_Range("A" + Convert.ToString(rowno + 2) + "");
                row.EntireRow.Font.Color = System.Drawing.ColorTranslator.ToOle(c);
                commentcolumn.ClearComments();
                commentcolumn.AddComment(errormsg);


                application.DisplayAlerts = false;
                workbook.Save();

                application.Quit();
                Marshal.ReleaseComObject(worksheet);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(application);
            }
            catch (Exception ex)
            {
                // application.Quit();
            }
        }
        public  string PickSheet(string excelFile)
        {
            OleDbConnection objConn = null;
            DataTable dt = null;
            String connString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";

            objConn = new OleDbConnection(connString);

            objConn.Open();

            dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            List<String> excelSheets = new List<string>();

            foreach (DataRow row in dt.Rows)
            {
                excelSheets.Add(row["TABLE_NAME"].ToString());
            }

            objConn.Close();
            objConn.Dispose();

            FrmSelectSheet frmSelectSheet = new FrmSelectSheet(excelSheets);
            DialogResult dr = frmSelectSheet.ShowDialog();

            if (dr == DialogResult.OK)
            {
                sheetname = frmSelectSheet.SelectedSheet;
                return frmSelectSheet.SelectedSheet;
            }

            return "$Sheet1";
        }
        public  DataSet ReadExcelFile(string fpath, int noColumn)
        {
            string connectionString = String.Format(@"provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=YES;""", fpath);

            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            DbDataAdapter adapter = factory.CreateDataAdapter();
            DbCommand selectCommand = factory.CreateCommand();
            selectCommand.CommandText = "Select * From [" + PickSheet(fpath) + "]";
            DbConnection connection = factory.CreateConnection();
            DataSet ds = new DataSet();
            bool empty;

            try
            {
                connection.ConnectionString = connectionString;
                selectCommand.Connection = connection;
                adapter.SelectCommand = selectCommand;
                adapter.Fill(ds, "Consumption");

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    empty = true;

                    foreach (DataColumn col in ds.Tables[0].Columns)
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i][col].ToString().Trim()))
                            empty = false;

                    if (empty)
                        ds.Tables[0].Rows[i].Delete();
                }

                ds.Tables[0].AcceptChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }

            int colcount = ds.Tables[0].Columns.Count;

            if (noColumn > colcount)
                throw new Exception("Imported excel file has less columns than needed.");
            if (noColumn < colcount)
                throw new Exception("Imported excel file has too many columns.");
            if (ds.Tables[0].Rows.Count == 0)
                throw new Exception("Imported excel is empty.");
            return ds;
        }
        private void butImport_Click(object sender, EventArgs e)
        {
            string regionName;
        
            string siteName;
          
            string rName = "";
            ForlabRegion region = null;
         
            ForlabSite site = null;
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            result = DataRepository.GetAllSite();
            if (result.Count > 0)
            {

                try
                {
                   
                    DataSet ds = ReadExcelFile(txtFilename.Text, 4);
                    Dt = ds.Tables[0];


                    //Dt.Columns.Add("ErrorMsg", typeof(string));
                    //Dt.Columns.Add("rowno", typeof(int));
                    //for (var i = 0; i < Dt.Rows.Count; i++)
                    //{


                    //    Dt.Rows[i]["rowno"] = i;
                    //    regionName = Convert.ToString(Dt.Rows[i][0]).Trim();   
                    //    siteName = Convert.ToString(Dt.Rows[i][1]).Trim();
                    //    if (rName != regionName)
                    //    {
                    //        if (!string.IsNullOrEmpty(regionName))
                    //            region = DataRepository.GetRegionByName(regionName);
                    //        else
                    //            region = null;
                    //        rName = regionName;
                    //    }

                    //    if (region != null)
                    //    {

                    //        if (!String.IsNullOrEmpty(siteName))
                    //            if (DataRepository.GetSiteByName(siteName, region.Id) != null)
                    //            {


                    //            }
                    //            else
                    //            {
                    //                Dt.Rows[i]["ErrorMsg"] = "Site Doesn't Exist";

                    //            }
                    //    }
                    //    else
                    //    {
                    //        Dt.Rows[i]["ErrorMsg"] = "Region Doesn't Exist";
                    //    }

                    //}
                   
                    this.DialogResult = DialogResult.OK;
                    this.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }
    }
}
