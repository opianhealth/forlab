﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Domain;
using LQT.Core.Util;
using System.Collections;
using System.Data.SqlClient;

using LQT.Core.UserExceptions;
using LQT.GUI.UserCtr;
using LQT.Core;
using System.Runtime.InteropServices;

namespace LQT.GUI.Quantification
{
    public partial class FrmDynamicsitebysite : Form
    {
        private Form _mdiparent;
    
        private ForecastInfo _mMforecastinfo;
        private int _selectedRegionId = 0;
        private IList<MMForecastParameter> _mMForecastParams;
      //  private IList result;
        DataTable Dt = new DataTable();
        public SqlConnection sqlConnection = ConnectionManager.GetInstance().GetSqlConnection();
        private string sqlretrieve = "";
        IList<ForlabSite> result = new List<ForlabSite>();

        public FrmDynamicsitebysite(Form mdiparent, ForecastInfo finfo)
        {
            InitializeComponent();
            this._mdiparent = mdiparent;
            this._mMforecastinfo = finfo;
            PopRegion();

            this.Top = 120;
            //this.Width = _Width;
            //this.Height = _Height;
            this.Left = 250;

            btnadd.TabStop = false;
            btnadd.FlatStyle = FlatStyle.Flat;
            btnadd.FlatAppearance.BorderSize = 0;
            btnadd.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            btnRemove.TabStop = false;
            btnRemove.FlatStyle = FlatStyle.Flat;
            btnRemove.FlatAppearance.BorderSize = 0;
            btnRemove.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            btnprevious.TabStop = false;
            btnprevious.FlatStyle = FlatStyle.Flat;
            btnprevious.FlatAppearance.BorderSize = 0;
            btnprevious.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);

            btnnext.TabStop = false;
            btnnext.FlatStyle = FlatStyle.Flat;
            btnnext.FlatAppearance.BorderSize = 0;
            btnnext.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            _mMForecastParams = DataRepository.GetMMForecastParameterByProgramId(_mMforecastinfo.ProgramId, MorbidityVariableUsage.OnAllSite.ToString());
            if (_mMforecastinfo.Id > 0)
            {
                this.editRecordDisplay(_mMforecastinfo.Id);
            }
        }
        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            try
            {
                e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);

            }
            catch (Exception ex)
            { }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) //&& e.KeyChar != '.'
            {
                e.Handled = true;
            }

            // only allow one decimal point
            //if (e.KeyChar == '.'
            //    && (sender as TextBox).Text.IndexOf('.') > -1)
            //{
            //    e.Handled = true;
            //}
        }
        private void PopRegion()
        {
            lstRegion.BeginUpdate();
            lstRegion.Items.Clear();

            foreach (var items in DataRepository.GetAllRegion())
            {
                ListViewItem li = new ListViewItem(items.RegionName) { Tag = items.Id };
                if (items.Id == _selectedRegionId)
                {
                    li.Selected = true;
                }
                lstRegion.Items.Add(li);
            }

            lstRegion.EndUpdate();


        }
        public bool checkColumnExist()
        {
            if (sqlretrieve != "")
            {
                return true;
            }
            btnadd.Enabled = false;
            btnRemove.Enabled = false;
            btnnext.Enabled = false;
            MessageBox.Show("This program can not have any parameter", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;

        }
        private void editRecordDisplay(int Id)
        {
            
             string executesql = "";
            if (_mMForecastParams != null)
            {
                foreach (MMForecastParameter mMFParam in _mMForecastParams)
                {
                    sqlretrieve += "[" + mMFParam.VariableName + "],";
                 
                }
            }

            if (checkColumnExist() == true)
            {
                executesql = "Select sf.ID,sf.SiteID,SiteName," + sqlretrieve + " 'E' as Flag   From ForecastSiteInfo sf  Left Join site st on st.SiteID = sf.SiteID   where ForecastinfoID=" + _mMforecastinfo.Id + "";

                Dt = InsertRepository.SelectData(sqlConnection, executesql);

                for (int i = 0; i < Dt.Columns.Count; i++)
                {
                    if (Dt.Columns[i].ColumnName == "CurrentPatient")
                    {
                        Dt.Columns[i].ColumnName = "Current Patient on treatment";
                    }
                    else if (Dt.Columns[i].ColumnName == "TargetPatient")
                    {
                        Dt.Columns[i].ColumnName = "Target Patient + New Patient";

                    }

                }
                dataGridView1.DataSource = Dt;
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[dataGridView1.Columns.Count - 1].Visible = false;
            }
        }

        private void lstRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstRegion.SelectedItems.Count > 0)
            {
                int id = (int)lstRegion.SelectedItems[0].Tag;
                if (id != _selectedRegionId)
                {
                    _selectedRegionId = id;
                    lstSite.BeginUpdate();
                    lstSite.Items.Clear();
                    foreach (var items in DataRepository.GetAllSiteByRegionId(_selectedRegionId))
                    {
                        ListViewItem li = new ListViewItem(items.SiteName) { Tag = items.Id };
                        //if (items.Id == _selectedRegionId)
                        //{
                        //    li.Selected = true;
                        //}
                        lstSite.Items.Add(li);
                    }

                    lstSite.EndUpdate();

                }
            }
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            bool isduplicate = true;
            foreach (ListViewItem items in lstSite.SelectedItems)
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    if (row.Cells["SiteID"].Value != null)
                    {
                        if (row.Cells["SiteID"].Value.ToString() == items.Tag.ToString())
                        {
                            isduplicate = false;
                        }
                    }
                }

                if (isduplicate == false)
                {
                    MessageBox.Show("Site already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }
                else
                {
                    //if (Dt.Rows.Count > 0)
                    //{
                        DataRow dr = Dt.NewRow();
                        dr["ID"] = 0;
                        dr["SiteID"] = items.Tag;
                        dr["SiteName"] = items.Text;
                        if (_mMForecastParams != null)
                        {
                            foreach (MMForecastParameter mMFParam in _mMForecastParams)
                            {
                                if (mMFParam.VariableName == "CurrentPatient")
                                {
                                    dr["Current Patient on treatment"] = 0;
                                }
                                else if (mMFParam.VariableName == "TargetPatient")
                                {
                                    dr["Target Patient + New Patient"] = 0;
                                }
                                else
                                {
                                    dr[mMFParam.VariableName] = 0;
                                }
                            }
                        }
                  
                        dr["Flag"] = "N";
                        Dt.Rows.Add(dr);
                        Dt.AcceptChanges();
        
                    dataGridView1.DataSource = Dt;
                    dataGridView1.Columns[0].Visible = false;
                    dataGridView1.Columns[1].Visible = false;
                    dataGridView1.Columns[dataGridView1.Columns.Count - 1].Visible = false;

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmDyanmicMorbidity frm = new FrmDyanmicMorbidity(_mdiparent, _mMforecastinfo);
            frm.ShowDialog();
            this.Close();
        }

        private void btnnext_Click(object sender, EventArgs e)
        {
            string _sql = "";
            string _msg = "Site by site forecast inserted successfully";
            bool Isvalid = true;
            bool IsSave = false;
            int rowindex = 0;
            int colindex = 0;
            int Totaltarget = 0;
            string columns = "";
            string columnsvalues = "";
            try
            {
              ////  foreach (DataGridViewRow row in dataGridView1.Rows)
                ////{
                ////    if (Convert.ToInt32(row.Cells["ColCurrentPatient"].Value) == 0)
                ////    {

                ////        Isvalid = false;
                ////        rowindex = row.Index;
                ////        colindex = row.Cells["ColCurrentPatient"].ColumnIndex;
                ////        break;
                ////    }
                ////}
                ////if (Isvalid == false)
                ////{
                ////    dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                ////    MessageBox.Show("Current Patient must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ////    return;
                  

                ////}



                ////foreach (DataGridViewRow row in dataGridView1.Rows)
                ////{
                ////    if (Convert.ToInt32(row.Cells["ColTargetPatient"].Value) == 0)
                ////    {

                ////        Isvalid = false;
                ////        rowindex = row.Index;
                ////        colindex = row.Cells["ColTargetPatient"].ColumnIndex;
                ////    }
                ////}
                ////if (Isvalid == false)
                ////{
                ////    dataGridView1.CurrentCell = dataGridView1.Rows[rowindex].Cells[colindex];
                ////    MessageBox.Show("Target Patient must be greater than zero", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ////    return;
                 
                ////}

                //int Totaltarget = 0;
                DataTable table = new DataTable();
                table.Columns.Add("ID", typeof(int));
                table.Columns.Add("ForecastinfoID", typeof(int));
                table.Columns.Add("Name", typeof(string));
                //table.Columns.Add("CurrentPatient", typeof(int));
                //table.Columns.Add("TargetPatient", typeof(int));
              
               
                columns = "[SiteID],[ForecastinfoID],";
                _sql = "Delete from [ForecastSiteInfo] where ForecastinfoID=" + _mMforecastinfo.Id + " ";
                InsertRepository.ExcuteScripts(sqlConnection, _sql);
                foreach (MMForecastParameter mMFParam in _mMForecastParams)
                {
                    //   sqlretrieve += "[" + mMFParam.VariableName + "],";
                    columns+="[" + mMFParam.VariableName + "]," ;
                    table.Columns.Add("" + mMFParam.VariableName + "", typeof(int));
                    
                }
                   columns = columns.TrimEnd(',');
                    if (dataGridView1.Rows.Count > 0)
                    {
                        foreach (DataGridViewRow row in dataGridView1.Rows)
                        {
                                   //_sql = "Delete from [ForecastSiteInfo] where ID=" + row.Cells["ID"].Value + " ";
                                   //InsertRepository.ExcuteScripts(sqlConnection, _sql);
                                   columnsvalues = row.Cells["SiteID"].Value.ToString()+"," + Convert.ToString(_mMforecastinfo.Id) +",";
                                    for (int i = 3; i < row.Cells.Count - 1; i++)
                                    {

                                        columnsvalues += Convert.ToString(row.Cells[i].Value) +",";
                            
                                    }
                                    columnsvalues = columnsvalues.TrimEnd(',');
                                    _sql = "INSERT INTO [ForecastSiteInfo] (" + columns + ") VALUES(" + columnsvalues + ")";

                                    InsertRepository.ExcuteScripts(sqlConnection, _sql);
                                    Totaltarget = Totaltarget + Convert.ToInt32(row.Cells["Target Patient + New Patient"].Value);
                                    table.Rows.Add(Convert.ToInt32(row.Cells["SiteId"].Value), _mMforecastinfo.Id, row.Cells["SiteName"].Value, Convert.ToInt32(row.Cells["Current Patient on treatment"].Value), Convert.ToInt32(row.Cells["Target Patient + New Patient"].Value));
                           
                        }
                        ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Record Save/Update successfully..", true);
                        this.Hide();
                        DynamicPatientGroupRatio frm = new DynamicPatientGroupRatio(_mdiparent, _mMforecastinfo, Totaltarget, table);
                        //PatientGroupRatio frm = new PatientGroupRatio(_mdiparent, _selectedForcastId, "E", StartDate, EndDate, Period, Totaltarget, table);
                        // Form1 frm = new Form1();
                        frm.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Please select atleast one site", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;

                    }
                }
               
    
           
            catch (Exception ex)
            {
               // connection.Close();
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex > 2)
            {
                //for (int i = 3; i < dataGridView1.Rows[e.RowIndex].Cells.Count - 1; i++)
                //{
                    if (Convert.ToInt64(e.FormattedValue) == 0)
                    {

                        e.Cancel = true;
                        dataGridView1.Rows[e.RowIndex].ErrorText = "The value  must be greater than zero";

                        //  throw new LQTUserException("The value of Current Patient must be greater than zero");
                        MessageBox.Show("The value  must be greater than zero", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
               // }
            }
        }

        private void linkLabel3_Click(object sender, EventArgs e)
        {
            Frmimportpatientno frm = new Frmimportpatientno();
            DialogResult dr = frm.ShowDialog();
            DataTable Dt;
            if (dr == DialogResult.OK)
            {
                Dt = frm.patientData;
                FillGridData(Dt, frm);
            }
        }
        private void FillGridData(DataTable Dtsite, Frmimportpatientno importpatient)
        {

            string regionName;

            string siteName;
            bool error = false;
            int errorno = 0;
            string rName = "";
            ForlabRegion region = null;

            ForlabSite site = null;
            DataTable dt = new DataTable();
         
            dt.Columns.Add("ID");
            dt.Columns.Add("SiteID");
            dt.Columns.Add("SiteName");
            dt.Columns.Add("Current Patient on treatment");
            dt.Columns.Add("Target Patient + New Patient");
            dt.Columns.Add("Flag");
            result = DataRepository.GetAllSite();
            if (result.Count > 0)
            {

                try
                {




                    for (var i = 0; i < Dtsite.Rows.Count; i++)
                    {
                        label12.Text = "Importing Data.....................";

                        //  Dtsite.Rows[i]["rowno"] = i;
                        regionName = Convert.ToString(Dtsite.Rows[i][0]).Trim();
                        siteName = Convert.ToString(Dtsite.Rows[i][1]).Trim();
                        if (rName != regionName)
                        {
                            if (!string.IsNullOrEmpty(regionName))
                                region = DataRepository.GetRegionByName(regionName);
                            else
                                region = null;
                            rName = regionName;
                        }

                        if (region != null)
                        {

                            if (!String.IsNullOrEmpty(siteName))
                                site = DataRepository.GetSiteByName(siteName, region.Id);
                            if (site != null)
                            {
                                bool isduplicate = true;
                                //foreach (ListViewItem items in lstSite.SelectedItems)
                                //{
                                foreach (DataGridViewRow row in dataGridView1.Rows)
                                {
                                    if (row.Cells["SiteID"].Value != null)
                                    {
                                        if (row.Cells["SiteID"].Value.ToString() == Convert.ToString(site.Id))
                                        {
                                            isduplicate = false;
                                        }
                                    }
                                }

                                if (isduplicate == false)
                                {
                                    MessageBox.Show("Site already added", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;

                                }
                                else
                                {
                                    //if (Convert.ToInt32(Dtsite.Rows[i][2].ToString()) < Convert.ToInt32(Dtsite.Rows[i][3].ToString()))
                                    //{
                                    DataRow dr = dt.NewRow();
                                    dr["SiteID"] = Convert.ToString(site.Id);
                                    dr["SiteName"] = siteName;
                                    dr["Current Patient on treatment"] = Dtsite.Rows[i][2];
                                    dr["Target Patient + New Patient"] = Dtsite.Rows[i][3];
                                    dr["Flag"] = "N";
                                    dt.Rows.Add(dr);
                                    dt.AcceptChanges();
                                    dataGridView1.Columns[1].DataPropertyName = "SiteID";
                                    dataGridView1.Columns[2].DataPropertyName = "SiteName";
                                    dataGridView1.Columns[3].DataPropertyName = "Current Patient on treatment";
                                    dataGridView1.Columns[4].DataPropertyName = "Target Patient + New Patient";
                                    dataGridView1.Columns[5].DataPropertyName = "Flag";
                                    dataGridView1.DataSource = dt;
                                    //}
                                    //else
                                    //{
                                    //    errorno++;
                                    //    importpatient.changecolorexcel("Target Patient should be greater than Current Patient", Color.Red, i);
                                    //    error = true;
                                    //}
                                }
                                //}

                            }
                            else
                            {
                                //  Dtsite.Rows[i]["ErrorMsg"] = "Site Doesn't Exist";
                                errorno++;
                                importpatient.changecolorexcel("Site Doesn't Exist", Color.Red, i);
                                error = true;
                            }
                        }
                        else
                        {
                            // Dtsite.Rows[i]["ErrorMsg"] = "Region Doesn't Exist";
                            errorno++;
                            importpatient.changecolorexcel("Region Doesn't Exist", Color.Red, i);
                            error = true;
                        }
                        if (errorno > 20)
                        {
                            label12.Text = "";
                            throw new Exception("There too many problem with Site Instrument data, please troubleshoot and try to import again.");
                        }

                    }
                    if (error == true)
                        label12.Text = "Please Check excel to correct some records";
                    //this.DialogResult = DialogResult.OK;
                    //this.Close();

                }
                catch (Exception ex)
                {
                    label12.Text = "";
                    MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }



        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (ListViewItem items in lstSite.Items)
            {
                if (items.Selected == false)
                {
                    items.Selected = true;
                    items.BackColor = ColorTranslator.FromHtml("#3399FF");
                    items.ForeColor = Color.White;
                }
                else
                {
                    items.Selected = false;
                    items.BackColor = Color.White;
                    items.ForeColor = Color.Black;

                }

            }
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}
