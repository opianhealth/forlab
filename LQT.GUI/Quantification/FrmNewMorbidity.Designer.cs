﻿namespace LQT.GUI.Quantification
{
    partial class FrmNewMorbidity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.stepWizardControl1 = new AeroWizard.StepWizardControl();
            this.wizardPage1 = new AeroWizard.WizardPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanelWiz1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lvForecastMethod = new System.Windows.Forms.ListView();
            this.c123 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvGeneralAssumptions = new System.Windows.Forms.ListView();
            this.c11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvTestingProtocolOptions = new System.Windows.Forms.ListView();
            this.lvPatientGroup = new System.Windows.Forms.ListView();
            this.c001 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wizardPage2 = new AeroWizard.WizardPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rdbNoCtrl = new System.Windows.Forms.RadioButton();
            this.rdbincludeCtrl = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rdbaggregated = new System.Windows.Forms.RadioButton();
            this.rdbsitebysite = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dtpstart = new LQT.GUI.DurationPicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comPeriod = new System.Windows.Forms.ComboBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.cboscope = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.comForecastMethodWiz2 = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.wizardPage3 = new AeroWizard.WizardPage();
            this.wizardPage5 = new AeroWizard.WizardPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.flowLayoutPanelWiz4 = new System.Windows.Forms.FlowLayoutPanel();
            this.wizardPage4 = new AeroWizard.WizardPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.flowLayoutPanelWizPGroup = new System.Windows.Forms.FlowLayoutPanel();
            this.wizardPage6 = new AeroWizard.WizardPage();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.flowLayoutPanelPatientPopAss = new System.Windows.Forms.FlowLayoutPanel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.ddlpatientgroupPatient = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.btnaddpatientassumption = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.lvAssumptionPatient = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.wizardPage7 = new AeroWizard.WizardPage();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.flowLayoutPanelProdAssump = new System.Windows.Forms.FlowLayoutPanel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.ddlpatientgroupProduct = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.lvAssumptionProduct = new System.Windows.Forms.ListView();
            this.lvPAGroupName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvPAVarName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvPAValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.btnaddproductassumption = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.wizardPage8 = new AeroWizard.WizardPage();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel21 = new System.Windows.Forms.Panel();
            this.ddlpatientgroupTest = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.lvAssumptionTest = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.btnaddtestassumption = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.flowLayoutPanelTestAssump = new System.Windows.Forms.FlowLayoutPanel();
            this.wizardPage9 = new AeroWizard.WizardPage();
            this.wizardPage10 = new AeroWizard.WizardPage();
            this.wizardPage11 = new AeroWizard.WizardPage();
            this.wizardPage12 = new AeroWizard.WizardPage();
            this.wizardPage13 = new AeroWizard.WizardPage();
            ((System.ComponentModel.ISupportInitialize)(this.stepWizardControl1)).BeginInit();
            this.wizardPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.wizardPage2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel11.SuspendLayout();
            this.wizardPage5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel12.SuspendLayout();
            this.wizardPage4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.wizardPage6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel19.SuspendLayout();
            this.wizardPage7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.wizardPage8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel22.SuspendLayout();
            this.SuspendLayout();
            // 
            // stepWizardControl1
            // 
            this.stepWizardControl1.BackButtonToolTipText = "Back";
            this.stepWizardControl1.BackColor = System.Drawing.Color.White;
            this.stepWizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stepWizardControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stepWizardControl1.Location = new System.Drawing.Point(0, 0);
            this.stepWizardControl1.Name = "stepWizardControl1";
            this.stepWizardControl1.Pages.Add(this.wizardPage1);
            this.stepWizardControl1.Pages.Add(this.wizardPage2);
            this.stepWizardControl1.Pages.Add(this.wizardPage3);
            this.stepWizardControl1.Pages.Add(this.wizardPage5);
            this.stepWizardControl1.Pages.Add(this.wizardPage4);
            this.stepWizardControl1.Pages.Add(this.wizardPage6);
            this.stepWizardControl1.Pages.Add(this.wizardPage7);
            this.stepWizardControl1.Pages.Add(this.wizardPage8);
            this.stepWizardControl1.Pages.Add(this.wizardPage9);
            this.stepWizardControl1.Pages.Add(this.wizardPage10);
            this.stepWizardControl1.Pages.Add(this.wizardPage11);
            this.stepWizardControl1.Pages.Add(this.wizardPage12);
            this.stepWizardControl1.Pages.Add(this.wizardPage13);
            this.stepWizardControl1.ShowProgressInTaskbarIcon = true;
            this.stepWizardControl1.Size = new System.Drawing.Size(953, 574);
            this.stepWizardControl1.StepListFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.stepWizardControl1.StepListWidth = 200;
            this.stepWizardControl1.TabIndex = 0;
            this.stepWizardControl1.Text = "Morbidity Forecast";
            this.stepWizardControl1.Title = "Morbidity Forecast";
            this.stepWizardControl1.SelectedPageChanged += new System.EventHandler(this.stepWizardControl1_SelectedPageChanged);
            // 
            // wizardPage1
            // 
            this.wizardPage1.Controls.Add(this.panel6);
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage1, "Program/Model Selection");
            this.wizardPage1.TabIndex = 2;
            this.wizardPage1.Text = "Program/Model Selection";
            // 
            // panel6
            // 
            this.panel6.AutoScroll = true;
            this.panel6.Controls.Add(this.tableLayoutPanel1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(705, 420);
            this.panel6.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(699, 116);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanelWiz1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(699, 116);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(693, 22);
            this.label5.TabIndex = 7;
            this.label5.Text = "Select the Program Type you want to do forecast for.";
            // 
            // flowLayoutPanelWiz1
            // 
            this.flowLayoutPanelWiz1.AutoScroll = true;
            this.flowLayoutPanelWiz1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelWiz1.Location = new System.Drawing.Point(3, 25);
            this.flowLayoutPanelWiz1.Name = "flowLayoutPanelWiz1";
            this.flowLayoutPanelWiz1.Size = new System.Drawing.Size(693, 88);
            this.flowLayoutPanelWiz1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 125);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(699, 292);
            this.panel2.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lvForecastMethod, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lvGeneralAssumptions, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.lvTestingProtocolOptions, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.lvPatientGroup, 0, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 9;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(699, 292);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(3, 436);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.label6.Size = new System.Drawing.Size(693, 32);
            this.label6.TabIndex = 14;
            this.label6.Text = "Tesing Protocol Options";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(3, 254);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(693, 32);
            this.label4.TabIndex = 12;
            this.label4.Text = "General Assumptions";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(3, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(693, 32);
            this.label2.TabIndex = 10;
            this.label2.Text = "Patient/Population Group";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(693, 32);
            this.label1.TabIndex = 8;
            this.label1.Text = "Forecast Methods";
            // 
            // lvForecastMethod
            // 
            this.lvForecastMethod.BackColor = System.Drawing.SystemColors.Window;
            this.lvForecastMethod.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvForecastMethod.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c123});
            this.lvForecastMethod.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvForecastMethod.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvForecastMethod.GridLines = true;
            this.lvForecastMethod.Location = new System.Drawing.Point(3, 35);
            this.lvForecastMethod.Name = "lvForecastMethod";
            this.lvForecastMethod.Size = new System.Drawing.Size(693, 74);
            this.lvForecastMethod.TabIndex = 9;
            this.lvForecastMethod.UseCompatibleStateImageBehavior = false;
            this.lvForecastMethod.View = System.Windows.Forms.View.Details;
            // 
            // c123
            // 
            this.c123.Text = "Method";
            this.c123.Width = 481;
            // 
            // lvGeneralAssumptions
            // 
            this.lvGeneralAssumptions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvGeneralAssumptions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c11,
            this.c21,
            this.c31,
            this.c41});
            this.lvGeneralAssumptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvGeneralAssumptions.GridLines = true;
            this.lvGeneralAssumptions.Location = new System.Drawing.Point(3, 289);
            this.lvGeneralAssumptions.Name = "lvGeneralAssumptions";
            this.lvGeneralAssumptions.Size = new System.Drawing.Size(693, 144);
            this.lvGeneralAssumptions.TabIndex = 13;
            this.lvGeneralAssumptions.UseCompatibleStateImageBehavior = false;
            this.lvGeneralAssumptions.View = System.Windows.Forms.View.Details;
            // 
            // c11
            // 
            this.c11.Text = "Data Type";
            this.c11.Width = 81;
            // 
            // c21
            // 
            this.c21.Text = "Variable Name";
            this.c21.Width = 351;
            // 
            // c31
            // 
            this.c31.Text = "Formula";
            this.c31.Width = 252;
            // 
            // c41
            // 
            this.c41.Text = "Use On";
            this.c41.Width = 95;
            // 
            // lvTestingProtocolOptions
            // 
            this.lvTestingProtocolOptions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTestingProtocolOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvTestingProtocolOptions.Location = new System.Drawing.Point(3, 471);
            this.lvTestingProtocolOptions.Name = "lvTestingProtocolOptions";
            this.lvTestingProtocolOptions.Size = new System.Drawing.Size(693, 63);
            this.lvTestingProtocolOptions.TabIndex = 15;
            this.lvTestingProtocolOptions.UseCompatibleStateImageBehavior = false;
            // 
            // lvPatientGroup
            // 
            this.lvPatientGroup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvPatientGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c001});
            this.lvPatientGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvPatientGroup.GridLines = true;
            this.lvPatientGroup.Location = new System.Drawing.Point(3, 147);
            this.lvPatientGroup.Name = "lvPatientGroup";
            this.lvPatientGroup.Size = new System.Drawing.Size(693, 104);
            this.lvPatientGroup.TabIndex = 16;
            this.lvPatientGroup.UseCompatibleStateImageBehavior = false;
            this.lvPatientGroup.View = System.Windows.Forms.View.Details;
            // 
            // c001
            // 
            this.c001.Text = "Group Name";
            this.c001.Width = 477;
            // 
            // wizardPage2
            // 
            this.wizardPage2.Controls.Add(this.tableLayoutPanel4);
            this.wizardPage2.Name = "wizardPage2";
            this.wizardPage2.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage2, "Define Forecast");
            this.wizardPage2.TabIndex = 3;
            this.wizardPage2.Text = "Define Forecast";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.Controls.Add(this.panel5, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.panel9, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.panel4, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.panel10, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.panel3, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.panel8, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.panel7, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel11, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 8;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 213F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.rdbNoCtrl);
            this.panel5.Controls.Add(this.rdbincludeCtrl);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(15, 473);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(834, 139);
            this.panel5.TabIndex = 14;
            // 
            // rdbNoCtrl
            // 
            this.rdbNoCtrl.AutoSize = true;
            this.rdbNoCtrl.Location = new System.Drawing.Point(139, 39);
            this.rdbNoCtrl.Name = "rdbNoCtrl";
            this.rdbNoCtrl.Size = new System.Drawing.Size(41, 19);
            this.rdbNoCtrl.TabIndex = 13;
            this.rdbNoCtrl.Text = "No";
            this.rdbNoCtrl.UseVisualStyleBackColor = true;
            // 
            // rdbincludeCtrl
            // 
            this.rdbincludeCtrl.AutoSize = true;
            this.rdbincludeCtrl.Checked = true;
            this.rdbincludeCtrl.Location = new System.Drawing.Point(139, 14);
            this.rdbincludeCtrl.Name = "rdbincludeCtrl";
            this.rdbincludeCtrl.Size = new System.Drawing.Size(42, 19);
            this.rdbincludeCtrl.TabIndex = 12;
            this.rdbincludeCtrl.TabStop = true;
            this.rdbincludeCtrl.Text = "Yes";
            this.rdbincludeCtrl.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label9);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(15, 441);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(834, 26);
            this.panel9.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.label9.Size = new System.Drawing.Size(834, 26);
            this.label9.TabIndex = 11;
            this.label9.Text = "Control Included";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.rdbaggregated);
            this.panel4.Controls.Add(this.rdbsitebysite);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(15, 366);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(834, 69);
            this.panel4.TabIndex = 13;
            // 
            // rdbaggregated
            // 
            this.rdbaggregated.AutoSize = true;
            this.rdbaggregated.Location = new System.Drawing.Point(139, 38);
            this.rdbaggregated.Name = "rdbaggregated";
            this.rdbaggregated.Size = new System.Drawing.Size(163, 19);
            this.rdbaggregated.TabIndex = 19;
            this.rdbaggregated.Text = "Aggregated (groupd sites)";
            this.rdbaggregated.UseVisualStyleBackColor = true;
            // 
            // rdbsitebysite
            // 
            this.rdbsitebysite.AutoSize = true;
            this.rdbsitebysite.Checked = true;
            this.rdbsitebysite.Location = new System.Drawing.Point(139, 15);
            this.rdbsitebysite.Name = "rdbsitebysite";
            this.rdbsitebysite.Size = new System.Drawing.Size(86, 19);
            this.rdbsitebysite.TabIndex = 18;
            this.rdbsitebysite.TabStop = true;
            this.rdbsitebysite.Text = "Site-by-Site";
            this.rdbsitebysite.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label8);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(15, 334);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(834, 26);
            this.panel10.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.label8.Size = new System.Drawing.Size(834, 26);
            this.label8.TabIndex = 10;
            this.label8.Text = "Conduct Forecast In";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dtpstart);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.comPeriod);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.cboscope);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(15, 121);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(834, 207);
            this.panel3.TabIndex = 12;
            // 
            // dtpstart
            // 
            this.dtpstart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dtpstart.Location = new System.Drawing.Point(139, 168);
            this.dtpstart.Margin = new System.Windows.Forms.Padding(0);
            this.dtpstart.Name = "dtpstart";
            this.dtpstart.Size = new System.Drawing.Size(410, 27);
            this.dtpstart.TabIndex = 25;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(139, 129);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(412, 23);
            this.textBox3.TabIndex = 24;
            // 
            // comPeriod
            // 
            this.comPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPeriod.FormattingEnabled = true;
            this.comPeriod.Location = new System.Drawing.Point(139, 88);
            this.comPeriod.Name = "comPeriod";
            this.comPeriod.Size = new System.Drawing.Size(412, 23);
            this.comPeriod.TabIndex = 23;
            this.comPeriod.SelectedIndexChanged += new System.EventHandler(this.comPeriod_SelectedIndexChanged_1);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(346, 47);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(205, 23);
            this.textBox2.TabIndex = 22;
            // 
            // cboscope
            // 
            this.cboscope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboscope.FormattingEnabled = true;
            this.cboscope.Items.AddRange(new object[] {
            "NATIONAL",
            "PROGRAM",
            "SITE",
            "GLOBAL",
            "CUSTOM"});
            this.cboscope.Location = new System.Drawing.Point(139, 48);
            this.cboscope.Name = "cboscope";
            this.cboscope.Size = new System.Drawing.Size(200, 23);
            this.cboscope.TabIndex = 21;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(139, 8);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(412, 23);
            this.textBox1.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 176);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 15);
            this.label14.TabIndex = 4;
            this.label14.Text = "Start Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 132);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 15);
            this.label13.TabIndex = 3;
            this.label13.Text = "Forecast Period";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 15);
            this.label12.TabIndex = 2;
            this.label12.Text = "Reporting Period";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 15);
            this.label11.TabIndex = 1;
            this.label11.Text = "Scope of Forecast";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 15);
            this.label10.TabIndex = 0;
            this.label10.Text = "Forecast Id/Name";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label7);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(15, 89);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(834, 26);
            this.panel8.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(0, 0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.label7.Size = new System.Drawing.Size(834, 26);
            this.label7.TabIndex = 9;
            this.label7.Text = "Forecast Basic Detail";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(15, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(834, 26);
            this.panel7.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 4, 0, 0);
            this.label3.Size = new System.Drawing.Size(834, 26);
            this.label3.TabIndex = 10;
            this.label3.Text = "Select Forecast Method";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.comForecastMethodWiz2);
            this.panel11.Controls.Add(this.label15);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(15, 35);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(834, 48);
            this.panel11.TabIndex = 19;
            // 
            // comForecastMethodWiz2
            // 
            this.comForecastMethodWiz2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comForecastMethodWiz2.FormattingEnabled = true;
            this.comForecastMethodWiz2.Location = new System.Drawing.Point(139, 11);
            this.comForecastMethodWiz2.Name = "comForecastMethodWiz2";
            this.comForecastMethodWiz2.Size = new System.Drawing.Size(412, 23);
            this.comForecastMethodWiz2.TabIndex = 25;
            this.comForecastMethodWiz2.SelectedIndexChanged += new System.EventHandler(this.comForecastMethodWiz2_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 15);
            this.label15.TabIndex = 24;
            this.label15.Text = "Forecast Method";
            // 
            // wizardPage3
            // 
            this.wizardPage3.Name = "wizardPage3";
            this.wizardPage3.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage3, "Site/Category Selection");
            this.wizardPage3.TabIndex = 4;
            this.wizardPage3.Text = "Site/Category Selection";
            // 
            // wizardPage5
            // 
            this.wizardPage5.Controls.Add(this.tableLayoutPanel5);
            this.wizardPage5.Name = "wizardPage5";
            this.wizardPage5.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage5, "Program/Model Variables");
            this.wizardPage5.TabIndex = 6;
            this.wizardPage5.Text = "Program/Model Variables";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.panel12, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanelWiz4, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label16);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(23, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(679, 22);
            this.panel12.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label16.ForeColor = System.Drawing.Color.DimGray;
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label16.Size = new System.Drawing.Size(679, 22);
            this.label16.TabIndex = 8;
            this.label16.Text = "Program/Model Variables List";
            // 
            // flowLayoutPanelWiz4
            // 
            this.flowLayoutPanelWiz4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelWiz4.Location = new System.Drawing.Point(23, 31);
            this.flowLayoutPanelWiz4.Name = "flowLayoutPanelWiz4";
            this.flowLayoutPanelWiz4.Size = new System.Drawing.Size(679, 386);
            this.flowLayoutPanelWiz4.TabIndex = 1;
            // 
            // wizardPage4
            // 
            this.wizardPage4.Controls.Add(this.tableLayoutPanel6);
            this.wizardPage4.Name = "wizardPage4";
            this.wizardPage4.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage4, "Patient/Population Group Ratio");
            this.wizardPage4.TabIndex = 7;
            this.wizardPage4.Text = "Patient/Population Group Ratio";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.panel13, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.flowLayoutPanelWizPGroup, 1, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label17);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(23, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(679, 22);
            this.panel13.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label17.Size = new System.Drawing.Size(679, 22);
            this.label17.TabIndex = 8;
            this.label17.Text = "Patient/Population Group";
            // 
            // flowLayoutPanelWizPGroup
            // 
            this.flowLayoutPanelWizPGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelWizPGroup.Location = new System.Drawing.Point(23, 31);
            this.flowLayoutPanelWizPGroup.Name = "flowLayoutPanelWizPGroup";
            this.flowLayoutPanelWizPGroup.Size = new System.Drawing.Size(679, 386);
            this.flowLayoutPanelWizPGroup.TabIndex = 1;
            // 
            // wizardPage6
            // 
            this.wizardPage6.Controls.Add(this.tableLayoutPanel7);
            this.wizardPage6.Name = "wizardPage6";
            this.wizardPage6.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage6, "Patient Population Assumption");
            this.wizardPage6.TabIndex = 8;
            this.wizardPage6.Text = "Patient Population Assumption";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.panel14, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.flowLayoutPanelPatientPopAss, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.panel16, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.panel19, 1, 3);
            this.tableLayoutPanel7.Controls.Add(this.lvAssumptionPatient, 1, 4);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label18);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(23, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(679, 22);
            this.panel14.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label18.ForeColor = System.Drawing.Color.DimGray;
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label18.Size = new System.Drawing.Size(679, 22);
            this.label18.TabIndex = 8;
            this.label18.Text = "Patient Population Assumption";
            // 
            // flowLayoutPanelPatientPopAss
            // 
            this.flowLayoutPanelPatientPopAss.AutoScroll = true;
            this.flowLayoutPanelPatientPopAss.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanelPatientPopAss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelPatientPopAss.Location = new System.Drawing.Point(23, 77);
            this.flowLayoutPanelPatientPopAss.Name = "flowLayoutPanelPatientPopAss";
            this.flowLayoutPanelPatientPopAss.Size = new System.Drawing.Size(679, 112);
            this.flowLayoutPanelPatientPopAss.TabIndex = 1;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.ddlpatientgroupPatient);
            this.panel16.Controls.Add(this.label20);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(23, 31);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(679, 40);
            this.panel16.TabIndex = 2;
            // 
            // ddlpatientgroupPatient
            // 
            this.ddlpatientgroupPatient.DisplayMember = "GroupName";
            this.ddlpatientgroupPatient.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlpatientgroupPatient.FormattingEnabled = true;
            this.ddlpatientgroupPatient.Location = new System.Drawing.Point(192, 7);
            this.ddlpatientgroupPatient.Name = "ddlpatientgroupPatient";
            this.ddlpatientgroupPatient.Size = new System.Drawing.Size(412, 23);
            this.ddlpatientgroupPatient.TabIndex = 26;
            this.ddlpatientgroupPatient.ValueMember = "Id";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(177, 15);
            this.label20.TabIndex = 0;
            this.label20.Text = "Select Patient/Population Group";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.panel23);
            this.panel19.Controls.Add(this.btnaddpatientassumption);
            this.panel19.Controls.Add(this.label23);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(23, 195);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(679, 44);
            this.panel19.TabIndex = 3;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.DimGray;
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(679, 2);
            this.panel23.TabIndex = 3;
            // 
            // btnaddpatientassumption
            // 
            this.btnaddpatientassumption.Location = new System.Drawing.Point(529, 17);
            this.btnaddpatientassumption.Name = "btnaddpatientassumption";
            this.btnaddpatientassumption.Size = new System.Drawing.Size(75, 23);
            this.btnaddpatientassumption.TabIndex = 1;
            this.btnaddpatientassumption.Text = "Add";
            this.btnaddpatientassumption.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.DimGray;
            this.label23.Location = new System.Drawing.Point(9, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(92, 15);
            this.label23.TabIndex = 0;
            this.label23.Text = "Assumption List";
            // 
            // lvAssumptionPatient
            // 
            this.lvAssumptionPatient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvAssumptionPatient.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvAssumptionPatient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAssumptionPatient.GridLines = true;
            this.lvAssumptionPatient.Location = new System.Drawing.Point(23, 245);
            this.lvAssumptionPatient.Name = "lvAssumptionPatient";
            this.lvAssumptionPatient.Size = new System.Drawing.Size(679, 172);
            this.lvAssumptionPatient.TabIndex = 4;
            this.lvAssumptionPatient.UseCompatibleStateImageBehavior = false;
            this.lvAssumptionPatient.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Group Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Variable Name";
            this.columnHeader2.Width = 314;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 145;
            // 
            // wizardPage7
            // 
            this.wizardPage7.Controls.Add(this.tableLayoutPanel8);
            this.wizardPage7.Name = "wizardPage7";
            this.wizardPage7.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage7, "Product Assumption");
            this.wizardPage7.TabIndex = 9;
            this.wizardPage7.Text = "Product Assumption";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.panel15, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.flowLayoutPanelProdAssump, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.panel17, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.lvAssumptionProduct, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.panel18, 1, 3);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 5;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label19);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(23, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(679, 22);
            this.panel15.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label19.ForeColor = System.Drawing.Color.DimGray;
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label19.Size = new System.Drawing.Size(679, 22);
            this.label19.TabIndex = 8;
            this.label19.Text = "Product Assumption";
            // 
            // flowLayoutPanelProdAssump
            // 
            this.flowLayoutPanelProdAssump.AutoScroll = true;
            this.flowLayoutPanelProdAssump.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanelProdAssump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelProdAssump.Location = new System.Drawing.Point(23, 77);
            this.flowLayoutPanelProdAssump.Name = "flowLayoutPanelProdAssump";
            this.flowLayoutPanelProdAssump.Size = new System.Drawing.Size(679, 112);
            this.flowLayoutPanelProdAssump.TabIndex = 1;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.ddlpatientgroupProduct);
            this.panel17.Controls.Add(this.label21);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(23, 31);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(679, 40);
            this.panel17.TabIndex = 2;
            // 
            // ddlpatientgroupProduct
            // 
            this.ddlpatientgroupProduct.DisplayMember = "GroupName";
            this.ddlpatientgroupProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlpatientgroupProduct.FormattingEnabled = true;
            this.ddlpatientgroupProduct.Location = new System.Drawing.Point(192, 7);
            this.ddlpatientgroupProduct.Name = "ddlpatientgroupProduct";
            this.ddlpatientgroupProduct.Size = new System.Drawing.Size(412, 23);
            this.ddlpatientgroupProduct.TabIndex = 26;
            this.ddlpatientgroupProduct.ValueMember = "Id";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(9, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(177, 15);
            this.label21.TabIndex = 0;
            this.label21.Text = "Select Patient/Population Group";
            // 
            // lvAssumptionProduct
            // 
            this.lvAssumptionProduct.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvAssumptionProduct.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvPAGroupName,
            this.lvPAVarName,
            this.lvPAValue});
            this.lvAssumptionProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAssumptionProduct.GridLines = true;
            this.lvAssumptionProduct.Location = new System.Drawing.Point(23, 245);
            this.lvAssumptionProduct.Name = "lvAssumptionProduct";
            this.lvAssumptionProduct.Size = new System.Drawing.Size(679, 172);
            this.lvAssumptionProduct.TabIndex = 4;
            this.lvAssumptionProduct.UseCompatibleStateImageBehavior = false;
            this.lvAssumptionProduct.View = System.Windows.Forms.View.Details;
            // 
            // lvPAGroupName
            // 
            this.lvPAGroupName.Text = "Group Name";
            this.lvPAGroupName.Width = 150;
            // 
            // lvPAVarName
            // 
            this.lvPAVarName.Text = "Variable Name";
            this.lvPAVarName.Width = 314;
            // 
            // lvPAValue
            // 
            this.lvPAValue.Text = "Value";
            this.lvPAValue.Width = 145;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.panel24);
            this.panel18.Controls.Add(this.btnaddproductassumption);
            this.panel18.Controls.Add(this.label22);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(23, 195);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(679, 44);
            this.panel18.TabIndex = 5;
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.DimGray;
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(0, 0);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(679, 2);
            this.panel24.TabIndex = 4;
            // 
            // btnaddproductassumption
            // 
            this.btnaddproductassumption.Location = new System.Drawing.Point(529, 17);
            this.btnaddproductassumption.Name = "btnaddproductassumption";
            this.btnaddproductassumption.Size = new System.Drawing.Size(75, 23);
            this.btnaddproductassumption.TabIndex = 1;
            this.btnaddproductassumption.Text = "Add";
            this.btnaddproductassumption.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.DimGray;
            this.label22.Location = new System.Drawing.Point(9, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 15);
            this.label22.TabIndex = 0;
            this.label22.Text = "Assumption List";
            // 
            // wizardPage8
            // 
            this.wizardPage8.Controls.Add(this.tableLayoutPanel9);
            this.wizardPage8.Name = "wizardPage8";
            this.wizardPage8.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage8, "Test Assumption");
            this.wizardPage8.TabIndex = 10;
            this.wizardPage8.Text = "Test Assumption";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.panel20, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.panel21, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.lvAssumptionTest, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.panel22, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.flowLayoutPanelTestAssump, 1, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 5;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(705, 420);
            this.tableLayoutPanel9.TabIndex = 4;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.label24);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(23, 3);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(679, 22);
            this.panel20.TabIndex = 0;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label24.ForeColor = System.Drawing.Color.DimGray;
            this.label24.Location = new System.Drawing.Point(0, 0);
            this.label24.Name = "label24";
            this.label24.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.label24.Size = new System.Drawing.Size(679, 22);
            this.label24.TabIndex = 8;
            this.label24.Text = "Test Assumption";
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.ddlpatientgroupTest);
            this.panel21.Controls.Add(this.label25);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(23, 31);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(679, 40);
            this.panel21.TabIndex = 2;
            // 
            // ddlpatientgroupTest
            // 
            this.ddlpatientgroupTest.DisplayMember = "GroupName";
            this.ddlpatientgroupTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlpatientgroupTest.FormattingEnabled = true;
            this.ddlpatientgroupTest.Location = new System.Drawing.Point(192, 7);
            this.ddlpatientgroupTest.Name = "ddlpatientgroupTest";
            this.ddlpatientgroupTest.Size = new System.Drawing.Size(412, 23);
            this.ddlpatientgroupTest.TabIndex = 26;
            this.ddlpatientgroupTest.ValueMember = "Id";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(9, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(177, 15);
            this.label25.TabIndex = 0;
            this.label25.Text = "Select Patient/Population Group";
            // 
            // lvAssumptionTest
            // 
            this.lvAssumptionTest.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvAssumptionTest.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvAssumptionTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvAssumptionTest.GridLines = true;
            this.lvAssumptionTest.Location = new System.Drawing.Point(23, 245);
            this.lvAssumptionTest.Name = "lvAssumptionTest";
            this.lvAssumptionTest.Size = new System.Drawing.Size(679, 172);
            this.lvAssumptionTest.TabIndex = 4;
            this.lvAssumptionTest.UseCompatibleStateImageBehavior = false;
            this.lvAssumptionTest.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Group Name";
            this.columnHeader4.Width = 150;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Variable Name";
            this.columnHeader5.Width = 314;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Value";
            this.columnHeader6.Width = 145;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.panel25);
            this.panel22.Controls.Add(this.btnaddtestassumption);
            this.panel22.Controls.Add(this.label26);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(23, 195);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(679, 44);
            this.panel22.TabIndex = 5;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.DimGray;
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(0, 0);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(679, 2);
            this.panel25.TabIndex = 4;
            // 
            // btnaddtestassumption
            // 
            this.btnaddtestassumption.Location = new System.Drawing.Point(529, 17);
            this.btnaddtestassumption.Name = "btnaddtestassumption";
            this.btnaddtestassumption.Size = new System.Drawing.Size(75, 23);
            this.btnaddtestassumption.TabIndex = 1;
            this.btnaddtestassumption.Text = "Add";
            this.btnaddtestassumption.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.DimGray;
            this.label26.Location = new System.Drawing.Point(9, 20);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(92, 15);
            this.label26.TabIndex = 0;
            this.label26.Text = "Assumption List";
            // 
            // flowLayoutPanelTestAssump
            // 
            this.flowLayoutPanelTestAssump.AutoScroll = true;
            this.flowLayoutPanelTestAssump.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanelTestAssump.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelTestAssump.Location = new System.Drawing.Point(23, 77);
            this.flowLayoutPanelTestAssump.Name = "flowLayoutPanelTestAssump";
            this.flowLayoutPanelTestAssump.Size = new System.Drawing.Size(679, 112);
            this.flowLayoutPanelTestAssump.TabIndex = 1;
            // 
            // wizardPage9
            // 
            this.wizardPage9.Name = "wizardPage9";
            this.wizardPage9.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage9, "Test Selection");
            this.wizardPage9.TabIndex = 11;
            this.wizardPage9.Text = "Test Selection";
            // 
            // wizardPage10
            // 
            this.wizardPage10.Name = "wizardPage10";
            this.wizardPage10.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage10, "Rapid Test Protocol");
            this.wizardPage10.TabIndex = 12;
            this.wizardPage10.Text = "Rapid Test Protocol";
            // 
            // wizardPage11
            // 
            this.wizardPage11.Name = "wizardPage11";
            this.wizardPage11.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage11, "General Test Protocol");
            this.wizardPage11.TabIndex = 13;
            this.wizardPage11.Text = "General Test Protocol";
            // 
            // wizardPage12
            // 
            this.wizardPage12.Name = "wizardPage12";
            this.wizardPage12.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage12, "Review");
            this.wizardPage12.TabIndex = 14;
            this.wizardPage12.Text = "Review";
            // 
            // wizardPage13
            // 
            this.wizardPage13.Name = "wizardPage13";
            this.wizardPage13.Size = new System.Drawing.Size(705, 420);
            this.stepWizardControl1.SetStepText(this.wizardPage13, "Report");
            this.wizardPage13.TabIndex = 15;
            this.wizardPage13.Text = "Report";
            // 
            // FrmNewMorbidity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(953, 574);
            this.Controls.Add(this.stepWizardControl1);
            this.Name = "FrmNewMorbidity";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Morbidity Quantification";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.stepWizardControl1)).EndInit();
            this.wizardPage1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.wizardPage2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.wizardPage5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.wizardPage4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.wizardPage6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.wizardPage7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.wizardPage8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private AeroWizard.StepWizardControl stepWizardControl1;
        private AeroWizard.WizardPage wizardPage1;
        private AeroWizard.WizardPage wizardPage2;
        private AeroWizard.WizardPage wizardPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelWiz1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvForecastMethod;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lvGeneralAssumptions;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lvTestingProtocolOptions;
        private System.Windows.Forms.ListView lvPatientGroup;
        private System.Windows.Forms.ColumnHeader c11;
        private System.Windows.Forms.ColumnHeader c21;
        private System.Windows.Forms.ColumnHeader c31;
        private System.Windows.Forms.ColumnHeader c41;
        private System.Windows.Forms.ColumnHeader c001;
        private System.Windows.Forms.ColumnHeader c123;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ComboBox cboscope;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.ComboBox comPeriod;
        private DurationPicker dtpstart;
        private System.Windows.Forms.RadioButton rdbaggregated;
        private System.Windows.Forms.RadioButton rdbsitebysite;
        private System.Windows.Forms.RadioButton rdbNoCtrl;
        private System.Windows.Forms.RadioButton rdbincludeCtrl;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.ComboBox comForecastMethodWiz2;
        private System.Windows.Forms.Label label15;
        private AeroWizard.WizardPage wizardPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelWiz4;
        private AeroWizard.WizardPage wizardPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelWizPGroup;
        private AeroWizard.WizardPage wizardPage6;
        private AeroWizard.WizardPage wizardPage7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelProdAssump;
        private AeroWizard.WizardPage wizardPage8;
        private AeroWizard.WizardPage wizardPage9;
        private AeroWizard.WizardPage wizardPage10;
        private AeroWizard.WizardPage wizardPage11;
        private AeroWizard.WizardPage wizardPage12;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.ComboBox ddlpatientgroupProduct;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListView lvAssumptionProduct;
        private System.Windows.Forms.ColumnHeader lvPAGroupName;
        private System.Windows.Forms.ColumnHeader lvPAVarName;
        private System.Windows.Forms.ColumnHeader lvPAValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelPatientPopAss;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.ComboBox ddlpatientgroupPatient;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button btnaddpatientassumption;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListView lvAssumptionPatient;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelTestAssump;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.ComboBox ddlpatientgroupTest;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ListView lvAssumptionTest;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Button btnaddproductassumption;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Button btnaddtestassumption;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private AeroWizard.WizardPage wizardPage13;

    }
}