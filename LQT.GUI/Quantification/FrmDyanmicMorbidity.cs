﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Domain;
using LQT.Core.Util;
using System.Text.RegularExpressions;

namespace LQT.GUI.Quantification
{
    public partial class FrmDyanmicMorbidity : Form
    {
        private Form _mdiparent;
        private int _selectedForcastId = 0;
        public string OldForcastId = "0";
        public static string Mode = "";
       
        DataTable dt = new DataTable();
        private ForecastInfo _mMforecastinfo;
        public FrmDyanmicMorbidity(Form mdiparent, ForecastInfo finfo)
        {
            InitializeComponent();
         //   _selectedForcastId = id;
            _mMforecastinfo = finfo;
            _mdiparent = mdiparent;
           
            this.fillPeriod();
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.BorderColor = Color.FromArgb(0, 255, 255, 255);
            txtForecastID.KeyPress += new KeyPressEventHandler(txtForecastID_KeyPress);
           

            dpStart.Format = DateTimePickerFormat.Custom;
            dpStart.CustomFormat = "MMMM yyyy";
            dpStart.ShowUpDown = true;
            dpEnd.Format = DateTimePickerFormat.Custom;
            dpEnd.CustomFormat = "MMMM yyyy";
            dpEnd.ShowUpDown = true;
            this.Top = 120;
         
            this.Left = 250;

            if (_mMforecastinfo.Id > 0)
            {
                this.editRecordDisplay1(_mMforecastinfo.Id);
            }
        }
        private void txtForecastID_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                if (e.KeyChar == (char)Keys.Back) e.Handled = false;
                else e.Handled = true;
            }
        }
        private void fillPeriod()
        {
            string[] periods = Enum.GetNames(typeof(ForecastPeriodEnum));

            cbRPeriod.Items.Clear();
            for (int i = 0; i < periods.Length; i++)
            {
                cbRPeriod.Items.Add(periods[i]);
            }
        }
        private void editRecordDisplay1(int ID)
        {
            _mMforecastinfo = DataRepository.GetForecastInfoById(ID);        
            txtForecastID.Text = _mMforecastinfo.ForecastNo.ToString();
            OldForcastId = _mMforecastinfo.ForecastNo.ToString();
            dpStart.Text = _mMforecastinfo.StartDate.ToString();
            dpEnd.Text = _mMforecastinfo.ForecastDate.ToString();
            cbRPeriod.Text = _mMforecastinfo.Period.ToString();
            if (_mMforecastinfo.ForecastType.Equals("S"))
            {
                rdSBSForecast.Checked = true;
            }
            else rbAForecast.Checked = true;
            rdSBSForecast.Enabled = false;
            rbAForecast.Enabled = false;

       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime _startDate = DateTime.Parse(dpStart.Value.ToString());
            DateTime _endDate = DateTime.Parse(dpEnd.Value.ToString());
            int year = _endDate.Year;
            int day = _startDate.Day;
            int month = _endDate.Month;
            DateTime dtF = new DateTime(year, month, day); 
            if (this.txtForecastID.Text == string.Empty)//b
            {
                MessageBox.Show("ForecastID must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                if (_mMforecastinfo.Id <= 0 || (_mMforecastinfo.Id > 0 && _mMforecastinfo.ForecastNo != txtForecastID.Text))
                {
                    IList<ForecastInfo> _forcastinfo = DataRepository.GetAllForecastInfo();//b
                    for (int i = 0; i < _forcastinfo.Count; i++)//b
                    //foreach (ForecastInfo info in _forecastInfo)
                    {
                        if (_forcastinfo[i].ForecastNo == txtForecastID.Text)
                        {
                            MessageBox.Show("ForecastID must not be duplicate.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                }



                _mMforecastinfo.ForecastNo = this.txtForecastID.Text;
            }
            if (dpEnd.Value < dpStart.Value)
            {
                //MessageBox.Show("Forecast End Date must be greater then Forecast Start Date.");
                MessageBox.Show("Forecast End Date must be greater then Forecast Start Date.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return ;
            }
            if (cbRPeriod.Text.Trim() == string.Empty)
            {
                //MessageBox.Show("Period must not be empty.");
                MessageBox.Show("Period must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cbRPeriod.Focus();
                return ;
            }
            if (rbAForecast.Checked == false && rdSBSForecast.Checked == false)
            {
                //MessageBox.Show("Atleast one Forecast Type must be selected.");
                MessageBox.Show("Atleast one Forecast Type must be selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return ;
            }

         
            _mMforecastinfo.Methodology = "MORBIDITY";
            _mMforecastinfo.DataUsage = "DATA_USAGE1";
            _mMforecastinfo.Status = "OPEN";
            _mMforecastinfo.StartDate= Convert.ToDateTime(dpStart.Text);
            _mMforecastinfo.Period = cbRPeriod.Text;
            _mMforecastinfo.ForecastDate = Convert.ToDateTime(dtF.ToString("MMMM yyyy"));
            _mMforecastinfo.Method= "Linear";
            _mMforecastinfo.SlowMovingPeriod = cbRPeriod.Text;
            if (rdSBSForecast.Checked == true)
                _mMforecastinfo.ForecastType = "S";
            else
                _mMforecastinfo.ForecastType = "C";
            _mMforecastinfo.Extension = 4;
            _mMforecastinfo.LastUpdated = DateTime.Now;
            DataRepository.SaveOrUpdateForecastInfo(_mMforecastinfo);
            ((LqtMainWindowForm)_mdiparent).ShowStatusBarInfo("Forecast was Saved successfully.", true);
            _selectedForcastId = _mMforecastinfo.Id;

            if (_mMforecastinfo.ForecastType == "S")
            {
                this.Hide();
                FrmDynamicsitebysite frm = new FrmDynamicsitebysite(_mdiparent,_mMforecastinfo);
                frm.ShowDialog();

                this.Close();
            }
            if (_mMforecastinfo.ForecastType == "C")
            {
                this.Hide();
                DynamicAggregateForecast frm = new DynamicAggregateForecast(_mdiparent, _mMforecastinfo);
                frm.ShowDialog();

                this.Close();
            }

        }
    }
}
