﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.UserCtr;
using LQT.Core.UserExceptions;
using LQT.GUI.MorbidityUserCtr;

namespace LQT.GUI.MorbidityProtocolSetting
{
    public partial class FrmNewProtocol : Form
    {
        private ClassOfMorbidityTestEnum _classOfTest;
        private Protocol _protocol;
        private Form _mdiparent;
        private bool _isedited = false;

        private ProtocolPanel _cd4Panel;
        private SiteListView _cd4ListView;
        private SiteListView _chemListView;
        private SiteListView _othListView;

        public FrmNewProtocol()
        {
     
           
            InitializeComponent();

            lqtToolStrip1.SaveAndCloseClick += new EventHandler(lqtToolStrip1_SaveAndCloseClick);
            lqtToolStrip1.DisableSaveAndNewButton();
            //BindProtocol();
         
        }

        private void lsvpanel_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsvpanel.SelectedItems.Count > 0)
            {
                this.butEditpanel.Enabled = true;
                this.butDeletepanel.Enabled = true;
            }
            else
            {
                this.butEditpanel.Enabled = false;
                this.butDeletepanel.Enabled = false;
            }
        }

        void lqtToolStrip1_SaveAndCloseClick(object sender, EventArgs e)
        {
            try
            {
                //LQTUserMessage msg = SaveOrUpdateObject();               
                //DataRepository.CloseSession();
                this.Close();
            }
            catch (Exception ex)
            {
                new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();
            }
        }

        private void FrmProtocol_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_isedited)
            {
                System.Windows.Forms.DialogResult dr = MessageBox.Show("Do you want to save changes?", "Edit Protocol", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                try
                {
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                    {
              
                    }
                    else if (dr == System.Windows.Forms.DialogResult.Cancel)
                        e.Cancel = true;
                }
                catch (Exception ex)
                {
                    new FrmShowError(CustomExceptionHandler.ShowExceptionText(ex)).ShowDialog();
                    e.Cancel = true;
                }
            }
        }

       
       

        private void butNewpanel_Click(object sender, EventArgs e)
        {
            //ProtocolPanel panel = new ProtocolPanel();
            //panel.Protocol = _protocol;
            
            FrmNewPanel frm = new FrmNewPanel();//panel, _mdiparent); 

            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
               // _protocol.ProtocolPanels.Add(panel);
               // BindPanels();
            }
        }

        private void butEditpanel_Click(object sender, EventArgs e)
        {
            //ProtocolPanel panel = GetSelectedProtocol();
            //if (panel != null)
            //{   
                FrmNewPanel frm = new FrmNewPanel();//panel, _mdiparent); 
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {                    
                    //BindPanels();
                }
            //}
        }

        private void butDeletepanel_Click(object sender, EventArgs e)
        {
           
                if (MessageBox.Show("Are you sure you want to delete this Panel?", "Delete Panel", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        //_protocol.ProtocolPanels.Remove(panel);
                        //DataRepository.SaveOrUpdateProtocol(_protocol);
                    }
                    catch (Exception ex)
                    {
                        FrmShowError frm = new FrmShowError(new ExceptionStatus() { message = "Panel could not be deleted.", ex = ex });
                        frm.ShowDialog();
                    }
                }

           
        }

        private void txttestsrepeated_KeyPress(object sender, KeyPressEventArgs e)
        {
            int x = e.KeyChar;

            if ((x >= 48 && x <= 57) || (x == 8) || (x == 46))
            {
                e.Handled = false;
            }
            else
                e.Handled = true;

        }      
     /*   
        private void CreatNewProtocol()
        {
            if (_protocol == null)
            {
                _protocol = new Protocol();
                _protocol.ProtocolType = (int)_classOfTest;
            }

            switch (_classOfTest)
            {
                //case ClassOfMorbidityTestEnum.CD4:
                //    ProtocolPanel panel = new ProtocolPanel();
                //    panel.PanelName = "CD4 Panel";
                //    panel.Protocol = _protocol;
                //    _protocol.ProtocolPanels.Add(panel);
                //    break;
                case ClassOfMorbidityTestEnum.Chemistry:
                    var results = DataRepository.GetTestByPlatform(ClassOfMorbidityTestEnum.Chemistry.ToString());
                    //string[] testname = Enum.GetNames(typeof(ChemistryTestNameEnum));

                    foreach (Test t in results)
                    {
                        if (_protocol.GetSymptomDirectedTestByTestId(t.Id) == null)
                        {
                            PSymptomDirectedTest sdt = new PSymptomDirectedTest();
                            sdt.Test = t;
                            sdt.Protocol = _protocol;
                            _protocol.SymptomDirectedTests.Add(sdt);
                        }
                    }
                    break;
                case ClassOfMorbidityTestEnum.OtherTest:
                    var results2 = DataRepository.GetTestByPlatform(ClassOfMorbidityTestEnum.OtherTest.ToString());
                    //string[] testname = Enum.GetNames(typeof(ChemistryTestNameEnum));

                    foreach (Test t in results2)
                    {
                        if (_protocol.GetSymptomDirectedTestByTestId(t.Id) == null)
                        {
                            PSymptomDirectedTest sdt = new PSymptomDirectedTest();
                            sdt.Test = t;
                            sdt.Protocol = _protocol;
                            _protocol.SymptomDirectedTests.Add(sdt);
                        }
                    }
                    break;
            }
        }
    */
    }
}
