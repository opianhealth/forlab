﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using LQT.Core.Domain;
using LQT.Core.Util;
using LQT.GUI.Location;
using LQT.GUI.Asset;
using LQT.GUI.Testing;
using LQT.GUI.Quantification;
using LQT.GUI.UserCtr;
using LQT.GUI.Reports;
using LQT.GUI.Tools;

using LQT.Core;
using LQT.GUI.ReportBorwser;
using LQT.GUI.ReportParameterUserCtr;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using LQT.GUI.MorbidityUserCtr;


namespace LQT.GUI
{
    public partial class LqtMainWindowForm : Form
    {
        public BaseUserControl _currentUserCtr;
        private RptBaseUserControl _ReportParamUserCtr;
        string path;
        SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
        SqlDataAdapter daForecast = null;
        SqlCommand cmdForecast = null;
        private MMProgram _mMProgram;
        private IList<MMProgram> _mMPrograms;
        public LqtMainWindowForm()
        {
            Cursor.Current = Cursors.WaitCursor;
            InitializeComponent();
            updateDatabase();
            Initialize();
            Cursor.Current = Cursors.Default;
            treeViewNav.BeforeSelect+=new TreeViewCancelEventHandler(treeViewNav_BeforeSelect);
           
           // this.IsMdiContainer = true;
        }
        private void updateDatabase()
        {

            string script = Path.Combine(AppSettings.GetUpdatePath, "UpdateDatabase.sql");

            //Server server = new Server(new ServerConnection(connection));              
            //server.ConnectionContext.ExecuteNonQuery(script); 



            var fileContent = File.ReadAllText(script);
            var sqlqueries = fileContent.Split(new[] { " GO " }, StringSplitOptions.RemoveEmptyEntries);
            IEnumerable<string> commandStrings = Regex.Split(fileContent, @"^\s*GO\s*$", RegexOptions.Multiline | RegexOptions.IgnoreCase);

            SqlCommand cmd1 = new SqlCommand();

            cmd1.CommandType = CommandType.Text;

            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            foreach (string commandString in commandStrings)
            {


                cmd1.Connection = connection;
                if (!string.IsNullOrEmpty(commandString))
                {
                    cmd1.CommandText = commandString;
                    cmd1.ExecuteNonQuery();
                }
            }
            connection.Close();
        }
        private void treeViewNav_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            //TreeNode tb = (TreeNode)sender;
            if (Color.Gray == e.Node.ForeColor)
                e.Cancel = true;
        }
        private void Initialize()
        {
            treeViewNav.PreseveTreeState = true;
            // Display version
            toolBarLblVersion.Text = String.Format(" {0}", LQT.Core.AppSettings.SoftwareVersion);
            mainStatusBarLblDate.Text = DateTime.Now.ToString();

            BuildNavigationMenu();
            TreeNode conNode = new TreeNode("Consumption Methodology");
            conNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-1";
            conNode.ImageIndex = 6;
            conNode.SelectedImageIndex = 6;
            
            TreeNode serNode = new TreeNode("Service Statistic Methodology");
            serNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-2";
            serNode.ImageIndex = 15;
            serNode.SelectedImageIndex = 6;

            //TreeNode typeNode = new TreeNode("Morbidity Methodology");
            //typeNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-3";
            //typeNode.ImageIndex = 15;
            //typeNode.SelectedImageIndex = 15;

   
           
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F1)
            {
                showHelp();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        #region Build left panel navigation section................................................

        public void BuildNavigationMenu()
        {
            treeViewNav.SaveTreeState();
            treeViewNav.Nodes.Clear();

            treeViewNav.SelectedNode = null;

            //add dash board menu
            TreeNode rootNode = new TreeNode("Dash Board");
            rootNode.Tag = MainMenuTag.DASHBOARD.ToString();
            rootNode.ImageIndex = 5;
            rootNode.SelectedImageIndex = 5;
            treeViewNav.Nodes.Add(rootNode);
            BuildSettingsMenu();
            //BuildTestMenu();
            //BuildProductMenu();
            //BuildInstrumentMenu();
            //BuildProtocolMenus();
            //BuildLoactionMenu();
            BuildMethodologyMenu();
            //treeViewNav.RestoreTreeState();
            
        }

        public void BuildSettingsMenu()
        {
            TreeNode rootNode = new TreeNode("Settings");
            rootNode.Tag = MainMenuTag.SETTINGS.ToString();
            rootNode.ImageIndex = 2;
            rootNode.SelectedImageIndex = 2;

            TreeNode generalNode = new TreeNode("General Settings");
            generalNode.Tag = MainMenuTag.SETTINGS.ToString();
            generalNode.ImageIndex = 2;
            generalNode.SelectedImageIndex = 2;
            generalNode.ToolTipText = "These settings applies for all methodologies.";
            rootNode.Nodes.Add(generalNode);

            //TreeNode consumptionNode = new TreeNode("Consumption Settings");
            //consumptionNode.Tag = MainMenuTag.SETTINGS.ToString();
            //consumptionNode.ImageIndex = 2;
            //consumptionNode.SelectedImageIndex = 2;
            //rootNode.Nodes.Add(consumptionNode);

            //TreeNode servicNode = new TreeNode("Service Statistic Settings");
            //servicNode.Tag = MainMenuTag.SETTINGS.ToString();
            //servicNode.ImageIndex = 2;
            //servicNode.SelectedImageIndex = 2;
            //rootNode.Nodes.Add(servicNode);

            //TreeNode morbidityNode = new TreeNode("Morbidity Settings");
            //morbidityNode.Tag = MainMenuTag.SETTINGS.ToString();
            //morbidityNode.ImageIndex = 2;
            //morbidityNode.SelectedImageIndex = 2;
            //rootNode.Nodes.Add(morbidityNode);

            BuildInstrumentMenu(generalNode);
            BuildTestMenu(generalNode);
            BuildProductMenu(generalNode);
        
            BuildLoactionMenu(generalNode);

            //BuildProtocolMenu(morbidityNode);
            //BuildRapidTestMenu(morbidityNode);
            
            treeViewNav.Nodes.Add(rootNode);
        }

        private void BuildTestMenu(TreeNode parentNode)
        {
            IList<TestingArea> list = DataRepository.GetAllTestingArea();
            
            //added just for this build will be optimized later on
            //IList<TestingGroup> listtg = DataRepository.GetAllTestingGroup();
            IList<Test> listtc = DataRepository.GetAllTests();

            TreeNode rootNode = new TreeNode("Test Profile");
            rootNode.Tag = MainMenuTag.TEST.ToString();
            rootNode.ImageIndex = 11;
            rootNode.SelectedImageIndex = 11;

            TreeNode tareaNode = new TreeNode("Testing Areas ["+list.Count+"]");
            tareaNode.Tag = MainMenuTag.TEST.ToString() + "|" + -1;
            tareaNode.ImageIndex = 11;
            tareaNode.SelectedImageIndex = 11;
            rootNode.Nodes.Add(tareaNode);
            
            //TreeNode tgroupNode = new TreeNode("Testing Groups ["+listtg.Count+"]");
            //tgroupNode.Tag = MainMenuTag.TEST.ToString() + "|" + -2;
            //tgroupNode.ImageIndex = 4;
            //tgroupNode.SelectedImageIndex = 4;
            //rootNode.Nodes.Add(tgroupNode);

            TreeNode testNode = new TreeNode("Tests ["+listtc.Count+"]");
            testNode.Tag = MainMenuTag.TEST.ToString() + "|" + -3;
            testNode.ImageIndex = 11;
            testNode.SelectedImageIndex = 11;



            foreach (TestingArea ta in list)
            {
                //if (!ta.UseInDemography)
                //{
                    TreeNode node = new TreeNode(ta.AreaName);
                    node.Tag = MainMenuTag.TEST.ToString() + "|-4|" + ta.Id;
                    node.ImageIndex = 11;
                    node.SelectedImageIndex = 11;
                    //AddTestingGroupToMenu(node, ta.TestingGroups);
                    testNode.Nodes.Add(node);
                //}
            }
            rootNode.Nodes.Add(testNode);

            //ClassOfMorbidityTestEnum[] mtestEnums = LqtUtil.EnumToArray<ClassOfMorbidityTestEnum>();
            //TreeNode artNode = new TreeNode("ART Tests");
            //artNode.Tag = MainMenuTag.TEST.ToString() + "|" + -6;
            //artNode.ImageIndex = 5;
            //artNode.SelectedImageIndex = 5;

            
            //for (int i = 0; i < mtestEnums.Length; i++)
            //{
                ClassOfMorbidityTestEnum cm = ClassOfMorbidityTestEnum.Consumable; //mtestEnums[i];
                TreeNode nodem = new TreeNode(cm.ToString());
                nodem.Tag = MainMenuTag.TEST.ToString() + "|-6|" + (int)cm;
                nodem.ImageIndex = 11;
                nodem.SelectedImageIndex = 11;
                //AddTestingGroupToMenu(node, ta.TestingGroups);
                //artNode.Nodes.Add(nodem);
                rootNode.Nodes.Add(nodem);
            //}

            //rootNode.Nodes.Add(artNode);
            //rootNode.Expand();
            parentNode.Nodes.Add(rootNode);
            //treeViewNav.Nodes.Add(rootNode);
        }

        private void BuildProductMenu(TreeNode parentNode)
        {
            IList<MasterProduct> plist = DataRepository.GetAllProduct();

            TreeNode rootNode = new TreeNode("Product Profile");
            rootNode.Tag = MainMenuTag.PRODUCT.ToString();
            rootNode.ImageIndex = 3;
            rootNode.SelectedImageIndex = 3;


            TreeNode typeNode = new TreeNode("Product Types");
            typeNode.Tag = MainMenuTag.PRODUCT.ToString() + "|-1";
            typeNode.ImageIndex = 3;
            typeNode.SelectedImageIndex = 3;
            rootNode.Nodes.Add(typeNode);

            TreeNode proNode = new TreeNode("Products ["+plist.Count+"]");
            proNode.Tag = MainMenuTag.PRODUCT.ToString() + "|-2";
            proNode.ImageIndex = 3;
            proNode.SelectedImageIndex = 3;

            int ptcount = 0;
            foreach (ProductType r in DataRepository.GetAllProductType())
            {
                TreeNode node = new TreeNode(r.TypeName);
                node.Tag = MainMenuTag.PRODUCT.ToString() + "|-3|" + r.Id;
                node.ImageIndex = 3;
                node.SelectedImageIndex = 3;
                proNode.Nodes.Add(node);
                ptcount++;
            }
            typeNode.Text = typeNode.Text + "[" + ptcount + "]";

            rootNode.Nodes.Add(proNode);
            parentNode.Nodes.Add(rootNode);

            //treeViewNav.Nodes.Add(rootNode);

        }

        private void BuildInstrumentMenu(TreeNode parentNode)
        {
            IList<Instrument> ilist = DataRepository.GetAllInstrument();

            TreeNode rootNode = new TreeNode("Instruments ["+ ilist.Count+"]");
            rootNode.Tag = MainMenuTag.INSTRUMENT.ToString();
            rootNode.ImageIndex = 8;
            rootNode.SelectedImageIndex = 8;
            parentNode.Nodes.Add(rootNode);
            //treeViewNav.Nodes.Add(rootNode);
        }

        private void BuildProtocolMenu(TreeNode parentNode)
        {
            TreeNode rootNode = new TreeNode("Protocols");
            rootNode.Tag = MainMenuTag.PROTOCOLS.ToString();
            rootNode.ImageIndex = 10;
            rootNode.SelectedImageIndex = 10;
            parentNode.Nodes.Add(rootNode);
            //treeViewNav.Nodes.Add(rootNode);
        }

        private void BuildRapidTestMenu(TreeNode parentNode)
        {
            TreeNode rootNode = new TreeNode("Rapid Test Specification");
            rootNode.Tag = MainMenuTag.RAPIDTEST.ToString();
            rootNode.ImageIndex = 10;
            rootNode.SelectedImageIndex = 10;
            parentNode.Nodes.Add(rootNode);
            //treeViewNav.Nodes.Add(rootNode);
        }
       
        private void BuildLoactionMenu(TreeNode parentNode)
        {
            IList<ForlabSite> slist = DataRepository.GetAllSite();

            TreeNode rootNode = new TreeNode("Laboratory Profile");
            rootNode.Tag = MainMenuTag.LOCATION.ToString();
            rootNode.ImageIndex = 7;
            rootNode.SelectedImageIndex = 7;


            TreeNode regNode = new TreeNode("Regions/Districts/Provinces");
            regNode.Tag = MainMenuTag.LOCATION.ToString() + "|" + -1;
            regNode.ImageIndex = 10;
            regNode.SelectedImageIndex = 10;
            rootNode.Nodes.Add(regNode);

            TreeNode sitecNode = new TreeNode("Site Category");
            sitecNode.Tag = MainMenuTag.LOCATION.ToString() + "|-4";
            sitecNode.ImageIndex = 7;
            sitecNode.SelectedImageIndex = 7;
            rootNode.Nodes.Add(sitecNode);

            TreeNode siteNode = new TreeNode("Sites ["+slist.Count+"]");
            siteNode.Tag = MainMenuTag.LOCATION.ToString() + "|-2";
            siteNode.ImageIndex = 7;
            siteNode.SelectedImageIndex = 7;

            int regioncount = 0;
            foreach (ForlabRegion r in DataRepository.GetAllRegion())
            {
                regioncount++;
                TreeNode node = new TreeNode(r.RegionName);
                node.Tag = MainMenuTag.LOCATION.ToString() + "|-3|" + r.Id;
                node.ImageIndex = 7;
                node.SelectedImageIndex = 7;
                siteNode.Nodes.Add(node);
            }
            regNode.Text = regNode.Text + " [" + regioncount + "]";

            rootNode.Nodes.Add(siteNode);
            parentNode.Nodes.Add(rootNode);
            //rootNode.Expand();
            //treeViewNav.Nodes.Add(rootNode);
        }

        public void BuildMethodologyMenu()
        {
            TreeNode rootNode = new TreeNode("Quantification Process");
            rootNode.Tag = MainMenuTag.METHODOLOGY.ToString();
            rootNode.ImageIndex = 6;
            rootNode.SelectedImageIndex = 6;

            TreeNode conNode = new TreeNode("Consumption Methodology");
            conNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-1";
            conNode.ImageIndex = 6;
            conNode.SelectedImageIndex = 6;
            rootNode.Nodes.Add(conNode);

            TreeNode serNode = new TreeNode("Service Statistic Methodology");
            serNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-2";
            serNode.ImageIndex = 6;
            serNode.SelectedImageIndex = 6;
            rootNode.Nodes.Add(serNode);

            //TreeNode typeNode = new TreeNode("Morbidity Methodology");
            //typeNode.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-3";
            //typeNode.ImageIndex = 15;
            //typeNode.SelectedImageIndex = 15;
            //rootNode.Nodes.Add(typeNode);

            TreeNode typeNodeMorbidity = new TreeNode("Demographic Methodology");
            typeNodeMorbidity.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-4";
            typeNodeMorbidity.ImageIndex = 6;
            typeNodeMorbidity.SelectedImageIndex = 6;
           // rootNode.Nodes.Add(typeNodeMorbidity);
            
            //TreeNode typeNodeMorbidityForecast = new TreeNode("Morbidity Methodology");
            //typeNodeMorbidityForecast.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-1";

            //TreeNode typeNodeMorbidityForecast = new TreeNode("Morbidity Methodology");
            TreeNode typeNodeMorbidityForecast = new TreeNode("Demographic Methodology");
            typeNodeMorbidityForecast.Tag = MainMenuTag.METHODOLOGY.ToString();
            typeNodeMorbidityForecast.ImageIndex = 6;
            typeNodeMorbidityForecast.SelectedImageIndex = 6;

           // typeNodeMorbidityForecast.ForeColor = System.Drawing.Color.Gray;
            //typeNodeMorbidityForecast.Tag = MainMenuTag.METHODOLOGY.ToString() + "|-5";
            //typeNodeMorbidityForecast.ImageIndex = 15;
            //typeNodeMorbidityForecast.SelectedImageIndex = 15;



            int i = 15;
           
            _mMPrograms = DataRepository.GetMMPrograms();
            //treeViewNav.Nodes.Clear();
            //treeViewNav.BeginUpdate();
            foreach (MMProgram mMprogram in _mMPrograms)
            {
                TreeNode HIV = new TreeNode(mMprogram.ProgramName);
                //if (mMprogram.ProgramName.ToString() == "Name1")
                //{
                //    treeViewNav.SelectedNode = HIV;
                //}
                HIV.Tag = MainMenuTag.METHODOLOGY.ToString()+"#" + Convert.ToString(mMprogram.Id) + "|-5";
                HIV.ImageIndex = 15;
                HIV.SelectedImageIndex = 15;
                typeNodeMorbidityForecast.Nodes.Add(HIV);
                i++;
            }
            //TreeNode HIV1 = new TreeNode("HIV");
            //HIV1.Tag = MainMenuTag.METHODOLOGY.ToString()+"#0" + "|-5";
            //HIV1.ImageIndex = 15;
            //HIV1.SelectedImageIndex = 15;
            //typeNodeMorbidityForecast.Nodes.Add(HIV1);
            
            rootNode.Nodes.Add(typeNodeMorbidityForecast);

            rootNode.Expand();
            treeViewNav.Nodes.Add(rootNode);
            //treeViewNav.EndUpdate();
            //treeViewNav.Refresh();
            

        }
        private void treeViewNav_AfterSelect_1(object sender, TreeViewEventArgs e)
        {        
            string[] tag = e.Node.Tag.ToString().Split(new char[] { '|' });
            string[] tag1 = tag[0].ToString().Split(new char[] { '#' });
             MainMenuTag mtag = (MainMenuTag)Enum.Parse(typeof(MainMenuTag), tag1[0]);
            
            e.Node.Expand();
            int mid;

            switch (mtag)
            {
                case MainMenuTag.LOCATION:
                    if (tag.Length > 1)
                    {
                        mid = int.Parse(tag[1]);
                        if (mid == -1)
                        {
                            this._currentUserCtr = new ListRegionPane();
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -2)
                        {
                            this._currentUserCtr = new ListSitePane(0);
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -3)
                        {
                            this._currentUserCtr = new ListSitePane(int.Parse(tag[2]));
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -4)
                        {
                            this._currentUserCtr = new ListSiteCatPane();
                            LoadCurrentUserCtr();
                        }
                       
                    }
                    else
                    {
                        this._currentUserCtr = new ChartSiteperRegion();
                        LoadCurrentUserCtr();
                    }
                    break;
                case MainMenuTag.SETTINGS:                    
                    break;
                case MainMenuTag.TEST:
                    if (tag.Length > 1)
                    {
                        mid = int.Parse(tag[1]);
                        if (mid == -1)
                        {
                            this._currentUserCtr = new ListTestingAreaPane();
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -2)
                        {
                            //this._currentUserCtr = new ListTestingGroupPane();
                            //LoadCurrentUserCtr();
                        }
                        else if (mid == -3)
                        {
                            this._currentUserCtr = new ListTestPane(FiliterTestByEnum.All, 0);
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -4)
                        {
                            this._currentUserCtr = new ListTestPane(FiliterTestByEnum.TestArea, int.Parse(tag[2]));
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -5)
                        {
                            this._currentUserCtr = new ListTestPane(FiliterTestByEnum.TestGroup, int.Parse(tag[2]));
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -6)
                        {
                            if (tag.Length > 2)
                            {
                                ClassOfMorbidityTestEnum mtest = (ClassOfMorbidityTestEnum)Enum.ToObject(typeof(ClassOfMorbidityTestEnum), int.Parse(tag[2]));
                                if (int.Parse(tag[2]) <= 4)
                                {
                                    this._currentUserCtr = new ListMorbidityTestPane(mtest);
                                }
                                else
                                {
                                    this._currentUserCtr = new ListSupplyListPane(mtest);
                                }

                                LoadCurrentUserCtr();
                            }
                        }
                    }
                    else
                    {
                        this._currentUserCtr = new ChartTestperArea();
                        LoadCurrentUserCtr();
                    }
                    break;
                case MainMenuTag.PRODUCT:
                    if (tag.Length > 1)
                    {
                        mid = int.Parse(tag[1]);
                        if (mid == -1)
                        {
                            this._currentUserCtr = new ListProductTypePane();
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -2)
                        {
                            this._currentUserCtr = new ListProductPane(0);
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -3)
                        {
                            this._currentUserCtr = new ListProductPane(int.Parse(tag[2]));
                            LoadCurrentUserCtr();
                        }
                    }
                    else
                    {
                        this._currentUserCtr = new ProductNoperCat();
                        LoadCurrentUserCtr();
                    }
                    break;
                case MainMenuTag.INSTRUMENT:
                    this._currentUserCtr = new ListInstrumentPane();
                    LoadCurrentUserCtr();
                    break;
                case MainMenuTag.METHODOLOGY:
                    if (tag.Length > 1)
                    {
                        mid = int.Parse(tag[1]);
                        if (mid == -1)
                        {
                            this._currentUserCtr = new ListConsumptionPane(MethodologyEnum.CONSUMPTION);
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -2)
                        {
                            this._currentUserCtr = new ListConsumptionPane(MethodologyEnum.SERVICE_STATISTIC);
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -3)
                        {
                            this._currentUserCtr = new ListMorbidityPane();
                            LoadCurrentUserCtr();
                        }
                        else if (mid == -4)
                        {
                            this._currentUserCtr = new ListNewMorbidityPane();
                            LoadCurrentUserCtr();
                            // LQT.GUI.MorbidityUserCtr.FrmMorbidityProgramSelection frmm = new MorbidityUserCtr.FrmMorbidityProgramSelection();
                            //frmm.ShowDialog();
                        }
                        else if (mid == -5)
                        {
                            // working 
                            this._currentUserCtr = new ListMorbidityForecastPane("MORBIDITY", tag1[1]);
                            LoadCurrentUserCtr();
                        }
                    }
                    break;
                case MainMenuTag.DASHBOARD:
                    this._currentUserCtr = new DashBoard();
                    LoadCurrentUserCtr();
                    break;
                case MainMenuTag.PROTOCOLS:

                    this._currentUserCtr = new ListProtocolPane();
                    LoadCurrentUserCtr();
                    break;
                case MainMenuTag.RAPIDTEST:

                    //this._currentUserCtr = new ListProtocolPane();
                    //LoadCurrentUserCtr();
                    RapidTestForm frm = new RapidTestForm();
                    frm.ShowDialog();
                    break;
            }
        }

        public void LoadCurrentUserCtr()
        {
            SetEditButtonsStatus(false);

            this.tlpMainPanel.Controls.Clear();
            _currentUserCtr.MdiParentForm = this;
            _currentUserCtr.Dock = DockStyle.Fill;
            _currentUserCtr.OnSelectedItemChanged += new EventHandler(_currentUserCtr_OnSelectedItemChanged);
            this.lblTitle.Text = _currentUserCtr.GetControlTitle;
            this.tlpMainPanel.Controls.Add(_currentUserCtr);
        }

        private void LoadReportParamUserCtr()
        {
            this.tlpMainPanel.Controls.Clear();
            _ReportParamUserCtr.MdiParentForm = this;
            _ReportParamUserCtr.Dock = DockStyle.Top;
            _ReportParamUserCtr.BackColor = Control.DefaultBackColor;
            this.lblTitle.Text = _ReportParamUserCtr.GetControlTitle;
            this.tlpMainPanel.Controls.Add(_ReportParamUserCtr);
        }

        private void _currentUserCtr_OnSelectedItemChanged(object sender, EventArgs e)
        {
            if (sender is ListView)
            {
                ListView lv = (ListView)sender;
                if (lv.SelectedItems.Count > 0)
                {
                    SetEditButtonsStatus(true);
                }
                else
                {
                    SetEditButtonsStatus(false);
                }
            }
            else
            {
                SetEditButtonsStatus(false);
            }
        }

        #endregion

        #region Tool-strip button actions...........................................................

        private void tsbEdit_Click(object sender, EventArgs e)
        {
            if (_currentUserCtr != null)
            {
                _currentUserCtr.EditSelectedItem();
            }
        }

        private void tsbDelete_Click(object sender, EventArgs e)
        {
            if (_currentUserCtr != null)
            {
                SetEditButtonsStatus(!_currentUserCtr.DeleteSelectedItem());
            }
        }

        private void tsbReport_Click(object sender, EventArgs e)
        {
            //FrmReportBrowser frm = new FrmReportBrowser();
            //frm.ShowDialog();
            this._currentUserCtr = null;

            SetEditButtonsStatus(false);

            ListReport lr = new ListReport();

            this.tlpMainPanel.Controls.Clear();
            //lr.MdiParentForm = this;
            lr.Dock = DockStyle.Fill;
            //.OnSelectedItemChanged += new EventHandler(_currentUserCtr_OnSelectedItemChanged);
            this.lblTitle.Text = "List of reports";
            this.tlpMainPanel.Controls.Add(lr);
        }

        private void tsbForecast_Click(object sender, EventArgs e)
        {
            ForecastForm frm = new ForecastForm();
            frm.ShowDialog();
        }

        #endregion

        #region Menustrip button action............................................................

        private void newRegionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegionForm frm = new RegionForm(new ForlabRegion(), this);
            frm.ShowDialog();
        }

        private void newSiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SiteForm frm = new SiteForm(new ForlabSite(), this);
            frm.ShowDialog();
        }

        private void searchSiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmSearchSite(this).ShowDialog();
        }

        private void newProductTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductTypeForm frm = new ProductTypeForm(new ProductType(), this);
            frm.ShowDialog();
        }

        private void newProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductForm frm = new ProductForm(new MasterProduct(), this);
            frm.ShowDialog();
        }

        private void searchProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrmSearchProduct(this).ShowDialog();
        }

        private void newInstrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstrumentForm frm = new InstrumentForm(new Instrument(), this);
            frm.ShowDialog();
        }

        private void newTestingAreaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestingAreaFrom frm = new TestingAreaFrom(new TestingArea(), this);
            frm.ShowDialog();
        }

        //private void newTestingGroupToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    TestingGroupFrom frm = new TestingGroupFrom(new TestingGroup(), this);
        //    frm.ShowDialog();
        //}

        private void newTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TestFrom frm = new TestFrom(new Test(), this);
            frm.ShowDialog();
        }

        private void backupDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = new frmAdvanceSetting(FrmDatabaseSettingsEnum.SqlServerSettings, false, true);
            frm.ShowDialog();
        }

        private void aboutLQTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox1().ShowDialog();
        }

        private void importRegionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportRegion frm = new FrmImportRegion();
            frm.ShowDialog();
        }

        private void importSitesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportSite frm = new FrmImportSite(0);
            frm.ShowDialog();
        }

        private void importSitesInstrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportSiteInstrument frm = new FrmImportSiteInstrument(1);
            frm.ShowDialog();
        }

        private void importProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportPro frm = new FrmImportPro();
            frm.ShowDialog();
        }

        private void importInstrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportIns frm = new FrmImportIns();
            frm.ShowDialog();
        }

        private void importTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportTest frm = new FrmImportTest();
            frm.ShowDialog();
        }

        private void importProductUsageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportProUsage frm = new FrmImportProUsage();
            frm.ShowDialog();
        }

        private void siteCategoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SiteCategoryForm frm = new SiteCategoryForm();
            frm.ShowDialog();
        }

        private void chartTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //LQT.GUI.ReportBorwser.frmRptViewer frm = new ReportBorwser.frmRptViewer();
            //frm.ShowDialog();
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void getImportTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog1.Title = "Specify Destination Filename";
                saveFileDialog1.FileName = "ImportTemplate.xls";
                saveFileDialog1.Filter = "Execl files (*.xls)|*.xls";
                saveFileDialog1.FilterIndex = 1;
                saveFileDialog1.OverwritePrompt = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create))//LqtUtil.GetFolderPath(AppSettings.ExportPath) + "\\ImportTemplate.xls", FileMode.Create))
                    {
                        Stream xstream = Assembly.GetExecutingAssembly().GetManifestResourceStream("LQT.GUI.Resources.ImportTemplate.xls");

                        byte[] b = new byte[xstream.Length + 1];
                        xstream.Read(b, 0, Convert.ToInt32(xstream.Length));

                        fs.Write(b, 0, Convert.ToInt32(b.Length - 1));
                        fs.Flush();
                        fs.Close();


                    }
                    MessageBox.Show("Exported Successfully!");
                }
                // + Environment.NewLine + "To:- " + LqtUtil.GetFolderPath(AppSettings.ExportPath) + "\\ImportTemplate.xls");

            }
            catch
            {
                MessageBox.Show("Access Denied. ", "Template Export", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void tsRapidTestAlgorithm_Click(object sender, EventArgs e)
        {
            new RapidTestForm().ShowDialog();
        }

        private void regionListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new RegionListParam();
            LoadReportParamUserCtr();
        }

        private void siteListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new SiteListParam();
            LoadReportParamUserCtr();
        }

        private void siteInstrumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new SiteInstrumentListParam();
            LoadReportParamUserCtr();
        }

        private void instrumentListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new InstrumentListParam();
            LoadReportParamUserCtr();
        }

        private void productListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new ProductListParam();
            LoadReportParamUserCtr();
        }

        private void productPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new ProductListParam(true);
            LoadReportParamUserCtr();
        }
        private void productUsageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new ProductUsageParam();
            LoadReportParamUserCtr();
        }
        private void testListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new TestListParam();
            LoadReportParamUserCtr();
        }

        private void testProductUsageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new ProductUsageParam();
            LoadReportParamUserCtr();
        }

        private void forecastComparisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new ComparisionReportParam();
            LoadReportParamUserCtr();
        }

        private void morbidityForecastCostSummaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new MorbidityCostSReportParam();
            LoadReportParamUserCtr();
        }

        private void morbidityForecastNoOfPatientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new MorbidityNoPatientReportParam();
            LoadReportParamUserCtr();
        }

        private void morbidityForecastNoOfTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._ReportParamUserCtr = new MorbidityNoofTestReportParam();
            LoadReportParamUserCtr();
        }

        private void importQuantificationVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportQMenu frm = new FrmImportQMenu();
            frm.ShowDialog();
        }

        private void viewHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showHelp();
        }

        #endregion

        #region Methods ...........................................................................

        private void SetEditButtonsStatus(bool status)
        {
            tsbEdit.Enabled = status;
            tsbDelete.Enabled = status;
        }

        public void ShowStatusBarInfo(string msg)
        {
            mainStatusBarLblInfo.Text = msg;
        }

        public void ShowStatusBarInfo(string msg, bool reloadUserCtr)
        {
            mainStatusBarLblInfo.Text = msg;
            if (reloadUserCtr)
            {
                if (_currentUserCtr != null)
                    _currentUserCtr.ReloadUserCtrContents();
            }
        }

        //public void ExcelHeaderFormatt(Excel.Range unicell)
        //{
        //    unicell.Font.Color = System.Drawing.ColorTranslator.ToOle(Color.White);
        //    unicell.Interior.Color = System.Drawing.ColorTranslator.ToOle(Color.Gray);
        //    unicell.Font.Bold = true;
        //    unicell.BorderAround(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, 1);
        //}

        private void showHelp()
        {
            string path = LqtUtil.GetFolderPath(AppSettings.ExportPath);
            if (!File.Exists(path + "\\ForLabUMV2.0.pdf"))
            {
                GenerateHelp();
            }
            path = "file://" + Path.Combine(path, "ForLabUMV2.0.pdf");

            Help.ShowHelp(this, path);
        }

        public void GenerateHelp()
        {
            try
            {

                using (FileStream fs = new FileStream(LqtUtil.GetFolderPath(AppSettings.ExportPath) + "\\ForLabUMV2.0.pdf", FileMode.Create))
                {
                    Stream xstream = Assembly.GetExecutingAssembly().GetManifestResourceStream("LQT.GUI.Resources.ForLabUMV2.0.pdf");

                    byte[] b = new byte[xstream.Length + 1];
                    xstream.Read(b, 0, Convert.ToInt32(xstream.Length));

                    fs.Write(b, 0, Convert.ToInt32(b.Length - 1));
                    fs.Flush();
                    fs.Close();


                }

            }
            catch
            {
                MessageBox.Show("Access Denied. ", "Generate Help File", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void referalSiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportRefSite frm = new FrmImportRefSite(0);
            frm.ShowDialog();
        
        }

        private void forecastToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ForecastForm frm = new ForecastForm();
            frm.ShowDialog();
        }

        private void consumablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmImportConsumable frm = new FrmImportConsumable();
            frm.ShowDialog();
        }

       

        private void treeViewNav_NodeMouseClick_1(object sender, TreeNodeMouseClickEventArgs e)
        {
            var hitTest = e.Node.TreeView.HitTest(e.Location);
            if (hitTest.Location == TreeViewHitTestLocations.Indent)
            {

                if (e.Node.IsExpanded)
                    e.Node.Collapse();
                else
                    e.Node.Expand();
                return;
            }
        }

        private void LqtMainWindowForm_Load(object sender, EventArgs e)
        {

        }

        #region SendError
        public void DoMails()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();//passing argument
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            path = Path.Combine(System.IO.Path.GetTempPath(), "error-log.txt");
            //Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "error-log.txt");
            DoMails();
        }

        private void DoMail()
        {
            // MailMessage message;
            SmtpClient smtp;
            string BodyText = "Please Find the attached Error-Log txt file";

            try
            {
                FileInfo inf = new FileInfo(path);
                if (System.IO.File.Exists(inf.FullName))
                {
                    using (MailMessage message = new MailMessage())
                    {
                        message.To.Add("support@forlabtool.com");
                        message.Subject = "Error Logs";
                        message.From = new MailAddress("support@forlabtool.com");
                        message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                        System.Net.Mail.AlternateView plainview = System.Net.Mail.AlternateView.CreateAlternateViewFromString
                                                 (System.Text.RegularExpressions.Regex.Replace(BodyText, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                        message.IsBodyHtml = false;
                        message.AlternateViews.Add(plainview);
                        // message.Body = "Please find the attached Error-log file";

                        message.Attachments.Add(new Attachment(inf.FullName));//path + "\\bin\\Debug\\" + "error-log.txt"));

                        // set smtp details
                        smtp = new SmtpClient("mail.forlabtool.com");
                        smtp.Port = 25;
                        smtp.EnableSsl = false;
                        smtp.Credentials = new NetworkCredential("support@forlabtool.com", "0Pian");
                        smtp.Send(message);

                    }
                    inf.Delete();

                }



            }
            catch (Exception ex)
            {
                //  MessageBox.Show(ex.Message);

            }
        }

        #endregion

        private void LqtMainWindowForm_Leave(object sender, EventArgs e)
        {
           
           // this.Dispose();
        }

        private void LqtMainWindowForm_Deactivate(object sender, EventArgs e)
        {
            this.BackColor = Color.DarkGray;
        }

        private void manageMorbidityModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
  ///FrmMorbidityParameter frm = new FrmMorbidityParameter();
 // FrmMorbidityParameter_new frm = new FrmMorbidityParameter_new();
            FrmMorbidityParameter_new1 frm = new FrmMorbidityParameter_new1(this);
            frm.ShowDialog();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void importDataToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Frmimportdata frm = new Frmimportdata(this);
            frm.ShowDialog();
        }

        private void panelLeft_Paint(object sender, PaintEventArgs e)
        {

        }

       

      
     
       

    }
}
