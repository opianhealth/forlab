IF COL_LENGTH('ForecastInfo','ForecastType') IS NULL
BEGIN
 alter table ForecastInfo
  add  ForecastType char(1) default 'S' not NULL
END
Go

IF COL_LENGTH('ForecastInfo','ProgramId') IS NULL
BEGIN
 alter table ForecastInfo
  add  ProgramId int NULL
END
GO


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ForecastCategoryInfo' AND xtype='U')
   
CREATE TABLE dbo.ForecastCategoryInfo
	(
	ID               INT IDENTITY NOT NULL,
	ForecastinfoID   INT,
	SiteCategoryName NVARCHAR (50),
	CurrentPatient   INT,
	TargetPatient    INT,
	SiteCategoryId   BIGINT,
	CONSTRAINT PK_ForecastCategoryInfo PRIMARY KEY (ID),
	CONSTRAINT FK_ForecastCategoryInfo_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO




IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ForecastCategorySiteInfo' AND xtype='U')
   
CREATE TABLE dbo.ForecastCategorySiteInfo
	(
	ID             INT IDENTITY NOT NULL,
	ForecastInfoID BIGINT,
	CategoryID     INT NOT NULL,
	SiteID         INT NOT NULL,
	CONSTRAINT PK_ForecastCategorySiteInfo PRIMARY KEY (ID)
	)
GO





IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ForecastedTestByTest' AND xtype='U')
CREATE TABLE dbo.ForecastedTestByTest
	(
	ForeCastID INT,
	Tst        INT,
	PGrp       INT,
	TotalTst   DECIMAL (18, 4)
	)
GO


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='insertReportData' AND xtype='U')
CREATE table insertReportData
(
[ID] [int] IDENTITY(1,1) NOT NULL,
ForecastId int,
Methodology nvarchar(64),
ProductType nvarchar(64),
FYear varchar(10), 
Quantity DECIMAL(18,2), 
TotalPrice DECIMAL(18,2),
Duration nvarchar(100),
DurationDateTime datetime,
Title nvarchar(100)
)
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ForecastSiteInfo' AND xtype='U')
CREATE TABLE dbo.ForecastSiteInfo
	(
	ID             INT IDENTITY NOT NULL,
	SiteID         INT,
	ForecastinfoID INT,
	CurrentPatient BIGINT,
	TargetPatient  BIGINT,
	CONSTRAINT PK_ForecastSiteInfo PRIMARY KEY (ID),
	CONSTRAINT FK_ForecastSiteInfo_Site FOREIGN KEY (SiteID) REFERENCES dbo.Site (SiteID),
	CONSTRAINT FK_ForecastSiteInfo_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO



IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='PatientAssumption' AND xtype='U')

CREATE TABLE dbo.PatientAssumption
	(
	ID                  INT IDENTITY NOT NULL,
	ForecastinfoID      INT,
	CategoryID          INT,
	SiteID              INT,
	HIVPositivePer      NUMERIC (18, 2),
	HIVPerWithoutFollow NUMERIC (18, 2),
	ReceiveCD4Per       NUMERIC (18, 2),
	CONSTRAINT PK_PatientAssumption PRIMARY KEY (ID),
	CONSTRAINT FK_PatientAssumption_Site FOREIGN KEY (SiteID) REFERENCES dbo.Site (SiteID),
	CONSTRAINT FK_PatientAssumption_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO





IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='PatientGroup' AND xtype='U')

CREATE TABLE dbo.PatientGroup
	(
	ID                INT IDENTITY NOT NULL,
	ForecastinfoID    INT,
	PatientGroupName  NVARCHAR (50),
	PatientPercentage NUMERIC (18, 2) NOT NULL,
	PatientRatio      NUMERIC (18, 2),
	CONSTRAINT PK_PatientGroup PRIMARY KEY (ID),
	CONSTRAINT FK_PatientGroup_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='PatientNumberHeader' AND xtype='U')

CREATE TABLE dbo.PatientNumberHeader
	(
	ID             INT IDENTITY NOT NULL,
	ForecastinfoID INT,
	SiteID         INT,
	CategoryID     INT,
	CurrentPatient BIGINT,
	TargetPatient  BIGINT,
	CONSTRAINT PK_PatientNumberHeader PRIMARY KEY (ID),
	CONSTRAINT FK_PatientNumberHeader_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO







IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='PatientNumberDetail' AND xtype='U')

CREATE TABLE dbo.PatientNumberDetail
	(
	ID         INT IDENTITY NOT NULL,
	HeaderID   INT,
	Serial     NUMERIC (18, 5),
	Columnname VARCHAR (20),
	ForeCastId BIGINT,
	CONSTRAINT PK_PatientNumberDetail PRIMARY KEY (ID),
	CONSTRAINT FK_PatientNumberDetail_PatientNumberDetail FOREIGN KEY (HeaderID) REFERENCES dbo.PatientNumberHeader (ID)
	)
GO



IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='PercentageVal' AND xtype='U')

CREATE TABLE dbo.PercentageVal
	(
	TestN              INT,
	PGrpID             INT,
	PerNew             DECIMAL (18, 4),
	PerOld             DECIMAL (18, 4),
	TotalTestPerYear   DECIMAL (18, 2),
	TestRepeatPerYear  DECIMAL (18, 2),
	SymptomTestPerYear DECIMAL (18, 2)
	)
GO




IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ForecastedTestByTest' AND xtype='U')

CREATE TABLE dbo.ForecastedTestByTest
	(
	ForeCastID INT,
	Tst        INT,
	PGrp       INT,
	TotalTst   DECIMAL (18, 4)
	)
GO


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='TempTbl1' AND xtype='U')

CREATE TABLE dbo.TempTbl1
	(
	Tst  INT,
	PGrp INT,
	Num  INT,
	Valu INT
	)
GO


IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='TestByMonth' AND xtype='U')
CREATE TABLE dbo.TestByMonth
	(
	ForeCastID INT,
	TestID     INT,
	Month      NVARCHAR (20),
	TstNo      DECIMAL (18, 2),
	PGrp       INT,
	ID         INT IDENTITY NOT NULL,
	SNo        INT
	)
GO


IF COL_LENGTH('TestByMonth','SNo') IS NULL
BEGIN
 alter table TestByMonth
  add  SNo INT
END
Go

IF COL_LENGTH('TestByMonth','NewPatient') IS NULL
BEGIN
 alter table TestByMonth
  add  NewPatient numeric(18, 2)
END
Go

IF COL_LENGTH('TestByMonth','TotalTestPerYear') IS NULL
BEGIN
 alter table TestByMonth
  add  TotalTestPerYear int
END
Go

IF COL_LENGTH('TestByMonth','TestRepeatPerYear') IS NULL
BEGIN
 alter table TestByMonth
  add  TestRepeatPerYear int
END
Go

IF COL_LENGTH('TestByMonth','SymptomTestPerYear') IS NULL
BEGIN
 alter table TestByMonth
  add  SymptomTestPerYear int
END
Go

IF COL_LENGTH('TestByMonth','ExistingPatient') IS NULL
BEGIN
 alter table TestByMonth
  add  ExistingPatient numeric(18, 2)
END
Go



IF COL_LENGTH('TestByMonth','Duration') IS NULL
BEGIN
 alter table TestByMonth
  add  Duration numeric(18, 2)
END
Go

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Tempproductneed' AND xtype='U')

CREATE TABLE dbo.Tempproductneed
	(
	ProductID   INT,
	ProductName NVARCHAR (250),
	Productneed NUMERIC (18, 2),
	ForcaseID   INT
	)
GO



IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='TestingAssumption' AND xtype='U')
CREATE TABLE dbo.TestingAssumption
	(
	ID                   INT IDENTITY NOT NULL,
	ForecastinfoID       INT,
	TestAreaID           INT,
	ProgramGrowRate      NUMERIC (18, 2),
	AvgBloodDrawPerYearE NUMERIC (18, 2),
	AvgBloodDrawPerYearN NUMERIC (18, 2),
	WastageRate          NUMERIC (18, 2) DEFAULT ((0)) NOT NULL,
	CONSTRAINT PK_TestingAssumption PRIMARY KEY (ID),
	CONSTRAINT FK_TestingAssumption_TestingArea FOREIGN KEY (TestAreaID) REFERENCES dbo.TestingArea (TestingAreaID),
	CONSTRAINT FK_TestingAssumption_ForecastInfo FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO




IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='TestingProtocol' AND xtype='U')


CREATE TABLE dbo.TestingProtocol
	(
	ID                 INT IDENTITY NOT NULL,
	TestID             INT,
	PatientGroupID     INT,
	ForecastinfoID     INT,
	PercentagePanel    NUMERIC (18, 2),
	Baseline           INT,
	Month1             INT,
	Month2             INT,
	Month3             INT,
	Month4             INT,
	Month5             INT,
	Month6             INT,
	Month7             INT,
	Month8             INT,
	Month9             INT,
	Month10            INT,
	Month11            INT,
	Month12            INT,
	TotalTestPerYear   INT,
	TestRepeatPerYear  INT,
	SymptomTestPerYear INT,
	CONSTRAINT PK_TestingProtocol PRIMARY KEY (ID),
	CONSTRAINT FK_TestingProtocol_TestingProtocol FOREIGN KEY (TestID) REFERENCES dbo.Test (TestID),
	CONSTRAINT FK_TestingProtocol_PatientGroup FOREIGN KEY (ForecastinfoID) REFERENCES dbo.ForecastInfo (ForecastID)
	)
GO

IF COL_LENGTH('TestingAssumption','ProductTypeID') IS NULL
BEGIN
 alter table TestingAssumption
  add  ProductTypeID INT
END
Go

IF COL_LENGTH('MMGeneralAssumption','IsActive') IS NULL
BEGIN
 alter table MMGeneralAssumption
  add  IsActive bit  
END
Go


IF COL_LENGTH('MMGroup','IsActive') IS NULL
BEGIN
 alter table MMGroup
  add  IsActive bit  
END
Go

IF COL_LENGTH('MMForecastParameter','IsActive') IS NULL
BEGIN
 alter table MMForecastParameter
  add  IsActive bit  
END
Go



IF EXISTS (SELECT * FROM sysobjects WHERE name='fnGetMorbidityMethoSummary1' AND xtype='TF')
BEGIN
DROP FUNCTION [dbo].[fnGetMorbidityMethoSummary1]
END
Go
CREATE  FUNCTION [dbo].[fnGetMorbidityMethoSummary1]
(	
	@ForecastId int
)
RETURNS @ForecastMSummary TABLE 
(

Methodology nvarchar(64),
ProductType nvarchar(64),
Fyear int, 
Quantity DECIMAL(18,9), 
TotalPrice DECIMAL(18,9),
Duration nvarchar(100),
DurationDateTime datetime,
Title varchar(100)

)
AS
BEGIN
INSERT INTO @ForecastMSummary 
(  
Methodology,
ProductType,
Fyear, 
Quantity, 
TotalPrice,
Duration,
DurationDateTime,
Title 
)  
SELECT Methodology,ProductType,Fyear, Quantity, TotalPrice,Duration,DurationDateTime,Title  FROM insertReportData  where ForecastId=@ForecastId
return 
end 


GO

IF EXISTS (SELECT * FROM sysobjects WHERE name='fnGetMorbidityMethoSummary' AND xtype='TF')
BEGIN
DROP FUNCTION [dbo].[fnGetMorbidityMethoSummary]
END
Go

Create  FUNCTION [dbo].[fnGetMorbidityMethoSummary]
(	
	@ForecastId int
)
RETURNS @ForecastMSummary TABLE 
(

Methodology nvarchar(64),
ProductType nvarchar(64),
Fyear int, 
Quantity DECIMAL(18,9), 
TotalPrice DECIMAL(18,9),
Duration nvarchar(100),
DurationDateTime datetime,
Title varchar(100)

)
AS
BEGIN
Declare @siteTable table
(
    TestID bigint,
    TstNo decimal(18,2),
    [Month] nvarchar(100),
	[ExistingPatient] decimal(18,2),
	[Duration] decimal(18,2),
	[TotalTestPerYear] decimal(18,2),
	[TestRepeatPerYear] decimal(18,2),
	[SymptomTestPerYear] decimal(18,2),
	[NewPatient] decimal(18,2)
)
Declare @ForecastMSummary1 TABLE 
(
Methodology nvarchar(64),
ProductType nvarchar(64),
Fyear int, 
Quantity DECIMAL(18,9), 
TotalPrice DECIMAL(18,9),
Duration nvarchar(100),
DurationDateTime datetime,
Title varchar(100),
Adjustedpacksize  DECIMAL(18,9),
Productneed  DECIMAL(18,9),
adjProgramgrowthrate DECIMAL(18,9),
ProgramGrowRate DECIMAL(18,9),
totalquantityinpack DECIMAL(18,9),
WastageRate DECIMAL(18,9),
ProductId   bigint ,
month1 varchar(max)
)
Declare @ForecastMSummary2 TABLE 
(
Methodology nvarchar(64),
ProductType nvarchar(64),
Fyear int, 
Quantity DECIMAL(18,9), 
TotalPrice DECIMAL(18,9),
Duration nvarchar(100),
DurationDateTime datetime,
Title varchar(100),
Adjustedpacksize  DECIMAL(18,9),
Productneed  DECIMAL(18,9),
adjProgramgrowthrate DECIMAL(18,9),
ProgramGrowRate DECIMAL(18,9),
totalquantityinpack DECIMAL(18,9),
WastageRate DECIMAL(18,9),
ProductId   bigint ,
month1 varchar(max)
)
declare @TestID bigint,
    @TstNo decimal(18,2),
	@TestNo decimal(18,2),
    @Month nvarchar(100),
	@Existing decimal(18,2),
	@FExisting decimal(18,2),
	@Duration1 decimal(18,2),
	@TotalTestPerYear decimal(18,2),
	@TestRepeatPerYear decimal(18,2),
	@SymptomTestPerYear decimal(18,2),
	@PerYear decimal(18,2),
	@testPermonth decimal(18,2),
	@totaltest decimal(18,2),
	@newpatienttestnumber  decimal(18,2),
	@NewPatient decimal(18,2),
	@Title nvarchar(100),
	@Duration varchar(50),
	@DurationDateTime datetime,
	@ForecastPeriod varchar(100),
	@WorkingDays decimal(10,2),
	@ForecastType varchar(10),
	@TypeName varchar(100),
	@Quantity decimal(18,2),
	@TypeID bigint,
	@TotalPrice decimal(18,2),
	@Variablecount int,
	@Adjustedpacksize  DECIMAL(18,9),
	@Productneed  DECIMAL(18,9),
	@adjProgramgrowthrate DECIMAL(18,9),
	@ProgramGrowRate DECIMAL(18,9),
	@totalquantityinpack DECIMAL(18,9),
	@WastageRate DECIMAL(18,9),
	@ProductId   bigint ,
    @month1 varchar(max),
	@query varchar(max),
    @Productneed_  DECIMAL(18,9),
	@adjProgramgrowthrate_ DECIMAL(18,9),
	@Adjustedpacksize_ DECIMAL(18,9),
	@totalquantityinpack_ DECIMAL(18,9),

	@count int;
	set @count=0
select @Title=ForecastNo from ForecastInfo Where [ForecastID]=@ForecastId
select @ForecastType=Isnull([ForecastType],'') from ForecastInfo Where [ForecastID]=@ForecastId
select @ForecastPeriod=Right(Replace(convert(varchar(11), StartDate, 106),' ','-'),8) + '    '+ Right(Replace(convert(varchar(11), ForecastDate, 106),' ','-'),8) from ForecastInfo Where [ForecastID]=@ForecastId
select @Duration=datename(m,ForecastDate)+'-'+cast(datepart(yyyy,ForecastDate) as varchar) from ForecastInfo Where [ForecastID]=@ForecastId
select @DurationDateTime=ForecastDate from ForecastInfo Where [ForecastID]=@ForecastId




if @ForecastType='S'  
	BEGIN 
	select @WorkingDays= Avg(s.WorkingDays)  FROM [ForecastSiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=@ForecastId
	END 
ELSE 
	  BEGIN 
	 select @WorkingDays= Avg(s.WorkingDays)  FROM [ForecastCategorySiteInfo] FSI Left join site s on s.SiteID =FSI.SiteID  where ForecastInfoID=@ForecastId
	  End 

insert into @siteTable(TestID,TstNo,[Month],[ExistingPatient],[Duration],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear],[NewPatient])
SELECT TestID,TstNo,[Month],[ExistingPatient],[Duration],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear],[NewPatient]   FROM [TestByMonth] where ForecastId=@ForecastId order by SNo

SELECT top 1  @Existing =ISNULL((ExistingPatient),0) , @Duration1=isnull(Duration,0),@TotalTestPerYear=isnull(TotalTestPerYear,0) FROM TestByMonth where ForeCastID=@ForecastId  order by SNo
set @Existing=(@Existing * (@Duration1 * @TotalTestPerYear))
Select @Variablecount=count(*) from @siteTable
set @FExisting=(@Existing / @Variablecount)


DECLARE cur CURSOR FOR SELECT TestID, TstNo,[Month],[ExistingPatient],[Duration],[TotalTestPerYear],[TestRepeatPerYear],[SymptomTestPerYear],[NewPatient]  FROM @siteTable
OPEN cur
FETCH NEXT FROM cur INTO @TestID, @TestNo,@Month,@Existing,@Duration1,@TotalTestPerYear,@TestRepeatPerYear,@SymptomTestPerYear,@NewPatient 
WHILE @@FETCH_STATUS = 0 BEGIN

select @PerYear = (@TestRepeatPerYear + @SymptomTestPerYear)
select @testPermonth = (@TestNo * @NewPatient)
select @totaltest = (@testPermonth + @FExisting)
select @TstNo = (@totaltest + (@totaltest * (@PerYear / 100)))


 DECLARE cur1 CURSOR FOR 

--select TypeID,TypeName,sum(Price*Packsize),sum(Price*Packsize) as TotalPrice from (
select TypeID,TypeName,Packsize as Quantity,(Price) as TotalPrice,Productneed,ProgramGrowRate,WastageRate,ProductId,month1 from (
select T.ProductId,Max(PT.TypeID) as TypeID,Max(PT.TypeName) as TypeName ,Max(T.ProductName) as ProductName,T.month1 as month1, Sum(T.testnumber) as testnumber,sum(Productneed) as Productneed,Max(PackSize) as Packsize,Max(Price) as Price,
 @Title as title,@ForecastPeriod as forecastperiod, Max(Isnull(ProgramGrowRate,0)) as ProgramGrowRate,Max(Isnull(WastageRate,0)) as WastageRate  
 from(SELECT  PU.ProductId as ProductId,mp.ProductName As ProductName,PU.TestId As TestId, @TstNo  As testnumber, Cast(((PU.Rate*@TstNo)*Isnull(T.Percentage,0)/100) as numeric(18,2)) As Productneed ,@Month as month1   FROM [ProductUsage] PU      
 Left Join Instrument ins on ins.InstrumentID=PU.InstrumentId   
 Left join MasterProduct mp on mp.ProductID=PU.ProductId 
 Left join TestingArea TA on TA.TestingAreaID=ins.TestingAreaID  
 Left join  (select Max(T.testingArea) As testingArea,   Max(T.instrumentname) As instrumentname,Avg(T.Quantity) as Quantity,  Avg(T.Percentage) As Percentage,T.InstrumentID,T.TestingAreaID   from	   
 (  SELECT SI.SiteID,TA.AreaName As testingArea ,  Ins.InstrumentName As instrumentname,SI.Quantity As Quantity, SI.TestRunPercentage As Percentage,Ins.InstrumentID,TA.TestingAreaID  FROM [ForecastSiteInfo] FS     
 Left join SiteInstrument SI on SI.SiteID=FS.SiteID  
 Left join Instrument Ins on Ins.InstrumentID=SI.InstrumentID 	  
 Left join TestingArea TA on TA.TestingAreaID=Ins.TestingAreaID   where FS.ForecastinfoID=@ForecastId) As T Group by TestingAreaID,InstrumentID) 	    As T on T.TestingAreaID=TA.TestingAreaID and T.InstrumentID=ins.InstrumentID  
 where PU.TestId =@TestID	and PU.IsForControl=0 	
 Union 
 select mp.productID as ProductId,mp.ProductName As ProductName,PU.TestId As TestId,@TstNo As testnumber, (Case when Ins.DailyCtrlTest>0 then @WorkingDays *1*pu.Rate*INS.DailyCtrlTest  when INS.MaxTestBeforeCtrlTest>0  then 0 when INS.WeeklyCtrlTest>0 then  1*4*pu.Rate*INS.WeeklyCtrlTest  when INS.MonthlyCtrlTest>0 then 1*pu.Rate*INS.MonthlyCtrlTest  when Ins.QuarterlyCtrlTest>0 then (1/4)*pu.Rate*INS.QuarterlyCtrlTest else 0 end) as Productneed ,
 @Month as month1     from ProductUsage PU  
 lEFT JOIN Instrument INS ON ins.InstrumentID=pu.InstrumentId  
 lEFT JOIN MasterProduct mp ON MP.ProductID=PU.ProductId  
 Left join ProductType PT on PT.TypeID=mp.ProductTypeId  where PU.IsForControl=1 and PU.TestId=@TestID	

 Union   
 select Mp.ProductID,MP.ProductName,Mc.TestId As TestId,@TstNo As testnumber,  
 (case when Pertest>0 and @TstNo >0 then (@TstNo/PerTest)*CU.UsageRate   when CU.Period='Daily' then 1*@WorkingDays*CU.UsageRate   
 when CU.Period='Weekly' then 1*4*Isnull((select  Isnull(avg(Quantity),0)    
 from SiteInstrument where siteid  in (select SiteID from ForecastSiteInfo where ForecastinfoID=89) 
 and InstrumentID =Ins.InstrumentID    group by InstrumentID),0) else 0 end ) as productneed,@Month as month1  from ConsumableUsage CU  
 Left join MasterConsumable MC on MC.MasterCID=CU.ConsumableId  
 Left join MasterProduct MP on mp.ProductID=CU.ProductId  
 Left join Instrument Ins on Ins.InstrumentID= CU.InstrumentId  where    Isnull(mc.TestId,'')=@TestID) As T 
 Left join MasterProduct mp on mp.ProductID = T.ProductId    
 Left join ProductType PT on PT.TypeID = mp.ProductTypeID 
 Left join Productprice pp on pp.ProductId=T.ProductId   
 Left join (SELECT *   FROM [TestingAssumption] where ForecastinfoID=@ForecastId)  as TA on TA.ProductTypeID=PT.TypeID    group by T.month1,T.ProductId 
 ) f 



 --group by f.TypeID,f.TypeName

		OPEN cur1
		FETCH NEXT FROM cur1 INTO @TypeID,@TypeName, @Quantity,@TotalPrice,@Productneed,@ProgramGrowRate,@WastageRate,@ProductId,@month1 
		WHILE @@FETCH_STATUS = 0 BEGIN
		
		select  @Adjustedpacksize = (Ceiling((@Productneed) /(@Quantity)))
		select  @adjProgramgrowthrate =Ceiling(((@Adjustedpacksize) + ((@Adjustedpacksize) * ((@ProgramGrowRate) / 100))))
		select @totalquantityinpack= Ceiling(@adjProgramgrowthrate + (@adjProgramgrowthrate * (@WastageRate / 100)))
		if @count=0 
		begin
		insert into @ForecastMSummary1 values ('Morbidity Statistics',@TypeName,1,@Quantity,@TotalPrice,@Duration,@DurationDateTime,@Title,@Adjustedpacksize,@Productneed,@adjProgramgrowthrate,@ProgramGrowRate,@totalquantityinpack,@WastageRate,@ProductId,@month1)
		insert into @ForecastMSummary2 values ('Morbidity Statistics',@TypeName,1,@Quantity,@TotalPrice,@Duration,@DurationDateTime,@Title,@Adjustedpacksize,@Productneed,@adjProgramgrowthrate,@ProgramGrowRate,@totalquantityinpack,@WastageRate,@ProductId,@month1)
			END
			else
			BEGIN 
			SELECT   @Productneed_ =ISNULL((Productneed),0) , @adjProgramgrowthrate_=isnull(adjProgramgrowthrate,0),@Adjustedpacksize_=isnull(Adjustedpacksize,0) ,@totalquantityinpack_=isnull(totalquantityinpack,0) FROM @ForecastMSummary2 where ProductId=@ProductId and month1=@month1
			if @Productneed_!=0 BEGIN 
			insert into @ForecastMSummary2 values ('Morbidity Statistics',@TypeName,1,@Quantity,@TotalPrice,@Duration,@DurationDateTime,@Title,(@Adjustedpacksize+@Adjustedpacksize_),(@Productneed+@Productneed_),(@adjProgramgrowthrate+@adjProgramgrowthrate_),@ProgramGrowRate,(@totalquantityinpack+@totalquantityinpack_),@WastageRate,@ProductId,@month1)
			END 
			else 
			BEGIN
			insert into @ForecastMSummary2 values ('Morbidity Statistics',@TypeName,1,@Quantity,@TotalPrice,@Duration,@DurationDateTime,@Title,@Adjustedpacksize,@Productneed,@adjProgramgrowthrate,@ProgramGrowRate,@totalquantityinpack,@WastageRate,@ProductId,@month1)
			END
			END 
			FETCH NEXT FROM cur1 INTO @TypeID,@TypeName, @Quantity,@TotalPrice,@Productneed,@ProgramGrowRate,@WastageRate,@ProductId,@month1
		END
		CLOSE cur1    
DEALLOCATE cur1
set @count=@count+1
FETCH NEXT FROM cur INTO @TestID, @TestNo,@Month,@Existing,@Duration1,@TotalTestPerYear,@TestRepeatPerYear,@SymptomTestPerYear,@NewPatient 

END

CLOSE cur    
DEALLOCATE cur


insert into @ForecastMSummary
SELECT max(Methodology),ProductType,max(Fyear),sum(totalquantityinpack),sum(totalquantityinpack*TotalPrice),max(Duration),max(DurationDateTime),max(Title)
FROM @ForecastMSummary2
group by ProductType
return 
end 

GO

IF EXISTS (SELECT * FROM sysobjects WHERE name='fnGetForecastComparisionSummary' AND xtype='TF')
BEGIN
DROP FUNCTION [dbo].[fnGetForecastComparisionSummary]
END
Go

CREATE FUNCTION [dbo].[fnGetForecastComparisionSummary]
(	@MForecastId int=-1,
	@SForecastId int=-1,
	@CForecastId int=-1
)
RETURNS @ForecastSummary TABLE 
(
Methodology nvarchar(64),
ProductType nvarchar(64),
FYear int, 
Quantity DECIMAL(18,9), 
TotalPrice DECIMAL(18,9),
Duration nvarchar(100),
DurationDateTime datetime,
Title nvarchar(100)
)
AS
BEGIN
DECLARE @Title Nvarchar(64)
select @Title = Title from MorbidityForecast where ForecastID = @MForecastId 
INSERT INTO @ForecastSummary 
Select *,@Title from dbo.fnGetMorbidityForecastSummary(@MForecastId)
select @Title = ForecastNo from ForecastInfo where ForecastID = @SForecastId
INSERT INTO @ForecastSummary 
Select *,@Title from dbo.fnGetServiceForecastSummary(@SForecastId)
--where 
--1= case when @MForecastId=-1 then 1 when Fyear=(Select top 1 Fyear from dbo.fnGetMorbidityForecastSummary(@MForecastId))then 1 end
select @Title = ForecastNo from ForecastInfo where ForecastID = @CForecastId
INSERT INTO @ForecastSummary 
Select *,@Title from dbo.fnGetConsumptionForecastSummary(@CForecastId) 
INSERT INTO @ForecastSummary 
Select * from dbo.fnGetMorbidityMethoSummary1(@MForecastId) 
--where
--1= case when @MForecastId=-1 then 1 when Fyear=(Select top 1 Fyear from dbo.fnGetMorbidityForecastSummary(@MForecastId))then 1 end
RETURN
END

GO



IF EXISTS (SELECT * FROM sysobjects WHERE name='AddDefaultProgram' AND xtype='P')
BEGIN
DROP Procedure [dbo].[AddDefaultProgram]
END
Go 
Create procedure [dbo].[AddDefaultProgram]
 As
begin
IF NOT EXISTS 
(
   SELECT Id
   FROM MMProgram
   WHERE ProgramName = 'HIV' 
)
BEGIN
SET NOCOUNT ON;
INSERT INTO MMProgram
(
  ProgramName,NoofYear

)
Values (
   'HIV',1
)
Declare @Id as int
SET @Id=SCOPE_IDENTITY()
INSERT INTO [dbo].[MMForecastParameter]
           ([ForecastMethod]
           ,[VariableName]
           ,[VariableDataType]
           ,[UseOn]          
           ,[ProgramId]
           ,[VarCode]
           ,[IsPrimaryOutput]
           ,[VariableEffect]
           ,[IsActive])
     VALUES
           (1
           ,'CurrentPatient'
           ,1
           ,'OnAllSite'
           
           ,@Id
           ,'CP'
           ,0
           ,1
           ,1)
 INSERT INTO [dbo].[MMForecastParameter]
           ([ForecastMethod]
           ,[VariableName]
           ,[VariableDataType]
           ,[UseOn]          
           ,[ProgramId]
           ,[VarCode]
           ,[IsPrimaryOutput]
           ,[VariableEffect]
           ,[IsActive])
     VALUES
           (1
           ,'TargetPatient'
           ,1
           ,'OnAllSite'
           
           ,@Id
           ,'TP'
           ,0
           ,1
           ,1)

		   
INSERT INTO dbo.MMGroup (GroupName, ProgramId, IsActive)
VALUES ('Adult', @Id, 1)
INSERT INTO dbo.MMGroup (GroupName, ProgramId, IsActive)
VALUES ('Pediatric', @Id, 1)
		   


INSERT INTO dbo.MMGeneralAssumption (VariableName, VariableDataType, UseOn, VariableFormula, ProgramId, VarCode, AssumptionType, VariableEffect, IsActive)
VALUES ('Lost to Follw-up', 2, 'OnEachSite', NULL, @Id, 'HIV', 1, 0, 1)
IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name IN (N'Lost to Follw-up') AND Object_ID = Object_ID(N'PatientAssumption')) 
BEGIN 
ALTER TABLE PatientAssumption ADD [Lost to Follw-up] numeric(18,2)
 END

 INSERT INTO dbo.MMGeneralAssumption (VariableName, VariableDataType, UseOn, VariableFormula, ProgramId, VarCode, AssumptionType, VariableEffect, IsActive)
VALUES ('% of Repeat Test/year', 2, 'OnEachSite', NULL, @Id, 'TR', 3, 1, 1)
IF COL_LENGTH('TestingProtocol','% of Repeat Test/year') IS NULL 
BEGIN  alter table TestingProtocol add  [% of Repeat Test/year] numeric(18, 2)  
END
IF COL_LENGTH('Testbymonth','% of Repeat Test/year') IS NULL
 BEGIN  alter table Testbymonth add  [% of Repeat Test/year] numeric(18, 2)  END 
IF COL_LENGTH('percentageval','% of Repeat Test/year') IS NULL 
BEGIN  alter table percentageval add  [% of Repeat Test/year] numeric(18, 2)  END 


INSERT INTO dbo.MMGeneralAssumption (VariableName, VariableDataType, UseOn, VariableFormula, ProgramId, VarCode, AssumptionType, VariableEffect, IsActive)
VALUES ('% of Symptom directed Test/Year', 2, 'OnEachSite', NULL, @Id, 'STY', 3, 1, 1)

IF COL_LENGTH('TestingProtocol','% of Symptom directed Test/Year') IS NULL 
BEGIN  alter table TestingProtocol add  [% of Symptom directed Test/Year] numeric(18, 2)  
END
IF COL_LENGTH('Testbymonth','% of Symptom directed Test/Year') IS NULL
 BEGIN  alter table Testbymonth add  [% of Symptom directed Test/Year] numeric(18, 2)  END 
IF COL_LENGTH('percentageval','% of Symptom directed Test/Year') IS NULL 
BEGIN  alter table percentageval add  [% of Symptom directed Test/Year] numeric(18, 2)  END 

INSERT INTO dbo.MMGeneralAssumption (VariableName, VariableDataType, UseOn, VariableFormula, ProgramId, VarCode, AssumptionType, VariableEffect, IsActive)
VALUES ('Wastage Rate', 2, 'OnEachSite', NULL, @Id, 'WR', 2, 1, 1)
IF COL_LENGTH('TestingAssumption','Wastage Rate') IS NULL 
BEGIN  
alter table TestingAssumption add  [Wastage Rate] numeric(18, 2) 
 END 



INSERT INTO dbo.MMGeneralAssumption (VariableName, VariableDataType, UseOn, VariableFormula, ProgramId, VarCode, AssumptionType, VariableEffect, IsActive)
VALUES ('Program growth rate during the forecasting Period', 2, 'OnEachSite', NULL, @Id, 'PP', 2, 1, 1)

IF COL_LENGTH('TestingAssumption','Program growth rate during the forecasting Period') IS NULL 
BEGIN  
alter table TestingAssumption add  [Program growth rate during the forecasting Period] numeric(18, 2)  END 


END
END

GO







IF COL_LENGTH('MMProgram','NoofYear') IS NULL
BEGIN
 alter table MMProgram
  add  NoofYear int NULL
END
GO


Exec [AddDefaultProgram]
Go