﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Drawing;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.UserExceptions;
using LQT.Core.Util;
using LQT.GUI.UserCtr;
using System.Runtime.InteropServices;


namespace LQT.GUI
{
    public partial class ImportConForm : Form
    {
        private ForecastInfo _forecastInfo;
        private IList<ReportedData> _rdata;
        private int _noColumn;

        public ForecastInfo GetForecastInfo
        {
            get { return _forecastInfo; }
        }

        public ImportConForm(ForecastInfo finfo)
        {
            this._forecastInfo = finfo;
            InitializeComponent();

            txtForecastid.Text = _forecastInfo.ForecastNo;
            txtPeriod.Text = _forecastInfo.Period;
            txtSdate.Text = _forecastInfo.StartDate.ToShortDateString();
            txtExtension.Text = _forecastInfo.Extension.ToString();
            txtMethodology.Text = _forecastInfo.Methodology;
            txtDusage.Text = _forecastInfo.DataUsage;

            if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
            {
                lvImport.Columns.Remove(lvImport.Columns[2]);
                lvImport.Columns.Remove(lvImport.Columns[2]);

                _noColumn = 6;
            }
            else
            {
                lvImport.Columns.Remove(lvImport.Columns[1]);
                _noColumn = 7;
            }
        }

        private void butBrowse_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                txtFilename.Text = openFileDialog1.FileName;
            }
        }
               
        private IList<ReportedData> GetDataRow(DataSet ds)
        {
            string categoryName = null;
            string regionName;
            string siteName;
            string productName;
            string duration;
            decimal amount;
            int stockout;
            int instrumentDownTime;
            decimal adjusited;
            int rowno = 0;
            bool haserror;
            string cName = "";
            string rName = "";
            string sName = "";
            string pName = "";
            int errornos = 0;
            string errormsg = "";
            ForecastCategory fcategory = null;
            ForlabRegion region = null;
            ForlabSite site = null;
            MasterProduct product = null;
            //Test test = null;
            string errorDescription = "";
            IList<ReportedData> rdlist = new List<ReportedData>();
            DataRow dr = ds.Tables[0].Rows[0];
            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //try
            //{
           // bool newerror = false;
          
        //    int errornos = 0;
            DataTable dtexcelcolor = new DataTable();
            dtexcelcolor.Columns.Add("Sheet");
            dtexcelcolor.Columns.Add("Row");
            dtexcelcolor.Columns.Add("Error");
            dtexcelcolor.Columns.Add("Color");
            dtexcelcolor.AcceptChanges();

            for (int i = 1; i < ds.Tables[0].Rows.Count; i = i + 4)//ds.Tables[0].Rows.Count
                {
                    rowno++;
                    haserror = false;
                    DataRow dr1 = ds.Tables[0].Rows[i];
                    DataRow dr2 = ds.Tables[0].Rows[i + 1];
                    DataRow dr3 = ds.Tables[0].Rows[i + 2];
                    DataRow dr4 = ds.Tables[0].Rows[i + 3];
                    DataRow g = ds.Tables[0].Rows[0];
                    int f = 3;
                    int colid;//0 
                    if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                        colid = 3;
                    else
                        colid = 4;
                    label7.Text = "Importing Data.....................";
                    int noofcolumn = 0;
                    noofcolumn = ds.Tables[0].Columns.Count - 1;
                    bool newerror = false;
                    do
                    {
                        if (dr1[noofcolumn] == System.DBNull.Value)
                            dr1[noofcolumn] = 0;
                        Decimal amount1 = Convert.ToDecimal(dr1[noofcolumn]);
                        if (Convert.ToDecimal(dr1[noofcolumn]) == 0)
                        {
                            newerror = true;
                            break;
                        }

                        noofcolumn--;
                    }
                    while (noofcolumn >= ds.Tables[0].Columns.Count - 3);
                    if (newerror == true)
                    {
                        errornos++;
                        if (errornos > 20)
                        {
                            label7.Text = "";
                            throw new Exception("There too many problem with Consumption data, please troubleshoot and try to import again.");
                        }
                        errormsg = "The product is not consumed in last three months";
                        dtexcelcolor.Rows.Add(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errormsg, Color.Red);
                      //  changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errormsg, Color.Red);
                        errormsg = "Please Check excel Some product are not consumed in recent last three months";

                        continue;

                    }
                    else
                    {
                        if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                            colid = 3;
                        else
                            colid = 4;

                    }
                  //  label7.Text = "Importing Data.....................";
                    do
                    {
                        regionName = "";
                        siteName = "";
                        errorDescription = "";
                        if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                        {
                            categoryName = Convert.ToString(dr1[0]).Trim();//(dr[colid++])
                            productName = Convert.ToString(dr1[1]).Trim();   //(dr[colid++])product name
                        }
                        else
                        {
                            regionName = Convert.ToString(dr1[0]).Trim(); //(dr[colid++]) region name
                            siteName = Convert.ToString(dr1[1]).Trim();           //(dr[colid++]) site name
                            productName = Convert.ToString(dr1[2]).Trim();   //(dr[colid++])product name
                        }
                        if (_forecastInfo.PeriodEnum != ForecastPeriodEnum.Monthly || _forecastInfo.PeriodEnum != ForecastPeriodEnum.Bimonthly)
                            duration = Convert.ToString(DateTime.FromOADate(Convert.ToDouble(dr[colid])));//(g[f]) Convert.ToString(dr[colid++]); //  reporting period(duration)
                        else
                            duration = Convert.ToString(dr[colid]);
                        try
                        {
                            if (dr1[colid] == System.DBNull.Value)
                                dr1[colid] = 0;
                           amount = Convert.ToDecimal(dr1[colid]);  //amount
                        //amount = 0;
                        //    //}
                        //    if (amount == 0)
                        //    {
                           
                        //    }
                        }
                        catch
                        {
                            haserror = true;
                            amount = 0;
                        }
                        try
                        {
                            if (dr2[colid] == System.DBNull.Value)
                                dr2[colid] = 0;
                            stockout = Convert.ToInt32(dr2[colid]);     //stock out
                        }
                        catch
                        {
                            haserror = true;
                            stockout = 0;
                        }
                        try
                        {
                            if (dr3[colid] == System.DBNull.Value)
                                dr3[colid] = 0;
                            instrumentDownTime = Convert.ToInt32(dr3[colid]);     //instrumentDownTime
                        }
                        catch
                        {
                            haserror = true;
                            instrumentDownTime = 0;
                        }


                        adjusited = 0;
                        ReportedData rd = null;

                        if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                        {
                            rd = new ReportedData(rowno, categoryName, productName, duration, amount, stockout, instrumentDownTime);//b
                            if (cName != categoryName)
                            {
                                if (!string.IsNullOrEmpty(categoryName))
                                {
                                    fcategory = DataRepository.GetForecastCategoryByName(_forecastInfo.Id, categoryName);
                                }
                                else
                                    fcategory = null;
                                cName = categoryName;
                            }

                            if (fcategory != null)
                                rd.Category = fcategory;
                            else
                            {
                                rd.HasError = true;
                                errorDescription = errorDescription + " Category Doesn't Exist";
                                dtexcelcolor.Rows.Add(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errorDescription, Color.Red);
                             //   changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, " Category Doesn't Exist", Color.Red);
                            }

                        }
                        else
                        {
                            rd = new ReportedData(rowno, regionName, siteName, productName, duration, amount, stockout, instrumentDownTime);//b

                            if (rName != regionName)
                            {
                                if (!string.IsNullOrEmpty(regionName))
                                    region = DataRepository.GetRegionByName(regionName);
                                else
                                    region = null;
                                rName = regionName;
                            }

                            if (region != null)
                            {
                                rd.Region = region;
                                if (sName != siteName)
                                {
                                    if (!string.IsNullOrEmpty(siteName))
                                        site = DataRepository.GetSiteByName(siteName, region.Id);
                                    else
                                        site = null;
                                    sName = siteName;
                                }
                                if (site != null)
                                    rd.Site = site;
                                else
                                {
                                    haserror = true;
                                    errorDescription = errorDescription + " Site Doesn't Exist";
                                    dtexcelcolor.Rows.Add(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errorDescription, Color.Red);
                              //      changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, " Site Doesn't Exist", Color.Red);
                                }
                            }
                            else
                            {
                                haserror = true;
                                errorDescription = errorDescription + " Region Doesn't Exist";
                                dtexcelcolor.Rows.Add(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errorDescription, Color.Red);
                              //  changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, " Region Doesn't Exist", Color.Red);
                            }
                        }

                        if (pName != productName)
                        {
                            if (!string.IsNullOrEmpty(productName))
                                product = DataRepository.GetProductByName(productName);
                            else
                                product = null;
                            pName = productName;
                        }

                        if (product != null)
                            rd.Product = product;
                        else
                        {
                            haserror = true;
                            errorDescription = errorDescription + " Product Doesn't Exist";
                            dtexcelcolor.Rows.Add(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, errorDescription, Color.Red);
                         //   changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), i + 1, " Product Doesn't Exist", Color.Red);
                        }

                        rd.HasError = haserror;
                        rd.ErrorDescription = errorDescription;
                        rdlist.Add(rd);
                        //rd.RowNo
                        colid++;
                        errorDescription = "";
                    }
                   while (colid < g.ItemArray.Length && g[colid].ToString() != "");// dr.ItemArray.Length / ds.Tables[0].Rows.Count);
                }
                if (errormsg != "")
                {
                    label7.Text = errormsg;

                }

                dtexcelcolor.AcceptChanges();
               // changecolorexcelcolumnaddByDatatable(dtexcelcolor);
                if (dtexcelcolor.Rows.Count > 0)
                {
                    changecolorexcelcolumnaddByDatatable(dtexcelcolor);
                }
                return rdlist;
        }

        private void butImport_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilename.Text.Trim()))
                return;
            try
            {
                DataSet ds = LqtUtil.ReadExcelFileforecast(txtFilename.Text, _noColumn);
                int errornos = 0;
                string errormsg="";
                _rdata = GetDataRow(ds);
              
                lvImport.BeginUpdate();
                lvImport.Items.Clear();
                ForecastSite fs = new ForecastSite();
                ForecastCategorySite fcatsite = new ForecastCategorySite();

                bool isduplicate = false;
                ForecastSite efs = new ForecastSite();//existing
                IList<ForecastSiteProduct> existingFsp = new List<ForecastSiteProduct>();
                foreach (ForecastSite efss in _forecastInfo.ForecastSites)
                {
                    foreach (ForecastSiteProduct efsp in efss.SiteProducts)
                        existingFsp.Add(efsp);
                }
                progressBar1.Maximum = _rdata.Count;
                progressBar1.Visible = true;
                progressBar1.Enabled = true;
                int i = 0;
                foreach (ReportedData rd in _rdata)
                {
                    ListViewItem li = new ListViewItem(rd.RowNo.ToString());
                    i++;
                    progressBar1.Value = i;
                    if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                    {
                        li.SubItems.Add(rd.CategoryName);
                    }
                    else
                    {
                        li.SubItems.Add(rd.RegionName);
                        li.SubItems.Add(rd.SiteName);
                    }

                    li.SubItems.Add(rd.ProductName);
                    if (!LqtUtil.IsDateTime(rd.Duration))
                    {
                        
                        try
                        {
                            DateTime dd = LqtUtil.DurationToDateTime(rd.Duration);
                            if (rd.Duration.StartsWith("Q") && (_forecastInfo.PeriodEnum == ForecastPeriodEnum.Yearly))
                            {

                                rd.Duration = dd.Year.ToString();
                                li.SubItems.Add(LqtUtil.DatetimeToDurationStr(_forecastInfo.PeriodEnum, dd));
                            }
                            else
                            li.SubItems.Add(rd.Duration);
                        }
                        catch (Exception ex)
                        {
                            li.SubItems.Add(rd.Duration);
                            rd.HasError = true;
                        }
                       
                    }
                    else
                    {
                       
                        string datestr = LqtUtil.DatetimeToDurationStr(_forecastInfo.PeriodEnum, DateTime.Parse(rd.Duration));
                        if (!rd.Duration.StartsWith("Q"))
                        {
                            rd.Duration = LqtUtil.DatetimeToDurationStr(_forecastInfo.PeriodEnum, DateTime.Parse(rd.Duration));
                            if (_forecastInfo.PeriodEnum == ForecastPeriodEnum.Yearly)
                            {
                                li.SubItems.Add(datestr);
                              
                            }
                            else
                            {
                                li.SubItems.Add(rd.Duration);
                                
                            }
                        }
                        else
                        {
                            {
                                li.SubItems.Add(datestr);
                               
                            }
                            

                        }

                    }
                    li.SubItems.Add(rd.Amount.ToString());
                    li.SubItems.Add(rd.StockOut.ToString());
                    li.SubItems.Add(rd.InstrumentDownTime.ToString());//
                    //if (rd.HasError == true && rd.ErrorDescription == "")
                    //    rd.ErrorDescription = " Product Consumed Required ";
                    
               //     li.SubItems.Add(rd.ErrorDescription.ToString());
                    if (LqtUtil.validDate(rd.Duration, _forecastInfo.PeriodEnum))
                        rd.HasError = true;

                   
                   


                    //add to forecast site product
                    if (!rd.HasError && _forecastInfo.DatausageEnum != DataUsageEnum.DATA_USAGE3)
                    {

                        ForecastSiteProduct fp = new ForecastSiteProduct();
                        fs = _forecastInfo.GetForecastSiteBySiteId(rd.Site.Id);

                        if (fs == null)
                        {
                            fs = new ForecastSite();
                            fs.Site = rd.Site;
                            fs.ForecastInfo = _forecastInfo;
                            _forecastInfo.ForecastSites.Add(fs);
                        }
                        fp.ForecastSite = fs;
                        fp.Product = rd.Product;
                        if (!LqtUtil.IsDateTime(rd.Duration))
                        {
                            fp.CDuration = rd.Duration;
                            fp.DurationDateTime = LqtUtil.DurationToDateTime(rd.Duration);
                            
                        }
                        else
                        { 
                            fp.DurationDateTime = DateTime.Parse(rd.Duration);
                            fp.CDuration = LqtUtil.DatetimeToDurationStr(_forecastInfo.PeriodEnum, DateTime.Parse(rd.Duration)); 
                        }
                        fp.AmountUsed = rd.Amount;
                        fp.StockOut = rd.StockOut;
                        fp.InstrumentDowntime = rd.InstrumentDownTime;//b
                        if (fp.StockOut > 0)
                        {
                            int days = fp.StockOut;
                            decimal workingday = GetActiveSiteWorkingDays(fp);

                            if (days >= workingday)
                            {
                                days = 0;
                                fp.StockOut = 0;
                            }
                            if (days >= 0)
                                fp.StockOut = days;
                        }

                        //foreach (ForecastSiteProduct efsp in existingFsp)
                        //{
                        //    isduplicate = false;
                        //    if (fp.ForecastSite.Site.Region == efsp.ForecastSite.Site.Region)
                        //        if (fp.ForecastSite.Site == efsp.ForecastSite.Site)
                        //            if (fp.Product == efsp.Product && fp.DurationDateTime == efsp.DurationDateTime)
                        //            { isduplicate = true; break; }
                        //}
                        //foreach (ForecastSiteProduct fsp in fs.SiteProducts)
                        //{
                        //    isduplicate = false;
                        //    if (fp.ForecastSite.Site.Region == fsp.ForecastSite.Site.Region)
                        //        if (fp.ForecastSite.Site == fsp.ForecastSite.Site)
                        //            if (fp.Product == fsp.Product && fp.DurationDateTime == fsp.DurationDateTime)
                        //            { isduplicate = true; break; }
                        //}

                        //Check duplicate data by abhishek 04052018
                        isduplicate = CheckDuplicatedata(rd.SiteName.ToString(), rd.ProductName.ToString(), txtForecastid.Text.Trim());
                        if (!isduplicate)
                            fs.SiteProducts.Add(fp);
                    }
                  
                    //end adding

                    //add by category
                    if (!rd.HasError && _forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE3)
                    {
                            ForecastCategory fcat = new ForecastCategory();
                         ForecastCategoryProduct fp = new ForecastCategoryProduct();
                         fcat = _forecastInfo.GetForecastCategorybyname(rd.CategoryName);
                         if (fcat== null)
                         {
                             fcat = new ForecastCategory();
                             fcat.CategoryName = rd.CategoryName;
                             fcat.ForecastInfo = _forecastInfo;
                             _forecastInfo.ForecastCategories.Add(fcat);
                         }
                            

                        fp = fcat.GetFCatProduct(rd.Product.Id, rd.Duration);
                        isduplicate = false;

                        if (fp == null)
                            fp = new ForecastCategoryProduct();
                        else
                            isduplicate = true;

                        fp.Category = fcat;
                        fp.Product = rd.Product;
                        
                        fp.AmountUsed = rd.Amount;
                        fp.StockOut = rd.StockOut;
                        fp.InstrumentDowntime = rd.InstrumentDownTime;//b
                     
                        if (!LqtUtil.IsDateTime(rd.Duration))
                        {
                            fp.DurationDateTime = LqtUtil.DurationToDateTime(rd.Duration);
                            fp.CDuration = rd.Duration;
                        }
                        else
                        {
                            fp.DurationDateTime = DateTime.Parse(rd.Duration);
                            fp.CDuration = LqtUtil.DatetimeToDurationStr(_forecastInfo.PeriodEnum, DateTime.Parse(rd.Duration));
                        }

                        if (fp.StockOut > 0)
                        {
                            int days = fp.StockOut;
                            decimal workingday = 22;

                            if (days >= workingday)
                            {
                                days = 0;
                                fp.StockOut = 0;
                            }
                            if (days >= 0)
                                fp.StockOut = days;
                        }    
                        if (!isduplicate)
                            fcat.CategoryProducts.Add(fp);
                    }
                   
                    //end category adding
                    if (rd.HasError)
                    {
                        //errornos++;
                        //if (errornos > 20)
                        //{

                        //    MessageBox.Show("There too many problem with Consumption data, please troubleshoot and try to import again.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    return;
                        //}
                        //errormsg = "The product is not consumed in last three months";
                        //changecolorexcelcolumnadd(LqtUtil.sheetname.Remove(LqtUtil.sheetname.IndexOf('$')).Trim('\''), rd.RowNo, errormsg, Color.Red);
                      
                        li.BackColor = Color.Red;
                    }
                    else if (isduplicate)
                    {
                        li.ToolTipText = "Duplicate Data";
                        li.BackColor = Color.Yellow;
                        li.SubItems.Add("Duplicate Data");
                        rd.ErrorDescription = "Duplicate Data";
                    }
                    if (rd.HasError == true && rd.ErrorDescription == "")
                        rd.ErrorDescription = " Product Consumed Required ";
                    li.SubItems.Add(rd.ErrorDescription.ToString());
                    lvImport.Items.Add(li);
                }

                lvImport.EndUpdate();

                butClear.Enabled = true;
                butSave.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void changecolorexcelcolumnadd(string sheetname, int rowno, string errormsg, Color c)
        {

            try
            {
                
                Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();


                Microsoft.Office.Interop.Excel.Workbook workbook = application.Workbooks.Open(txtFilename.Text,Type.Missing,false);
                // Microsoft.Office.Interop.Excel.Worksheet worksheet1 = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets["Site Instrument"];

                Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[sheetname.TrimStart()]; //.Remove(sheetname.IndexOf('$')).Replace('\'', ' ')
                // worksheet.Columns.Insert(3);

                Microsoft.Office.Interop.Excel.Range usedRange = worksheet.UsedRange;

                //worksheet.Cells[1, 3] = "Error";
                //Microsoft.Office.Interop.Excel.Range addrow = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["C1"];
                //addrow.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                //addrow.Font.Bold = true;
                //addrow.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);

                //   addrow.col = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);
                //Microsoft.Office.Interop.Excel.Range row1 = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["C" + Convert.ToString(rowno) + ""];
                //row1.Value = "Duplicate";
                //int count = 0;

                Microsoft.Office.Interop.Excel.Range row = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["A" + Convert.ToString(rowno + 1) + ":D" + Convert.ToString(rowno + 1) + ""];
                Microsoft.Office.Interop.Excel.Range commentcolumn = worksheet.get_Range("D" + Convert.ToString(rowno + 1) + "");
                row.EntireRow.Font.Color = System.Drawing.ColorTranslator.ToOle(c);
                commentcolumn.ClearComments();
                commentcolumn.AddComment(errormsg);

                //if (!System.IO.File.Exists(@"C:\test2.xls"))
                //{
                //    workbook.SaveAs(@"c:\test2.xls");
                //}
                //else
                //{

                //    workbook.SaveAs(@"c:\test2(Copy).xls");

                //}
                application.DisplayAlerts = false;
              workbook.Save();

              //  workbook.Close(true, txtFilename.Text);



                application.Quit();
                Marshal.ReleaseComObject(worksheet);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(application);
            }
            catch (Exception ex)
            {
                // application.Quit();
            }

        }

        private void changecolorexcelcolumnaddByDatatable(DataTable dt)
        {
            Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook workbook = application.Workbooks.Open(txtFilename.Text, Type.Missing, false);
            Microsoft.Office.Interop.Excel.Worksheet worksheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets[dt.Rows[0]["Sheet"].ToString()]; //.Remove(sheetname.IndexOf('$')).Replace('\'', ' ')
            try
            {
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        Microsoft.Office.Interop.Excel.Range usedRange = worksheet.UsedRange;
                        Microsoft.Office.Interop.Excel.Range row = (Microsoft.Office.Interop.Excel.Range)usedRange.Range["A" + Convert.ToString(Convert.ToInt32(dt.Rows[i]["Row"].ToString()) + 1) + ":D" + Convert.ToString(Convert.ToInt32(dt.Rows[i]["Row"].ToString()) + 1) + ""];
                        Microsoft.Office.Interop.Excel.Range commentcolumn = worksheet.get_Range("D" + Convert.ToString(Convert.ToInt32(dt.Rows[i]["Row"].ToString()) + 1) + "");
                        row.EntireRow.Font.Color = System.Drawing.ColorTranslator.ToOle(Color.Red);
                        commentcolumn.ClearComments();
                        commentcolumn.AddComment(dt.Rows[i]["Error"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                // application.Quit();

            }
            finally
            {
                application.DisplayAlerts = false;
                workbook.Save();

                application.Quit();
                Marshal.ReleaseComObject(worksheet);
                Marshal.ReleaseComObject(workbook);
                Marshal.ReleaseComObject(application);
            }
        }
        
        private void butClear_Click(object sender, EventArgs e)
        {
            lvImport.BeginUpdate();
            lvImport.Items.Clear();
            lvImport.EndUpdate();
            butSave.Enabled = false;
            butClear.Enabled = false;
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateAdjustment();
                DataRepository.SaveOrUpdateForecastInfo(_forecastInfo);
                MessageBox.Show("Consumption data imported and saved successfully.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch
            {
                MessageBox.Show("Error: Unable to import and save consumption data.", "Importing", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateAdjustment()
        {
            if (_forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE1 || _forecastInfo.DatausageEnum == DataUsageEnum.DATA_USAGE2)
            {
                foreach (ForecastSite fs in _forecastInfo.ForecastSites)
                {
                    foreach (ForecastSiteProduct fp in fs.SiteProducts)
                    {
                        fp.Adjusted = fp.AmountUsed;
                        if (fp.AmountUsed == 0)
                        {
                            try
                            {
                                Consumption cs = GetConsumption(fp.Product.Id, fp);
                                fp.Adjusted = Math.Round(cs.TotalConsumption / cs.NoConsumption, 2, MidpointRounding.ToEven);
                            }
                            catch { fp.Adjusted = fp.AmountUsed; }
                        }
                       
                        if ((fp.InstrumentDowntime > 0 || fp.StockOut > 0) && fp.AmountUsed > 0)
                            fp.Adjusted = LqtUtil.GetAdjustedVolume(fp.AmountUsed, fp.StockOut + fp.InstrumentDowntime, _forecastInfo.PeriodEnum, GetActiveSiteWorkingDays(fp));//b
                    }
                }
            }
            else
            {
                foreach (ForecastCategory fc in _forecastInfo.ForecastCategories)
                {
                    foreach (ForecastCategoryProduct fp in fc.CategoryProducts)
                    {
                        fp.Adjusted = fp.AmountUsed;
                        if (fp.AmountUsed == 0)
                        {
                            try
                            {
                                Consumption cs = GetConsumption(fp.Product.Id, fp);
                                fp.Adjusted = Math.Round(cs.TotalConsumption / cs.NoConsumption, 2, MidpointRounding.ToEven);
                            }
                            catch { fp.Adjusted = fp.AmountUsed; }
                        }
                        if ((fp.InstrumentDowntime > 0 || fp.StockOut > 0) && fp.AmountUsed > 0)
                            fp.Adjusted = LqtUtil.GetAdjustedVolume(fp.AmountUsed, fp.StockOut + fp.InstrumentDowntime, _forecastInfo.PeriodEnum, 22);//b
                    }
                }
            }
           
        }

        

        public Consumption GetConsumption(int id,ForecastSiteProduct fp)
        {
            Consumption con = null;
            switch (_forecastInfo.DatausageEnum)
                {
                    case DataUsageEnum.DATA_USAGE1:
                    case DataUsageEnum.DATA_USAGE2:
                        con = fp.ForecastSite.ConsumptionByProduct(id);
                        break;
                    case DataUsageEnum.DATA_USAGE3:
                        con = fp.ForecastSite.ConsumptionByProduct(id);
                        break;
                }
            return con;
        }
        public Consumption GetConsumption(int id, ForecastCategoryProduct fp)
        {
            Consumption con = null;
            switch (_forecastInfo.DatausageEnum)
            {
                case DataUsageEnum.DATA_USAGE1:
                case DataUsageEnum.DATA_USAGE2:
                    con = fp.Category.ConsumptionByProduct(id);
                    break;
                case DataUsageEnum.DATA_USAGE3:
                    con = fp.Category.ConsumptionByProduct(id);
                    break;
            }
            return con;
        }


        public decimal GetActiveSiteWorkingDays(ForecastSiteProduct fp)
        {
            decimal workingday = 22;

            switch (_forecastInfo.DatausageEnum)
            {
                case DataUsageEnum.DATA_USAGE1:
                case DataUsageEnum.DATA_USAGE2:
                    workingday = fp.ForecastSite.Site.WorkingDays;
                    break;
            }
            return workingday;
        }

            private bool CheckDuplicatedata(string Site,string Product,string ForecastNo)
        { 
            bool Result=false;
            SqlCommand cmdForecast = null; SqlDataAdapter daForecast = null;
            SqlConnection connection = ConnectionManager.GetInstance().GetSqlConnection();
            using (cmdForecast = new SqlCommand("", connection))
            {
                if (connection.State == ConnectionState.Open)
                { }
                else
                { connection.Open(); }

                Product = Product.Replace("'", "''");
                Site = Site.Replace("'", "''");
                ForecastNo = ForecastNo.Replace("'", "''");
                string _sql = "select * from [ForecastInfo] FI inner join [dbo].[ForecastSite] FCS on fcs.ForecastInfoId=FI.ForecastID inner join  [dbo].[ForecastSiteProduct] Fsp on  FCS.Id=Fsp.ForecastSiteID  where  FI.ForecastNo='" + ForecastNo + "' and  FCS.SiteId =(Select SiteId from [dbo].[Site] where sitename='" + Site + "') and Fsp.ProductID=(Select ProductID from [dbo].[MasterProduct] where ProductName='" + Product + "')";
                cmdForecast.CommandText = _sql;
                DataTable dtduplicate = new System.Data.DataTable();
                daForecast = new SqlDataAdapter(cmdForecast);
                daForecast.Fill(dtduplicate);
                if (dtduplicate.Rows.Count > 0)
                    Result = true;
               
            }
            return Result;
        }    

    }

}
