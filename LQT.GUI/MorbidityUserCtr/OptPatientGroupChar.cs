﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class OptPatientGroupChar : NewBaseMorbidityControl
    {
       
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;

        private IList<MorbidityCustomeAssumption> _morbidityAssumptions;
        private bool _isedited = false;

        public IList<MorbidityCustomeAssumption> _updatedmorbidityAssumptions;
        
        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public OptPatientGroupChar(NewMorbidityForecast forecast)
        {
            this._forecast = forecast;
            
            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
            _morbidityAssumptions = new List<MorbidityCustomeAssumption>();
            InitializeComponent();
            //BuildDefaultAssumptions();
            BindCategorys();


           // LoadSiteListView();
        }


        public override IList<MorbidityCustomeAssumption> PMorbidity
        {
            get { return _morbidityAssumptions; }
            
        }

        public override string Title
        {
            get { return "Define Patient Group and Characterstics"; }
        }

        public override string Description
        {
            get
            {

                string desc = "<p> Define Patient Group and Characterstics.</p>";

                desc += "<p> System will generate Site/Category Data Import Template based on the defined variable <br>";

                desc += "<p>1. e.g Adult, Pediatric ... ";

 
                return desc;
            }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.OptOtherGenralAssumption;
            }
        }

        public override bool EnableNextButton()
        {
            return true;// _artSites.Count > 0;
        }


     
        private void BindCategorys()
        {
            lvCategory.BeginUpdate();
            lvCategory.Items.Clear();

            foreach (MorbidityCustomeAssumption cat in _morbidityAssumptions)
            {
                string[] row = { cat.VariableName, cat.DataType, cat.VariableValue.ToString() };
                var listViewItem = new ListViewItem(row);
                lvCategory.Items.Add(listViewItem);
                //ListViewItem li = new ListViewItem(cat.CategoryName) { Tag = cat };
                //lvCategory.Items.Add(li);
            }

            lvCategory.EndUpdate();
            
        }

        private void lvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }


        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

                OnNextButtonStatusChanged(true);
                _isedited = true;

        }


        private void lbtRemovesite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

             OnNextButtonStatusChanged(false);
            _isedited = true;
        }

        

        private void butSave_Click(object sender, EventArgs e)
        {
            //_activeCategory.CategoryName = txtCatname.Text;

            try
            {
              // DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
               BindCategorys();
            }
            catch
            {
                MessageBox.Show("Error: unable to save Group");
            }            
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure, you want to delete it? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    //_forecast.MorbidityCategories.Remove(_activeCategory);
                    //DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
                   
                    BindCategorys();
                  
                }
                catch
                {
                    MessageBox.Show("Error: unable to delete custom variable");
                }
            }
        }

        private void butAddnew_Click(object sender, EventArgs e)
        {
            MorbidityCustomeAssumption mc = new MorbidityCustomeAssumption();
            mc.VariableName = txtCatname.Text;
            mc.DataType = comboBox1.SelectedItem.ToString();
            mc.UseForSite = false;
            mc.UseForSite = false;

         
            mc.VariableValue = textBox1.Text;

            mc.MorbidityForecast = _forecast;

            _morbidityAssumptions.Add(mc);

           
            BindCategorys();
           
        }

        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.BatchSaveARTSite(_artSites);
                //DataRepository.BatchDeleteARTSite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            int x = e.KeyChar;

            if ((x >= 48 && x <= 57) || (x == 8) || (x == 46))
            {
                e.Handled = false;
            }
            else
                e.Handled = true;
        }

      

       

        

    }
}
