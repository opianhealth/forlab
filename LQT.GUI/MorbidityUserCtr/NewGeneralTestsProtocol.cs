﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewGeneralTestsProtocol : NewBaseMorbidityControl
    {
        private SiteListView _sListView;
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;
        private IList<NewMordidityCategorySite> _artSites;
        private IList<NewMordidityCategorySite> _deletedArtSites;
        private bool _isedited = false;

        private IList<Test> _test;

        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public NewGeneralTestsProtocol(NewMorbidityForecast forecast)
        {
            this._forecast = forecast;
           
            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
            _test=new List<Test>();
            InitializeComponent();
            //LoadSiteListView();
        }

        public NewGeneralTestsProtocol()
        {
            //this._forecast = forecast;

            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
            _test = new List<Test>();
            InitializeComponent();

        }

        public override string Title
        {
            get { return "General Test Protocols"; }
        }

        public override string Description
        {
            get
            {

                string desc = "<p> General Test Protocols.</p>";

                desc += "<p>1. Add Tests to include in the forecast. General Tests, excluding HIV Rapid Test</p><br>";
                desc += "<p>2. For The Added Tests, Select and define Protocol<br>";


                return desc;
            }
        }
        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.NewRapidTestProtocol;
            }
        }

        public override bool EnableNextButton()
        {
            return true;// 1;// _artSites.Count > 0;
        }



        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.BatchSaveNewMordidityCategorySite(_artSites);
                //DataRepository.BatchDeleteNewMordidityCategorySite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            if (AddTest())
            {
                BindTest();
                // SetStatusOfCatRadioButton();

                OnNextButtonStatusChanged(true);
                _isedited = true;
            }
        }

        public bool AddTest()
        {
            FrmNewSelectTest frm;

            frm = new FrmNewSelectTest();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (Test t in frm._selectedTest)
                {
                    _test.Add(t);
                }
                return true;
            }
            return false;
        }

        private void BindTest()
        {
            //lvCategory.BeginUpdate();
            //lvCategory.Items.Clear();

            //foreach (Test t in _test)
            //{
            //    ListViewItem li = new ListViewItem(t.TestName) { Tag = t };
            //    ListViewItem li1 = new ListViewItem(t.TestingArea.AreaName) { Tag = t };
            //    ListViewItem li2 = new ListViewItem(t.TestType) { Tag = t };
            //    //ListViewItem li = new ListViewItem(t.TestName) { Tag = t };
            //    lvCategory.Items.Add(li);
            //    //lvCategory.Items.Add(li1);
            //   // lvCategory.Items.Add(li2);
            //}

            //lvCategory.EndUpdate();
        }

        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
             LQT.GUI.MorbidityProtocolSetting.FrmNewProtocol frm;

             frm = new LQT.GUI.MorbidityProtocolSetting.FrmNewProtocol();
            frm.ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }


    }
}
