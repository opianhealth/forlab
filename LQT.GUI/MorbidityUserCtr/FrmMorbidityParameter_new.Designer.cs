﻿namespace LQT.GUI.MorbidityUserCtr
{
    partial class FrmMorbidityParameter_new
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabForecastMethod = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lvforecastParamsWiz2 = new System.Windows.Forms.ListView();
            this.c1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cobforecastmethodWiz2 = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.chkprimaryoutput = new System.Windows.Forms.CheckBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.rdbPosWiz2 = new System.Windows.Forms.RadioButton();
            this.rdbNegWiz2 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddWiz2 = new System.Windows.Forms.Button();
            this.btnaddformulaWiz2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbAggWiz2 = new System.Windows.Forms.RadioButton();
            this.rdbeachWiz2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.cobDatatypeWiz2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVariableNameWiz2 = new System.Windows.Forms.TextBox();
            this.tabDefprogram = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnProgramAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProgramName = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lvProgramList = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colprogramname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPatientGroup = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chkActiveGroup = new System.Windows.Forms.CheckBox();
            this.btnAddWiz3 = new System.Windows.Forms.Button();
            this.txtgroupnameWiz3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lvGroupWiz3 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabGeneralAssumption = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.chkGeneralAssumption = new System.Windows.Forms.CheckBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.rdbPosWiz4 = new System.Windows.Forms.RadioButton();
            this.rdbNegWiz4 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.cobAssumptionTypeWiz4 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnAddWiz4 = new System.Windows.Forms.Button();
            this.btnaddformulaWiz4 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.rdbAggWiz4 = new System.Windows.Forms.RadioButton();
            this.txtVariableNameWiz4 = new System.Windows.Forms.TextBox();
            this.rdbeachWiz4 = new System.Windows.Forms.RadioButton();
            this.cobDatatypeWiz4 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lvgeneralassumptions = new System.Windows.Forms.ListView();
            this.cc1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabTestingProtocol = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.chkrapidprotocol = new System.Windows.Forms.CheckBox();
            this.chkgeneralprotocol = new System.Windows.Forms.CheckBox();
            this.tabReview = new System.Windows.Forms.TabPage();
            this.lbltestingprotocol1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lstReviewGeneralAssumption = new System.Windows.Forms.ListView();
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label17 = new System.Windows.Forms.Label();
            this.lstReviewPatientGroup = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label16 = new System.Windows.Forms.Label();
            this.lstReviewForecastingVariable = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label13 = new System.Windows.Forms.Label();
            this.lblprogram1 = new System.Windows.Forms.Label();
            this.lblProgram = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.tabForecastMethod.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tabDefprogram.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPatientGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabGeneralAssumption.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabTestingProtocol.SuspendLayout();
            this.panel14.SuspendLayout();
            this.tabReview.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabForecastMethod
            // 
            this.tabForecastMethod.Controls.Add(this.tableLayoutPanel2);
            this.tabForecastMethod.Location = new System.Drawing.Point(4, 25);
            this.tabForecastMethod.Name = "tabForecastMethod";
            this.tabForecastMethod.Padding = new System.Windows.Forms.Padding(3);
            this.tabForecastMethod.Size = new System.Drawing.Size(1092, 579);
            this.tabForecastMethod.TabIndex = 1;
            this.tabForecastMethod.Tag = "Forecasting Method";
            this.tabForecastMethod.Text = "Forecasting Method";
            this.tabForecastMethod.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel7, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 269F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1086, 573);
            this.tableLayoutPanel2.TabIndex = 5;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lvforecastParamsWiz2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 314);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1080, 256);
            this.panel7.TabIndex = 1;
            // 
            // lvforecastParamsWiz2
            // 
            this.lvforecastParamsWiz2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvforecastParamsWiz2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c1,
            this.c7,
            this.c6,
            this.c2,
            this.c3,
            this.c4,
            this.c5});
            this.lvforecastParamsWiz2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvforecastParamsWiz2.FullRowSelect = true;
            this.lvforecastParamsWiz2.GridLines = true;
            this.lvforecastParamsWiz2.Location = new System.Drawing.Point(0, 0);
            this.lvforecastParamsWiz2.Name = "lvforecastParamsWiz2";
            this.lvforecastParamsWiz2.OwnerDraw = true;
            this.lvforecastParamsWiz2.Size = new System.Drawing.Size(1080, 256);
            this.lvforecastParamsWiz2.TabIndex = 36;
            this.lvforecastParamsWiz2.UseCompatibleStateImageBehavior = false;
            this.lvforecastParamsWiz2.View = System.Windows.Forms.View.Details;
            this.lvforecastParamsWiz2.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvforecastParamsWiz2_DrawColumnHeader);
            this.lvforecastParamsWiz2.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvforecastParamsWiz2_DrawItem);
            this.lvforecastParamsWiz2.SelectedIndexChanged += new System.EventHandler(this.lvforecastParamsWiz2_SelectedIndexChanged);
            this.lvforecastParamsWiz2.Click += new System.EventHandler(this.lvforecastParamsWiz2_Click);
            // 
            // c1
            // 
            this.c1.Text = "No";
            this.c1.Width = 30;
            // 
            // c7
            // 
            this.c7.DisplayIndex = 6;
            this.c7.Text = "Active";
            this.c7.Width = 85;
            // 
            // c6
            // 
            this.c6.DisplayIndex = 1;
            this.c6.Text = "Method";
            this.c6.Width = 127;
            // 
            // c2
            // 
            this.c2.DisplayIndex = 2;
            this.c2.Text = "Data Type";
            this.c2.Width = 100;
            // 
            // c3
            // 
            this.c3.DisplayIndex = 3;
            this.c3.Text = "Variable Name";
            this.c3.Width = 148;
            // 
            // c4
            // 
            this.c4.DisplayIndex = 4;
            this.c4.Text = "Use on";
            this.c4.Width = 61;
            // 
            // c5
            // 
            this.c5.DisplayIndex = 5;
            this.c5.Text = "Formula";
            this.c5.Width = 115;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.cobforecastmethodWiz2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1080, 36);
            this.panel4.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Forecast Method";
            // 
            // cobforecastmethodWiz2
            // 
            this.cobforecastmethodWiz2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobforecastmethodWiz2.FormattingEnabled = true;
            this.cobforecastmethodWiz2.Location = new System.Drawing.Point(113, 7);
            this.cobforecastmethodWiz2.Margin = new System.Windows.Forms.Padding(2);
            this.cobforecastmethodWiz2.Name = "cobforecastmethodWiz2";
            this.cobforecastmethodWiz2.Size = new System.Drawing.Size(617, 21);
            this.cobforecastmethodWiz2.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkActive);
            this.panel2.Controls.Add(this.chkprimaryoutput);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.btnAddWiz2);
            this.panel2.Controls.Add(this.btnaddformulaWiz2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.rdbAggWiz2);
            this.panel2.Controls.Add(this.rdbeachWiz2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cobDatatypeWiz2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtVariableNameWiz2);
            this.panel2.Location = new System.Drawing.Point(3, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1080, 263);
            this.panel2.TabIndex = 2;
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(113, 232);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(67, 17);
            this.chkActive.TabIndex = 23;
            this.chkActive.Text = "Is Active";
            this.chkActive.UseVisualStyleBackColor = true;
            // 
            // chkprimaryoutput
            // 
            this.chkprimaryoutput.AutoSize = true;
            this.chkprimaryoutput.Location = new System.Drawing.Point(611, 37);
            this.chkprimaryoutput.Name = "chkprimaryoutput";
            this.chkprimaryoutput.Size = new System.Drawing.Size(106, 17);
            this.chkprimaryoutput.TabIndex = 22;
            this.chkprimaryoutput.Text = "Is Primary Output";
            this.chkprimaryoutput.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.rdbPosWiz2);
            this.panel15.Controls.Add(this.rdbNegWiz2);
            this.panel15.Location = new System.Drawing.Point(79, 172);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(147, 53);
            this.panel15.TabIndex = 21;
            // 
            // rdbPosWiz2
            // 
            this.rdbPosWiz2.AutoSize = true;
            this.rdbPosWiz2.Checked = true;
            this.rdbPosWiz2.Location = new System.Drawing.Point(34, 3);
            this.rdbPosWiz2.Name = "rdbPosWiz2";
            this.rdbPosWiz2.Size = new System.Drawing.Size(69, 17);
            this.rdbPosWiz2.TabIndex = 18;
            this.rdbPosWiz2.TabStop = true;
            this.rdbPosWiz2.Text = "Positively";
            this.rdbPosWiz2.UseVisualStyleBackColor = true;
            // 
            // rdbNegWiz2
            // 
            this.rdbNegWiz2.AutoSize = true;
            this.rdbNegWiz2.Location = new System.Drawing.Point(34, 28);
            this.rdbNegWiz2.Name = "rdbNegWiz2";
            this.rdbNegWiz2.Size = new System.Drawing.Size(75, 17);
            this.rdbNegWiz2.TabIndex = 19;
            this.rdbNegWiz2.Text = "Negatively";
            this.rdbNegWiz2.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(9, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(721, 19);
            this.label9.TabIndex = 17;
            this.label9.Text = "Affect Primary Output";
            // 
            // btnAddWiz2
            // 
            this.btnAddWiz2.Location = new System.Drawing.Point(650, 232);
            this.btnAddWiz2.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz2.Name = "btnAddWiz2";
            this.btnAddWiz2.Size = new System.Drawing.Size(80, 23);
            this.btnAddWiz2.TabIndex = 15;
            this.btnAddWiz2.Text = "Add";
            this.btnAddWiz2.UseVisualStyleBackColor = true;
            this.btnAddWiz2.Click += new System.EventHandler(this.btnAddWiz2_Click);
            // 
            // btnaddformulaWiz2
            // 
            this.btnaddformulaWiz2.Location = new System.Drawing.Point(492, 65);
            this.btnaddformulaWiz2.Name = "btnaddformulaWiz2";
            this.btnaddformulaWiz2.Size = new System.Drawing.Size(102, 23);
            this.btnaddformulaWiz2.TabIndex = 14;
            this.btnaddformulaWiz2.Text = "Define Formula";
            this.btnaddformulaWiz2.UseVisualStyleBackColor = true;
            this.btnaddformulaWiz2.Click += new System.EventHandler(this.btnaddformulaWiz2_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(9, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(721, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Variables";
            // 
            // rdbAggWiz2
            // 
            this.rdbAggWiz2.AutoSize = true;
            this.rdbAggWiz2.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggWiz2.Location = new System.Drawing.Point(113, 122);
            this.rdbAggWiz2.Name = "rdbAggWiz2";
            this.rdbAggWiz2.Size = new System.Drawing.Size(179, 17);
            this.rdbAggWiz2.TabIndex = 11;
            this.rdbAggWiz2.Text = "Use on Aggregate Site/Category";
            this.rdbAggWiz2.UseVisualStyleBackColor = true;
            // 
            // rdbeachWiz2
            // 
            this.rdbeachWiz2.AutoSize = true;
            this.rdbeachWiz2.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachWiz2.Checked = true;
            this.rdbeachWiz2.Location = new System.Drawing.Point(113, 99);
            this.rdbeachWiz2.Name = "rdbeachWiz2";
            this.rdbeachWiz2.Size = new System.Drawing.Size(155, 17);
            this.rdbeachWiz2.TabIndex = 10;
            this.rdbeachWiz2.TabStop = true;
            this.rdbeachWiz2.Text = "Use on Each Site/Category";
            this.rdbeachWiz2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Variable Name";
            // 
            // cobDatatypeWiz2
            // 
            this.cobDatatypeWiz2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobDatatypeWiz2.FormattingEnabled = true;
            this.cobDatatypeWiz2.Location = new System.Drawing.Point(113, 65);
            this.cobDatatypeWiz2.Name = "cobDatatypeWiz2";
            this.cobDatatypeWiz2.Size = new System.Drawing.Size(373, 21);
            this.cobDatatypeWiz2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Data Type";
            // 
            // txtVariableNameWiz2
            // 
            this.txtVariableNameWiz2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVariableNameWiz2.Location = new System.Drawing.Point(113, 34);
            this.txtVariableNameWiz2.Name = "txtVariableNameWiz2";
            this.txtVariableNameWiz2.Size = new System.Drawing.Size(481, 20);
            this.txtVariableNameWiz2.TabIndex = 3;
            // 
            // tabDefprogram
            // 
            this.tabDefprogram.Controls.Add(this.tableLayoutPanel1);
            this.tabDefprogram.Location = new System.Drawing.Point(4, 25);
            this.tabDefprogram.Name = "tabDefprogram";
            this.tabDefprogram.Padding = new System.Windows.Forms.Padding(3);
            this.tabDefprogram.Size = new System.Drawing.Size(1092, 579);
            this.tabDefprogram.TabIndex = 0;
            this.tabDefprogram.Tag = "Define Program";
            this.tabDefprogram.Text = "Define Program";
            this.tabDefprogram.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1086, 573);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnProgramAdd);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.txtProgramName);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1080, 81);
            this.panel11.TabIndex = 0;
            // 
            // btnProgramAdd
            // 
            this.btnProgramAdd.Location = new System.Drawing.Point(650, 54);
            this.btnProgramAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnProgramAdd.Name = "btnProgramAdd";
            this.btnProgramAdd.Size = new System.Drawing.Size(80, 21);
            this.btnProgramAdd.TabIndex = 4;
            this.btnProgramAdd.Text = "Add";
            this.btnProgramAdd.UseVisualStyleBackColor = true;
            this.btnProgramAdd.Click += new System.EventHandler(this.btnProgramAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Program Name";
            // 
            // txtProgramName
            // 
            this.txtProgramName.Location = new System.Drawing.Point(102, 10);
            this.txtProgramName.Margin = new System.Windows.Forms.Padding(1);
            this.txtProgramName.Name = "txtProgramName";
            this.txtProgramName.Size = new System.Drawing.Size(628, 20);
            this.txtProgramName.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.lvProgramList);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(3, 90);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1080, 480);
            this.panel12.TabIndex = 1;
            // 
            // lvProgramList
            // 
            this.lvProgramList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvProgramList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colprogramname});
            this.lvProgramList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvProgramList.FullRowSelect = true;
            this.lvProgramList.GridLines = true;
            this.lvProgramList.Location = new System.Drawing.Point(0, 0);
            this.lvProgramList.Name = "lvProgramList";
            this.lvProgramList.OwnerDraw = true;
            this.lvProgramList.Size = new System.Drawing.Size(1080, 480);
            this.lvProgramList.TabIndex = 34;
            this.lvProgramList.UseCompatibleStateImageBehavior = false;
            this.lvProgramList.View = System.Windows.Forms.View.Details;
            this.lvProgramList.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvProgramList_DrawColumnHeader);
            this.lvProgramList.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvProgramList_DrawItem);
            this.lvProgramList.SelectedIndexChanged += new System.EventHandler(this.lvProgramList_SelectedIndexChanged);
            // 
            // colNo
            // 
            this.colNo.Text = "No";
            this.colNo.Width = 29;
            // 
            // colprogramname
            // 
            this.colprogramname.Text = "Program Name";
            this.colprogramname.Width = 686;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabDefprogram);
            this.tabControl1.Controls.Add(this.tabForecastMethod);
            this.tabControl1.Controls.Add(this.tabPatientGroup);
            this.tabControl1.Controls.Add(this.tabGeneralAssumption);
            this.tabControl1.Controls.Add(this.tabTestingProtocol);
            this.tabControl1.Controls.Add(this.tabReview);
            this.tabControl1.Location = new System.Drawing.Point(0, 45);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1100, 608);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tabPatientGroup
            // 
            this.tabPatientGroup.Controls.Add(this.tableLayoutPanel3);
            this.tabPatientGroup.Location = new System.Drawing.Point(4, 25);
            this.tabPatientGroup.Name = "tabPatientGroup";
            this.tabPatientGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPatientGroup.Size = new System.Drawing.Size(1092, 579);
            this.tabPatientGroup.TabIndex = 2;
            this.tabPatientGroup.Tag = "Patient/Population Group";
            this.tabPatientGroup.Text = "Patient/Population Group";
            this.tabPatientGroup.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1086, 573);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.chkActiveGroup);
            this.panel5.Controls.Add(this.btnAddWiz3);
            this.panel5.Controls.Add(this.txtgroupnameWiz3);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1080, 65);
            this.panel5.TabIndex = 0;
            // 
            // chkActiveGroup
            // 
            this.chkActiveGroup.AutoSize = true;
            this.chkActiveGroup.Checked = true;
            this.chkActiveGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActiveGroup.Location = new System.Drawing.Point(14, 39);
            this.chkActiveGroup.Name = "chkActiveGroup";
            this.chkActiveGroup.Size = new System.Drawing.Size(67, 17);
            this.chkActiveGroup.TabIndex = 24;
            this.chkActiveGroup.Text = "Is Active";
            this.chkActiveGroup.UseVisualStyleBackColor = true;
            // 
            // btnAddWiz3
            // 
            this.btnAddWiz3.Location = new System.Drawing.Point(650, 39);
            this.btnAddWiz3.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz3.Name = "btnAddWiz3";
            this.btnAddWiz3.Size = new System.Drawing.Size(80, 21);
            this.btnAddWiz3.TabIndex = 4;
            this.btnAddWiz3.Text = "Add";
            this.btnAddWiz3.UseVisualStyleBackColor = true;
            this.btnAddWiz3.Click += new System.EventHandler(this.btnAddWiz3_Click);
            // 
            // txtgroupnameWiz3
            // 
            this.txtgroupnameWiz3.Location = new System.Drawing.Point(102, 10);
            this.txtgroupnameWiz3.Margin = new System.Windows.Forms.Padding(1);
            this.txtgroupnameWiz3.Name = "txtgroupnameWiz3";
            this.txtgroupnameWiz3.Size = new System.Drawing.Size(628, 20);
            this.txtgroupnameWiz3.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Group Name";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lvGroupWiz3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 74);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1080, 496);
            this.panel6.TabIndex = 1;
            // 
            // lvGroupWiz3
            // 
            this.lvGroupWiz3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvGroupWiz3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader6});
            this.lvGroupWiz3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvGroupWiz3.GridLines = true;
            this.lvGroupWiz3.Location = new System.Drawing.Point(0, 0);
            this.lvGroupWiz3.Name = "lvGroupWiz3";
            this.lvGroupWiz3.OwnerDraw = true;
            this.lvGroupWiz3.Size = new System.Drawing.Size(1080, 496);
            this.lvGroupWiz3.TabIndex = 34;
            this.lvGroupWiz3.UseCompatibleStateImageBehavior = false;
            this.lvGroupWiz3.View = System.Windows.Forms.View.Details;
            this.lvGroupWiz3.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvGroupWiz3_DrawColumnHeader);
            this.lvGroupWiz3.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvGroupWiz3_DrawItem);
            this.lvGroupWiz3.SelectedIndexChanged += new System.EventHandler(this.lvGroupWiz3_SelectedIndexChanged);
            this.lvGroupWiz3.Click += new System.EventHandler(this.lvGroupWiz3_Click);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "No";
            this.columnHeader5.Width = 29;
            // 
            // columnHeader7
            // 
            this.columnHeader7.DisplayIndex = 2;
            this.columnHeader7.Text = "Active";
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 1;
            this.columnHeader6.Text = "Group Name";
            this.columnHeader6.Width = 681;
            // 
            // tabGeneralAssumption
            // 
            this.tabGeneralAssumption.Controls.Add(this.tableLayoutPanel4);
            this.tabGeneralAssumption.Location = new System.Drawing.Point(4, 25);
            this.tabGeneralAssumption.Name = "tabGeneralAssumption";
            this.tabGeneralAssumption.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneralAssumption.Size = new System.Drawing.Size(1092, 579);
            this.tabGeneralAssumption.TabIndex = 3;
            this.tabGeneralAssumption.Tag = "General Assumptions";
            this.tabGeneralAssumption.Text = "General Assumptions";
            this.tabGeneralAssumption.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 269F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1086, 573);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.chkGeneralAssumption);
            this.panel8.Controls.Add(this.panel17);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.cobAssumptionTypeWiz4);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.btnAddWiz4);
            this.panel8.Controls.Add(this.btnaddformulaWiz4);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.rdbAggWiz4);
            this.panel8.Controls.Add(this.txtVariableNameWiz4);
            this.panel8.Controls.Add(this.rdbeachWiz4);
            this.panel8.Controls.Add(this.cobDatatypeWiz4);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1080, 263);
            this.panel8.TabIndex = 0;
            // 
            // chkGeneralAssumption
            // 
            this.chkGeneralAssumption.AutoSize = true;
            this.chkGeneralAssumption.Checked = true;
            this.chkGeneralAssumption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGeneralAssumption.Location = new System.Drawing.Point(116, 238);
            this.chkGeneralAssumption.Name = "chkGeneralAssumption";
            this.chkGeneralAssumption.Size = new System.Drawing.Size(67, 17);
            this.chkGeneralAssumption.TabIndex = 25;
            this.chkGeneralAssumption.Text = "Is Active";
            this.chkGeneralAssumption.UseVisualStyleBackColor = true;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.rdbPosWiz4);
            this.panel17.Controls.Add(this.rdbNegWiz4);
            this.panel17.Location = new System.Drawing.Point(38, 175);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(346, 52);
            this.panel17.TabIndex = 24;
            // 
            // rdbPosWiz4
            // 
            this.rdbPosWiz4.AutoSize = true;
            this.rdbPosWiz4.Checked = true;
            this.rdbPosWiz4.Location = new System.Drawing.Point(78, 3);
            this.rdbPosWiz4.Name = "rdbPosWiz4";
            this.rdbPosWiz4.Size = new System.Drawing.Size(62, 17);
            this.rdbPosWiz4.TabIndex = 21;
            this.rdbPosWiz4.TabStop = true;
            this.rdbPosWiz4.Text = "Positive";
            this.rdbPosWiz4.UseVisualStyleBackColor = true;
            // 
            // rdbNegWiz4
            // 
            this.rdbNegWiz4.AutoSize = true;
            this.rdbNegWiz4.Location = new System.Drawing.Point(78, 28);
            this.rdbNegWiz4.Name = "rdbNegWiz4";
            this.rdbNegWiz4.Size = new System.Drawing.Size(68, 17);
            this.rdbNegWiz4.TabIndex = 22;
            this.rdbNegWiz4.Text = "Negative";
            this.rdbNegWiz4.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(12, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(721, 19);
            this.label10.TabIndex = 20;
            this.label10.Text = "Variable Effect";
            // 
            // cobAssumptionTypeWiz4
            // 
            this.cobAssumptionTypeWiz4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobAssumptionTypeWiz4.FormattingEnabled = true;
            this.cobAssumptionTypeWiz4.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cobAssumptionTypeWiz4.Location = new System.Drawing.Point(116, 11);
            this.cobAssumptionTypeWiz4.Name = "cobAssumptionTypeWiz4";
            this.cobAssumptionTypeWiz4.Size = new System.Drawing.Size(614, 21);
            this.cobAssumptionTypeWiz4.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Assumption Type";
            // 
            // btnAddWiz4
            // 
            this.btnAddWiz4.Location = new System.Drawing.Point(649, 234);
            this.btnAddWiz4.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz4.Name = "btnAddWiz4";
            this.btnAddWiz4.Size = new System.Drawing.Size(80, 23);
            this.btnAddWiz4.TabIndex = 16;
            this.btnAddWiz4.Text = "Add";
            this.btnAddWiz4.UseVisualStyleBackColor = true;
            this.btnAddWiz4.Click += new System.EventHandler(this.btnAddWiz4_Click);
            // 
            // btnaddformulaWiz4
            // 
            this.btnaddformulaWiz4.Location = new System.Drawing.Point(627, 73);
            this.btnaddformulaWiz4.Name = "btnaddformulaWiz4";
            this.btnaddformulaWiz4.Size = new System.Drawing.Size(102, 23);
            this.btnaddformulaWiz4.TabIndex = 14;
            this.btnaddformulaWiz4.Text = "Define Formula";
            this.btnaddformulaWiz4.UseVisualStyleBackColor = true;
            this.btnaddformulaWiz4.Click += new System.EventHandler(this.btnaddformulaWiz4_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Variable Name";
            // 
            // rdbAggWiz4
            // 
            this.rdbAggWiz4.AutoSize = true;
            this.rdbAggWiz4.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggWiz4.Location = new System.Drawing.Point(116, 129);
            this.rdbAggWiz4.Name = "rdbAggWiz4";
            this.rdbAggWiz4.Size = new System.Drawing.Size(179, 17);
            this.rdbAggWiz4.TabIndex = 11;
            this.rdbAggWiz4.Text = "Use on Aggregate Site/Category";
            this.rdbAggWiz4.UseVisualStyleBackColor = true;
            // 
            // txtVariableNameWiz4
            // 
            this.txtVariableNameWiz4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVariableNameWiz4.Location = new System.Drawing.Point(116, 42);
            this.txtVariableNameWiz4.Name = "txtVariableNameWiz4";
            this.txtVariableNameWiz4.Size = new System.Drawing.Size(614, 20);
            this.txtVariableNameWiz4.TabIndex = 3;
            // 
            // rdbeachWiz4
            // 
            this.rdbeachWiz4.AutoSize = true;
            this.rdbeachWiz4.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachWiz4.Checked = true;
            this.rdbeachWiz4.Location = new System.Drawing.Point(116, 105);
            this.rdbeachWiz4.Name = "rdbeachWiz4";
            this.rdbeachWiz4.Size = new System.Drawing.Size(155, 17);
            this.rdbeachWiz4.TabIndex = 10;
            this.rdbeachWiz4.TabStop = true;
            this.rdbeachWiz4.Text = "Use on Each Site/Category";
            this.rdbeachWiz4.UseVisualStyleBackColor = true;
            // 
            // cobDatatypeWiz4
            // 
            this.cobDatatypeWiz4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobDatatypeWiz4.FormattingEnabled = true;
            this.cobDatatypeWiz4.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cobDatatypeWiz4.Location = new System.Drawing.Point(116, 73);
            this.cobDatatypeWiz4.Name = "cobDatatypeWiz4";
            this.cobDatatypeWiz4.Size = new System.Drawing.Size(505, 21);
            this.cobDatatypeWiz4.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Data Type";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 272);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(1080, 298);
            this.panel9.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.lvgeneralassumptions);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1080, 298);
            this.panel13.TabIndex = 0;
            // 
            // lvgeneralassumptions
            // 
            this.lvgeneralassumptions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvgeneralassumptions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cc1,
            this.cc7,
            this.cc6,
            this.cc2,
            this.cc3,
            this.cc4,
            this.cc5});
            this.lvgeneralassumptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvgeneralassumptions.FullRowSelect = true;
            this.lvgeneralassumptions.GridLines = true;
            this.lvgeneralassumptions.Location = new System.Drawing.Point(14, 16);
            this.lvgeneralassumptions.Name = "lvgeneralassumptions";
            this.lvgeneralassumptions.OwnerDraw = true;
            this.lvgeneralassumptions.Size = new System.Drawing.Size(785, 279);
            this.lvgeneralassumptions.TabIndex = 1;
            this.lvgeneralassumptions.UseCompatibleStateImageBehavior = false;
            this.lvgeneralassumptions.View = System.Windows.Forms.View.Details;
            this.lvgeneralassumptions.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvgeneralassumptions_DrawColumnHeader);
            this.lvgeneralassumptions.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvgeneralassumptions_DrawItem);
            this.lvgeneralassumptions.SelectedIndexChanged += new System.EventHandler(this.lvgeneralassumptions_SelectedIndexChanged);
            this.lvgeneralassumptions.Click += new System.EventHandler(this.lvgeneralassumptions_Click);
            // 
            // cc1
            // 
            this.cc1.Text = "No";
            this.cc1.Width = 33;
            // 
            // cc7
            // 
            this.cc7.DisplayIndex = 6;
            this.cc7.Text = "Active";
            // 
            // cc6
            // 
            this.cc6.DisplayIndex = 1;
            this.cc6.Text = "Type";
            // 
            // cc2
            // 
            this.cc2.DisplayIndex = 2;
            this.cc2.Text = "Data Type";
            this.cc2.Width = 76;
            // 
            // cc3
            // 
            this.cc3.DisplayIndex = 3;
            this.cc3.Text = "Variable Name";
            this.cc3.Width = 328;
            // 
            // cc4
            // 
            this.cc4.DisplayIndex = 4;
            this.cc4.Text = "Use on ";
            this.cc4.Width = 102;
            // 
            // cc5
            // 
            this.cc5.DisplayIndex = 5;
            this.cc5.Text = "Formula";
            this.cc5.Width = 124;
            // 
            // tabTestingProtocol
            // 
            this.tabTestingProtocol.Controls.Add(this.panel14);
            this.tabTestingProtocol.Location = new System.Drawing.Point(4, 25);
            this.tabTestingProtocol.Name = "tabTestingProtocol";
            this.tabTestingProtocol.Padding = new System.Windows.Forms.Padding(3);
            this.tabTestingProtocol.Size = new System.Drawing.Size(1092, 579);
            this.tabTestingProtocol.TabIndex = 4;
            this.tabTestingProtocol.Tag = "Testing Protocol Options";
            this.tabTestingProtocol.Text = "Testing Protocol Options";
            this.tabTestingProtocol.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label4);
            this.panel14.Controls.Add(this.chkrapidprotocol);
            this.panel14.Controls.Add(this.chkgeneralprotocol);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(3, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(1086, 573);
            this.panel14.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(14, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(721, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "How many year(s(does this testing protocol cycle has? 1/2";
            // 
            // chkrapidprotocol
            // 
            this.chkrapidprotocol.AutoSize = true;
            this.chkrapidprotocol.Location = new System.Drawing.Point(15, 60);
            this.chkrapidprotocol.Name = "chkrapidprotocol";
            this.chkrapidprotocol.Size = new System.Drawing.Size(134, 17);
            this.chkrapidprotocol.TabIndex = 2;
            this.chkrapidprotocol.Text = "Rapid Testing Protocol";
            this.chkrapidprotocol.UseVisualStyleBackColor = true;
            this.chkrapidprotocol.CheckedChanged += new System.EventHandler(this.chkrapidprotocol_CheckedChanged);
            // 
            // chkgeneralprotocol
            // 
            this.chkgeneralprotocol.AutoSize = true;
            this.chkgeneralprotocol.Location = new System.Drawing.Point(15, 34);
            this.chkgeneralprotocol.Name = "chkgeneralprotocol";
            this.chkgeneralprotocol.Size = new System.Drawing.Size(143, 17);
            this.chkgeneralprotocol.TabIndex = 1;
            this.chkgeneralprotocol.Text = "General Testing Protocol";
            this.chkgeneralprotocol.UseVisualStyleBackColor = true;
            this.chkgeneralprotocol.CheckedChanged += new System.EventHandler(this.chkgeneralprotocol_CheckedChanged);
            // 
            // tabReview
            // 
            this.tabReview.Controls.Add(this.lbltestingprotocol1);
            this.tabReview.Controls.Add(this.label18);
            this.tabReview.Controls.Add(this.lstReviewGeneralAssumption);
            this.tabReview.Controls.Add(this.label17);
            this.tabReview.Controls.Add(this.lstReviewPatientGroup);
            this.tabReview.Controls.Add(this.label16);
            this.tabReview.Controls.Add(this.lstReviewForecastingVariable);
            this.tabReview.Controls.Add(this.label13);
            this.tabReview.Controls.Add(this.lblprogram1);
            this.tabReview.Controls.Add(this.lblProgram);
            this.tabReview.Location = new System.Drawing.Point(4, 25);
            this.tabReview.Name = "tabReview";
            this.tabReview.Padding = new System.Windows.Forms.Padding(3);
            this.tabReview.Size = new System.Drawing.Size(1092, 579);
            this.tabReview.TabIndex = 5;
            this.tabReview.Tag = "Review";
            this.tabReview.Text = "Review";
            this.tabReview.UseVisualStyleBackColor = true;
            // 
            // lbltestingprotocol1
            // 
            this.lbltestingprotocol1.AutoSize = true;
            this.lbltestingprotocol1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltestingprotocol1.Location = new System.Drawing.Point(179, 544);
            this.lbltestingprotocol1.Name = "lbltestingprotocol1";
            this.lbltestingprotocol1.Size = new System.Drawing.Size(111, 13);
            this.lbltestingprotocol1.TabIndex = 43;
            this.lbltestingprotocol1.Text = "Testing Protocol Type";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(19, 544);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Testing Protocol Type";
            // 
            // lstReviewGeneralAssumption
            // 
            this.lstReviewGeneralAssumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewGeneralAssumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.lstReviewGeneralAssumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReviewGeneralAssumption.FullRowSelect = true;
            this.lstReviewGeneralAssumption.GridLines = true;
            this.lstReviewGeneralAssumption.Location = new System.Drawing.Point(18, 391);
            this.lstReviewGeneralAssumption.Name = "lstReviewGeneralAssumption";
            this.lstReviewGeneralAssumption.OwnerDraw = true;
            this.lstReviewGeneralAssumption.Size = new System.Drawing.Size(1071, 138);
            this.lstReviewGeneralAssumption.TabIndex = 41;
            this.lstReviewGeneralAssumption.UseCompatibleStateImageBehavior = false;
            this.lstReviewGeneralAssumption.View = System.Windows.Forms.View.Details;
            this.lstReviewGeneralAssumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewGeneralAssumption_DrawColumnHeader);
            this.lstReviewGeneralAssumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewGeneralAssumption_DrawItem);
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "No";
            this.columnHeader14.Width = 33;
            // 
            // columnHeader15
            // 
            this.columnHeader15.DisplayIndex = 6;
            this.columnHeader15.Text = "Active";
            this.columnHeader15.Width = 68;
            // 
            // columnHeader16
            // 
            this.columnHeader16.DisplayIndex = 1;
            this.columnHeader16.Text = "Type";
            // 
            // columnHeader17
            // 
            this.columnHeader17.DisplayIndex = 2;
            this.columnHeader17.Text = "Data Type";
            this.columnHeader17.Width = 76;
            // 
            // columnHeader18
            // 
            this.columnHeader18.DisplayIndex = 3;
            this.columnHeader18.Text = "Variable Name";
            this.columnHeader18.Width = 328;
            // 
            // columnHeader19
            // 
            this.columnHeader19.DisplayIndex = 4;
            this.columnHeader19.Text = "Use on ";
            this.columnHeader19.Width = 102;
            // 
            // columnHeader20
            // 
            this.columnHeader20.DisplayIndex = 5;
            this.columnHeader20.Text = "Formula";
            this.columnHeader20.Width = 124;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.SteelBlue;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(17, 364);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(1069, 24);
            this.label17.TabIndex = 40;
            this.label17.Text = "General Assumptions";
            // 
            // lstReviewPatientGroup
            // 
            this.lstReviewPatientGroup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewPatientGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.lstReviewPatientGroup.GridLines = true;
            this.lstReviewPatientGroup.Location = new System.Drawing.Point(18, 207);
            this.lstReviewPatientGroup.Name = "lstReviewPatientGroup";
            this.lstReviewPatientGroup.OwnerDraw = true;
            this.lstReviewPatientGroup.Size = new System.Drawing.Size(1071, 147);
            this.lstReviewPatientGroup.TabIndex = 39;
            this.lstReviewPatientGroup.UseCompatibleStateImageBehavior = false;
            this.lstReviewPatientGroup.View = System.Windows.Forms.View.Details;
            this.lstReviewPatientGroup.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewPatientGroup_DrawColumnHeader);
            this.lstReviewPatientGroup.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewPatientGroup_DrawItem);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "No";
            this.columnHeader11.Width = 29;
            // 
            // columnHeader12
            // 
            this.columnHeader12.DisplayIndex = 2;
            this.columnHeader12.Text = "Active";
            this.columnHeader12.Width = 82;
            // 
            // columnHeader13
            // 
            this.columnHeader13.DisplayIndex = 1;
            this.columnHeader13.Text = "Group Name";
            this.columnHeader13.Width = 681;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.SteelBlue;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(17, 179);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(1069, 25);
            this.label16.TabIndex = 38;
            this.label16.Text = "Patient/Population Group";
            // 
            // lstReviewForecastingVariable
            // 
            this.lstReviewForecastingVariable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewForecastingVariable.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.lstReviewForecastingVariable.FullRowSelect = true;
            this.lstReviewForecastingVariable.GridLines = true;
            this.lstReviewForecastingVariable.Location = new System.Drawing.Point(18, 68);
            this.lstReviewForecastingVariable.Name = "lstReviewForecastingVariable";
            this.lstReviewForecastingVariable.OwnerDraw = true;
            this.lstReviewForecastingVariable.Size = new System.Drawing.Size(1071, 103);
            this.lstReviewForecastingVariable.TabIndex = 37;
            this.lstReviewForecastingVariable.UseCompatibleStateImageBehavior = false;
            this.lstReviewForecastingVariable.View = System.Windows.Forms.View.Details;
            this.lstReviewForecastingVariable.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewForecastingVariable_DrawColumnHeader);
            this.lstReviewForecastingVariable.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewForecastingVariable_DrawItem);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No";
            this.columnHeader1.Width = 30;
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 6;
            this.columnHeader2.Text = "Active";
            this.columnHeader2.Width = 85;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 1;
            this.columnHeader3.Text = "Method";
            this.columnHeader3.Width = 116;
            // 
            // columnHeader4
            // 
            this.columnHeader4.DisplayIndex = 2;
            this.columnHeader4.Text = "Data Type";
            this.columnHeader4.Width = 86;
            // 
            // columnHeader8
            // 
            this.columnHeader8.DisplayIndex = 3;
            this.columnHeader8.Text = "Variable Name";
            this.columnHeader8.Width = 252;
            // 
            // columnHeader9
            // 
            this.columnHeader9.DisplayIndex = 4;
            this.columnHeader9.Text = "Use on";
            this.columnHeader9.Width = 61;
            // 
            // columnHeader10
            // 
            this.columnHeader10.DisplayIndex = 5;
            this.columnHeader10.Text = "Formula";
            this.columnHeader10.Width = 165;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.SteelBlue;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(17, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(1069, 19);
            this.label13.TabIndex = 18;
            this.label13.Text = "Forcasting Method Variable";
            // 
            // lblprogram1
            // 
            this.lblprogram1.AutoSize = true;
            this.lblprogram1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprogram1.Location = new System.Drawing.Point(132, 16);
            this.lblprogram1.Name = "lblprogram1";
            this.lblprogram1.Size = new System.Drawing.Size(77, 13);
            this.lblprogram1.TabIndex = 1;
            this.lblprogram1.Text = "Program Name";
            // 
            // lblProgram
            // 
            this.lblProgram.AutoSize = true;
            this.lblProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgram.Location = new System.Drawing.Point(19, 16);
            this.lblProgram.Name = "lblProgram";
            this.lblProgram.Size = new System.Drawing.Size(89, 13);
            this.lblProgram.TabIndex = 0;
            this.lblProgram.Text = "Program Name";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(4, 659);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1096, 34);
            this.panel1.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1015, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(71, 28);
            this.button3.TabIndex = 2;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(861, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Back";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(938, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Next";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel3.Controls.Add(this.label11);
            this.panel3.Location = new System.Drawing.Point(0, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1179, 37);
            this.panel3.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(7, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(328, 21);
            this.label11.TabIndex = 0;
            this.label11.Text = "Demographic Forecasting Method Parameters";
            // 
            // FrmMorbidityParameter_new
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1112, 705);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmMorbidityParameter_new";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Morbidity Parameters";
            this.Load += new System.EventHandler(this.FrmMorbidityParameter_new_Load);
            this.tabForecastMethod.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tabDefprogram.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPatientGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tabGeneralAssumption.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.tabTestingProtocol.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.tabReview.ResumeLayout(false);
            this.tabReview.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabForecastMethod;
        private System.Windows.Forms.TabPage tabDefprogram;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPatientGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ListView lvforecastParamsWiz2;
        private System.Windows.Forms.ColumnHeader c1;
        private System.Windows.Forms.ColumnHeader c6;
        private System.Windows.Forms.ColumnHeader c2;
        private System.Windows.Forms.ColumnHeader c3;
        private System.Windows.Forms.ColumnHeader c4;
        private System.Windows.Forms.ColumnHeader c5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cobforecastmethodWiz2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkprimaryoutput;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RadioButton rdbPosWiz2;
        private System.Windows.Forms.RadioButton rdbNegWiz2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnAddWiz2;
        private System.Windows.Forms.Button btnaddformulaWiz2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbAggWiz2;
        private System.Windows.Forms.RadioButton rdbeachWiz2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cobDatatypeWiz2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVariableNameWiz2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnProgramAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProgramName;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ListView lvProgramList;
        private System.Windows.Forms.ColumnHeader colNo;
        private System.Windows.Forms.ColumnHeader colprogramname;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnAddWiz3;
        private System.Windows.Forms.TextBox txtgroupnameWiz3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ListView lvGroupWiz3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TabPage tabGeneralAssumption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RadioButton rdbPosWiz4;
        private System.Windows.Forms.RadioButton rdbNegWiz4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cobAssumptionTypeWiz4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnAddWiz4;
        private System.Windows.Forms.Button btnaddformulaWiz4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rdbAggWiz4;
        private System.Windows.Forms.TextBox txtVariableNameWiz4;
        private System.Windows.Forms.RadioButton rdbeachWiz4;
        private System.Windows.Forms.ComboBox cobDatatypeWiz4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.ListView lvgeneralassumptions;
        private System.Windows.Forms.ColumnHeader cc1;
        private System.Windows.Forms.ColumnHeader cc6;
        private System.Windows.Forms.ColumnHeader cc2;
        private System.Windows.Forms.ColumnHeader cc3;
        private System.Windows.Forms.ColumnHeader cc4;
        private System.Windows.Forms.ColumnHeader cc5;
        private System.Windows.Forms.TabPage tabTestingProtocol;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkrapidprotocol;
        private System.Windows.Forms.CheckBox chkgeneralprotocol;
        private System.Windows.Forms.TabPage tabReview;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.ColumnHeader c7;
        private System.Windows.Forms.CheckBox chkActiveGroup;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.CheckBox chkGeneralAssumption;
        private System.Windows.Forms.ColumnHeader cc7;
        private System.Windows.Forms.Label lbltestingprotocol1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView lstReviewGeneralAssumption;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListView lstReviewPatientGroup;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ListView lstReviewForecastingVariable;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblprogram1;
        private System.Windows.Forms.Label lblProgram;

    }
}