﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;
using LQT.GUI.Location;
using LQT.Core.UserExceptions;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewCheckupForm : NewBaseMorbidityControl
    {
        private MorbidityForecast _forecast;
        private IList<ARTSite> _artSites;
        private InventoryAssumption _invAssumption;
        private Form _mdiparent;
        private SiteListView _sPlatformView;
        private SiteListView _pBehaviorView;
        private SiteListView _supplyListView;
        private double _maxAttrition = 45;
        private double _minMigration = 5;
        private Color _colorMaxAttrition = Color.Yellow;
        private Color _colorMinMigration = Color.Red;

        public NewCheckupForm()
        {
            InitializeComponent();
            
        }

        public override string Title
        {
            get { return "Review your data & Do Forecast!"; }
        }

        public override MorbidityCtrEnum PriviousCtr
        {
            get
            {
                return MorbidityCtrEnum.Nothing;
            }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {

                return MorbidityCtrEnum.Nothing;
            }
        }

        public override bool EnableNextButton()
        {
   
            return false;
        }

        public override string Description
        {
            get
            {
                string desc = "Now that you have finished entering all your data. <br>";
                desc += "Before you prompt the model to begin calculating, " +
                    "take this time to review your data inputs for accuracy." +
                    "Pay special attention to the data inputs that have the " +
                    "greatest impact on the final result.";

                return desc;
            }
        }


    }
}
