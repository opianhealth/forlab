﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Util;
using LQT.GUI.Quantification;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewBaseMorbidityControl : UserControl
    {
        public event EventHandler<NextButtonStatusEventArgss> NextButtonStatusChanged;
        public IList<MorbidityCustomeAssumption> _pforecast;
        ////private 
        //public BaseMorbidityControl()
        //{
        //    InitializeComponent();
        //}

        public virtual IList<MorbidityCustomeAssumption> PMorbidity
        {
            get { return _pforecast; }
        
        }

        public virtual string Title
        {
            get { return null; }
        }
        
        public virtual string Description
        {
            get { return ""; }
        }

        private NewMorbidityForm _morbidityForm;
        public NewMorbidityForm MorbidityForm
        {
            get { return _morbidityForm; }
            set { _morbidityForm = value; }
        }
        
        public virtual MorbidityCtrEnum PriviousCtr
        {
            get { return MorbidityCtrEnum.Nothing; }
        }

        public virtual MorbidityCtrEnum NextCtr
        {
            get { return MorbidityCtrEnum.Nothing; }
        }

        public virtual bool EnableNextButton()
        {
            return true;
        }

        public virtual bool DoSomthingBeforeUnload()
        {
            return true;
        }

        public virtual void OnNextButtonStatusChanged(bool status)
        {
            if (NextButtonStatusChanged != null)
            {
                NextButtonStatusChanged(this, new NextButtonStatusEventArgss(status));
            }
        }

    }

    public class NextButtonStatusEventArgss : EventArgs
    {
        private bool _boolValue;

        public NextButtonStatusEventArgss(bool value)
        {
            _boolValue = value;
        }

        public bool BoolValue
        {
            get { return _boolValue; }
        }
    }
}
