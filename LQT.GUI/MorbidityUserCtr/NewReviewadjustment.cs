﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewReviewadjustment : NewBaseMorbidityControl
    {
        private SiteListView _sListView;
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;
        private IList<NewMordidityCategorySite> _artSites;
        private IList<NewMordidityCategorySite> _deletedArtSites;
        private bool _isedited = false;

        private IList<Test> _test;

        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public NewReviewadjustment()
        {
            //this._forecast = forecast;
           
            //_sites = DataRepository.GetAllSite();
            //_regions = DataRepository.GetAllRegion();
            //_test=new List<Test>();
            InitializeComponent();
            //LoadSiteListView();
            BindTestingArea();
        }

        private void BindTestingArea()
        {
            
            cboTestingArea.DataSource = DataRepository.GetAllTestingArea();//.GetAllTests();//.GetAllProductType();



        }

        public override string Title
        {
            get { return "Review and Adjustment"; }
        }

        public override string Description
        {
            get
            {

                string desc = "<p> Review and Adjustment.</p>";

               // desc += "<p>1. Add Tests to include in the forecast. General Tests, excluding HIV Rapid Test</p><br>";
                //desc += "<p>2. For The Added Tests, Select and define Protocol<br>";


                return desc;
            }
        }
        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.NewCheckupForm;//.OptRecentData;
            }
        }

        public override bool EnableNextButton()
        {
            return true;// 1;// _artSites.Count > 0;
        }



        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.BatchSaveNewMordidityCategorySite(_artSites);
                //DataRepository.BatchDeleteNewMordidityCategorySite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }

      




    }
}
