﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Domain;
using LQT.Core.Util;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class FrmMorbidityProgramSelection : Form
    {

        public IList<MMProgram> _mMPrograms = new List<MMProgram>();

        public FrmMorbidityProgramSelection()
        {
            _mMPrograms = DataRepository.GetMMPrograms();
            InitializeComponent();
            LoadMorbidityParameters();
        }

        public void LoadMorbidityParameters()
        {
            foreach (MMProgram mProgram in _mMPrograms)
            {

                RadioButton button = new RadioButton();
                button.Tag = mProgram;
                button.Text = mProgram.ProgramName;
                button.Width=350;
                button.Height = 40;
                button.AutoSize = false;
                button.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
                flowLayoutPanel1.Controls.Add(button);
            }

        }
    }
}
