﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewCatSelection : NewBaseMorbidityControl
    {
        private SiteListView _sListView;
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;
        private IList<NewMordidityCategorySite> _artSites;
        private IList<NewMordidityCategorySite> _deletedArtSites;
        private bool _isedited = false;
        
        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public NewCatSelection(NewMorbidityForecast forecast, IList<NewMordidityCategorySite> artsites)
        {
            this._forecast = forecast;
            this._artSites = artsites;
            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
            InitializeComponent();
            LoadSiteListView();
        }
        public NewCatSelection()
        {
            InitializeComponent();
        }

        public override string Title
        {
            get { return "Categories included in quantification"; }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.OptRecentData;
            }
        }

        public override bool EnableNextButton()
        {
            return _artSites.Count > 0;
        }



        private void LoadSiteListView()
        {
            _sListView = new SiteListView();
            _sListView.MySortBrush = SystemBrushes.ControlLight;
            _sListView.MyHighlightBrush = Brushes.Goldenrod;
            _sListView.GridLines = true;
            _sListView.MultiSelect = false;
            _sListView.Dock = DockStyle.Fill;
            _sListView.ControlPadding = 4;
            _sListView.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            _sListView.Scrollable = true;
           
            //add SmallImageList to ListView - images will be shown in ColumnHeaders
            ImageList colimglst = new ImageList();
            colimglst.Images.Add("down", trueFalseImageList.Images[2]);
            colimglst.Images.Add("up", trueFalseImageList.Images[3]);
            colimglst.ColorDepth = ColorDepth.Depth32Bit;
            colimglst.ImageSize = new Size(20, 20); // this will affect the row height
            _sListView.SmallImageList = colimglst;
            
            //add columns and items
            _sListView.Columns.Add(new EXColumnHeader("Category", 100));
            _sListView.Columns.Add(new EXColumnHeader("Selected Site", 150));
            _sListView.Columns.Add(new EXColumnHeader("Site Type", 80));
            
            /*
            EXBoolColumnHeader boolcol = new EXBoolColumnHeader("VCT",trueFalseImageList.Images[0],trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("CD4", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("Chemistry", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("Hematology", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("Viral Load", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("Other Test", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);

            boolcol = new EXBoolColumnHeader("Consumable", trueFalseImageList.Images[0], trueFalseImageList.Images[1], 60);
            boolcol.Editable = true;
            _sListView.Columns.Add(boolcol);
             * */
            _sListView.BoolListViewSubItemValueChanged += new EventHandler<EXBoolListViewSubItemEventArgs>(sListView_BoolListViewSubItemValueChanged);
            _sListView.SelectedIndexChanged += new EventHandler(sListView_SelectedIndexChanged);

            panSites.Controls.Add(_sListView);
        }

        private void sListView_BoolListViewSubItemValueChanged(object sender, EXBoolListViewSubItemEventArgs e)
        {
            ARTSite site = (ARTSite)e.ListVItem.Tag;
         
            BooleanColumnName colname = (BooleanColumnName)e.Subitem.ColumnName;
            switch (colname)
            {
                case BooleanColumnName.VCT:
                    site.ForecastVCT = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.CD4:
                    site.ForecastCD4 = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.Chemistry:
                    site.ForecastChemistry = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.Hematology:
                    site.ForecastHematology = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.ViralLoad:
                    site.ForecastViralLoad = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.OtherTest:
                    site.ForecastOtherTest = e.Subitem.BoolValue;
                    break;
                case BooleanColumnName.Consumable:
                    site.ForecastConsumable = e.Subitem.BoolValue;
                    break;
            }
            _isedited = true;
        }

        private IList<NewMordidityCategorySite> GetARTSiteByCategory(int catid)
        {
            IList<NewMordidityCategorySite> result = new List<NewMordidityCategorySite>();

            foreach (NewMordidityCategorySite s in _artSites)
            {
                if (s.NewMorbidityCategory.Id == catid)
                    result.Add(s);
            }

            return result;
        }

        private void BindArtSites()
        {
            _sListView.Items.Clear();
            _sListView.BeginUpdate();
            
            if (_activeCategory != null)
            {
                foreach (NewMordidityCategorySite site in GetARTSiteByCategory( _activeCategory.Id))
                {
                    EXListViewItem item = new EXListViewItem(site.NewMorbidityCategory.CategoryName) { Tag = site };

                    item.SubItems.Add(new EXListViewSubItem(site.Site.SiteName));
                    item.SubItems.Add(new EXListViewSubItem(site.Site.SiteCategory != null ? site.Site.SiteCategory.CategoryName : ""));

                   /* item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastVCT, BooleanColumnName.VCT));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastCD4, BooleanColumnName.CD4));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastChemistry, BooleanColumnName.Chemistry));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastHematology, BooleanColumnName.Hematology));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastViralLoad, BooleanColumnName.ViralLoad));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastOtherTest, BooleanColumnName.OtherTest));
                    item.SubItems.Add(new EXBoolListViewSubItem(site.ForecastConsumable, BooleanColumnName.Consumable));
                    */
                    _sListView.Items.Add(item);
                }
                lbtAddsite.Enabled = true;
            }
            else
            {
                lbtAddsite.Enabled = false;
                lbtRemovesite.Enabled = false;
            }
            _sListView.EndUpdate();
        }
        
        private void CreateCategoryFromRegion()
        {
            foreach (ForlabRegion r in _regions)
            {
                MorbidityCategory cat = new MorbidityCategory();
                cat.CategoryName = r.RegionName;
                cat.RegionId = r.Id;
                //cat.MorbidityForecast = _forecast;
               // _forecast.MorbidityCategories.Add(cat);
            }
        }

        private void BindCategorys()
        {
            lvCategory.BeginUpdate();
            lvCategory.Items.Clear();

            foreach (NewMorbidityCategory cat in _forecast.MorbidityCategories)
            {
                ListViewItem li = new ListViewItem(cat.CategoryName) { Tag = cat };
                lvCategory.Items.Add(li);
            }

            lvCategory.EndUpdate();
        }

        private void lvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCategory.SelectedItems.Count > 0)
            {
                _activeCategory = (NewMorbidityCategory)lvCategory.SelectedItems[0].Tag; 
                if (_activeCategory.Id < 0)
                    DataRepository.SaveOrUpdateNewMorbidityForecast(_forecast);
                txtCatname.Text = _activeCategory.CategoryName;
            }
            else
            {
                _activeCategory = null;
                txtCatname.Text = "";
            }

            BindForecastCategory();
            BindArtSites();
        }

        private void BindForecastCategory()
        {
            if (_activeCategory != null)
            {
                butSave.Enabled = true;
                //txtCatname.Enabled = true;
                //txtCatname.Text = _activeCategory.CategoryName;

                if (_activeCategory.Id > 0)
                    butDelete.Enabled = true;
                else
                    butDelete.Enabled = false;
                //butAddnew.Enabled = true;
            }
            else
            {
                //txtCatname.Enabled = false;
                butSave.Enabled = false;
                butDelete.Enabled = false;
                //butAddnew.Enabled = false;
            }
        }

        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (AddSitesToCategory())
            {
                BindArtSites();
               // SetStatusOfCatRadioButton();

                OnNextButtonStatusChanged(true);
                _isedited = true;
            }
        }

        private IList<int> GetSelectedSiteId()
        {
            IList<int> result = new List<int>();
            foreach (NewMordidityCategorySite s in _artSites)
            {
                result.Add(s.Site.Id);
            }
            return result;
        }
        public bool AddSitesToCategory()
        {
            FrmSelectSite frm;
            //if (_forecast.UseRegionAsCat)
            //    frm = new FrmSelectSite(GetSelectedSiteId(), _sites, _activeCategory.RegionId);
            //else
                
            frm = new FrmSelectSite();//GetSelectedSiteId(), _sites);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //foreach (ForlabSite site in frm.SelectedSites)
                //{
                //    NewMordidityCategorySite artsite = new NewMordidityCategorySite();
                //    artsite.Site = site;
                //    artsite.NewMorbidityCategory = _activeCategory;


                //    _artSites.Add(artsite);
                //}

                return true;
            }
            return false;
        }
        private void lbtRemovesite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NewMordidityCategorySite art = (NewMordidityCategorySite)_sListView.SelectedItems[0].Tag;
            
            _artSites.Remove(art);
            _deletedArtSites.Add(art);

            BindArtSites();

            if (_artSites.Count == 0)
                OnNextButtonStatusChanged(false);
            _isedited = true;
        }

        private void sListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_sListView.SelectedItems.Count <= 0)
                lbtRemovesite.Enabled = false;
            else
            {
                if (!lbtRemovesite.Enabled)
                    lbtRemovesite.Enabled = true;
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            //_activeCategory.CategoryName = txtCatname.Text;

            try
            {
                DataRepository.SaveOrUpdateNewMorbidityForecast(_forecast);
                BindCategorys();
            }
            catch
            {
                MessageBox.Show("Error: unable to save categories");
            }            
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure, you want to delete it? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    //_forecast.MorbidityCategories.Remove(_activeCategory);
                    //DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
                    _activeCategory = null;

                    BindCategorys();
                    BindForecastCategory();
                    BindArtSites();
                }
                catch
                {
                    MessageBox.Show("Error: unable to delete ART category");
                }
            }
        }

        private void butAddnew_Click(object sender, EventArgs e)
        {
            _activeCategory = new NewMorbidityCategory();
            _activeCategory.CategoryName = txtCatname.Text;
            _activeCategory.NewMorbidityForecast = _forecast;
            _forecast.MorbidityCategories.Add(_activeCategory);
            
            BindCategorys();
            BindForecastCategory();
            BindArtSites();
        }

        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                DataRepository.BatchSaveNewMordidityCategorySite(_artSites);
                //DataRepository.BatchDeleteNewMordidityCategorySite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }

        private void UseCategory_CheckedChanged(object sender, EventArgs e)
        {
            _activeCategory = null;
            //_forecast.MorbidityCategories.Clear();
            //_forecast.UseRegionAsCat = rbtRegion.Checked;

            //if (rbtRegion.Checked)
            //{                
            //    CreateCategoryFromRegion();
            //}
            BindCategorys();
            BindForecastCategory();
            lbtAddsite.Enabled = false;
        }



    }
}
