﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewSiteSelection : NewBaseMorbidityControl
    {
        private SiteListView _sListView;
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;
        private IList<NewMorbiditySite> _artSites;
        //private IList<NewMordidityCategorySite> _deletedArtSites;
        private bool _isedited = false;



        public NewSiteSelection(NewMorbidityForecast forecast, IList<NewMorbiditySite> artsites)
        {
            this._forecast = forecast;
            this._artSites = artsites;
            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
           

            InitializeComponent();

          

            LoadSiteListView();
        }
        public NewSiteSelection()
        {
            InitializeComponent();
        }

        public override string Title
        {
            get { return "Sites included in quantification"; }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.OptRecentData;
            }
        }

        public override bool EnableNextButton()
        {
            return _artSites.Count > 0;
        }



        private void LoadSiteListView()
        {
            _sListView = new SiteListView();
            _sListView.MySortBrush = SystemBrushes.ControlLight;
            _sListView.MyHighlightBrush = Brushes.Goldenrod;
            _sListView.GridLines = true;
            _sListView.MultiSelect = false;
            _sListView.Dock = DockStyle.Fill;
            _sListView.ControlPadding = 4;
            _sListView.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            _sListView.Scrollable = true;
           
            //add SmallImageList to ListView - images will be shown in ColumnHeaders
            ImageList colimglst = new ImageList();
            colimglst.Images.Add("down", trueFalseImageList.Images[2]);
            colimglst.Images.Add("up", trueFalseImageList.Images[3]);
            colimglst.ColorDepth = ColorDepth.Depth32Bit;
            colimglst.ImageSize = new Size(20, 20); // this will affect the row height
            _sListView.SmallImageList = colimglst;
            
            //add columns and items
            _sListView.Columns.Add(new EXColumnHeader("Region", 100));
            _sListView.Columns.Add(new EXColumnHeader("Selected Site", 150));
            _sListView.Columns.Add(new EXColumnHeader("Site Type", 80));
            
            _sListView.BoolListViewSubItemValueChanged += new EventHandler<EXBoolListViewSubItemEventArgs>(sListView_BoolListViewSubItemValueChanged);
            _sListView.SelectedIndexChanged += new EventHandler(sListView_SelectedIndexChanged);

            panSites.Controls.Add(_sListView);
        }

        private void sListView_BoolListViewSubItemValueChanged(object sender, EXBoolListViewSubItemEventArgs e)
        {

            _isedited = true;
        }


        private void BindArtSites()
        {
            _sListView.Items.Clear();
            _sListView.BeginUpdate();
            
            if (_forecast != null)
            {
                foreach (NewMorbiditySite site in _forecast.NewMorbiditySites)
                {
                    EXListViewItem item = new EXListViewItem(site.Site.Region.RegionName) { Tag = site };

                    item.SubItems.Add(new EXListViewSubItem(site.Site.SiteName));
                    item.SubItems.Add(new EXListViewSubItem(site.Site.SiteCategory != null ? site.Site.SiteCategory.CategoryName : ""));

                    _sListView.Items.Add(item);
                }
                lbtAddsite.Enabled = true;
            }
            else
            {
                lbtAddsite.Enabled = false;
                lbtRemovesite.Enabled = false;
            }
            _sListView.EndUpdate();
        }
        

        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (AddSitesToCategory())
            {
               // BindArtSites();
                

                OnNextButtonStatusChanged(true);
                _isedited = true;
            }
        }

        private IList<int> GetSelectedSiteId()
        {
            IList<int> result = new List<int>();
            foreach (NewMorbiditySite s in _artSites)
            {
                result.Add(s.Site.Id);
            }
            return result;
        }
        public bool AddSitesToCategory()
        {
            FrmSelectSite frm;
            
            frm = new FrmSelectSite();//GetSelectedSiteId(), _sites);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //foreach (ForlabSite site in frm.SelectedSites)
                //{
                //    NewMorbiditySite artsite = new NewMorbiditySite();
                    
                //    artsite.Site = site;
                //    artsite.RegionId = site.Region.Id;
                //    _forecast.NewMorbiditySites.Add(artsite);
                //    artsite.NewMorbidityForecast = _forecast;
                //    _artSites.Add(artsite);
                //}


                return true;
            }
            return false;
        }
        private void lbtRemovesite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NewMorbiditySite art = (NewMorbiditySite)_sListView.SelectedItems[0].Tag;
            
            _artSites.Remove(art);
           // _deletedArtSites.Add(art);

            BindArtSites();

            if (_artSites.Count == 0)
                OnNextButtonStatusChanged(false);
            _isedited = true;
        }

        private void sListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_sListView.SelectedItems.Count <= 0)
                lbtRemovesite.Enabled = false;
            else
            {
                if (!lbtRemovesite.Enabled)
                    lbtRemovesite.Enabled = true;
            }
        }


        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                DataRepository.SaveOrUpdateNewMorbidityForecast(_forecast);
                //DataRepository.BatchSaveNewMorbiditySite(_artSites);
                //DataRepository.BatchDeleteARTSite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }


    }
}
