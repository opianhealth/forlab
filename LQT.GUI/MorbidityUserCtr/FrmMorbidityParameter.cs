﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.Util;
using LQT.Core.UserExceptions;
using LQT.Core.Domain;
using System.Globalization;

namespace LQT.GUI
{
    public partial class FrmMorbidityParameter : Form
    {
        private MMProgram _mMProgram;
        private IList<MMProgram> _mMPrograms;

        private MMForecastParameter _mMForecastParam;
        private IList<MMForecastParameter> _mMForecastParams;

        private MMGroup _mMGroup;
        private IList<MMGroup> _mMGroups;

        private MMGeneralAssumption _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mMGeneralAssumptions;

        public FrmMorbidityParameter()
        {
            InitializeComponent();

            BindMMPrograms();
        }

 


        public bool AddAddFormula()
        {
            FrmAddFormula frm;
            IList<MMForecastParameter> _mMForecastParamss = DataRepository.GetAllForecastParameterByProgram(_mMProgram.Id);

            
            frm = new FrmAddFormula(_mMForecastParamss);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMForecastParameter newcat in frm._updatedmMForecastParams)
                {
                    foreach (MMForecastParameter cat in _mMForecastParamss)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            _mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMForecastParameter(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void stWizard_SelectedPageChanged(object sender, EventArgs e)
        {
            if (stWizard.SelectedPage==wizardPage2)
            {
                wizardPage2.AllowBack = true;
                //SaveOrUpdateObject();

                BindForecastMethod();
                BindVariablesDataType();

                BindMMForecastParameters();

            }
            if (stWizard.SelectedPage == wizardPage3)
            {
                BindMMGroups();
            }
            if (stWizard.SelectedPage == wizardPage4)
            {
                BindGAVariablesDataType();
                BindMMGeneralAssumptionss();
                BindGATypes();
            }
        }

        private void btnProgramAdd_Click(object sender, EventArgs e)
        {
            SaveOrUpdateObject();
            BindMMPrograms();
        }

        private void SaveOrUpdateObject()
        {
            if (_mMProgram == null)
                _mMProgram = new MMProgram();

            if (txtProgramName.Text.Trim() == string.Empty)
                throw new LQTUserException("Program Name can not be empty.");

            DataRepository.CloseSession();
            _mMProgram.ProgramName = this.txtProgramName.Text.Trim();

            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
            _mMProgram = null;

        }

        private void BindMMPrograms()
        {
            _mMPrograms = DataRepository.GetMMPrograms();
            int i = 1;
            lvProgramList.BeginUpdate();
            lvProgramList.Items.Clear();

            foreach (MMProgram mMprogram in _mMPrograms)
            {
                ListViewItem listViewItem = new ListViewItem(i.ToString())
                {
                    Tag = mMprogram
                };
                listViewItem.SubItems.Add(mMprogram.ProgramName);
                listViewItem.SubItems.Add(mMprogram.Description);

                lvProgramList.Items.Add(listViewItem);
                i++;
            }
            lvProgramList.EndUpdate();
            SetColors(lvProgramList);
        }

        private void lvProgramList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvProgramList.SelectedItems.Count > 0)
            {
                _mMProgram = (MMProgram)lvProgramList.SelectedItems[0].Tag;
                this.txtProgramName.Text = _mMProgram.ProgramName;
            }
            else
            {
                _mMProgram = null;
                this.txtProgramName.Text = string.Empty;
            }
        }

        public void SetColors(ListView lst)
        { 
            foreach (ListViewItem item in lst.Items)
                item.BackColor = (item.Index % 2 == 0) ? Color.GhostWhite : Color.WhiteSmoke; 
        }

        public void SetColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            using (StringFormat sf = new StringFormat())
            {
                // Store the column text alignment, letting it default
                // to Left if it has not been set to Center or Right.
                switch (e.Header.TextAlign)
                {
                    case HorizontalAlignment.Center:
                        sf.Alignment = StringAlignment.Center;
                        break;
                    case HorizontalAlignment.Right:
                        sf.Alignment = StringAlignment.Far;
                        break;
                }
                // Draw the standard header background.
                e.DrawBackground();

                // Draw the header text.
                using (Font headerFont = new Font("Helvetica", 10, FontStyle.Bold)) //Font size!!!!
                {
                    e.Graphics.DrawString(e.Header.Text, headerFont, Brushes.DimGray, e.Bounds, sf);
                }
            }
        }

        private void lvProgramList_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvProgramList_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void BindForecastMethod()
        {
            string[] forecastMethod = Enum.GetNames(typeof(MorbidityForecastingMethod));
            cobforecastmethodWiz2.Items.Clear();
            for (int i = 0; i < forecastMethod.Length; i++)
            {
                cobforecastmethodWiz2.Items.Add(forecastMethod[i].Replace('_',' '));
            }
        }

        private void BindVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz2.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz2.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void lvCategory_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvGroup_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvgeneralassumptions_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void btnAddWiz2_Click(object sender, EventArgs e)
        {
           // if (_mMProgram != null)
           // {
                SaveOrUpdateMMForecastParameterObject();
                BindMMForecastParameters();
           // }
        }

        private void SaveOrUpdateMMForecastParameterObject()
        {
            if (_mMForecastParam == null)
                _mMForecastParam = new MMForecastParameter();

            _mMForecastParam.ForecastMethod = (int)Enum.Parse(typeof(MorbidityForecastingMethod), cobforecastmethodWiz2.Text.Trim().Replace(' ','_'));
            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            _mMForecastParam.VariableName = t.ToTitleCase(txtVariableNameWiz2.Text.Trim());

            _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz2.Text.Trim());
            _mMForecastParam.UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

            _mMForecastParam.MMProgram = _mMProgram;
            _mMForecastParam.VarCode = LqtUtil.GetUpperCases(_mMForecastParam.VariableName);

            _mMForecastParam.IsPrimaryOutput = chkprimaryoutput.Checked;

            _mMForecastParam.VariableEffect = rdbPosWiz2.Checked? true:false;
            
            _mMProgram.MMForecastParameters.Add(_mMForecastParam);



            DataRepository.SaveOrUpdateMMForecastParameter(_mMForecastParam);
            BindMMForecastParameters();
            _mMForecastParam = null;

        }

        private void BindMMForecastParameters()
        {
            int i = 1;
            lvforecastParamsWiz2.BeginUpdate();
            lvforecastParamsWiz2.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMFParam
                    };


                    listViewItem.SubItems.Add(((MorbidityForecastingMethod)mMFParam.ForecastMethod).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMFParam.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMFParam.VariableName);
                    listViewItem.SubItems.Add(mMFParam.UseOn);
                    listViewItem.SubItems.Add(mMFParam.VariableFormula);

                    lvforecastParamsWiz2.Items.Add(listViewItem);
                    i++;
                }
            }
            lvforecastParamsWiz2.EndUpdate();
            SetColors(lvforecastParamsWiz2);
        }

        private void lvforecastParamsWiz2_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvforecastParamsWiz2_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void btnaddformulaWiz2_Click(object sender, EventArgs e)
        {
            if (AddAddFormula())
            {
                BindMMForecastParameters();
            }
        }

        # region Group
        private void btnAddWiz3_Click(object sender, EventArgs e)
        {
            SaveOrUpdateMMGroupObject();
            BindMMGroups();
        }

        private void SaveOrUpdateMMGroupObject()
        {
            if (_mMGroup == null)
                _mMGroup = new MMGroup();

            _mMGroup.GroupName = txtgroupnameWiz3.Text.Trim();

            _mMGroup.MMProgram = _mMProgram;
            _mMProgram.MMGroups.Add(_mMGroup);


            DataRepository.SaveOrUpdateMMGroup(_mMGroup);

            BindMMGroups();
            _mMGroup = null;

        }

        private void BindMMGroups()
        {
            int i = 1;
            lvGroupWiz3.BeginUpdate();
            lvGroupWiz3.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGroup mMGroup in _mMProgram.MMGroups)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGroup
                    };

                    listViewItem.SubItems.Add(mMGroup.GroupName);

                    lvGroupWiz3.Items.Add(listViewItem);
                    i++;
                }
            }
            lvGroupWiz3.EndUpdate();
            SetColors(lvGroupWiz3);
        }

        #endregion

        private void lvGroupWiz3_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }
        //**************** --Wizard4-- ***********************//

        private void btnAddWiz4_Click(object sender, EventArgs e)
        {
            SaveOrUpdateMMGeneralAssumptionsObject();
            BindMMGeneralAssumptionss();
        }

        private void SaveOrUpdateMMGeneralAssumptionsObject()
        {
            if (_mMGeneralAssumption == null)
                _mMGeneralAssumption = new MMGeneralAssumption();

            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            _mMGeneralAssumption.VariableName = t.ToTitleCase(txtVariableNameWiz4.Text.Trim());

            _mMGeneralAssumption.AssumptionType = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ' ,'_'));

            _mMGeneralAssumption.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz4.Text.Trim());
            _mMGeneralAssumption.UseOn = rdbeachWiz4.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

            _mMGeneralAssumption.MMProgram = _mMProgram;
            _mMGeneralAssumption.VarCode = LqtUtil.GetUpperCases(_mMGeneralAssumption.VariableName);

            _mMGeneralAssumption.VariableEffect = rdbPosWiz4.Checked ? true : false;

            _mMProgram.MMGeneralAssumptions.Add(_mMGeneralAssumption);

            DataRepository.SaveOrUpdateMMGeneralAssumption(_mMGeneralAssumption);
            BindMMGeneralAssumptionss();
            _mMGeneralAssumption = null;

        }

        private void BindMMGeneralAssumptionss()
        {
        
            int i = 1;
            lvgeneralassumptions.BeginUpdate();
            lvgeneralassumptions.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lvgeneralassumptions.Items.Add(listViewItem);
                    i++;
                }
            }
            lvgeneralassumptions.EndUpdate();
            SetColors(lvgeneralassumptions);
        }

        private void BindGAVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz4.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz4.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void BindGATypes()
        {
            string[] variableAssumptionType = Enum.GetNames(typeof(MorbidityGeneralAssumptionTypes));
            cobAssumptionTypeWiz4.Items.Clear();
            for (int i = 0; i < variableAssumptionType.Length; i++)
            {
                cobAssumptionTypeWiz4.Items.Add(variableAssumptionType[i].Replace('_', ' '));
            }
        }

        private void btnaddformulaWiz4_Click(object sender, EventArgs e)
        {
            if (AddAddFormulaGA())
            {
                BindMMGeneralAssumptionss();
            }
        }

        public bool AddAddFormulaGA()
        {
            FrmAddFormula frm;
            IList<MMGeneralAssumption> _mMGA = DataRepository.GetAllGeneralAssumptionByProgram(_mMProgram.Id);


            frm = new FrmAddFormula(_mMGA,1,"GA");
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMGeneralAssumption newcat in frm._updatedmMGeneralAssumptions)
                {
                    foreach (MMGeneralAssumption cat in _mMGA)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            _mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMGeneralAssumption(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void lvgeneralassumptions_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void chkgeneralprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.GTP = chkgeneralprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);

        }

        private void chkrapidprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.RTP = chkrapidprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

     

    }
}
