﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewOptRecentData : NewBaseMorbidityControl
    {
        private LQTCheckBox _activeCheckBox;
        private NewMorbidityForecast _forecast;
        private bool _edited = false;

        public NewOptRecentData(NewMorbidityForecast forecast)
        {
            this._forecast = forecast;
            InitializeComponent();

            //if (forecast.OptInitialPatientData > 0)
            //{
            //    if (forecast.OptInitialPatientData == 1)
            //    {
            //        lqtCheckBox1.Checked = true;
            //        _activeCheckBox = lqtCheckBox1;
            //    }
            //    else
            //    {
            //        lqtCheckBox2.Checked = true;
            //        _activeCheckBox = lqtCheckBox2;
            //    }
            //}
            //else
            //{
                OnNextButtonStatusChanged(false);
            //}

            lqtCheckBox1.LQTCheckBoxClick += new EventHandler<LQTCheckBoxEvenArgs>(LQTCheckBoxClick);
            lqtCheckBox2.LQTCheckBoxClick += new EventHandler<LQTCheckBoxEvenArgs>(LQTCheckBoxClick);
            lqtCheckBox3.LQTCheckBoxClick += new EventHandler<LQTCheckBoxEvenArgs>(LQTCheckBoxClick);
            lqtCheckBox4.LQTCheckBoxClick += new EventHandler<LQTCheckBoxEvenArgs>(LQTCheckBoxClick);
        }

        public override string Title
        {
            get { return "Forecating Method"; }
        }
        public override string Description
        {
            get
            {
                //"Choose the Method that You Would Like to Load Patient Data"
                string desc = "<p> Select options for patient population and related assumptions towards the forecast.</p>";

                desc += "<p>1. Population based forecast </br> If main data to be used/available is population and "
                        +" population based national health statstics. </p>";

                desc += "<p>2. Target based forecast</br> If target to be reached at end of forecast period is available.</p>";

                desc += "<p>3. Define custom Model/Dynamic-Formula</br> If user want to define custome formula.</p>";

                desc += "<p>4. Get custom pre-defined formulas from online/internet</br> If user want get program specific forecast formulas from internet.</p>";

                return desc;
            }
        }
        public override MorbidityCtrEnum PriviousCtr
        {
            get
            {
                return MorbidityCtrEnum.SiteSelection;
            }
        }
        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                if (_forecast.OptForecastFormula == 1)
                    return MorbidityCtrEnum.OptPopulationBased;
                else if (_forecast.OptForecastFormula == 2)
                    return MorbidityCtrEnum.OptTargetBased;
                else if (_forecast.OptForecastFormula == 3)
                    return MorbidityCtrEnum.OptDynamicBased;

                return MorbidityCtrEnum.Nothing;
            }
        }

        public override bool EnableNextButton()
        {
            return _forecast.OptForecastFormula > 0;
        }

        private void LQTCheckBoxClick(object sender, LQTCheckBoxEvenArgs e)
        {
            if (_activeCheckBox != null)
                _activeCheckBox.Checked = false;
            _forecast.OptForecastFormula = Convert.ToInt32(e.Tag);
            _activeCheckBox = (LQTCheckBox)sender;
            _edited = true;
            OnNextButtonStatusChanged(EnableNextButton());
        }

        public override bool DoSomthingBeforeUnload()
        {
            if (_edited)
            {
                //DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
            }
            return true;
        }
    }
}
