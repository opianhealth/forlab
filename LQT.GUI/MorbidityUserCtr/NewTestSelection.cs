﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewTestSelection : NewBaseMorbidityControl
    {
        private IList<Test> _test;

        private bool _isedited = false;




        public NewTestSelection()
        {
            InitializeComponent();
        }

        public override string Title
        {
            get { return "Tests to included in the forecast"; }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.OptRecentData;
            }
        }

        public override bool EnableNextButton()
        {
            return true;
        }

        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (AddTest())
            {
                OnNextButtonStatusChanged(true);
                _isedited = true;
            }
        }

        public bool AddTest()
        {
            FrmNewSelectTest frm;

            frm = new FrmNewSelectTest();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (Test t in frm._selectedTest)
                {
                    //_test.Add(t);
                }
                return true;
            }
            return false;
        }

      
       
        private void lbtRemovesite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           
            _isedited = true;
        }

        private void sListView_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }


        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.SaveOrUpdateNewMorbidityForecast(_forecast);
                //DataRepository.BatchSaveNewMorbiditySite(_artSites);
                //DataRepository.BatchDeleteARTSite(_deletedArtSites);
                //MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }


    }
}
