﻿namespace LQT.GUI.MorbidityUserCtr
{
    partial class FrmMorbidityParameter_new1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabReview = new System.Windows.Forms.TabPage();
            this.lstReviewProductAssumption = new System.Windows.Forms.ListView();
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label27 = new System.Windows.Forms.Label();
            this.lstReviewTestingassumption = new System.Windows.Forms.ListView();
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label26 = new System.Windows.Forms.Label();
            this.lbltestingprotocol1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lstReviewGeneralAssumption = new System.Windows.Forms.ListView();
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label17 = new System.Windows.Forms.Label();
            this.lstReviewPatientGroup = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label16 = new System.Windows.Forms.Label();
            this.lstReviewForecastingVariable = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label13 = new System.Windows.Forms.Label();
            this.lblprogram1 = new System.Windows.Forms.Label();
            this.lblProgram = new System.Windows.Forms.Label();
            this.tabTestingProtocol = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnTPA = new System.Windows.Forms.Button();
            this.rbrapidprotocol = new System.Windows.Forms.RadioButton();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.rbgeneralprotocol = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.chkrapidprotocol = new System.Windows.Forms.CheckBox();
            this.chkgeneralprotocol = new System.Windows.Forms.CheckBox();
            this.tabPatientAssumption = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstParameters = new System.Windows.Forms.ListBox();
            this.chkGeneralAssumption = new System.Windows.Forms.CheckBox();
            this.panel17 = new System.Windows.Forms.Panel();
            this.rdbPosWiz4 = new System.Windows.Forms.RadioButton();
            this.rdbNegWiz4 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.cobAssumptionTypeWiz4 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnAddWiz4 = new System.Windows.Forms.Button();
            this.btnaddformulaWiz4 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.rdbAggWiz4 = new System.Windows.Forms.RadioButton();
            this.txtVariableNameWiz4 = new System.Windows.Forms.TextBox();
            this.rdbeachWiz4 = new System.Windows.Forms.RadioButton();
            this.cobDatatypeWiz4 = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.lvgeneralassumptions = new System.Windows.Forms.ListView();
            this.cc1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cc5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPatientGroup = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.chkActiveGroup = new System.Windows.Forms.CheckBox();
            this.btnAddWiz3 = new System.Windows.Forms.Button();
            this.txtgroupnameWiz3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lvGroupWiz3 = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabForecastMethod = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lvforecastParamsWiz2 = new System.Windows.Forms.ListView();
            this.c1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.c5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chkActive = new System.Windows.Forms.CheckBox();
            this.chkprimaryoutput = new System.Windows.Forms.CheckBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.rdbPosWiz2 = new System.Windows.Forms.RadioButton();
            this.rdbNegWiz2 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddWiz2 = new System.Windows.Forms.Button();
            this.btnaddformulaWiz2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbAggWiz2 = new System.Windows.Forms.RadioButton();
            this.rdbeachWiz2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.cobDatatypeWiz2 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVariableNameWiz2 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnForecastAdd = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cobforecastmethodWiz2 = new System.Windows.Forms.ComboBox();
            this.tabDefprogram = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnProgramAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProgramName = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.lvProgramList = new System.Windows.Forms.ListView();
            this.colNo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colprogramname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rdbPosProduct = new System.Windows.Forms.TabControl();
            this.tabTestingAssumption = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lvTestingAssumption = new System.Windows.Forms.ListView();
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkTestingAssumption = new System.Windows.Forms.CheckBox();
            this.panel10 = new System.Windows.Forms.Panel();
            this.rdbPosTesting = new System.Windows.Forms.RadioButton();
            this.rdbNegTesting = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.rdbAggTesting = new System.Windows.Forms.RadioButton();
            this.rdbeachTesting = new System.Windows.Forms.RadioButton();
            this.cboTestingDatatype = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cboTestingAssumption = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTVariableName = new System.Windows.Forms.TextBox();
            this.tabProductAssumption = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.lvProductAssumption = new System.Windows.Forms.ListView();
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chkProductAssumption = new System.Windows.Forms.CheckBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.rdbPosProduct1 = new System.Windows.Forms.RadioButton();
            this.rdbNegProduct = new System.Windows.Forms.RadioButton();
            this.label25 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.rdbAggProduct = new System.Windows.Forms.RadioButton();
            this.rdbeachProduct = new System.Windows.Forms.RadioButton();
            this.button6 = new System.Windows.Forms.Button();
            this.cboProductDatatype = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cboProductassuption = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtProductVariableName = new System.Windows.Forms.TextBox();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.panel1.SuspendLayout();
            this.tabReview.SuspendLayout();
            this.tabTestingProtocol.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel18.SuspendLayout();
            this.tabPatientAssumption.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabPatientGroup.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabForecastMethod.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabDefprogram.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.rdbPosProduct.SuspendLayout();
            this.tabTestingAssumption.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabProductAssumption.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(4, 600);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(940, 34);
            this.panel1.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(839, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(71, 28);
            this.button3.TabIndex = 2;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(685, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 28);
            this.button2.TabIndex = 1;
            this.button2.Text = "Back";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(762, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Next";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabReview
            // 
            this.tabReview.Controls.Add(this.lstReviewProductAssumption);
            this.tabReview.Controls.Add(this.label27);
            this.tabReview.Controls.Add(this.lstReviewTestingassumption);
            this.tabReview.Controls.Add(this.label26);
            this.tabReview.Controls.Add(this.lbltestingprotocol1);
            this.tabReview.Controls.Add(this.label18);
            this.tabReview.Controls.Add(this.lstReviewGeneralAssumption);
            this.tabReview.Controls.Add(this.label17);
            this.tabReview.Controls.Add(this.lstReviewPatientGroup);
            this.tabReview.Controls.Add(this.label16);
            this.tabReview.Controls.Add(this.lstReviewForecastingVariable);
            this.tabReview.Controls.Add(this.label13);
            this.tabReview.Controls.Add(this.lblprogram1);
            this.tabReview.Controls.Add(this.lblProgram);
            this.tabReview.Location = new System.Drawing.Point(4, 25);
            this.tabReview.Name = "tabReview";
            this.tabReview.Padding = new System.Windows.Forms.Padding(3);
            this.tabReview.Size = new System.Drawing.Size(940, 562);
            this.tabReview.TabIndex = 5;
            this.tabReview.Tag = "Review";
            this.tabReview.Text = "Review";
            this.tabReview.UseVisualStyleBackColor = true;
            this.tabReview.Click += new System.EventHandler(this.tabReview_Click);
            // 
            // lstReviewProductAssumption
            // 
            this.lstReviewProductAssumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewProductAssumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader42,
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45,
            this.columnHeader46,
            this.columnHeader47,
            this.columnHeader48});
            this.lstReviewProductAssumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReviewProductAssumption.FullRowSelect = true;
            this.lstReviewProductAssumption.GridLines = true;
            this.lstReviewProductAssumption.Location = new System.Drawing.Point(24, 435);
            this.lstReviewProductAssumption.Name = "lstReviewProductAssumption";
            this.lstReviewProductAssumption.OwnerDraw = true;
            this.lstReviewProductAssumption.Size = new System.Drawing.Size(908, 58);
            this.lstReviewProductAssumption.TabIndex = 47;
            this.lstReviewProductAssumption.UseCompatibleStateImageBehavior = false;
            this.lstReviewProductAssumption.View = System.Windows.Forms.View.Details;
            this.lstReviewProductAssumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView2_DrawColumnHeader);
            this.lstReviewProductAssumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView2_DrawItem);
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "No";
            this.columnHeader42.Width = 33;
            // 
            // columnHeader43
            // 
            this.columnHeader43.DisplayIndex = 6;
            this.columnHeader43.Text = "Active";
            this.columnHeader43.Width = 68;
            // 
            // columnHeader44
            // 
            this.columnHeader44.DisplayIndex = 1;
            this.columnHeader44.Text = "Type";
            // 
            // columnHeader45
            // 
            this.columnHeader45.DisplayIndex = 2;
            this.columnHeader45.Text = "Data Type";
            this.columnHeader45.Width = 76;
            // 
            // columnHeader46
            // 
            this.columnHeader46.DisplayIndex = 3;
            this.columnHeader46.Text = "Variable Name";
            this.columnHeader46.Width = 328;
            // 
            // columnHeader47
            // 
            this.columnHeader47.DisplayIndex = 4;
            this.columnHeader47.Text = "Use on ";
            this.columnHeader47.Width = 102;
            // 
            // columnHeader48
            // 
            this.columnHeader48.DisplayIndex = 5;
            this.columnHeader48.Text = "Formula";
            this.columnHeader48.Width = 124;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.SteelBlue;
            this.label27.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(25, 413);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(908, 19);
            this.label27.TabIndex = 46;
            this.label27.Text = "Product Assumptions";
            // 
            // lstReviewTestingassumption
            // 
            this.lstReviewTestingassumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewTestingassumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader40,
            this.columnHeader41});
            this.lstReviewTestingassumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReviewTestingassumption.FullRowSelect = true;
            this.lstReviewTestingassumption.GridLines = true;
            this.lstReviewTestingassumption.Location = new System.Drawing.Point(23, 341);
            this.lstReviewTestingassumption.Name = "lstReviewTestingassumption";
            this.lstReviewTestingassumption.OwnerDraw = true;
            this.lstReviewTestingassumption.Size = new System.Drawing.Size(908, 66);
            this.lstReviewTestingassumption.TabIndex = 45;
            this.lstReviewTestingassumption.UseCompatibleStateImageBehavior = false;
            this.lstReviewTestingassumption.View = System.Windows.Forms.View.Details;
            this.lstReviewTestingassumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView1_DrawColumnHeader);
            this.lstReviewTestingassumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView1_DrawItem);
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "No";
            this.columnHeader21.Width = 33;
            // 
            // columnHeader22
            // 
            this.columnHeader22.DisplayIndex = 6;
            this.columnHeader22.Text = "Active";
            this.columnHeader22.Width = 68;
            // 
            // columnHeader23
            // 
            this.columnHeader23.DisplayIndex = 1;
            this.columnHeader23.Text = "Type";
            // 
            // columnHeader24
            // 
            this.columnHeader24.DisplayIndex = 2;
            this.columnHeader24.Text = "Data Type";
            this.columnHeader24.Width = 76;
            // 
            // columnHeader25
            // 
            this.columnHeader25.DisplayIndex = 3;
            this.columnHeader25.Text = "Variable Name";
            this.columnHeader25.Width = 328;
            // 
            // columnHeader40
            // 
            this.columnHeader40.DisplayIndex = 4;
            this.columnHeader40.Text = "Use on ";
            this.columnHeader40.Width = 102;
            // 
            // columnHeader41
            // 
            this.columnHeader41.DisplayIndex = 5;
            this.columnHeader41.Text = "Formula";
            this.columnHeader41.Width = 124;
            // 
            // label26
            // 
            this.label26.BackColor = System.Drawing.Color.SteelBlue;
            this.label26.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(24, 319);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(908, 19);
            this.label26.TabIndex = 44;
            this.label26.Text = "Testing Assumptions";
            // 
            // lbltestingprotocol1
            // 
            this.lbltestingprotocol1.AutoSize = true;
            this.lbltestingprotocol1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltestingprotocol1.Location = new System.Drawing.Point(179, 499);
            this.lbltestingprotocol1.Name = "lbltestingprotocol1";
            this.lbltestingprotocol1.Size = new System.Drawing.Size(111, 13);
            this.lbltestingprotocol1.TabIndex = 43;
            this.lbltestingprotocol1.Text = "Testing Protocol Type";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(19, 499);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(132, 13);
            this.label18.TabIndex = 42;
            this.label18.Text = "Testing Protocol Type";
            // 
            // lstReviewGeneralAssumption
            // 
            this.lstReviewGeneralAssumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewGeneralAssumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20});
            this.lstReviewGeneralAssumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstReviewGeneralAssumption.FullRowSelect = true;
            this.lstReviewGeneralAssumption.GridLines = true;
            this.lstReviewGeneralAssumption.Location = new System.Drawing.Point(23, 246);
            this.lstReviewGeneralAssumption.Name = "lstReviewGeneralAssumption";
            this.lstReviewGeneralAssumption.OwnerDraw = true;
            this.lstReviewGeneralAssumption.Size = new System.Drawing.Size(908, 66);
            this.lstReviewGeneralAssumption.TabIndex = 41;
            this.lstReviewGeneralAssumption.UseCompatibleStateImageBehavior = false;
            this.lstReviewGeneralAssumption.View = System.Windows.Forms.View.Details;
            this.lstReviewGeneralAssumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewGeneralAssumption_DrawColumnHeader);
            this.lstReviewGeneralAssumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewGeneralAssumption_DrawItem);
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "No";
            this.columnHeader14.Width = 33;
            // 
            // columnHeader15
            // 
            this.columnHeader15.DisplayIndex = 6;
            this.columnHeader15.Text = "Active";
            this.columnHeader15.Width = 68;
            // 
            // columnHeader16
            // 
            this.columnHeader16.DisplayIndex = 1;
            this.columnHeader16.Text = "Type";
            // 
            // columnHeader17
            // 
            this.columnHeader17.DisplayIndex = 2;
            this.columnHeader17.Text = "Data Type";
            this.columnHeader17.Width = 76;
            // 
            // columnHeader18
            // 
            this.columnHeader18.DisplayIndex = 3;
            this.columnHeader18.Text = "Variable Name";
            this.columnHeader18.Width = 328;
            // 
            // columnHeader19
            // 
            this.columnHeader19.DisplayIndex = 4;
            this.columnHeader19.Text = "Use on ";
            this.columnHeader19.Width = 102;
            // 
            // columnHeader20
            // 
            this.columnHeader20.DisplayIndex = 5;
            this.columnHeader20.Text = "Formula";
            this.columnHeader20.Width = 124;
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.SteelBlue;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(22, 219);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(910, 19);
            this.label17.TabIndex = 40;
            this.label17.Text = "Paient Assumptions";
            // 
            // lstReviewPatientGroup
            // 
            this.lstReviewPatientGroup.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewPatientGroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.lstReviewPatientGroup.GridLines = true;
            this.lstReviewPatientGroup.Location = new System.Drawing.Point(20, 154);
            this.lstReviewPatientGroup.Name = "lstReviewPatientGroup";
            this.lstReviewPatientGroup.OwnerDraw = true;
            this.lstReviewPatientGroup.Size = new System.Drawing.Size(908, 58);
            this.lstReviewPatientGroup.TabIndex = 39;
            this.lstReviewPatientGroup.UseCompatibleStateImageBehavior = false;
            this.lstReviewPatientGroup.View = System.Windows.Forms.View.Details;
            this.lstReviewPatientGroup.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewPatientGroup_DrawColumnHeader);
            this.lstReviewPatientGroup.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewPatientGroup_DrawItem);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "No";
            this.columnHeader11.Width = 29;
            // 
            // columnHeader12
            // 
            this.columnHeader12.DisplayIndex = 2;
            this.columnHeader12.Text = "Active";
            this.columnHeader12.Width = 82;
            // 
            // columnHeader13
            // 
            this.columnHeader13.DisplayIndex = 1;
            this.columnHeader13.Text = "Group Name";
            this.columnHeader13.Width = 681;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.SteelBlue;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(19, 126);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(913, 19);
            this.label16.TabIndex = 38;
            this.label16.Text = "Patient/Population Group";
            // 
            // lstReviewForecastingVariable
            // 
            this.lstReviewForecastingVariable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstReviewForecastingVariable.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.lstReviewForecastingVariable.FullRowSelect = true;
            this.lstReviewForecastingVariable.GridLines = true;
            this.lstReviewForecastingVariable.Location = new System.Drawing.Point(18, 68);
            this.lstReviewForecastingVariable.Name = "lstReviewForecastingVariable";
            this.lstReviewForecastingVariable.OwnerDraw = true;
            this.lstReviewForecastingVariable.Size = new System.Drawing.Size(905, 54);
            this.lstReviewForecastingVariable.TabIndex = 37;
            this.lstReviewForecastingVariable.UseCompatibleStateImageBehavior = false;
            this.lstReviewForecastingVariable.View = System.Windows.Forms.View.Details;
            this.lstReviewForecastingVariable.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lstReviewForecastingVariable_DrawColumnHeader);
            this.lstReviewForecastingVariable.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstReviewForecastingVariable_DrawItem);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No";
            this.columnHeader1.Width = 30;
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 6;
            this.columnHeader2.Text = "Active";
            this.columnHeader2.Width = 85;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 1;
            this.columnHeader3.Text = "Method";
            this.columnHeader3.Width = 116;
            // 
            // columnHeader4
            // 
            this.columnHeader4.DisplayIndex = 2;
            this.columnHeader4.Text = "Data Type";
            this.columnHeader4.Width = 86;
            // 
            // columnHeader8
            // 
            this.columnHeader8.DisplayIndex = 3;
            this.columnHeader8.Text = "Variable Name";
            this.columnHeader8.Width = 252;
            // 
            // columnHeader9
            // 
            this.columnHeader9.DisplayIndex = 4;
            this.columnHeader9.Text = "Use on";
            this.columnHeader9.Width = 61;
            // 
            // columnHeader10
            // 
            this.columnHeader10.DisplayIndex = 5;
            this.columnHeader10.Text = "Formula";
            this.columnHeader10.Width = 165;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.SteelBlue;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(17, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(914, 19);
            this.label13.TabIndex = 18;
            this.label13.Text = "Forcasting Method Variable";
            // 
            // lblprogram1
            // 
            this.lblprogram1.AutoSize = true;
            this.lblprogram1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprogram1.Location = new System.Drawing.Point(132, 16);
            this.lblprogram1.Name = "lblprogram1";
            this.lblprogram1.Size = new System.Drawing.Size(77, 13);
            this.lblprogram1.TabIndex = 1;
            this.lblprogram1.Text = "Program Name";
            // 
            // lblProgram
            // 
            this.lblProgram.AutoSize = true;
            this.lblProgram.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgram.Location = new System.Drawing.Point(19, 16);
            this.lblProgram.Name = "lblProgram";
            this.lblProgram.Size = new System.Drawing.Size(89, 13);
            this.lblProgram.TabIndex = 0;
            this.lblProgram.Text = "Program Name";
            // 
            // tabTestingProtocol
            // 
            this.tabTestingProtocol.Controls.Add(this.panel14);
            this.tabTestingProtocol.Location = new System.Drawing.Point(4, 25);
            this.tabTestingProtocol.Name = "tabTestingProtocol";
            this.tabTestingProtocol.Padding = new System.Windows.Forms.Padding(3);
            this.tabTestingProtocol.Size = new System.Drawing.Size(940, 562);
            this.tabTestingProtocol.TabIndex = 4;
            this.tabTestingProtocol.Tag = "Testing Protocol Options";
            this.tabTestingProtocol.Text = "Testing Protocol Options";
            this.tabTestingProtocol.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnTPA);
            this.panel14.Controls.Add(this.rbrapidprotocol);
            this.panel14.Controls.Add(this.cboYear);
            this.panel14.Controls.Add(this.label28);
            this.panel14.Controls.Add(this.panel18);
            this.panel14.Controls.Add(this.label4);
            this.panel14.Controls.Add(this.chkrapidprotocol);
            this.panel14.Controls.Add(this.chkgeneralprotocol);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(3, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(934, 556);
            this.panel14.TabIndex = 1;
            // 
            // btnTPA
            // 
            this.btnTPA.Location = new System.Drawing.Point(733, 40);
            this.btnTPA.Name = "btnTPA";
            this.btnTPA.Size = new System.Drawing.Size(75, 23);
            this.btnTPA.TabIndex = 46;
            this.btnTPA.Text = "Add";
            this.btnTPA.UseVisualStyleBackColor = true;
            this.btnTPA.Click += new System.EventHandler(this.btnTPA_Click);
            // 
            // rbrapidprotocol
            // 
            this.rbrapidprotocol.AutoSize = true;
            this.rbrapidprotocol.Location = new System.Drawing.Point(17, 169);
            this.rbrapidprotocol.Name = "rbrapidprotocol";
            this.rbrapidprotocol.Size = new System.Drawing.Size(133, 17);
            this.rbrapidprotocol.TabIndex = 22;
            this.rbrapidprotocol.Text = "Rapid Testing Protocol";
            this.rbrapidprotocol.UseVisualStyleBackColor = true;
            this.rbrapidprotocol.Visible = false;
            // 
            // cboYear
            // 
            this.cboYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cboYear.Location = new System.Drawing.Point(482, 40);
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(210, 21);
            this.cboYear.TabIndex = 45;
            this.cboYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox1_KeyDown);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(447, 41);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 13);
            this.label28.TabIndex = 44;
            this.label28.Text = "Year :";
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.rbgeneralprotocol);
            this.panel18.Location = new System.Drawing.Point(17, 38);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(170, 52);
            this.panel18.TabIndex = 43;
            // 
            // rbgeneralprotocol
            // 
            this.rbgeneralprotocol.AutoSize = true;
            this.rbgeneralprotocol.Checked = true;
            this.rbgeneralprotocol.Location = new System.Drawing.Point(6, 3);
            this.rbgeneralprotocol.Name = "rbgeneralprotocol";
            this.rbgeneralprotocol.Size = new System.Drawing.Size(142, 17);
            this.rbgeneralprotocol.TabIndex = 21;
            this.rbgeneralprotocol.TabStop = true;
            this.rbgeneralprotocol.Text = "General Testing Protocol";
            this.rbgeneralprotocol.UseVisualStyleBackColor = true;
            this.rbgeneralprotocol.CheckedChanged += new System.EventHandler(this.rbgeneralprotocol_CheckedChanged);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(14, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(721, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "How many year/s does this testing protocol cycle have? 1/2";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // chkrapidprotocol
            // 
            this.chkrapidprotocol.AutoSize = true;
            this.chkrapidprotocol.Location = new System.Drawing.Point(17, 135);
            this.chkrapidprotocol.Name = "chkrapidprotocol";
            this.chkrapidprotocol.Size = new System.Drawing.Size(134, 17);
            this.chkrapidprotocol.TabIndex = 2;
            this.chkrapidprotocol.Text = "Rapid Testing Protocol";
            this.chkrapidprotocol.UseVisualStyleBackColor = true;
            this.chkrapidprotocol.Visible = false;
            this.chkrapidprotocol.CheckedChanged += new System.EventHandler(this.chkrapidprotocol_CheckedChanged);
            // 
            // chkgeneralprotocol
            // 
            this.chkgeneralprotocol.AutoSize = true;
            this.chkgeneralprotocol.Location = new System.Drawing.Point(17, 109);
            this.chkgeneralprotocol.Name = "chkgeneralprotocol";
            this.chkgeneralprotocol.Size = new System.Drawing.Size(143, 17);
            this.chkgeneralprotocol.TabIndex = 1;
            this.chkgeneralprotocol.Text = "General Testing Protocol";
            this.chkgeneralprotocol.UseVisualStyleBackColor = true;
            this.chkgeneralprotocol.Visible = false;
            this.chkgeneralprotocol.CheckedChanged += new System.EventHandler(this.chkgeneralprotocol_CheckedChanged);
            // 
            // tabPatientAssumption
            // 
            this.tabPatientAssumption.Controls.Add(this.tableLayoutPanel4);
            this.tabPatientAssumption.Location = new System.Drawing.Point(4, 25);
            this.tabPatientAssumption.Name = "tabPatientAssumption";
            this.tabPatientAssumption.Padding = new System.Windows.Forms.Padding(3);
            this.tabPatientAssumption.Size = new System.Drawing.Size(940, 562);
            this.tabPatientAssumption.TabIndex = 3;
            this.tabPatientAssumption.Tag = "Patient Assumptions";
            this.tabPatientAssumption.Text = "Patient Assumptions";
            this.tabPatientAssumption.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 269F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(934, 556);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.groupBox1);
            this.panel8.Controls.Add(this.chkGeneralAssumption);
            this.panel8.Controls.Add(this.panel17);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.cobAssumptionTypeWiz4);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.btnAddWiz4);
            this.panel8.Controls.Add(this.btnaddformulaWiz4);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.rdbAggWiz4);
            this.panel8.Controls.Add(this.txtVariableNameWiz4);
            this.panel8.Controls.Add(this.rdbeachWiz4);
            this.panel8.Controls.Add(this.cobDatatypeWiz4);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(928, 263);
            this.panel8.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstParameters);
            this.groupBox1.Location = new System.Drawing.Point(699, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(214, 241);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Possible patient related assumptions ";
            // 
            // lstParameters
            // 
            this.lstParameters.FormattingEnabled = true;
            this.lstParameters.Items.AddRange(new object[] {
            "Lost to follow-up",
            "Attrition rate",
            ""});
            this.lstParameters.Location = new System.Drawing.Point(6, 23);
            this.lstParameters.Name = "lstParameters";
            this.lstParameters.Size = new System.Drawing.Size(178, 212);
            this.lstParameters.TabIndex = 26;
            // 
            // chkGeneralAssumption
            // 
            this.chkGeneralAssumption.AutoSize = true;
            this.chkGeneralAssumption.Checked = true;
            this.chkGeneralAssumption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGeneralAssumption.Location = new System.Drawing.Point(116, 238);
            this.chkGeneralAssumption.Name = "chkGeneralAssumption";
            this.chkGeneralAssumption.Size = new System.Drawing.Size(67, 17);
            this.chkGeneralAssumption.TabIndex = 25;
            this.chkGeneralAssumption.Text = "Is Active";
            this.chkGeneralAssumption.UseVisualStyleBackColor = true;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.rdbPosWiz4);
            this.panel17.Controls.Add(this.rdbNegWiz4);
            this.panel17.Location = new System.Drawing.Point(38, 175);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(346, 52);
            this.panel17.TabIndex = 24;
            // 
            // rdbPosWiz4
            // 
            this.rdbPosWiz4.AutoSize = true;
            this.rdbPosWiz4.Checked = true;
            this.rdbPosWiz4.Location = new System.Drawing.Point(78, 3);
            this.rdbPosWiz4.Name = "rdbPosWiz4";
            this.rdbPosWiz4.Size = new System.Drawing.Size(62, 17);
            this.rdbPosWiz4.TabIndex = 21;
            this.rdbPosWiz4.TabStop = true;
            this.rdbPosWiz4.Text = "Positive";
            this.rdbPosWiz4.UseVisualStyleBackColor = true;
            // 
            // rdbNegWiz4
            // 
            this.rdbNegWiz4.AutoSize = true;
            this.rdbNegWiz4.Location = new System.Drawing.Point(78, 28);
            this.rdbNegWiz4.Name = "rdbNegWiz4";
            this.rdbNegWiz4.Size = new System.Drawing.Size(68, 17);
            this.rdbNegWiz4.TabIndex = 22;
            this.rdbNegWiz4.Text = "Negative";
            this.rdbNegWiz4.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(12, 153);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(596, 19);
            this.label10.TabIndex = 20;
            this.label10.Text = "Variable Effect";
            // 
            // cobAssumptionTypeWiz4
            // 
            this.cobAssumptionTypeWiz4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobAssumptionTypeWiz4.Enabled = false;
            this.cobAssumptionTypeWiz4.FormattingEnabled = true;
            this.cobAssumptionTypeWiz4.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cobAssumptionTypeWiz4.Location = new System.Drawing.Point(116, 11);
            this.cobAssumptionTypeWiz4.Name = "cobAssumptionTypeWiz4";
            this.cobAssumptionTypeWiz4.Size = new System.Drawing.Size(492, 21);
            this.cobAssumptionTypeWiz4.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 14);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Assumption Type";
            // 
            // btnAddWiz4
            // 
            this.btnAddWiz4.Location = new System.Drawing.Point(528, 232);
            this.btnAddWiz4.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz4.Name = "btnAddWiz4";
            this.btnAddWiz4.Size = new System.Drawing.Size(80, 23);
            this.btnAddWiz4.TabIndex = 16;
            this.btnAddWiz4.Text = "Add";
            this.btnAddWiz4.UseVisualStyleBackColor = true;
            this.btnAddWiz4.Click += new System.EventHandler(this.btnAddWiz4_Click);
            // 
            // btnaddformulaWiz4
            // 
            this.btnaddformulaWiz4.Location = new System.Drawing.Point(506, 71);
            this.btnaddformulaWiz4.Name = "btnaddformulaWiz4";
            this.btnaddformulaWiz4.Size = new System.Drawing.Size(102, 23);
            this.btnaddformulaWiz4.TabIndex = 14;
            this.btnaddformulaWiz4.Text = "Define Formula";
            this.btnaddformulaWiz4.UseVisualStyleBackColor = true;
            this.btnaddformulaWiz4.Click += new System.EventHandler(this.btnaddformulaWiz4_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Variable Name";
            // 
            // rdbAggWiz4
            // 
            this.rdbAggWiz4.AutoSize = true;
            this.rdbAggWiz4.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggWiz4.Location = new System.Drawing.Point(116, 129);
            this.rdbAggWiz4.Name = "rdbAggWiz4";
            this.rdbAggWiz4.Size = new System.Drawing.Size(179, 17);
            this.rdbAggWiz4.TabIndex = 11;
            this.rdbAggWiz4.Text = "Use on Aggregate Site/Category";
            this.rdbAggWiz4.UseVisualStyleBackColor = true;
            this.rdbAggWiz4.Visible = false;
            // 
            // txtVariableNameWiz4
            // 
            this.txtVariableNameWiz4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVariableNameWiz4.Location = new System.Drawing.Point(116, 42);
            this.txtVariableNameWiz4.Name = "txtVariableNameWiz4";
            this.txtVariableNameWiz4.Size = new System.Drawing.Size(492, 20);
            this.txtVariableNameWiz4.TabIndex = 3;
            // 
            // rdbeachWiz4
            // 
            this.rdbeachWiz4.AutoSize = true;
            this.rdbeachWiz4.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachWiz4.Checked = true;
            this.rdbeachWiz4.Location = new System.Drawing.Point(116, 105);
            this.rdbeachWiz4.Name = "rdbeachWiz4";
            this.rdbeachWiz4.Size = new System.Drawing.Size(155, 17);
            this.rdbeachWiz4.TabIndex = 10;
            this.rdbeachWiz4.TabStop = true;
            this.rdbeachWiz4.Text = "Use on Each Site/Category";
            this.rdbeachWiz4.UseVisualStyleBackColor = true;
            this.rdbeachWiz4.Visible = false;
            // 
            // cobDatatypeWiz4
            // 
            this.cobDatatypeWiz4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobDatatypeWiz4.FormattingEnabled = true;
            this.cobDatatypeWiz4.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cobDatatypeWiz4.Location = new System.Drawing.Point(116, 73);
            this.cobDatatypeWiz4.Name = "cobDatatypeWiz4";
            this.cobDatatypeWiz4.Size = new System.Drawing.Size(383, 21);
            this.cobDatatypeWiz4.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Data Type";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 272);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(928, 281);
            this.panel9.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.lvgeneralassumptions);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(928, 281);
            this.panel13.TabIndex = 0;
            // 
            // lvgeneralassumptions
            // 
            this.lvgeneralassumptions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvgeneralassumptions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cc1,
            this.cc7,
            this.cc6,
            this.cc2,
            this.cc3,
            this.cc4,
            this.cc5});
            this.lvgeneralassumptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvgeneralassumptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvgeneralassumptions.FullRowSelect = true;
            this.lvgeneralassumptions.GridLines = true;
            this.lvgeneralassumptions.Location = new System.Drawing.Point(0, 0);
            this.lvgeneralassumptions.Name = "lvgeneralassumptions";
            this.lvgeneralassumptions.OwnerDraw = true;
            this.lvgeneralassumptions.Size = new System.Drawing.Size(928, 281);
            this.lvgeneralassumptions.TabIndex = 1;
            this.lvgeneralassumptions.UseCompatibleStateImageBehavior = false;
            this.lvgeneralassumptions.View = System.Windows.Forms.View.Details;
            this.lvgeneralassumptions.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvgeneralassumptions_DrawColumnHeader);
            this.lvgeneralassumptions.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvgeneralassumptions_DrawItem);
            this.lvgeneralassumptions.SelectedIndexChanged += new System.EventHandler(this.lvgeneralassumptions_SelectedIndexChanged);
            this.lvgeneralassumptions.Click += new System.EventHandler(this.lvgeneralassumptions_Click);
            // 
            // cc1
            // 
            this.cc1.Text = "No";
            this.cc1.Width = 33;
            // 
            // cc7
            // 
            this.cc7.DisplayIndex = 6;
            this.cc7.Text = "Active";
            // 
            // cc6
            // 
            this.cc6.DisplayIndex = 1;
            this.cc6.Text = "Type";
            // 
            // cc2
            // 
            this.cc2.DisplayIndex = 2;
            this.cc2.Text = "Data Type";
            this.cc2.Width = 76;
            // 
            // cc3
            // 
            this.cc3.DisplayIndex = 3;
            this.cc3.Text = "Variable Name";
            this.cc3.Width = 328;
            // 
            // cc4
            // 
            this.cc4.DisplayIndex = 4;
            this.cc4.Text = "Use on ";
            this.cc4.Width = 102;
            // 
            // cc5
            // 
            this.cc5.DisplayIndex = 5;
            this.cc5.Text = "Formula";
            this.cc5.Width = 124;
            // 
            // tabPatientGroup
            // 
            this.tabPatientGroup.Controls.Add(this.tableLayoutPanel3);
            this.tabPatientGroup.Location = new System.Drawing.Point(4, 25);
            this.tabPatientGroup.Name = "tabPatientGroup";
            this.tabPatientGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tabPatientGroup.Size = new System.Drawing.Size(940, 562);
            this.tabPatientGroup.TabIndex = 2;
            this.tabPatientGroup.Tag = "Patient/Population Group";
            this.tabPatientGroup.Text = "Patient/Population Group";
            this.tabPatientGroup.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(934, 556);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.chkActiveGroup);
            this.panel5.Controls.Add(this.btnAddWiz3);
            this.panel5.Controls.Add(this.txtgroupnameWiz3);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(928, 65);
            this.panel5.TabIndex = 0;
            // 
            // chkActiveGroup
            // 
            this.chkActiveGroup.AutoSize = true;
            this.chkActiveGroup.Checked = true;
            this.chkActiveGroup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActiveGroup.Location = new System.Drawing.Point(14, 39);
            this.chkActiveGroup.Name = "chkActiveGroup";
            this.chkActiveGroup.Size = new System.Drawing.Size(67, 17);
            this.chkActiveGroup.TabIndex = 24;
            this.chkActiveGroup.Text = "Is Active";
            this.chkActiveGroup.UseVisualStyleBackColor = true;
            // 
            // btnAddWiz3
            // 
            this.btnAddWiz3.Location = new System.Drawing.Point(808, 9);
            this.btnAddWiz3.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz3.Name = "btnAddWiz3";
            this.btnAddWiz3.Size = new System.Drawing.Size(80, 21);
            this.btnAddWiz3.TabIndex = 4;
            this.btnAddWiz3.Text = "Add";
            this.btnAddWiz3.UseVisualStyleBackColor = true;
            this.btnAddWiz3.Click += new System.EventHandler(this.btnAddWiz3_Click);
            // 
            // txtgroupnameWiz3
            // 
            this.txtgroupnameWiz3.Location = new System.Drawing.Point(102, 10);
            this.txtgroupnameWiz3.Margin = new System.Windows.Forms.Padding(1);
            this.txtgroupnameWiz3.Name = "txtgroupnameWiz3";
            this.txtgroupnameWiz3.Size = new System.Drawing.Size(703, 20);
            this.txtgroupnameWiz3.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Group Name";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lvGroupWiz3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 74);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(928, 479);
            this.panel6.TabIndex = 1;
            // 
            // lvGroupWiz3
            // 
            this.lvGroupWiz3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvGroupWiz3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader7,
            this.columnHeader6});
            this.lvGroupWiz3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvGroupWiz3.GridLines = true;
            this.lvGroupWiz3.Location = new System.Drawing.Point(0, 0);
            this.lvGroupWiz3.Name = "lvGroupWiz3";
            this.lvGroupWiz3.OwnerDraw = true;
            this.lvGroupWiz3.Size = new System.Drawing.Size(928, 479);
            this.lvGroupWiz3.TabIndex = 34;
            this.lvGroupWiz3.UseCompatibleStateImageBehavior = false;
            this.lvGroupWiz3.View = System.Windows.Forms.View.Details;
            this.lvGroupWiz3.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvGroupWiz3_DrawColumnHeader);
            this.lvGroupWiz3.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvGroupWiz3_DrawItem);
            this.lvGroupWiz3.SelectedIndexChanged += new System.EventHandler(this.lvGroupWiz3_SelectedIndexChanged);
            this.lvGroupWiz3.Click += new System.EventHandler(this.lvGroupWiz3_Click);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "No";
            this.columnHeader5.Width = 29;
            // 
            // columnHeader7
            // 
            this.columnHeader7.DisplayIndex = 2;
            this.columnHeader7.Text = "Active";
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 1;
            this.columnHeader6.Text = "Group Name";
            this.columnHeader6.Width = 681;
            // 
            // tabForecastMethod
            // 
            this.tabForecastMethod.Controls.Add(this.tableLayoutPanel2);
            this.tabForecastMethod.Location = new System.Drawing.Point(4, 25);
            this.tabForecastMethod.Name = "tabForecastMethod";
            this.tabForecastMethod.Padding = new System.Windows.Forms.Padding(3);
            this.tabForecastMethod.Size = new System.Drawing.Size(940, 562);
            this.tabForecastMethod.TabIndex = 1;
            this.tabForecastMethod.Tag = "Forecasting Method";
            this.tabForecastMethod.Text = "Forecasting Method";
            this.tabForecastMethod.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lvforecastParamsWiz2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel7, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 433F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(934, 556);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // lvforecastParamsWiz2
            // 
            this.lvforecastParamsWiz2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvforecastParamsWiz2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.c1,
            this.c7,
            this.c6,
            this.c2,
            this.c3,
            this.c4,
            this.c5});
            this.lvforecastParamsWiz2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvforecastParamsWiz2.FullRowSelect = true;
            this.lvforecastParamsWiz2.GridLines = true;
            this.lvforecastParamsWiz2.Location = new System.Drawing.Point(3, 45);
            this.lvforecastParamsWiz2.Name = "lvforecastParamsWiz2";
            this.lvforecastParamsWiz2.OwnerDraw = true;
            this.lvforecastParamsWiz2.Size = new System.Drawing.Size(928, 427);
            this.lvforecastParamsWiz2.TabIndex = 36;
            this.lvforecastParamsWiz2.UseCompatibleStateImageBehavior = false;
            this.lvforecastParamsWiz2.View = System.Windows.Forms.View.Details;
            this.lvforecastParamsWiz2.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvforecastParamsWiz2_DrawColumnHeader);
            this.lvforecastParamsWiz2.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvforecastParamsWiz2_DrawItem);
            this.lvforecastParamsWiz2.SelectedIndexChanged += new System.EventHandler(this.lvforecastParamsWiz2_SelectedIndexChanged);
            this.lvforecastParamsWiz2.Click += new System.EventHandler(this.lvforecastParamsWiz2_Click);
            // 
            // c1
            // 
            this.c1.Text = "No";
            this.c1.Width = 30;
            // 
            // c7
            // 
            this.c7.DisplayIndex = 6;
            this.c7.Text = "Active";
            this.c7.Width = 85;
            // 
            // c6
            // 
            this.c6.DisplayIndex = 1;
            this.c6.Text = "Method";
            this.c6.Width = 116;
            // 
            // c2
            // 
            this.c2.DisplayIndex = 2;
            this.c2.Text = "Data Type";
            this.c2.Width = 86;
            // 
            // c3
            // 
            this.c3.DisplayIndex = 3;
            this.c3.Text = "Variable Name";
            this.c3.Width = 252;
            // 
            // c4
            // 
            this.c4.DisplayIndex = 4;
            this.c4.Text = "Use on";
            this.c4.Width = 61;
            // 
            // c5
            // 
            this.c5.DisplayIndex = 5;
            this.c5.Text = "Formula";
            this.c5.Width = 165;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 478);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(928, 75);
            this.panel7.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkActive);
            this.panel2.Controls.Add(this.chkprimaryoutput);
            this.panel2.Controls.Add(this.panel15);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.btnAddWiz2);
            this.panel2.Controls.Add(this.btnaddformulaWiz2);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.rdbAggWiz2);
            this.panel2.Controls.Add(this.rdbeachWiz2);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.cobDatatypeWiz2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txtVariableNameWiz2);
            this.panel2.Location = new System.Drawing.Point(2, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(807, 260);
            this.panel2.TabIndex = 2;
            // 
            // chkActive
            // 
            this.chkActive.AutoSize = true;
            this.chkActive.Checked = true;
            this.chkActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkActive.Location = new System.Drawing.Point(113, 232);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(67, 17);
            this.chkActive.TabIndex = 23;
            this.chkActive.Text = "Is Active";
            this.chkActive.UseVisualStyleBackColor = true;
            // 
            // chkprimaryoutput
            // 
            this.chkprimaryoutput.AutoSize = true;
            this.chkprimaryoutput.Location = new System.Drawing.Point(611, 37);
            this.chkprimaryoutput.Name = "chkprimaryoutput";
            this.chkprimaryoutput.Size = new System.Drawing.Size(106, 17);
            this.chkprimaryoutput.TabIndex = 22;
            this.chkprimaryoutput.Text = "Is Primary Output";
            this.chkprimaryoutput.UseVisualStyleBackColor = true;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.rdbPosWiz2);
            this.panel15.Controls.Add(this.rdbNegWiz2);
            this.panel15.Location = new System.Drawing.Point(79, 172);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(147, 53);
            this.panel15.TabIndex = 21;
            // 
            // rdbPosWiz2
            // 
            this.rdbPosWiz2.AutoSize = true;
            this.rdbPosWiz2.Checked = true;
            this.rdbPosWiz2.Location = new System.Drawing.Point(34, 3);
            this.rdbPosWiz2.Name = "rdbPosWiz2";
            this.rdbPosWiz2.Size = new System.Drawing.Size(69, 17);
            this.rdbPosWiz2.TabIndex = 18;
            this.rdbPosWiz2.TabStop = true;
            this.rdbPosWiz2.Text = "Positively";
            this.rdbPosWiz2.UseVisualStyleBackColor = true;
            // 
            // rdbNegWiz2
            // 
            this.rdbNegWiz2.AutoSize = true;
            this.rdbNegWiz2.Location = new System.Drawing.Point(34, 28);
            this.rdbNegWiz2.Name = "rdbNegWiz2";
            this.rdbNegWiz2.Size = new System.Drawing.Size(75, 17);
            this.rdbNegWiz2.TabIndex = 19;
            this.rdbNegWiz2.Text = "Negatively";
            this.rdbNegWiz2.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(9, 150);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(721, 19);
            this.label9.TabIndex = 17;
            this.label9.Text = "Affect Primary Output";
            // 
            // btnAddWiz2
            // 
            this.btnAddWiz2.Location = new System.Drawing.Point(650, 232);
            this.btnAddWiz2.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddWiz2.Name = "btnAddWiz2";
            this.btnAddWiz2.Size = new System.Drawing.Size(80, 23);
            this.btnAddWiz2.TabIndex = 15;
            this.btnAddWiz2.Text = "Add";
            this.btnAddWiz2.UseVisualStyleBackColor = true;
            this.btnAddWiz2.Click += new System.EventHandler(this.btnAddWiz2_Click);
            // 
            // btnaddformulaWiz2
            // 
            this.btnaddformulaWiz2.Location = new System.Drawing.Point(492, 65);
            this.btnaddformulaWiz2.Name = "btnaddformulaWiz2";
            this.btnaddformulaWiz2.Size = new System.Drawing.Size(102, 23);
            this.btnaddformulaWiz2.TabIndex = 14;
            this.btnaddformulaWiz2.Text = "Define Formula";
            this.btnaddformulaWiz2.UseVisualStyleBackColor = true;
            this.btnaddformulaWiz2.Click += new System.EventHandler(this.btnaddformulaWiz2_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(6, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(721, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Variables";
            // 
            // rdbAggWiz2
            // 
            this.rdbAggWiz2.AutoSize = true;
            this.rdbAggWiz2.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggWiz2.Location = new System.Drawing.Point(113, 122);
            this.rdbAggWiz2.Name = "rdbAggWiz2";
            this.rdbAggWiz2.Size = new System.Drawing.Size(179, 17);
            this.rdbAggWiz2.TabIndex = 11;
            this.rdbAggWiz2.Text = "Use on Aggregate Site/Category";
            this.rdbAggWiz2.UseVisualStyleBackColor = true;
            // 
            // rdbeachWiz2
            // 
            this.rdbeachWiz2.AutoSize = true;
            this.rdbeachWiz2.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachWiz2.Checked = true;
            this.rdbeachWiz2.Location = new System.Drawing.Point(113, 99);
            this.rdbeachWiz2.Name = "rdbeachWiz2";
            this.rdbeachWiz2.Size = new System.Drawing.Size(155, 17);
            this.rdbeachWiz2.TabIndex = 10;
            this.rdbeachWiz2.TabStop = true;
            this.rdbeachWiz2.Text = "Use on Each Site/Category";
            this.rdbeachWiz2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Variable Name";
            // 
            // cobDatatypeWiz2
            // 
            this.cobDatatypeWiz2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobDatatypeWiz2.FormattingEnabled = true;
            this.cobDatatypeWiz2.Location = new System.Drawing.Point(113, 65);
            this.cobDatatypeWiz2.Name = "cobDatatypeWiz2";
            this.cobDatatypeWiz2.Size = new System.Drawing.Size(373, 21);
            this.cobDatatypeWiz2.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Data Type";
            // 
            // txtVariableNameWiz2
            // 
            this.txtVariableNameWiz2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVariableNameWiz2.Location = new System.Drawing.Point(113, 34);
            this.txtVariableNameWiz2.Name = "txtVariableNameWiz2";
            this.txtVariableNameWiz2.Size = new System.Drawing.Size(481, 20);
            this.txtVariableNameWiz2.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnForecastAdd);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.cobforecastmethodWiz2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(928, 36);
            this.panel4.TabIndex = 0;
            // 
            // btnForecastAdd
            // 
            this.btnForecastAdd.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnForecastAdd.Location = new System.Drawing.Point(833, 5);
            this.btnForecastAdd.Name = "btnForecastAdd";
            this.btnForecastAdd.Size = new System.Drawing.Size(75, 23);
            this.btnForecastAdd.TabIndex = 2;
            this.btnForecastAdd.Text = "Add";
            this.btnForecastAdd.UseVisualStyleBackColor = false;
            this.btnForecastAdd.Visible = false;
            this.btnForecastAdd.Click += new System.EventHandler(this.btnForecastAdd_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Forecast Method";
            // 
            // cobforecastmethodWiz2
            // 
            this.cobforecastmethodWiz2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cobforecastmethodWiz2.FormattingEnabled = true;
            this.cobforecastmethodWiz2.Location = new System.Drawing.Point(113, 7);
            this.cobforecastmethodWiz2.Margin = new System.Windows.Forms.Padding(2);
            this.cobforecastmethodWiz2.Name = "cobforecastmethodWiz2";
            this.cobforecastmethodWiz2.Size = new System.Drawing.Size(714, 21);
            this.cobforecastmethodWiz2.TabIndex = 1;
            this.cobforecastmethodWiz2.SelectedIndexChanged += new System.EventHandler(this.cobforecastmethodWiz2_SelectedIndexChanged);
            // 
            // tabDefprogram
            // 
            this.tabDefprogram.Controls.Add(this.tableLayoutPanel1);
            this.tabDefprogram.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabDefprogram.Location = new System.Drawing.Point(4, 25);
            this.tabDefprogram.Name = "tabDefprogram";
            this.tabDefprogram.Padding = new System.Windows.Forms.Padding(3);
            this.tabDefprogram.Size = new System.Drawing.Size(940, 562);
            this.tabDefprogram.TabIndex = 0;
            this.tabDefprogram.Tag = "Define Program";
            this.tabDefprogram.Text = "Define Program";
            this.tabDefprogram.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel12, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(934, 556);
            this.tableLayoutPanel1.TabIndex = 2;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnProgramAdd);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.txtProgramName);
            this.panel11.Location = new System.Drawing.Point(3, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(875, 53);
            this.panel11.TabIndex = 0;
            // 
            // btnProgramAdd
            // 
            this.btnProgramAdd.Location = new System.Drawing.Point(741, 9);
            this.btnProgramAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnProgramAdd.Name = "btnProgramAdd";
            this.btnProgramAdd.Size = new System.Drawing.Size(80, 21);
            this.btnProgramAdd.TabIndex = 4;
            this.btnProgramAdd.Text = "Add";
            this.btnProgramAdd.UseVisualStyleBackColor = true;
            this.btnProgramAdd.Click += new System.EventHandler(this.btnProgramAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Program Name";
            // 
            // txtProgramName
            // 
            this.txtProgramName.Location = new System.Drawing.Point(102, 10);
            this.txtProgramName.Margin = new System.Windows.Forms.Padding(1);
            this.txtProgramName.Name = "txtProgramName";
            this.txtProgramName.Size = new System.Drawing.Size(628, 20);
            this.txtProgramName.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.lvProgramList);
            this.panel12.Location = new System.Drawing.Point(3, 62);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(924, 425);
            this.panel12.TabIndex = 1;
            // 
            // lvProgramList
            // 
            this.lvProgramList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvProgramList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colNo,
            this.colprogramname});
            this.lvProgramList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvProgramList.FullRowSelect = true;
            this.lvProgramList.GridLines = true;
            this.lvProgramList.Location = new System.Drawing.Point(0, 0);
            this.lvProgramList.Name = "lvProgramList";
            this.lvProgramList.OwnerDraw = true;
            this.lvProgramList.Size = new System.Drawing.Size(924, 425);
            this.lvProgramList.TabIndex = 34;
            this.lvProgramList.UseCompatibleStateImageBehavior = false;
            this.lvProgramList.View = System.Windows.Forms.View.Details;
            this.lvProgramList.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvProgramList_DrawColumnHeader);
            this.lvProgramList.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvProgramList_DrawItem);
            this.lvProgramList.SelectedIndexChanged += new System.EventHandler(this.lvProgramList_SelectedIndexChanged);
            // 
            // colNo
            // 
            this.colNo.Text = "No";
            this.colNo.Width = 29;
            // 
            // colprogramname
            // 
            this.colprogramname.Text = "Program Name";
            this.colprogramname.Width = 686;
            // 
            // rdbPosProduct
            // 
            this.rdbPosProduct.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.rdbPosProduct.Controls.Add(this.tabDefprogram);
            this.rdbPosProduct.Controls.Add(this.tabForecastMethod);
            this.rdbPosProduct.Controls.Add(this.tabPatientGroup);
            this.rdbPosProduct.Controls.Add(this.tabPatientAssumption);
            this.rdbPosProduct.Controls.Add(this.tabTestingAssumption);
            this.rdbPosProduct.Controls.Add(this.tabProductAssumption);
            this.rdbPosProduct.Controls.Add(this.tabTestingProtocol);
            this.rdbPosProduct.Controls.Add(this.tabReview);
            this.rdbPosProduct.Location = new System.Drawing.Point(0, 5);
            this.rdbPosProduct.Multiline = true;
            this.rdbPosProduct.Name = "rdbPosProduct";
            this.rdbPosProduct.SelectedIndex = 0;
            this.rdbPosProduct.Size = new System.Drawing.Size(948, 591);
            this.rdbPosProduct.TabIndex = 0;
            this.rdbPosProduct.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.rdbPosProduct.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            this.rdbPosProduct.MouseDown += new System.Windows.Forms.MouseEventHandler(this.rdbPosProduct_MouseDown);
            // 
            // tabTestingAssumption
            // 
            this.tabTestingAssumption.Controls.Add(this.groupBox2);
            this.tabTestingAssumption.Controls.Add(this.lvTestingAssumption);
            this.tabTestingAssumption.Controls.Add(this.chkTestingAssumption);
            this.tabTestingAssumption.Controls.Add(this.panel10);
            this.tabTestingAssumption.Controls.Add(this.label20);
            this.tabTestingAssumption.Controls.Add(this.button4);
            this.tabTestingAssumption.Controls.Add(this.button5);
            this.tabTestingAssumption.Controls.Add(this.rdbAggTesting);
            this.tabTestingAssumption.Controls.Add(this.rdbeachTesting);
            this.tabTestingAssumption.Controls.Add(this.cboTestingDatatype);
            this.tabTestingAssumption.Controls.Add(this.label21);
            this.tabTestingAssumption.Controls.Add(this.cboTestingAssumption);
            this.tabTestingAssumption.Controls.Add(this.label12);
            this.tabTestingAssumption.Controls.Add(this.label19);
            this.tabTestingAssumption.Controls.Add(this.txtTVariableName);
            this.tabTestingAssumption.Location = new System.Drawing.Point(4, 25);
            this.tabTestingAssumption.Name = "tabTestingAssumption";
            this.tabTestingAssumption.Padding = new System.Windows.Forms.Padding(3);
            this.tabTestingAssumption.Size = new System.Drawing.Size(940, 562);
            this.tabTestingAssumption.TabIndex = 6;
            this.tabTestingAssumption.Tag = "Testing Assumption";
            this.tabTestingAssumption.Text = "Testing Assumptions";
            this.tabTestingAssumption.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Location = new System.Drawing.Point(708, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(216, 265);
            this.groupBox2.TabIndex = 37;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Possible testing related assumptions ";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "% of Repeat ",
            "% if Symptom Directed Test"});
            this.listBox1.Location = new System.Drawing.Point(6, 23);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(196, 238);
            this.listBox1.TabIndex = 26;
            // 
            // lvTestingAssumption
            // 
            this.lvTestingAssumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvTestingAssumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39});
            this.lvTestingAssumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvTestingAssumption.FullRowSelect = true;
            this.lvTestingAssumption.GridLines = true;
            this.lvTestingAssumption.Location = new System.Drawing.Point(20, 285);
            this.lvTestingAssumption.Name = "lvTestingAssumption";
            this.lvTestingAssumption.OwnerDraw = true;
            this.lvTestingAssumption.Size = new System.Drawing.Size(914, 231);
            this.lvTestingAssumption.TabIndex = 36;
            this.lvTestingAssumption.UseCompatibleStateImageBehavior = false;
            this.lvTestingAssumption.View = System.Windows.Forms.View.Details;
            this.lvTestingAssumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listView3_DrawColumnHeader);
            this.lvTestingAssumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView3_DrawItem);
            this.lvTestingAssumption.Click += new System.EventHandler(this.lvTestingAssumption_Click);
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "No";
            this.columnHeader26.Width = 33;
            // 
            // columnHeader27
            // 
            this.columnHeader27.DisplayIndex = 6;
            this.columnHeader27.Text = "Active";
            // 
            // columnHeader35
            // 
            this.columnHeader35.DisplayIndex = 1;
            this.columnHeader35.Text = "Type";
            // 
            // columnHeader36
            // 
            this.columnHeader36.DisplayIndex = 2;
            this.columnHeader36.Text = "Data Type";
            this.columnHeader36.Width = 76;
            // 
            // columnHeader37
            // 
            this.columnHeader37.DisplayIndex = 3;
            this.columnHeader37.Text = "Variable Name";
            this.columnHeader37.Width = 176;
            // 
            // columnHeader38
            // 
            this.columnHeader38.DisplayIndex = 4;
            this.columnHeader38.Text = "Use on ";
            this.columnHeader38.Width = 62;
            // 
            // columnHeader39
            // 
            this.columnHeader39.DisplayIndex = 5;
            this.columnHeader39.Text = "Formula";
            this.columnHeader39.Width = 71;
            // 
            // chkTestingAssumption
            // 
            this.chkTestingAssumption.AutoSize = true;
            this.chkTestingAssumption.Checked = true;
            this.chkTestingAssumption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestingAssumption.Location = new System.Drawing.Point(126, 249);
            this.chkTestingAssumption.Name = "chkTestingAssumption";
            this.chkTestingAssumption.Size = new System.Drawing.Size(67, 17);
            this.chkTestingAssumption.TabIndex = 34;
            this.chkTestingAssumption.Text = "Is Active";
            this.chkTestingAssumption.UseVisualStyleBackColor = true;
            this.chkTestingAssumption.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.rdbPosTesting);
            this.panel10.Controls.Add(this.rdbNegTesting);
            this.panel10.Location = new System.Drawing.Point(47, 186);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(346, 52);
            this.panel10.TabIndex = 33;
            this.panel10.Paint += new System.Windows.Forms.PaintEventHandler(this.panel10_Paint);
            // 
            // rdbPosTesting
            // 
            this.rdbPosTesting.AutoSize = true;
            this.rdbPosTesting.Checked = true;
            this.rdbPosTesting.Location = new System.Drawing.Point(78, 3);
            this.rdbPosTesting.Name = "rdbPosTesting";
            this.rdbPosTesting.Size = new System.Drawing.Size(62, 17);
            this.rdbPosTesting.TabIndex = 21;
            this.rdbPosTesting.TabStop = true;
            this.rdbPosTesting.Text = "Positive";
            this.rdbPosTesting.UseVisualStyleBackColor = true;
            // 
            // rdbNegTesting
            // 
            this.rdbNegTesting.AutoSize = true;
            this.rdbNegTesting.Location = new System.Drawing.Point(78, 28);
            this.rdbNegTesting.Name = "rdbNegTesting";
            this.rdbNegTesting.Size = new System.Drawing.Size(68, 17);
            this.rdbNegTesting.TabIndex = 22;
            this.rdbNegTesting.Text = "Negative";
            this.rdbNegTesting.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label20.ForeColor = System.Drawing.Color.DimGray;
            this.label20.Location = new System.Drawing.Point(22, 164);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(721, 19);
            this.label20.TabIndex = 32;
            this.label20.Text = "Variable Effect";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(534, 245);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(80, 23);
            this.button4.TabIndex = 31;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btnAddWiz4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(512, 81);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(102, 23);
            this.button5.TabIndex = 30;
            this.button5.Text = "Define Formula";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.btnaddformulaWiz4_Click);
            // 
            // rdbAggTesting
            // 
            this.rdbAggTesting.AutoSize = true;
            this.rdbAggTesting.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggTesting.Location = new System.Drawing.Point(126, 140);
            this.rdbAggTesting.Name = "rdbAggTesting";
            this.rdbAggTesting.Size = new System.Drawing.Size(179, 17);
            this.rdbAggTesting.TabIndex = 29;
            this.rdbAggTesting.Text = "Use on Aggregate Site/Category";
            this.rdbAggTesting.UseVisualStyleBackColor = true;
            this.rdbAggTesting.Visible = false;
            this.rdbAggTesting.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // rdbeachTesting
            // 
            this.rdbeachTesting.AutoSize = true;
            this.rdbeachTesting.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachTesting.Checked = true;
            this.rdbeachTesting.Location = new System.Drawing.Point(126, 116);
            this.rdbeachTesting.Name = "rdbeachTesting";
            this.rdbeachTesting.Size = new System.Drawing.Size(155, 17);
            this.rdbeachTesting.TabIndex = 28;
            this.rdbeachTesting.TabStop = true;
            this.rdbeachTesting.Text = "Use on Each Site/Category";
            this.rdbeachTesting.UseVisualStyleBackColor = true;
            this.rdbeachTesting.Visible = false;
            this.rdbeachTesting.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // cboTestingDatatype
            // 
            this.cboTestingDatatype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTestingDatatype.FormattingEnabled = true;
            this.cboTestingDatatype.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cboTestingDatatype.Location = new System.Drawing.Point(122, 81);
            this.cboTestingDatatype.Name = "cboTestingDatatype";
            this.cboTestingDatatype.Size = new System.Drawing.Size(383, 21);
            this.cboTestingDatatype.TabIndex = 26;
            this.cboTestingDatatype.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(17, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 13);
            this.label21.TabIndex = 27;
            this.label21.Text = "Data Type";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // cboTestingAssumption
            // 
            this.cboTestingAssumption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTestingAssumption.Enabled = false;
            this.cboTestingAssumption.FormattingEnabled = true;
            this.cboTestingAssumption.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cboTestingAssumption.Location = new System.Drawing.Point(122, 14);
            this.cboTestingAssumption.Name = "cboTestingAssumption";
            this.cboTestingAssumption.Size = new System.Drawing.Size(492, 21);
            this.cboTestingAssumption.TabIndex = 21;
            this.cboTestingAssumption.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 17);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Assumption Type";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Variable Name";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // txtTVariableName
            // 
            this.txtTVariableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTVariableName.Location = new System.Drawing.Point(122, 45);
            this.txtTVariableName.Name = "txtTVariableName";
            this.txtTVariableName.Size = new System.Drawing.Size(492, 20);
            this.txtTVariableName.TabIndex = 20;
            this.txtTVariableName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tabProductAssumption
            // 
            this.tabProductAssumption.Controls.Add(this.groupBox3);
            this.tabProductAssumption.Controls.Add(this.lvProductAssumption);
            this.tabProductAssumption.Controls.Add(this.chkProductAssumption);
            this.tabProductAssumption.Controls.Add(this.panel16);
            this.tabProductAssumption.Controls.Add(this.label25);
            this.tabProductAssumption.Controls.Add(this.button7);
            this.tabProductAssumption.Controls.Add(this.rdbAggProduct);
            this.tabProductAssumption.Controls.Add(this.rdbeachProduct);
            this.tabProductAssumption.Controls.Add(this.button6);
            this.tabProductAssumption.Controls.Add(this.cboProductDatatype);
            this.tabProductAssumption.Controls.Add(this.label22);
            this.tabProductAssumption.Controls.Add(this.cboProductassuption);
            this.tabProductAssumption.Controls.Add(this.label23);
            this.tabProductAssumption.Controls.Add(this.label24);
            this.tabProductAssumption.Controls.Add(this.txtProductVariableName);
            this.tabProductAssumption.Location = new System.Drawing.Point(4, 25);
            this.tabProductAssumption.Name = "tabProductAssumption";
            this.tabProductAssumption.Padding = new System.Windows.Forms.Padding(3);
            this.tabProductAssumption.Size = new System.Drawing.Size(940, 562);
            this.tabProductAssumption.TabIndex = 7;
            this.tabProductAssumption.Tag = "Product Assumption";
            this.tabProductAssumption.Text = "Product Assumptions";
            this.tabProductAssumption.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBox2);
            this.groupBox3.Location = new System.Drawing.Point(703, 20);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 265);
            this.groupBox3.TabIndex = 45;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Possible product related assumptions ";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Items.AddRange(new object[] {
            "% of Repeat ",
            "% if Symptom Directed Test"});
            this.listBox2.Location = new System.Drawing.Point(6, 23);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(201, 238);
            this.listBox2.TabIndex = 26;
            // 
            // lvProductAssumption
            // 
            this.lvProductAssumption.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lvProductAssumption.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34});
            this.lvProductAssumption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvProductAssumption.FullRowSelect = true;
            this.lvProductAssumption.GridLines = true;
            this.lvProductAssumption.Location = new System.Drawing.Point(20, 301);
            this.lvProductAssumption.Name = "lvProductAssumption";
            this.lvProductAssumption.OwnerDraw = true;
            this.lvProductAssumption.Size = new System.Drawing.Size(901, 215);
            this.lvProductAssumption.TabIndex = 44;
            this.lvProductAssumption.UseCompatibleStateImageBehavior = false;
            this.lvProductAssumption.View = System.Windows.Forms.View.Details;
            this.lvProductAssumption.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.lvProductAssumption_DrawColumnHeader);
            this.lvProductAssumption.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lvProductAssumption_DrawItem);
            this.lvProductAssumption.SelectedIndexChanged += new System.EventHandler(this.lvProductAssumption_SelectedIndexChanged);
            this.lvProductAssumption.Click += new System.EventHandler(this.lvProductAssumption_Click);
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "No";
            this.columnHeader28.Width = 33;
            // 
            // columnHeader29
            // 
            this.columnHeader29.DisplayIndex = 6;
            this.columnHeader29.Text = "Active";
            // 
            // columnHeader30
            // 
            this.columnHeader30.DisplayIndex = 1;
            this.columnHeader30.Text = "Type";
            // 
            // columnHeader31
            // 
            this.columnHeader31.DisplayIndex = 2;
            this.columnHeader31.Text = "Data Type";
            this.columnHeader31.Width = 76;
            // 
            // columnHeader32
            // 
            this.columnHeader32.DisplayIndex = 3;
            this.columnHeader32.Text = "Variable Name";
            this.columnHeader32.Width = 328;
            // 
            // columnHeader33
            // 
            this.columnHeader33.DisplayIndex = 4;
            this.columnHeader33.Text = "Use on ";
            this.columnHeader33.Width = 102;
            // 
            // columnHeader34
            // 
            this.columnHeader34.DisplayIndex = 5;
            this.columnHeader34.Text = "Formula";
            this.columnHeader34.Width = 124;
            // 
            // chkProductAssumption
            // 
            this.chkProductAssumption.AutoSize = true;
            this.chkProductAssumption.Checked = true;
            this.chkProductAssumption.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProductAssumption.Location = new System.Drawing.Point(126, 270);
            this.chkProductAssumption.Name = "chkProductAssumption";
            this.chkProductAssumption.Size = new System.Drawing.Size(67, 17);
            this.chkProductAssumption.TabIndex = 43;
            this.chkProductAssumption.Text = "Is Active";
            this.chkProductAssumption.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.rdbPosProduct1);
            this.panel16.Controls.Add(this.rdbNegProduct);
            this.panel16.Location = new System.Drawing.Point(48, 207);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(346, 52);
            this.panel16.TabIndex = 42;
            // 
            // rdbPosProduct1
            // 
            this.rdbPosProduct1.AutoSize = true;
            this.rdbPosProduct1.Checked = true;
            this.rdbPosProduct1.Location = new System.Drawing.Point(78, 3);
            this.rdbPosProduct1.Name = "rdbPosProduct1";
            this.rdbPosProduct1.Size = new System.Drawing.Size(62, 17);
            this.rdbPosProduct1.TabIndex = 21;
            this.rdbPosProduct1.TabStop = true;
            this.rdbPosProduct1.Text = "Positive";
            this.rdbPosProduct1.UseVisualStyleBackColor = true;
            // 
            // rdbNegProduct
            // 
            this.rdbNegProduct.AutoSize = true;
            this.rdbNegProduct.Location = new System.Drawing.Point(78, 28);
            this.rdbNegProduct.Name = "rdbNegProduct";
            this.rdbNegProduct.Size = new System.Drawing.Size(68, 17);
            this.rdbNegProduct.TabIndex = 22;
            this.rdbNegProduct.Text = "Negative";
            this.rdbNegProduct.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label25.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World);
            this.label25.ForeColor = System.Drawing.Color.DimGray;
            this.label25.Location = new System.Drawing.Point(22, 185);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(721, 19);
            this.label25.TabIndex = 41;
            this.label25.Text = "Variable Effect";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(542, 264);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 23);
            this.button7.TabIndex = 40;
            this.button7.Text = "Add";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.btnAddWiz4_Click);
            // 
            // rdbAggProduct
            // 
            this.rdbAggProduct.AutoSize = true;
            this.rdbAggProduct.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbAggProduct.Location = new System.Drawing.Point(126, 161);
            this.rdbAggProduct.Name = "rdbAggProduct";
            this.rdbAggProduct.Size = new System.Drawing.Size(179, 17);
            this.rdbAggProduct.TabIndex = 39;
            this.rdbAggProduct.Text = "Use on Aggregate Site/Category";
            this.rdbAggProduct.UseVisualStyleBackColor = true;
            this.rdbAggProduct.Visible = false;
            // 
            // rdbeachProduct
            // 
            this.rdbeachProduct.AutoSize = true;
            this.rdbeachProduct.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rdbeachProduct.Checked = true;
            this.rdbeachProduct.Location = new System.Drawing.Point(126, 137);
            this.rdbeachProduct.Name = "rdbeachProduct";
            this.rdbeachProduct.Size = new System.Drawing.Size(155, 17);
            this.rdbeachProduct.TabIndex = 38;
            this.rdbeachProduct.TabStop = true;
            this.rdbeachProduct.Text = "Use on Each Site/Category";
            this.rdbeachProduct.UseVisualStyleBackColor = true;
            this.rdbeachProduct.Visible = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(520, 85);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(102, 23);
            this.button6.TabIndex = 37;
            this.button6.Text = "Define Formula";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.btnaddformulaWiz4_Click);
            // 
            // cboProductDatatype
            // 
            this.cboProductDatatype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductDatatype.FormattingEnabled = true;
            this.cboProductDatatype.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cboProductDatatype.Location = new System.Drawing.Point(126, 87);
            this.cboProductDatatype.Name = "cboProductDatatype";
            this.cboProductDatatype.Size = new System.Drawing.Size(387, 21);
            this.cboProductDatatype.TabIndex = 35;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(21, 90);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 13);
            this.label22.TabIndex = 36;
            this.label22.Text = "Data Type";
            // 
            // cboProductassuption
            // 
            this.cboProductassuption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboProductassuption.Enabled = false;
            this.cboProductassuption.FormattingEnabled = true;
            this.cboProductassuption.Items.AddRange(new object[] {
            "Numeric",
            "Percentage"});
            this.cboProductassuption.Location = new System.Drawing.Point(126, 20);
            this.cboProductassuption.Name = "cboProductassuption";
            this.cboProductassuption.Size = new System.Drawing.Size(496, 21);
            this.cboProductassuption.TabIndex = 33;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(21, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 13);
            this.label23.TabIndex = 34;
            this.label23.Text = "Assumption Type";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(21, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 13);
            this.label24.TabIndex = 31;
            this.label24.Text = "Variable Name";
            // 
            // txtProductVariableName
            // 
            this.txtProductVariableName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProductVariableName.Location = new System.Drawing.Point(126, 51);
            this.txtProductVariableName.Name = "txtProductVariableName";
            this.txtProductVariableName.Size = new System.Drawing.Size(496, 20);
            this.txtProductVariableName.TabIndex = 32;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // FrmMorbidityParameter_new1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 642);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.rdbPosProduct);
            this.Name = "FrmMorbidityParameter_new1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Demographic Forecasting Method Parameters";
            this.panel1.ResumeLayout(false);
            this.tabReview.ResumeLayout(false);
            this.tabReview.PerformLayout();
            this.tabTestingProtocol.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.tabPatientAssumption.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.tabPatientGroup.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.tabForecastMethod.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabDefprogram.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.rdbPosProduct.ResumeLayout(false);
            this.tabTestingAssumption.ResumeLayout(false);
            this.tabTestingAssumption.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabProductAssumption.ResumeLayout(false);
            this.tabProductAssumption.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TabPage tabReview;
        private System.Windows.Forms.Label lbltestingprotocol1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView lstReviewGeneralAssumption;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ListView lstReviewPatientGroup;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ListView lstReviewForecastingVariable;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblprogram1;
        private System.Windows.Forms.Label lblProgram;
        private System.Windows.Forms.TabPage tabTestingProtocol;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkrapidprotocol;
        private System.Windows.Forms.CheckBox chkgeneralprotocol;
        private System.Windows.Forms.TabPage tabPatientAssumption;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.CheckBox chkGeneralAssumption;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RadioButton rdbPosWiz4;
        private System.Windows.Forms.RadioButton rdbNegWiz4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cobAssumptionTypeWiz4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnAddWiz4;
        private System.Windows.Forms.Button btnaddformulaWiz4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rdbAggWiz4;
        private System.Windows.Forms.TextBox txtVariableNameWiz4;
        private System.Windows.Forms.RadioButton rdbeachWiz4;
        private System.Windows.Forms.ComboBox cobDatatypeWiz4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.ListView lvgeneralassumptions;
        private System.Windows.Forms.ColumnHeader cc1;
        private System.Windows.Forms.ColumnHeader cc7;
        private System.Windows.Forms.ColumnHeader cc6;
        private System.Windows.Forms.ColumnHeader cc2;
        private System.Windows.Forms.ColumnHeader cc3;
        private System.Windows.Forms.ColumnHeader cc4;
        private System.Windows.Forms.ColumnHeader cc5;
        private System.Windows.Forms.TabPage tabPatientGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox chkActiveGroup;
        private System.Windows.Forms.Button btnAddWiz3;
        private System.Windows.Forms.TextBox txtgroupnameWiz3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ListView lvGroupWiz3;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.TabPage tabForecastMethod;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ListView lvforecastParamsWiz2;
        private System.Windows.Forms.ColumnHeader c1;
        private System.Windows.Forms.ColumnHeader c7;
        private System.Windows.Forms.ColumnHeader c6;
        private System.Windows.Forms.ColumnHeader c2;
        private System.Windows.Forms.ColumnHeader c3;
        private System.Windows.Forms.ColumnHeader c4;
        private System.Windows.Forms.ColumnHeader c5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkActive;
        private System.Windows.Forms.CheckBox chkprimaryoutput;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RadioButton rdbPosWiz2;
        private System.Windows.Forms.RadioButton rdbNegWiz2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnAddWiz2;
        private System.Windows.Forms.Button btnaddformulaWiz2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdbAggWiz2;
        private System.Windows.Forms.RadioButton rdbeachWiz2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cobDatatypeWiz2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVariableNameWiz2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cobforecastmethodWiz2;
        private System.Windows.Forms.TabPage tabDefprogram;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnProgramAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProgramName;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.ListView lvProgramList;
        private System.Windows.Forms.ColumnHeader colNo;
        private System.Windows.Forms.ColumnHeader colprogramname;
        private System.Windows.Forms.TabControl rdbPosProduct;
        private System.Windows.Forms.TabPage tabTestingAssumption;
        private System.Windows.Forms.TabPage tabProductAssumption;
        private System.Windows.Forms.CheckBox chkTestingAssumption;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.RadioButton rdbPosTesting;
        private System.Windows.Forms.RadioButton rdbNegTesting;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.RadioButton rdbAggTesting;
        private System.Windows.Forms.RadioButton rdbeachTesting;
        private System.Windows.Forms.ComboBox cboTestingDatatype;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboTestingAssumption;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTVariableName;
        private System.Windows.Forms.ListView lvProductAssumption;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.CheckBox chkProductAssumption;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.RadioButton rdbPosProduct1;
        private System.Windows.Forms.RadioButton rdbNegProduct;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.RadioButton rdbAggProduct;
        private System.Windows.Forms.RadioButton rdbeachProduct;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox cboProductDatatype;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cboProductassuption;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtProductVariableName;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.ListView lvTestingAssumption;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ListView lstReviewTestingassumption;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ListView lstReviewProductAssumption;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.RadioButton rbgeneralprotocol;
        private System.Windows.Forms.RadioButton rbrapidprotocol;
        private System.Windows.Forms.ComboBox cboYear;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnTPA;
        private System.Windows.Forms.Button btnForecastAdd;
        private System.Windows.Forms.ListBox lstParameters;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox listBox2;

    }
}