﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.UserExceptions;
using LQT.Core.Util;
using LQT.Core.Domain;
using System.Globalization;
using System.Data.SqlClient;
namespace LQT.GUI.MorbidityUserCtr
{
    public partial class FrmMorbidityParameter_new1 : Form
    {
        private MMProgram _mMProgram;
        private IList<MMProgram> _mMPrograms;
        private FrmMorbidityParameter_new1 _FrmMorbidityParameter_new1;
        // private InstrumentPane _rPane;
        private Form _mdiparent;
        private MMForecastParameter _mMForecastParam;
        private IList<MMForecastParameter> _mMForecastParams;

        private MMGroup _mMGroup;
        private IList<MMGroup> _mMGroups;

        private MMGeneralAssumption _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mMGeneralAssumptions;
        private string _currentTab = "Define Program";
        private bool Flag;
        string[] _parameterName = new string[2];
        public SqlConnection sqlConnection = ConnectionManager.GetInstance().GetSqlConnection();
        public FrmMorbidityParameter_new1(Form mdiparent)
        {
            InitializeComponent();
            _mdiparent = mdiparent;
            BindMMPrograms();
            fontBold();
        }

        private void btnProgramAdd_Click(object sender, EventArgs e)
        {
            bool a;
           a= SaveOrUpdateObject();
         
            BindMMPrograms();
            txtProgramName.Text = string.Empty;
            ((LqtMainWindowForm)_mdiparent).BuildNavigationMenu();
           // ((LqtMainWindowForm)_mdiparent).BuildSettingsMenu();
        }
        public void SetColors(ListView lst)
        {
            foreach (ListViewItem item in lst.Items)
                item.BackColor = (item.Index % 2 == 0) ? Color.GhostWhite : Color.WhiteSmoke;
        }
        private void BindMMPrograms()
        {
            _mMPrograms = DataRepository.GetMMPrograms();
            int i = 1;
            lvProgramList.BeginUpdate();
            lvProgramList.Items.Clear();

            foreach (MMProgram mMprogram in _mMPrograms)
            {
                ListViewItem listViewItem = new ListViewItem(i.ToString())
                {
                    Tag = mMprogram
                };
                listViewItem.SubItems.Add(mMprogram.ProgramName);
                listViewItem.SubItems.Add(mMprogram.Description);

                lvProgramList.Items.Add(listViewItem);
                i++;
            }
            lvProgramList.EndUpdate();
            SetColors(lvProgramList);
        }
        private bool SaveOrUpdateObject()
        {

            foreach (MMProgram mMprogram in _mMPrograms)
            {



                if (mMprogram.ProgramName == this.txtProgramName.Text.Trim())
                {

                    MessageBox.Show("Program Name must not be duplicate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }
            if (_mMProgram == null)
                _mMProgram = new MMProgram();

            if (txtProgramName.Text.Trim() == string.Empty)
                throw new LQTUserException("Program Name can not be empty.");

            DataRepository.CloseSession();
            _mMProgram.ProgramName = this.txtProgramName.Text.Trim();

            DataRepository.SaveOrUpdateMMProgram(_mMProgram);

            _mMProgram = null;
            return true;

        }

        private void btnAddWiz2_Click(object sender, EventArgs e)
        {
           
           // Flag = DatabaseManager.AlterTables("0");
           // string[] _parameterName = { "Current Patient", "Target Patient" };
            string[] _parameterName = { "Current Patient on Treatment", "Target Patient" };
            //Flag = SaveOrUpdateMMForecastParameterObject();
            Flag = SaveOrUpdateMMForecastParameterObject1(_parameterName);
            BindMMForecastParameters();
            BindForecastMethod();
            BindVariablesDataType();
            txtVariableNameWiz2.Text = string.Empty;
        }
        private bool SaveOrUpdateMMForecastParameterObject()
        {
            string _sql = "";
            bool result;
             TextInfo t = new CultureInfo("en-US", false).TextInfo;
             string UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
            {

             

                if (mMFParam.VariableName == t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) && mMFParam.UseOn == UseOn)
                {

                    MessageBox.Show("Variable Name must not be duplicate for " + UseOn + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

           
            if (_mMForecastParam == null)
                _mMForecastParam = new MMForecastParameter();

            _mMForecastParam.ForecastMethod = (int)Enum.Parse(typeof(MorbidityForecastingMethod), cobforecastmethodWiz2.Text.Trim().Replace(' ', '_'));
           
            _mMForecastParam.VariableName = t.ToTitleCase(txtVariableNameWiz2.Text.Trim());

            _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz2.Text.Trim());
            _mMForecastParam.UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

            _mMForecastParam.MMProgram = _mMProgram;
            _mMForecastParam.VarCode = LqtUtil.GetUpperCases(_mMForecastParam.VariableName);

            _mMForecastParam.IsPrimaryOutput = chkprimaryoutput.Checked;

            _mMForecastParam.VariableEffect = rdbPosWiz2.Checked ? true : false;
            _mMForecastParam.IsActive = chkActive.Checked;
            _mMProgram.MMForecastParameters.Add(_mMForecastParam);



            DataRepository.SaveOrUpdateMMForecastParameter(_mMForecastParam);

            if (UseOn == "OnEachSite")
            {
                _sql = "IF COL_LENGTH('ForecastSiteInfo','[" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "]') IS NULL BEGIN  alter table ForecastSiteInfo add  [" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "] numeric(18, 2)  END ";
            }
            else
            {
                _sql = "IF COL_LENGTH('ForecastCategoryInfo','[" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "]') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "] numeric(18, 2)  END ";
            }
            result = DatabaseManager.AlterTables(_sql);
            BindMMForecastParameters();
            _mMForecastParam = null;
            return true;

        }
        private void cobforecastmethodWiz2_SelectedIndexChanged(object sender, EventArgs e)
        {
           // _parameterName ={};
            string[] _parameterName = new string[] { "", "" };
            string name=cobforecastmethodWiz2.Text;
            if (cobforecastmethodWiz2.Text == "Target Based")
            {
                _parameterName[0] = "CurrentPatient";
                _parameterName[1] = "TargetPatient";
            }
            else
            {
                _parameterName[0] = "PopulationNumber";
                _parameterName[1] = "PrevalenceRate";                          
            }
           
            //Flag = SaveOrUpdateMMForecastParameterObject();

            fillForecastList(_parameterName);

            //Flag = SaveOrUpdateMMForecastParameterObject1(_parameterName);
            //BindMMForecastParameters();
            //BindForecastMethod();
            //BindVariablesDataType();
            //txtVariableNameWiz2.Text = string.Empty;
            //cobforecastmethodWiz2.Enabled = false;
        }
        public void fillForecastList(string[] _parameterName)
        {
            int j = 1;
            lvforecastParamsWiz2.BeginUpdate();
            lvforecastParamsWiz2.Items.Clear();
            for (int i = 0; i < _parameterName.Length; i++)
            {
                   
                    ListViewItem listViewItem = new ListViewItem(j.ToString())
                        {
                            Tag = _parameterName[i].Trim()
                        };
                        listViewItem.SubItems.Add("Yes");
                        listViewItem.SubItems.Add(cobforecastmethodWiz2.Text.Trim().Replace(' ', '_').ToString());
                        listViewItem.SubItems.Add("Numeric");
                        listViewItem.SubItems.Add(_parameterName[i].Trim());
                        listViewItem.SubItems.Add(MorbidityVariableUsage.OnAllSite.ToString());
                        listViewItem.SubItems.Add("");

                        lvforecastParamsWiz2.Items.Add(listViewItem);
                        j++;
               
            }
            lvforecastParamsWiz2.EndUpdate();
            SetColors(lvforecastParamsWiz2);
        }
        private bool SaveOrUpdateMMForecastParameterObject2(string []_parameterName)
        {
            string _sql = "";
            bool result;
            for (int i = 0; i < _parameterName.Length; i++)
            {
                TextInfo t = new CultureInfo("en-US", false).TextInfo;
                //string UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
                string UseOn =MorbidityVariableUsage.OnAllSite.ToString();
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    //if (mMFParam.VariableName == t.ToTitleCase(_parameterName[i].Trim()) && mMFParam.UseOn == UseOn)
                    //{

                    //    MessageBox.Show("Variable Name must not be duplicate for " + UseOn + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    return false;
                    //}
                    if (mMFParam.VariableName == t.ToTitleCase(_parameterName[i].Trim()) && mMFParam.UseOn == UseOn)
                    {

                         
                    }
                }

                if (_mMForecastParam == null)
                    _mMForecastParam = new MMForecastParameter();

                _mMForecastParam.ForecastMethod = (int)Enum.Parse(typeof(MorbidityForecastingMethod), cobforecastmethodWiz2.Text.Trim().Replace(' ', '_'));

                _mMForecastParam.VariableName = t.ToTitleCase(_parameterName[i].Trim());

               // _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz2.Text.Trim());
                _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType),"Numeric");
               // _mMForecastParam.UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
                _mMForecastParam.UseOn =MorbidityVariableUsage.OnAllSite.ToString();
                _mMForecastParam.MMProgram = _mMProgram;
                _mMForecastParam.VarCode = LqtUtil.GetUpperCases(_mMForecastParam.VariableName);

                _mMForecastParam.IsPrimaryOutput = chkprimaryoutput.Checked;
               // _mMForecastParam.Id = -1;
                _mMForecastParam.VariableEffect = rdbPosWiz2.Checked ? true : false;
                _mMForecastParam.IsActive = chkActive.Checked;
                _mMProgram.MMForecastParameters.Add(_mMForecastParam);



                DataRepository.SaveOrUpdateMMForecastParameter(_mMForecastParam);

                if (UseOn == "OnAllSite")
                {
                    //_sql = "IF COL_LENGTH('ForecastSiteInfo','[" + t.ToTitleCase(_parameterName[i].Trim()) + "]') IS NULL BEGIN  alter table ForecastSiteInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                    _sql = "IF COL_LENGTH('ForecastSiteInfo','" + t.ToTitleCase(_parameterName[i].Trim()) + "') IS NULL BEGIN  alter table ForecastSiteInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                    result = DatabaseManager.AlterTables(_sql);
                    _sql = "IF COL_LENGTH('ForecastCategoryInfo','" +t.ToTitleCase(_parameterName[i].Trim()) + "') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                }
                //else
                //{
                //   // _sql = "IF COL_LENGTH('ForecastCategoryInfo','[" + t.ToTitleCase(_parameterName[i].Trim()) + "]') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                //    _sql = "IF COL_LENGTH('ForecastCategoryInfo','" + t.ToTitleCase(_parameterName[i].Trim().Replace(" ", string.Empty)) + "') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(_parameterName[i].Trim().Replace(" ", string.Empty)) + "] numeric(18, 2)  END ";
                //}
                result = DatabaseManager.AlterTables(_sql);
                _mMForecastParam = null;
            }
            BindMMForecastParameters();
          
            return true;

        }
        private bool SaveOrUpdateMMForecastParameterObject1(string[] _parameterName)
        {
            string _sql = "";
            bool result;
            string UseOn = MorbidityVariableUsage.OnAllSite.ToString();
            foreach (ListViewItem item in lvforecastParamsWiz2.Items)
            {

                TextInfo t = new CultureInfo("en-US", false).TextInfo;
                //string UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
               
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    if (mMFParam.VariableName == item.Tag.ToString().Trim() && mMFParam.UseOn == UseOn)
                    {

                        MessageBox.Show("Variable Name must not be duplicate for " + UseOn + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }

                }

                if (_mMForecastParam == null)
                    _mMForecastParam = new MMForecastParameter();

                _mMForecastParam.ForecastMethod = (int)Enum.Parse(typeof(MorbidityForecastingMethod), cobforecastmethodWiz2.Text.Trim().Replace(' ', '_'));

                _mMForecastParam.VariableName = item.Tag.ToString().Trim();

                // _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz2.Text.Trim());
                _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), "Numeric");
                // _mMForecastParam.UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
                _mMForecastParam.UseOn = MorbidityVariableUsage.OnAllSite.ToString();
                _mMForecastParam.MMProgram = _mMProgram;
                _mMForecastParam.VarCode = LqtUtil.GetUpperCases(_mMForecastParam.VariableName);

                _mMForecastParam.IsPrimaryOutput = chkprimaryoutput.Checked;
                // _mMForecastParam.Id = -1;
                _mMForecastParam.VariableEffect = rdbPosWiz2.Checked ? true : false;
                _mMForecastParam.IsActive = chkActive.Checked;
                _mMProgram.MMForecastParameters.Add(_mMForecastParam);



                DataRepository.SaveOrUpdateMMForecastParameter(_mMForecastParam);

                if (UseOn == "OnAllSite")
                {
                    //_sql = "IF COL_LENGTH('ForecastSiteInfo','[" + t.ToTitleCase(_parameterName[i].Trim()) + "]') IS NULL BEGIN  alter table ForecastSiteInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                    _sql = "IF COL_LENGTH('ForecastSiteInfo','" + item.Tag.ToString().Trim() + "') IS NULL BEGIN  alter table ForecastSiteInfo add  [" +item.Tag.ToString().Trim() + "] numeric(18, 2)  END ";
                    result = DatabaseManager.AlterTables(_sql);
                    _sql = "IF COL_LENGTH('ForecastCategoryInfo','" +item.Tag.ToString().Trim() + "') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" +item.Tag.ToString().Trim() + "] numeric(18, 2)  END ";
                }
                //else
                //{
                //   // _sql = "IF COL_LENGTH('ForecastCategoryInfo','[" + t.ToTitleCase(_parameterName[i].Trim()) + "]') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(_parameterName[i].Trim()) + "] numeric(18, 2)  END ";
                //    _sql = "IF COL_LENGTH('ForecastCategoryInfo','" + t.ToTitleCase(_parameterName[i].Trim().Replace(" ", string.Empty)) + "') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(_parameterName[i].Trim().Replace(" ", string.Empty)) + "] numeric(18, 2)  END ";
                //}
                result = DatabaseManager.AlterTables(_sql);
                _mMForecastParam = null;
            }
           
            BindMMForecastParameters();
            //MessageBox.Show("All variable name saved successfully for " + UseOn + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            return true;

        }
        private void BindMMForecastParameters()
        {
            int i = 1;
            lvforecastParamsWiz2.BeginUpdate();
            lvforecastParamsWiz2.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMFParam
                    };

                    if (mMFParam.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityForecastingMethod)mMFParam.ForecastMethod).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMFParam.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMFParam.VariableName);
                    listViewItem.SubItems.Add(mMFParam.UseOn);
                    listViewItem.SubItems.Add(mMFParam.VariableFormula);
                  
                    lvforecastParamsWiz2.Items.Add(listViewItem);
                    i++;
                }
            }
            lvforecastParamsWiz2.EndUpdate();
            SetColors(lvforecastParamsWiz2);
        

        }
        private void BindReviewMMForecastParameters()
        {
            int i = 1;
            lstReviewForecastingVariable.BeginUpdate();
            lstReviewForecastingVariable.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMFParam
                    };

                    if (mMFParam.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityForecastingMethod)mMFParam.ForecastMethod).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMFParam.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMFParam.VariableName);
                    listViewItem.SubItems.Add(mMFParam.UseOn);
                    listViewItem.SubItems.Add(mMFParam.VariableFormula);

                    lstReviewForecastingVariable.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewForecastingVariable.EndUpdate();
            SetColors(lstReviewForecastingVariable);
        }

        private void btnaddformulaWiz2_Click(object sender, EventArgs e)
        {
            if (AddAddFormula())
            {
                BindMMForecastParameters();
            }
        }
        public bool AddAddFormula()
        {
            FrmAddFormula frm;
            IList<MMForecastParameter> _mMForecastParamss = DataRepository.GetAllForecastParameterByProgram(_mMProgram.Id);


            frm = new FrmAddFormula(_mMForecastParamss);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMForecastParameter newcat in frm._updatedmMForecastParams)
                {
                    foreach (MMForecastParameter cat in _mMForecastParamss)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            _mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMForecastParameter(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void btnAddWiz3_Click(object sender, EventArgs e)
        {
            SaveOrUpdateMMGroupObject();
            BindMMGroups();
            txtgroupnameWiz3.Text = string.Empty;
        }
        private void SaveOrUpdateMMGroupObject()
        {
            if (txtgroupnameWiz3.Text != "")
            {
                if (_mMGroup == null)
                    _mMGroup = new MMGroup();

                _mMGroup.GroupName = txtgroupnameWiz3.Text.Trim();
                _mMGroup.MMProgram = _mMProgram;
                _mMGroup.IsActive = chkActiveGroup.Checked;
                _mMProgram.MMGroups.Add(_mMGroup);
                DataRepository.SaveOrUpdateMMGroup(_mMGroup);
                BindMMGroups();
                _mMGroup = null;
            }
            else
            {
                MessageBox.Show("Please Enter Group Name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BindMMGroups()
        {
            int i = 1;
            lvGroupWiz3.BeginUpdate();
            lvGroupWiz3.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGroup mMGroup in _mMProgram.MMGroups)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGroup
                    };
                    if (mMGroup.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(mMGroup.GroupName);

                    lvGroupWiz3.Items.Add(listViewItem);
                    i++;
                }
            }
            lvGroupWiz3.EndUpdate();
            SetColors(lvGroupWiz3);
        }
        private void BindReviewMMGroups()
        {

            int i = 1;
            lstReviewPatientGroup.BeginUpdate();
            lstReviewPatientGroup.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGroup mMGroup in _mMProgram.MMGroups)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGroup
                    };
                    if (mMGroup.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(mMGroup.GroupName);

                    lstReviewPatientGroup.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewPatientGroup.EndUpdate();
            SetColors(lstReviewPatientGroup);
        }

        private void btnAddWiz4_Click(object sender, EventArgs e)
        {
            if (_currentTab.Equals("Patient Assumptions"))
            {
                Flag = SaveOrUpdateMMGeneralAssumptionsObject();
                BindGATypes();
                BindMMGeneralAssumptionss();
                if (Flag == true)
                {
                    txtVariableNameWiz4.Text = string.Empty;
                    BindGAVariablesDataType();
                }
            }
            else if (_currentTab.Equals("Testing Assumption"))
            {
                Flag = SaveOrUpdateTestingAssumptionsObject();
                BindTestingATypes();
                BindMMTestingAssumptionss();
                if (Flag == true)
                {
                    txtTVariableName.Text = string.Empty;
                    BindTAVariablesDataType();
                }
            }
            else
            {
                Flag = SaveOrUpdateProductAssumptionsObject();
                BindProductATypes();
                BindMMProductAssumptionss();
                if (Flag == true)
                {
                    txtProductVariableName.Text = string.Empty;
                    BindPAVariablesDataType();
                }
            }
            
           
        }
        private bool SaveOrUpdateMMGeneralAssumptionsObject()
        {

            string _sql = "";
            bool result;
            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            string UseOn = rdbeachWiz4.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            if (txtVariableNameWiz4.Text.Trim() != "")
            {
                if (cobDatatypeWiz4.Text != "")
                {
                    foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                    {
                        if (mMGA.VariableName == txtVariableNameWiz4.Text.Trim() && mMGA.AssumptionType == (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')))
                        {

                            MessageBox.Show("Variable Name must not be duplicate for " + cobAssumptionTypeWiz4.Text.Trim() + " ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    if (_mMGeneralAssumption == null)
                        _mMGeneralAssumption = new MMGeneralAssumption();


                    _mMGeneralAssumption.VariableName = txtVariableNameWiz4.Text.Trim();

                    _mMGeneralAssumption.AssumptionType = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_'));

                    _mMGeneralAssumption.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz4.Text.Trim());
                    _mMGeneralAssumption.UseOn = rdbeachWiz4.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

                    _mMGeneralAssumption.MMProgram = _mMProgram;
                    _mMGeneralAssumption.VarCode = LqtUtil.GetUpperCases(_mMGeneralAssumption.VariableName);

                    _mMGeneralAssumption.VariableEffect = rdbPosWiz4.Checked ? true : false;


                    _mMGeneralAssumption.IsActive = chkGeneralAssumption.Checked;
                    _mMProgram.MMGeneralAssumptions.Add(_mMGeneralAssumption);

                    DataRepository.SaveOrUpdateMMGeneralAssumption(_mMGeneralAssumption);
                    BindMMGeneralAssumptionss();
                    _mMGeneralAssumption = null;

                    //if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    //{
                    //    _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //else
                    //{
                    //    _sql = "IF COL_LENGTH('TestingAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table TestingAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    {
                        _sql = "IF NOT EXISTS(SELECT * FROM sys.columns WHERE Name IN (N'" + txtVariableNameWiz4.Text.Trim() + "') AND Object_ID = Object_ID(N'PatientAssumption')) BEGIN ALTER TABLE PatientAssumption ADD [" + txtVariableNameWiz4.Text.Trim() + "] numeric(18,2) END";
                        // _sql = "IF COL_LENGTH('PatientAssumption','[" + txtVariableNameWiz4.Text.Trim() + "]') IS NULL BEGIN  alter table PatientAssumption add  [" +txtVariableNameWiz4.Text.Trim() + "] numeric(18, 2)  END ";

                    }
                    //else
                    //{
                    //    _sql = "IF COL_LENGTH('TestingAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table TestingAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}

                    result = DatabaseManager.AlterTables(_sql);
                }
                else
                {
                    MessageBox.Show("Please Enter The Data Type ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cobDatatypeWiz4.Focus();
                    return false;
                }
            }
            else {
                MessageBox.Show("Please Enter The Variable Name ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtVariableNameWiz4.Focus();
                return false;
            }
            return true;
        }
        private bool SaveOrUpdateTestingAssumptionsObject()
        {

            string _sql = "";
            bool result;
            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            string UseOn = rdbeachTesting.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            if (txtTVariableName.Text != "")
            {
                if (cboTestingDatatype.Text != "")
                {
                    foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                    {
                        if (mMGA.VariableName == txtTVariableName.Text.Trim() && mMGA.AssumptionType == (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboTestingAssumption.Text.Trim().Replace(' ', '_')))
                        {

                            MessageBox.Show("Variable Name must not be duplicate for " + cboTestingAssumption.Text.Trim() + " ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    if (_mMGeneralAssumption == null)
                        _mMGeneralAssumption = new MMGeneralAssumption();


                    _mMGeneralAssumption.VariableName = txtTVariableName.Text.Trim();

                    _mMGeneralAssumption.AssumptionType = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboTestingAssumption.Text.Trim().Replace(' ', '_'));

                    _mMGeneralAssumption.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cboTestingDatatype.Text.Trim());
                    _mMGeneralAssumption.UseOn = rdbeachTesting.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

                    _mMGeneralAssumption.MMProgram = _mMProgram;
                    _mMGeneralAssumption.VarCode = LqtUtil.GetUpperCases(_mMGeneralAssumption.VariableName);

                    _mMGeneralAssumption.VariableEffect = rdbPosTesting.Checked ? true : false;


                    _mMGeneralAssumption.IsActive = chkTestingAssumption.Checked;
                    _mMProgram.MMGeneralAssumptions.Add(_mMGeneralAssumption);

                    DataRepository.SaveOrUpdateMMGeneralAssumption(_mMGeneralAssumption);
                    BindMMTestingAssumptionss();
                    _mMGeneralAssumption = null;

                    //if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    //{
                    //    _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //else
                    //{
                    //    _sql = "IF COL_LENGTH('TestingAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table TestingAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    //{
                    //    _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //else
                    //{
                    _sql = "IF COL_LENGTH('TestingProtocol','" + txtTVariableName.Text.Trim() + "') IS NULL BEGIN  alter table TestingProtocol add  [" + txtTVariableName.Text.Trim() + "] numeric(18, 2)  END ";
                    result = DatabaseManager.AlterTables(_sql);
                    _sql = "IF COL_LENGTH('Testbymonth','" + txtTVariableName.Text.Trim() + "') IS NULL BEGIN  alter table Testbymonth add  [" + txtTVariableName.Text.Trim() + "] numeric(18, 2)  END ";
                    result = DatabaseManager.AlterTables(_sql);
                    _sql = "IF COL_LENGTH('percentageval','" + txtTVariableName.Text.Trim() + "') IS NULL BEGIN  alter table percentageval add  [" + txtTVariableName.Text.Trim() + "] numeric(18, 2)  END ";

                    //}

                    result = DatabaseManager.AlterTables(_sql);
                }

                else
                {
                    MessageBox.Show("Please Enter The Data Type ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cboTestingDatatype.Focus();
                    return false;
                } 
            }
            else
            {
                MessageBox.Show("Please Enter The Variable Name ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTVariableName.Focus();
                return false;
            }
            return true;

        }
        private bool SaveOrUpdateProductAssumptionsObject()
        {

            string _sql = "";
            bool result;
            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            string UseOn = rdbeachProduct.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            if (txtProductVariableName.Text != "")
            {
                if (cboProductDatatype.Text != "")
                {
                    foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                    {
                        if (mMGA.VariableName == txtProductVariableName.Text.Trim() && mMGA.AssumptionType == (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboProductassuption.Text.Trim().Replace(' ', '_')))
                        {

                            MessageBox.Show("Variable Name must not be duplicate for " + cboProductassuption.Text.Trim() + " ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    if (_mMGeneralAssumption == null)
                        _mMGeneralAssumption = new MMGeneralAssumption();


                    _mMGeneralAssumption.VariableName = txtProductVariableName.Text.Trim();

                    _mMGeneralAssumption.AssumptionType = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboProductassuption.Text.Trim().Replace(' ', '_'));

                    _mMGeneralAssumption.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cboProductDatatype.Text.Trim());
                    _mMGeneralAssumption.UseOn = rdbeachProduct.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

                    _mMGeneralAssumption.MMProgram = _mMProgram;
                    _mMGeneralAssumption.VarCode = LqtUtil.GetUpperCases(_mMGeneralAssumption.VariableName);

                    _mMGeneralAssumption.VariableEffect = rdbPosProduct1.Checked ? true : false;


                    _mMGeneralAssumption.IsActive = chkProductAssumption.Checked;
                    _mMProgram.MMGeneralAssumptions.Add(_mMGeneralAssumption);

                    DataRepository.SaveOrUpdateMMGeneralAssumption(_mMGeneralAssumption);
                    BindMMProductAssumptionss();
                    _mMGeneralAssumption = null;

                    //if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    //{
                    //    _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //else
                    //{
                    //    _sql = "IF COL_LENGTH('TestingAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table TestingAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
                    //{
                    //    _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

                    //}
                    //else
                    //{
                    _sql = "IF COL_LENGTH('TestingAssumption','" + txtProductVariableName.Text.Trim() + "') IS NULL BEGIN  alter table TestingAssumption add  [" + txtProductVariableName.Text.Trim() + "] numeric(18, 2)  END ";

                    //}

                    result = DatabaseManager.AlterTables(_sql);

                }
                else
                {
                    MessageBox.Show("Please Enter The Data Type ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cboProductDatatype.Focus();
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Please Enter The Variable Name ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtProductVariableName.Focus();
                return false;
            }    
            return true;

        }
        private void BindMMGeneralAssumptionss()
        {

            int i = 1;
            lvgeneralassumptions.BeginUpdate();
            lvgeneralassumptions.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
         
            //if (_mMProgram != null)
            //{
            //    foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
            //    {
            //        ListViewItem listViewItem = new ListViewItem(i.ToString())
            //        {
            //            Tag = mMGA
            //        };
            //        if (mMGA.IsActive == true)
            //            listViewItem.SubItems.Add("Yes");
            //        else
            //            listViewItem.SubItems.Add("No");
            //        listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
            //        listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
            //        listViewItem.SubItems.Add(mMGA.VariableName);
            //        listViewItem.SubItems.Add(mMGA.UseOn);
            //        listViewItem.SubItems.Add(mMGA.VariableFormula);

            //        lvgeneralassumptions.Items.Add(listViewItem);
            //        i++;
            //    }
            //}

           if (mMGAbyType != null)
           {
               foreach (MMGeneralAssumption mMGA in mMGAbyType)
               {
                   ListViewItem listViewItem = new ListViewItem(i.ToString())
                   {
                       Tag = mMGA
                   };
                   if (mMGA.IsActive == true)
                       listViewItem.SubItems.Add("Yes");
                   else
                       listViewItem.SubItems.Add("No");
                   listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                   listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                   listViewItem.SubItems.Add(mMGA.VariableName);
                   listViewItem.SubItems.Add(mMGA.UseOn);
                   listViewItem.SubItems.Add(mMGA.VariableFormula);

                   lvgeneralassumptions.Items.Add(listViewItem);
                   i++;
               }
           }

            lvgeneralassumptions.EndUpdate();
            SetColors(lvgeneralassumptions);
            SetColors(lvTestingAssumption);
        }
        private void BindMMProductAssumptionss()
        {

            int i = 1;
            lvProductAssumption.BeginUpdate();
            lvProductAssumption.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboProductassuption.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
            if (mMGAbyType != null)
            {
                foreach (MMGeneralAssumption mMGA in mMGAbyType)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lvProductAssumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lvProductAssumption.EndUpdate();
            SetColors(lvProductAssumption);
        }
        private void BindMMTestingAssumptionss()
        {

            int i = 1;
            lvTestingAssumption.BeginUpdate();
            lvTestingAssumption.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboTestingAssumption.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
            if (mMGAbyType != null)
            {
                foreach (MMGeneralAssumption mMGA in mMGAbyType)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lvTestingAssumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lvTestingAssumption.EndUpdate();
            SetColors(lvTestingAssumption);
        }
        private void BindReviewMMGeneralAssumptions()
        {

            int i = 1;
            lstReviewGeneralAssumption.BeginUpdate();
            lstReviewGeneralAssumption.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
            //if (_mMProgram != null)
            //{
            //    foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
            //    {
            //        ListViewItem listViewItem = new ListViewItem(i.ToString())
            //        {
            //            Tag = mMGA
            //        };
            //        if (mMGA.IsActive == true)
            //            listViewItem.SubItems.Add("Yes");
            //        else
            //            listViewItem.SubItems.Add("No");
            //        listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
            //        listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
            //        listViewItem.SubItems.Add(mMGA.VariableName);
            //        listViewItem.SubItems.Add(mMGA.UseOn);
            //        listViewItem.SubItems.Add(mMGA.VariableFormula);

            //        lstReviewGeneralAssumption.Items.Add(listViewItem);
            //        i++;
            //    }
            //}
            if (mMGAbyType != null)
            {
                foreach (MMGeneralAssumption mMGA in mMGAbyType)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lstReviewGeneralAssumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewGeneralAssumption.EndUpdate();
            SetColors(lstReviewGeneralAssumption);

        }

        private void BindReviewMMTestingAssumptions()
        {

            int i = 1;
            lstReviewTestingassumption.BeginUpdate();
            lstReviewTestingassumption.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboTestingAssumption.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
            if (mMGAbyType != null)
            {
                foreach (MMGeneralAssumption mMGA in mMGAbyType)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lstReviewTestingassumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewTestingassumption.EndUpdate();
            SetColors(lstReviewTestingassumption);

        }
        private void BindReviewMMProductAssumptions()
        {

            int i = 1;
            lstReviewProductAssumption.BeginUpdate();
            lstReviewProductAssumption.Items.Clear();
            int typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboProductassuption.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> mMGAbyType = DataRepository.GetAllGeneralAssumptionByType(typeId, _mMProgram.Id);
            if (mMGAbyType != null)
            {
                foreach (MMGeneralAssumption mMGA in mMGAbyType)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lstReviewProductAssumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewProductAssumption.EndUpdate();
            SetColors(lstReviewProductAssumption);

        }
        private void btnaddformulaWiz4_Click(object sender, EventArgs e)
        {
            if (AddAddFormulaGA())
            {
                BindMMGeneralAssumptionss();
            }
        }
        public bool AddAddFormulaGA()
        {
            FrmAddFormula frm;
            int typeId=0;
            //IList<MMGeneralAssumption> _mMGA = DataRepository.GetAllGeneralAssumptionByProgram(_mMProgram.Id);
            if (_currentTab.Equals("Patient Assumptions")) typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_'));
            else if (_currentTab.Equals("Testing Assumption")) typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboTestingAssumption.Text.Trim().Replace(' ', '_'));
            else if (_currentTab.Equals("Product Assumption")) typeId = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cboProductassuption.Text.Trim().Replace(' ', '_'));
            IList<MMGeneralAssumption> _mMGA = DataRepository.GetAllGeneralAssumptionByType(typeId,_mMProgram.Id);
            frm = new FrmAddFormula(_mMGA, 1, "GA");
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMGeneralAssumption newcat in frm._updatedmMGeneralAssumptions)
                {
                    foreach (MMGeneralAssumption cat in _mMGA)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            //_mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMGeneralAssumption(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void chkgeneralprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.GTP = chkgeneralprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
        }

        private void chkrapidprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.RTP = chkrapidprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
        }

        private void lvProgramList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvProgramList.SelectedItems.Count > 0)
            {
                _mMProgram = (MMProgram)lvProgramList.SelectedItems[0].Tag;
                this.txtProgramName.Text = _mMProgram.ProgramName;
            }
            else
            {
                _mMProgram = null;
                this.txtProgramName.Text = string.Empty;
            }
        }
        public void SetColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            using (StringFormat sf = new StringFormat())
            {
                // Store the column text alignment, letting it default
                // to Left if it has not been set to Center or Right.
                switch (e.Header.TextAlign)
                {
                    case HorizontalAlignment.Center:
                        sf.Alignment = StringAlignment.Center;
                        break;
                    case HorizontalAlignment.Right:
                        sf.Alignment = StringAlignment.Far;
                        break;
                }
                // Draw the standard header background.
                e.DrawBackground();

                // Draw the header text.
                using (Font headerFont = new Font("Helvetica", 10, FontStyle.Bold)) //Font size!!!!
                {
                    e.Graphics.DrawString(e.Header.Text, headerFont, Brushes.DimGray, e.Bounds, sf);
                }
            }
        }

        private void lvProgramList_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvProgramList_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void BindForecastMethod()
        {
            string[] forecastMethod = Enum.GetNames(typeof(MorbidityForecastingMethod));
            cobforecastmethodWiz2.Items.Clear();
            for (int i = 0; i < forecastMethod.Length; i++)
            {
                if (forecastMethod[i] != "Custom_User_Defined")
                cobforecastmethodWiz2.Items.Add(forecastMethod[i].Replace('_', ' '));
            }
        }

        private void BindVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz2.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz2.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void lvgeneralassumptions_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvGroupWiz3_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {

            SetColumnHeader(e);
        }

        private void lvGroupWiz3_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvforecastParamsWiz2_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvforecastParamsWiz2_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvgeneralassumptions_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        public void fontBold()
        {

            for (int i = 0; i < lvProductAssumption.Columns.Count; i++)
            {
                lvProductAssumption.Columns[i].ListView.Font = new Font(lvProductAssumption.Columns[i].ListView.Font, FontStyle.Regular);
            }
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            _currentTab = e.TabPage.Tag.ToString();
            if (checKProgramSelected() == true)
            {
                switch (_currentTab)
                {
                    case "Define Program":
                        button2.Enabled = false;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Forecasting Method":
                        BindForecastMethod();
                        BindVariablesDataType();
                        BindMMForecastParameters();
                        button2.Enabled = true;
                        button1.Enabled = true;
                        if (lvforecastParamsWiz2.Items.Count > 0) cobforecastmethodWiz2.Enabled = false;
                        else cobforecastmethodWiz2.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Patient/Population Group":
                        BindMMGroups();
                        button2.Enabled = true;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Patient Assumptions":
                        BindGAVariablesDataType();
                        BindGATypes();
                        BindMMGeneralAssumptionss();
                        button2.Enabled = true;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Product Assumption":
                        BindProductATypes();
                        BindPAVariablesDataType();
                        //BindMMProductAssumptionss("Product Assumption");
                        BindMMProductAssumptionss();
                        button2.Enabled = true;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Testing Assumption":
                        BindTestingATypes();
                        BindTAVariablesDataType();
                        BindMMTestingAssumptionss();
                        button2.Enabled = true;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Testing Protocol Options":
                        button2.Enabled = true;
                        button1.Enabled = true;
                        button3.Text = "Cancel";
                        break;
                    case "Review":
                        lblprogram1.Text = txtProgramName.Text;
                        button3.Text = "Done";
                        //  _mMProgram
                        BindReviewMMForecastParameters();
                        BindReviewMMGroups();
                        BindReviewMMGeneralAssumptions();
                        BindReviewMMTestingAssumptions();
                        BindReviewMMProductAssumptions();
                        //if (chkgeneralprotocol.Checked == true)
                        //{
                        //    lbltestingprotocol1.Text = chkgeneralprotocol.Text;
                        //}
                        //else if (chkrapidprotocol.Checked == true)
                        //{
                        //    lbltestingprotocol1.Text = chkrapidprotocol.Text;
                        //}
                        //else if (chkrapidprotocol.Checked == true && chkgeneralprotocol.Checked == true)
                        //{
                        //    lbltestingprotocol1.Text = chkgeneralprotocol.Text + "," + chkrapidprotocol.Text;
                        //}
                        //else
                        //{
                        //    lbltestingprotocol1.Text = "";
                        //}
                        if (rbgeneralprotocol.Checked == true)
                        {
                            lbltestingprotocol1.Text = rbgeneralprotocol.Text;
                        }
                        else if (rbrapidprotocol.Checked == true)
                        {
                            lbltestingprotocol1.Text = rbrapidprotocol.Text;
                        }
                        else if (rbrapidprotocol.Checked == true && rbgeneralprotocol.Checked == true)
                        {
                            lbltestingprotocol1.Text = rbgeneralprotocol.Text + "," + rbrapidprotocol.Text;
                        }
                        else
                        {
                            lbltestingprotocol1.Text = "";
                        }
                        button2.Enabled = true;
                        button1.Enabled = false;
                        break;

                }
            }
        }
        private void BindGAVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz4.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz4.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void BindTAVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cboTestingDatatype.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cboTestingDatatype.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }
        private void BindPAVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cboProductDatatype.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cboProductDatatype.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }
        private void BindGATypes()
        {
            string[] variableAssumptionType = Enum.GetNames(typeof(MorbidityGeneralAssumptionTypes));
            cobAssumptionTypeWiz4.Items.Clear();
            for (int i = 0; i < variableAssumptionType.Length; i++)
            {
                cobAssumptionTypeWiz4.Items.Add(variableAssumptionType[i].Replace('_', ' '));
            }
            //cobAssumptionTypeWiz4.Text = "Patient Population Assumption";
            cobAssumptionTypeWiz4.Text = "Patient Number Assumption";
        }
        private void BindTestingATypes()
        {
            string[] variableAssumptionType = Enum.GetNames(typeof(MorbidityGeneralAssumptionTypes));
            cboTestingAssumption.Items.Clear();
            for (int i = 0; i < variableAssumptionType.Length; i++)
            {
                cboTestingAssumption.Items.Add(variableAssumptionType[i].Replace('_', ' '));
            }
            cboTestingAssumption.Text = "Test Assumption";
        }
        private void BindProductATypes()
        {
            string[] variableAssumptionType = Enum.GetNames(typeof(MorbidityGeneralAssumptionTypes));
            cboProductassuption.Items.Clear();
            for (int i = 0; i < variableAssumptionType.Length; i++)
            {
                cboProductassuption.Items.Add(variableAssumptionType[i].Replace('_', ' '));
            }
            cboProductassuption.Text = "Product Assumption";
        }

        public bool checKProgramSelected()
        {
            if (lvProgramList.SelectedItems.Count > 0)
            {
                return true;
            }
            MessageBox.Show("Please Select Atleast One Program", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            txtgroupnameWiz3.Text = string.Empty;
            txtVariableNameWiz2.Text = string.Empty;
            txtVariableNameWiz4.Text = string.Empty;
            txtTVariableName.Text = string.Empty;
            txtProductVariableName.Text = string.Empty;
            if (checKProgramSelected() == true)
            {
                switch (_currentTab)
                {
                    case "Define Program":
                        lvforecastParamsWiz2.Items.Clear();
                        BindMMForecastParameters();
                        if (lvforecastParamsWiz2.Items.Count > 0) cobforecastmethodWiz2.Enabled = false;
                        else cobforecastmethodWiz2.Enabled = true;
                        rdbPosProduct.SelectedTab = tabForecastMethod;
                        break;
                    case "Forecasting Method":
                        //lvforecastParamsWiz2.Items.Clear();
                        //BindMMForecastParameters();
                        //if (lvforecastParamsWiz2.Items.Count == 0)
                        int cnt = _mMProgram.MMForecastParameters.Count;
                        if (cnt == 0)
                        Flag = SaveOrUpdateMMForecastParameterObject1(_parameterName);
                        BindMMForecastParameters();
                        BindForecastMethod();
                        BindVariablesDataType();
                        txtVariableNameWiz2.Text = string.Empty;
                        cobforecastmethodWiz2.Enabled = false;
                        rdbPosProduct.SelectedTab = tabPatientGroup;
                        break;
                    case "Patient/Population Group":
                        rdbPosProduct.SelectedTab = tabPatientAssumption;
                        break;
                    case "Patient Assumptions":
                        rdbPosProduct.SelectedTab = tabTestingAssumption;
                        break;
                    case "Testing Assumption":
                        rdbPosProduct.SelectedTab = tabProductAssumption;
                        break;
                    case "Product Assumption":
                        rdbPosProduct.SelectedTab = tabTestingProtocol;
                        break;
                    case "Testing Protocol Options":
                        rdbPosProduct.SelectedTab = tabReview;
                        break;
                    //case "CONSUMPTION":
                    //    SaveConsumption("Consumption Data usage was saved successfully.");
                    //    break;
                }
            }
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (_currentTab)
            {
             
                case "Forecasting Method":
                    if (lvforecastParamsWiz2.Items.Count > 0) cobforecastmethodWiz2.Enabled = false;
                    else cobforecastmethodWiz2.Enabled = true;
                    rdbPosProduct.SelectedTab = tabDefprogram;
                    break;
                case "Patient/Population Group":
                    rdbPosProduct.SelectedTab = tabForecastMethod;
                    break;
                case "Patient Assumptions":
                    rdbPosProduct.SelectedTab = tabPatientGroup;
                    break;
                case "Testing Assumption":
                    rdbPosProduct.SelectedTab = tabPatientAssumption;
                    break;
                case "Product Assumption":
                    rdbPosProduct.SelectedTab = tabTestingAssumption;
                    break;
                case "Testing Protocol Options":
                    rdbPosProduct.SelectedTab = tabProductAssumption;
                    break;
                case "Review":
                    rdbPosProduct.SelectedTab = tabTestingProtocol;
                    break;
            
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lvforecastParamsWiz2_SelectedIndexChanged(object sender, EventArgs e)
        {
        
        }

        private void lvforecastParamsWiz2_Click(object sender, EventArgs e)
        {
            if (lvforecastParamsWiz2.SelectedItems.Count > 0)
            {

                int cnt = _mMProgram.MMForecastParameters.Count;
                if (cnt > 0)
                {
                    ListViewItem itm = lvforecastParamsWiz2.SelectedItems[0];
                    MMForecastParameter m = (MMForecastParameter)itm.Tag;
                    // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    var confirmation = MessageBox.Show("For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (confirmation == DialogResult.Yes)
                    {
                        //DataRepository.DeleteforecastParameter(DataRepository.GetAllForecastParameterById(m.Id));
                        //  _mMProgram.MMForecastParameters.Remove(m);
                        if (m.IsActive == true)
                        {
                            m.IsActive = false;
                        }
                        else
                        {
                            m.IsActive = true;
                        }
                        DataRepository.SaveOrUpdateMMForecastParameter(m);
                    }
                    else if (confirmation == DialogResult.No)
                    {

                        //_mMProgram.MMForecastParameters.Remove(m);
                        //_mMProgram.MMForecastParameters.Add(m);
                    }
                    BindMMForecastParameters();
                }
                else
                {
                    MessageBox.Show("Please First Save The Program", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
           
        }

        private void lvGroupWiz3_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void lvgeneralassumptions_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void lvGroupWiz3_Click(object sender, EventArgs e)
        {
            if (lvGroupWiz3.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvGroupWiz3.SelectedItems[0];
                MMGroup m = (MMGroup)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show("For Activation/InActivation " + m.GroupName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                 //   DataRepository.DeleteProgramGroup(DataRepository.GetProgramGroupbyid(m.Id));
                    //_mMProgram.MMGroups.Remove(m);
                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGroup(m);
                }
                else if (confirmation == DialogResult.No)
                {
                  
                    //_mMProgram.MMForecastParameters.Remove(m);
                    //_mMProgram.MMForecastParameters.Add(m);
                }
                BindMMGroups();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void lvgeneralassumptions_Click(object sender, EventArgs e)
        {
            if (lvgeneralassumptions.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvgeneralassumptions.SelectedItems[0];
                MMGeneralAssumption m = (MMGeneralAssumption)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show( "For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                
                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGeneralAssumption(m);
                }
                else if (confirmation == DialogResult.No)
                {
               
                  
                }
                BindMMGeneralAssumptionss();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void lstReviewForecastingVariable_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewForecastingVariable_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstReviewPatientGroup_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewPatientGroup_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstReviewGeneralAssumption_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewGeneralAssumption_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void listView3_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void listView3_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvProductAssumption_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lvProductAssumption_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvProductAssumption_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvProductAssumption_Click(object sender, EventArgs e)
        {
            if (lvProductAssumption.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvProductAssumption.SelectedItems[0];
                MMGeneralAssumption m = (MMGeneralAssumption)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show("For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {

                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGeneralAssumption(m);
                }
                else if (confirmation == DialogResult.No)
                {


                }
                BindMMProductAssumptionss();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void tabReview_Click(object sender, EventArgs e)
        {

        }

        private void listView1_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void listView1_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listView2_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void listView2_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvTestingAssumption_Click(object sender, EventArgs e)
        {
            if (lvTestingAssumption.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvTestingAssumption.SelectedItems[0];
                MMGeneralAssumption m = (MMGeneralAssumption)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show("For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {

                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGeneralAssumption(m);
                }
                else if (confirmation == DialogResult.No)
                {


                }
                BindMMTestingAssumptionss();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void rbgeneralprotocol_CheckedChanged(object sender, EventArgs e)
        {
            if (rbgeneralprotocol.Checked == true)
            {
                cboYear.Enabled = true;
            }
            else
            {
                cboYear.Enabled = false;
            }
           
        }
        public bool checkYear()
        {
            string _colName = "",_query="";
            if (cboYear.Text != "")
            {
                if (cboYear.Text == "2") _colName = "Month24";
                if (cboYear.Text == "3") _colName = "Month36";
                if (cboYear.Text == "4") _colName = "Month48";
                _query = "SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = 'TestingProtocol' AND column_name = '" + _colName + "'";
               bool status= InsertRepository.getMonth(sqlConnection,_query);
               if (status != true)
                   return true;
               else return false;
            }
            
            return false;
        }

        public void createColumn()
        {
            bool result;
            string _sql = "",_columnName="";
            int noMont = 13, tMonth = 0;
            if (cboYear.Text == "2")
            {
                tMonth = 12;
            }
            if (cboYear.Text == "3")
            {
                tMonth = 24;
            }
            if (cboYear.Text == "4")
            {
                tMonth = 36;
            }
             TextInfo t = new CultureInfo("en-US", false).TextInfo;

             for (int i = 1; i <= tMonth; i++)
            {
                string _month="Month"+Convert.ToString(noMont);
                _columnName = _month;
                _sql = "IF COL_LENGTH('TestingProtocol','" + t.ToTitleCase(_columnName.Trim()) + "') IS NULL BEGIN  alter table TestingProtocol add  [" + t.ToTitleCase(_columnName.Trim()) + "] int  END ";
                result = DatabaseManager.AlterTables(_sql);
                noMont = noMont + 1;
            }
          
        }
        private void btnTPA_Click(object sender, EventArgs e)
        {
            if (cboYear.Text != "")
            {
                this.createColumn();
                if (rbgeneralprotocol.Checked == true)
                {
                    _mMProgram.GTP = rbgeneralprotocol.Checked ? 1 : 0;
                    _mMProgram.NoofYear = Convert.ToInt32(cboYear.Text);
                    DataRepository.SaveOrUpdateMMProgram(_mMProgram);
                }
                else
                {
                    _mMProgram.RTP = rbrapidprotocol.Checked ? 1 : 0;
                    _mMProgram.NoofYear = Convert.ToInt32(cboYear.Text);
                    DataRepository.SaveOrUpdateMMProgram(_mMProgram);
                }
                MessageBox.Show("Testing Protocol save successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            else
            { 
              
            }
        }

       
        private void rdbPosProduct_MouseDown(object sender, MouseEventArgs e)
        {
            if (checKProgramSelected() == true)
            {
            }
            
        }

        private void btnForecastAdd_Click(object sender, EventArgs e)
        {
            Flag = SaveOrUpdateMMForecastParameterObject1(_parameterName);
            BindMMForecastParameters();
            BindForecastMethod();
            BindVariablesDataType();
            txtVariableNameWiz2.Text = string.Empty;
            cobforecastmethodWiz2.Enabled = false;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
