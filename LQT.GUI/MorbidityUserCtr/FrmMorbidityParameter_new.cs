﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LQT.Core.UserExceptions;
using LQT.Core.Util;
using LQT.Core.Domain;
using System.Globalization;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class FrmMorbidityParameter_new : Form
    {
        private MMProgram _mMProgram;
        private IList<MMProgram> _mMPrograms;

        private MMForecastParameter _mMForecastParam;
        private IList<MMForecastParameter> _mMForecastParams;

        private MMGroup _mMGroup;
        private IList<MMGroup> _mMGroups;

        private MMGeneralAssumption _mMGeneralAssumption;
        private IList<MMGeneralAssumption> _mMGeneralAssumptions;
        private string _currentTab = "Define Program";
        private bool Flag;
        public FrmMorbidityParameter_new()
        {
            InitializeComponent();
            BindMMPrograms();
        }

        private void btnProgramAdd_Click(object sender, EventArgs e)
        {
            bool a;
           a= SaveOrUpdateObject();
            BindMMPrograms();
            txtProgramName.Text = string.Empty;
        }
        public void SetColors(ListView lst)
        {
            foreach (ListViewItem item in lst.Items)
                item.BackColor = (item.Index % 2 == 0) ? Color.GhostWhite : Color.WhiteSmoke;
        }
        private void BindMMPrograms()
        {
            _mMPrograms = DataRepository.GetMMPrograms();
            int i = 1;
            lvProgramList.BeginUpdate();
            lvProgramList.Items.Clear();

            foreach (MMProgram mMprogram in _mMPrograms)
            {
                ListViewItem listViewItem = new ListViewItem(i.ToString())
                {
                    Tag = mMprogram
                };
                listViewItem.SubItems.Add(mMprogram.ProgramName);
                listViewItem.SubItems.Add(mMprogram.Description);

                lvProgramList.Items.Add(listViewItem);
                i++;
            }
            lvProgramList.EndUpdate();
            SetColors(lvProgramList);
        }
        private bool SaveOrUpdateObject()
        {

            foreach (MMProgram mMprogram in _mMPrograms)
            {



                if (mMprogram.ProgramName == this.txtProgramName.Text.Trim())
                {

                    MessageBox.Show("Program Name must not be duplicate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }
            if (_mMProgram == null)
                _mMProgram = new MMProgram();

            if (txtProgramName.Text.Trim() == string.Empty)
                throw new LQTUserException("Program Name can not be empty.");

            DataRepository.CloseSession();
            _mMProgram.ProgramName = this.txtProgramName.Text.Trim();

            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
            _mMProgram = null;
            return true;

        }

        private void btnAddWiz2_Click(object sender, EventArgs e)
        {
           
           // Flag = DatabaseManager.AlterTables("0");
            Flag = SaveOrUpdateMMForecastParameterObject();
            BindMMForecastParameters();
            BindForecastMethod();
            BindVariablesDataType();
            txtVariableNameWiz2.Text = string.Empty;
        }
        private bool SaveOrUpdateMMForecastParameterObject()
        {
            string _sql = "";
            bool result;
             TextInfo t = new CultureInfo("en-US", false).TextInfo;
             string UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
            {

             

                if (mMFParam.VariableName == t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) && mMFParam.UseOn == UseOn)
                {

                    MessageBox.Show("Variable Name must not be duplicate for " + UseOn + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

            }

           
            if (_mMForecastParam == null)
                _mMForecastParam = new MMForecastParameter();

            _mMForecastParam.ForecastMethod = (int)Enum.Parse(typeof(MorbidityForecastingMethod), cobforecastmethodWiz2.Text.Trim().Replace(' ', '_'));
           
            _mMForecastParam.VariableName = t.ToTitleCase(txtVariableNameWiz2.Text.Trim());

            _mMForecastParam.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz2.Text.Trim());
            _mMForecastParam.UseOn = rdbeachWiz2.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

            _mMForecastParam.MMProgram = _mMProgram;
            _mMForecastParam.VarCode = LqtUtil.GetUpperCases(_mMForecastParam.VariableName);

            _mMForecastParam.IsPrimaryOutput = chkprimaryoutput.Checked;

            _mMForecastParam.VariableEffect = rdbPosWiz2.Checked ? true : false;
            _mMForecastParam.IsActive = chkActive.Checked;
            _mMProgram.MMForecastParameters.Add(_mMForecastParam);



            DataRepository.SaveOrUpdateMMForecastParameter(_mMForecastParam);

            if (UseOn == "OnEachSite")
            {
                _sql = "IF COL_LENGTH('ForecastSiteInfo','[" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "]') IS NULL BEGIN  alter table ForecastSiteInfo add  [" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "] numeric(18, 2)  END ";
            }
            else
            {
                _sql = "IF COL_LENGTH('ForecastCategoryInfo','[" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "]') IS NULL BEGIN  alter table ForecastCategoryInfo add  [" + t.ToTitleCase(txtVariableNameWiz2.Text.Trim()) + "] numeric(18, 2)  END ";
            }
            result = DatabaseManager.AlterTables(_sql);
            BindMMForecastParameters();
            _mMForecastParam = null;
            return true;

        }

        private void BindMMForecastParameters()
        {
            int i = 1;
            lvforecastParamsWiz2.BeginUpdate();
            lvforecastParamsWiz2.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMFParam
                    };

                    if (mMFParam.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityForecastingMethod)mMFParam.ForecastMethod).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMFParam.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMFParam.VariableName);
                    listViewItem.SubItems.Add(mMFParam.UseOn);
                    listViewItem.SubItems.Add(mMFParam.VariableFormula);
                  
                    lvforecastParamsWiz2.Items.Add(listViewItem);
                    i++;
                }
            }
            lvforecastParamsWiz2.EndUpdate();
            SetColors(lvforecastParamsWiz2);
        

        }
        private void BindReviewMMForecastParameters()
        {
            int i = 1;
            lstReviewForecastingVariable.BeginUpdate();
            lstReviewForecastingVariable.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMForecastParameter mMFParam in _mMProgram.MMForecastParameters)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMFParam
                    };

                    if (mMFParam.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityForecastingMethod)mMFParam.ForecastMethod).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMFParam.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMFParam.VariableName);
                    listViewItem.SubItems.Add(mMFParam.UseOn);
                    listViewItem.SubItems.Add(mMFParam.VariableFormula);

                    lstReviewForecastingVariable.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewForecastingVariable.EndUpdate();
            SetColors(lstReviewForecastingVariable);
        }

        private void btnaddformulaWiz2_Click(object sender, EventArgs e)
        {
            if (AddAddFormula())
            {
                BindMMForecastParameters();
            }
        }
        public bool AddAddFormula()
        {
            FrmAddFormula frm;
            IList<MMForecastParameter> _mMForecastParamss = DataRepository.GetAllForecastParameterByProgram(_mMProgram.Id);


            frm = new FrmAddFormula(_mMForecastParamss);
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMForecastParameter newcat in frm._updatedmMForecastParams)
                {
                    foreach (MMForecastParameter cat in _mMForecastParamss)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            _mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMForecastParameter(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void btnAddWiz3_Click(object sender, EventArgs e)
        {
            SaveOrUpdateMMGroupObject();
            BindMMGroups();
            txtgroupnameWiz3.Text = string.Empty;
        }
        private void SaveOrUpdateMMGroupObject()
        {
            if (_mMGroup == null)
                _mMGroup = new MMGroup();

            _mMGroup.GroupName = txtgroupnameWiz3.Text.Trim();

            _mMGroup.MMProgram = _mMProgram;
            _mMGroup.IsActive = chkActiveGroup.Checked;
            _mMProgram.MMGroups.Add(_mMGroup);


            DataRepository.SaveOrUpdateMMGroup(_mMGroup);

            BindMMGroups();
            _mMGroup = null;

        }

        private void BindMMGroups()
        {
            int i = 1;
            lvGroupWiz3.BeginUpdate();
            lvGroupWiz3.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGroup mMGroup in _mMProgram.MMGroups)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGroup
                    };
                    if (mMGroup.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(mMGroup.GroupName);

                    lvGroupWiz3.Items.Add(listViewItem);
                    i++;
                }
            }
            lvGroupWiz3.EndUpdate();
            SetColors(lvGroupWiz3);
        }
        private void BindReviewMMGroups()
        {

            int i = 1;
            lstReviewPatientGroup.BeginUpdate();
            lstReviewPatientGroup.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGroup mMGroup in _mMProgram.MMGroups)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGroup
                    };
                    if (mMGroup.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(mMGroup.GroupName);

                    lstReviewPatientGroup.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewPatientGroup.EndUpdate();
            SetColors(lstReviewPatientGroup);
        }

        private void btnAddWiz4_Click(object sender, EventArgs e)
        {
           Flag= SaveOrUpdateMMGeneralAssumptionsObject();
            BindMMGeneralAssumptionss();
            txtVariableNameWiz4.Text = string.Empty;
            BindGATypes();
            BindGAVariablesDataType();
        }
        private bool SaveOrUpdateMMGeneralAssumptionsObject()
        {

            string _sql = "";
            bool result;
            TextInfo t = new CultureInfo("en-US", false).TextInfo;
            string UseOn = rdbeachWiz4.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();
            foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
            {
                if (mMGA.VariableName == t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) && mMGA.AssumptionType == (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')))
                {

                    MessageBox.Show("Variable Name must not be duplicate for " + cobAssumptionTypeWiz4.Text.Trim() +" ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            if (_mMGeneralAssumption == null)
                _mMGeneralAssumption = new MMGeneralAssumption();

           
            _mMGeneralAssumption.VariableName = t.ToTitleCase(txtVariableNameWiz4.Text.Trim());

            _mMGeneralAssumption.AssumptionType = (int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_'));

            _mMGeneralAssumption.VariableDataType = (int)Enum.Parse(typeof(MorbidityVariablesDataType), cobDatatypeWiz4.Text.Trim());
            _mMGeneralAssumption.UseOn = rdbeachWiz4.Checked ? MorbidityVariableUsage.OnEachSite.ToString() : MorbidityVariableUsage.OnAggSite.ToString();

            _mMGeneralAssumption.MMProgram = _mMProgram;
            _mMGeneralAssumption.VarCode = LqtUtil.GetUpperCases(_mMGeneralAssumption.VariableName);

            _mMGeneralAssumption.VariableEffect = rdbPosWiz4.Checked ? true : false;


            _mMGeneralAssumption.IsActive = chkGeneralAssumption.Checked;
            _mMProgram.MMGeneralAssumptions.Add(_mMGeneralAssumption);

            DataRepository.SaveOrUpdateMMGeneralAssumption(_mMGeneralAssumption);
            BindMMGeneralAssumptionss();
            _mMGeneralAssumption = null;

            if ((int)Enum.Parse(typeof(MorbidityGeneralAssumptionTypes), cobAssumptionTypeWiz4.Text.Trim().Replace(' ', '_')) == 1)
            {
                _sql = "IF COL_LENGTH('PatientAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table PatientAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";

            }
            else
            {
                _sql = "IF COL_LENGTH('TestingAssumption','[" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "]') IS NULL BEGIN  alter table TestingAssumption add  [" + t.ToTitleCase(txtVariableNameWiz4.Text.Trim()) + "] numeric(18, 2)  END ";
            
            }

                
            result = DatabaseManager.AlterTables(_sql);



            return true;

        }

        private void BindMMGeneralAssumptionss()
        {

            int i = 1;
            lvgeneralassumptions.BeginUpdate();
            lvgeneralassumptions.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lvgeneralassumptions.Items.Add(listViewItem);
                    i++;
                }
            }
            lvgeneralassumptions.EndUpdate();
            SetColors(lvgeneralassumptions);
        }

        private void BindReviewMMGeneralAssumptions()
        {

            int i = 1;
            lstReviewGeneralAssumption.BeginUpdate();
            lstReviewGeneralAssumption.Items.Clear();
            if (_mMProgram != null)
            {
                foreach (MMGeneralAssumption mMGA in _mMProgram.MMGeneralAssumptions)
                {
                    ListViewItem listViewItem = new ListViewItem(i.ToString())
                    {
                        Tag = mMGA
                    };
                    if (mMGA.IsActive == true)
                        listViewItem.SubItems.Add("Yes");
                    else
                        listViewItem.SubItems.Add("No");
                    listViewItem.SubItems.Add(((MorbidityGeneralAssumptionTypes)mMGA.AssumptionType).ToString());
                    listViewItem.SubItems.Add(((MorbidityVariablesDataType)mMGA.VariableDataType).ToString());
                    listViewItem.SubItems.Add(mMGA.VariableName);
                    listViewItem.SubItems.Add(mMGA.UseOn);
                    listViewItem.SubItems.Add(mMGA.VariableFormula);

                    lstReviewGeneralAssumption.Items.Add(listViewItem);
                    i++;
                }
            }
            lstReviewGeneralAssumption.EndUpdate();
            SetColors(lstReviewGeneralAssumption);
        
        }

        private void btnaddformulaWiz4_Click(object sender, EventArgs e)
        {
            if (AddAddFormulaGA())
            {
                BindMMGeneralAssumptionss();
            }
        }
        public bool AddAddFormulaGA()
        {
            FrmAddFormula frm;
            IList<MMGeneralAssumption> _mMGA = DataRepository.GetAllGeneralAssumptionByProgram(_mMProgram.Id);


            frm = new FrmAddFormula(_mMGA, 1, "GA");
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MMGeneralAssumption newcat in frm._updatedmMGeneralAssumptions)
                {
                    foreach (MMGeneralAssumption cat in _mMGA)
                    {
                        if (cat.VariableName == newcat.VariableName)
                        {
                            cat.VariableFormula = newcat.VariableFormula;
                            _mMProgram.MMForecastParameters.Add(cat);
                            DataRepository.SaveOrUpdateMMGeneralAssumption(cat);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        private void chkgeneralprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.GTP = chkgeneralprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
        }

        private void chkrapidprotocol_CheckedChanged(object sender, EventArgs e)
        {
            _mMProgram.RTP = chkrapidprotocol.Checked ? 1 : 0;
            DataRepository.SaveOrUpdateMMProgram(_mMProgram);
        }

        private void lvProgramList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvProgramList.SelectedItems.Count > 0)
            {
                _mMProgram = (MMProgram)lvProgramList.SelectedItems[0].Tag;
                this.txtProgramName.Text = _mMProgram.ProgramName;
            }
            else
            {
                _mMProgram = null;
                this.txtProgramName.Text = string.Empty;
            }
        }
        public void SetColumnHeader(DrawListViewColumnHeaderEventArgs e)
        {
            using (StringFormat sf = new StringFormat())
            {
                // Store the column text alignment, letting it default
                // to Left if it has not been set to Center or Right.
                switch (e.Header.TextAlign)
                {
                    case HorizontalAlignment.Center:
                        sf.Alignment = StringAlignment.Center;
                        break;
                    case HorizontalAlignment.Right:
                        sf.Alignment = StringAlignment.Far;
                        break;
                }
                // Draw the standard header background.
                e.DrawBackground();

                // Draw the header text.
                using (Font headerFont = new Font("Helvetica", 10, FontStyle.Bold)) //Font size!!!!
                {
                    e.Graphics.DrawString(e.Header.Text, headerFont, Brushes.DimGray, e.Bounds, sf);
                }
            }
        }

        private void lvProgramList_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvProgramList_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void BindForecastMethod()
        {
            string[] forecastMethod = Enum.GetNames(typeof(MorbidityForecastingMethod));
            cobforecastmethodWiz2.Items.Clear();
            for (int i = 0; i < forecastMethod.Length; i++)
            {
                cobforecastmethodWiz2.Items.Add(forecastMethod[i].Replace('_', ' '));
            }
        }

        private void BindVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz2.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz2.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void lvgeneralassumptions_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvGroupWiz3_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {

            SetColumnHeader(e);
        }

        private void lvGroupWiz3_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvforecastParamsWiz2_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lvforecastParamsWiz2_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lvgeneralassumptions_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            _currentTab = e.TabPage.Tag.ToString();
            switch (_currentTab)
            {
                case "Define Program":
                    button2.Enabled = false;
                    button1.Enabled = true;
                    break;
                case "Forecasting Method":             
                  BindForecastMethod();
                  BindVariablesDataType();
                  BindMMForecastParameters();
                  button2.Enabled = true;
                  button1.Enabled = true;
                break;
                case "Patient/Population Group":
                   BindMMGroups();
                   button2.Enabled = true;
                   button1.Enabled = true;
                break;
                case "General Assumptions":
                     BindGAVariablesDataType();
                     BindMMGeneralAssumptionss();
                     BindGATypes();
                     button2.Enabled = true;
                     button1.Enabled = true;
                break;
                case "Testing Protocol Options":
                    button2.Enabled = true;
                    button1.Enabled = true;
                break;
                case "Review":
                   lblprogram1.Text = txtProgramName.Text;
                  //  _mMProgram
                    BindReviewMMForecastParameters();
                    BindReviewMMGroups();
                    BindReviewMMGeneralAssumptions();

                    if (chkgeneralprotocol.Checked == true)
                    {
                        lbltestingprotocol1.Text = chkgeneralprotocol.Text;
                    }
                    else if (chkrapidprotocol.Checked == true)
                    {
                        lbltestingprotocol1.Text = chkrapidprotocol.Text;
                    }
                    else if (chkrapidprotocol.Checked == true && chkgeneralprotocol.Checked == true)
                    {
                        lbltestingprotocol1.Text = chkgeneralprotocol.Text + "," + chkrapidprotocol.Text;
                    }
                    else
                    {
                        lbltestingprotocol1.Text = "";
                    }
                    button2.Enabled = true;
                    button1.Enabled = false;
                break;
                   
            }
        }
        private void BindGAVariablesDataType()
        {
            string[] variableDataType = Enum.GetNames(typeof(MorbidityVariablesDataType));
            cobDatatypeWiz4.Items.Clear();
            for (int i = 0; i < variableDataType.Length; i++)
            {
                cobDatatypeWiz4.Items.Add(variableDataType[i].Replace('_', ' '));
            }
        }

        private void BindGATypes()
        {
            string[] variableAssumptionType = Enum.GetNames(typeof(MorbidityGeneralAssumptionTypes));
            cobAssumptionTypeWiz4.Items.Clear();
            for (int i = 0; i < variableAssumptionType.Length; i++)
            {
                cobAssumptionTypeWiz4.Items.Add(variableAssumptionType[i].Replace('_', ' '));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtgroupnameWiz3.Text = string.Empty;
            txtVariableNameWiz2.Text = string.Empty;
            txtVariableNameWiz4.Text = string.Empty;
            switch (_currentTab)
            {
                case "Define Program":                  
                    tabControl1.SelectedTab = tabForecastMethod;
                    break;
                case "Forecasting Method":
                    tabControl1.SelectedTab = tabPatientGroup;
                    break;
                case "Patient/Population Group":
                    tabControl1.SelectedTab = tabGeneralAssumption;
                    break;
                case "General Assumptions":
                    tabControl1.SelectedTab = tabTestingProtocol;
                    break;
                case "Testing Protocol Options":
                    tabControl1.SelectedTab = tabReview;
                    break;
                //case "CONSUMPTION":
                //    SaveConsumption("Consumption Data usage was saved successfully.");
                //    break;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (_currentTab)
            {
             
                case "Forecasting Method":
                    tabControl1.SelectedTab = tabDefprogram;
                    break;
                case "Patient/Population Group":
                    tabControl1.SelectedTab = tabForecastMethod;
                    break;
                case "General Assumptions":
                    tabControl1.SelectedTab = tabPatientGroup;
                    break;
                case "Testing Protocol Options":
                    tabControl1.SelectedTab = tabGeneralAssumption;
                    break;
                case "Review":
                    tabControl1.SelectedTab = tabTestingProtocol;
                    break;
            
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lvforecastParamsWiz2_SelectedIndexChanged(object sender, EventArgs e)
        {
        
        }

        private void lvforecastParamsWiz2_Click(object sender, EventArgs e)
        {
            if (lvforecastParamsWiz2.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvforecastParamsWiz2.SelectedItems[0];
                MMForecastParameter m = (MMForecastParameter)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show("For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                    //DataRepository.DeleteforecastParameter(DataRepository.GetAllForecastParameterById(m.Id));
                  //  _mMProgram.MMForecastParameters.Remove(m);
                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMForecastParameter(m);
                }
                else if (confirmation == DialogResult.No)
                {
                  
                    //_mMProgram.MMForecastParameters.Remove(m);
                    //_mMProgram.MMForecastParameters.Add(m);
                }
                BindMMForecastParameters();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void lvGroupWiz3_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void lvgeneralassumptions_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void lvGroupWiz3_Click(object sender, EventArgs e)
        {
            if (lvGroupWiz3.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvGroupWiz3.SelectedItems[0];
                MMGroup m = (MMGroup)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show("For Activation/InActivation " + m.GroupName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                 //   DataRepository.DeleteProgramGroup(DataRepository.GetProgramGroupbyid(m.Id));
                    //_mMProgram.MMGroups.Remove(m);
                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGroup(m);
                }
                else if (confirmation == DialogResult.No)
                {
                  
                    //_mMProgram.MMForecastParameters.Remove(m);
                    //_mMProgram.MMForecastParameters.Add(m);
                }
                BindMMGroups();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void lvgeneralassumptions_Click(object sender, EventArgs e)
        {
            if (lvgeneralassumptions.SelectedItems.Count > 0)
            {


                ListViewItem itm = lvgeneralassumptions.SelectedItems[0];
                MMGeneralAssumption m = (MMGeneralAssumption)itm.Tag;
                // var confirmation = MessageBox.Show("Do you want to delete " + m.VariableName +  " variable?", "Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                var confirmation = MessageBox.Show( "For Activation/InActivation " + m.VariableName + " variable Press Yes " + Environment.NewLine + "For No operation Press No", "Operation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmation == DialogResult.Yes)
                {
                
                    if (m.IsActive == true)
                    {
                        m.IsActive = false;
                    }
                    else
                    {
                        m.IsActive = true;
                    }
                    DataRepository.SaveOrUpdateMMGeneralAssumption(m);
                }
                else if (confirmation == DialogResult.No)
                {
               
                  
                }
                BindMMGeneralAssumptionss();
            }
            else
            {
                //   MessageBox.Show("aucin stagiaire selectionnes", ...);
            }
        }

        private void lstReviewForecastingVariable_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewForecastingVariable_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstReviewPatientGroup_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewPatientGroup_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void lstReviewGeneralAssumption_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            SetColumnHeader(e);
        }

        private void lstReviewGeneralAssumption_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FrmMorbidityParameter_new_Load(object sender, EventArgs e)
        {

        }
        
     
    }
}
