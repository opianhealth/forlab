﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class OptPopulationBased : NewBaseMorbidityControl
    {
       
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;

        private IList<MorbidityCustomeAssumption> _morbidityAssumptions;
        private bool _isedited = false;

        public IList<MorbidityCustomeAssumption> _updatedmorbidityAssumptions;
        
        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public OptPopulationBased(NewMorbidityForecast forecast)
        {
            this._forecast = forecast;
            
            _sites = DataRepository.GetAllSite();
            _regions = DataRepository.GetAllRegion();
            _morbidityAssumptions = new List<MorbidityCustomeAssumption>();
            InitializeComponent();
            BuildDefaultAssumptions();
            BindCategorys();


           // LoadSiteListView();
        }

        private void BuildDefaultAssumptions()
        {
            MorbidityCustomeAssumption mc = new MorbidityCustomeAssumption();
            mc.MorbidityForecast = _forecast;
            mc.VariableName = "Populaiton size";
            mc.DataType = "Numeric";
            mc.UseForAggregate = false;
            mc.UseForSite = true;
            mc.VariableValue = string.Empty;
            _morbidityAssumptions.Add(mc);

            MorbidityCustomeAssumption mc1 = new MorbidityCustomeAssumption();
            mc1.MorbidityForecast = _forecast;
            mc1.VariableName = "Incident Rate";
            mc1.DataType = "Percentage";
            mc1.UseForAggregate = true;
            mc1.UseForSite = false;
            mc1.VariableValue = string.Empty;

            _morbidityAssumptions.Add(mc1);

            MorbidityCustomeAssumption mc2 = new MorbidityCustomeAssumption();
            mc2.MorbidityForecast = _forecast;
            mc2.VariableName = "Prevalence Rate";
            mc2.DataType = "Percentage";
            mc2.UseForAggregate = true;
            mc2.UseForSite = false;
            mc2.VariableValue = string.Empty;
            _morbidityAssumptions.Add(mc2);
        }

        public override string Title
        {
            get { return "Define Population Based Parameters"; }
        }



        public override string Description
        {
            get
            {
            
                string desc = "<p> Define patient populationa and related assumptions.</p>";

                desc += "<p> System will generate Site/Category Data Import Template based on the assumpitons <br>";

                desc += "<p>1. If Variable is defined as Use on Each Site/Category: Import template will prompt value for each Site/Category";

 
                return desc;
            }
        }

        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.OptPatientGroupChar;
            }
        }

        public override bool EnableNextButton()
        {
            return true;// _artSites.Count > 0;
        }


     
        private void BindCategorys()
        {
            lvCategory.BeginUpdate();
            lvCategory.Items.Clear();

            foreach (MorbidityCustomeAssumption cat in _morbidityAssumptions)
            {
                string[] row = { cat.VariableName, cat.DataType, cat.UseForSite.ToString(), cat.UseForAggregate.ToString(), cat.VariableValue.ToString() };
                var listViewItem = new ListViewItem(row);
                lvCategory.Items.Add(listViewItem);
                //ListViewItem li = new ListViewItem(cat.CategoryName) { Tag = cat };
                //lvCategory.Items.Add(li);
            }

            lvCategory.EndUpdate();
            if (_morbidityAssumptions.Count > 3)
                btnAddFormula.Enabled = true;
        }

        private void lvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (lvCategory.SelectedItems.Count > 0)
            //{
            //    _activeCategory = (MorbidityCategory)lvCategory.SelectedItems[0].Tag; 
            //    if (_activeCategory.Id < 0)
            //        DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
            //    txtCatname.Text = _activeCategory.CategoryName;
            //}
            //else
            //{
            //    _activeCategory = null;
            //    txtCatname.Text = "";
            //}

            //BindForecastCategory();
            //BindArtSites();
        }


        private void lbtAddsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

                OnNextButtonStatusChanged(true);
                _isedited = true;

        }


        private void lbtRemovesite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

             OnNextButtonStatusChanged(false);
            _isedited = true;
        }

        

        private void butSave_Click(object sender, EventArgs e)
        {
            //_activeCategory.CategoryName = txtCatname.Text;

            try
            {
              // DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
               BindCategorys();
            }
            catch
            {
                MessageBox.Show("Error: unable to save custom variable");
            }            
        }

        private void butDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure, you want to delete it? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    //_forecast.MorbidityCategories.Remove(_activeCategory);
                    //DataRepository.SaveOrUpdateMorbidityForecast(_forecast);
                   
                    BindCategorys();
                  
                }
                catch
                {
                    MessageBox.Show("Error: unable to delete custom variable");
                }
            }
        }

        private void butAddnew_Click(object sender, EventArgs e)
        {
            MorbidityCustomeAssumption mc = new MorbidityCustomeAssumption();
            mc.VariableName = txtCatname.Text;
            mc.DataType = comboBox1.SelectedItem.ToString();
            mc.UseForSite = false;
            mc.UseForSite = false;

            if (radioButton1.Checked)
                mc.UseForSite = true;

            if (radioButton2.Checked)
                mc.UseForAggregate = true;
            mc.VariableValue = textBox1.Text;

            mc.MorbidityForecast = _forecast;

            _morbidityAssumptions.Add(mc);

           
            BindCategorys();
           
        }

        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.BatchSaveARTSite(_artSites);
                //DataRepository.BatchDeleteARTSite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }

        private void btnAddFormula_Click(object sender, EventArgs e)
        {
            if (AddAddFormula())
            {

                OnNextButtonStatusChanged(true);
                _isedited = true;
                BindCategorys();
            }
        }

        public bool AddAddFormula()
        {

            FrmAddFormula frm;

            frm = new FrmAddFormula(_morbidityAssumptions, _updatedmorbidityAssumptions);
            if (frm.ShowDialog() == DialogResult.OK)
            {

                foreach (MorbidityCustomeAssumption newcat in frm._updatedmorbidityAssumptions)
                {
                    foreach (MorbidityCustomeAssumption cat in _morbidityAssumptions)
                    {
                        if (cat.VariableName == newcat.VariableName)
                            cat.VariableValue = newcat.VariableValue;
                    }
                }
                return true;
            }
            return false;
        }

        

    }
}
