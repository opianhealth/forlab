﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using LQT.Core.Domain;
using LQT.Core.UserExceptions;
using LQT.Core.Util;

namespace LQT.GUI
{
    public partial class FrmAddFormula : Form
    {
        public IList<int> SelectedSiteIds;
        private IList<int> _selectedSiteids;
        private IList<ForlabSite> _sites;
        private bool _filiterByregion = false;
        private int _regionid;
        public IList<ForlabSite> SelectedSites;

        private IList<MorbidityCustomeAssumption> _morbidityAssumptions;
        public IList<MorbidityCustomeAssumption> _updatedmorbidityAssumptions;

        private IList<MMForecastParameter> _mMForecastParams;
        public IList<MMForecastParameter> _updatedmMForecastParams;

        private IList<MMGeneralAssumption> _mMGeneralAssumptions;
        public IList<MMGeneralAssumption> _updatedmMGeneralAssumptions;
        public string who = string.Empty;

        public FrmAddFormula()
        {
            InitializeComponent();
        }

        public FrmAddFormula(IList<MMForecastParameter> mMForecastParams)
        {
            _mMForecastParams = mMForecastParams;
            _updatedmMForecastParams = new List<MMForecastParameter>();
            InitializeComponent();

            BindMMForecastParams();
        }

        public FrmAddFormula(IList<MMGeneralAssumption> mMGeneralAssumption,int i, string _who)
        {
            _mMGeneralAssumptions = mMGeneralAssumption;
            _updatedmMGeneralAssumptions = new List<MMGeneralAssumption>();
            who = _who;
            InitializeComponent();

            BindMMGeneralAssumption();
        }

        private void BindMMForecastParams()
        {
            cbodefinefor.BeginUpdate();
            cbodefinefor.Items.Clear();

            lvVariables.BeginUpdate();
            lvVariables.Items.Clear();

            foreach (MMForecastParameter cat in _mMForecastParams)
            {
                string[] row = { cat.FvarCode, cat.VariableDataType.ToString(), cat.VariableName, cat.VariableFormula};
                var listViewItem = new ListViewItem(row);
                lvVariables.Items.Add(listViewItem);


                ComboboxItem item = new ComboboxItem();
                item.Text = cat.VariableName;
                item.Value = cat.VariableName;
                cbodefinefor.Items.Add(item);
            }

            lvVariables.EndUpdate();
            cbodefinefor.EndUpdate();
        }

        private void BindMMGeneralAssumption()
        {
            cbodefinefor.BeginUpdate();
            cbodefinefor.Items.Clear();

            lvVariables.BeginUpdate();
            lvVariables.Items.Clear();

            foreach (MMGeneralAssumption cat in _mMGeneralAssumptions)
            {
                string[] row = { cat.FvarCode, cat.VariableDataType.ToString(), cat.VariableName, cat.VariableFormula };
                var listViewItem = new ListViewItem(row);
                lvVariables.Items.Add(listViewItem);


                ComboboxItem item = new ComboboxItem();
                item.Text = cat.VariableName;
                item.Value = cat.VariableName;
                cbodefinefor.Items.Add(item);
            }

            lvVariables.EndUpdate();
            cbodefinefor.EndUpdate();
        }

        public FrmAddFormula(IList<MorbidityCustomeAssumption> morbidityAssumptions, IList<MorbidityCustomeAssumption> updatedmorbidityAssumptions)
        {
            _morbidityAssumptions = morbidityAssumptions;
            updatedmorbidityAssumptions = new List<MorbidityCustomeAssumption>();
            _updatedmorbidityAssumptions = updatedmorbidityAssumptions;
            InitializeComponent();

           
        }

       

        private void button8_Click(object sender, EventArgs e)
        {
            if (who == "GA")
            {
                foreach (MMGeneralAssumption cat in _mMGeneralAssumptions)
                {
                    if (cbodefinefor.SelectedItem.ToString() == cat.VariableName)
                    {
                        cat.VariableFormula = txtformula.Text;
                        _updatedmMGeneralAssumptions.Add(cat);
                    }
                }
                BindMMGeneralAssumption();
            }
            else
            {

                foreach (MMForecastParameter cat in _mMForecastParams)
                {
                    if (cbodefinefor.SelectedItem.ToString() == cat.VariableName)
                    {
                        cat.VariableFormula = txtformula.Text;
                        _updatedmMForecastParams.Add(cat);
                    }
                }
                BindMMForecastParams();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button3.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button6.Text;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button5.Text;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button4.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            txtformula.Text = txtformula.Text + button7.Text;
        }

        private void lvVariables_DoubleClick(object sender, EventArgs e)
        {
            if (lvVariables.SelectedItems.Count == 1)
            {
                ListView.SelectedListViewItemCollection items = lvVariables.SelectedItems;

                ListViewItem lvItem = items[0];
                txtformula.Text = txtformula.Text + lvItem.Text;

            } 
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btndone_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void FrmAddFormula_Load(object sender, EventArgs e)
        {

        }

        private void FrmAddFormula_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

       
    }
}
