﻿namespace LQT.GUI.MorbidityUserCtr
{
    partial class NewOptRecentData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.lqtCheckBox1 = new LQT.GUI.MorbidityUserCtr.LQTCheckBox();
            this.lqtCheckBox2 = new LQT.GUI.MorbidityUserCtr.LQTCheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lqtCheckBox3 = new LQT.GUI.MorbidityUserCtr.LQTCheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lqtCheckBox4 = new LQT.GUI.MorbidityUserCtr.LQTCheckBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(91, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(387, 57);
            this.label3.TabIndex = 2;
            this.label3.Text = "1- Population based forecast";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lqtCheckBox1
            // 
            this.lqtCheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.lqtCheckBox1.Checked = false;
            this.lqtCheckBox1.Location = new System.Drawing.Point(44, 51);
            this.lqtCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            this.lqtCheckBox1.Name = "lqtCheckBox1";
            this.lqtCheckBox1.Size = new System.Drawing.Size(33, 33);
            this.lqtCheckBox1.TabIndex = 8;
            this.lqtCheckBox1.Tag = "1";
            // 
            // lqtCheckBox2
            // 
            this.lqtCheckBox2.BackColor = System.Drawing.Color.Transparent;
            this.lqtCheckBox2.Checked = false;
            this.lqtCheckBox2.Location = new System.Drawing.Point(44, 128);
            this.lqtCheckBox2.Margin = new System.Windows.Forms.Padding(0);
            this.lqtCheckBox2.Name = "lqtCheckBox2";
            this.lqtCheckBox2.Size = new System.Drawing.Size(33, 33);
            this.lqtCheckBox2.TabIndex = 9;
            this.lqtCheckBox2.Tag = "2";
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(91, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(387, 57);
            this.label1.TabIndex = 10;
            this.label1.Text = "2- Target based forecast";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(91, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(387, 57);
            this.label2.TabIndex = 12;
            this.label2.Text = "3- Define custom Model/Dynamic-Formula";
            // 
            // lqtCheckBox3
            // 
            this.lqtCheckBox3.BackColor = System.Drawing.Color.Transparent;
            this.lqtCheckBox3.Checked = false;
            this.lqtCheckBox3.Location = new System.Drawing.Point(44, 206);
            this.lqtCheckBox3.Margin = new System.Windows.Forms.Padding(0);
            this.lqtCheckBox3.Name = "lqtCheckBox3";
            this.lqtCheckBox3.Size = new System.Drawing.Size(33, 33);
            this.lqtCheckBox3.TabIndex = 11;
            this.lqtCheckBox3.Tag = "3";
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(91, 275);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(387, 57);
            this.label4.TabIndex = 14;
            this.label4.Text = "4. Get custom pre-defined formulas from online/internet";
            // 
            // lqtCheckBox4
            // 
            this.lqtCheckBox4.BackColor = System.Drawing.Color.Transparent;
            this.lqtCheckBox4.Checked = false;
            this.lqtCheckBox4.Location = new System.Drawing.Point(44, 283);
            this.lqtCheckBox4.Margin = new System.Windows.Forms.Padding(0);
            this.lqtCheckBox4.Name = "lqtCheckBox4";
            this.lqtCheckBox4.Size = new System.Drawing.Size(33, 33);
            this.lqtCheckBox4.TabIndex = 13;
            this.lqtCheckBox4.Tag = "4";
            // 
            // NewOptRecentData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lqtCheckBox4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lqtCheckBox3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lqtCheckBox2);
            this.Controls.Add(this.lqtCheckBox1);
            this.Controls.Add(this.label3);
            this.Name = "NewOptRecentData";
            this.Size = new System.Drawing.Size(716, 367);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private LQTCheckBox lqtCheckBox1;
        private LQTCheckBox lqtCheckBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private LQTCheckBox lqtCheckBox3;
        private System.Windows.Forms.Label label4;
        private LQTCheckBox lqtCheckBox4;
    }
}
