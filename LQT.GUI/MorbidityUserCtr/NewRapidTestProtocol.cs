﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using LQT.Core;
using LQT.Core.Util;
using LQT.Core.Domain;

namespace LQT.GUI.MorbidityUserCtr
{
    public partial class NewRapidTestProtocol : NewBaseMorbidityControl
    {
        private SiteListView _sListView;
        private NewMorbidityForecast _forecast;
        private NewMorbidityCategory _activeCategory;
        private IList<ForlabSite> _sites;
        private IList<ForlabRegion> _regions;
        private IList<NewMordidityCategorySite> _artSites;
        private IList<NewMordidityCategorySite> _deletedArtSites;
        private bool _isedited = false;

        private IList<Test> _test;

        private IList<MasterProduct> _product;

        enum BooleanColumnName
        {
            VCT,
            CD4,
            Chemistry,
            Hematology,
            ViralLoad,
            OtherTest,
            Consumable
        }

        public NewRapidTestProtocol()
        {
            //this._forecast = forecast;
           
            //_sites = DataRepository.GetAllSite();
            //_regions = DataRepository.GetAllRegion();
            _test=new List<Test>();
            _product = new List<MasterProduct>();
            InitializeComponent();
            //LoadSiteListView();
            BindTest();
            
        }

        public override string Title
        {
            get { return " Rapid Test Protocol"; }
        }

        public override string Description
        {
            get
            {

                string desc = "<p> Rapid Test Protocol.</p>";

               // desc += "<p>1. Add Tests to include in the forecast. General Tests, excluding HIV Rapid Test</p><br>";
                //desc += "<p>2. For The Added Tests, Select and define Protocol<br>";


                return desc;
            }
        }
        public override MorbidityCtrEnum NextCtr
        {
            get
            {
                return MorbidityCtrEnum.NewReviewadjustment;
            }
        }

        public override bool EnableNextButton()
        {
            return true;// 1;// _artSites.Count > 0;
        }



        public override bool DoSomthingBeforeUnload()
        {
            //bool result = true;
            if (_isedited)
            {
                //DataRepository.BatchSaveNewMordidityCategorySite(_artSites);
                //DataRepository.BatchDeleteNewMordidityCategorySite(_deletedArtSites);
                MorbidityForm.ReInitMorbidityFrm();
            }
            return true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //FrmNewPickProducts
            if (AddProduct())
            {
                BindProducts();
                // SetStatusOfCatRadioButton();

                OnNextButtonStatusChanged(true);
                _isedited = true;
            }
        }


        public bool AddProduct()
        {
            FrmNewPickProducts frm;

            frm = new FrmNewPickProducts();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (MasterProduct t in frm._newSelectedProduct)
                {
                    _product.Add(t);
                }
                return true;
            }
            return false;
        }

        private void BindProducts()
        {
            lvCategory.BeginUpdate();
            lvCategory.Items.Clear();

            foreach (MasterProduct t in _product)
            {
                ListViewItem li = new ListViewItem(t.ProductType.TypeName) { Tag = t };

                li.SubItems.Add(t.ProductName);
                lvCategory.Items.Add(li);
            }

            lvCategory.EndUpdate();
        }

        private void BindTest()
        {

            cbotest.DataSource = DataRepository.GetAllTests();//.GetAllProductType();



        }


    }
}
